$("input[name='imagen[]']").each(function(){
    var file = $(this).val();
    var ext = file.substring(file.lastIndexOf("."));
    if(ext != ".xls" && ext != ".xlsx")
    {
        valido = false;
        return false;
    }
    else
    {
        valido = true;
        return true;
    }  
});
//Ahora validamos el formulario
 
if(valido == false)
{
    alert("La extensión " + ext + " no es una imagen");
}

