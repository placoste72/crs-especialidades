<%-- 
    Document   : Agendar
    Created on : 18-10-2016, 09:52:55 AM
    Author     : Informatica
--%>

<%@page import="Modelos.procedimiento"%>
<%@page import="Controlador.controlador_cupos"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.General"%>
<%@page import="Controlador.controlador_cargar_masiva_citas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:include page="../comunes/Header.jsp" />

<%@ page import="java.sql.*,java.net.URL,java.util.Date,java.util.GregorianCalendar,java.util.Vector,java.text.DateFormat,java.util.Locale,java.util.Calendar" %>

<link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/jquery-ui.js" type="text/javascript"></script>

<link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
<style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
<script type="text/javascript" src="../../public/js/calendar.js"></script>
<script type="text/javascript" src="../../public/js/calendar-es.js"></script>
<title>Carga Masiva de Citas</title>
<script type="text/javascript" src="../../public/js/calendar-setup.js"></script>   

<div class="container"> 
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">

                <form id="planifica" name="planifica" action="cargaarchivotemporal.jsp"  method="post" >
                    <fieldset>
                        
                        <legend class="text-center header">Carga Masiva de Citas</legend>


                        <table class="center-block2" aling="center" >
                            <tr>
                                <td colspan="2">
                                    <div class="form-group">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6 text-center">
                                        Bienvenido a la carga masiva de citas, para esta ejecución se debe buscar y subir un archivo Excel, con los registros de los pacientes que solicitan horas con los especialista.<br>Debe preocuparse que la información sea consistente
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="form-group">

                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-photo bigicon"></i></span>
                                    <div class="col-md-7" >

                                        <input  id="cargaarchivo" name="cargaarchivo" class="file"  type="file">
                                    </div>
                                </div>     

                                </td>
                            </tr>

                            

                        </table>
                        <br>
                        <br>
                        <br>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary" >Cargar y Validar Archivo</button>

                            </div>
                        </div>

                    </fieldset>
                </form>
            </div></div>
    </div>
</div>
</body>
 

<jsp:include page="../comunes/Footer.jsp" />