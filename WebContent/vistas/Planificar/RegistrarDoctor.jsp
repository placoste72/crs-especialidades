<%-- 
    Document   : RegistrarDoctor
    Created on : 17-10-2016, 03:26:04 PM
    Author     : Informatica
--%>

<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="Modelos.especialidades"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.General" %>
<!DOCTYPE html>

<jsp:include page="../comunes/Header.jsp"/>

<%int especialidad = 0;
    controlador_especialidad ce = new controlador_especialidad();
    controlador_doctor cd = new controlador_doctor();

    ArrayList lista_opciones = ce.buscarListaEspecialidadessinDoctor("-1");
    Iterator it = lista_opciones.iterator();

    ArrayList lista_vacia = ce.buscarListaEspecialidadesconDoctor("-1");
    Iterator it2 = lista_vacia.iterator();

    if (request.getParameter("men") != null) {
        String mensaje = "";
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Registrar Profesional">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>


<%
    }

%>
<script>

    function formateaRut(Rut)
    {
        var sRut = new String(Rut);
        var sRutFormateado = '';
        sRut = quitaFormato(sRut);
        var sDV = sRut.charAt(sRut.length - 1);
        sRut = sRut.substring(0, sRut.length - 1);
        document.forms["doctor"].txtRutSinDV.value = sRut;
        document.forms["doctor"].txtDV.value = sDV;
        while (sRut.length > 3)
        {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
        }
        sRutFormateado = sRut + sRutFormateado;
        if (sRutFormateado != "")
            sRutFormateado += "-";
        sRutFormateado += sDV;
        if (document.forms["doctor"].rutdoctor.value != sRutFormateado)
            document.forms["doctor"].rutdoctor.value = sRutFormateado;
    }
    function quitaFormato(Nro)
    {
        var strNro = new String(Nro);
        while (strNro.indexOf(".") != - 1)
            strNro = strNro.replace(".", "");
        strNro = strNro.replace("-", "");
        return strNro;
    }

    function validaRut() {
        var sRut = new String(document.getElementById('rutdoctor').value);
        sRut = quitaFormato(sRut);
        var sDV = new String(sRut.charAt(sRut.length - 1));
        sRut = sRut.substring(0, sRut.length - 1);
        if (sDV.toUpperCase() == DigitoVerificadorRut(sRut))
        {
            return true;
        }
        if (sDV.toUpperCase() != DigitoVerificadorRut(sRut))
        {
            return false;
        }
    }

    function DigitoVerificadorRut(strRut) {
        var rut = 0;
        var s = 0;
        var l_dv = "";

        rut = strRut;
        for (i = 2; i < 8; i++) {
            s = s + (rut % 10) * i;
            rut = (rut - (rut % 10)) / 10;
        }
        s = s + (rut % 10) * 2;
        rut = (rut - (rut % 10)) / 10;
        s = s + (rut % 10) * 3;
        rut = (rut - (rut % 10)) / 10;
        s = 11 - (s % 11);
        if (s == 10)
            l_dv = "K";
        else
        if (s == 11)
            l_dv = "0"
        else
            l_dv = s + "";
        return(l_dv);
    }



    function validar()
    {
        if (!validaRut()) {

            Alert.render("Rut invalida,Debe Completar los Datos para Continuar")
            return false;

        }
        if(validateIndustry() == false){
             Alert.render("Debe decir la especialidad del Profesional,Debe Completar los Datos para Continuar");
            return false;
        }
    }
    function pone() {

        if (!isSelected("#fromSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
        $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
        return false;

    }
    function saca() {

        //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
        if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
        $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

    }

    function todos() {
        selectAll('#fromSelectBox');
        pone();
    }

    function ninguno() {
        selectAll('#toSelectBox');
        saca();
    }

    function selecciona_todos() {
        selectAll('#toSelectBox');
    }

    //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
    function isSelected(thisObj) {
        if (!$(thisObj + " option:selected").length) {
            Alert.render("Seleccione al menos uno!!Debe Completar los Datos para Continuar")
            return 0;
        }
        return 1;
    }

    //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
    function noOptions(thisObj) {
        if (!$(thisObj + " option").length) {
            // alert("There are no options to select/move");
            return 0;
        }
        return 1;
    }

    //Below function is to de-select all items if any of the item(s) are selected
    function clearAll(thisObj) {
        $('#' + thisObj).each(function () {
            $(this).find('option:selected').removeAttr("selected");
        });
    }//function close

//Below function is to select all items
    function selectAll(thisObj) {
        if (!noOptions("#" + thisObj)) {
            return;
        }
        $('#' + thisObj + ' option').each(function () {
            $(this).attr("selected", "selected");
        });
    }
    $(document).ready(function () {
        $("#rutdoctor").on('paste', function (e) {
            e.preventDefault();
            Alert.render('Esta acci�n est� prohibida');
        })

        $("#rutdoctor").on('copy', function (e) {
            e.preventDefault();
            Alert.render('Esta acci�n est� prohibida');
        })
    })
    
    
     function validateIndustry()
    {
         
            if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
                return false;
               
            }
           
    }
</script>
<div class="container">
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#crear">Crear</a></li>
        <li><a data-toggle="tab" href="#lista">Listado</a></li>

    </ul>
    <div class="tab-content">
        <div id="crear" class="tab-pane fade in active">





            <form name="doctor" enctype='multipart/form-data' class="form-horizontal" id="doctor" action='<%=ce.getLocal()%>ingresar_doctor' method="post" onsubmit="return validar()" >

                <fieldset>

                    <legend class="text-center header">Registra Profesional</legend>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-credit-card bigicon"></i></span>
                        <div class="col-md-8">

                            <input type="text" id="rutdoctor" name="rutdoctor" style="text-transform:uppercase;" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                    return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Doctor">
                            <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                            <input value="d" name="txtDV" id="txtDV" type="hidden">
                        </div>
                    </div>
                    <div id="busca">
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="text" id="nombredoctor" name="nombredoctor" class="form-control" placeholder="Nombre Profesional" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="text" id="apellidopdoctor" name="apellidopdoctor" class="form-control" placeholder="Apellido Profesional" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="text" id="apellidomdoctor" name="apellidomdoctor" class="form-control" placeholder="Apellido Profesional" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="tel" id="telefonodoctor" name="telefonodoctor" class="form-control" placeholder="+56 9 1111 1111" maxlength="11"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="email" id="emaildoctor" name="emaildoctor" class="form-control" placeholder="Email Profesional" >
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <p class="letra" style="margin-left: 450px" >La Imagen deber ser de 150 X 150 P�xeles</p> 

                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon" style=" font-size: 14px">Firma <br> Digital</i></span>
                        <div class="col-md-9" >

                            <input  id="file-0a" name="file-0a" class="file"    type="file"  multiple data-min-file-count="1" >
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-xing bigicon" style=" font-size: 14px">Profesi�n</i></span>
                        <div class="col-md-8">
                            <input type="text" id="profesion" name="profesion" class="form-control" placeholder="Profesion " required oninvalid="setCustomValidity('El campo profesion es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                    <div   style=" padding-left: 240px">

                        <h4 class="letra2 "> Eliga las especialidades del Doctor </h4>
                        <div class="col-md-8 text-center">
                            <table class="table">
                                <tr style="align-content: center"  >
                                    <td>
                                        <select  id="fromSelectBox" name="especialidades2"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px; width: 260px ">

                                            <%

                                                while (it.hasNext()) {
                                                    especialidades e = (especialidades) it.next();
                                                    out.write("   <option value='" + e.getId_especialidad() + "' >" + e.getNombre() + "</option>");
                                                }
                                            %>

                                        </select>
                                    </td>
                                    <td style="width:5px">
                                        <span class='cell' >
                                            <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon " onclick="pone();
                                                    return false"></i>
                                            <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca();
                                                    return false"></i>

                                        </span>

                                    </td>       
                                    <td>
                                        <select  id="toSelectBox" name="especialidades"  size="10" multiple="multiple" style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px ">


                                            <%
                                                out.write("   <option value= -1  > Seleccione una Opcion</option>");
                                                while (it2.hasNext()) {
                                                    especialidades e2 = (especialidades) it2.next();
                                                    out.write("   <option value='" + e2.getId_especialidad() + "' >" + e2.getNombre() + "</option>");
                                                }
                                            %>

                                        </select>
                                    </td>    

                                </tr> 

                            </table>


                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Crear </button>

                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                        </div>
                    </div>


                </fieldset>

            </form>


        </div>

        <div id="lista" class="tab-pane fade">
            <fieldset>
                <legend class="text-center header">Lista de Profesionales </legend>

                <table class="table table-striped" style="width: 100%">
                    <thead>
                        <tr>
                            <th><strong>Rut</strong></th>
                            <th><strong>Nombre</strong></th>
                            <th><strong>Telefono</strong></th>
                            <th><strong>Email</strong></th>
                            <th><strong>Actualizar</strong></th>
                            <th><strong>Eliminar</strong></th>

                        </tr>
                    </thead>
                    <%
                        for (doctor temp : cd.buscarDoctores()) {
                    %>

                    <tr>



                        <td><%=temp.getRut()%></td>
                        <td><%=temp.getNombre() + "  " + temp.getApellido_paterno()%></td>
                        <td><%=temp.getTelefono()%></td>
                        <td><%=temp.getEmail()%></td>

                        <td align="center">
                            <a href="ActualizarDoctor.jsp?cod=<%=temp.getRut()%>" onClick="return popup2(this, 'notes')" class="button3">
                                Actualizar
                            </a>


                        </td>
                        <td align="center">
                            <a href='<%=ce.getLocal()%>SEliminarDoctor?cod=<%=temp.getRut()%>' class="button2"> Eliminar

                            </a>
                        </td>



                    </tr>
                    <%}%>
                </table>
            </fieldset>
        </div>
    </div>
</div>

<script>
    $('#file-fr').fileinput({
        language: 'fr',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
    });
    $('#file-es').fileinput({
        language: 'es',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
    });
    $("#file-0").fileinput({
        'allowedFileExtensions': ['jpg', 'png', 'gif'],
    });
    $("#file-1").fileinput({
        uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    /*
     $(".file").on('fileselect', function(event, n, l) {
     alert('File Selected. Name: ' + l + ', Num: ' + n);
     });
     */
    $("#file-3").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-lg",
        fileType: "any",
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
    });
    $("#file-4").fileinput({
        uploadExtraData: {kvId: '10'}
    });
    $(".btn-warning").on('click', function () {
        if ($('#file-4').attr('disabled')) {
            $('#file-4').fileinput('enable');
        } else {
            $('#file-4').fileinput('disable');
        }
    });
    $(".btn-info").on('click', function () {
        $('#file-4').fileinput('refresh', {previewClass: 'bg-info'});
    });
    /*
     $('#file-4').on('fileselectnone', function() {
     alert('Huh! You selected no files.');
     });
     $('#file-4').on('filebrowse', function() {
     alert('File browse clicked for #file-4');
     });
     */
    $(document).ready(function () {
        $("#test-upload").fileinput({
            'showPreview': false,
            'allowedFileExtensions': ['jpg', 'png', 'gif'],
            'elErrorContainer': '#errorBlock'
        });
        /*
         $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
         alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
         });
         */
    });
</script>



<jsp:include page="../comunes/Footer.jsp"/>