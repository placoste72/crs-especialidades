<%-- 
    Document   : Registrar Usuario
    Created on : 26-09-2016, 03:37:04 PM
    Author     : Informatica
--%>
<%@page import="Modelos.funcionario"%>
<%@page import="Controlador.General" %>
<%@page import="Modelos.usuario"%>
<%@page import="Modelos.rol"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controlador.controlador_usuario"%>
<jsp:include page="../comunes/Header.jsp" />  

<title>Registrar Usuario</title>
<!DOCTYPE html>

<% String rut = "";
    int rol = 0;
    controlador_usuario cu = new controlador_usuario();
%>
<script>
    function validar() {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var x = document.forms["f"]["funcionario"].value;
        var y = document.forms["f"]["nombreusuario"].value;
        var z = document.forms["f"]["email"].value;
        var a = document.forms["f"]["roles"].value;
        if (x == null || x == "" || y == null || y == "" || z == null || z == "" || !expr.test(z) || a == null || a == "") {
            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }
    }

    function validaremail() {
        var z = document.forms["f"]["email"].value;

        extensiones_permitidas = new Array("@redsalud.gob.cl", "@redsalud.gov.cl" , "@gmail.com");
        mierror = "";

        //recupero la extensi�n de este nombre de archivo 
        extension = (z.substring(z.lastIndexOf("@"))).toLowerCase();
        //alert (extension); 
        //compruebo si la extensi�n est� entre las permitidas 

        permitida = 0;
        for (var i = 0; i < extensiones_permitidas.length; i++) {

            if (extensiones_permitidas[i] == extension) {

                permitida = 1;
                break;

            }
        }
        if (permitida == 0) {
            mierror = "Comprueba la extensi�n del correo. \nS�lo se pueden con extensiones: " + extensiones_permitidas.join();


            //si estoy aqui es que no se ha podido submitir 
            Alert.render(mierror);
            return false;
        }
        if(validateIndustry()== false){
           
            return false;
        }
     
        
        

    }



    //para elegir

    function pone() {

        if (!isSelected("#fromSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
        $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
        return false;

    }
    function saca() {

        //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
        if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
        $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

    }

    function todos() {
        selectAll('#fromSelectBox');
        pone();
    }

    function ninguno() {
        selectAll('#toSelectBox');
        saca();
    }

    function selecciona_todos() {
        selectAll('#toSelectBox');
    }

    //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
    function isSelected(thisObj) {
        if (!$(thisObj + " option:selected").length) {
            Alert.render("Debe Elejir una opcion de Perfil !!Debe Completar los Datos para Continuar");
            return 0;
        }
        return 1;
    }

    //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
    function noOptions(thisObj) {
        if (!$(thisObj + " option").length) {
            // alert("There are no options to select/move");
            return 0;
        }
        return 1;
    }

    //Below function is to de-select all items if any of the item(s) are selected
    function clearAll(thisObj) {
        $('#' + thisObj).each(function () {
            $(this).find('option:selected').removeAttr("selected");
        });
    }//function close

//Below function is to select all items
    function selectAll(thisObj) {
        if (!noOptions("#" + thisObj)) {
            return;
        }
        $('#' + thisObj + ' option').each(function () {
            $(this).attr("selected", "selected");
        });
    }

    function validateIndustry()
    {
         
            if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
                return false;
               
            }
           
    }
</script>

<%
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Registar Usuario">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>

<%
    }

    ArrayList lista_opciones = cu.buscarListaRol();
    Iterator it = lista_opciones.iterator();

    ArrayList lista_vacia = cu.buscarListaRolconUsuario(-1);
    Iterator it2 = lista_vacia.iterator();

%>


<div class="container"> 
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>


    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#crear">Crear</a></li>
        <li><a data-toggle="tab" href="#lista">Listado</a></li>

    </ul>
    <div class="tab-content">
        <div id="crear" class="tab-pane fade in active">





            <form id="f" name="f" class="form-horizontal" action='<%=cu.getLocal()%>ingresar_usuario' method="post" onsubmit="return validaremail()" >
                <fieldset>

                    <legend class="text-center header">Registra Usuario</legend>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                        <div class="col-md-8" >
                            <select class="form-control" id="funcionario" name="funcionario">
                                <option>Elegir Funcionario
                                    <%
                                        for (funcionario funci : cu.BuscarUsuariossinAccesoparaCrearAcceso()) {

                                            if (funci.getRut() == rut) {%>
                                <option selected value="<%=funci.getRut()%>"><%=funci.getNombre()%><%
                                } else {%>
                                <option value="<%=funci.getRut()%>"><%=funci.getNombre()%><%
                                        }

                                    }
                                    %>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
                        <div class="col-md-8" >
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" onchange="return validarEmail(this.from)" required oninvalid="setCustomValidity('El campo email es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>


                    <div  style=" padding-left: 240px">
                        <h4 class="letra2"> Eliga los Perfiles del Usuario </h4>
                        <div class="col-md-8 text-center">

                            <table class="table">
                                <tr>
                                    <td>
                                        <select  id="fromSelectBox" name="roles2"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px;  width: 260px ">

                                            <%
                                                while (it.hasNext()) {
                                                    rol r = (rol) it.next();
                                                    out.write("   <option value='" + r.getIdRol() + "' >" + r.getNombreRol() + "</option>");
                                                }
                                            %>

                                        </select>
                                    </td>
                                    <td style="width:5px">
                                        <span class='cell' >
                                            <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon " onclick="pone();
                                                    return false"></i>
                                            <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca();
                                                    return false"></i>

                                        </span>

                                    </td>       
                                    <td>
                                        <select  id="toSelectBox" name="roles"  size="10" multiple="multiple" style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px ">

                                            <%
                                                out.write("   <option value= -1  > Seleccione una Opcion</option>");
                                                while (it2.hasNext()) {
                                                    rol r2 = (rol) it2.next();
                                                    out.write("   <option value='" + r2.getIdRol() + "' >" + r2.getNombreRol() + "</option>");
                                                }
                                            %>

                                        </select>
                                    </td>    

                                </tr> 

                            </table>


                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Crear</button>

                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>

        <div id="lista" class="tab-pane fade">
            <fieldset>
                <legend class="text-center header">Lista de Usuarios</legend>
                <table class="table table-striped" style="width: 100%">
                    <thead>
                        <tr><th><strong>Codigo</strong></th>
                            <th><strong>Nombre Usuario</strong></th>
                            <th><strong>Email</strong></th>
                            <th><strong>Fecha Registro</strong></th>
                            <th><strong>Actualizar</strong></th>
                            <th><strong>Eliminar</strong></th>

                        </tr>
                    </thead>
                    <%
                        for (usuario temp : cu.buscarUsuarios()) {
                    %>

                    <tr>



                        <td><%=temp.getId_usuairo()%></td>
                        <td><%=temp.getNombre_usuario()%></td>
                        <td><%=temp.getEmail()%></td>
                        <td><%=temp.getFecha_registro()%></td>


                        <td align="center">
                            <a href="ActualizarUsuario.jsp?cod=<%=temp.getId_usuairo()%>" onClick="return popup2(this, 'notes')" class="button3">
                                Actualizar 
                            </a>


                        </td>
                        <td align="center">
                            <a href='<%=cu.getLocal()%>SEliminarUsuario?cod=<%=temp.getId_usuairo()%>' onclick="return confirma()" class="button2"> Eliminar

                            </a>
                        </td>


                    </tr>
                    <%}%>
                </table>
            </fieldset>
        </div>
    </div>
</div>


<jsp:include page="../comunes/Footer.jsp" />  
