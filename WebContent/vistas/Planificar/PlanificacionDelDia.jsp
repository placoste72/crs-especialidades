<%-- 
    Document   : PlanificacionDelDia
    Created on : 07-12-2016, 04:42:56 PM
    Author     : Informatica
--%>

<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Controlador.General"%>
<%@page import="java.sql.ResultSet"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
        <link href="../../public/css/estilos.css" rel="stylesheet">

        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>

        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>




    </head>
    <%
        controlador_cita cc = new controlador_cita();

        String fecha = request.getParameter("fecha");

        String especialidad = request.getParameter("especialidad");
        String doctor = request.getParameter("doctor");
        String where = " pl.fecha = '" + fecha + "'";
        if (!especialidad.equals("-1")) {
            where = where + " and e.id_especialidad= " + especialidad + "";
        }
        if (!doctor.equals("-1")) {
            where = where + " and d.rut = '" + doctor + "'";
        }


    %>
    <body>
        <table class="table table-striped" style=" width: 100%" >
            <thead>
                <tr>
                    <th> N� </th>
                    <th>Tipo</th>
                    <th>Especialidad</th>

                    <th>Profesional</th>
                    <th>Tipo Atecion </th>
                    <th>Hora Cita</th>
                    <th>Motivo de Cita</th>
                    <th>Rut</th>
                    <th>Paciente</th>
                    <th>Edad</th>
                    <th>Prevision</th>



                    <th>Hora de Recepci�n</th>
                    <th>Funcionario que Recepciona</th>
                    <th>Estado de la Cita</th>


                </tr>
            </thead>


            <% int i = 1;
                   for (cita c : cc.buscarInformaciondeCitasporDiaoEspecialidadoProfesional(where)) {%>
            <tr>
                <td><%=i%> </td>
                <td><%=c.getTemporales7()%></td>

                <td><%=c.getTemporales()%></td>

                <td><%=c.getRut_doctor()%></td>
                <td><%=c.getTemporales5()%></td>
                <td><%=c.getTemporales1()%></td>
                <td><%=c.getTemporales6()%></td>
                <td><%=c.getRut_paciente()%></td>
                <td><%=c.getTemporales2()%></td>
                <td><%=c.getTemporales3()%></td>
                
               
                <td><%=c.getTemporales4()%></td>

                <td><%=c.getMotivo_cancela()%></td>
                 <td><%=c.getTemporales19() %></td>
                <%if (c.getEstatus() == 2) {%>
                <td style=" background-color: #ff4d4d"><%=c.getMotivo()%></td>
                <%} else if (c.getEstatus() == 4) {%>
                <td style=" background-color: #b3ffb3" ><%=c.getMotivo()%></td>
                <%} else if (c.getEstatus() == 1) {%>
                <td style=" background-color: #ffffcc" ><%=c.getMotivo()%></td>
                <%} else {%>
                <td style=" background-color: #ff9980" ><%=c.getMotivo()%></td>
                <%}%>
            </tr> 
            <%++i;
                  }%>
        </table>
    </body>
</html>
