<%-- 
    Document   : Agendar
    Created on : 18-10-2016, 09:52:55 AM
    Author     : Informatica
--%>

<%@page import="Modelos.procedimiento"%>
<%@page import="Controlador.controlador_cupos"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.General"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:include page="../comunes/Header.jsp" />

<%@ page import="java.sql.*,java.net.URL,java.util.Date,java.util.GregorianCalendar,java.util.Vector,java.text.DateFormat,java.util.Locale,java.util.Calendar" %>

<link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/jquery-ui.js" type="text/javascript"></script>

<link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
<style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
<script type="text/javascript" src="../../public/js/calendar.js"></script>
<script type="text/javascript" src="../../public/js/calendar-es.js"></script>
<title>Planificar Cupos</title>
<script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
<%    int idatencion = 0;
    int tipo = 0;
    controlador_doctor cd = new controlador_doctor();
    controlador_cupos cc = new controlador_cupos();
    General g = new General();
    String rut = "";
    if (request.getParameter("funcionario") != null) {
        rut = request.getParameter("funcionario");
    }
    String control = "";
    if (request.getParameter("fecha_inicio") != null) {
        control = request.getParameter("fecha_inicio");
    }
    especialidades e = null;
    String especialidad = request.getParameter("especialidad");
    if (especialidad == null) {
    } else {

        controlador_especialidad ce = new controlador_especialidad();
        e = ce.buscarEspecialidadCodigo(Integer.parseInt(especialidad));
    }
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Agendar Cupos">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>


<%
    }

    String fecha;
    String fecha2;
    Calendar fe = Calendar.getInstance();
    if (request.getParameter("fecha_inicio") != null) {
        fecha = request.getParameter("fecha_inicio");

    } else {

        int año = fe.get(Calendar.YEAR);
        int mes = fe.get(Calendar.MONTH) + 1;
        int dia = fe.get(Calendar.DAY_OF_MONTH);

        String cad_valor = String.valueOf(dia);

        if (cad_valor.length() < 2) {
            cad_valor = "0" + cad_valor;
        }

        String cad_mes = String.valueOf(mes);

        if (cad_mes.length() < 2) {
            cad_mes = "0" + cad_mes;
        }

        fecha = cad_valor + "/" + cad_mes + "/" + año;

    }


%>




<script>

    $(function () {
        $("#fecha_inicio").datepicker();
    });

    function buscarprestacion() {
        var a = document.forms["planifica"]["atencion"].value;
        if (a == 3) {
            document.getElementById('datosporcirugia').style.display = 'block';

        } else {
            document.getElementById('datosporcirugia').style.display = 'none';
        }

        var e = document.forms["planifica"]["id_especialidad"].value;

        try
        {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e)
        {
            try
            {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
            xmlhttp = new XMLHttpRequest();
        }

        if (xmlhttp) {
            var objeto_recibidor = document.getElementById("pres");
            xmlhttp.open("post", "comboprestacion.jsp?especialidad=" + e + "&atencion=" + a);
            xmlhttp.send("");
            if (xmlhttp.readyState == 1) {
                objeto_recibidor.innerHTML = '</br>';

            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    objeto_recibidor.innerHTML = xmlhttp.responseText;

                }
                if (xmlhttp.status != 200) {
                    objeto_recibidor.innerHTML = 'Realizar Busqueda!';
                    //  document.getElementById('btn_buscar').disabled = false;
                }
            }
        }
    }




    function buscaPlanificacion()
    {

        var f2 = document.forms["planifica"]["fecha_inicio"].value;
        var e = document.forms["planifica"]["id_especialidad"].value;
        var doctor = document.forms["planifica"]["funcionario"].value;

        try
        {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e)
        {
            try
            {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
            xmlhttp = new XMLHttpRequest();
        }

        if (xmlhttp) {
            var objeto_recibidor = document.getElementById("datosCupos");
            xmlhttp.open("post", "cuposysobrecupos.jsp?fechabuscar=" + f2 + "&especialidad=" + e + "&doctor=" + doctor);
            xmlhttp.send("");
            if (xmlhttp.readyState == 1) {
                objeto_recibidor.innerHTML = '</br>';

            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    objeto_recibidor.innerHTML = xmlhttp.responseText;

                }
                if (xmlhttp.status != 200) {
                    objeto_recibidor.innerHTML = 'Realizar Busqueda!';
                    //  document.getElementById('btn_buscar').disabled = false;
                }
            }
        }
    }


    function validar() {

        var x = document.forms["planifica"]["funcionario"].value;
        var y = document.forms["planifica"]["horainicio"].value;
        var z = document.forms["planifica"]["horafin"].value;
        var a = document.forms["planifica"]["atencion"].value;
        var b = document.forms["planifica"]["programa"].value;
        var c = document.forms["planifica"]["numerocupo1"].value;
        var d = document.forms["planifica"]["duracion"].value;
        var e = document.forms["planifica"]["fecha_inicio"].value;
        var p = document.forms["planifica"]["prestacion"].value;
        
        var inicio = document.forms["planifica"]["horainiciopabellon"].value;
        var fin = document.forms["planifica"]["horafinpabellon"].value;
        var pabellon =  document.forms["planifica"]["pabellon"].value;

        if (x == null || x == "" || x == "-1" || p == null || p == "0" || p == "-1" || y == null || y == "" || z == null || z == "" || a == null || a == "" || a == "-1" || b == null || b == "" || b == "-1" || c == null || c == "" || c == 0 || d == null || d == "" || d == 0 || e == null || e == "")
        {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }
        if (a == 3 && (pabellon == "-1" || inicio== "" || fin == "" )){
            Alert.render("Complete los datos para tabla quirúrgica!! Debe Completar los Datos para Continuar");
            return false;
        }
        


    }





    function calcularcupos(duracion) {
        /*para limpiar los campos por si tengo unos cupos anteriores*/
        document.getElementById("numerocupo").innerText = 0;
        document.forms["planifica"]["numerocupo1"].value = 0;

        /*valido los cupos*/
        var inicio = document.forms["planifica"]["horainicio"].value;
        var fin = document.forms["planifica"]["horafin"].value;

        inicioMinutos = parseInt(inicio.substr(3, 2));
        inicioHoras = parseInt(inicio.substr(0, 2));

        finMinutos = parseInt(fin.substr(3, 2));
        finHoras = parseInt(fin.substr(0, 2));

        transcurridoMinutos = finMinutos - inicioMinutos;
        transcurridoHoras = finHoras - inicioHoras;

        if (transcurridoMinutos < 0) {
            transcurridoHoras--;
            transcurridoMinutos = 60 + transcurridoMinutos;
        }

        horas = transcurridoHoras.toString();
        minutos = transcurridoMinutos.toString();

        if (horas.length < 2) {
            horas = "0" + horas;
        }

        if (minutos.length < 2) {
            minutos = "0" + minutos;
        }



        // debo llevar todo a minutos y dividir por la duracion

        horaminutos = parseInt(horas) * 60;

        totalminutos = parseInt(horaminutos) + parseInt(minutos);

        dura = document.forms["planifica"]["duracion"].value;
        if (dura.length < 2) {
            dura = "0" + dura;
        }

        if (dura < 1) {
            Alert.render("Ingrese un numero Mayor !!Debe Completar los Datos para Continuar");
            return false;
            totalcupos = 0;
        } else
        {
            totalcupos = parseInt(totalminutos) / parseInt(dura);
        }
        /*no pueden ser negativos*/
        if (totalcupos < 0) {
            Alert.render("Debe ingresar Hora Fin mayor a Hora inicio; recuerde que el formato es de 24 horas !!Debe Completar los Datos para Continuar");
            totalcupos = 0;
            return false;

        }
        /*debe ser entero*/
        totalcupos = Math.trunc(totalcupos);

        document.getElementById("numerocupo").innerText = totalcupos;
        document.forms["planifica"]["numerocupo1"].value = totalcupos;
    }
    function justNumbers(e)
    {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
            return true;
        return /\d/.test(String.fromCharCode(keynum));
    }

</script>




<div class="container"> 
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">

                <form id="planifica" name="planifica" action="<%=g.getLocal()%>ingresar_planificacion" method="post" onsubmit=" return validar()" >
                    <fieldset>
                        <INPUT type="text" id="id_especialidad" name="id_especialidad" value="<%=especialidad%>" hidden> 
                        <legend class="text-center header">Planificar Cupos al Doctor en <%=e.getNombre()%> </legend>


                        <table class="center-block2" aling="center" >

                            <tr style=" align-content: center; margin-right: 100px;" >
                                <td class="text-center header" style=" font-size: 15px">Dia a Planificar:
                                    <input title="Debe ingresar la Fecha" type="text" value="<%=fecha%>" onkeypress="return buscaPlanificacion();" onchange="buscaPlanificacion()" maxlength="13" size="12" id="fecha_inicio" name="fecha_inicio" ><br>


                                <td>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center" ><i class="fa fa-user-md bigicon" ></i></span>

                                        <div class="col-md-8" >
                                            <select class="form-control" id="funcionario" name="funcionario" title="Doctor" onchange="buscaPlanificacion()">
                                                <option value="-1">Eligir Doctor
                                                    <%

                                                        for (doctor docto : cd.buscarDoctoresporEspecialidad(Integer.parseInt(especialidad))) {

                                                            if (docto.getRut().equals(rut)) {%>
                                                <option selected value="<%=docto.getRut()%>"><%=docto.getNombre()%><%
                                                } else {%>
                                                <option value="<%=docto.getRut()%>"><%=docto.getNombre()%><%
                                                        }

                                                    }
                                                    %>
                                            </select>
                                        </div> 
                                    </div>        

                                </td>
                            </tr>
                            <tr>



                                <td>

                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-clock-o bigicon"></i></span>


                                        <div class="col-md-8" > <i class="fa bigicon" style="font-size: 12px;">Inc:</i>
                                            <input style="padding-right: 20px; margin: -3px 10px -19px; width: 100px" type="time" id="horainicio" name="horainicio"  min="8:00" max="22:55" value="08:00" title="Hora Incio"  required>
                                            <i class="fa bigicon" style="font-size: 12px;"> - Fin:</i>

                                            <input style="padding-right: 10px; margin: 3px -5px -9px;width: 100px" type="time" name="horafin" id="horafin" value="20:00"  min="8:05" max="23:00"  title="Hora Fin"   required>

                                        </div>
                                    </div>
                                </td><td>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-hospital-o bigicon"></i></span>


                                        <div class="col-md-8" >
                                            <select class="form-control" id="atencion" name="atencion" title="Atencion" onchange="buscarprestacion();
                                                    ">
                                                <option value="-1">Elegir Atencion
                                                    <%
                                                        for (lugar atencion : g.buscarAtencionparaAgendarCupos()) {

                                                            if (atencion.getId_lugar() == idatencion) {%>
                                                <option selected value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                                                } else {%>
                                                <option value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                                                        }

                                                    }
                                                    %> 
                                            </select>
                                        </div>
                                    </div>
                                </td></tr>

                            <tr><td>
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list bigicon"></i></span>


                                        <div class="col-md-8" >
                                            <select class="form-control" id="programa" name="programa" title="Programa">
                                                <option value="-1">Elegir Programa
                                                    <%
                                                        for (lugar programa : g.buscarProgramaparaAgendarCupos()) {

                                                            if (programa.getId_lugar() == idatencion) {%>
                                                <option selected value="<%=programa.getId_lugar()%>"><%=programa.getNombre()%><%
                                                } else {%>
                                                <option value="<%=programa.getId_lugar()%>"><%=programa.getNombre()%><%
                                                        }

                                                    }
                                                    %> 
                                            </select>
                                        </div>
                                    </div>
                                </td>

                                <td colspan="2" >

                                    <span class="col-md-1 col-md-offset-2 "  ><i class="fa fa-clock-o bigicon" style="font-size: 15px;" >Min</i>

                                    </span>
                                    <div class="col-md-8" >

                                        <input style="padding-right: 15px; margin: -3px 20px -19px; width: 80px"  type="text" size="2" maxlength="2" id="duracion" name="duracion" onchange="return calcularcupos(this.value)"  onkeypress="return justNumbers(event);"  title="Duracion" placeholder="5 Min" required>

                                        <i class="fa bigicon" style="font-size: 15px;" >NºCupos</i>  <label name="numerocupo" id="numerocupo" ></label> 
                                        <input type="hidden" style="padding-right: 10px; margin: 3px -2px -9px;width: 60px" name="numerocupo1" id="numerocupo1" readonly="readonly" placeholder="Numero de cupos" required> 

                                    </div>
                                </td>



                            </tr>
                        </table>
                                            <table style=" margin-left: 50px">                      
                            <tr name="datosporcirugia" id="datosporcirugia" style=" width: 100%; display: none">
                               <td>
                                    <br>
                                </td> 
                                <td colspan="2">
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-list bigicon"></i></span>


                                        <div class="col-md-9" >
                                            <select class="form-control" id="pabellon" name="pabellon" title="pabellon">
                                                <option value="-1">Elegir Pabellon
                                                    <%
                                                        for (lugar pabellon : g.salasactivas()) {

                                                            if (pabellon.getId_lugar() == idatencion) {%>
                                                <option selected value="<%=pabellon.getId_lugar()%>"><%=pabellon.getNombre()%><%
                                                } else {%>
                                                <option value="<%=pabellon.getId_lugar()%>"><%=pabellon.getNombre()%><%
                                                        }

                                                    }
                                                    %> 
                                            </select>
                                        </div>
                                    </div> 
                                </td>
                                <td colspan="2">
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center "><i class="fa bigicon" style="font-size: 12px;">Inc Pabellon:</i></span>
                                        <div class="col-md-8" >   
                                            <input  type="time" style="margin-left: 41px;" id="horainiciopabellon" name="horainiciopabellon"  min="8:00" max="22:55" value="08:00" title="Hora Incio"  required>

                                        </div>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center "> <i class="fa bigicon" style="font-size: 12px;"> - Fin Pabellon:</i></span>
                                        <div class="col-md-8" > 
                                            <input  type="time" style="margin-left: 41px;" name="horafinpabellon" id="horafinpabellon" value="20:00"  min="8:05" max="23:00"  title="Hora Fin"   required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td  colspan="3">

                                    <span class="col-md-1 col-md-offset-1" style=" margin-left: 150px"  ><i class="fa fa-list bigicon" style="font-size: 20px;" >Prestación:</i> </span>

                                    <div class="col-md-8" style=" margin-left: 60px" id="pres" name="pres" >

                                        <select class="form-control" id="prestacion" name="prestacion" title="Prestacion">
                                            <option value="-1">Elegir Prestacion
                                                <%
                                                    for (procedimiento p : cc.buscarPrestaciones(Integer.parseInt(especialidad), tipo)) {

                                                %>
                                            <option value="<%=p.getId()%>"><%=p.getPrestacionesfonasa() + " " + p.getDescripcion()%><%
                                                }


                                                %> 
                                        </select>

                                    </div>
                                </td>
                            </tr>


                        </table>
                        <br>
                        <br>
                        <br>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary" >Planificar</button>

                                <button type="reset" class="btn btn-primary ">Cancelar</button>
                            </div>
                        </div>

                        <div id="datosCupos">
                            <p class="letra">¡ Realizar la Busqueda !</p>
                        </div>    



                    </fieldset>
                </form>
            </div></div>
    </div>
</div>
</body>
<script>
  
    var f3 = document.forms["planifica"]["fecha_inicio"].value;
    if (f3 != null || f3 != "") {
        buscaPlanificacion()
    }


</script>

<jsp:include page="../comunes/Footer.jsp" />