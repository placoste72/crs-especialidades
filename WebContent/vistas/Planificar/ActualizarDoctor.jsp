<%-- 
    Document   : ActualizarDoctor
    Created on : 05-12-2016, 03:11:10 PM
    Author     : Informatica
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="Controlador.General" %>
<%@page import="Modelos.especialidades" %>
<jsp:include page="../comunes/headerwindows.jsp"/> 
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <title>Actualiza Doctor</title>


        <script>
            //para elegir

            function pone() {

                if (!isSelected("#fromSelectBox")) {
                    return;
                }
                //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
                $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
                return false;

            }
            function saca() {

                //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
                if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
                    return;
                }
                //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
                $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

            }

            function todos() {
                selectAll('#fromSelectBox');
                pone();
            }

            function ninguno() {
                selectAll('#toSelectBox');
                saca();
            }

            function selecciona_todos() {
                selectAll('toSelectBox');
            }

            //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
            function isSelected(thisObj) {
                if (!$(thisObj + " option:selected").length) {

                    Alert.render("Debe Elejir una Opcion para Continuar");
                    return 0;
                }
                return 1;
            }

            //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
            function noOptions(thisObj) {
                if (!$(thisObj + " option").length) {
                    // alert("There are no options to select/move");
                    return 0;
                }
                return 1;
            }

            //Below function is to de-select all items if any of the item(s) are selected
            function clearAll(thisObj) {
                $('#' + thisObj).each(function () {
                    $(this).find('option:selected').removeAttr("selected");
                });
            }//function close

            //Below function is to select all items

            function selectAll(thisObj) {
                obj = document.getElementById(thisObj);
                for (var i = 0; i < obj.options.length; i++) {
                    obj.options[i].selected = true;
                }
            }

        </script>


    </head>
    <body>
        <%
            controlador_doctor cd = new controlador_doctor();
            controlador_especialidad ce = new controlador_especialidad();
            String id = request.getParameter("cod");
            doctor d = cd.traerdoctor(id);
            ArrayList lista_opciones = ce.buscarListaEspecialidadessinDoctor(id);
            Iterator it = lista_opciones.iterator();

            ArrayList lista_vacia = ce.buscarListaEspecialidadesconDoctor(id);
            Iterator it2 = lista_vacia.iterator();
            String mensaje = "";
            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Actualizar Doctor">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>


        <%
            }
        %>

        <form style="border: #619fd8 5px solid;" enctype='multipart/form-data' name="doctor" class="form-horizontal" id="doctor" action='<%=ce.getLocal()%>actualizardoctor' method="post" onsubmit="return selecciona_todos()"  >
            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div>
            <fieldset>
                <legend class="text-center header">Actualizar Profesional</legend>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-credit-card bigicon"></i></span>
                    <div class="col-md-8">

                        <input type="text" id="rutdoctor" name="rutdoctor" value="<%=d.getRut()%>" readonly="readonly" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Doctor">
                        <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                        <input value="d" name="txtDV" id="txtDV" type="hidden">
                    </div>
                </div>
                <div id="busca">
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                        <div class="col-md-8">
                            <input type="text" id="nombredoctor" name="nombredoctor" value="<%=d.getNombre()%>" class="form-control" placeholder="Nombre Doctor" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                        <div class="col-md-8">
                            <input type="text" id="apellidopdoctor" name="apellidopdoctor" value="<%=d.getApellido_paterno()%>" class="form-control" placeholder="Apellido Doctor" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                        <div class="col-md-8">
                            <input type="text" id="apellidomdoctor" name="apellidomdoctor" value="<%=d.getApellido_materno()%>" class="form-control" placeholder="Apellido Doctor" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span>
                        <div class="col-md-8">
                            <input type="tel" id="telefonodoctor" name="telefonodoctor" value="<%=d.getTelefono()%>" class="form-control" placeholder="+56 9 1111 1111" >
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
                        <div class="col-md-8">
                            <input type="email" id="emaildoctor" name="emaildoctor" value="<%=d.getEmail()%>"  class="form-control" placeholder="Email Doctor" >
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="letra" style="margin-left: 450px" >La Imagen deber ser de 150 X 150 P�xeles</p> 

                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon" style=" font-size: 14px">Firma <br> Digital</i></span>
                        <div class="col-md-9" >

                            <input  id="file-0a" name="file-0a" class="file"    type="file"  multiple data-min-file-count="1" >
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-xing bigicon" style=" font-size: 14px">Profesi�n</i></span>
                        <div class="col-md-8">
                            <input type="text" id="profesion" name="profesion" class="form-control" value="<%=d.getProfesion()%>" placeholder="Profesion Doctor" required oninvalid="setCustomValidity('El campo profesion es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                </div>
                <div  style=" padding-left: 240px">

                    <h4 class="letra2"> Eliga las especialidades del Doctor </h4>
                    <div class="col-md-8 text-center">
                        <table class="table">
                            <tr>
                                <td>
                                    <select  id="fromSelectBox" name="especialidades2"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px;  width: 260px ">

                                        <%

                                            while (it.hasNext()) {
                                                especialidades e = (especialidades) it.next();
                                                out.write("   <option value='" + e.getId_especialidad() + "' >" + e.getNombre() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>
                                <td style="width:5px">
                                    <span class='cell' >
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon " onclick="pone();
                                                return false"></i>
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca();
                                                return false"></i>

                                    </span>

                                </td>       
                                <td>
                                    <select  id="toSelectBox" name="especialidades"  size="10" multiple="multiple"  style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px ">

                                        <%
                                            while (it2.hasNext()) {
                                                especialidades e2 = (especialidades) it2.next();
                                                out.write("   <option value='" + e2.getId_especialidad() + "' >" + e2.getNombre() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>    

                            </tr> 

                        </table>


                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Actualizar </button>

                        <button type="reset" class="btn btn-primary btn-lg" onclick="javascript: window.close();">Cancelar</button>
                    </div>
                </div>


            </fieldset>

        </form>
        <script>
            $('#file-fr').fileinput({
                language: 'fr',
                uploadUrl: '#',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
            });
            $('#file-es').fileinput({
                language: 'es',
                uploadUrl: '#',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
            });
            $("#file-0").fileinput({
                'allowedFileExtensions': ['jpg', 'png', 'gif'],
            });
            $("#file-1").fileinput({
                uploadUrl: '#', // you must set a valid URL here else you will get an error
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                overwriteInitial: false,
                maxFileSize: 1000,
                maxFilesNum: 10,
                //allowedFileTypes: ['image', 'video', 'flash'],
                slugCallback: function (filename) {
                    return filename.replace('(', '_').replace(']', '_');
                }
            });
            
            $("#file-3").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any",
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
            });
            $("#file-4").fileinput({
                uploadExtraData: {kvId: '10'}
            });
            $(".btn-warning").on('click', function () {
                if ($('#file-4').attr('disabled')) {
                    $('#file-4').fileinput('enable');
                } else {
                    $('#file-4').fileinput('disable');
                }
            });
            $(".btn-info").on('click', function () {
                $('#file-4').fileinput('refresh', {previewClass: 'bg-info'});
            });
           
            $(document).ready(function () {
                $("#test-upload").fileinput({
                    'showPreview': false,
                    'allowedFileExtensions': ['jpg', 'png', 'gif'],
                    'elErrorContainer': '#errorBlock'
                });
              
            });
        </script>
    </body>


</html>
