<%-- 
    Document   : DuplicarCupo
    Created on : 19-10-2016, 04:12:42 PM
    Author     : Informatica
--%>

<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="Controlador.General" %>
<%@ page import="java.sql.*,java.net.URL,java.util.Date,java.util.GregorianCalendar,java.util.Vector,java.text.DateFormat,java.util.Locale,java.util.Calendar" %>

<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>


<style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
<script type="text/javascript" src="../../public/js/calendar.js"></script>
<script type="text/javascript" src="../../public/js/calendar-es.js"></script>

<script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
<link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
<link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
<link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/crs.js" type="text/javascript"></script>
<style>
    nav1 {
        float: right;
        max-width: 550px;
        margin: 0px;
        padding: 1em;
        margin-top: 10px;
    }

    article{

        border-left: 1px solid #bce8f1;
        padding: 1em;
        overflow: hidden;
        margin-top: 0px;
        height: 900px;
        overflow: scroll;
    }


</style>

<%   
    controlador_doctor contrad = new controlador_doctor();
    controlador_especialidad ce = new controlador_especialidad();
    String rut = "";
    boolean error;

    General g = new General();
    String traef = "";
    if (request.getParameter("doctor") != null) {
        rut = request.getParameter("doctor");
        /*trato de buscar*/
%>
<script>
    buscaCupos()
</script>

<%
    }
Calendar fe = Calendar.getInstance();
    if (request.getParameter("fecha_inicio2") != null) {
        traef = request.getParameter("fecha_inicio2");
    }else {

        int a�o = fe.get(Calendar.YEAR);
        int mes = fe.get(Calendar.MONTH) + 1;
        int dia = fe.get(Calendar.DAY_OF_MONTH);

        String cad_valor = String.valueOf(dia);

        if (cad_valor.length() < 2) {
            cad_valor = "0" + cad_valor;
        }

        String cad_mes = String.valueOf(mes);

        if (cad_mes.length() < 2) {
            cad_mes = "0" + cad_mes;
        }

        traef = cad_valor + "/" + cad_mes + "/" + a�o;

    }
    String e = "";
    int es;
    String mensaje = "Debe completar los Datos";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>

<div id="dialog-message"  title="Duplicar Cupos">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>


    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>



<%
    }
    if (request.getParameter("especialidad") == null || request.getParameter("especialidad") == "") {
        es = 0;
    } else {
        e = request.getParameter("especialidad");
        es = Integer.parseInt(e);
    }

    Calendar c1 = Calendar.getInstance();
    //cargar arreglos
    String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
    String dias[] = {"Lu", "Ma", "Mi", "Ju", "Vi", "S�", "Do"};
    String anno;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
    anno = Integer.toString(c1.get(Calendar.YEAR));
    //manejo de a�os
    int anoactual = 2016;
    int finanno = Integer.parseInt(anno) + 10;
    Calendar calFin = Calendar.getInstance();
    Calendar calIncio = Calendar.getInstance();
    ArrayList fechas;
    int dia_inicio;
    String ann1 = " ";
    String m1 = " ";
    int m = 0;
    // fecha actual
    Calendar fechaActual = Calendar.getInstance();
    if (request.getParameter("mes") != null) {

        m1 = request.getParameter("mes");
    } else {
        m1 = Integer.toString(fechaActual.get(Calendar.MONTH));
    }

    if (request.getParameter("anno") != null) {
        ann1 = request.getParameter("anno");
    } else {
        ann1 = Integer.toString(fechaActual.get(Calendar.YEAR));
    }

    //creacion de agenda
    calIncio.set(Integer.parseInt(ann1), Integer.parseInt(m1), 1);
    int ultimo = calIncio.getActualMaximum(Calendar.DAY_OF_MONTH);
    calFin.set(Integer.parseInt(ann1), Integer.parseInt(m1), calFin.getActualMaximum(Calendar.DAY_OF_MONTH));
    Date DD = calIncio.getTime();
    Date D2 = calFin.getTime();
    fechas = g.generaAgenda(DD, ultimo);
    dia_inicio = DD.getDay();

    if (dia_inicio == 0) {
        dia_inicio = 6;
    } else {
        dia_inicio = dia_inicio - 1;
    }
//intero las fechas
    Iterator it = fechas.iterator();
%>








<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>
<div class="container"> 

    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">

                <form id="duplicarcupos" name="duplicarcupos" action='<%=g.getLocal()%>duplicar_planificacion' onsubmit="return validar()" >

                    <fieldset>
                        <legend class="text-center header">Duplicar Planificacion Cupos del Profesional</legend>							
                        <div class="row">
                            <nav1>
                                <legend class="text-center header" style=" font-size: 22px">Fecha donde se copiar�n los cupos:</legend>
                                <div class="form-group">   

                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar bigicon"></i></span>
                                    <div class="col-md-4" >
                                        <select class="form-control" id="mes" name="mes" onchange="javascript: document.duplicarcupos.action = 'DuplicarCupo.jsp';
                                                document.duplicarcupos.submit();">

                                            <%   for (int i = 0; i < meses.length; ++i) {

                                                    if (i == Integer.parseInt(m1)) {%>
                                            <option selected value="<%=i%>"><%=meses[i]%>
                                                <%
                                                } else {%>


                                            <option value="<%=i%>"><%=meses[i]%>

                                                <%
                                                        }
                                                    }


                                                %>
                                        </select>
                                    </div>
                                    <div class="col-md-4" >
                                        <select class="form-control" id="anno" name="anno" onchange="javascript: document.duplicarcupos.action = 'DuplicarCupo.jsp';
                                                document.duplicarcupos.submit();">

                                            <% while (anoactual <= finanno) {

                                                    if (anoactual == Integer.parseInt(ann1)) {%>
                                            <option selected value="<%=anoactual%>"><%=anoactual%><%
                                            } else {%>
                                            <option value="<%=anoactual%>"><%=anoactual%>
                                                <%}
                                                        ++anoactual;
                                                    }%>
                                        </select>

                                    </div>
                                    <div class="form-group">

                                        <table class="table table-striped" style=" width: 100%">
                                            <thead>
                                                <tr>
                                                    <%for (int i = 0; i < dias.length; ++i) {%>
                                                    <th>

                                                        <%=dias[i]%>
                                                    </th>
                                                    <%}%>
                                                </tr>
                                            </thead>
                                            <%
                                                boolean pasa;
                                                pasa = false;
                                                for (int column = 0; column < 6; ++column) {
                                                    int filas = 0;

                                            %>
                                            <tr style="padding: 5px;">

                                                <%  while ((filas < 7) && (it.hasNext())) {

                                                        if ((filas == dia_inicio) || (pasa == true)) {
                                                            Date d = (Date) it.next();
                                                            String dia = Integer.toString(d.getDate()) + "/" + Integer.toString(d.getMonth() + 1) + "/" + Integer.toString(d.getYear() + 1900);

                                                %>
                                                <td  style="background-color:#ccffcc;padding: 5px;" > <input type="checkbox"  name="chk<%=d.getDate()%>" id="chk<%=d.getDate()%>"  value="checkbox" /><%=d.getDate()%> </td>
                                                    <%

                                                        pasa = true;
                                                    } else {
                                                    %>
                                                <td style="padding: 5px;"> .</td>
                                                <%
                                                        }
                                                        ++filas;
                                                    }%>
                                            </tr>
                                            <%}%>
                                        </table>


                                    </div>      
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary ">Duplicar</button>

                                            <button type="reset" class="btn btn-primary">Cancelar</button>
                                        </div>
                                    </div>
                                </div>    
                            </nav1>
                            <article>
                                <legend class="text-center header" style=" font-size: 22px">Cupos Planificados</legend>
                                <table class="center-block2 " >
                                    <tr style="text-align: center;">
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list bigicon"></i></span>


                                        <div class="col-md-8" > 
                                            <select class="form-control" id="especialidad" title="Especialidad" name="especialidad" onchange="javascript: document.duplicarcupos.action = 'DuplicarCupo.jsp';
                                                    document.duplicarcupos.submit();
                                                    "> 
                                                <option value ="-1">Seleccione Especialidad
                                                    <%

                                                        for (especialidades espe : ce.buscarEspecialdadTodas()) {

                                                            if (es == espe.getId_especialidad()) {%>
                                                <option selected value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                } else {%>
                                                <option value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                        }

                                                    }
                                                    %>
                                            </select>


                                        </div>
                                    </div>

                                    </td>
                                    </tr>
                                    <tr>

                                        <td class="letra"  >Fecha planificado: <input type="text" maxlength="13" size="12" id="fecha_inicio2" name="fecha_inicio2" value="<%=traef%>" ><br>
                                        <td>  
                                            <div class="form-group">
                                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>


                                                <div class="col-md-8" > 
                                                    <select class="form-control" id="funcionario" name="funcionario" title="Doctor">
                                                         <option value="-1">Profesional
                                                        <%

                                                            for (doctor doc : contrad.buscarDoctoresporEspecialidad(es)) {

                                                                if (doc.getRut().equals(rut)) {%>
                                                        <option selected value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                        } else {%>
                                                        <option value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                                }

                                                            }
                                                            %>
                                                    </select>
                                                </div> 
                                            </div>
                                        </td>
                                        <td>
                                            <button id="buscar" value="buscar" type="button" class="btn btn-primary" onclick="javascript : buscaCupos()">Buscar</button>
                                        </td>
                                    <tr>
                                </table>
                                <div id="busqueda">            
                                    <p class="letra">� Realizar la Busqueda !</p>
                                </div>
                            </article>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

    $(function () {
        $("#fecha_inicio").datepicker();
    });
    $(function () {
        $("#fecha_inicio2").datepicker();
    });
    var entre = false;
    function buscaCupos()
    {
        var fecha = document.forms["duplicarcupos"]["fecha_inicio2"].value;
        var funcionario = document.forms["duplicarcupos"]["funcionario"].value;
        var esp = document.forms["duplicarcupos"]["especialidad"].value;
        if (fecha == null || fecha == "" || funcionario == null || fecha == "" || esp == null || esp == "" || esp == 0) {
            Alert.render("Error debe completar los datos");

        } else
        {
            entre = true;
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("busqueda");
                xmlhttp.open("post", "buscarCupos.jsp?fecha=" + fecha + "&funcionario=" + funcionario + "&especialidad=" + esp);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br><b>Favor espere... </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;

                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }
        }
    }

    function validar() {
        var m = document.forms["duplicarcupos"]["fecha_inicio"].value;
        var a = document.forms["duplicarcupos"]["fecha_inicio2"].value;
        if (m == null || m == "" || a == null || a == "" || entre == false) {

            Alert.render("Error debe completar los datos");
        }
    }


</script>

</body>

<jsp:include page="../comunes/Footer.jsp" />

