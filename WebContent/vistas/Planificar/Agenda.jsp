<%-- 
    Document   : Agenda
    Created on : 06-12-2016, 11:02:37 AM
    Author     : Informatica
--%>

<%@page import="Modelos.planificar"%>
<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="Controlador.General"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../comunes/Header.jsp"/>

<!DOCTYPE html>
<title>Agenda Mensual</title>
<%    controlador_doctor cd = new controlador_doctor();
    controlador_especialidad ce = new controlador_especialidad();
    String doc = "-1";
    String nombre = "Todas las agendas";
    if (request.getParameter("funcionario") != null) {
        doc = request.getParameter("funcionario");

    }

    int es = -1;

    if (request.getParameter("especialidad") != null) {
        es = Integer.parseInt(request.getParameter("especialidad"));
        especialidades espe = ce.buscarEspecialidadCodigo(es);
        if (espe != null) {
            nombre = "Agenda de " + espe.getNombre();
        }

    }
    General g = new General();
    Calendar c1 = Calendar.getInstance();
    //cargar arreglos
    String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
    String dias[] = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
    String anno;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
    anno = Integer.toString(c1.get(Calendar.YEAR));
    //manejo de años
    int anoactual = 2016;
    int finanno = Integer.parseInt(anno) + 10;
    Calendar calFin = Calendar.getInstance();
    Calendar calIncio = Calendar.getInstance();
    ArrayList fechas;
    int dia_inicio;
    String ann1 = " ";
    String m1 = " ";
    int m = 0;
    // fecha actual
    Calendar fechaActual = Calendar.getInstance();
    if (request.getParameter("mes") != null) {

        m1 = request.getParameter("mes");
    } else {
        m1 = Integer.toString(fechaActual.get(Calendar.MONTH));
    }

    if (request.getParameter("anno") != null) {
        ann1 = request.getParameter("anno");
    } else {
        ann1 = Integer.toString(fechaActual.get(Calendar.YEAR));
    }
    //creacion de agenda
    calIncio.set(Integer.parseInt(ann1), Integer.parseInt(m1), 1);
    int ultimo = calIncio.getActualMaximum(Calendar.DAY_OF_MONTH);
    calFin.set(Integer.parseInt(ann1), Integer.parseInt(m1), calFin.getActualMaximum(Calendar.DAY_OF_MONTH));
    Date DD = calIncio.getTime();
    Date D2 = calFin.getTime();
    fechas = g.generaAgenda(DD, ultimo);
    dia_inicio = DD.getDay();

    if (dia_inicio == 0) {
        dia_inicio = 6;
    } else {
        dia_inicio = dia_inicio - 1;
    }
//intero las fechas
    Iterator it = fechas.iterator();
%>

    <div class="container">

        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <form id="agenda" name="agenda" action="Agenda.jsp" > 
            <fieldset>
                <legend class="text-center header"> Planificación Agenda Mensual </legend>
                 <h3 class="text-center header"> Informacion Cupos (Utilizados/Total Cupos)</h3>
                <h3 class="text-center header"> <%=nombre%> </h3>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list bigicon"></i></span> 


                    <div class="col-md-2" > 
                        <select class="form-control" id="especialidad" name="especialidad" title="Especialidad"  onchange="javascript: document.getElementById('agenda').submit()">  
                            <option value="-1">Todas 
                                <%
                                    for (especialidades esp : ce.buscarEspecialdadTodas()) {

                                        if (es == esp.getId_especialidad()) {%>
                            <option selected value="<%=esp.getId_especialidad()%>"><%=esp.getNombre()%><%
                            } else {%>
                            <option value="<%=esp.getId_especialidad()%>"><%=esp.getNombre()%><%
                                    }

                                }
                                %>
                        </select>


                    </div>
                    <span class="col-md-1 col-md-offset-1 " ><i class="fa fa-user-md bigicon" ></i></span>

                    <div class="col-md-4" >
                        <select class="form-control" id="funcionario" name="funcionario" title="Profesional" onchange="javascript: document.getElementById('agenda').submit()">
                            <option value="-1">Profesional
                                <%              for (doctor doctores : cd.buscarDoctoresporEspecialidad(es)) {

                                        if (doc.equals(doctores.getRut())) {%>
                            <option selected value="<%=doctores.getRut()%>"><%=doctores.getNombre()%><%
                            } else {%>
                            <option value="<%=doctores.getRut()%>"><%=doctores.getNombre()%><%
                                    }

                                }
                                %>
                        </select>
                    </div> 
                </div> 
                <br>
                <br>
                <div class="form-group">   

                    <span class="col-md-1 col-md-offset-3 text-center"><i class="fa fa-calendar bigicon"></i></span>
                    <div class="col-md-2" >
                        <select class="form-control" id="mes" name="mes" onchange="javascript: document.getElementById('agenda').submit();">

                            <%   for (int i = 0; i < meses.length; ++i) {

                                    if (i == Integer.parseInt(m1)) {%>
                            <option selected value="<%=i%>"><%=meses[i]%>
                                <%
                                } else {%>


                            <option value="<%=i%>"><%=meses[i]%>

                                <%
                                        }
                                    }


                                %>
                        </select>
                    </div>
                    <div class="col-md-2" >
                        <select class="form-control" id="anno" name="anno" onchange="javascript: document.getElementById('agenda').submit();">

                            <% while (anoactual <= finanno) {

                                    if (anoactual == Integer.parseInt(ann1)) {%>
                            <option selected value="<%=anoactual%>"><%=anoactual%><%
                            } else {%>
                            <option value="<%=anoactual%>"><%=anoactual%>
                                <%}
                                        ++anoactual;
                                    }%>
                        </select>

                    </div>

                </div>
                <br>     
                <div class="form-group">

                    <table class="table table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <%for (int i = 0; i < dias.length; ++i) {%>
                                <th>

                                    <%=dias[i]%>
                                </th>
                                <%}%>
                            </tr>
                        </thead>
                        <%
                            boolean pasa;
                            pasa = false;
                            for (int column = 0; column < 6; ++column) {
                                int filas = 0;

                        %>
                        <tr style="padding: 27px;">

                            <%  while ((filas < 7) && (it.hasNext())) {

                                    if ((filas == dia_inicio) || (pasa == true)) {
                                        Date d = (Date) it.next();
                                        String cad_valor3 = String.valueOf(d.getDate());
                                        String cad_valor4 = String.valueOf(d.getMonth() + 1);
                                        if (cad_valor3.length() < 2) {
                                            cad_valor3 = "0" + cad_valor3;
                                        }

                                        if (cad_valor4.length() < 2) {
                                            cad_valor4 = "0" + cad_valor4;
                                        }
                                        String dia = cad_valor3 + "/" + cad_valor4 + "/" + Integer.toString(d.getYear() + 1900);
                                        boolean tienealgo = g.buscarPlanificacion(d, es, doc);
                                        if (tienealgo == true) {
                                            String informacion="";
                                              for(planificar p : g.buscarsolocuposparaAgendaMensual(d, es, doc)){
                                                 informacion = informacion +" "+ p.getRut_doctor() + ":("+p.getCupos_disponibles()+"/"+p.getNumero_cupo()+") \n";
                                              
                                              }
                                          %>
                                           
                                        <td  style="background-color:#ffcccc; padding: 27px; font-weight: bold; font-size: 12px; " onclick="javascript: window.open('PlanificacionDelDia.jsp?especialidad=<%=es%>&fecha=<%=d%>&doctor=<%=doc%>', 'Detalle Agenda', 'width=1900,height=600')" > <%=d.getDate() +"<br>"+informacion%></td> 
                                               
                                       <%
                                        
                                        } else {
                                        %>
                                        <td  style="background-color:#ccffcc;padding: 27px; font-weight: bold; font-size: 12px;" > <%=d.getDate()%> </td>
                                        <%

                                            }
                                            pasa = true;
                                        } else {
                                        %>
                                        <td style="padding: 27px;"> .</td>
                                        <%
                                    }
                                    ++filas;
                                }%>
                        </tr>
                        <%}%>
                    </table>


                </div>      
            </fieldset>
        </form>
    </div>



    <jsp:include page="../comunes/Footer.jsp"/>