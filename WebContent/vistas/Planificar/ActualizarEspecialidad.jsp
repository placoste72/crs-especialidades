<%-- 
    Document   : RegistrarEspecialidad
    Created on : 18-10-2016, 09:19:51 AM
    Author     : Informatica
--%>

<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.General" %>
<%@page import="Modelos.especialidades" %>
<jsp:include page="../comunes/headerwindows.jsp"/> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    </head>
    <body>

        <%
            String mensaje = "";
            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Actualizar Especialidad">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>
        <%
            }
            controlador_especialidad ce = new controlador_especialidad();
            especialidades e = new especialidades();
            int codigo = Integer.parseInt(request.getParameter("cod"));
            e = ce.buscarEspecialidadCodigo(codigo);
            session.setAttribute("codigoespecialidad", request.getParameter("cod"));
        %>

        <form enctype='multipart/form-data' style="border: #619fd8 5px solid;" action='ModificarEspecialidad.jsp' method="post" >
            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div>

            <fieldset>
                <legend class="text-center header">Actualizar Especialidad</legend>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                    <div class="col-md-8">
                        <input id="codigo" name="codigo" type="text" value="<%=codigo%>" hidden>
                        <input id="nombre" name="nombre"  type="text" placeholder="Nombre Especialidad" value="<%=e.getNombre()%>" class="form-control">
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-md-8">
                        <br>
                        <input id="file-0a" name="file-0a" class="file"  type="file" multiple data-min-file-count="1">
                    </div>

                </div>


                <br>
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Actualizar</button>
                        <button type="reset" class="btn btn-primary btn-lg" onclick="javascript: window.close();">Cancelar</button>
                    </div>
                </div>



            </fieldset>
        </form>







        <script>
            $('#file-fr').fileinput({
                language: 'fr',
                uploadUrl: '#',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
            });
            $('#file-es').fileinput({
                language: 'es',
                uploadUrl: '#',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
            });
            $("#file-0").fileinput({
                'allowedFileExtensions': ['jpg', 'png', 'gif'],
            });
            $("#file-1").fileinput({
                uploadUrl: '#', // you must set a valid URL here else you will get an error
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                overwriteInitial: false,
                maxFileSize: 1000,
                maxFilesNum: 10,
                //allowedFileTypes: ['image', 'video', 'flash'],
                slugCallback: function (filename) {
                    return filename.replace('(', '_').replace(']', '_');
                }
            });
            /*
             $(".file").on('fileselect', function(event, n, l) {
             alert('File Selected. Name: ' + l + ', Num: ' + n);
             });
             */
            $("#file-3").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any",
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
            });
            $("#file-4").fileinput({
                uploadExtraData: {kvId: '10'}
            });
            $(".btn-warning").on('click', function () {
                if ($('#file-4').attr('disabled')) {
                    $('#file-4').fileinput('enable');
                } else {
                    $('#file-4').fileinput('disable');
                }
            });
            $(".btn-info").on('click', function () {
                $('#file-4').fileinput('refresh', {previewClass: 'bg-info'});
            });
            /*
             $('#file-4').on('fileselectnone', function() {
             alert('Huh! You selected no files.');
             });
             $('#file-4').on('filebrowse', function() {
             alert('File browse clicked for #file-4');
             });
             */
            $(document).ready(function () {
                $("#test-upload").fileinput({
                    'showPreview': false,
                    'allowedFileExtensions': ['jpg', 'png', 'gif'],
                    'elErrorContainer': '#errorBlock'
                });
                /*
                 $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
                 alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
                 });
                 */
            });
        </script>
    </body>

</html>