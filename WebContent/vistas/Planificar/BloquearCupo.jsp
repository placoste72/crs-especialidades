<%@page import="Controlador.controlador_doctor"%>
<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.especialidades"%>

.<%-- 
    Document   : DuplicarCupo
    Created on : 19-10-2016, 04:12:42 PM
    Author     : Informatica
--%>


<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<%@page import="Controlador.General" %>
<%@ page import="java.sql.*,java.net.URL,java.util.Date,java.util.GregorianCalendar,java.util.Vector,java.text.DateFormat,java.util.Locale,java.util.Calendar" %>

<style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
<script type="text/javascript" src="../../public/js/calendar.js"></script>
<script type="text/javascript" src="../../public/js/calendar-es.js"></script>
<script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
<link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
<link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
<title>Bloqueo de Cupos</title>

<%    String rut = "";
    String traef = "";
    Calendar fe = Calendar.getInstance();
    controlador_doctor contrad = new controlador_doctor();
    controlador_especialidad ce = new controlador_especialidad();
    if (request.getParameter("funcionario") != null) {
        rut = request.getParameter("funcionario");
    }
    if (request.getParameter("fecha_inicio") != null) {
        traef = request.getParameter("fecha_inicio");
    } else {

        int a�o = fe.get(Calendar.YEAR);
        int mes = fe.get(Calendar.MONTH) + 1;
        int dia = fe.get(Calendar.DAY_OF_MONTH);

        String cad_valor = String.valueOf(dia);

        if (cad_valor.length() < 2) {
            cad_valor = "0" + cad_valor;
        }

        String cad_mes = String.valueOf(mes);

        if (cad_mes.length() < 2) {
            cad_mes = "0" + cad_mes;
        }

        traef = cad_valor + "/" + cad_mes + "/" + a�o;

    }

    General g = new General();
    String e = "";
    int es;

    if (request.getParameter("especialidad") == null || request.getParameter("especialidad") == "") {
        es = 0;
    } else {
        e = request.getParameter("especialidad");
        es = Integer.parseInt(e);
    }
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>

<div id="dialog-message" title="Bloquear Cupos">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>



<%
    }


%>
<style>
    nav1 {
        float: right;
        max-width: 550px;
        margin: 0px;
        padding: 1em;
        margin-top: 10px;
    }
    article {

        border-left: 1px solid #bce8f1;
        padding: 1em;
        overflow: hidden;
        margin-top: 0px;
        height: 900px;
        overflow: scroll;
    }



</style>




<body>
    <div class="container"> 
        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">

                    <form id="bloqueocupos" name="bloqueocupos" method="post" action='<%=g.getLocal()%>bloquear_planificacion' onsubmit="return validar()"> 

                        <legend class="text-center header">Bloquear Planificacion del Profesional</legend>								
                        <div class="row">
                            <nav1>
                                <legend class="text-center header" style=" font-size: 22px">Colocar Motivo:</legend>
                                <table class="center-block " >
                                    <td class="letra" > <textarea lang="es" name="motivo" id="motivo" rows="5" cols="30" placeholder="Colocar el Motivo" oninvalid="setCustomValidity('El campo motivo es obligatorio')" 
                                                                  oninput="setCustomValidity('')" required></textarea><br>

                                </table>
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary" id="lock">Bloquear</button>

                                        <button type="reset" class="btn btn-primary ">Cancelar</button>
                                        <a class="btn btn-primary"  onClick="listarCupos()">Lista de cupos</a>
                                    </div>
                                </div>
                            </nav1>
                            <article>
                                <legend class="text-center header" style=" font-size: 22px">Cupos Planificados</legend>
                                <table class="center-block2 " style="margin: 10 auto; text-align: center;" >
                                    <tr style="text-align: center;">
                                    <div class="form-group">
                                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list bigicon"></i></span>


                                        <div class="col-md-8" > 
                                            <select class="form-control" id="especialidad" name="especialidad" title="Especialidad" onchange="javascript: document.bloqueocupos.action = 'BloquearCupo.jsp';
                                                        document.bloqueocupos.submit();
                                                    ">  
                                                <option> Seleccione Especialidad
                                                    <%

                                                            for (especialidades espe : ce.buscarEspecialdadTodas()) {

                                                                if (es == espe.getId_especialidad()) {%>
                                                <option selected value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                    } else {%>
                                                <option value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                        }

                                                    }
                                                    %>
                                            </select>


                                        </div>
                                    </div>     

                                    </td>
                                    </tr>
                                    <tr>
                                        <td class="letra" >Fecha planificado: <input type="text" maxlength="13" size="12" id="fecha_inicio" name="fecha_inicio" value="<%=traef%>" ><br>

                                        <td>
                                            <div class="form-group">
                                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>


                                                <div class="col-md-8" > 
                                                    <select class="form-control" id="funcionario" name="funcionario" title="Doctor">
                                                        <option value="-1">Profesional
                                                            <%for (doctor doc : contrad.buscarDoctoresporEspecialidad(es)) {

                                                                    if (doc.getRut().equals(rut)) {%>
                                                        <option selected value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                        } else {%>
                                                        <option value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                                }

                                                            }
                                                            %>
                                                    </select>



                                                </div> 
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary" onclick="javascript : buscaCupos()">Buscar</button>
                                        </td>
                                    </tr>
                                </table>
                                <div id="busqueda">            
                                    <p class="letra">� Realizar la Busqueda !</p>
                                </div>
                            </article>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
<script>


    $(function () {
        $("#fecha_inicio").datepicker();
    });

    var entre = false;
    function buscaCupos()
    {
        var fecha = document.forms["bloqueocupos"]["fecha_inicio"].value;
        var funcionario = document.forms["bloqueocupos"]["funcionario"].value;
        var esp = document.forms["bloqueocupos"]["especialidad"].value;
        if (fecha == null || fecha == "" || funcionario == null || funcionario == "" || esp == null || esp == "" || esp == 0)
        {

            Alert.render("Debe Completar los Datos para Continuar");
        } else
        {
            entre = true;
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("busqueda");
                xmlhttp.open("post", "buscarCupos.jsp?fecha=" + fecha + "&funcionario=" + funcionario + "&especialidad=" + esp);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br><b>Favor espere... </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;

                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                    
                }
             

            }
           
        }
    }
    /*
     * @author Ricardo Ramirez
     * @since 30-07-2019
     * @version 1.0.0
     * 
     * Funci�n que obtine las citas de los cupos
     */
    function listarCupos(){
        var fecha = document.forms["bloqueocupos"]["fecha_inicio"].value;
        var funcionario = document.forms["bloqueocupos"]["funcionario"].value;
        var esp = document.forms["bloqueocupos"]["especialidad"].value;
        
        if (fecha == null || fecha == "" || funcionario == null || funcionario == "" || esp == null || esp == "" || esp == 0)
        {

            Alert.render("Debe Completar los Datos para Continuar");
        }else{
            window.open('<%=g.getLocallink()%>Planificar/ListadoCupos.jsp?fecha='+fecha+'&especialidad='+esp+'&funcionario='+funcionario+'', 'Lista de Cupos con paciente', 'width=1200, height=1000');
        }
    }

    function validar() {
        var m = document.forms["bloqueocupos"]["fecha_inicio"].value;
        var motivo = document.forms["bloqueocupos"]["motivo"].value;
        if (m == null || m == "" || motivo == null || motivo == "" || entre == false) {


            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }


    }

</script>
</body>

<jsp:include page="../comunes/Footer.jsp" />

