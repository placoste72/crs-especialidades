<%-- 
    Document   : ListadoCupos
    Created on : 17-07-2019, 10:43:47
    Author     : Camilo Mendez
--%>

<%@page import="Modelos.oferta"%>
<%@page import="Controlador.controlador_cupos"%>
<%@page import="Modelos.paciente"%>
<%@page import="Modelos.doctor"%>

<!DOCTYPE html>
<%@page import="java.lang.String"%>
<%@page import="Controlador.General" %>
<%@page import="java.util.Date"%>

<%  String doctor = request.getParameter("funcionario");
    String fecha = request.getParameter("fecha");
    String a = fecha.substring(6);
    String d = fecha.substring(0, 2);
    String m = fecha.substring(3, 5);

    String fecha2 = m + "/" + d + "/" + a;
    String es = request.getParameter("especialidad");
    Date fecha1 = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);

    System.out.print(fecha1);

    controlador_cupos cc = new controlador_cupos();
%>

<jsp:include page="../comunes/headerwindows.jsp"/>
<input type="text" name="doctor1" id="doctor1" value="<%=fecha%>" hidden>
<input type="text" name="doctor" id="doctor" value="<%=doctor%>" hidden>
<input type="text" name="f" id="f" value="<%=fecha2%>" hidden>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listado de Paciente con cupos</title>
    </head>
    <div class="visible" id="printDiv">
        <body>
        <legend class="text-center header">Lista de Pacientes con cupos</legend>
        <table class="table table-striped" style="width: 100%" id="listcupos">
            <thead>
                <tr>
                    <th>Paciente</th>           
                    <th>Doctor</th>
                    <th>Especialidad</th>
                    <th>Dia</th>
                    <th>Hora de inicio</th>
                    <th>Hora de termino</th>
                </tr>
            </thead>
            <%

                for (oferta ofer : cc.listarcupos(fecha1, Integer.parseInt(es), doctor)) {
            %>
            <tr>
                <td><%=ofer.getTemporal2()%></td>
                <td><%=ofer.getTemporal()%></td>
                <td><%=ofer.getTemporal1()%></td>
                <td><%=ofer.getTemporal3()%></td>
                <td><%=ofer.getTemporal4()%></td>
                <td><%=ofer.getTemporal5()%></td>
            </tr>

            <%}%>
        </table>
    </div>
       
    <!--
    * @author Juan Fernández
    * @since 15-08-2019
    * @version 1.0.0
    * 
    * Función que imprime el listado de Paciente con cupos
    *
    -->
    <button class="btn btn-primary" id="imprelistacupos">Imprimir Lista</button>
    <script>
        document.getElementById("imprelistacupos").addEventListener("click", imprimir);
        function imprimir() {
            var Contaimp;
            var Contorig;
            Contaimp = document.getElementById("printDiv").innerHTML;
            Contorig = document.body.innerHTML;
            document.body.innerHTML = Contaimp;
            window.print();
            document.body.innerHTML = Contorig;
        };
    </script>
</body>
</body>
</html>
