<%-- 
    Document   : cuposysobrecupos
    Created on : 07-11-2016, 11:07:37 AM
    Author     : Informatica
--%>

<%@page import="Modelos.oferta"%>
<%@page import="Controlador.controlador_cupos"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.Date"%>




<%
    controlador_cupos cc = new controlador_cupos();
    String fecha = "";
    String e = "";
    String fecha2 = "";
    String a = "";
    String d = "";
    String m = "";
    String doctor = "";

    fecha = request.getParameter("fechabuscar");
    a = fecha.substring(6);
    d = fecha.substring(0, 2);
    m = fecha.substring(3, 5);
    Date fechaComparar = new Date(Integer.parseInt(a) - 1900, Integer.parseInt(m) - 1, Integer.parseInt(d));

    fecha2 = m + "/" + d + "/" + a;
    Date fecha1 = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);

    e = request.getParameter("especialidad");
    doctor = request.getParameter("doctor");


%>


<form>

    <table class="table table-striped" style="width: 100%" >
        <thead>
            <tr><th><strong>Doctor</strong></th>
                <th><strong>Fecha</strong></th>
                <th><strong>Hora Inicio</strong></th>
                <th><strong>Hora Fin</strong></th>
                <th><strong>Atención</strong></th>
                <th><strong>Programa</strong></th>
                <th><strong>Duración</strong></th>
                <th><strong>Cupos</strong></th>
                <th><strong>Sobrecupos</strong></th>
                <th><strong>Bloqueados</strong></th>
                <th><strong>Prestación Planificada</strong></th>
                <th><strong>Eliminar</strong></th></tr>
        </thead>
        <%  for (oferta ofer : cc.buscarcuposysobrecupos(fecha1, Integer.parseInt(e), doctor)) {

        %> 
        <tr>
            <td><%=ofer.getTemporal()%></td> 
            <td><%=ofer.getTemporal1()%></td>
            <td><%=ofer.getTemporal2()%></td>
            <td><%=ofer.getTemporal3()%></td>
            <td><%=ofer.getHora()%></td>
            <td><%=ofer.getHorapabellon()%></td>
            <td><%=ofer.getEstatus()%></td>
            <td><%=ofer.getProcede()%></td>

            <td><%=ofer.getId_plani_sobre()%></td>
            <td><%=ofer.getCupos()%></td>
            <td><%=ofer.getTemporal4() %></td>
            <td align="center">

                <a href='<%=cc.getLocal()%>SEliminarcupo?cod=<%=ofer.getId_oferta()%>&fecha=<%=fecha%>&especialidad=<%=e%>' onclick="return confirma()" class="button2">
                    Eliminar
                </a>
            </td>

        </tr>
        <%}
        %>
    </table>
</form>
