<%-- 
    Document   : modificardatostens
    Created on : 27-11-2017, 15:36:27
    Author     : a
--%>

<%@page import="Modelos.atencion_clinica_oftalmologia"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Modificar TENS</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
        <link href="../../public/css/estilos.css" rel="stylesheet">

        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>

        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>


        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>

    </head>
    <%

        String id = request.getParameter("id");
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        atencion_clinica_oftalmologia aco = caco.BuscarAtencionTensporId(Integer.parseInt(id));

        String mensaje = "";
        if (request.getParameter("men") != null) {
            mensaje = request.getParameter("men");
            String idcita = request.getParameter("id");
            String estatus = request.getParameter("estatus");

            if (Integer.parseInt(estatus) == 5) {

    %>
    <script  type="text/javascript">


        window.opener.location.href = "<%=caco.getLocallink()%>Oftalmologia/RegistrarAtencionClinicaTecnologo.jsp?cita=<%=request.getParameter("id")%>";
            window.close();

    </script>


    <% } else {
    %>
    <script  type="text/javascript">


        window.opener.location.href = "<%=caco.getLocallink()%>Oftalmologia/FinalizarAtencionTecnologo.jsp?cita=<%=request.getParameter("id")%>";
            window.close();

    </script>


    <%

        }

    } else {%>
    <body>
        <div class="container"> 






            <form style="border: #619fd8 5px solid;" name="mt" class="form-horizontal" id="mt" action='<%=caco.getLocal()%>modificarTens' method="post" onsubmit="return validar()" >

                <fieldset>
                    <legend class="text-center header">Modificar Atención TENS</legend>
                    <input id="idatencion" name="idatencion" value="<%=aco.getId_atencion_coftalmologia()%>" hidden >

                    <table style=" " >

                        <tr><td><div class=" letra"> Realiza Autorefracción:</div></td></tr>
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td><span class="col-md-1 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" ></i></span>

                                <div class="col-md-8">
                                    <select id="autorefraccion" name="autorefraccion" > 
                                        <%if (aco.getAutorefraccion() == 1) { %>
                                        <option selected value="1"> SI </option>
                                        <option  value="0"> NO </option>
                                        <%} else {%>
                                        <option selected value="0"> NO </option>
                                        <option  value="1"> SI </option>
                                        <%}%>
                                    </select>  
                                </div>
                            </td>
                            <td><div class=" letra"> Observaciones:</div>
                                <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                <div class="col-md-8">
                                    <input type="text" style=" height: 80px "  id="observacion" name="observacion" value="<%=aco.getAutorefaccionobservaciones()%>"  class="form-control" placeholder="Observaciones"  >


                                </div>  
                            </td>

                        </tr>  

                    </table>


                    <table style="border-top: #619fd8 2px solid;" >

                        <tr><td colspan="10"><div class=" letra"> Realiza Medición de PIO:</div></td></tr>
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td width="2" ><span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OI</i></span>
                            </td>
                            <td>
                                <div>
                                    <select id="oi" name="oi" id="oi" > 
                                        <%if (aco.getPresionio() == 1) {%>
                                        <option  selected value="1"> SI </option>
                                        <option  value="0"> NO </option>
                                        <%} else { %>
                                        <option selected value="0"> NO </option>
                                        <option   value="1"> SI </option>

                                        <% }%>
                                    </select>  
                                </div>
                            </td>
                            <td width="2">
                                <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OD</i></span>
                            </td>
                            <td>
                                <div>
                                    <select id="od" name="od" id="od"   >  
                                        <%if (aco.getPresionod() == 1) {%>
                                        <option  selected value="1"> SI </option>
                                        <option  value="0"> NO </option>
                                        <%} else { %>
                                        <option selected value="0"> NO </option>
                                        <option   value="1"> SI </option>

                                        <% }%>
                                    </select>  
                                </div>
                            </td>

                        </tr>
                        <tr><td> &nbsp;</td></tr>
                        <br>
                    </table>
                    <table  WIDTH=1000>

                        <tr><td colspan="10"><div class=" letra"> Resultado de Presión Intra Ocular:</div></td></tr>
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td width="2" ><span ><i class="fa fa-edit bigicon" style="font-size:16px" > OI</i></span>
                            </td>
                            <td WIDTH=500>
                                <div>
                                    <input type="text" id="numerooi" name="numerooi" value="<%=aco.getNumerooi()%>" placeholder="Número OI"> 
                                </div>
                            </td>
                            <td width="4">
                                <span ><i class="fa fa-edit bigicon" style="font-size:16px" > OD</i></span>
                            </td>
                            <td WIDTH=500>
                                <div>
                                    <input type="text" id="numerood" name="numerood" value="<%=aco.getNumerood()%>" placeholder="Número OD">    
                                </div>
                            </td>

                        </tr> 

                    </table>
                    <table>



                        <tr><td> &nbsp;</td></tr>

                        <br>
                        <tr><div class=" letra"> Observaciones:</div>
                        <td colspan="10"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                            <div class="col-md-8">
                                <input style=" height: 80px " type="text" value="<%=aco.getPresionobservaciones()%>" rows="3" id="observacionpresion"  name="observacionpresion"  class="form-control" placeholder="Observaciones"  >


                            </div>  
                        </td>

                        </tr>  

                    </table>




                    <table style="border-top: #619fd8 2px solid;" >
                        <tr><td colspan="4"><div class=" letra"> Realiza Dilatación:</div></td></tr>
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                        <tr>
                            <td width="2" ><span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OI</i></span>
                            </td>
                            <td>
                                <div>
                                    <select id="dilatacionoi" name="dilatacionoi" > 
                                        <%if (aco.getDilatacionoi() == 1) {%>
                                        <option  selected value="1"> SI </option>
                                        <option  value="0"> NO </option>
                                        <%} else { %>
                                        <option selected value="0"> NO </option>
                                        <option   value="1"> SI </option>

                                        <% }%>
                                    </select>  
                                </div>
                            </td>
                            <td  width="4"> <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OD</i></span></span>
                            </td>
                            <td>
                                <div >
                                    <select id="dilatacionod" name="dilatacionod"> 
                                        <%if (aco.getDilatacionod() == 1) {%>
                                        <option  selected value="1"> SI </option>
                                        <option  value="0"> NO </option>
                                        <%} else { %>
                                        <option selected value="0"> NO </option>
                                        <option   value="1"> SI </option>

                                        <% }%>
                                    </select>  
                                </div>
                            </td>

                        </tr>
                        <tr><td> &nbsp;</td></tr>
                    </table>

                    <table   >
                        <tr><td colspan="4"><div class=" letra">Colirio utilizado para dilatación:</div></td></tr>
                        <tr><td> &nbsp;</td></tr>

                        <tr>
                            <td colspan="8" WIDTH=1000><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                <div class="col-md-8" >
                                    <input style=" height: 80px " type="text" value="<%=aco.getDilatacion_colirio()%>"  rows="3" id="colirio" name="colirio"  class="form-control" placeholder="Colirio utilizado para dilatación" >


                                </div>  
                            </td>

                        </tr>
                    </table>
                    <table>

                        <tr><td colspan="4"><div class=" letra"> Observaciones:</div></td></tr>
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td colspan="4"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                <div class="col-md-8">
                                    <input style=" height: 80px " type="text"  rows="3" value="<%=aco.getDilatacion_observaciones()%>" id="observaciondilatacion" name="observaciondilatacion"  class="form-control" placeholder="Observaciones"  >


                                </div>  
                            </td>

                        </tr>   

                    </table>

                    <table style="border-top:   #619fd8 2px solid;" >
                        <br>
                        <div class=" letra"> Observaciones relevantes en el proceso de atención:</div>
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                <div class="col-md-8">
                                    <input style=" height: 80px " type="text"  rows="4" id="observacionr" value="<%=aco.getRegistro_de_observaciones()%>" name="observacionr"  class="form-control" placeholder="Observaciones relevantes" >


                                </div>  
                            </td>
                        </tr>

                    </table>

                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Modificar</button>

                            <button onclick="javascritp: window.close();"   class="btn btn-primary btn-lg" >Cerrar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</body>

<%}%>
</html>