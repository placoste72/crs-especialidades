<%-- 
    Document   : RegistrarAtencionClinicaOftalmologia
    Created on : 13-04-2017, 14:59:55
    Author     : a
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="Modelos.atencion"%>
<%@page import="Modelos.excepcion_garantia"%>
<%@page import="Modelos.registrodepatologia"%>
<%@page import="Modelos.vicio_refraccion"%>
<%@page import="Modelos.receta_lente_opticos"%>
<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Modelos.atencion_clinica_oftalmologia"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.cita"%>

<!DOCTYPE html>
<html>
    <head>


        <link href="../../public/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>

        <script src="../../public/js/script2.js" type="text/javascript"></script>

        <link rel="shortcut icon" href="../../public/imagenes/icon.png">
        <script src="../../public/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css"> 
        <link href="../../public/css/estilos.css" rel="stylesheet"> 

        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/> 
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>

        <link href="../../public/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/> 
        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/fileinput.js" type="text/javascript"></script>
        <script src="../../public/js/fileinput_locale_fr.js" type="text/javascript"></script>
        <script src="../../public/js/fileinput_locale_es.js" type="text/javascript"></script>
        <link href="../../public/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>

        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>
        <style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
        <script type="text/javascript" src="../../public/js/calendar.js"></script>
        <script type="text/javascript" src="../../public/js/calendar-es.js"></script>

        <script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>

        <style>
            * {box-sizing: border-box}
            body {font-family: "Lato", sans-serif;}

            /* Style the tab */
            div.tab {
                float: left;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
                width: 30%;
                height: 100%;

            }

            /* Style the buttons inside the tab */
            div.tab button {
                display: block;
                background-color: inherit;
                color: black;
                padding: 10px 8px;
                width: 100%;
                border: none;
                outline: none;
                text-align: left;
                cursor: pointer;
                transition: 0.3s;
                font-size: 14px;
                color:#428bca;
            }

            /* Change background color of buttons on hover */
            div.tab button:hover {
                background-color: #ddd;
                color:#428bca;
            }

            /* Create an active/current "tab button" class */
            div.tab button.active {
                background-color: #194f80;
                color: #ddd;
            }

            /* Style the tab content */
            .tabcontent {
                float: left;
                padding: 0px 12px;
                border: 1px solid #ccc;
                width: 70%;
                border-left: none;
                height: 100%;
            }

            table{
                font-size: 12px;
            }
            th, td {
                padding: 0px;
            }

        </style>

    </head>
    <%
        int idatencion = 0;
        String cita = request.getParameter("cita");
        controlador_cita cc = new controlador_cita();
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        atencion_clinica_oftalmologia aco = new atencion_clinica_oftalmologia();
        atencion_clinica_tecnologo act = new atencion_clinica_tecnologo();
        receta_lente_opticos rlo = new receta_lente_opticos();
        vicio_refraccion vr = new vicio_refraccion();
        registrodepatologia rp = new registrodepatologia();
        idatencion = caco.buscaridtencionTecnologoconIdCita(Integer.parseInt(cita));
        act = caco.buscarAtencionTecnologoporId(idatencion);
        rlo = caco.buscarRecetadeIdAtencion(idatencion,0);
        rp = caco.BuscarRegistrodePatologia(idatencion);

        vr = caco.buscarVicioRefraccionporIdAtencion(idatencion);

        aco = caco.BuscarAtencionTecnicoporCita(Integer.parseInt(cita));
        cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
        String[] datos = cc.buscarPerfilyNombredelloggeado();
        Integer[] idpertinencia = {1, 0};
        String[] nombrepertinencia = {"SI", "NO"};
        String fechaComoCadena = "";
        String motivotexto = "";
        boolean tengoSC = cc.buscarsiExistecontrol(Integer.parseInt(cita));

        if (tengoSC == true) {
            atencion a = cc.buscarControlporIdcita(Integer.parseInt(cita));

            fechaComoCadena = a.getFormatofecha();
            motivotexto = a.getDetallereceta();
        }

        Integer cantidadTratamientoParche = 0;
        String rut = c.getMotivo_cancela();

        String diag = "";
        String fundam = "";
        String tratami = "";

        if (rp.getDiagnostico() != null) {
            diag = rp.getDiagnostico();
        }
        if (rp.getTratamientoeindicacion() != null) {
            tratami = rp.getTratamientoeindicacion();
        }
        if (rp.getFundamentodeldiagnostico() != null) {
            fundam = rp.getFundamentodeldiagnostico();
        }


    %>

    <body>


        <section  class="section">
            <nav class="nav1 ">
                <table class="table table-bordered" style="width: 100%">
                    <thead><tr>
                            <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos del Paciente</th>
                        </tr>
                        <tr>
                            <th>RUT</th>
                            <th>Paciente</th>
                            <th>Edad</th>
                            <th>Sexo</th>


                        </tr>

                    </thead>

                    <tr>
                        <td><%= c.getMotivo_cancela()%></td>
                        <td><%= c.getRut_paciente()%></td>
                        <td><%= c.getTemporales2()%></td>
                        <td><%= c.getTemporales1()%></td>


                    </tr>
                </table>

                <table class="table table-bordered" style="width: 100%">
                    <thead><tr>
                            <th colspan="6" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos de la Citaci�n</th>
                        </tr>
                        <tr>
                            <th>Especialidad</th>
                            <th>Tipo de Cita</th>
                            <th>Diagn�stica</th>


                            <th>Fecha y Hora</th>


                        </tr>

                    </thead>

                    <tr>
                        <td><%= c.getTemporales()%> </td>
                        <td><%= c.getTemporales3()%>  </td>
                        <td><%= c.getTemporales4()%></td>


                        <td><%= c.getMotivo()%></td>


                    </tr>
                </table>
                <p class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; font-size: 10px; text-align: left">Datos otras atenciones</p>
                <table class="table-striped" >
                    <tr>
                        <th>Fecha</th>
                        <th>Atencion</th>
                        <th>Especialidad</th>
                        <th>Profesional</th>
                        <th>Detalle</th>
                    </tr>
                    <%for (atencion_clinica_tecnologo at : cc.BuscarAtencionesdeunPaciente(c.getMotivo_cancela())) {

                    %>
                    <tr>
                        <td><%=at.getAuxiliarcinco()%></td>
                        <td><%=at.getAuxiliarseis()%></td>
                        <td><%=at.getAuxiliardos()%></td>
                        <td><%=at.getAuxiliartres()%></td>
                        <%if (at.getAuxiliarcuatro() == 1) {
                        %>
                        <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   >

                                VER  </a> </td>
                                <%} else if (at.getAuxiliarcuatro() == 2) {%>
                        <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   >

                                VER  </a> </td>
                                <%} else if (at.getAuxiliarcuatro() == 3) {%>
                        <td><a onClick="window.open('<%=cc.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  >

                                VER  </a> </td>
                                <%} else if (at.getAuxiliarcuatro() == 4) {%>
                        <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   >

                                VER  </a> </td>
                                <%} else if (at.getAuxiliarcuatro() == 5) {%>
                        <td> </td>
                        <%} else if (at.getAuxiliarcuatro() == 6) {%>
                        <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"   >

                                Protocolo  </a> 

                            <%int ide = cc.idepicrisis(at.getId_atencion_tecnologo());%>
                            <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    >

                                Epicrisis  </a> </td>
                                <%} else if (at.getAuxiliarcuatro() == 7) {%>
                        <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   >

                                VER  </a> </td>
                                <%} else if (at.getAuxiliarcuatro() == 8) {%>
                        <td><a  onClick="window.open('<%=cc.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenolog�a', 'width=1200, height=1000')"   >

                                VER  </a> </td>
                                <%} else if (at.getAuxiliarcuatro() == 9) {%>
                        <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   >

                                VER  </a> </td>
                                <%}%>
                    </tr>
                    <%

                        }%>

                </table>

            </nav>
            <article class="article">
                <div>
                    <div id="dialogoverlay"></div>
                    <div id="dialogbox">
                        <div>
                            <div id="dialogboxhead"></div>
                            <div id="dialogboxbody"></div>
                            <div id="dialogboxfoot"></div>
                        </div>
                    </div>

                    <div>

                        <fieldset>

                            <legend class="text-center header">Registro Tecnologo</legend>
                            <table class="table table-striped" style="width: 100%">
                                <thead>

                                    <tr>
                                        <th>Perfil:          <%=datos[1]%></th>

                                        <th>Usuario:         <%=datos[0]%></th>


                                    </tr>
                                </thead>
                            </table>   

                            <table class="table table-bordered" border="0" style=" width: 100%">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="btn-primary" style=" padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 20px; text-align: center">INFORMACI�N DE TENS</th>

                                    </tr>
                                    <tr>
                                        <th height=20px>Auto Refracci�n</th>
                                        <th height=20px>Medici�n de Presi�n Intra Ocular</th>
                                        <th height=20px>Dilataci�n</th>
                                        <th height=20px>Observaci�n al proceso de atenci�n</th>
                                        <th></th>

                                    </tr>

                                </thead>

                                <tr>
                                    <td><%= aco.getAutorefaccionobservaciones()%></td>
                                    <td><%= aco.getPresionobservaciones()%></td>
                                    <td><%= aco.getDilatacion_observaciones()%></td>
                                    <td><%= aco.getRegistro_de_observaciones()%></td>
                                    <td align="center">
                                        <a href="modificardatostens.jsp?id=<%=aco.getId_atencion_coftalmologia()%>" onClick="return popup2(this, 'notes')" class="button3">
                                            Modificar Datos
                                        </a>


                                    </td>


                                </tr>
                            </table>       


                            <div class="container"> 



                                <form id="atencioOft" name="atencioOft" method="POST" onsubmit="return validar()" action="<%=caco.getLocal()%>finalizaratenciontecnologo"  >
                                    <input id="cita" name="cita" type="text" value="<%=cita%>" hidden>
                                    <input id="idatencioncita" name="idatencioncita" value="<%=c.getId_tipoatencion()%>" hidden>



                                    <div id="RegistroPatologiaGES" style=" width:100% " class="tabcontent" >

                                        <%if (c.getId_tipoatencion() == 1) {%>  
                                        <table> 
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td colspan="2">
                                                    <span class="col-md-8   col-md-offset-4 "><i class="fa fa-edit bigicon"  style=" font-size: 20px" >�Es pertinente la derivaci�n a su atenci�n?</i></span>
                                                </td>
                                                <td>
                                                    <div class="col-md-8">
                                                        <select id="pertinencia" name="pertinencia" > 


                                                            <option selected  value="-1"> Seleccionar </option>

                                                            <% for (int i = 0; i < idpertinencia.length; ++i) {
                                                                    if (idpertinencia[i] == act.getPertinencia()) {%>
                                                            <option selected value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                                <%} else {%>

                                                            <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                                <% }
                                                                    }
                                                                %>

                                                        </select>  
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr><td> &nbsp;</td></tr>
                                        </table>
                                        <%}%>


                                        <table style="border-top:  #619fd8 2px solid; ">
                                            <tr><td colspan="8"><div class=" letra">Motivo de la Consulta:</div></td></tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input type="text" style=" height: 80px" value="<%=vr.getMotivoconsulta()%>"  rows="3" id="motivodelaconsulta" name="motivodelaconsulta"  class="form-control" placeholder="Motivo de la Consulta"  >


                                                    </div>  
                                                </td>

                                            </tr>
                                        </table>
                                        <br>
                                        <table style="border-top:  #619fd8 2px solid; ">
                                            <tr><td colspan="8"><div class=" letra">Antecedentes Morbidos</div></td></tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr> 
                                                <td >
                                                    <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > HTA</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <select id="amhta" name="amhta"  > 
                                                            <% if (vr.getAmhta() == 1) { %> 
                                                            <option selected value="1"> SI </option>
                                                            <option  value="0"> NO </option>
                                                            <%} else {
                                                            %> 
                                                            <option selected value="0"> NO </option>
                                                            <option  value="1"> SI </option>

                                                            <%
                                                                }%>
                                                        </select>  
                                                    </div>
                                                </td>
                                                <td >
                                                    <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > DM</i></span>
                                                </td><td>
                                                    <div>
                                                        <select id="amdm" name="amdm"  > 
                                                            <% if (vr.getAmdm() == 1) { %> 
                                                            <option selected value="1"> SI </option>
                                                            <option  value="0"> NO </option>
                                                            <%} else {
                                                            %> 
                                                            <option selected value="0"> NO </option>
                                                            <option  value="1"> SI </option>

                                                            <%
                                                                }%>
                                                        </select>  
                                                    </div>
                                                </td>
                                                <td >
                                                    <span ><i class="fa fa-times bigicon" style="font-size:16px" >  A�os</i></span>
                                                </td>
                                                <td >
                                                    <div>
                                                        <input type="text" placeholder="A�os" value="<%=vr.getAnosam()%>" id="amano" name="amano" size="10px"> </input>

                                                    </div>

                                                </td>
                                                <td >
                                                    <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > Glaucoma</i></span>
                                                </td><td>
                                                    <div>
                                                        <select id="amglaucoma" name="amglaucoma"  > 
                                                            <% if (vr.getAmglaucoma() == 1) { %> 
                                                            <option selected value="1"> SI </option>
                                                            <option  value="0"> NO </option>
                                                            <%} else {
                                                            %> 
                                                            <option selected value="0"> NO </option>
                                                            <option  value="1"> SI </option>

                                                            <%
                                                                }%>
                                                        </select>  
                                                    </div>
                                                </td>


                                            </tr>
                                        </table>
                                        <br>
                                        <table>
                                            <tr><td><div class=" letra">Otro:</div></td></tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input style=" height: 80px"  value="<%=vr.getAmotro()%>" type="text"  rows="2" id="amotro" name="amotro"  class="form-control" placeholder="Otro"  >


                                                    </div>  
                                                </td>

                                            </tr>

                                        </table>
                                        <br>
                                        <table style="border-top:  #619fd8 2px solid; ">
                                            <tr><td colspan="8"><div class=" letra">Presi�n Intraocular[Aire/Aplan�tica]</div></td></tr>
                                            <tr><td> &nbsp;</td></tr>  
                                            <tr> 
                                                <td>
                                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" >  OD</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <input type="text" id="piod" name="piod" value="<%=vr.getPiod()%>" >   
                                                    </div>
                                                </td>
                                                <td><P class="letra" style=" margin-right: 320px">mmHG </P> </td>

                                                <td>
                                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" >  OI</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <input type="text" id="pioi" name="pioi" value="<%=vr.getPioi()%>">  
                                                    </div>
                                                </td>
                                                <td><P class="letra">mmHG </P> </td>
                                            </tr>
                                        </table>

                                        <br>
                                        <table style="border-top:  #619fd8 2px solid; ">
                                            <tr><td colspan="8"><div class=" letra">Agudeza Visual</div></td></tr>
                                            <tr><td> &nbsp;</td></tr> 
                                            <tr> 
                                                <td>
                                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" >  PL SC OD</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <input type="text" id="plscod" name="plscod" value="<%=vr.getPlscod()%>" >  
                                                    </div>
                                                </td>

                                                <td>
                                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" >  PL CC OD</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <input type="text" id="plccod" name="plccod" value="<%=vr.getPlccod()%>" >  
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td>
                                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" >  PL SC OI</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <input type="text" id="plscoi" name="plscoi" value="<%=vr.getPlscoi()%>" >  
                                                    </div>
                                                </td>

                                                <td>
                                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" >  PL CC OI</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <input type="text" id="plccoi" name="plccoi" value="<%=vr.getPlccoi()%>" >  
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td> &nbsp;</td>
                                                <td> &nbsp;</td>
                                                <td>
                                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" >  PL CC ODI</i></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <input type="text" id="plccadi" name="plccadi" value="<%=vr.getPlccadi()%>" >  
                                                    </div>
                                                </td>

                                            </tr> 
                                        </table>
                                        <BR>
                                        <table style="border-top:  #619fd8 2px solid; ">

                                            <tr><td colspan="8"><div class=" letra"> Registro de Patologia </div></td></tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td >
                                                    <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > Ges</i></span>
                                                </td><td>
                                                    <div>
                                                        <select id="ges" name="ges" onchange="javascript:
                                                                        var valoruno = document.forms['atencioOft']['ges'].value;
                                                                var tengo = document.forms['atencioOft']['excepciondegarantia'].checked;

                                                                if (valoruno == 1 && !tengo) {

                                                                    document.getElementById('k').style.display = 'block';
                                                                    document.getElementById('nogestabla').style.display = 'none';


                                                                } else if (valoruno == 0 && !tengo) {
                                                                    document.getElementById('nogestabla').style.display = 'block';
                                                                    document.getElementById('k').style.display = 'none';
                                                                } else {
                                                                    document.getElementById('nogestabla').style.display = 'none';
                                                                    document.getElementById('k').style.display = 'none';
                                                                }
                                                                "> 

                                                            <%

                                                                if (vr.getGes() == 1) { %>

                                                            <option value="-1">Es Ges?</option>
                                                            <option selected value="1"> SI </option>
                                                            <option  value="0"> NO </option>

                                                            <%} else if (vr.getGes() == 0) {
                                                            %> 
                                                            <option value="-1">Es Ges?</option>
                                                            <option selected value="0"> NO </option>
                                                            <option  value="1"> SI </option>


                                                            <%
                                                            } else { %>
                                                            <option selected value="-1">Es Ges?</option>
                                                            <option  value="1"> SI </option>
                                                            <option  value="0"> NO </option>


                                                            <%}%>

                                                        </select>  
                                                    </div>
                                                </td>
                                                <!--incluyo garantia -->
                                                <td>
                                                    <span><i class="fa fa-chevron-down bigicon" style="font-size:16px" > Excepci�n de Garant�a</i></span>
                                                </td>
                                                <td>
                                                    <input type="checkbox" id="excepciondegarantia" name="excepciondegarantia" value="checkbox" onchange="javascript: var tengo = document.forms['atencioOft']['excepciondegarantia'].checked
                                                            if (tengo) {
                                                                document.getElementById('eg').style.display = 'block';
                                                                document.getElementById('k').style.display = 'none';
                                                                document.getElementById('nogestabla').style.display = 'none';
                                                            } else {
                                                                document.getElementById('eg').style.display = 'none';
                                                                var valoruno = document.forms['atencioOft']['ges'].value;


                                                                if (valoruno == 1) {

                                                                    document.getElementById('k').style.display = 'block';
                                                                    document.getElementById('nogestabla').style.display = 'none';


                                                                } else if (valoruno == 0) {
                                                                    document.getElementById('nogestabla').style.display = 'block';
                                                                    document.getElementById('k').style.display = 'none';
                                                                } else
                                                                {
                                                                    document.getElementById('nogestabla').style.display = 'none';
                                                                    document.getElementById('k').style.display = 'none';

                                                                }

                                                            }">   &nbsp;&nbsp;
                                                    <i class="fa fa-check bigicon" style="font-size:16px">  </i>
                                                </td> 
                                            </tr>   
                                        </table>
                                        <% excepcion_garantia exg = caco.buscarGarantiaporIddeAtencion(idatencion,1);%>
                                        <table id="eg" style=" display: none; width: 100%">
                                            <tr><td> &nbsp;</td></tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr><td><div class=" letra"><strong> Causal de excepcion</strong></div></td></tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="rpd" name="rpd" value="checkbox"> &nbsp;&nbsp;
                                                    <i class="fa fa-check bigicon" style="font-size:16px"> Rechazo del prestador Designado  </i>
                                                </td>  
                                                <td>
                                                    <input type="checkbox" id="rapg" name="rapg" value="checkbox"> &nbsp;&nbsp;
                                                    <i class="fa fa-check bigicon" style="font-size:16px"> Rechazo de la atenci�n o procedimiento garantizado  </i>
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="pr" name="pr" value="checkbox"> &nbsp;&nbsp;
                                                    <i class="fa fa-check bigicon" style="font-size:16px">Prestaci�n Rechazada  </i>
                                                </td>  
                                                <td>
                                                    <input type="checkbox" id="ocdp" name="ocdp" value="checkbox"> &nbsp;&nbsp;
                                                    <i class="fa fa-check bigicon" style="font-size:16px"> Otra Causa definida por el paciente </i>
                                                </td>  
                                            </tr>

                                            <tr><td><div class=" letra"> Observacion:</div></td></tr>
                                            <tr><td> &nbsp;</td></tr>
                                            <tr>
                                                <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input type="text" style=" height: 80px"    id="observaciongarantia" name="observaciongarantia" <% if (exg.getObservaciones() != null) {%> value="<%=exg.getObservaciones()%>" <%}%> class="form-control" placeholder="OBSERVACIONES(complementar selecci�n anterior)"  >


                                                    </div>  
                                                </td>

                                            </tr>

                                            <tr><td> &nbsp;</td></tr>
                                        </table>
                                        <table id="k" style="display: none">
                                            <tr>
                                                <td> 
                                                    <table>
                                                        <tr><td><div class=" letra"><strong> Problema de Salud</strong></div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>


                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" id="v" name="vicio" value="checkbox" onchange="javascript:
                                                                                var vicio = document.forms['atencioOft']['v'].checked;
                                                                        if (!vicio) {
                                                                            document.getElementById('confirma').checked = false;
                                                                            document.getElementById('confirma2').checked = false;
                                                                            document.getElementById('a').style.display = 'none';
                                                                            document.getElementById('b').style.display = 'none';
                                                                            document.getElementById('receta').style.display = 'none';
                                                                        }

                                                                       "> &nbsp;&nbsp;
                                                                <i class="fa fa-check bigicon" style="font-size:16px"> Vicios de Refracci�n(problema 29) </i>
                                                            </td>
                                                            <td>
                                                                <input type="radio" id="confirma"  name="confirma" value="si" onchange="javascritp:

                                                                                var pre = document.getElementById('confirma').value;
                                                                        if (pre == 'si') {
                                                                            document.getElementById('a').style.display = 'block';
                                                                            document.getElementById('b').style.display = 'none';
                                                                        } else {
                                                                            document.getElementById('a').style.display = 'none';
                                                                        }"> &nbsp;&nbsp;
                                                                <i class="fa fa-check bigicon" style="font-size:16px"> Confirma (SI)</i>
                                                            </td>
                                                            <td>
                                                                <input type="radio" id="confirma2"  name="confirma" value="no" onchange="javascritp:

                                                                                var pre = document.getElementById('confirma2').value;
                                                                        if (pre == 'no') {
                                                                            document.getElementById('b').style.display = 'block';
                                                                            document.getElementById('a').style.display = 'none';
                                                                            document.getElementById('receta').style.display = 'none';
                                                                        } else {
                                                                            document.getElementById('b').style.display = 'none';
                                                                        }">&nbsp; &nbsp;
                                                                <i class="fa fa-check bigicon" style="font-size:16px">  Descarta (NO)</i>
                                                            </td>

                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>

                                                    </table>

                                                    <table id="a" style="display: none">
                                                        <tr><td><div class=" letra"> Diagnostico:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input style=" height: 80px" type="text" value="<%=diag%>"  rows="3" id="diagnosticoconges" name="diagnosticoconges"    class="form-control" placeholder="Diagnostico de Refracci�n">


                                                                </div>  
                                                            </td>

                                                        </tr>

                                                        <tr><td colspan="4"><div class=" letra"> Indicaciones para el problema GES especifico:</div></td></tr>

                                                        <tr><td colspan="4"><div class=" letra"> <strong>Vicios de Refracci�n en Personas de 65 a�os y m�s (problema 29)</strong></div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>

                                                        <tr>
                                                            <td colspan="4">


                                                                <div class="col-md-8">

                                                                    <input type="checkbox" name="tratamientolentesCerca" id="tratamientolentesCerca" value="checkbox" onchange="javascritp:

                                                                                    var pre = document.getElementById('tratamientolentesCerca').checked;
                                                                            if (pre) {
                                                                                document.getElementById('receta').style.display = 'block';
                                                                            } else {
                                                                                document.getElementById('receta').style.display = 'none';
                                                                            }">&nbsp;&nbsp;
                                                                    <i class="fa fa-check bigicon" style="font-size:16px">  Tratamiento con Lentes �pticos Cerca </i>
                                                                    <br>
                                                                    <input type="checkbox" name="tratamientolentesLejos" id="tratamientolentesLejos" value="checkbox" onchange="javascritp:

                                                                                    var pre = document.getElementById('tratamientolentesLejos').checked;
                                                                            if (pre) {
                                                                                document.getElementById('receta').style.display = 'block';
                                                                            } else {
                                                                                document.getElementById('receta').style.display = 'none';
                                                                            }">&nbsp;&nbsp;<i class="fa fa-check bigicon" style="font-size:16px">  Tratamiento con Lentes �pticos Lejos</i>
                                                                </div>  
                                                            </td>

                                                        </tr>  


                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td> &nbsp;</td></tr>

                                                        <br>
                                                        <tr><td colspan="4"><div class=" letra"> Plazos:Tratamiento con Lentes �pticos: Entrega de lentes en 30 d�as desde Confirmaci�n Diagn�stica</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <br>

                                                    </table>


                                                    <br>
                                                    <table id="b" style="display: none">
                                                        <tr><td><div class=" letra"> Diagnostico:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input type="text" style=" height: 80px" value="<%=diag%>" rows="3" id="diagnosticosiges" name="diagnosticosiges"  class="form-control" placeholder="Diagnostico no confirmo"  >


                                                                </div>  
                                                            </td>

                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td><div class=" letra">Fundamento de Diagnostico:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input type="text" style=" height: 80px" value="<%=fundam%>" rows="3" id="fundamentodediagnostico" name="fundamentodediagnostico"  class="form-control" placeholder="Fundamento de Diagnostico"  >


                                                                </div>  
                                                            </td>

                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td><div class=" letra"> Tratamiento e Indicaci�n:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input type="text" style=" height: 80px " value="<%=tratami%>"  rows="3" id="tratamientoeindicacion" name="tratamientoeindicacion"  class="form-control" placeholder="Tratamiento e Indicaci�n"  ></textarea>


                                                                </div>  
                                                            </td>

                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td colspan="4"><div class=" letra"> Plazos:Tratamiento con Lentes �pticos: Entrega de lentes en 30 d�as desde Confirmaci�n Diagn�stica</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <br>
                                                    </table>

                                                    <br>
                                                    <%
                                                        String od = "";
                                                        String odc = "";
                                                        String ode = "";
                                                        String oie = "";
                                                        String oic = "";
                                                        String oiej = "";
                                                        String add = "";
                                                        String dpl = "";
                                                        String dpc = "";
                                                        String or = "";

                                                        if (rlo.getOjoderechoesfera() != null) {
                                                            od = rlo.getOjoderechoesfera();
                                                        }

                                                        if (rlo.getOjoderechoeje() != null) {
                                                            ode = rlo.getOjoderechoeje();
                                                        }

                                                        if (rlo.getOjoderechocilindro() != null) {
                                                            odc = rlo.getOjoderechocilindro();
                                                        }
                                                        if (rlo.getOjoiaquierdacilindro() != null) {
                                                            oic = rlo.getOjoiaquierdacilindro();
                                                        }
                                                        if (rlo.getOjoizquierdaeje() != null) {
                                                            oiej = rlo.getOjoizquierdaeje();
                                                        }

                                                        if (rlo.getOjoizquierdoesfera() != null) {
                                                            oie = rlo.getOjoizquierdoesfera();
                                                        }
                                                        if (rlo.getAadd() != null) {
                                                            add = rlo.getAadd();
                                                        }

                                                        if (rlo.getDistanciapupilarcerca() != null) {
                                                            dpc = rlo.getDistanciapupilarcerca();
                                                        }

                                                        if (rlo.getDistanciapupilarlejos() != null) {
                                                            dpl = rlo.getDistanciapupilarlejos();
                                                        }

                                                        if (rlo.getObservaciones() != null) {
                                                            or = rlo.getObservaciones();
                                                        }

                                                    %>

                                                    <table id="receta" style="border-top:  #619fd8 2px solid;  display: none ">
                                                        <tr><td><div class=" letra"> Receta de Lente:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" style="height:40px " id="odf" value="<%=od%>" name="odf" placeholder=" Ojo Derecho Esfera"> 
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" style="height:40px " id="odc" name="odc" value="<%=odc%>"  placeholder=" Ojo Derecho Cilindro">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" style=" height:40px" id="ode" name="ode" value="<%=ode%>" placeholder=" Ojo Derecho Eje">
                                                                </div>
                                                            </td>



                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" style=" height: 40px " id="oie" value="<%=oie%>" name="oie" placeholder=" Ojo Izquierdo Esfera">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" id="oic" style=" height: 40px " value="<%=oic%>" name="oic"  placeholder=" Ojo Izquierdo Cilindro">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input  rows="1" cols="22" id="oiej" style=" height: 40px " value="<%=oiej%>" name="oiej" placeholder=" Ojo Izquierdo Eje">
                                                                </div>
                                                            </td>



                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <input style=" height: 40px " rows="1" cols="22" value="<%=add%>" id="add" name="add" placeholder="ADD">
                                                                </div>
                                                            </td> 
                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <input rows="1" style=" height: 40px" value="<%=dpl%>" cols="22" id="dpl" name="dpl" placeholder="Distancia Pupilar Lejos"> 
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" style=" height: 40px" value="<%=dpc%>" id="dpc" name="dpc" placeholder="Distancia Pupilar Cerca">
                                                                </div>
                                                            </td>




                                                        </tr>
                                                        <tr><td colspan="4"><div class=" letra"> Observaciones:</div></td></tr>
                                                        <tr>
                                                            <td colspan="4"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input rows="1" style=" height: 80px" value="<%=or%>" type="text"  rows="3" id="observacionesreceta" name="observacionesreceta"  class="form-control" placeholder="Observaciones">


                                                                </div>  
                                                            </td>

                                                        </tr> 
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <%
                                            if (exg.getOtra_causa() == 1) {
                                        %>  
                                        <script type="text/javascript">
                                            document.getElementById('ocdp').checked = 1;

                                        </script>
                                        <%}%> 

                                        <%
                                            if (exg.getPrestacion_rechazada() == 1) {
                                        %>  
                                        <script type="text/javascript">
                                            document.getElementById('pr').checked = 1;

                                        </script>
                                        <%}%> 
                                        <%
                                            if (exg.getRechazo_prestador_designado() == 1) {
                                        %>  
                                        <script type="text/javascript">
                                            document.getElementById('rpd').checked = 1;

                                        </script>
                                        <%}%>
                                        <%
                                            if (exg.getOtra_causa() == 1) {
                                        %>  
                                        <script type="text/javascript">
                                            document.getElementById('ocdp').checked = 1;

                                        </script>
                                        <%}%> 
                                        <%
                                            if (exg.getRechazo_atencion_garantizado() == 1) {
                                        %>  
                                        <script type="text/javascript">
                                            document.getElementById('rapg').checked = 1;

                                        </script>
                                        <%}%> 




                                        <%
                                            if (rp.getTratamientocerca() == 1) {
                                        %>
                                        <script type="text/javascript">
                                            document.getElementById('tratamientolentesCerca').checked = 1;
                                            validartratamientocerca();
                                        </script>

                                        <%
                                            }

                                            if (rp.getTratamientolejos() == 1) {
                                        %>
                                        <script type="text/javascript">
                                            document.getElementById('tratamientolentesLejos').checked = 1;
                                            validartratamientolejos();
                                        </script>

                                        <%
                                            }

                                        %>



                                        <table id="nogestabla" style="display: none">
                                            <tr>
                                                <td> 
                                                    <table>
                                                        <tr><td><div class=" letra"><strong> Problema de Salud</strong></div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>


                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" name="vicionoges" id="vicionoges" value="checkbox"> &nbsp;&nbsp;
                                                                <i class="fa fa-check bigicon" style="font-size:16px"> Vicios de Refracci�n </i>
                                                            </td>



                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>

                                                    </table>

                                                    <%if (rp.getVicio_refraccion() == 1) {%>
                                                    <script type="text/javascript">
                                                        document.getElementById('vicionoges').checked = 1;
                                                    </script>
                                                    <%}%>

                                                    <br>
                                                    <table>
                                                        <tr><td><div class=" letra"> Diagnostico:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input style=" height: 80px" type="text"  value="<%=diag%>" rows="3" id="diagnosticonoges" name="diagnosticonoges"   class="form-control" placeholder="Diagnostico no ges"  >


                                                                </div>  
                                                            </td>

                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td><div class=" letra">Fundamento de Diagnostico:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input type="text" style=" height: 80px " value="<%=fundam%>"  rows="3" id="fundamentodediagnosticonoges" name="fundamentodediagnosticonoges"  class="form-control" placeholder="Fundamento de Diagnostico"  >


                                                                </div>  
                                                            </td>

                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td><div class=" letra"> Tratamiento e Indicaci�n:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input type="text" style=" height: 80px " value="<%=tratami%>"  rows="3" id="tratamientoeindicacionnoges"  name="tratamientoeindicacionnoges"  class="form-control" placeholder="Tratamiento e Indicaci�n"  >


                                                                </div>  
                                                            </td>


                                                    </table>

                                                    <br>
                                                    <table  style="border-top:  #619fd8 2px solid; " >
                                                        <tr><td><div class=" letra"> Receta de Lente:</div></td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" style=" height: 40px" id="odenoges" value="<%=od%>" name="odenoges" placeholder=" Ojo Derecho Esfera"> 
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" id="odcnoges" style=" height: 40px" name="odcnoges" value="<%=odc%>" placeholder=" Ojo Derecho Cilindro"> 
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" id="odejnoges" style=" height: 40px" value="<%=ode%>" name="odejnoges" placeholder=" Ojo Derecho Eje"> 
                                                                </div>
                                                            </td>



                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" id="oienoges" style=" height: 40px" name="oienoges" value="<%=oie%>" placeholder=" Ojo Izquierdo Esfera">
                                                                    </td>

                                                                    <td>
                                                                        <div>
                                                                            <input rows="1" cols="22" id="oicnoges" style=" height: 40px" name="oicnoges" value="<%=oic%>" placeholder=" Ojo Izquierdo Cilindro"> 
                                                                        </div>
                                                                    </td>

                                                                    <td>
                                                                        <div>
                                                                            <input rows="1" cols="22" id="oiejnoges" style=" height: 40px" name="oiejnoges" value="<%=oiej%>" placeholder=" Ojo Izquierdo Eje">
                                                                        </div>
                                                                    </td>



                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" id="addnoges" value="<%=add%>" style=" height: 40px" name="addnoges" placeholder="ADD"> 
                                                                </div>
                                                            </td> 
                                                        </tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr><td> &nbsp;</td></tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" id="dplnoges" style=" height: 40px" value="<%=dpl%>" name="dplnoges" placeholder=" Distancia Pupilar Lejos"> 
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div>
                                                                    <input rows="1" cols="22" id="dpcnoges" style=" height: 40px" name="dpcnoges" value="<%=dpc%>" placeholder=" Distancia Pupilar Cerca"> 
                                                                </div>
                                                            </td>




                                                        </tr>
                                                        <tr><td colspan="4"><div class=" letra"> Observaciones:</div></td></tr>
                                                        <tr>
                                                            <td colspan="4"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                                <div class="col-md-8">
                                                                    <input type="text"  rows="3" value="<%=or%>" style=" height: 80px" id="observacionesrecetanoges" name="observacionesrecetanoges"  class="form-control" placeholder="Observaciones">


                                                                </div>  
                                                            </td>

                                                        </tr> 
                                                    </table>
                                                </td>
                                            </tr>
                                        </table> 
                                        <%
                                            if (exg.getIdexcepcion_garantia() != 0) {
                                        %>
                                        <script type="text/javascript">
                                            document.getElementById('excepciondegarantia').checked = 1;
                                            var tengo = document.forms['atencioOft']['excepciondegarantia'].checked;
                                            if (tengo) {
                                                document.getElementById('eg').style.display = 'block';
                                                document.getElementById('k').style.display = 'none';
                                                document.getElementById('nogestabla').style.display = 'none';
                                            } else {
                                                document.getElementById('eg').style.display = 'none';
                                                var valoruno = document.forms['atencioOft']['ges'].value;


                                                if (valoruno == 1) {

                                                    document.getElementById('k').style.display = 'block';
                                                    document.getElementById('nogestabla').style.display = 'none';


                                                } else if (valoruno == 0) {
                                                    document.getElementById('nogestabla').style.display = 'block';
                                                    document.getElementById('k').style.display = 'none';
                                                } else
                                                {
                                                    document.getElementById('nogestabla').style.display = 'none';
                                                    document.getElementById('k').style.display = 'none';

                                                }

                                            }

                                        </script>
                                        <%}%>                            



                                        <table style="border-top:  #619fd8 2px solid; ">

                                            <tr><td colspan="3"><div class=" letra"> Reflejo Fotomotor:</div></td></tr>
                                            <tr>
                                                <td><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input type="text" style=" height: 80px" value="<%=vr.getReflejofotomotor()%>"  rows="1" id="reflejofotomotor" name="reflejofotomotor"  class="form-control" placeholder="RFM">


                                                    </div>  
                                                </td>

                                            </tr> 
                                            <tr><td colspan="3"><div class=" letra"> Rojo Pupilar:</div></td></tr>
                                            <tr>
                                                <td><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input type="text" style=" height: 80px" value="<%=vr.getRojopopular()%>"  rows="1" id="rojopupilar" name="rojopupilar"  class="form-control" placeholder="Rojo Pupilar">


                                                    </div>  
                                                </td>

                                            </tr> 
                                            <tr><td colspan="3"><div class=" letra"> BMC + Polo Posterior:</div></td></tr>
                                            <tr>
                                                <td><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input type="text" style=" height: 80px" rows="3" id="bmcpoloposterior" name="bmcpoloposterior" value="<%=vr.getBmcpoloposterior()%>" class="form-control" placeholder="BMC + Polo Posterior">


                                                    </div>  
                                                </td>

                                            </tr>
                                            <tr><td colspan="3"><div class=" letra"> Indicaciones:</div></td></tr>
                                            <tr>
                                                <td><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input type="text" style=" height: 80px" value="<%=vr.getIndicaciones()%>"   rows="3" id="indicacionesvr" name="indicacionesvr"  class="form-control" placeholder="Indicaciones">


                                                    </div>  
                                                </td>

                                            </tr> 
                                        </table>
                                        <table style="border-top:  #619fd8 2px solid; ">
                                            <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Solicitud de Control</div></td></tr>

                                        </table>
                                        <table style=" width: 100%">

                                            <tr>
                                                <td colspan="6">
                                                    <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-check bigicon" style="font-size:18px">Fecha Control </i></span>
                                                    <div class="col-md-8">
                                                        <input type="text" maxlength="13" size="12" id="fechacontrol" name="fechacontrol" value ="<%=fechaComoCadena%>"><br> </td>
                                                    </div>  
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span> 


                                                    <div class="col-md-8">
                                                        <textarea type="text"  rows="3" id="motivocontrol" name="motivocontrol"  class="form-control" placeholder="Motivo Control"  ><%=motivotexto%> </textarea>


                                                    </div>  
                                                </td>
                                            </tr>
                                        </table>              

                                        <br>

                                    </div>




                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button  type="button" onclick="javascript: document.atencioOft.action = '<%=caco.getLocal()%>atenciontecnologo';
                                                    document.atencioOft.submit()" class="btn btn-primary btn-lg">Grabar</button>
                                            &nbsp;
                                            <button type="submit"    class="btn btn-primary btn-lg">Finalizar</button>
                                            &nbsp;
                                            <button type="button" onclick="javascript: document.atencioOft.action = '<%=caco.getLocal()%>volveraTens';
                                                    document.atencioOft.submit()"   class="btn btn-primary btn-lg"> Volver al TENS</button>
                                            &nbsp;
                                            <button type="button" onclick=" javascript: document.atencioOft.action = '<%=caco.getLocallink()%>Oftalmologia/AtencionClinicaTecnologo2.jsp';
                                                    document.atencioOft.submit()"    class="btn btn-primary btn-lg" >Cancelar</button>
                                        </div>
                                    </div>


                                </form>
                            </div> 

                            <%
                                if (rp.getVicio_refraccion() == 1) {
                            %>
                            <script type="text/javascript">
                                document.getElementById('v').checked = 1;
                            </script>
                            <%}%>
                            <%
                                if (rp.getConfima() == 1) {

                            %>
                            <script type="text/javascript">

                                document.getElementById('confirma').checked = 1;
                                confirma();
                            </script>
                            <%  }
                                if ((rp.getId_registropatologia() != 0) && (rp.getConfima() == 0)) {
                            %>
                            <script type="text/javascript">

                                document.getElementById('confirma2').checked = 1;
                                noconfirma();
                            </script>
                            <%}

                            %>

                            <script>
                                revisar();
                            </script>

                        </fieldset>


                    </div>

                </div>
            </article>
        </section>
        <script>
            $(function () {
                $("#fechacontrol").datepicker();
            });


            function validartratamientocerca() {
                var pre = document.getElementById('tratamientolentesCerca').checked;
                if (pre) {
                    document.getElementById('receta').style.display = 'block';
                } else {
                    document.getElementById('receta').style.display = 'none';
                }
            }

            function validarexcepciondegarantia() {
                var tengo = document.forms['atencioOft']['excepciondegarantia'].checked;
                var rpd = document.forms['atencioOft']['rpd'].checked;
                var rapg = document.forms['atencioOft']['rapg'].checked;
                var pr = document.forms['atencioOft']['pr'].checked;
                var ocdp = document.forms['atencioOft']['ocdp'].checked;
                var observaciongarantia = document.forms['atencioOft']['observaciongarantia'].value;
                if (tengo) {
                    if (!rpd && !rapg && !pr && !ocdp && observaciongarantia == "") {
                        return false;
                    }
                }

            }

            function validartratamientolejos() {
                var pre = document.getElementById('tratamientolentesLejos').checked;
                if (pre) {
                    document.getElementById('receta').style.display = 'block';
                } else {
                    document.getElementById('receta').style.display = 'none';
                }
            }

            function confirma() {

                var pre = document.getElementById('confirma').value;
                if (pre == 'si') {
                    document.getElementById('a').style.display = 'block';
                    document.getElementById('b').style.display = 'none';
                } else {
                    document.getElementById('a').style.display = 'none';
                }
            }

            function noconfirma() {
                var pre = document.getElementById('confirma2').value;
                if (pre == 'no') {
                    document.getElementById('b').style.display = 'block';
                    document.getElementById('a').style.display = 'none';
                    document.getElementById('receta').style.display = 'none';
                } else {
                    document.getElementById('b').style.display = 'none';
                }
            }


            function validarquecoloqueGes() {
                var ges = document.forms['atencioOft']['ges'].value;
                if (ges == "-1") {
                    return false;

                }
            }

            function validarpertinencia() {
                var atencion = document.getElementById('idatencioncita').value;
                var pertinencia = document.getElementById('pertinencia').value;

                if (atencion == 1) {
                    if (pertinencia == -1) {
                        return false;
                    }
                }


            }


            function validarsiesnoGesycolocaViciollenarCampos() {
                var ges = document.forms['atencioOft']['ges'].value;
                if (ges == 0) {
                    var vicionoges = document.forms['atencioOft']['vicionoges'].checked;
                    var diagnostico = document.forms['atencioOft']['diagnosticonoges'].value;
                    var fundamentodediagnosticonoges = document.forms['atencioOft']['fundamentodediagnosticonoges'].value;
                    var tratamientoeindicacionnoges = document.forms['atencioOft']['tratamientoeindicacionnoges'].value;
                    if (vicionoges) {
                        if (diagnostico == "" || fundamentodediagnosticonoges == "" || tratamientoeindicacionnoges == "") {
                            return false;
                        }
                    }
                }
            }


            function validarreceta() {

                var tratamientocerca = document.forms['atencioOft']['tratamientolentesCerca'].checked;
                var tratamientolejos = document.forms['atencioOft']['tratamientolentesLejos'].checked;
                var lejosaddod = document.forms['atencioOft']['odf'].value;
                var lejosesfod = document.forms['atencioOft']['odc'].value;
                var lejoscylod = document.forms['atencioOft']['ode'].value;
                var lejosejeod = document.forms['atencioOft']['oie'].value;
                var lejosaddoi = document.forms['atencioOft']['oic'].value;
                var lejosesfoi = document.forms['atencioOft']['oiej'].value;
                var lejoscyloi = document.forms['atencioOft']['add'].value;


                if (tratamientocerca || tratamientolejos) {

                    if (lejosaddod == "" & lejosesfod == "" & lejoscylod == "" & lejosejeod == "" & lejosaddoi == "" & lejosesfoi == "" & lejoscyloi == "") {
                        return false;
                    }


                }
            }


            /*validacion que se pide por si coloco vicio refraccion debo colocar si o no y escribir el diagnostico*/
            function validarVicioRefraccion() {
                var ges = document.forms['atencioOft']['ges'].value;
                if (ges == 1) {
                    var vicio = document.forms['atencioOft']['v'].checked;
                    var si = document.forms['atencioOft']['confirma'].value;
                    var no = document.forms['atencioOft']['confirma2'].value;

                    var diagnosticopatologia = document.forms['atencioOft']['diagnosticoconges'].value;
                    var diagnostico = document.forms['atencioOft']['diagnosticosiges'].value;
                    var fundamento = document.forms['atencioOft']['fundamentodediagnostico'].value;
                    var tratamiento = document.forms['atencioOft']['tratamientoeindicacion'].value;


                    if (vicio) {
                        if (si == 'si') {

                            if (diagnosticopatologia == "") {
                                return false;
                            }
                        } else if (no == 'no') {

                            if (diagnostico == "" || fundamento == "" || tratamiento == "") {
                                return false;
                            }
                        }
                    }


                }

            }

            function validarquedebecolocarTratamientoOjo() {
                var si = document.forms['atencioOft']['si'].checked;
                var tratamientolentesopticos = document.forms['atencioOft']['tratamientolentes'].checked;
                if (si) {
                    if (!tratamientolentesopticos) {
                        return false;
                    }
                }
            }

            function camposObligatorios() {
                var ffm = document.forms['atencioOft']['reflejofotomotor'].value;
                var rp = document.forms['atencioOft']['rojopupilar'].value;
                var bmcpupilar = document.forms['atencioOft']['bmcpoloposterior'].value;
                var indicaciones = document.forms['atencioOft']['indicacionesvr'].value;

                if (ffm == "" & rp == "" & bmcpupilar == "" & indicaciones == "") {
                    return false;
                }
            }


            function validarcontrol() {

                var fechacontrol = document.forms["atencioOft"]["fechacontrol"].value;
                var motivocontrol = document.forms["atencioOft"]["motivocontrol"].value;

                if (fechacontrol != "") {
                    if (motivocontrol == "") {
                        return false;
                    }
                }
            }


            function validar() {

                if (camposObligatorios() == false) {
                    Alert.render("Debe completar los datos de Reflejo Fotomotor, Rojo Pupilar ,BMC y Indicaciones ");
                    return false;
                }

                if (validarquecoloqueGes() == false) {
                    Alert.render("Debe indicar si es GES o NOGES!!  Debe Completar los Datos para continuar");
                    return false;
                }

                if (validarVicioRefraccion() == false) {
                    Alert.render("Debe Confirmar el Vicio Refraccion Junto a un Diagnostico!!  Debe Completar los Datos para continuar");
                    return false;
                }

                if (validarsiesnoGesycolocaViciollenarCampos() == false) {
                    Alert.render("Si Confirma Vicio Refraccion debe llenar los campos de Diagnostico, Fundamento y Tratameitno!!Debe Completar los Datos para continuar ")
                    return false;
                }
                if (validarexcepciondegarantia() == false) {
                    Alert.render("Si Confirma Excepcion de Garantia,indique alguna causa y observacion para continuar ")
                    return false;
                }

                if (validarreceta() == false) {

                    Alert.render("Si Coloco que tiene Tratamiento con Lentes �pticos !! Debe Completar los Datos de Receta Lentes �pticos");
                    return false;
                }

                if (validarpertinencia() == false) {
                    Alert.render("Como su atencion es Nueva debe indicar si �Es pertinente la derivaci�n a su atenci�n? !! Debe Completar los Datos para Continuar");
                    return false;
                }

                if (validarcontrol() == false) {
                    Alert.render("Debe Indicar el Motivo del Control!! Complete los datos para continuar");
                    return false;
                }

            }

            function revisar() {
                var valoruno = document.forms['atencioOft']['ges'].value;
                var tengo = document.forms['atencioOft']['excepciondegarantia'].checked;

                if (valoruno == 1 && !tengo) {

                    document.getElementById('k').style.display = 'block';
                    document.getElementById('nogestabla').style.display = 'none';


                } else if (valoruno == 0 && !tengo) {
                    document.getElementById('nogestabla').style.display = 'block';
                    document.getElementById('k').style.display = 'none';
                } else {
                    document.getElementById('nogestabla').style.display = 'none';
                    document.getElementById('k').style.display = 'none';
                }
            }



            function openCity(evt, cityName) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }
                document.getElementById(cityName).style.display = "block";
                evt.currentTarget.className += " active";
            }

            // Get the element with id="defaultOpen" and click on it
            document.getElementById("defaultOpen").click();
        </script>

    </body>
</html>