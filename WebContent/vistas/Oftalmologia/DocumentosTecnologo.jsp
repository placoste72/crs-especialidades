<%-- 
    Document   : DocumentosTecnologo
    Created on : 14-09-2017, 10:26:35
    Author     : a
--%>

<%@page import="Modelos.excepcion_garantia"%>
<%@page import="Modelos.registrodepatologia"%>
<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>


        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>
        <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">-->

        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>

    </head>

    <% controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        String atencion = request.getParameter("atencion");
        boolean tengoreceta = caco.tengorecetaIdAtencion(Integer.parseInt(atencion),0);
        excepcion_garantia exg = caco.buscarGarantiaporIddeAtencion(Integer.parseInt(atencion),1);
        registrodepatologia rpg = caco.BuscarRegistrodePatologia(Integer.parseInt(atencion));
    %>
    <div class="container"> 
        <form name="buscarsic" class="form-horizontal" id="buscarsic" action='' method="post"  >

            <br>
            <fieldset>
                <legend class="text-center header">Documentos a Imprimir</legend>
                <br>
                <br>
                <table class="table table-striped" style="width: 100%"  >

                    <thead>
                    <th><p  class="letra" > Informe del Prestación Realizada <br> IPR <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=caco.getLocal()%>InformedePrestacionRealizadaOftalmologia?idatencion=<%=atencion%>', 'Informe de Prestacion Realizada  -  IPR', 'width=600,height=450')"/></p></th>

                    <%if (tengoreceta == true) {%>
                    <th><p class="letra"> Receta de Lentes<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=caco.getLocal()%>RecetadeLenteOpticos?idatencion=<%=atencion%>&tipo=0', 'Receta de Lente', 'width=600,height=450')"/></p> </th>


                    <%}
                            if (rpg.getConfima() == 1) {%>
                    <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=caco.getLocal()%>ConstanciaGes2?idatencion=<%=atencion%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                   
                    
                 

                    <%}
                        if (rpg.getVicio_refraccion() == 1) {

                    %>
                    <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=caco.getLocal()%>infromeProcesoDiagnosticoT?idatencion=<%=atencion%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>


                    <%}
                        if (exg.getIdexcepcion_garantia() != 0) {

                    %>
                    <th><p  class="letra" > Excepción de Garantia <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=caco.getLocal()%>ExcepciondeGarantia?idatencion=<%=atencion%>&tipo=1', 'Excepcion de Garantia', 'width=600,height=450')"/></p></th>


                    <%  }%>
                    <th></th>
                    </tr>


                    </thead>
                </table>  
            </fieldset>
        </form>
        <button type="button" style="margin-left:  500px" class="btn btn-primary" onclick="javascript : location.href = '<%=caco.getLocallink()%>Oftalmologia/AtencionClinicaTecnologo2.jsp';">Concluir proceso de atención <br>y volver a listado de pacientes por atender</button>
    </div> 
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
