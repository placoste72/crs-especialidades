<%-- 
    Document   : ActualizarMotivoConsulta
    Created on : 31-01-2018, 8:48:27
    Author     : a
--%>

<%@page import="Modelos.rol"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controlador.General"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.motivo_consulta"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../comunes/headerwindows.jsp"/>
<script>
    //para elegir

    function pone() {

        if (!isSelected("#fromSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
        $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
        return false;

    }
    function saca() {

        //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
        if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
        $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

    }

    function todos() {
        selectAll('#fromSelectBox');
        pone();
    }

    function ninguno() {
        selectAll('#toSelectBox');
        saca();
    }

    function selecciona_todos() {
        selectAll('#toSelectBox');
    }

    //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
    function isSelected(thisObj) {
        if (!$(thisObj + " option:selected").length) {
            Alert.render("Debe Elejir una opcion !!Debe Completar los Datos para Continuar");
            return 0;
        }
        return 1;
    }

    //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
    function noOptions(thisObj) {
        if (!$(thisObj + " option").length) {
            // alert("There are no options to select/move");
            return 0;
        }
        return 1;
    }

    //Below function is to de-select all items if any of the item(s) are selected
    function clearAll(thisObj) {
        $('#' + thisObj).each(function () {
            $(this).find('option:selected').removeAttr("selected");
        });
    }//function close

//Below function is to select all items
    function selectAll(thisObj) {
        if (!noOptions("#" + thisObj)) {
            return;
        }
        $('#' + thisObj + ' option').each(function () {
            $(this).attr("selected", "selected");
        });
    }

    function validateIndustry()
    {

        if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
            return false;

        }

    }

    function validar() {
        var x = document.forms["motivoconsulta"]["nombremotivo"].value;
        var y = document.forms["motivoconsulta"]["atencion"].value
        if (x == null || x == "" || y == null || y == "-1" || y == -1) {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }
        if (validateIndustry() == false) {
            Alert.render("Debe seleccionar por lo menos una opcion!!Debe Completar los Datos para Continuar");
            return false;
        }
    }


</script>

<html>
    <head>
        <title>Actualizar Motivo Consulta</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        
    </head>

    <body>

        <%            
            
            String mensaje = "";

            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Actualizar Motivo de la Consulta">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>

        <%
            }
            controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
            String idmotivo = request.getParameter("cod");
            motivo_consulta mc = caco.buscarMotivodeConsultaporId(Integer.parseInt(idmotivo));
            General g = new General();
            ArrayList lista_opciones = caco.buscarlosrolesQuenoestanenunMotivodeConsulta(Integer.parseInt(idmotivo));
            Iterator it = lista_opciones.iterator();

            ArrayList lista_vacia = caco.buscarlosrolesdeunMotivodeConsulta(Integer.parseInt(idmotivo));
            Iterator it2 = lista_vacia.iterator();
        %>



        <form name="motivoconsulta" id="motivoconsulta"  class="form-horizontal"  action='<%=caco.getLocal()%>ActualizarMotivodeConsulta' method="post" onsubmit="return selecciona_todos()" >

            <fieldset>

                <legend class="text-center header">Actualizar Motivo Consulta</legend>
                <input type="text" id="idmotivo" name="idmotivo" value="<%=idmotivo%>" hidden>

                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>
                    <div class="col-md-8">
                        <input type="text" id="nombremotivo" name="nombremotivo" value="<%= mc.getNombremotivo()%>" class="form-control" placeholder="Motivo Consulta"  required oninvalid="setCustomValidity('El campo Motivo de Consulta es obligatorio')" oninput="setCustomValidity('')">
                    </div>
                </div>

                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-hospital-o bigicon"></i></span>


                    <div class="col-md-8" >
                        <select class="form-control" id="atencion" name="atencion" title="Atencion">
                            <option value="-1">Elegir Atencion
                                <%

                                    for (lugar atencion : g.buscarAtencionparaAgendarCupos()) {

                                        if (atencion.getId_lugar() == mc.getAtencion_cita()) {%>
                            <option selected value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                                    } else {%>
                            <option value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                                    }

                                }
                                %> 
                        </select>
                    </div>
                </div>

                <div  style=" padding-left: 240px">
                    <h4 class="letra2"> Eliga los Perfiles del Usuario </h4>
                    <div class="col-md-8 text-center">

                        <table class="table">
                            <tr>
                                <td>
                                    <select  id="fromSelectBox" name="roles2"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px;  width: 260px ">

                                        <%
                                            while (it.hasNext()) {
                                                rol r = (rol) it.next();
                                                out.write("   <option value='" + r.getIdRol() + "' >" + r.getNombreRol() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>
                                <td style="width:5px">
                                    <span class='cell' >
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon " onclick="pone();
                                                        return false"></i>
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca();
                                                        return false"></i>

                                    </span>

                                </td>       
                                <td>
                                    <select  id="toSelectBox" name="roles"  size="10" multiple="multiple" style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px ">

                                        <%
                                            out.write("   <option value= -1  > Seleccione una Opcion</option>");
                                            while (it2.hasNext()) {
                                                rol r2 = (rol) it2.next();
                                                out.write("   <option value='" + r2.getIdRol() + "' >" + r2.getNombreRol() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>    

                            </tr> 

                        </table>


                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-lg">Actualizar</button>

                    <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                </div>
            </div>
        </form>

    </body>
</html>
