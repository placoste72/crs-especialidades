<%-- 
    Document   : MotivoConsulta
    Created on : 19-10-2017, 9:03:45
    Author     : a
--%>

<%@page import="Modelos.motivo_consulta"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Modelos.rol"%>
<%@page import="Controlador.controlador_usuario"%>

<%@page import="Modelos.opciones"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>

<%@page import="Controlador.General"%>
<%@page import="Modelos.lugar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<script>
    //para elegir

    function pone() {

        if (!isSelected("#fromSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
        $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
        return false;

    }
    function saca() {

        //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
        if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
            return;
        }
        //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
        $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

    }

    function todos() {
        selectAll('#fromSelectBox');
        pone();
    }

    function ninguno() {
        selectAll('#toSelectBox');
        saca();
    }

    function selecciona_todos() {
        selectAll('#toSelectBox');
    }

    //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
    function isSelected(thisObj) {
        if (!$(thisObj + " option:selected").length) {
            Alert.render("Debe Elejir una opcion !!Debe Completar los Datos para Continuar");
            return 0;
        }
        return 1;
    }

    //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
    function noOptions(thisObj) {
        if (!$(thisObj + " option").length) {
            // alert("There are no options to select/move");
            return 0;
        }
        return 1;
    }

    //Below function is to de-select all items if any of the item(s) are selected
    function clearAll(thisObj) {
        $('#' + thisObj).each(function () {
            $(this).find('option:selected').removeAttr("selected");
        });
    }//function close

//Below function is to select all items
    function selectAll(thisObj) {
        if (!noOptions("#" + thisObj)) {
            return;
        }
        $('#' + thisObj + ' option').each(function () {
            $(this).attr("selected", "selected");
        });
    }

    function validateIndustry()
    {

        if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
            return false;

        }

    }

    function validar() {
        var x = document.forms["motivoconsulta"]["nombremotivo"].value;
        var y = document.forms["motivoconsulta"]["atencion"].value
        if (x == null || x == "" || y == null || y == "-1" || y == -1) {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }
        if (validateIndustry() == false) {
            Alert.render("Debe seleccionar por lo menos una opcion!!Debe Completar los Datos para Continuar");
            return false;
        }
    }


</script>
<%

    int rol = 0;
    controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
    controlador_usuario cu = new controlador_usuario();
    ArrayList lista_opciones = cu.buscarListaRol();
    Iterator it = lista_opciones.iterator();

    ArrayList lista_vacia = cu.buscarListaRolconUsuario(-1);
    Iterator it2 = lista_vacia.iterator();

    General g = new General();
    int idatencion = 0;
    if (request.getParameter("men") != null) {
        String mensaje = "";
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Registrar Motivo Consulta">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>


<%
    }

%>
<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>
<div class="tab-content">

    <div class="container"> 
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#crear">Crear</a></li>
            <li><a data-toggle="tab" href="#lista">Listado</a></li>

        </ul>
        <div class="tab-content" style="text-align:center;">
            <div id="crear" class="tab-pane fade in active" style="text-align:center;">





                <form name="motivoconsulta" id="motivoconsulta"  class="form-horizontal"  action='<%=g.getLocal()%>registrarMotivoConsulta' method="post" onsubmit="return validar()" >

                    <fieldset>

                        <legend class="text-center header">Registra Motivo Consulta</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user-md bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="text" id="nombremotivo" name="nombremotivo" class="form-control" placeholder="Motivo Consulta"  required oninvalid="setCustomValidity('El campo Motivo de Consulta es obligatorio')" oninput="setCustomValidity('')">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-hospital-o bigicon"></i></span>


                            <div class="col-md-8" >
                                <select class="form-control" id="atencion" name="atencion" title="Atencion">
                                    <option value="-1">Elegir Atencion
                                        <%                                                        for (lugar atencion : g.buscarAtencionparaAgendarCupos()) {

                                                if (atencion.getId_lugar() == idatencion) {%>
                                    <option selected value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                                    } else {%>
                                    <option value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                                            }

                                        }
                                        %> 
                                </select>
                            </div>
                        </div>

                        <div  style=" padding-left: 240px">
                            <h4 class="letra2"> Eliga los Perfiles del Usuario </h4>
                            <div class="col-md-8 text-center">

                                <table class="table">
                                    <tr>
                                        <td>
                                            <select  id="fromSelectBox" name="roles2"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px;  width: 260px ">

                                                <%
                                                    while (it.hasNext()) {
                                                        rol r = (rol) it.next();
                                                        out.write("   <option value='" + r.getIdRol() + "' >" + r.getNombreRol() + "</option>");
                                                    }
                                                %>

                                            </select>
                                        </td>
                                        <td style="width:5px">
                                            <span class='cell' >
                                                <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon " onclick="pone();
                                                        return false"></i>
                                                <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca();
                                                        return false"></i>

                                            </span>

                                        </td>       
                                        <td>
                                            <select  id="toSelectBox" name="roles"  size="10" multiple="multiple" style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px ">

                                                <%
                                                    out.write("   <option value= -1  > Seleccione una Opcion</option>");
                                                    while (it2.hasNext()) {
                                                        rol r2 = (rol) it2.next();
                                                        out.write("   <option value='" + r2.getIdRol() + "' >" + r2.getNombreRol() + "</option>");
                                                    }
                                                %>

                                            </select>
                                        </td>    

                                    </tr> 

                                </table>


                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Crear</button>

                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="lista" class="tab-pane fade">
                <fieldset>
                    <legend class="text-center header">Lista de Motivo de Consultas</legend>

                    <table class="table table-striped" style="width: 100%">
                        <thead>
                            <tr >
                                <th>Nombre Motivo</th>
                                <th>Atencion </th>
                                <th>Actualizar</th>
                                <th>Eliminar</th>

                            </tr>
                        </thead>
                        <%
                            for (motivo_consulta temp : caco.buscarlosmotivoConsultasActivos()) {
                        %>

                        <tr>



                            <td><%=temp.getNombremotivo()%></td>
                            <td><%=temp.getNombreatencion()%></td>



                            <td align="center">
                                <a href="ActualizarMotivoConsulta.jsp?cod=<%=temp.getId_motivo()%>" onClick="return popup2(this, 'notes')" class="button3">
                                    Actualizar
                                </a>


                            </td>
                            <td align="center">

                                <a href='<%=caco.getLocal()%>SEliminarMotivoConsulta?cod=<%=temp.getId_motivo()%>'  class="button2">
                                    Eliminar  
                                </a>
                            </td>


                        </tr>
                        <%}%>
                    </table>
                </fieldset>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../comunes/Footer.jsp"/>
