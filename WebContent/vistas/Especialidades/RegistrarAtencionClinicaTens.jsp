<%-- 
    Document   : RegistrarAtencionClinicaOftalmologia
    Created on : 13-04-2017, 14:59:55
    Author     : a
--%>

<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Modelos.atencion_clinica_oftalmologia"%>
<%@page import="Modelos.lugar"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.cita"%>


<!DOCTYPE html>
<html>
    <head>

        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>
        <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">-->

        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>

        <div>

            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div>

            <%            controlador_especialidad ce = new controlador_especialidad();

                String cita = request.getParameter("idcita");
                controlador_cita cc = new controlador_cita();
                cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));%>
            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div>
            <div class="container">
                <form id="entrevistaproperatorio" name="entrevistaproperatorio" action="<%=cc.getLocal()%>/registrarentrevistaTens" >
                    <div class="tab-content">
                        <fieldset>
                            <legend class="text-center header">Entrevista Preoperatoria</legend>

                            <table class="table table-bordered" style="width: 100%">
                                <thead><tr>
                                        <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">DATOS DEL PACIENTE</th>
                                        <th colspan="5" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">DATOS DE CITACI�N</th>
                                    </tr>
                                    <tr>
                                        <th>RUT</th>
                                        <th>Paciente</th>
                                        <th>Edad</th>
                                        <th>Sexo</th>
                                        <th>Especialidad</th>
                                        <th>Tipo de Cita</th>
                                        <th>Hip�tesis Diagn�stica</th>
                                        <th>Profesional</th>
                                        <th>Fecha y Hora</th>

                                    </tr>

                                </thead>

                                <tr>
                                    <td><%= c.getMotivo_cancela()%></td>
                                    <td><%= c.getRut_paciente()%></td>
                                    <td><%= c.getTemporales2()%></td>
                                    <td><%= c.getTemporales1()%></td>
                                    <td><%= c.getTemporales()%> </td>
                                    <td><%= c.getTemporales3()%>  </td>
                                    <td><%= c.getTemporales4()%></td>
                                    <td><%= c.getRut_doctor()%></td>
                                    <td><%= c.getMotivo()%></td>

                                </tr>
                            </table>
                            <BR>

                            <table style="border-top:  #619fd8 2px solid; " >
                                <tr><td> &nbsp;</td></tr>
                                <input id="cita" name="cita" value="<%=cita%>" hidden>
                                
                                <tr><td> &nbsp;</td></tr>
                            </table>
                            <table>
                                <tr>
                                    <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 16px" >Peso:</i></span>



                                        <div class="col-md-6" id="detalle" >
                                            <input type="text" required  id="peso" name="peso" onchange=" javascritp:

                                                            var peso, altura, imc;

                                                    peso = document.getElementById('peso').value;
                                                    altura = document.getElementById('talla').value;

                                                    if (peso != '' && altura != '') {

                                                        altura = parseFloat(altura);

                                                        imc = parseFloat(peso / (altura * altura));
                                                        document.getElementById('imc').value = imc.toFixed(2);
                                                    }" oninvalid="setCustomValidity('El campo Peso es obligatorio')" oninput="setCustomValidity('')"  class="form-control" placeholder="Peso KG"  >


                                        </div>  
                                    </td>
                                    <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 16px" >Talla:</i></span>



                                        <div class="col-md-6" id="detalle">
                                            <input type="text"  required oninvalid="setCustomValidity('El campo Talla es obligatorio')" oninput="setCustomValidity('')"  id="talla" name="talla"  class="form-control" placeholder="Talla MT" onchange=" javascritp:

                                                            var peso, altura, imc;

                                                    peso = document.getElementById('peso').value;
                                                    altura = document.getElementById('talla').value;

                                                    if (peso != '' && altura != '') {

                                                        altura = parseFloat(altura);

                                                        imc = parseFloat(peso / (altura * altura));
                                                        document.getElementById('imc').value = imc.toFixed(2);
                                                    }"  >


                                        </div>  
                                    </td>
                                    <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 16px" >IMC:</i></span>



                                        <div class="col-md-6" id="detalle" >
                                            <input type="text"  required  id="imc" name="imc" oninvalid="setCustomValidity('El campo IMC es obligatorio')" oninput="setCustomValidity('')"  class="form-control" placeholder="IMC"  >


                                        </div>  
                                    </td>

                                </tr>
                                <tr><td> &nbsp;</td></tr>
                                <tr>
                                    <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 16px" >PA:</i></span>



                                        <div class="col-md-6" id="detalle" >
                                            <input type="text"  required oninvalid="setCustomValidity('El campo PA es obligatorio')" oninput="setCustomValidity('')"  id="pa" name="pa"  class="form-control" placeholder="PA"  >


                                        </div>  
                                    </td>
                                    <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 16px" >F. CARD�ACA:</i></span>



                                        <div class="col-md-6" id="detalle" >
                                            <input type="text" required  oninvalid="setCustomValidity('El campo FIA CARDIACA es obligatorio')" oninput="setCustomValidity('')" id="cardiaca" name="cardiaca"   class="form-control" placeholder="F. CARD�ACA"  >


                                        </div>  
                                    </td>
                                    <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 16px" >Sat02:</i></span>



                                        <div class="col-md-6" id="detalle" >
                                            <input type="text"  required oninvalid="setCustomValidity('El campo Sat02 es obligatorio')" oninput="setCustomValidity('')" id="sat" name="sat"  class="form-control" placeholder="Sat02"  >


                                        </div>  
                                    </td>

                                </tr>

                            </table>
                            <br>
                            <br>

                            <table>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <button type="submit" class="btn btn-primary btn-lg">Guardar </button>

                                                <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                            </div>
                                        </div> 
                                    </td>
                                </tr>


                            </table>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>

    </body>
</html>
