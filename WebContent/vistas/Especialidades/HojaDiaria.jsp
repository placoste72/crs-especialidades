<%-- 
    Document   : HojaDiaria
    Created on : 30-04-2018, 8:37:25
    Author     : a
--%>

<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<script src="../../public/js/validarhojadiaria.js" type="text/javascript"></script>
<%
    String cita = request.getParameter("cita");
    controlador_cita cc = new controlador_cita();
    cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
    controlador_especialidad ce = new controlador_especialidad();
    int idespecialidad = 0;
    idespecialidad = c.getId_patologia();

    Integer[] idpertinencia = {1, 0};
    String[] nombrepertinencia = {"SI", "NO"};
%>

<style>
    table{
        font-size: 10px;
    }
    th, td {
        padding: 0px;
    }

</style>
<section  class="section">
    <nav class="nav1 ">
        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos del Paciente</th>
                </tr>
                <tr>
                    <th>RUT</th>
                    <th>Paciente</th>
                    <th>Edad</th>
                    <th>Sexo</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getMotivo_cancela()%></td>
                <td><%= c.getRut_paciente()%></td>
                <td><%= c.getTemporales2()%></td>
                <td><%= c.getTemporales1()%></td>


            </tr>
        </table>

        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="6" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos de la Citaci�n</th>
                </tr>
                <tr>
                    <th>Especialidad</th>
                    <th>Tipo de Cita</th>
                    <th>Diagn�stica</th>


                    <th>Fecha y Hora</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getTemporales()%> </td>
                <td><%= c.getTemporales3()%>  </td>
                <td><%= c.getTemporales4()%></td>


                <td><%= c.getMotivo()%></td>


            </tr>
        </table>
        <p class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; font-size: 10px; text-align: left">Datos otras atenciones</p>
        <table class="table-striped" >
            <tr>
                <th>Fecha</th>
                <th>Atencion</th>
                <th>Especialidad</th>
                <th>Profesional</th>
                <th>Detalle</th>
            </tr>
            <%for (atencion_clinica_tecnologo at : cc.BuscarAtencionesdeunPaciente(c.getMotivo_cancela())) {

            %>
            <tr>
                <td><%=at.getAuxiliarcinco()%></td>
                <td><%=at.getAuxiliarseis()%></td>
                <td><%=at.getAuxiliardos()%></td>
                <td><%=at.getAuxiliartres()%></td>
                <%if (at.getAuxiliarcuatro() == 1) {
                %>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 2) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 3) {%>
                <td><a onClick="window.open('<%=cc.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 4) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 5) {%>
                <td> </td>
                <%} else if (at.getAuxiliarcuatro() == 6) {%>
                <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"   >

                        Protocolo  </a> 

                    <%int ide = cc.idepicrisis(at.getId_atencion_tecnologo());%>
                    <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    >

                        Epicrisis  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 7) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 8) {%>
                <td><a  onClick="window.open('<%=cc.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenolog�a', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 9) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%}%>
            </tr>
            <%

                }%>

        </table>

    </nav>
    <article class="article">

        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <div class="container">
            <form id="hojadiaria" name="hojadiaria" action="<%=cc.getLocal()%>registrarhojadiaria" method="post" onsubmit="return validar()">
                <div class="tab-content">
                    
                        <legend class="text-center header">Estadistica Diaria</legend>

                        <table style="border-top:  #619fd8 2px solid; ">

                            <%if ((c.getId_tipoatencion() == 1) && (idespecialidad != 69)) {%>  
                            <tr>
                                <td colspan="2">
                                    <span class="col-md-8   col-md-offset-4 "><i class="fa fa-edit bigicon"  style=" font-size: 20px" >�Es pertinente la derivaci�n a su atenci�n?</i></span>
                                </td>
                                <td>
                                    <div class="col-md-8">
                                        <select id="pertinencia" name="pertinencia" > 


                                            <option selected  value="-1"> Seleccionar </option>

                                            <% for (int i = 0; i < idpertinencia.length; ++i) {
                                            %>

                                            <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                <%
                                                    }
                                                %>

                                        </select>  
                                    </div>
                                </td>
                            </tr>
                            <%}%>
                            <input id="especialidad" name="especialidad" value="<%=idespecialidad%>" hidden >
                            <input id="idatencioncita" name="idatencioncita" value="<%=c.getId_tipoatencion()%>" hidden>
                            <br>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td > &nbsp;</td>
                                <td colspan="3"><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon"  style=" font-size: 16px" >Caso GES:</i></span>

                                    <div class="col-md-8">
                                        <select id="ges" name="ges" > 
                                            <option selected value="0"> GES </option>

                                            <option value="1"> Si </option>
                                            <option value="2"> No </option>

                                        </select>  
                                    </div>
                                </td>
                                <td > &nbsp;</td>
                                <td colspan="3"><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon"  style=" font-size: 16px" >Desea Constancia GES:</i></span>

                                    <div class="col-md-8">
                                        <select id="constanciages" name="constanciages" > 
                                            <option selected value="0"> Constancia </option>

                                            <option value="1"> Si </option>
                                            <option value="2"> No </option>

                                        </select>  
                                    </div>
                                </td></tr>
                            <br>
                            <tr><td style="width: 30px"><div class=" letra" style=" font-size: 20px"> Diagnosticos </div></td></tr>
                            <tr>
                                <td > &nbsp;</td>
                                <td>
                                    <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {%>

                                    <input type="checkbox"  name="diagnostico<%=lu.getId_atencion()%>" id="diagnostico<%=lu.getId_atencion()%>" onchange="javascript: var numero = <%=lu.getId_atencion()%>;
                                            validarAgudezavisual(numero)" value="checkbox"><%=lu.getNombre()%>

                                    <br>

                                    <% }%>  
                                </td>




                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon"></i></span> 
                                </td>
                                <td colspan="3">


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="otrodiagnostico" name="otrodiagnostico"  class="form-control" placeholder="Otro Diagnostico"  ></textarea>


                                    </div>  
                                </td>

                            </tr>  
                        </table>
                        <br>

                        <table  id="agudeza" name="agudeza" style=" display:none; width: 100%" >
                            <tr><td colspan="3" >
                                    <div class=" letra " style=" font-size: 20px">Datos para IPD de Catarata.</div>
                                </td>
                                <td colspan="4">
                                    <div class="col-md-24">
                                        <textarea type="text"  rows="3" id="fundamentodeldiagnostico" name="fundamentodeldiagnostico"  class="form-control" placeholder="Fundamento del Diagnostico"  >Ex. Clinico AV</textarea>

                                    </div>

                                </td>
                                <td colspan="4">
                                    <div class="col-md-24">
                                        <textarea type="text"  rows="3" id="tratamientoeindicaciones" name="tratamientoeindicaciones"  class="form-control" placeholder="Tratamiento e Indicaciones"  >Cirugia de Cataratas de ojo </textarea>
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <table style="border-top:  #619fd8 2px solid; ">
                            <br>
                            <tr><td> &nbsp;</td></tr>
                            <br>
                            <tr><td style="width: 30px"><div class=" letra " style=" font-size: 20px">Procedimiento</div></td></tr>
                            <tr>
                                <td > &nbsp;</td>
                                <td>
                                    <%for (lugar lu : ce.buscarlosProcedimientosporEspecialidad(idespecialidad)) {%>

                                    <input type="checkbox" id="procedimiento<%=lu.getId_atencion()%>" name="procedimiento<%=lu.getId_atencion()%>" value="checkbox"><%=lu.getNombre()%>

                                    <br>

                                    <% }%>  
                                </td>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon"></i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="otroprocedimiento" name="otroprocedimiento"  class="form-control" placeholder="Otro Procedimiento"  ></textarea>


                                    </div>  
                                </td>

                            </tr>  
                        </table>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <br>
                            <tr><td> &nbsp;</td></tr>
                            <br>
                            <tr><td style="width: 30px"><div class=" letra " style=" font-size: 20px">Examenes</div></td></tr>
                            <tr>
                                <td > &nbsp;</td>
                                <td>
                                    <%for (lugar lu : ce.buscarlosExamenesporEspecialidad(idespecialidad)) {%>

                                    <input type="checkbox" id="examenes<%=lu.getId_atencion()%>" name="examenes<%=lu.getId_atencion()%>" value="checkbox"><%=lu.getNombre()%>

                                    <br>

                                    <% }%>  
                                </td>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon"></i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="otroexamene" name="otroexamene"  class="form-control" placeholder="Otro Examenes"  ></textarea>


                                    </div>  
                                </td>

                            </tr>  
                        </table>
                        <table style="border-top:  #619fd8 2px solid; " >
                            <tr><td> &nbsp;</td></tr>
                            <input id="cita" name="cita" value="<%=cita%>" hidden>
                            <tr>
                                <td style="width: 300px"><span class="col-md-3   col-md-offset-1 "><i class="fa fa-chevron-down bigicon"  style=" font-size: 16px" >Genera SIC:</i></span>

                                    <div class="col-md-8">
                                        <select id="sic" name="sic" onchange="javascript:

                                                        var pre = document.getElementById('sic').value;
                                                if (pre == 1) {


                                                    document.getElementById('detalle').style.display = 'block';
                                                } else
                                                    document.getElementById('detalle').style.display = 'none';" > 
                                            <option selected value="0" > SIC </option>

                                            <option value="1"> Si </option>
                                            <option value="2"> No </option>

                                        </select>  
                                    </div>

                                </td>
                                <td >
                                    <div class="col-md-12" id="detalle" style=" display: none ">
                                        <table>
                                            <tr>
                                                <td style="width: 4px">
                                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-chevron-down bigicon" style=" font-size: 16px">Diagnostico por el que Derivo</i></span> 

                                                </td>
                                                <td>


                                                    <select name="diagnosticosic" id="diagnosticosic" class="form-control" onchange="javascript:

                                                                    var diagnostico = document.getElementById('diagnosticosic').value;
                                                            if (diagnostico == -1) {


                                                                document.getElementById('otrodiagnosticosicmuestro').style.display = 'block';
                                                            } else
                                                                document.getElementById('otrodiagnosticosicmuestro').style.display = 'none';" >
                                                        <option  value="0" selected>Diagnostico
                                                        <option  value="-1" >Otro
                                                            <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {%>
                                                        <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                            <% }%>  
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="col-md-12" id="otrodiagnosticosicmuestro" style=" display: none">
                                                        <textarea type="text"  rows="1" id="otrodiagnosticosic" name="otrodiagnosticosic"  class="form-control" placeholder="Otro Diagnostico"  ></textarea>


                                                    </div>  

                                                </td>
                                                <td style="width: 4px">
                                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon"></i></span> 

                                                </td>
                                                <td>
                                                    <textarea type="text"  rows="3" id="detalledederivacion" name="detalledederivacion"  class="form-control" placeholder="Detalle de Derivaci�n"  ></textarea>

                                                </td>


                                            </tr>
                                        </table>
                                    </div>
                                </td>


                            </tr>
                        </table>
                        <table>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td style="width: 300px"  ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 16px" >Destino Paciente:</i></span>

                                    <div class="col-md-8">
                                        <select id="destinopaciente" name="destinopaciente" > 
                                            <option selected value="0"> Destino </option>
                                            <% for (lugar l : ce.BuscarlosDestinoPacientes()) {%>
                                            <option value="<%=l.getId_lugar()%>"> <%=l.getNombre()%> </option>
                                            <%}%>
                                        </select>  
                                    </div>
                                </td>
                                <td style="width: 4px">
                                    <span class="col-md-2 col-md-offset-2"><i class="fa fa-edit bigicon"></i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="otrodestino" name="otrodestino"  class="form-control" placeholder="Otro Destino"  ></textarea>


                                    </div>  
                                </td>
                            </tr>
                        </table>


                        <%
                            boolean tengoindicacion = ce.buscarsitengoindicaciones(idespecialidad);
                            if (tengoindicacion == true) {

                        %>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <br>
                            <tr><td> &nbsp;</td></tr>
                            <br>
                            <tr><td style="width: 30px"><div class=" letra " style=" font-size: 20px">Indicaciones</div></td></tr>
                            <tr>
                                <td > &nbsp;</td>
                                <td>
                                    <%for (lugar lu : ce.buscarlasindicacionesporEspecialidad(idespecialidad)) {

                                            if (lu.getId_lugar() != 0) {

                                    %>


                                    <input type="checkbox" id="indicaciones<%=lu.getId_atencion()%>" name="indicaciones<%=lu.getId_atencion()%>" value="checkbox"><%=lu.getNombre()%>
                                    <input  style=" width: 120px" type="text" id="cantidad<%=lu.getId_atencion()%>" name="cantidad<%=lu.getId_atencion()%>" class="form-control" placeholder="Cantidad" >

                                    <br>

                                    <%}
                                        }%>  
                                </td>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon"></i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="otroindicaciones" name="otroindicaciones"  class="form-control" placeholder="Otra Indicacion"  ></textarea>


                                    </div>  
                                </td>
                            </tr>

                        </table>

                        <%}%>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td colspan="6">
                                    <p class="letra" style=" font-size: 12px"> Quiere Solicitar</br> Pabell�n Quirurgico</p>
                                    <input type="checkbox" id="solicituddepabellon" name="solicituddepabellon" value="checkbox"
                                           onchange="javascritp:

                                                           var pre = document.getElementById('solicituddepabellon').checked;
                                                   if (pre) {


                                                       document.getElementById('numeros').style.display = 'block';
                                                   } else
                                                       document.getElementById('numeros').style.display = 'none';">   

                                </td>


                            </tr>  
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <div id="numeros" name="numeros" style=" display: none">
                                        <table style=" width: 320px; align-content: center" >
                                            <tr>

                                                <td style="width: 10px">
                                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon">Cuantas Solicitudes?</i></span> 
                                                </td>
                                                <td width="80px">
                                                    <input id="numerodesolicitudes" size="2" placeholder="Cantidad N�"  onkeypress="return numeros(event)" name="numerodesolicitudes" type="number" >
                                                </td>      
                                                <td width="80px">
                                                    <div class="form-group">
                                                        <div class="col-md-6 text-center">
                                                            <button type="button" class="btn  btn-primary " onclick="javascript :solicitudpabellon()">Solicitar</button> 

                                                        </div> 
                                                    </div>
                                                </td>
                                            </tr>  

                                        </table>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div id="respuesta" name="respuesta">

                                                    </div>
                                                </td>

                                            </tr>
                                        </table>

                                    </div>        
                                </td>

                            </tr>  
                        </table>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Solicitud de Control</div></td></tr>

                        </table>
                        <table style=" width: 100%">

                            <tr>
                                <td colspan="6">
                                    <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-check bigicon" style="font-size:18px">Fecha Control </i></span>
                                    <div class="col-md-8">
                                        <input type="text" maxlength="13" size="12" id="fechacontrol" name="fechacontrol" ><br> </td>
                                    </div>  
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                    <div class="col-md-8">
                                        <textarea type="text"  rows="3" id="motivocontrol" name="motivocontrol"  class="form-control" placeholder="Motivo Control"  ></textarea>


                                    </div>  
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                        <table>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary btn-lg">Guardar </button>

                                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                        </div>
                                    </div> 
                                </td>
                            </tr>


                        </table>


                    
                </div>
            </form>
        </div>
    </article>
</section>
<script>
     $(function () {
        $("#fechacontrol").datepicker();
    });

     function validarcontrol(){
         alert("pase");
       
         var fechacontrol = document.forms["hojadiaria"]["fechacontrol"].value;
         var motivocontrol = document.forms["hojadiaria"]["motivocontrol"].value;
         
         if (fechacontrol != "" ){
             if(motivocontrol  == ""){
                 return false;
             }
         }
    }    
        
    function numeros(e)  // 1

    {
        var key = window.Event ? e.which : e.keyCode
        return (key >= 48 && key <= 57)
    }


    function solicitudpabellon() {

        var solicitudes = document.forms["hojadiaria"]["numerodesolicitudes"].value;
        var especialidad = document.forms["hojadiaria"]["especialidad"].value;


        if (solicitudes == "") {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        } else
        {
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("respuesta");
                xmlhttp.open("post", "solicitud.jsp?cantidad=" + solicitudes + "&especialidad=" + especialidad);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b> </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;



                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }
        }
    }

    function validar() {
        var sic = document.forms["hojadiaria"]["sic"].value;
        var ges = document.forms["hojadiaria"]["ges"].value;
        var detalledederivacion = document.forms["hojadiaria"]["detalledederivacion"].value;
        var destino = document.forms["hojadiaria"]["destinopaciente"].value;
        var constanciages = document.forms["hojadiaria"]["constanciages"].value;
        var diagnosticosic = document.forms["hojadiaria"]["diagnosticosic"].value;
        var otrodiagnosticosic = document.forms["hojadiaria"]["otrodiagnosticosic"].value;
        var numerodesolicitudes = document.forms["hojadiaria"]["numerodesolicitudes"].value;


        if (sic == "1" & (diagnosticosic == "0" || detalledederivacion == "")) {
            Alert.render("Debe Colocar el Diagnostico por el que deriva y el Detalle de la Derivacion!!Ya que indic� que SIC es SI");
            return false;
        }
        if (diagnosticosic == "-1" & otrodiagnosticosic == "") {
            Alert.render("Debe indicar Diagnostico por el que deriva!!Complete los datos para Continuar");
            return false;
        }

        if (ges == "1") {
            if (constanciages == "0") {
                Alert.render("Debe Indicar si quiere Constacia Ges y IPD!!Complete los datos para continuar");
                return false;
            }
        }


        var atencion = document.getElementById('idatencioncita').value;
        
        var especialidad = document.getElementById('especialidad').value;

        if (atencion == 1 && especialidad != 69) {
            var pertinencia = document.getElementById('pertinencia').value;
            if (pertinencia == -1) {
                Alert.render("Como su atencion es Nueva debe indicar si �Es pertinente la derivaci�n a su atenci�n? !! Debe Completar los Datos para Continuar");
                return false;
            }
        }




        var pre = document.forms["hojadiaria"]["solicituddepabellon"].checked;
        
        if (pre) {
            if (numerodesolicitudes == "") {
                Alert.render("Debe Indicar Cuantas Solicitudes de Pabellon Requiere y dar clic en Solicitar!! Complete los datos para continuar");
                return false;
            } else {
                var malo = 0;
                for (i = 0; i < numerodesolicitudes; ++i) {
                    var d = document.forms["hojadiaria"]["diagnosticos" + i].value;
                    var od = document.forms["hojadiaria"]["otrodiagnostico" + i].value;
                    var inter = document.forms["hojadiaria"]["intervencion" + i].value;
                    var a = document.forms["hojadiaria"]["anestesia" + i].value;
                    var r = document.forms["hojadiaria"]["requerimientos" + i].value;
                    var o = document.forms["hojadiaria"]["observacion" + i].value;

                    if (d == "no" || inter == "" || r == "" || o == "" || a == "0") {
                        malo = 1;
                    }

                    if (d == "-1") {
                        if (od == "") {
                            malo = 1;
                        }
                    }
                }

                if (malo == 1) {
                    Alert.render("Debe Indicar la informacion de las solicitudes de Pabellon!! Complete los datos para continuar");
                    return false;
                }
            }


        }
        
         var fechacontrol = document.forms["hojadiaria"]["fechacontrol"].value;
         var motivocontrol = document.forms["hojadiaria"]["motivocontrol"].value;
        
         if (fechacontrol != "" ){
             if(motivocontrol  == ""){
                  Alert.render("Debe Indicar el Motivo del Control!! Complete los datos para continuar");
                  
                 return false;
             }
         }


    }
    function validarAgudezavisual(valor) {
        var ges = document.forms["hojadiaria"]["ges"].value;
        var constanciages = document.forms["hojadiaria"]["constanciages"].value;

        if ((valor == 2 || valor == 31) && (ges == 1) && (constanciages == 1)) {
            var clic = document.getElementById('diagnostico' + valor).checked;

            if (clic) {
                document.getElementById('agudeza').style.display = 'block';

            } else {
                document.getElementById('agudeza').style.display = 'none';
            }
        }
    }

</script>
<jsp:include page="../comunes/Footer.jsp"/>