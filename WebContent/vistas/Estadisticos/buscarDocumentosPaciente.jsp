<%-- 
    Document   : BuscarPacienteAdmitido
    Created on : 10-07-2017, 10:30:54
    Author     : a
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp" />
<script>
    function formateaRut(Rut)
    {
        var sRut = new String(Rut);
        var sRutFormateado = '';
        sRut = quitaFormato(sRut);
        var sDV = sRut.charAt(sRut.length - 1);
        sRut = sRut.substring(0, sRut.length - 1);
        document.forms["buscarsic"].txtRutSinDV.value = sRut;
        document.forms["buscarsic"].txtDV.value = sDV;
        while (sRut.length > 3)
        {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
        }
        sRutFormateado = sRut + sRutFormateado;
        if (sRutFormateado != "")
            sRutFormateado += "-";
        sRutFormateado += sDV;
        if (document.forms["buscarsic"].rutpaciento.value != sRutFormateado)
            document.forms["buscarsic"].rutpaciento.value = sRutFormateado;
    }
    function quitaFormato(Nro)
    {
        var strNro = new String(Nro);
        while (strNro.indexOf(".") != - 1)
            strNro = strNro.replace(".", "");
        strNro = strNro.replace("-", "");
        return strNro;
    }

    var entre = false;
    function  buscaPaciente()
    {

        var paciente = document.forms["buscarsic"]["rutpaciente"].value;


        if (paciente == null || paciente == "")
        {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        } else
        {
            entre = true;
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("buscar");
                xmlhttp.open("post", "documentospaciente.jsp?paciente=" + paciente);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '<b>Favor espere... </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;

                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }
    }



</script>

<%
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Archivos Paciente">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>

<%
    }
%> 

    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <div class="container"> 

        <div class="tab-content">
            <div id="crear" class="tab-pane fade in active">


                <form name="buscarsic" class="form-horizontal" id="buscarsic" action='' method="post"  >

                    <fieldset>
                        <legend class="text-center header">Buscar Documentos de Paciente</legend>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon">Rut</i></span>
                            <div class="col-md-4">

                                <input type="text" id="rutpaciente" name="rutpaciento" onmouseover="showToolTip(event, '¡Escriba su RUT sin puntos ni guiones!');
                                        return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                                <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                                <input value="d" name="txtDV" id="txtDV" type="hidden">

                            </div>
                            
                            <button type="button" style="margin-right: 200px" class="btn btn-primary" onclick="javascript : buscaPaciente()">Buscar</button>
                        </div>

                        <div id="buscar"></div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</body>
<jsp:include page="../comunes/Footer.jsp" />
