<%-- 
    Document   : ListaPacienteCitadosparaHoy
    Created on : 31-10-2017, 17:15:24
    Author     : a
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<fieldset>
   <%
    controlador_cita cc = new controlador_cita();
    String rutdoctor = cc.getUsuarioLoggin();
    int i = 1;
  
   %> 
    <legend class="text-center header">Lista de Paciente Para Hoy</legend>
    <h3>Fecha: <%=cc.fechadeldia()%></h3>
    <table class="table table-striped" style=" width: 100%">
        <thead>
            <tr>
                <th> N° </th>
                <th> Rut</th>
                <th>Nombre</th>
                <th>Tipo Atención</th>
                
                <th>Hora Citado</th>
                <th>Hora Recepcionado</th>
                <th>Estatus</th>
            </tr>
        </thead>
        <%for(cita c : cc.buscarCitasdeDoctorParaHoy(rutdoctor)){ %>
        <tr>
            <td><%=i%></td>
            <td><%=c.getRut_paciente() %></td> 
            <td><%=c.getTemporales() %></td>
            <td><%=c.getTemporales1() %></td>
           
            <td><%=c.getTemporales3() %></td>
            <td><%=c.getTemporales5() %></td>
                
            <%if (c.getEstatus()== 2){%>
               <td style=" background-color: #ff0000"><%=c.getTemporales4() %></td>
               <%}else if(c.getEstatus()== 4){%>
               <td style=" background-color: #33cc33" ><%=c.getTemporales4() %></td>
               <%}else if(c.getEstatus()== 1){%>
               <td style=" background-color: #ffff00" ><%=c.getTemporales4() %></td>
               <%}else if(c.getEstatus()== 3 || c.getEstatus() == 6){%>
               <td style=" background-color: #0000ff" ><%=c.getTemporales4() %></td>
               <%}else {%>
               <td style=" background-color: #ff3300" ><%=c.getTemporales4() %></td>
               <%}%>
        </tr>
        <%++i;
        }%>

    </table>


    <jsp:include page="../comunes/Footer.jsp"/>
</fieldset>