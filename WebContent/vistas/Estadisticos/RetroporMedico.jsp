<%-- 
    Document   : Retro por Médico
    Created on : 03-04-2017, 11:21:31
    Author     : a
--%>

<%@page import="Modelos.doctor"%>
<%@page import="Controlador.General"%>
<%@page import="java.util.Vector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.jfree.ui.RectangleInsets"%>
<%@page import="java.awt.Color"%>
<%@page import="org.jfree.chart.plot.CategoryPlot"%>
<%@page import="java.io.IOException"%>
<%@page import="org.jfree.chart.ChartUtilities"%>
<%@page import="java.io.OutputStream"%>
<%@page import="org.jfree.chart.plot.PlotOrientation"%>
<%@page import="org.jfree.chart.JFreeChart"%>
<%@page import="org.jfree.chart.ChartFactory"%>
<%@page import="org.jfree.data.category.DefaultCategoryDataset"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%
    //DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    //para lineal y area con barra uno o dos
 General g = new General();
   Vector<doctor> profesionales = g.buscarAtencionporProfesional();
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        for( doctor d :g.buscarAtencionporProfesional() ){
            dataset.setValue(d.getEstatus(), "Citas Tomadas", d.getNombre());
            dataset.setValue(g.buscarcuantosAtendidosSatisfactoriamente(d.getRut()), "Citas Atendidas", d.getNombre());
        }
    
       
        JFreeChart chart = ChartFactory.createBarChart("     Atención por Profesional", "", "", dataset, PlotOrientation.HORIZONTAL, true, true, true);
        CategoryPlot plot = chart.getCategoryPlot();

        // color del fondo del histograma
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
        plot.getRenderer().setSeriesPaint(0, Color.BLUE);
        plot.getRenderer().setSeriesPaint(1, Color.MAGENTA);
        // se oculta el recuadro del histograma
        plot.setOutlineVisible(false);
        // se elimina el margen entre los ejes y el chart
        plot.setAxisOffset(new RectangleInsets(0D, 0D, 0D, 0D));
        try {
            response.setContentType("image/png");
            OutputStream os = response.getOutputStream();
            ChartUtilities.writeChartAsPNG(os, chart, 450, 250);

        } catch (IOException e) {
            System.err.println("Error creando grafico.");
        }


%>
