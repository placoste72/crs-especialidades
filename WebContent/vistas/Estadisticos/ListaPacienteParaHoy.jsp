<%-- 
    Document   : ListaPacienteCitadosparaHoy
    Created on : 31-10-2017, 17:15:24
    Author     : a
--%>

<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<fieldset>
   <%
    controlador_cita cc = new controlador_cita();
   
   %> 
    <legend class="text-center header">Lista de Paciente Para Hoy </legend>
    <table class="table table-striped" style="width: 100%">
        <thead>
            <tr>
                <th>N°</th>
                <th>Especialidad</th>
                <th>Profesional </th>
                <th> Rut del Paciente:</th>
                <th>Nombre</th>
                <th>Tipo Atención</th>
                <th>Fecha</th>
                <th>Hora Citación</th>
                <th>Hora Repceción</th>
                <th>Quien Repciona</th>
                <th>Estatus</th>
            </tr>
        </thead>
        
        <%
            int i= 1; 
            for(cita c : cc.buscarCitasParaHoy()){ %>
        <tr>
            <td><%=i%></td>
            <td><%=c.getTemporales5() %></td>
            <td><%=c.getRut_doctor() %></td>
            <td><%=c.getRut_paciente() %></td> 
            <td><%=c.getTemporales() %></td>
            <td><%=c.getTemporales1() %></td>
            <td><%=c.getTemporales2() %></td>
            <td><%=c.getTemporales3() %></td>
            <td><%=c.getMotivo_cancela() %></td>
             <td><%=c.getTemporales6() %></td>
               <%if (c.getEstatus()== 2){%>
               <td style=" background-color: #ff4d4d"><%=c.getTemporales4() %></td>
               <%}else if(c.getEstatus()== 4){%>
               <td style=" background-color: #b3ffb3" ><%=c.getTemporales4() %></td>
               <%}else if(c.getEstatus()== 1){%>
               <td style=" background-color: #ffffcc" ><%=c.getTemporales4() %></td>
               <%}else{%>
               <td style=" background-color: #ff9980" ><%=c.getTemporales4() %></td>
               <%}%>
          
           
        </tr>
        <%++i;}%>

    </table>


    <jsp:include page="../comunes/Footer.jsp"/>
</fieldset>