<%-- 
    Document   : PacientesConEntrevistaPreoperatoria
    Created on : 17-05-2018, 7:27:26
    Author     : a
--%>
<%@page import="Modelos.funcionario"%>
<%@page import="Controlador.controlador_usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../comunes/Header.jsp"/>
<link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/crs.js" type="text/javascript"></script>
<title>Estadisticos</title>
<script>
    $(function () {
        $("#fecha_inicio").datepicker();
    });
    $(function () {
        $("#fecha_fin").datepicker();
    });
    function formateaRut(Rut)
    {
        var sRut = new String(Rut);
        var sRutFormateado = '';
        sRut = quitaFormato(sRut);
        var sDV = sRut.charAt(sRut.length - 1);
        sRut = sRut.substring(0, sRut.length - 1);
        document.forms["infomes"].txtRutSinDV.value = sRut;
        document.forms["infomes"].txtDV.value = sDV;
        while (sRut.length > 3)
        {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
        }
        sRutFormateado = sRut + sRutFormateado;
        if (sRutFormateado != "")
            sRutFormateado += "-";
        sRutFormateado += sDV;
        if (document.forms["infomes"].rutpaciento.value != sRutFormateado)
            document.forms["infomes"].rutpaciento.value = sRutFormateado;
    }
    function quitaFormato(Nro)
    {
        var strNro = new String(Nro);
        while (strNro.indexOf(".") != - 1)
            strNro = strNro.replace(".", "");
        strNro = strNro.replace("-", "");
        return strNro;
    }

    function validar() {
        var fechaIncio = document.forms["infomes"]["fecha_inicio"].value;
        var fechaFin = document.forms["infomes"]["fecha_fin"].value;
        var rutpaciente = document.forms["infomes"]["rutpaciente"].value;
        var doctor = document.forms["infomes"]["funcionario"].value;
        array_fecha = fechaIncio.split("/")

        var dia = array_fecha[0]
        var mes = (array_fecha[1] - 1)
        var ano = (array_fecha[2])
        var fechaDate = new Date(ano, mes, dia)

        array_fecha2 = fechaFin.split("/")

        var dia2 = array_fecha2[0]
        var mes2 = (array_fecha2[1] - 1)
        var ano2 = (array_fecha2[2])
        var fechaDate2 = new Date(ano2, mes2, dia2)

        if ((fechaIncio == "" || fechaFin == "") & rutpaciente == "" & doctor == -1) {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        } else if (fechaIncio != "" || fechaFin != "") {

            if (fechaDate > fechaDate2) {

                Alert.render("Rango de Fecha Incorrecto !!Debe Completar los Datos para Continuar");
                return false;
            }
        }
        try
        {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e)
        {
            try
            {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
            xmlhttp = new XMLHttpRequest();
        }

        if (xmlhttp) {
            var objeto_recibidor = document.getElementById("reporte");
            xmlhttp.open("post", "pacientesconEntrevistaPreoperatorias.jsp?inicio=" + fechaIncio + "&fin=" + fechaFin+"&doctor="+doctor+"&rutpaciente="+rutpaciente);
            xmlhttp.send("");
            if (xmlhttp.readyState == 1) {
                objeto_recibidor.innerHTML = '</br></br><b> </b>';

            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    objeto_recibidor.innerHTML = xmlhttp.responseText;



                }
                if (xmlhttp.status != 200) {
                    objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                }
            }
        }





    }



</script>
<%
    controlador_usuario cu = new controlador_usuario();

    String tipo = "-1";
    int es = -1;
    String doc = "-1";
    if (request.getParameter("funcionario") != null) {
        doc = request.getParameter("funcionario");
    }

%>
<body>
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>

    <div class="container"> 

        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">

                    <form id="infomes" name="infomes"  action=""  method="post" onsubmit="return false;"   > 
                        <fieldset>
                            <legend class="text-center header">Lista de Pacientes con Entrevista Preoperatoria</legend>								

                            <table>
                                <tr>

                                    <td>
                                        <span class="col-md-4 col-md-offset-2 "><i class="fa fa-user bigicon"  style="font-size: 18px" f>Por Rut:</i></span>


                                        <div class="col-md-6">

                                            <input type="text" id="rutpaciente" name="rutpaciento" onmouseover="showToolTip(event, '¡Escriba su RUT sin puntos ni guiones!');
                                                    return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                                            <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                                            <input value="d" name="txtDV" id="txtDV" type="hidden">

                                        </div>
                                    </td>
                                    <td>


                                        <span class="col-md-4 col-md-offset-2 " ><i class="fa fa-user-md bigicon" ></i></span>

                                        <div class="col-md-6" >
                                            <select class="form-control" id="funcionario" name="funcionario" title="Profesional">
                                                <option value="-1">Profesional
                                                    <%         for (funcionario f
                                                                : cu.BuscarFuncionaroconDoctorporEspecialidad(46)) {

                                                            if (doc.equals(f.getRut())) {%>
                                                <option selected value="<%=f.getRut()%>"><%=f.getNombre()%><%
                                                } else {%>
                                                <option value="<%=f.getRut()%>"><%=f.getNombre()%><%
                                                        }

                                                    }
                                                    %>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>


                                    <td > <span class="col-md-4 col-md-offset-2 "><i class="fa fa-calendar bigicon" style="font-size: 18px">Fecha Inicio</i></span>
                                        <div class="col-md-6"> 
                                            <input type="text" maxlength="13"  id="fecha_inicio" name="fecha_inicio" value="" >
                                        </div>
                                    </td> 
                                    <td><span class="col-md-4 col-md-offset-2 "><i class="fa fa-calendar bigicon" style="font-size: 18px">Fecha Fin</i></span>
                                        <div class="col-md-6">   
                                            <input type="text" maxlength="13"  id="fecha_fin" name="fecha_fin" value="" >

                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8" >
                                            <button class="btn  btn-primary " onclick="javascript : validar()">Buscar</button> 
                                          
                                        </div>
                                        
                                    </td>
                                    <td>
                                           <div class="col-md-8" >
                                            
                                            <button class="btn  btn-primary " type="reset">Cancelar</button>  
                                        </div>

                                    </td></tr>



                            </table>
                            <br>
                            <br>
                            <br>

                            <div id="reporte" name="reporte">

                            </div>





                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>


</html>
