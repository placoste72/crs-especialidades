<%-- 
    Document   : pacientesconEntrevistaPreoperatorias
    Created on : 17-05-2018, 7:46:54
    Author     : a
--%>
<%@page import="Modelos.entrevista_preoperatoria"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="java.util.Date"%>
<%

    String fechainicio = request.getParameter("inicio");
    String fechafin = request.getParameter("fin");
    String rutpaciente = request.getParameter("rutpaciente");
    String doctor = request.getParameter("doctor");
    controlador_especialidad ce = new controlador_especialidad();
    int traigo = 0;
    String where = "";

    if (!fechainicio.equalsIgnoreCase("")) {
        Date fecha1 = new Date(Integer.parseInt(fechainicio.substring(6, 10)) - 1900, Integer.parseInt(fechainicio.substring(3, 5)) - 1, Integer.parseInt(fechainicio.substring(0, 2)), 0, 0, 0);
        Date fecha2 = new Date(Integer.parseInt(fechafin.substring(6, 10)) - 1900, Integer.parseInt(fechafin.substring(3, 5)) - 1, Integer.parseInt(fechafin.substring(0, 2)), 0, 0, 0);

        where = " ep.fecha_registro :: date BETWEEN '" + fecha1 + "' AND '" + fecha2 + "' ";
        ++traigo;
    }

    if (!rutpaciente.equalsIgnoreCase("")) {
        if (traigo == 0) {
            where = " upper(p.rut)=upper('" + rutpaciente + "')";
            ++traigo;
        } else {
            where = where + " and upper(p.rut)=upper('" + rutpaciente + "')";
            ++traigo;
        }
    }

    if (!doctor.equalsIgnoreCase("-1")) {
        if (traigo == 0) {
            where = " pla.rut_doctor='" + doctor + "'";
            ++traigo;
        } else {
            where = where + " and pla.rut_doctor='" + doctor + "'";
            ++traigo;
        }

    }



%>

<table class=" table table-striped" style="width: 100%">
    <thead>
        <tr>
            <th> Profesional</th>
            <th>Fecha de Atenci�n</th>
            <th>Rut Paciente</th>
            <th>Nombre de Paciente</th>
            <th>Entrevista Preoperatoria</th>
        </tr>
    </thead>
    <%for (entrevista_preoperatoria e : ce.buscarEntrevistasporFechas(where)) {%>
    <tr>
        <td><%=e.getAuxiliar3()%></td>
        <td><%=e.getAuxiliar4()%></td>
        <td><%=e.getAuxiliar1()%></td>
        <td><%=e.getAuxiliar2()%></td>
        <td>
            <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=e.getIdentrevista_preoperatoria()%>', 'Entrevista Preoperatoria', 'width=600,height=450')"/>

        </td>
    </tr>

    <%}%>
</table>