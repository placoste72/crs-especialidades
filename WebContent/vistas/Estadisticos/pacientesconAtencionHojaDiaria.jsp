<%-- 
    Document   : pacientesconAtencionHojaDiaria
    Created on : 07-07-2018, 14:54:46
    Author     : a
--%>

<%@page import="Modelos.lugar"%>
<%@page import="java.util.Vector"%>
<%@page import="Modelos.hojadiaria"%>
<%@page import="java.util.Date"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String fechainicio = request.getParameter("inicio");
    String fechafin = request.getParameter("fin");
    String rutpaciente = request.getParameter("rutpaciente");
    String doctor = request.getParameter("doctor");
    String especialidad = request.getParameter("especialidad");
    controlador_especialidad ce = new controlador_especialidad();
    int traigo = 0;
    String where = "";

    if (!fechainicio.equalsIgnoreCase("")) {
        Date fecha1 = new Date(Integer.parseInt(fechainicio.substring(6, 10)) - 1900, Integer.parseInt(fechainicio.substring(3, 5)) - 1, Integer.parseInt(fechainicio.substring(0, 2)), 0, 0, 0);
        Date fecha2 = new Date(Integer.parseInt(fechafin.substring(6, 10)) - 1900, Integer.parseInt(fechafin.substring(3, 5)) - 1, Integer.parseInt(fechafin.substring(0, 2)), 0, 0, 0);

        where = " hd.fecharegistro:: date BETWEEN '" + fecha1 + "' AND '" + fecha2 + "' ";
        ++traigo;
    }

    if (!rutpaciente.equalsIgnoreCase("")) {
        if (traigo == 0) {
            where = " upper(pac.rut)=upper('" + rutpaciente + "')";
            ++traigo;
        } else {
            where = where + " and upper(pac.rut)=upper('" + rutpaciente + "')";
            ++traigo;
        }
    }

    if (!doctor.equalsIgnoreCase("-1")) {
        if (traigo == 0) {
            where = " d.rut='" + doctor + "'";
            ++traigo;
        } else {
            where = where + " and d.rut='" + doctor + "'";
            ++traigo;
        }

    }

    if (!especialidad.equalsIgnoreCase("-1")) {
        if (traigo == 0) {
            where = " p.id_especialidad='" + especialidad + "'";
            ++traigo;
        } else {
            where = where + " and p.id_especialidad='" + especialidad + "'";
            ++traigo;
        }

    }


%>

<table class=" table table-striped" style="width: 100%">
    <thead>
        <tr>
            <th> N°</th>
            <th> Especialdiad</th>
            <th> Profesional</th>
            <th>Fecha </th>
            <th>Rut </th>
            <th>Nombre </th>
            <th>GES</th>
            <th>Diagnostico</th>
            <th>Examenes</th>
            <th>Procedimiento</th>
            <th>Derivacion</th>
            <th>Documentos</th>
        </tr>
    </thead>
    <%    
        int i = 1;
      
        for (hojadiaria hd : ce.BuscarTodoslosPacienteVistosporHojaDiaria(where)) {
              hojadiaria documento = ce.BuscarHojaDiaria(hd.getIdhojadiaria());
               String diagnostico = ce.buscarlosdiagnosticosdeunaHojaDiariareporte(hd.getIdhojadiaria());
               String examenes = ce.buscarExamenesdeUnaHojaReporte(hd.getIdhojadiaria());
               String procedimiento = ce.buscarProcedimientosdeUnaHojaReporte(hd.getIdhojadiaria()); %>
    <tr>
        <td><%=i%></td>
        <td><%=hd.getAuxiliar5()%></td>
        <td><%=hd.getOtro_diagnostico()%></td>
        <td><%=hd.getFecha_registro() %></td>
        <td><%=hd.getOtrodestino_paciente()%></td>
        <td><%=hd.getOtro_procedimiento()%></td>
        <td><%=hd.getAuxiliar6()%></td>
        <td><%=diagnostico%> </td>
        <td><%=examenes%> </td>
        <td><%=procedimiento%> </td>
        <td><%=hd.getAuxiliar7()%></td>
        <td>
            <table class="table table-striped" style="width: 100%"  >

                <thead>
                    <%if ((documento.getSic() == 1) && (documento.getConstancia() == 1) && (documento.isSolicitudpabellon() == true)) {%>  
                <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico <br> Solicitud de Pabellón Quirurgico <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento<br> Informado Intervenciones Quirúrgicas  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>

                <%} else if ((documento.getSic() == 1) && (documento.getConstancia() == 1)) {%>
                <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                        <%} else if ((documento.getSic() == 1) && (documento.isSolicitudpabellon() == true)) {%>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento <br>Informado Intervenciones Quirúrgicas  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>
                <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                        <%} else if ((documento.getConstancia() == 1) && (documento.isSolicitudpabellon() == true)) {%>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento<br> Informado Intervenciones Quirúrgicas  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                        <%} else if (documento.getConstancia() == 1) {%>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                        <%} else if ((documento.getSic() == 1)) {%>
                <th><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                        <%} else if (documento.isSolicitudpabellon() == true) {%>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento <br> Informado Intervenciones Quirúrgicas <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hd.getIdhojadiaria()%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>

                <%}%>
                <th></th>
    </tr>


</thead>
</table> 
</td>

</tr>

<% i++;
        }%>
</table>