<%-- 
    Document   : citasdelPaciente
    Created on : 19-07-2018, 9:36:06
    Author     : a
--%>


<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/headerwindows.jsp"/>
<%
    String paciente = request.getParameter("rut");
    controlador_cita cc = new controlador_cita();

%>
<html>
    <body>
    <legend class="text-center header">Citas en el CRS</legend>
    <table class=" table table-striped" style=" width: 100% ">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Especialidad</th>
                <th>Profesional</th>
                <th>Atención</th>
                <th>Programa</th>
                <th>Motivo</th>
                <th>Estado</th>
                <th></th>
                <th></th>
            </tr>

        </thead>  
        <%for (cita c : cc.BuscarCitasdeunPacienteAgendaAntigua(paciente)) {
        %>
        <tr>
            <td><%=c.getTemporales5()%> </td>
            <td><%=c.getTemporales4()%> </td>
            <td><%=c.getTemporales()%> </td>
            <td><%=c.getRut_doctor()%></td>
            <td><%=c.getTemporales1()%> </td>
            <td><%=c.getTemporales3()%> </td>
            <td><%=c.getRut_paciente()%> </td>
            <td><%=c.getTemporales2()%> </td>
            <%if (c.getEstatus() == 0) {
            %>
            <td><%=c.getUsuario_cancela() %></td>
            <td><%=c.getTemporales6()%></td>
            <%}
             if (c.getEstatus() == 6 || c.getEstatus() == 3){
            %>
              <td><%=c.getTemporales7()%></td>
               <%}%>
        </tr>  


        <% 
            }%>


    </table>
</body>
</html>