<%-- 
    Document   : vistaExamenes
    Created on : 19-07-2018, 13:42:33
    Author     : a
--%>

<%@page import="Modelos.lugar"%>
<%@page import="Controlador.controlador_examenes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/headerwindows.jsp"/>
<%
  String atencion  = request.getParameter("idatencion");
  controlador_examenes ce = new controlador_examenes ();
  String examenes = ce.BuscarExamenesdeunaAtencion(Integer.parseInt(atencion));
  lugar e= ce.AtencionExamenesdeLaboratorioporidAtencion(Integer.parseInt(atencion));  
%>
<html>
    <head>
       
    </head>
    <body>
         <legend class="text-center header">Examenes de Laboratorio</legend>
        <table class="table table-striped" style="width: 100%">
            <thead>
                <tr>
                <th>
                    Examenes Realizados
                </th>
                <th>
                    Observaciones
                </th>
                </tr>
            </thead>
            <tr>
                <td><%=examenes%></td>
                <td><%=e.getObservaciones() %></td>
                
            </tr>
            
        </table>
    </body>
</html>
