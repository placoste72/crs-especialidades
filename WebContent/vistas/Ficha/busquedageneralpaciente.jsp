<%-- 
    Document   : busquedageneralpaciente
    Created on : 09-04-2018, 8:32:44
    Author     : a
--%>

<%@page import="Controlador.controlador_paciente"%>
<%@page import="Modelos.paciente"%>
<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Controlador.General"%>

<!DOCTYPE html>
<%
    String rut = request.getParameter("paciente");
    General g = new General();
    controlador_paciente cp = new controlador_paciente();
    paciente p = cp.BuscarDetallePaciente(rut);

    if (p.getRut() != null) {
        boolean tengoficha = g.verSipacienteTieneFichaenArchivo(rut);
%>

<table  style="border: #619fd8 5px solid;">
    <input type="text" name="f" id="f" value="<%=p.getRut()%>" hidden>
    <tr><td>
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon" style="font-size: 19px">Nombre:</i></span>
        </td>
        <td colspan="3">
            <div class="col-md-8">
                <label id="nombre"><%=p.getNombre() + "  " + p.getApellido_paterno() + " " + p.getApellido_moderno()%></label>
            </div>
        </td>
        <td>
            
        </td>
        <td>
            
        </td>

    </tr>
    <tr>

        <td>
            <span class="col-md-1 col-md-offset-2 "><i class="fa fa-envelope bigicon" style="font-size: 19px">Email</i></span>
        </td>   
        <td>
            <div class="col-md-8">
                <label><%=p.getEmail()%></label>
            </div>

        </td>
        <td>
            <%if (tengoficha == true) {
            %>
            <span class="col-md-4 col-md-offset-2 "><i class="fa  bigicon" style=" font-size: 15px">Paciente con Ficha en Papel</i> </span>

            <%} else {
            %>
            <span class="col-md-4 col-md-offset-2 "><i class="fa  bigicon" style=" font-size: 15px; color:  #FF0000">Paciente sin Ficha en Papel</i> </span>

            <%
                }%>
        </td>
        <td><a onClick="window.open('citasdelPaciente.jsp?rut=<%=p.getRut()%>', 'Citas de Paciente', 'width=1200, height=1000')">  <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 15px" >Citas </i> <img src="../../public/imagenes/estatus/citas.jpg" width="40px" height="40px" alt=""/> </span></a></td>

         <td><a onClick="window.open('estadiaencama.jsp?rut=<%=p.getRut()%>', 'Hospitalizaciones del Paciente', 'width=1200, height=1000')">  <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 15px" >Media Estadia </i> <img src="../../public/imagenes/estatus/mediaestadia.png" width="40px" height="40px" alt=""/> </span></a></td>



        <td><a onClick="window.open('HitosHistorial.jsp?rut=<%=p.getRut()%>', 'Hitos y Historial', 'width=1200, height=1000')"> <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 15px"> Hitos/Historial </i><img src="../../public/imagenes/icon/carpeta_fichero.jpeg" width="40px" height="40px" alt=""/> </span></a></td>


    </tr>
    <tr>

        <td>

            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon" style="font-size: 19px">Contactos:</i></span>
        </td><td>  
            <div class="col-md-8">
                <label><%=p.getContacto1() + " " + p.getContacto2()%></label>
            </div>
        </td>
        <td></td>
        <td></td>
        <td></td>
         <td></td>
    </tr>
    <tr>

        <td>

            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon" style="font-size: 19px">Prevision:</i></span>
        </td><td>  
            <div class="col-md-8">
                <label><%=p.getTemporal2() + " " + p.getTemporal4()%></label>
            </div>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td> </td>

    </tr>
</table>
<br>
<legend class="text-center header">Atenciones En Box</legend>
<table class="table table-striped" style=" width: 100%">
    <tr>
        <th>Fecha</th>
        <th>Atencion</th>
        <th>Especialidad</th>
        <th>Profesional</th>
        <th>Detalle</th>
    </tr>
    <%for (atencion_clinica_tecnologo at : g.BuscarAtencionesdeunPaciente(rut)) {

    %>
    <tr>
        <td><%=at.getAuxiliarcinco()%></td>
        <td><%=at.getAuxiliarseis() %></td>
        <td><%=at.getAuxiliardos()%></td>
        <td><%=at.getAuxiliartres()%></td>
        <%if (at.getAuxiliarcuatro() == 1) {
        %>
        <td><a onClick="window.open('vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   class="button3">

                VER  </a> </td>
                <%} else if (at.getAuxiliarcuatro() == 2) {%>
        <td><a onClick="window.open('vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   class="button3">

                VER  </a> </td>
                <%} else if (at.getAuxiliarcuatro() == 3) {%>
        <td><a onClick="window.open('<%=g.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  class="button3">

                VER  </a> </td>
                <%} else if (at.getAuxiliarcuatro() == 4) {%>
        <td><a onClick="window.open('vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   class="button3">

                VER  </a> </td>
                <%} else if (at.getAuxiliarcuatro() == 5) {%>
        <td> </td>
        <%} else if (at.getAuxiliarcuatro() == 6) {%>
        <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"    class="button3">

                Protocolo  </a> 
            
            <%int ide = g.idepicrisis(at.getId_atencion_tecnologo());%>
            <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    class="button3">

                Epicrisis  </a> </td>
                <%} else if (at.getAuxiliarcuatro() == 7) {%>
        <td><a  onClick="window.open('vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   class="button3">

                VER  </a> </td>
                <%}else if (at.getAuxiliarcuatro() == 8) {%>
        <td><a  onClick="window.open('<%=g.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenolog�a', 'width=1200, height=1000')"   class="button3">

                VER  </a> </td>
                <%}else if (at.getAuxiliarcuatro() == 9) {%>
        <td><a  onClick="window.open('DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   class="button3">

                VER  </a> </td>
                <%}%>
    </tr>
    <%

        }%>

</table>



<%

} else {
%>
<table class=" table table-striped " style="border: #619fd8 5px solid;">
    <tr>
        <th>
            <p style="color:#FF0000; font-size:15px">Paciente no registrado</p>
            <a href='<%=cp.getLocallink()%>Cita/RegistrarPaciente.jsp'>Registra Aqui�</a>
        </th>
    </tr>
</table> 
<%
    }


%>
