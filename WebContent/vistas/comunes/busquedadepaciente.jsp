<%-- 
    Document   : busquedadepaciente
    Created on : 11-07-2017, 10:29:37
    Author     : a
--%>

<%@page import="java.lang.NullPointerException"%>
<%@page import="Modelos.lugar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>
<%@page import="Modelos.prevision"%>
<%@include file="conexion.jsp" %>

<!DOCTYPE html>
<%     String rut = request.getParameter("paciente");

    controlador_paciente cp = new controlador_paciente();
    String t1 = "";
    String t2 = "";
    String direccion = "";

    paciente p = new paciente();

    String nombre = "";
    String apellidop = "";
    String apellidom = "";
    String email = "";

    /*1 fonasa , 2 isapre, 3 praim*/
    int codigoprevicion = 0;

    String fecha = "";
    int generoe = 0;
    int t = 0;
    int provincia = 0;
    int reg = 0;
    int procedencia1 = 0;
    int comuna = 0;
    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    p = cp.buscarpacienteporrut(rut);
    boolean existe = cp.buscarpaciente(rut);
    if (existe == true) {
        nombre = p.getNombre();
        apellidop = p.getApellido_paterno();
        apellidom = p.getApellido_moderno();
        fecha = formatoFecha.format(p.getFecha_nacimiento());
        generoe = p.getGenero();
        codigoprevicion = p.getProvision();
        t = p.getTramo();
        comuna = p.getId_comuna();
        t1 = p.getContacto1();
        t2 = p.getContacto2();
        direccion = p.getDireccion();
        provincia = p.getId_provincia();
        reg = p.getId_region();
        procedencia1 = p.getProcedencia();
        email = p.getEmail();
    }


%>

<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="nombrepaciente" name="nombrepaciente"  value="<%=nombre%>" class="form-control" onkeypress="return validarsololetra(event)" placeholder="Nombre Paciente"  required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="apellidoppaciente" name="apellidoppaciente"  value="<%=apellidop%>" class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Paterno"  required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="apellidompaciente" name="apellidompaciente"  value="<%=apellidom%>" class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Materno" required oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar-o bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" name="txt_inicial" id="txt_inicial" value="<%=fecha%>"  placeholder="Fecha de Nacimiento (dd/mm/yy) " class="form-control"  maxlength="13" size="12" onclick="
                javascript:  $(function () {
                    $('#txt_inicial').datepicker();
                });" onchange=" javascript:  $(function () {
                            $('#txt_inicial').datepicker();
                        });"  >
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="genero" name="genero">



            <option value="0">Genero
                <% if (generoe == 1) {
                %>
            <option selected value="<%=generoe%>">Femenino

                <%
                } else if (generoe == 2) {%>
            <option selected value="<%=generoe%>">Masculino
            <option  value="1">Femenino<%
            } else {%>
            <option  value="1">Femenino
            <option value="2"> Masculino
                <%}%>    
        </select>
    </div>  
</div>
<table>
    <tr>
        <td>
            <div class="form-group">
                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span>
                <div class="col-md-8">

                    <input type="text" id="telefonoPaciente" name="telefonoPaciente" value="<%=t1%>" class="form-control" placeholder="+56 9 1111 1111" maxlength="11"  required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
                </div>
            </div>
            <div class="form-group">
                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                <div class="col-md-8">

                    <input type="text" id="telefonoPaciente1" name="telefonoPaciente1" value="<%=t2%>" class="form-control" placeholder="+56 9 2222 2222" maxlength="11">
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="email" name="email" value="<%=email%>" class="form-control" placeholder="Email">
    </div>
</div>

<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon"></i></span>
    <div class="col-md-8">
        <input type="text"   rows="3" size="39" style="height: 89px" id="direccion" name="direccion" value="<%=direccion%>" class="form-control" placeholder="Direccion" required oninvalid="setCustomValidity('El campo Direccion es obligatorio')" oninput="setCustomValidity('')">


    </div>
</div>



<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="region" name="region" required onchange=" return buscaProvincia()"  >
            <option value=-1>Elegir Regi�n
                <%                                            try {
                        String region = " SELECT id_region, nombre FROM agenda.region where estatus = 1 order by nombre; ";

                        ResultSet rs_region = st.executeQuery(region);

                        while (rs_region.next()) {

                            if (Integer.parseInt(rs_region.getString("id_region")) == reg) {%>
            <option selected value="<%=rs_region.getString("id_region")%>"><%=rs_region.getString("nombre")%><%
            } else {%>
            <option value="<%=rs_region.getString("id_region")%>"><%=rs_region.getString("nombre")%><%
                        }

                    }
                } catch (SQLException ex) {
                }        %>
        </select>
    </div> 


</div>



<div id="provincia1">
    <div class="form-group">
        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
        <div class="col-md-8"  >
            <select class="form-control" id="provincia" name="provincia" required onchange=" return buscaComuna(this.value)">
                <option value=-1>Elegir provincia
                    <%
                        try {

                            String funcionario = " SELECT id_provincia, nombre FROM agenda.provincia where estatus = 1 and  id_region=" + reg + " order by nombre; ";

                            ResultSet rs_comuna = st.executeQuery(funcionario);

                            while (rs_comuna.next()) {

                                if (Integer.parseInt(rs_comuna.getString("id_provincia")) == provincia) {%>
                <option selected value="<%=rs_comuna.getString("id_provincia")%>"><%=rs_comuna.getString("nombre")%><%
                } else {%>
                <option value="<%=rs_comuna.getString("id_provincia")%>"><%=rs_comuna.getString("nombre")%><%
                            }

                        }
                    } catch (SQLException ex) {
                    }%>
            </select>
        </div>
    </div>
</div> 

<div id="comuna1">
    <div class="form-group">
        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
        <div class="col-md-8" >
            <select class="form-control" id="comuna" name="comuna" required>
                <option value="-1">Elegir Comuna
                    <%
                        try {
                            String funcionario = " SELECT id_comuna, nombre FROM agenda.comuna where estatus = 1 and id_provincia=" + provincia + " order by nombre; ";

                            ResultSet rs_comuna = st.executeQuery(funcionario);

                            while (rs_comuna.next()) {

                                if (Integer.parseInt(rs_comuna.getString("id_comuna")) == comuna) {%>
                <option selected value="<%=rs_comuna.getString("id_comuna")%>"><%=rs_comuna.getString("nombre")%><%
                } else {%>
                <option value="<%=rs_comuna.getString("id_comuna")%>"><%=rs_comuna.getString("nombre")%><%
                            }

                        }
                    } catch (SQLException ex) {
                    }%>
            </select>
        </div>
    </div>

</div> 

<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >

        <select class="form-control" id="procedencia" name="procedencia" required>
            <option value="-1">Elegir Consultorio
                <%     for (lugar temp : cp.buscarProcedencia()) {

                        if (temp.getId_lugar() == procedencia1) {%>
            <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
            } else {%>
            <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                    }
                }%>
        </select>

    </div>        
</div>            

<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="provision" name="provision" onchange="javascript:
                        var pre = document.forms['paciente_urgencia']['provision'].value;
                if (pre == 1) {

                    document.getElementById('t').style.display = 'block';
                } else
                    document.getElementById('t').style.display = 'none';
                " required>



            <option  value="-1">Prevision
                <% for (prevision pre : cp.buscarPrevicion()) {

                         if (pre.getId_prevision() == codigoprevicion) {%>
            <option selected  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                <%} else {%>
            <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                <% }
                                }%>      
        </select>
    </div>
</div>
<div class="form-group" id="tramo" >
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="tramo" name="tramo" >
            <option value="0">Tramo
                <% for (prevision pre : cp.buscarTramo()) {

                        if (pre.getId_prevision() == t) {%>
            <option selected value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>  
                <%} else {%>

            <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                <%}
                    }%>
        </select>
    </div>
</div>         



