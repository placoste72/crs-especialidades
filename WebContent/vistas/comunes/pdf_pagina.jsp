<%@page import="Controlador.General"%>
<%@page import="com.lowagie.text.pdf.BaseFont"%>
<%@page import="com.lowagie.text.pdf.PdfTemplate"%>
<%@page import="com.lowagie.text.pdf.PdfGState"%>
<%@page import="com.lowagie.text.Image"%>
<%@page import="com.lowagie.text.pdf.PdfPageEventHelper"%>
<%@page import="com.lowagie.text.pdf.PdfContentByte"%>
<%@page import="com.lowagie.text.Document"%>
<%@page import="com.lowagie.text.ExceptionConverter"%>
<%@page import="com.lowagie.text.pdf.PdfWriter"%>
<%!
    public class PageNumbersWatermark extends PdfPageEventHelper {

        /**
         * An Image that goes in the header.
         */
        public Image headerImage;
        public Image headerImage2;

        public PdfGState gstate;
        public PdfTemplate tpl;
        /**
         * The font that will be used.
         */
        public BaseFont helv;
        /**
         * A template that will hold the total number of pages.
         */
        General g = new General();
        String usuario = g.getUsuarioLoggin();

        public void onOpenDocument(PdfWriter writer, Document document) {
            try {

                /*Debo colocar si es tipo de doctor tipo de firma*/
                headerImage = Image.getInstance("/logo_gobierno.jpg");
                // headerImage2 = Image.getInstance("logo_gob_2.jpg");

                gstate = new PdfGState();
                gstate.setFillOpacity(0.89f);
                gstate.setStrokeOpacity(0.3f);
                helv = BaseFont.createFont("Helvetica", BaseFont.WINANSI, false);
                tpl = writer.getDirectContent().createTemplate(100, 100);
            } catch (Exception e) {
                throw new ExceptionConverter(e);
            }
        }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte cb = writer.getDirectContentUnder();
            String text = "";
            if (writer.getPageNumber() % 2 == 0 ) {
                text = "Copia 2 Establecimiento ";
            } else {
                text = "Copia 1 Paciente ";
            } 

            float textSize = helv.getWidthPoint(text, 8);
             float textBase = document.bottom() - 10;
            float adjust = helv.getWidthPoint("0", 12);
            cb.beginText();
            cb.setFontAndSize(helv, 8);
            cb.setTextMatrix(document.right() - textSize - adjust, textBase);
            cb.showText(text);
            cb.endText();
            cb.addTemplate(tpl, document.right() - adjust, textBase);

            cb.setGState(gstate);

            try {
                //  cb.addImage(headerImage2, 240, 0, 0, 89, 70, 50);
                cb.addImage(headerImage, 60, 0, 0, 50, 60, 720);
            } catch (Exception e) {
                throw new ExceptionConverter(e);
            }
        }

        public void onCloseDocument(PdfWriter writer, Document document) {
            tpl.beginText();
            tpl.setFontAndSize(helv, 8);
            tpl.setTextMatrix(0, 0);
            //tpl.showText("" + (writer.getPageNumber() - 1));
            tpl.endText();
        }
    }
%>
