<%-- 
    Document   : busquedadepaciente
    Created on : 11-07-2017, 10:29:37
    Author     : a
--%>

<%@page import="Modelos.prevision"%>
<%@page import="Modelos.lugar"%>
<%@page import="java.lang.NullPointerException"%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>

<%@include file="conexion.jsp" %>

<!DOCTYPE html>

<%    String rut = request.getParameter("paciente");
    String rutsinpunto = request.getParameter("rut");
    String dv = request.getParameter("dv");

    controlador_paciente cp = new controlador_paciente();
    String link = "http://10.8.4.11:9090/Especialidades/vistas/comunes/buscardatosdeFonasa.jsp";
    //String link = "http://localhost:9090/Especialidades/vistas/comunes/buscardatosdeFonasa.jsp";
    paciente p = new paciente();

    String nombre = "";
    String apellidop = "";
    String apellidom = "";
    String fechanacimiento = "";
    String genero = "";
    String previcion = "";
    /*1 fonasa , 2 isapre, 3 praim*/
    int codigoprevicion = 0;
    String error = "";
    String fecha = "";
    int generoe = 0;
    int t = 0;
    String tramo = "";
    String email = "";
    int comuna = 0;
    int region = 0;
    int provincia = 0;
    String direccion = "";
    String telefono1 = "";
    String telefono2 = "";

    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    if ((request.getParameter("buscar") == "") || (request.getParameter("buscar") == null)) {

        if (request.getParameter("error") != null) {
            error = request.getParameter("error");
            if (Integer.parseInt(error) == 0) {
                nombre = request.getParameter("nombre");
                apellidop = request.getParameter("apellidop");
                apellidom = request.getParameter("apellidom");
                fechanacimiento = request.getParameter("fechanacimiento");
                String FEC_NAC_ano = fechanacimiento.substring(0, 4);
                String FEC_NAC_mes = fechanacimiento.substring(5, 7);
                String FEC_NAC_dia = fechanacimiento.substring(8, 10);
                fecha = FEC_NAC_dia + "/" + FEC_NAC_mes + "/" + FEC_NAC_ano;
                genero = request.getParameter("genero");
                tramo = request.getParameter("tramo");
                if (genero.equalsIgnoreCase("F")) {
                    generoe = 1;
                } else {
                    generoe = 2;
                }
                codigoprevicion = Integer.parseInt(request.getParameter("previcion"));
                previcion = request.getParameter("descripcionprevicion");
                if (codigoprevicion == 1) {
                    if (tramo.equalsIgnoreCase(tramo)) {
                        if (tramo.equalsIgnoreCase("A")) {
                            t = 1;
                        } else if (tramo.equalsIgnoreCase("B")) {
                            t = 2;
                        } else if (tramo.equalsIgnoreCase("C")) {
                            t = 3;
                        } else if (tramo.equalsIgnoreCase("D")) {
                            t = 4;
                        }
                    }
                } else {
                    t = 0;
                }
                boolean existe = cp.buscarpaciente(rut);
                if (existe == true) {
                    p = cp.buscarpacienteporrut(rut);
                    codigoprevicion = p.getProvision();
                    region = p.getId_region();
                    comuna = p.getId_comuna();
                    provincia = p.getId_provincia();

                    email = p.getEmail();
                    direccion = p.getDireccion();
                    telefono1 = p.getContacto1();
                    telefono2 = p.getContacto2();

                }

            } else {

                boolean existe = cp.buscarpaciente(rut);
                if (existe == true) {
                    p = cp.buscarpacienteporrut(rut);
                    nombre = p.getNombre();
                    apellidop = p.getApellido_paterno();
                    apellidom = p.getApellido_moderno();
                    fecha = formatoFecha.format(p.getFecha_nacimiento());
                    generoe = p.getGenero();
                    codigoprevicion = p.getProvision();
                    email = p.getEmail();
                    direccion = p.getDireccion();
                    region = p.getId_region();
                    comuna = p.getId_comuna();
                    provincia = p.getId_provincia();

                    email = p.getEmail();
                    direccion = p.getDireccion();
                    telefono1 = p.getContacto1();
                    telefono2 = p.getContacto2();

                }
            }

        } else {

            try {

                response.sendRedirect("http://10.8.4.11:9090/Fonasa/certificadofonasa?rutfonasa=" + rutsinpunto + "&dvgfonasa=" + dv + "&linkrespuesta=" + link + "&rut=" + rut);
                //response.sendRedirect("http://localhost:9090/CertificadoMotivo/certificadofonasa?rutfonasa=" + rutsinpunto + "&dvgfonasa=" + dv + "&linkrespuesta=" + link + "&rut=" + rut);

            } catch (NullPointerException e) {

                System.out.print(e);

            }

        }

    } else {
        boolean exi = cp.buscarpaciente(rut);
        if (exi == true) {
            p = cp.buscarpacienteporrut(rut);
            nombre = p.getNombre();
            apellidop = p.getApellido_paterno();
            apellidom = p.getApellido_moderno();
            fecha = formatoFecha.format(p.getFecha_nacimiento());
            generoe = p.getGenero();
            codigoprevicion = p.getProvision();
            email = p.getEmail();
            direccion = p.getDireccion();
            region = p.getId_region();
            comuna = p.getId_comuna();
            provincia = p.getId_provincia();
            telefono1 = p.getContacto1();
            telefono2 = p.getContacto2();
        }
    }
%>

<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="nombrepaciente" name="nombrepaciente"  value="<%=nombre%>" class="form-control" onkeypress="return validarsololetra(event)" placeholder="Nombre Paciente"  required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="apellidoppaciente" name="apellidoppaciente"  value="<%=apellidop%>" class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Paterno"  required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="apellidompaciente" name="apellidompaciente"  value="<%=apellidom%>" class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Materno">
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar-o bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" name="txt_inicial" id="txt_inicial" value="<%=fecha%>"  placeholder="Fecha de Nacimiento (dd/mm/yy) " class="form-control"  maxlength="13" size="12" onclick="
                javascript:  $(function () {
                    $('#txt_inicial').datepicker();
                });" onchange=" javascript:  $(function () {
                            $('#txt_inicial').datepicker();
                        });"  >
    </div>
</div>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="genero" name="genero">



            <option value="0">Genero
                <% if (generoe == 1) {
                %>
            <option selected value="<%=generoe%>">Femenino

                <%
                } else if (generoe == 2) {%>
            <option selected value="<%=generoe%>">Masculino
            <option  value="1">Femenino<%
            } else {%>
            <option  value="1">Femenino
            <option value="2"> Masculino
                <%}%>    
        </select>
    </div>  
</div>
<table>
    <tr>
        <td>
            <div class="form-group">
                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span>
                <div class="col-md-8">

                    <input type="tel" id="telefonoPaciente" value="<%=telefono1%>" name="telefonoPaciente" value="" class="form-control" placeholder="+56 9 1111 1111" maxlength="11"  required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
                </div>
            </div>
            <div class="form-group">
                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                <div class="col-md-8">

                    <input type="tel" id="telefonoPaciente1"  value="<%=telefono2%>" name="telefonoPaciente1" value="" class="form-control" placeholder="+56 9 2222 2222" maxlength="11">
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
    <div class="col-md-8">
        <input type="text" id="email" name="email" value="<%=email%>" class="form-control" placeholder="Email">
    </div>
</div>

<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon"></i></span>
    <div class="col-md-8">
        <input type="text"    size="39" style="height: 89px" id="direccion" name="direccion" value="<%=direccion%>" class="form-control" placeholder="Direccion" required oninvalid="setCustomValidity('El campo Direccion es obligatorio')" oninput="setCustomValidity('')">


    </div>
</div>




<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="region" name="region" required onchange=" return buscaProvincia()"  >
            <option value=-1>Elegir Regi�n
                <%
                    for (lugar regiones : cp.buscarregion()) {

                        if (regiones.getId_lugar() == region) {%>
            <option selected value="<%=regiones.getId_lugar()%>"><%=regiones.getNombre()%><%
            } else {%>
            <option value="<%=regiones.getId_lugar()%>"><%=regiones.getNombre()%><%
                    }

                }
                %>
        </select>
    </div> 


</div>



<div id="provincia1">
    <div class="form-group">
        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
        <div class="col-md-8"  >
            <select class="form-control" id="provincia" name="provincia" required onchange=" return buscaComuna(this.value)">
                <option value="-1">Elegir provincia
                    <%
                        for (lugar l : cp.buscarProvidencia(region)) {

                            if (l.getId_lugar() == provincia) {%>
                <option selected value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                } else {%>
                <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                        }

                    }
                    %>
            </select>
        </div>
    </div>
</div> 

<div id="comuna1">
    <div class="form-group">
        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
        <div class="col-md-8" >
            <select class="form-control" id="comuna" name="comuna" required>
                <option value="-1">Elegir Comuna
                    <%
                        for (lugar temp : cp.buscarComuna(provincia)) {

                            if (temp.getId_lugar() == comuna) {%>
                <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                } else {%>
                <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                        }

                    }
                    %>
            </select>
        </div>
    </div>


</div> 
<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >

        <select class="form-control" id="procedencia" name="procedencia" required>
            <option value="0">Elegir Consultorio
                <%
                    try {
                        String procedencia = " SELECT id_servicio_salud, nombre_servicio_salud FROM  agenda.procedencia where estado_servicio_salud = 1 ; ";

                        ResultSet rs_procedencia = st.executeQuery(procedencia);

                        while (rs_procedencia.next()) {

                            if (rs_procedencia.getInt("id_servicio_salud") == comuna) {%>
            <option selected value="<%=rs_procedencia.getInt("id_servicio_salud")%>"><%=rs_procedencia.getString("nombre_servicio_salud")%><%
            } else {%>
            <option value="<%=rs_procedencia.getInt("id_servicio_salud")%>"><%=rs_procedencia.getString("nombre_servicio_salud")%><%
                        }

                    }
                } catch (SQLException ex) {
                }        %>
        </select>

    </div>        
</div>



<div class="form-group">
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="provision" name="provision" onchange="javascript:
                        var pre = document.forms['paciente_urgencia']['provision'].value;
                if (pre == 1) {

                    document.getElementById('t').style.display = 'block';
                } else
                    document.getElementById('t').style.display = 'none';
                " required>

            <option  value="-1">Prevision
                <% for (prevision pre : cp.buscarPrevicion()) {

                         if (pre.getId_prevision() == codigoprevicion) {%>
            <option selected  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                <%} else {%>
            <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                <% }
                                }%>       
        </select>
    </div>
</div>
<div class="form-group" id="tramo1" >
    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
    <div class="col-md-8" >
        <select class="form-control" id="tramo" name="tramo" >
            <option value="0">Tramo
                <% for (prevision pre : cp.buscarTramo()) {

                        if (pre.getId_prevision() == t) {%>
            <option selected value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>  
                <%} else {%>

            <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                <%}
                    }%>
        </select>
    </div>
</div>         


