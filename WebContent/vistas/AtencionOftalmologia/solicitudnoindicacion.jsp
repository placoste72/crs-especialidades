<%-- 
    Document   : solicitud
    Created on : 31-05-2018, 13:11:47
    Author     : a
--%>

<%@page import="Modelos.tipo_anestesia"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.lugar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    controlador_especialidad ce = new controlador_especialidad();
    String cantidadsolicitud = request.getParameter("cantidad");
    String especialidad = request.getParameter("especialidad");


%>
<table class="table table-striped" style=" width: 100%">
    <thead>
        <tr>
            <th>
                Diagnostico
            </th>
            <th>
                
            </th>
            <th>
                Lateralidad
            </th>
           
            <th>
                Intervención Quirúrgica
            </th>
            <th>
                Razones por las que no requiere Cirugía
            </th>
            <th>
               Indicaciones al Paciente
            </th>
            
        </tr>
    </thead>
    <% for (int i = 0; i < Integer.parseInt(cantidadsolicitud); ++i) {%>
    <tr>

        <td>


            <select name="diagnosticonrpt<%=i%>" id="diagnosticonrpt<%=i%>" class="form-control" onchange="javascript:

                            var diagnostico = document.getElementById('diagnosticonrpt<%=i%>').value;
                    if (diagnostico == -1) {


                        document.getElementById('otrodiagnosticosicmuestro<%=i%>').style.display = 'block';
                    } else
                        document.getElementById('otrodiagnosticosicmuestro<%=i%>').style.display = 'none';" >
                        <option  value="0" selected>Diagnostico
                        <option  value="-1" >Otro
                            <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(Integer.parseInt(especialidad))) {%>
                        <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                    <% }%>  
            </select>
        </td>
        <td>
            <div class="col-md-12" id="otrodiagnosticosicmuestro<%=i%>" style=" display: none">
                <textarea type="text" style=" width: 150px"  rows="1" id="otrodiagnosticonrpt<%=i%>" name="otrodiagnosticonrpt<%=i%>"  class="form-control" placeholder="Otro Diagnostico"  ></textarea>


            </div>  

        </td>
        <td>
            <select id="lateralidadesnrpt<%=i%>" name="lateralidadesnrpt<%=i%>" onchange="javascript:

                            var diagnostico = document.getElementById('diagnosticonrpt<%=i%>').value;
                             var lateralidad = document.getElementById('lateralidadesnrpt<%=i%>').value;
                                var la = '';
                                if (lateralidad == 1) {
                                    la = 'OJO DERECHO';
                                } else if (lateralidad == 2) {
                                    la = 'OJO IZQUIERDO';
                                }

                                if ((diagnostico == 2) || (diagnostico == 31)) {
                                    document.getElementById('intervencionnrpt<%=i%>').value = 'FACOERESIS CON IMPLANTE INTRAOCULAR ' + la;
                                } else {
                                    document.getElementById('intervencionnrpt<%=i%>').value = '';
                                }
                    ">
                <option selected value="0" >Lateralidad</option>

                <option value="1">Ojo Derecho</option>
                <option value="2">Ojo Izquierdo</option>

            </select>
        </td>

        <td>
            <textarea rows="2" cols="30" name="intervencionnrpt<%=i%>" id="intervencionnrpt<%=i%>" ></textarea>
        </td>
       
        <td>
            <textarea rows="2" cols="30" name="razonesnrpt<%=i%>" id="razonesnrpt<%=i%>" > </textarea>
        </td>
        <td>
            <textarea rows="2" cols="30" name="indicacionesnrpt<%=i%>" id="indicacionesnrpt<%=i%>" ></textarea>
        </td>

    </tr>
    <%}%> 

</table>


