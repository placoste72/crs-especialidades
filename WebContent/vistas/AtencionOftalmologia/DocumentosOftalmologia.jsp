<%-- 
    Document   : Documentos
    Created on : 17-07-2017, 13:54:31
    Author     : a
--%>


<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Modelos.atencion"%>
<%@page import="Controlador.controlador_atencion"%>


<!DOCTYPE html>
<head>


    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


    <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

    <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
    <link href="../../public/css/estilos.css" rel="stylesheet">
    <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
    <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
    <script src="../../public/js/crs.js" type="text/javascript"></script>

    <script src="../../public/js/jquery.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">-->

    <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
    <link href="../../public/css/estilos.css" rel="stylesheet">
    <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
    <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
    <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>

</head>

<%

    controlador_atencion ca = new controlador_atencion();
    controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
    String idatencion = request.getParameter("idatencion");
    atencion tengo = ca.BuscarquedocumentosTieneAtencion(Integer.parseInt(idatencion));
    boolean tengoreceta = caco.tengorecetaIdAtencion(Integer.parseInt(idatencion), 1);


%>
<div class="container"> 
    <form name="buscarsic" class="form-horizontal" id="buscarsic" action='' method="post"  >

        <br>
        <fieldset>
            <legend class="text-center header">Documentos a Imprimir</legend>
            <br>
            <br>
            <table class="table table-striped" style="width: 100%"  >

                <thead>
                    <tr>
                        <th><p  class="letra" > Informe de Atenci�n <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>InformeAtencionOftalmologia?idatencion=<%=idatencion%>', 'Informe de Atencion', 'width=600,height=450')"/></p></th>

                        <%/*1,2,3,4,5*/
                            if (tengo.isSolicitudpabellon() == true) {%>
                        <th><p  class="letra" > Consentimiento Informador Intervenciones Quir�rgicas<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>IntervencionQuirurgicas?idatencion=<%=idatencion%>', 'Consentimiento Informador Intervenciones Quir�rgicas', 'width=600,height=450')"/></p></th>
                        <th><p  class="letra" > Solicitud de Pabellon Quirurgico  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>SolicitudPabellonQuirurgico?idatencion=<%=idatencion%>', 'Solicitud de Pabellon Quirurgico', 'width=600,height=450')"/></p></th>

                        <%
                            }
                            if (tengo.isContgipd() == true) {%>
                        <th><p  class="letra" > Constancia Ges  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>constanciaGes?idatencion=<%=idatencion%>', 'Constancia Ges ', 'width=600,height=450')"/></p></th>

                        <%}
                            if (tengo.isTengoreceta() == true) {%>
                        <th><p  class="letra" > Receta Medica<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>RecetaMedica?idatencion=<%=idatencion%>', 'Receta Medica', 'width=600,height=450')"/></p></th>      

                        <%}
                            if (tengo.isTengoipd() == true) {%>
                        <th><p  class="letra" > IPD  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>InformeProcesoDiagnostico?idatencion=<%=idatencion%>', 'IPD', 'width=600,height=450')"/></p></th>

                        <%}
                            if (tengo.getSic() == 1) {%>
                        <th><p  class="letra" > Solicitud de Interconsulta  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>SolicitudInterconsulta?idatencion=<%=idatencion%>', 'Solicitud de Interconsulta', 'width=600,height=450')"/></p></th>
                                <%}

                                    if (tengo.isTengoindicacionnoquirurjica() == true) {%>
                        <th><p  class="letra" > Informe de no indicaci�n de cirug�a  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ca.getLocal()%>InformenoIndicicaciondeCirugia?idatencion=<%=idatencion%>', 'Informe de no indicaci�n de cirug�a', 'width=600,height=450')"/></p></th>
                                <%}

                                    if (tengo.isTengoexcepcion() == true) {%>
                        <th><p  class="letra" > Excepcion de Garantia  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:    window.open('<%=ca.getLocal()%>ExcepciondeGarantia?idatencion=<%=idatencion%>&tipo=2', 'Excepcion de Garantia', 'width=600,height=450')"/></p></th>
                                <%}
                                    if (tengo.isTengosolicitu() == true) {%>
                        <th><p  class="letra" > Solicitud de Examenes o Procedimiento Oftalmologico <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:    window.open('<%=ca.getLocal()%>solicituddeexamenesoftalmologico?idatencion=<%=idatencion%>', 'Solicitud de Examenes o Procedimiento Oftalmologico', 'width=600,height=450')"/></p></th>
                                <%}%>

                        <%if (tengoreceta == true) {%>
                        <th><p class="letra"> Receta de Lentes<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=caco.getLocal()%>RecetadeLenteOpticos?idatencion=<%=idatencion%>&tipo=1', 'Receta de Lente', 'width=600,height=450')"/></p> </th>


                        <%}%>
                        </tr>


                    </thead>
                </table>  
            </fieldset>
        </form>
        <button type="button" style="margin-left:  500px" class="btn btn-primary" onclick="javascript : location.href = '<%=ca.getLocallink()%>AtencionOftalmologia/ListaPacientes.jsp';">Atender Paciente</button>
                        </div> 
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>


