
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.hojadiaria"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@ page import="java.io.FileOutputStream,java.io.*,java.awt.Color,java.util.Vector,java.util.Date,java.text.DateFormat,java.util.Locale" %>
<%@ page import="com.lowagie.text.*,  com.lowagie.text.pdf.*,com.lowagie.text.pdf.PdfStamper,com.lowagie.text.pdf.PdfPageEventHelper" %>
<%@ page import="com.lowagie.text.pdf.PdfEncryptor,com.lowagie.text.pdf.PdfReader,com.lowagie.text.pdf.PdfWriter,com.lowagie.text.Chunk" %>
<%@ page import="com.lowagie.text.Document,com.lowagie.text.Element,com.lowagie.text.ExceptionConverter,com.lowagie.text.Font"%>
<%@ page import="com.lowagie.text.Image,com.lowagie.text.PageSize,com.lowagie.text.Rectangle,com.lowagie.text.pdf.BaseFont"%>
<%@ page import="com.lowagie.text.pdf.PdfContentByte,com.lowagie.text.pdf.PdfGState,com.lowagie.text.pdf.PdfPTable" %>
<%@ page import="com.lowagie.text.HeaderFooter,com.lowagie.text.Header,com.lowagie.text.pdf.PdfWriter"%>
<%@ page import="java.sql.*,java.util.GregorianCalendar,java.util.Calendar" %>

<%@include file="../comunes/pdf_pagina.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%    Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
    DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);



    String llego = request.getParameter("idhojadiaria");

   controlador_especialidad ce = new controlador_especialidad();
   hojadiaria hd = ce.BuscarconstanciaGes(Integer.parseInt(llego));
   Vector<lugar> vl = ce.buscarlosdiagnosticosdeunaHojaDiaria(Integer.parseInt(llego));
    response.setContentType("application/pdf");
    Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    PdfWriter writer = PdfWriter.getInstance(document, buffer);

    int SPACE_TITULO = 1;
    int SPACE_NORMAL = 1;
    int SPACE_NORMAL2 = 4;
    int SPACE_ESPACIO = 10;
   
    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(68, 117, 196));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
    Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(0, 0, 0));
    Font TEXT_NORMAL2N = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, new Color(0, 0, 0));
    Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0, 0, 0));
    Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL, new Color(0, 0, 0));
     writer.setPageEvent(new PageNumbersWatermark());
    document.open();

    Table tabla_titulo;
    Cell celda;

    tabla_titulo = new Table(3);
    tabla_titulo.setBorderWidth(0);
    tabla_titulo.setPadding(0);
    tabla_titulo.setSpacing(0);
    tabla_titulo.setWidth(100);

    String serviciot = "";
    String establecimientot = "";
    String especialidadt = "";
    String unidadt = "";
    String direccionyciudad = "";
    String constanciat = "";
    serviciot = "Metropolitano Central(SSMC)";
    establecimientot = "Centro de Referencia de Salud de Maipú";
    direccionyciudad = "Av. Camino a Rinconada 1001, Maipú, Santiago";
    especialidadt = " Tecnólogo";
    unidadt = " Oftalmología";
    constanciat = "Declaro que, con esta fecha y hora, he tomado conocimiento que tengo derecho a acceder a las "
            + "Garantías explícitas en Salud, GES , siempre que la atención sea otorgada en la Red de prestadores "
            + "que me corresponde según FONASA o la ISAPRE a la que me encuentro adscrito.";
    String importante = "";

    importante = "El paciente debe tener presente que si no se cumplen las garantías, puede reclamar ante el FONASA o la "
            + "ISAPRE, según corresponda. Si la respuesta no es satisfactoria, puede recurrir en segunda instancia a la Superintendencia"
            + "de Salud.";

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "FORMULARIO DE CONSTANCIA INFORMACIÓN AL PACIENTE GES", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph rutCita = new Paragraph();
    rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, "Folio:", TEXT_NORMAL));
    rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, hd.getOtras_indicaciones(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(rutCita);
    celda.setBorderWidth(0);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph nombreCita = new Paragraph();
    nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, "Fecha y hora de la solicitud: ", TEXT_NORMAL));
    nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, hd.getFecha_registro().toString(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombreCita);
    celda.setBorderWidth(0);

    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Datos del prestador", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph fechaCita = new Paragraph();
    fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, "Servicio de Salud: ", TEXT_NORMAL));
    fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, serviciot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(fechaCita);
    celda.setBorderWidth(0);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph horaCita = new Paragraph();
    horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, "Establecimiento: ", TEXT_NORMAL));
    horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, establecimientot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(horaCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph especialidadCita = new Paragraph();
    especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, "Dirección y Ciudad:", TEXT_NORMAL));
    especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, direccionyciudad, TEXT_SUPERTITULONORMAL));
    celda = new Cell(especialidadCita);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph medicoCita = new Paragraph();
    medicoCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    medicoCita.add(new Phrase(SPACE_TITULO, "Nombre persona que notifica en representación del prestador:", TEXT_NORMAL));
    medicoCita.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
    // medicoCita.add(new Phrase(SPACE_TITULO, "Dr."+nombredoctort + "," + especialidadt + ", RUT " + rutdoctort, TEXT_SUPERTITULONORMAL));
    celda = new Cell(medicoCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph medCita = new Paragraph();
    medCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // medicoCita.add(new Phrase(SPACE_TITULO, "Nombre persona que notifica en representación del prestador:", TEXT_NORMAL));
    medCita.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
    medCita.add(new Phrase(SPACE_TITULO, "Profesional." + hd.getOtro_diagnostico() + ", RUT " + hd.getOtro_examen(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(medCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /*datos del paciente*/
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Antecedentes del Paciente", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph AtencionGlosa = new Paragraph();
    AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
    AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, hd.getOtro_procedimiento(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa);
    //celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph AtencionGlosa1 = new Paragraph();
    AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
    AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa1.add(new Phrase(SPACE_TITULO, hd.getOtrodestino_paciente(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa1);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph domicilio = new Paragraph();
    domicilio.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    domicilio.add(new Phrase(SPACE_TITULO, "Domicilio: ", TEXT_NORMAL));
    domicilio.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    domicilio.add(new Phrase(SPACE_TITULO, hd.getAuxiliar1(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(domicilio);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph telefonos = new Paragraph();
    telefonos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    telefonos.add(new Phrase(SPACE_TITULO, "Telefonos: ", TEXT_NORMAL));
    telefonos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    telefonos.add(new Phrase(SPACE_TITULO, hd.getAuxiliar2(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(telefonos);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph correo = new Paragraph();
    correo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    correo.add(new Phrase(SPACE_TITULO, "Correo electronico: ", TEXT_NORMAL));
    correo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    correo.add(new Phrase(SPACE_TITULO, hd.getAuxiliar3(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(correo);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /*Datos de derivacion*/
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Información Médica", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph sederiva = new Paragraph();
    sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    sederiva.add(new Phrase(SPACE_TITULO, "Confirmación diagnóstico GES (Problema de Salud - Patología): ", TEXT_NORMAL));
    sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
   
  
    sederiva.add(new Phrase(SPACE_TITULO, "___________________________________________________________________________________________________________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(sederiva);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph seenvia = new Paragraph();
    seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    seenvia.add(new Phrase(SPACE_TITULO, "[ ] Confirmación Diagnóstica  ", TEXT_NORMAL));
    seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // seenvia.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
    celda = new Cell(seenvia);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph see = new Paragraph();
    see.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
   
        see.add(new Phrase(SPACE_TITULO, "[ ] Tratamiento", TEXT_NORMAL));
   

    see.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // seenvia.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
    celda = new Cell(see);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Constancia", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph nombredoctor = new Paragraph();
    nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
    nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombredoctor.add(new Phrase(SPACE_TITULO, constanciat, TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombredoctor);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /*firmas y pie*/
    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    Paragraph raya = new Paragraph();
    raya.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));

    raya.add(new Phrase(SPACE_TITULO, "_______________________________                _______________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(raya);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    /*pie de documento*/
    Paragraph pie = new Paragraph();
    pie.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    pie.add(new Phrase(SPACE_TITULO, "Informé diagnóstico GES                         Tomé conocimiento", TEXT_SUPERTITULO));
    // pie.add(new Phrase(SPACE_TITULO, "Firma de la persona que notifica                                    Firma o huella digital del paciente o representante", TEXT_NORMAL2));

    celda = new Cell(pie);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    Paragraph piet = new Paragraph();
    piet.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    //  piet.add(new Phrase(SPACE_TITULO, " Informé diagnóstico GES             Tomé conocimiento", TEXT_SUPERTITULO));
    piet.add(new Phrase(SPACE_TITULO, "Firma de la persona que notifica                                                      Firma o huella digital del paciente o representante", TEXT_NORMAL2));

    celda = new Cell(piet);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    Paragraph pieimpt = new Paragraph();
    pieimpt.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    pieimpt.add(new Phrase(SPACE_NORMAL, "Importante: ", TEXT_NORMAL2N));
    pieimpt.add(new Phrase(SPACE_NORMAL, importante, TEXT_NORMAL2));

    celda = new Cell(pieimpt);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph raya2 = new Paragraph();
    raya2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    raya2.add(new Phrase(SPACE_TITULO, "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------", TEXT_SUPERTITULONORMAL));
    celda = new Cell(raya2);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph conocimiento = new Paragraph();
    conocimiento.add(new Phrase(SPACE_NORMAL2, "En Caso que la persona que tomó conocimiento no sea el paciente, debe identificar los siguientes datos:", TEXT_SUPERTITULONORMAL));
    // nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
    conocimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

    celda = new Cell(conocimiento);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph piecentro = new Paragraph();
    piecentro.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    piecentro.add(new Phrase(SPACE_TITULO, "Antecedentes del representante", TEXT_SUPERTITULO));
    // pie.add(new Phrase(SPACE_TITULO, "Firma de la persona que notifica                                    Firma o huella digital del paciente o representante", TEXT_NORMAL2));

    celda = new Cell(piecentro);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    Paragraph nc = new Paragraph();
    nc.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nc.add(new Phrase(SPACE_TITULO, "Nombre Completo", TEXT_NORMAL));
    nc.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nc.add(new Phrase(SPACE_TITULO, "             :__________________________________________________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(nc);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph nct = new Paragraph();
    nct.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nct.add(new Phrase(SPACE_TITULO, "RUT", TEXT_NORMAL));
    nct.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nct.add(new Phrase(SPACE_TITULO, "                                   :____________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(nct);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph tnct = new Paragraph();
    tnct.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnct.add(new Phrase(SPACE_TITULO, "Teléfonos de contacto", TEXT_NORMAL));
    tnct.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnct.add(new Phrase(SPACE_TITULO, "      :Fijo____________________________________;Celular_____________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(tnct);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph tnctce = new Paragraph();
    tnctce.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnctce.add(new Phrase(SPACE_TITULO, "Correo electrónico", TEXT_NORMAL));
    tnctce.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnctce.add(new Phrase(SPACE_TITULO, "            :___________________________________________________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(tnctce);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    /*Seguda Hoja*/

 /*Segunda pagina*/
    document.newPage();
    document.add(tabla_titulo);

    Table tabla_titulo2;

    tabla_titulo2 = new Table(3);
    tabla_titulo2.setBorderWidth(0);
    tabla_titulo2.setPadding(0);
    tabla_titulo2.setSpacing(0);
    tabla_titulo2.setWidth(100);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "FORMULARIO DE CONSTANCIA INFORMACIÓN AL PACIENTE GES", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    /**/
    Paragraph rutCita2 = new Paragraph();
    rutCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutCita2.add(new Phrase(SPACE_TITULO, "Folio:", TEXT_NORMAL));
    rutCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutCita2.add(new Phrase(SPACE_TITULO, hd.getOtras_indicaciones(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(rutCita2);
    celda.setBorderWidth(0);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    /**/
    Paragraph nombreCita2 = new Paragraph();
    nombreCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombreCita2.add(new Phrase(SPACE_TITULO, "Fecha y hora de la solicitud: ", TEXT_NORMAL));
    nombreCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombreCita2.add(new Phrase(SPACE_TITULO,  hd.getFecha_registro().toString(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombreCita2);
    celda.setBorderWidth(0);

    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Datos del prestador", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph fechaCita2 = new Paragraph();
    fechaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechaCita2.add(new Phrase(SPACE_TITULO, "Servicio de Salud: ", TEXT_NORMAL));
    fechaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechaCita2.add(new Phrase(SPACE_TITULO, serviciot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(fechaCita2);
    celda.setBorderWidth(0);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph horaCita2 = new Paragraph();
    horaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    horaCita2.add(new Phrase(SPACE_TITULO, "Establecimiento: ", TEXT_NORMAL));
    horaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    horaCita2.add(new Phrase(SPACE_TITULO, establecimientot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(horaCita2);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph especialidadCita2 = new Paragraph();
    especialidadCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    especialidadCita2.add(new Phrase(SPACE_TITULO, "Dirección y Ciudad:", TEXT_NORMAL));
    especialidadCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    especialidadCita2.add(new Phrase(SPACE_TITULO, direccionyciudad, TEXT_SUPERTITULONORMAL));
    celda = new Cell(especialidadCita2);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph medicoCita2 = new Paragraph();
    medicoCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    medicoCita2.add(new Phrase(SPACE_TITULO, "Nombre persona que notifica en representación del prestador:", TEXT_NORMAL));
    medicoCita2.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
    // medicoCita.add(new Phrase(SPACE_TITULO, "Dr."+nombredoctort + "," + especialidadt + ", RUT " + rutdoctort, TEXT_SUPERTITULONORMAL));
    celda = new Cell(medicoCita2);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph medCita2 = new Paragraph();
    medCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // medicoCita.add(new Phrase(SPACE_TITULO, "Nombre persona que notifica en representación del prestador:", TEXT_NORMAL));
    medCita2.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
    medCita2.add(new Phrase(SPACE_TITULO, "Profesional." + hd.getOtro_diagnostico()+ ", RUT " + hd.getOtro_examen(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(medCita2);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    /*datos del paciente*/
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Antecedentes del Paciente", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph AtencionGlosa2 = new Paragraph();
    AtencionGlosa2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa2.add(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
    AtencionGlosa2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa2.add(new Phrase(SPACE_TITULO, hd.getOtro_procedimiento(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa2);
    //celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph AtencionGlosa12 = new Paragraph();
    AtencionGlosa12.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa12.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
    AtencionGlosa12.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa12.add(new Phrase(SPACE_TITULO, hd.getOtrodestino_paciente(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa12);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph domicilio2 = new Paragraph();
    domicilio2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    domicilio2.add(new Phrase(SPACE_TITULO, "Domicilio: ", TEXT_NORMAL));
    domicilio2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    domicilio2.add(new Phrase(SPACE_TITULO,hd.getAuxiliar1(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(domicilio2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph telefonos2 = new Paragraph();
    telefonos2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    telefonos2.add(new Phrase(SPACE_TITULO, "Telefonos: ", TEXT_NORMAL));
    telefonos2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    telefonos2.add(new Phrase(SPACE_TITULO, hd.getAuxiliar2(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(telefonos2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph correo2 = new Paragraph();
    correo2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    correo2.add(new Phrase(SPACE_TITULO, "Correo electronico: ", TEXT_NORMAL));
    correo2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    correo2.add(new Phrase(SPACE_TITULO, hd.getAuxiliar3(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(correo2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    /*Datos de derivacion*/
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Información Médica", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph sederiva2 = new Paragraph();
    sederiva2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    sederiva2.add(new Phrase(SPACE_TITULO, "Confirmación diagnóstico GES (Problema de Salud - Patología): ", TEXT_NORMAL));
    sederiva2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    sederiva2.add(new Phrase(SPACE_TITULO,"________________________________________________________________________________________________________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(sederiva2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph seenvia2 = new Paragraph();
    seenvia2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    seenvia2.add(new Phrase(SPACE_TITULO, "[ ] Confirmación Diagnóstica  ", TEXT_NORMAL));
    seenvia2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // seenvia.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
    celda = new Cell(seenvia2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph see2 = new Paragraph();
    see2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
   
        see2.add(new Phrase(SPACE_TITULO, "[ ] Tratamiento", TEXT_NORMAL));

   
    see2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // seenvia.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
    celda = new Cell(see);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Constancia", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph nombredoctor2 = new Paragraph();
    nombredoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
    nombredoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombredoctor2.add(new Phrase(SPACE_TITULO, constanciat, TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombredoctor);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    /*firmas y pie*/
    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo2.addCell(celda);

    Paragraph raya22 = new Paragraph();
    raya22.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    // nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));

    raya22.add(new Phrase(SPACE_TITULO, "_______________________________                _______________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(raya22);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo2.addCell(celda);

    /*pie de documento*/
    Paragraph pie2 = new Paragraph();
    pie2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    pie2.add(new Phrase(SPACE_TITULO, "Informé diagnóstico GES                         Tomé conocimiento", TEXT_SUPERTITULO));
    // pie.add(new Phrase(SPACE_TITULO, "Firma de la persona que notifica                                    Firma o huella digital del paciente o representante", TEXT_NORMAL2));

    celda = new Cell(pie2);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo2.addCell(celda);

    Paragraph piet2 = new Paragraph();
    piet2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    //  piet.add(new Phrase(SPACE_TITULO, " Informé diagnóstico GES             Tomé conocimiento", TEXT_SUPERTITULO));
    piet2.add(new Phrase(SPACE_TITULO, "Firma de la persona que notifica                                                      Firma o huella digital del paciente o representante", TEXT_NORMAL2));

    celda = new Cell(piet2);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo2.addCell(celda);

    Paragraph pieimpt2 = new Paragraph();
    pieimpt2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    pieimpt2.add(new Phrase(SPACE_NORMAL, "Importante: ", TEXT_NORMAL2N));
    pieimpt2.add(new Phrase(SPACE_NORMAL, importante, TEXT_NORMAL2));

    celda = new Cell(pieimpt2);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph raya223 = new Paragraph();
    raya223.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    raya223.add(new Phrase(SPACE_TITULO, "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------", TEXT_SUPERTITULONORMAL));
    celda = new Cell(raya223);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph conocimiento2 = new Paragraph();
    conocimiento2.add(new Phrase(SPACE_NORMAL2, "En Caso que la persona que tomó conocimiento no sea el paciente, debe identificar los siguientes datos:", TEXT_SUPERTITULONORMAL));
    // nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
    conocimiento2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

    celda = new Cell(conocimiento2);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph piecentro2 = new Paragraph();
    piecentro2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    piecentro2.add(new Phrase(SPACE_TITULO, "Antecedentes del representante", TEXT_SUPERTITULO));
    // pie.add(new Phrase(SPACE_TITULO, "Firma de la persona que notifica                                    Firma o huella digital del paciente o representante", TEXT_NORMAL2));

    celda = new Cell(piecentro2);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo2.addCell(celda);

    Paragraph nc2 = new Paragraph();
    nc2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nc2.add(new Phrase(SPACE_TITULO, "Nombre Completo", TEXT_NORMAL));
    nc2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nc2.add(new Phrase(SPACE_TITULO, "             :__________________________________________________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(nc2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph nct2 = new Paragraph();
    nct2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nct2.add(new Phrase(SPACE_TITULO, "RUT", TEXT_NORMAL));
    nct2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nct2.add(new Phrase(SPACE_TITULO, "                                   :____________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(nct2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph tnct2 = new Paragraph();
    tnct2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnct2.add(new Phrase(SPACE_TITULO, "Teléfonos de contacto", TEXT_NORMAL));
    tnct2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnct2.add(new Phrase(SPACE_TITULO, "      :Fijo____________________________________;Celular_____________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(tnct2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);

    Paragraph tnctce2 = new Paragraph();
    tnctce2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnctce2.add(new Phrase(SPACE_TITULO, "Correo electrónico", TEXT_NORMAL));
    tnctce2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    tnctce2.add(new Phrase(SPACE_TITULO, "            :___________________________________________________________________________", TEXT_SUPERTITULONORMAL));
    celda = new Cell(tnctce2);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo2.addCell(celda);
    document.add(tabla_titulo2);

    document.close();
    try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
        DataOutput output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
//output.flush();            
//output.close();
    } catch (Exception exstream) {
    }

%>