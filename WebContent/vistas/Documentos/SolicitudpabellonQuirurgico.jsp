

<%@page import="Modelos.lugar"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.controlador_doctor"%>
<%@ page import="java.io.FileOutputStream,java.io.*,java.awt.Color,java.util.Vector,java.util.Date,java.text.DateFormat,java.util.Locale" %>
<%@ page import="com.lowagie.text.*,  com.lowagie.text.pdf.*,com.lowagie.text.pdf.PdfStamper,com.lowagie.text.pdf.PdfPageEventHelper" %>
<%@ page import="com.lowagie.text.pdf.PdfEncryptor,com.lowagie.text.pdf.PdfReader,com.lowagie.text.pdf.PdfWriter,com.lowagie.text.Chunk" %>
<%@ page import="com.lowagie.text.Document,com.lowagie.text.Element,com.lowagie.text.ExceptionConverter,com.lowagie.text.Font"%>
<%@ page import="com.lowagie.text.Image,com.lowagie.text.PageSize,com.lowagie.text.Rectangle,com.lowagie.text.pdf.BaseFont"%>
<%@ page import="com.lowagie.text.pdf.PdfContentByte,com.lowagie.text.pdf.PdfGState,com.lowagie.text.pdf.PdfPTable" %>
<%@ page import="com.lowagie.text.HeaderFooter,com.lowagie.text.Header,com.lowagie.text.pdf.PdfWriter"%>
<%@ page import="java.sql.*,java.util.GregorianCalendar,java.util.Calendar" %>

<%@include file="../comunes/pdfconpieyfoto.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%    Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
    DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

    String llego = request.getParameter("idhojadiaria");
    controlador_especialidad ce = new controlador_especialidad();
    lugar l = ce.BuscardatosparaSoliciatudpabellon(Integer.parseInt(llego));
    Vector<lugar> diagnosticos = ce.buscarlosdiagnosticosdeunaHojaDiaria(Integer.parseInt(llego));
    response.setContentType("application/pdf");
    Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    PdfWriter writer = PdfWriter.getInstance(document, buffer);

    int SPACE_TITULO = 5;

    int SPACE_NORMAL2 = 17;
    int SPACE_ESPACIO = 4;
    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD, new Color(68, 117, 196));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(0, 0, 0));
    Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0, 0, 0));
    Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(0, 0, 0));
    //Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
    writer.setPageEvent(new PageNumbersWatermark());
    document.open();

    for (lugar solicitud : ce.buscarLasSolicitudesdePabellondeyunaHojaDiaria(Integer.parseInt(llego))) {

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        String serviciot = "";
        String establecimientot = "";
        String especialidadt = "";
        String unidadt = "";

        serviciot = "Metropolitano Central(SSMC)";
        establecimientot = "Centro de Referencia de Salud de Maipú";
        especialidadt = "Oftalmología";
        unidadt = "Especialidades";

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "SOLICITUD DE PABELLÓN QUIRURGICO", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph nombreCita = new Paragraph();
        nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita.add(new Phrase(SPACE_TITULO, "Fecha y hora del Informe: ", TEXT_NORMAL));
        nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita.add(new Phrase(SPACE_TITULO, l.getDescripcion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombreCita);
        celda.setBorderWidth(0);

        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa = new Paragraph();
        AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre Usuario: ", TEXT_NORMAL));
        AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa.add(new Phrase(SPACE_TITULO, l.getNombre(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa);
        //celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa1 = new Paragraph();
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, l.getRut(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph edad = new Paragraph();
        edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
        edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad.add(new Phrase(SPACE_TITULO, l.getEdad() + " años", TEXT_SUPERTITULONORMAL));
        celda = new Cell(edad);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        String diagnos = "";
        for (int i = 0; i < diagnosticos.size(); i++) {
            diagnos = diagnos + diagnosticos.get(i).getDiagnostico() + " ";
        }

        Paragraph sexo = new Paragraph();
        sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo.add(new Phrase(SPACE_TITULO, "Diagnostico: ", TEXT_NORMAL));
        sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo.add(new Phrase(SPACE_TITULO, solicitud.getComuna(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(sexo);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph sederiva = new Paragraph();
        sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva.add(new Phrase(SPACE_TITULO, "Intervencion: ", TEXT_NORMAL));
        sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva.add(new Phrase(SPACE_TITULO, solicitud.getIntervencio(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(sederiva);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph seenvia = new Paragraph();
        seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia.add(new Phrase(SPACE_TITULO, "Anestesia Solicitada: ", TEXT_NORMAL));
        seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia.add(new Phrase(SPACE_TITULO, solicitud.getDireccion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(seenvia);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph fd = new Paragraph();
        fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fd.add(new Phrase(SPACE_TITULO, "Requerimientos Especiales: ", TEXT_NORMAL));
        fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fd.add(new Phrase(SPACE_TITULO, solicitud.getDiagnostico(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(fd);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph examenes = new Paragraph();
        examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        examenes.add(new Phrase(SPACE_TITULO, "Observaciones: ", TEXT_NORMAL));
        examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        examenes.add(new Phrase(SPACE_TITULO, solicitud.getIndicaciones(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(examenes);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph nombredoctor = new Paragraph();
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, "Cirujano Solicitante: ", TEXT_NORMAL));
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, l.getRutdoctor() + " " + l.getNombredoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombredoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph firmadoctor = new Paragraph();
        firmadoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        firmadoctor.add(new Phrase(SPACE_TITULO, "_______________________ ", TEXT_NORMAL));
        firmadoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        firmadoctor.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        celda = new Cell(firmadoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        Paragraph firmadoctor2 = new Paragraph();
        firmadoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        firmadoctor2.add(new Phrase(SPACE_TITULO, "Firma o timbre Cirujano ", TEXT_NORMAL));
        firmadoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        firmadoctor2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        celda = new Cell(firmadoctor2);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        document.add(tabla_titulo);
        document.newPage();

    }

    document.close();
    try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
        DataOutput output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
//output.flush();            
//output.close();
    } catch (Exception exstream) {
    }

%>