<%-- 
    Document   : ConfirmarCita
    Created on : 15-12-2016, 04:55:08 PM
    Author     : Informatica
--%>


<%@page import="Controlador.controlador_cita"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp" />
<%
    controlador_cita cc = new controlador_cita();

%>
<script>


    function confirmar(cita) {
        var telefono = document.getElementById('telefonoPaciente').value;
        var telefono1 = document.getElementById('telefonoPaciente1').value;
        var email = document.getElementById('emaildoctor').value;
        var direccion = document.getElementById('direccion').value;
        var region = document.getElementById('region').value;
        var provincia = document.getElementById('provincia').value;
        var comuna = document.getElementById('comuna').value;


        document.confirmarcita.action = '<%=cc.getLocal()%>Confirmar_Cita?cita=' + cita + '&telefono=' + telefono + '&telefono1=' + telefono1 + '&email=' + email + '&direccion=' + direccion + '&region=' + region + '&provincia=' + provincia + '&comuna=' + comuna;
        document.confirmarcita.submit();
    }



    function buscaProvincia() {

        if (document.getElementById('region').value == -1)
        {

            Alert.render("Debe Elegir Region!! Debe Completar los Datos para Continuar");
            document.getElementById('region').focus();
        } else
        {

            var region = document.getElementById('region').value;

            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("provincia1");
                xmlhttp.open("post", "buscarprovincia2.jsp?region=" + region);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b> </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }
    function buscaComuna(provincia) {

        if (document.getElementById('provincia').value == -1)
        {

            Alert.render("Debe Elegir Provincia!! Debe Completar los Datos para Continuar");
            document.getElementById('provincia').focus();
        } else
        {




            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("comuna1");
                xmlhttp.open("post", "buscarcomuna2.jsp?provincia=" + provincia);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b></b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }


    function formateaRut(Rut)
    {
        var sRut = new String(Rut);
        var sRutFormateado = '';
        sRut = quitaFormato(sRut);
        var sDV = sRut.charAt(sRut.length - 1);
        sRut = sRut.substring(0, sRut.length - 1);
        document.forms["confirmarcita"].txtRutSinDV.value = sRut;
        document.forms["confirmarcita"].txtDV.value = sDV;
        while (sRut.length > 3)
        {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
        }
        sRutFormateado = sRut + sRutFormateado;
        if (sRutFormateado != "")
            sRutFormateado += "-";
        sRutFormateado += sDV;
        if (document.forms["confirmarcita"].rutpaciento.value != sRutFormateado)
            document.forms["confirmarcita"].rutpaciento.value = sRutFormateado;
    }
    function quitaFormato(Nro)
    {
        var strNro = new String(Nro);
        while (strNro.indexOf(".") != - 1)
            strNro = strNro.replace(".", "");
        strNro = strNro.replace("-", "");
        return strNro;
    }

    function DigitoVerificadorRut(strRut) {
        var rut = 0;
        var s = 0;
        var l_dv = "";

        rut = strRut;
        for (i = 2; i < 8; i++) {
            s = s + (rut % 10) * i;
            rut = (rut - (rut % 10)) / 10;
        }
        s = s + (rut % 10) * 2;
        rut = (rut - (rut % 10)) / 10;
        s = s + (rut % 10) * 3;
        rut = (rut - (rut % 10)) / 10;
        s = 11 - (s % 11);
        if (s == 10)
            l_dv = "K";
        else
        if (s == 11)
            l_dv = "0"
        else
            l_dv = s + "";
        return(l_dv);
    }



    function validaRut() {
        var sRut = new String(document.forms["confirmarcita"]["rutpaciento"].value);
        sRut = quitaFormato(sRut);
        var sDV = new String(sRut.charAt(sRut.length - 1));
        sRut = sRut.substring(0, sRut.length - 1);
        if (sDV.toUpperCase() == DigitoVerificadorRut(sRut))
        {
            return true;
        }
        if (sDV.toUpperCase() != DigitoVerificadorRut(sRut))
        {
            return false;
        }
    }





    var entre = false;
    function  buscaPaciente()
    {

        var paciente = document.forms["confirmarcita"]["rutpaciento"].value;


        if (paciente == null || paciente == "" || paciente.length == 0 )
        {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        } else
        {
            if (validaRut(document.forms["confirmarcita"]["rutpaciento"].value))

            {
                entre = true;
                try
                {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e)
                {
                    try
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (E) {
                        xmlhttp = false;
                    }
                }
                if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                    xmlhttp = new XMLHttpRequest();
                }

                if (xmlhttp) {
                    var objeto_recibidor = document.getElementById("buscar");
                    xmlhttp.open("post", "DatosPacienteConfirma.jsp?paciente=" + paciente);
                    xmlhttp.send("");
                    if (xmlhttp.readyState == 1) {
                        objeto_recibidor.innerHTML = '<b>Favor espere... </b>';

                    }
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            objeto_recibidor.innerHTML = xmlhttp.responseText;

                        }
                        if (xmlhttp.status != 200) {
                            objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                        }
                    }
                }

            } else {
                Alert.render("Rut Invalida");
            }
        }
    }

    $('body').keyup(function (e) {
        if (e.keyCode == 13) {

            return buscaPaciente();
        }


    });

</script>

<%
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");

%>

<div id="dialog-message" title="Recepci�n de Paciente">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>

<%
    }


%> 


<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>
<div class="container"> 

    <div class="tab-content">
        <div id="crear" class="tab-pane fade in active">


            <form name="confirmarcita" class="form-horizontal" id="confirmarcita" onsubmit="return false;" method="post"  >

                <fieldset>
                    <legend class="text-center header">Recepci�n de Paciente</legend>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon">Rut</i></span>
                        <div class="col-md-4">

                            <input type="text" id="rutpaciento" name="rutpaciento" style="text-transform:uppercase;" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                    return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                            <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                            <input value="d" name="txtDV" id="txtDV" type="hidden">

                        </div>

                        <button type="button" class="btn btn-primary" onclick="javascript : buscaPaciente()">Buscar</button>
                    </div>

                    <div id="buscar"></div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
<jsp:include page="../comunes/Footer.jsp" />