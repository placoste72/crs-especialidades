<%-- 
    Document   : RegistrarAtencionClinicaOftalmologia
    Created on : 13-04-2017, 14:59:55
    Author     : a
--%>

<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.cita"%>

<!DOCTYPE html>
<html>
<head>


     <link href="../../public/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>

        <script src="../../public/js/script2.js" type="text/javascript"></script>

        <link rel="shortcut icon" href="../../public/imagenes/icon.png">
        <script src="../../public/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css"> 
        <link href="../../public/css/estilos.css" rel="stylesheet"> 

        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/> 
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>

        <link href="../../public/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/> 
        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/fileinput.js" type="text/javascript"></script>
        <script src="../../public/js/fileinput_locale_fr.js" type="text/javascript"></script>
        <script src="../../public/js/fileinput_locale_es.js" type="text/javascript"></script>
        <link href="../../public/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>

        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>
        <style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
        <script type="text/javascript" src="../../public/js/calendar.js"></script>
        <script type="text/javascript" src="../../public/js/calendar-es.js"></script>

        <script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>

</head>
<%
    String cita = request.getParameter("cita");
    controlador_cita cc = new controlador_cita();
    cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
    String[] datos = cc.buscarPerfilyNombredelloggeado();
%>

<style>
    * {box-sizing: border-box}
    body {font-family: "Lato", sans-serif;}

    /* Style the tab */
    div.tab {
        float: left;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        width: 30%;
        height: 100%;

    }

    /* Style the buttons inside the tab */
    div.tab button {
        display: block;
        background-color: inherit;
        color: black;
        padding: 22px 16px;
        width: 100%;
        border: none;
        outline: none;
        text-align: left;
        cursor: pointer;
        transition: 0.3s;
        font-size: 17px;
        color:#428bca;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ddd;
        color:#428bca;
    }

    /* Create an active/current "tab button" class */
    div.tab button.active {
        background-color: #194f80;
        color: #ddd;
    }

    /* Style the tab content */
    .tabcontent {
        float: left;
        padding: 0px 12px;
        border: 1px solid #ccc;
        width: 70%;
        border-left: none;
        height: 100%;
    }
</style>
<script>
    function validarQueTodoEsteEnBlanco(){
        var valoruno = document.forms['atencioOft']['oi'].value;
        var valordos = document.forms['atencioOft']['od'].value;
        var numerouno = document.forms['atencioOft']['numerooi'].value;
        var numerodos = document.forms['atencioOft']['numerood'].value;
        var a = document.forms['atencioOft']['dilatacionoi'].value;
        var b = document.forms['atencioOft']['dilatacionod'].value;
        var colirio = document.forms['atencioOft']['colirio'].value;
        var autorefraccion = document.forms['atencioOft']['autorefraccion'].value ;
        var observacion = document.forms['atencioOft']['observacion'].value ; 
        var observacionpresion = document.forms['atencioOft']['observacionpresion'].value ;
        var   observaciondilatacion = document.forms['atencioOft']['observaciondilatacion'].value ;
        var   observacionr = document.forms['atencioOft']['observacionr'].value ;
        
        if(valoruno == "0" & valordos == "0" & numerouno =="" & numerodos == "" & a =="0" & b=="0" & colirio =="" & autorefraccion == "0" & observacion =="" & observacionpresion == "" & observaciondilatacion == "" & observacionr == ""  ){
            return false;
            
        }
    }

    function validarPresion() {
        var valoruno = document.forms['atencioOft']['oi'].value;
        var valordos = document.forms['atencioOft']['od'].value;
        var numerouno = document.forms['atencioOft']['numerooi'].value;
        var numerodos = document.forms['atencioOft']['numerood'].value;
        if (valoruno == 1) {
            
            if (numerouno == "") {
                return false;
            }
        }
        if(valordos == 1){
            if(numerodos == ""){
                return false;
            }
        }

    }
    
    function validarDilatacion(){
         var valoruno = document.forms['atencioOft']['dilatacionoi'].value;
        var valordos = document.forms['atencioOft']['dilatacionod'].value;
        var colirio = document.forms['atencioOft']['colirio'].value;
          if(valoruno == 1){
              if(colirio == ""){
                  return false;
              }
          }
          if(valordos ==1){
              if(colirio == ""){
                  return false;
              }
          }
      
        
    }

    function validar() {
       if(validarQueTodoEsteEnBlanco() == false){
           Alert.render("Esta Asumiendo que no se hizo Dilataciones al Paciente, y no tiene ningun tipo de Observacion!!  Debe Completar algun Dato");
            return false;
       }

        if (validarPresion() == false) {


            Alert.render("Si Coloco que si a la Medicaci�n de presi�n !! Debe Completar los Datos de Resultado de Presi�n");
            return false;
        }
        
         if (validarDilatacion() == false) {


            Alert.render("Si coloco Dilataci�n!! Debe Completar los Datos de Colirio para Continuar");
            return false;
        }




    }


</script>
<body>
<div class="container">
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>

    <div class="tab-content">

        <fieldset>
            <legend class="text-center header">Registro T�cnico</legend>
            <table class="table table-striped"  >
                <thead>

                    <tr>
                        <th>Perfil:          <%=datos[1]%></th>

                        <th>Usuario:         <%=datos[0]%></th>


                    </tr>
                </thead>
            </table>   


            <table class="table table-bordered">
                <thead><tr>
                        <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">DATOS DEL PACIENTE</th>
                        <th colspan="5" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">DATOS DE CITACI�N</th>
                    </tr>
                    <tr>
                        <th>RUT</th>
                        <th>Paciente</th>
                        <th>Edad</th>
                        <th>Sexo</th>
                        <th>Especialidad</th>
                        <th>Tipo de Cita</th>
                        <th>Hip�tesis Diagn�stica</th>
                        <th>Profesional</th>
                        <th>Fecha y Hora</th>

                    </tr>

                </thead>

                <tr>
                    <td><%= c.getMotivo_cancela()%></td>
                    <td><%= c.getRut_paciente()%></td>
                    <td><%= c.getTemporales2()%></td>
                    <td><%= c.getTemporales1()%></td>
                    <td><%= c.getTemporales()%> </td>
                    <td><%= c.getTemporales3()%>  </td>
                    <td><%= c.getTemporales4()%></td>
                    <td><%= c.getRut_doctor()%></td>
                    <td><%= c.getMotivo()%></td>

                </tr>
            </table>
            <div class="container"> 
                <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'Auto')" id="defaultOpen">Auto refracci�n</button>
                    <button class="tablinks" onclick="openCity(event, 'presion')">Medici�n de Presi�n Intra Ocular</button>
                    <button class="tablinks" onclick="openCity(event, 'Dilatacion')">Dilataci�n</button>
                    <button class="tablinks" onclick="openCity(event, 'observaciones')">Observaciones al proceso de atenci�n</button>
                </div>
                <form id="atencioOft" name="atencioOft" method="post" action='<%=cc.getLocal()%>atencionclinicaoftalmologia' onsubmit="return validar()"  >
                    <input id="cita" name="cita" type="text" value="<%=cita%>" hidden>
                    <div id="Auto" class="tabcontent" >
                        <table style=" " >

                            <tr><td><div class=" letra"> Realiza Autorefracci�n:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-1 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" ></i></span>

                                    <div class="col-md-8">
                                        <select id="autorefraccion" name="autorefraccion" > 
                                            <option value="1"> SI </option>
                                            <option selected value="0"> NO </option>
                                        </select>  
                                    </div>
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <br>
                            <tr><td><div class=" letra"> Observaciones:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                    <div class="col-md-8">
                                        <textarea type="text"  rows="3" id="observacion" name="observacion"  class="form-control" placeholder="Observaciones"  ></textarea>


                                    </div>  
                                </td>

                            </tr>  

                        </table>
                    </div>
                    <div id="presion" class="tabcontent">

                        <table style=" " >

                            <tr><td colspan="10"><div class=" letra"> Realiza Medici�n de PIO:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td width="2" ><span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OI</i></span>
                                </td>
                                <td>
                                    <div>
                                        <select id="oi" name="oi" id="oi"  onchange="javascript:
                                                        var valoruno = document.forms['atencioOft']['oi'].value;
                                                var valordos = document.forms['atencioOft']['od'].value;
                                                if (valoruno == 1 || valordos == 1) {

                                                    document.getElementById('colocarnumero').style.display = 'block';
                                                } else
                                                    document.getElementById('colocarnumero').style.display = 'none';
                                                " > 
                                            <option value="1"> SI </option>
                                            <option selected value="0"> NO </option>
                                        </select>  
                                    </div>
                                </td>
                                <td width="2">
                                    <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OD</i></span>
                                </td>
                                <td>
                                    <div>
                                        <select id="od" name="od" id="od"  onchange="javascript:
                                                        var valoruno = document.forms['atencioOft']['oi'].value;
                                                var valordos = document.forms['atencioOft']['od'].value;
                                                if (valoruno == 1 || valordos == 1) {

                                                    document.getElementById('colocarnumero').style.display = 'block';
                                                } else
                                                    document.getElementById('colocarnumero').style.display = 'none';
                                                " >  
                                            <option value="1"> SI </option>
                                            <option selected value="0"> NO </option>
                                        </select>  
                                    </div>
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <br>
                        </table>
                        <table id="colocarnumero" name="colocarnumero" style=" display: none " WIDTH=1000>

                            <tr><td colspan="10"><div class=" letra"> Resultado de Presi�n Intra Ocular:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td width="2" ><span ><i class="fa fa-edit bigicon" style="font-size:16px" > OI</i></span>
                                </td>
                                <td WIDTH=500>
                                    <div>
                                        <input type="text" id="numerooi" name="numerooi" placeholder="N�mero OI"> 
                                    </div>
                                </td>
                                <td width="4">
                                    <span ><i class="fa fa-edit bigicon" style="font-size:16px" > OD</i></span>
                                </td>
                                <td WIDTH=500>
                                    <div>
                                        <input type="text" id="numerood" name="numerood" placeholder="N�mero OD">    
                                    </div>
                                </td>

                            </tr> 

                        </table>
                        <table>



                            <tr><td> &nbsp;</td></tr>

                            <br>
                            <tr><td><div class=" letra"> Observaciones:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td colspan="10"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                    <div class="col-md-8">
                                        <textarea type="text"  rows="3" id="observacionpresion" name="observacionpresion"  class="form-control" placeholder="Observaciones"  ></textarea>


                                    </div>  
                                </td>

                            </tr>  

                        </table>

                    </div>

                    <div id="Dilatacion" class="tabcontent">
                        <table style=" " >
                            <tr><td colspan="4"><div class=" letra"> Realiza Dilataci�n:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                            <tr>
                                <td width="2" ><span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OI</i></span>
                                </td>
                                <td>
                                    <div>
                                        <select id="dilatacionoi" name="dilatacionoi" onchange="javascript:
                                                        var valoruno = document.forms['atencioOft']['dilatacionoi'].value;
                                                var valordos = document.forms['atencioOft']['dilatacionod'].value;
                                                if (valoruno == 1 || valordos == 1) {

                                                    document.getElementById('numero').style.display = 'block';
                                                } else
                                                    document.getElementById('numero').style.display = 'none';
                                                " > 
                                            <option value="1"> SI </option>
                                            <option selected value="0"> NO </option>
                                        </select>  
                                    </div>
                                </td>
                                <td  width="4"> <span ><i class="fa fa-chevron-down bigicon" style="font-size:16px" > OD</i></span></span>
                                </td>
                                <td>
                                    <div >
                                        <select id="dilatacionod" name="dilatacionod" onchange="javascript:
                                                        var valoruno = document.forms['atencioOft']['dilatacionoi'].value;
                                                var valordos = document.forms['atencioOft']['dilatacionod'].value;
                                                if (valoruno == 1 || valordos == 1) {

                                                    document.getElementById('numero').style.display = 'block';
                                                } else
                                                    document.getElementById('numero').style.display = 'none';
                                                " > 
                                            <option value="1"> SI </option>
                                            <option selected value="0"> NO </option>
                                        </select>  
                                    </div>
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>

                        <table id="numero" WIDTH=1000  style="display: none; "  >
                            <tr><td colspan="4"><div class=" letra">Colirio utilizado para dilataci�n:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>

                            <tr>
                                <td colspan="8" WIDTH=1000><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                    <div class="col-md-8" >
                                        <textarea type="text"  rows="3" id="colirio" name="colirio"  class="form-control" placeholder="Colirio utilizado para dilataci�n" ></textarea>


                                    </div>  
                                </td>

                            </tr>
                        </table>
                        <table>

                            <tr><td colspan="4"><div class=" letra"> Observaciones:</div></td></tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td colspan="4"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                    <div class="col-md-8">
                                        <textarea type="text"  rows="3" id="observaciondilatacion" name="observaciondilatacion"  class="form-control" placeholder="Observaciones"  ></textarea>


                                    </div>  
                                </td>

                            </tr>   

                        </table>
                    </div>

                    <div id="observaciones" class="tabcontent">
                        <table style=" " >
                            <br>
                            <div class=" letra"> Observaciones relevantes en el proceso de atenci�n:</div>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                    <div class="col-md-8">
                                        <textarea type="text"  rows="4" id="observacionr" name="observacionr"  class="form-control" placeholder="Observaciones relevantes" ></textarea>


                                    </div>  
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Grabar</button>

                            <button type="reset"   class="btn btn-primary btn-lg" >Cancelar</button>
                        </div>
                    </div>
                </form>
            </div> 





        </fieldset>
    </div>

</div> 
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

// Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>

</body>
</html>