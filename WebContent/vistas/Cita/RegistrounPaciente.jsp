
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.prevision"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>


<!DOCTYPE html>
<html>
    <head>

        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
        <link href="../../public/css/estilos.css" rel="stylesheet">

        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>

        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>

        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>


        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
        <script>





            function formateaRut(Rut)
            {
                var sRut = new String(Rut);
                var sRutFormateado = '';
                sRut = quitaFormato(sRut);
                var sDV = sRut.charAt(sRut.length - 1);
                sRut = sRut.substring(0, sRut.length - 1);
                document.forms["paciente"].txtRutSinDV.value = sRut;
                document.forms["paciente"].txtDV.value = sDV;
                while (sRut.length > 3)
                {
                    sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
                    sRut = sRut.substring(0, sRut.length - 3);
                }
                sRutFormateado = sRut + sRutFormateado;
                if (sRutFormateado != "")
                    sRutFormateado += "-";
                sRutFormateado += sDV;
                if (document.forms["paciente"].rutpaciento.value != sRutFormateado)
                    document.forms["paciente"].rutpaciento.value = sRutFormateado;
            }




            function quitaFormato(Nro)
            {
                var strNro = new String(Nro);
                while (strNro.indexOf(".") != - 1)
                    strNro = strNro.replace(".", "");
                strNro = strNro.replace("-", "");
                return strNro;
            }


            function validaRut() {
                var sRut = new String(document.getElementById('rutpaciento').value);
                sRut = quitaFormato(sRut);
                var sDV = new String(sRut.charAt(sRut.length - 1));
                sRut = sRut.substring(0, sRut.length - 1);
                if (sDV.toUpperCase() == DigitoVerificadorRut(sRut))
                {
                    return true;
                }
                if (sDV.toUpperCase() != DigitoVerificadorRut(sRut))
                {
                    return false;
                }
            }
            function DigitoVerificadorRut(strRut) {
                var rut = 0;
                var s = 0;
                var l_dv = "";

                rut = strRut;
                for (i = 2; i < 8; i++) {
                    s = s + (rut % 10) * i;
                    rut = (rut - (rut % 10)) / 10;
                }
                s = s + (rut % 10) * 2;
                rut = (rut - (rut % 10)) / 10;
                s = s + (rut % 10) * 3;
                rut = (rut - (rut % 10)) / 10;
                s = 11 - (s % 11);
                if (s == 10)
                    l_dv = "K";
                else
                if (s == 11)
                    l_dv = "0"
                else
                    l_dv = s + "";
                return(l_dv);
            }

            function validar() {

                if (!validaRut()) {


                    Alert.render("Rut Invalida !!Debe Completar los Datos para Continuar");
                    return false;
                }
            }

            $(document).ready(function () {
                $("#rutpaciento").on('paste', function (e) {
                    e.preventDefault();
                    Alert.render('Esta acci�n est� prohibida');
                })

                $("#rutpaciento").on('copy', function (e) {
                    e.preventDefault();
                    Alert.render('Esta acci�n est� prohibida');
                })
            })


        </script>

    </head>
    <body>



        <%    String mensaje = "";
            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Registrari Paciente">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>


        <%
            }

            int comuna = 0;
            int tramo = 0;
            int procedencia1 = 0;
            controlador_paciente cp = new controlador_paciente();
        %>

        <div class="container"> 
            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div> 
            <div class="tab-content">
                <div id="crear" class="tab-pane fade in active">



                    <form  style="border: #619fd8 5px solid;" name="paciente" class="form-horizontal" id="paciente" action='<%=cp.getLocal()%>ingresar_paciente' method="post" onsubmit="return validar()" >

                        <fieldset>
                            <legend class="text-center header">Registra Paciente</legend>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-credit-card bigicon"></i></span>
                                <div class="col-md-8">

                                    <input type="text" id="rutpaciento" name="rutpaciento" style="text-transform:uppercase;" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                            return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                                    <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                                    <input value="d" name="txtDV" id="txtDV" type="hidden">
                                </div>
                            </div>
                            <div id="busca">
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                    <div class="col-md-8">

                                        <input type="text" id="nombrepaciente" name="nombrepaciente" class="form-control" placeholder="Nombre Paciente"  required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input type="text" id="apellidoppaciente" name="apellidoppaciente" class="form-control" placeholder="Apellido Paterno Paciente"  required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input type="text" id="apellidomdoctor" name="apellidompaciente" class="form-control" placeholder="Apellido Materno Paciente" required oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                                    <div class="col-md-8" >
                                        <select class="form-control" id="genero" name="genero">
                                            <option>Genero

                                            <option  value="1">Femenino
                                            <option value="2"> Masculino
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar-o bigicon"></i></span>
                                    <div class="col-md-4">
                                        <table>
                                            <tr>
                                                <td >
                                                    <input type="text" name="txt_inicial" id="txt_inicial" value="" placeholder="Fecha de Nacimiento" class="form-control"  maxlength="13" size="12" >
                                                </td>
                                                <td colspan="3" style=" text-align: right">
                                                    <img src="../../public/imagenes/calendario.jpg" style="cursor:pointer;" id="boton_fecha_inicio" >
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input type="tel" id="telefonoPaciente" name="telefonoPaciente" class="form-control" placeholder="+56 9 1111 1111" maxlength="11"  required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input type="tel" id="telefonoPaciente1" name="telefonoPaciente1" class="form-control" placeholder="+56 9 2222 2222" maxlength="11">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input type="email" id="emaildoctor" name="emaildoctor" class="form-control" placeholder="Email Paciente" required oninvalid="setCustomValidity('El campo email es obligatorio')" oninput="setCustomValidity('')">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon"></i></span>
                                    <div class="col-md-8">
                                        <textarea type="text"  rows="3" id="direccion" name="direccion"  class="form-control" placeholder="Direccion" required oninvalid="setCustomValidity('El campo direccion es obligatorio')" oninput="setCustomValidity('')"></textarea>


                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                                <div class="col-md-8" >
                                    <select class="form-control" id="comuna" name="comuna" required>
                                        <option>Elegir Comuna
                                            <%
                                                for (lugar comu : cp.buscarTodasLasComuna()) {

                                                    if (comu.getId_lugar() == comuna) {%>
                                        <option selected value="<%=comu.getId_lugar()%>"><%=comu.getNombre()%><%
                                        } else {%>
                                        <option value="<%=comu.getId_lugar()%>"><%=comu.getNombre()%><%
                                                }

                                            }
                                            %>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                                <div class="col-md-8" >
                                    <select class="form-control" id="procedencia" name="procedencia" required>
                                        <option>Elegir Consultorio de donde procede
                                            <%
                                                for (lugar procedencia : cp.buscarProcedencia()) {

                                                    if (procedencia.getId_lugar() == procedencia1) {%>
                                        <option selected value="<%=procedencia.getId_lugar()%>"><%=procedencia.getNombre()%><%
                                        } else {%>
                                        <option value="<%=procedencia.getId_lugar()%>"><%=procedencia.getNombre()%><%
                                                }

                                            }
                                            %>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                                <div class="col-md-8" >
                                    <select class="form-control" id="provision" name="provision" onchange="javascript:
                                                    var pre = document.forms['paciente']['provision'].value;
                                            if (pre == 1) {

                                                document.getElementById('tramo').style.display = 'block';
                                            } else
                                                document.getElementById('tramo').style.display = 'none';
                                            ">
                                        <option>Prevision
                                            <% for (prevision pre : cp.buscarPrevicion()) {%>
                                        <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                                            <%}%>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="tramo" style="display:none">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                                <div class="col-md-8" >
                                    <select class="form-control" id="tramo" name="tramo" required>
                                        <option>Tramo
                                            <% for (prevision pre : cp.buscarTramo()) {

                                                    if (pre.getId_prevision() == tramo) {%>
                                        <option selected value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>  
                                            <%} else {%>

                                        <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                                            <%}%>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Crear </button>

                                    <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                </div>
                            </div>


                        </fieldset>

                    </form>


                </div>


            </div>
        </div>



    </body>
</html>
<script>
    Calendar.setup(
            {inputField: "txt_inicial", ifFormat: "%d/%m/%Y", button: "boton_fecha_inicio"}

    );

</script> 

