<%-- 
    Document   : ActualizarProcedimiento
    Created on : 20-12-2016, 04:36:02 PM
    Author     : Informatica
--%>

<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.patologia"%>
<%@page import="Controlador.controlador_patologia"%>
<%@page import="Modelos.procedimiento"%>
<%@page import="Controlador.controlador_procedimiento"%>
<jsp:include page="../comunes/headerwindows.jsp"/>
<!DOCTYPE html>


<html>
    <head>


        <meta name="generator" content="Bootply" />
       
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
       
        <title>Actualizar Causal de la Citaci�n</title>
    </head>
    <body> 

        <%      
            controlador_especialidad ce = new controlador_especialidad();
            String mensaje = "";
            int es, cod;
            es = 0;
            cod = 0;
            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Actualizar Patologia">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>


        <%
            }

            if (request.getParameter("cod") != null) {
                cod = Integer.parseInt(request.getParameter("cod"));
            }
            controlador_patologia cp = new controlador_patologia();
            patologia p = cp.buscarpatologiaporId(cod);

        %>




        <div class="container"> 




            <form style="border: #619fd8 5px solid;" name="procedimiento" class="form-horizontal" id="procedimiento" action='<%=cp.getLocal()%>actualizar_patologia' method="post" onsubmit="return validar()" >

                <fieldset>
                    <legend class="text-center header">Actualizar Causal de la Citaci�n</legend>
                    <input id="cod" name="cod" value="<%=p.getId()%>" hidden>

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil bigicon"></i></span>
                        <div class="col-md-8">
                            <input type="text" id="descripcion" name="descripcion" value="<%=p.getDescripcion()%>" class="form-control" placeholder="Descripcion"  required oninvalid="setCustomValidity('El campo Descripcion es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list bigicon"></i></span>


                        <div class="col-md-8" > 
                            <select class="form-control" id="especialidad" name="especialidad" title="Especialidad" 

                                    >  
                                <option> Seleccione Especialidad
                                    <%
                                        for (especialidades esp : ce.buscarEspecialdadTodas()) {

                                            if (p.getId_especialidad() == esp.getId_especialidad()) {%>
                                <option selected value="<%=esp.getId_especialidad()%>"><%=esp.getNombre()%><%
                                } else {%>
                                <option value="<%=esp.getId_especialidad()%>"><%=esp.getNombre()%><%
                                        }

                                    }
                                    %>
                            </select>


                        </div>
                    </div>  
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Actualizar</button>

                            <button type="reset" class="btn btn-primary btn-lg" onclick="javascript: window.close();">Cancelar</button>
                        </div>
                    </div>


                </fieldset>

            </form>

        </div>
    </body>


</html>
