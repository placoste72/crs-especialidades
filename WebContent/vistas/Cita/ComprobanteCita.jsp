
<%@ page import="java.io.FileOutputStream,java.io.*,java.awt.Color,java.util.Vector,java.util.Date,java.text.DateFormat,java.util.Locale" %>
<%@ page import="com.lowagie.text.*,com.lowagie.text.pdf.*,com.lowagie.text.pdf.PdfStamper,com.lowagie.text.pdf.PdfPageEventHelper" %>
<%@ page import="com.lowagie.text.pdf.PdfEncryptor,com.lowagie.text.pdf.PdfReader,com.lowagie.text.pdf.PdfWriter,com.lowagie.text.Chunk" %>
<%@ page import="com.lowagie.text.Document,com.lowagie.text.Element,com.lowagie.text.ExceptionConverter,com.lowagie.text.Font"%>
<%@ page import="com.lowagie.text.Image,com.lowagie.text.PageSize,com.lowagie.text.Rectangle,com.lowagie.text.pdf.BaseFont"%>
<%@ page import="com.lowagie.text.pdf.PdfContentByte,com.lowagie.text.pdf.PdfGState,com.lowagie.text.pdf.PdfPTable" %>
<%@ page import="com.lowagie.text.HeaderFooter,com.lowagie.text.Header,com.lowagie.text.pdf.PdfWriter"%>
<%@ page import="java.sql.*,java.util.GregorianCalendar,java.util.Calendar" %>

<%@include file="../comunes/pdf.jsp" %>

<!DOCTYPE html>
<%    Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
    DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

    String rut = "";
    String paciente_nombres = "";

    Date fecha = new Date();
    String hora = "";
    String especialidad = "";
    String doctor = "";
    String tipoatencion = "";

    String llego = request.getParameter("idcita");

    try {
        String query_paciente = "SELECT "
                + " p.rut, "
                + " (p.nombre || ' ' || p.apellido_paterno || ' ' || p.apellido_moderno) as paciente, "
                + " pl.fecha, "
                + " o.hora, "
                + " e.nombreespecialidad as especialidad, "
                + " (d.nombre ||' '||d.apellido_paterno ||' '|| d.apellido_materno) as doctor , "
                + " a.nombre as tipoatencion "
                + " FROM  "
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta "
                + "  join agenda.paciente p on c.rut_paciente = p.rut "
                + "   join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre "
                + "  join agenda.doctor d on d.rut = pl.rut_doctor "
                + "   join agenda.especialidades e on pl.id_especialidad = e.id_especialidad "
                + "  join agenda.atencion a on pl.id_atencion = a.id_atencion where c.id_cita= " + llego + " ;";

        ResultSet rs_paciente = st.executeQuery(query_paciente);
        rs_paciente.next();
        rut = rs_paciente.getString("rut");
        paciente_nombres = rs_paciente.getString("paciente");

        fecha = rs_paciente.getDate("fecha");
        hora = rs_paciente.getString("hora");
        especialidad = rs_paciente.getString("especialidad");
        doctor = rs_paciente.getString("doctor");
        tipoatencion = rs_paciente.getString("tipoatencion");

    } catch (SQLException ex) {

    }
    response.setContentType("application/pdf");
    Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(document, buffer);

    int SPACE_TITULO = 4;
    int SPACE_NORMAL = 12;
    int SPACE_NORMAL2 = 17;
    int SPACE_ESPACIO = 10;
    Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(68, 117, 196));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
    Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 22, Font.BOLD, new Color(32, 55, 100));
    Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
    Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD, new Color(68, 114, 196));
    Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
    writer.setPageEvent(new PageNumbersWatermark());
    document.open();

    Table tabla_titulo;
    Cell celda;

    tabla_titulo = new Table(3);
    tabla_titulo.setBorderWidth(0);
    tabla_titulo.setPadding(1);
    tabla_titulo.setSpacing(0);
    tabla_titulo.setWidth(100);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    String dondeAtencion = "";
    String dondedireccion = "";
    String transantiago = "";
    String dondetelefono = "";

    dondeAtencion = "CENTRO DE REFERENCIA DE SALUD MAIPU (CRS Maipu)";
    dondedireccion = "Camino Rinconada # 1001";
    transantiago = "108-109-109n-118-348-350-I03-I05-I09-I09c-I18-I24";
    dondetelefono = "574 6450";

    Paragraph Fecha = new Paragraph();
    Fecha.add(new Phrase(SPACE_TITULO, "FECHA:" + formateadorFecha.format(new Date()).toUpperCase(), fecha1));

    celda = new Cell(Fecha);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "____________________", Linea));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "COMPROBANTE DE CITA", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "__________________________________________________", Linea));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_NORMAL, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);


    
     
    celda = new Cell(new Phrase(SPACE_NORMAL2, "DATOS DEL PACIENTE:", TEXT));
    celda.setBackgroundColor(new Color(68, 114, 196));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph rutCita = new Paragraph();
     rutCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, "          RUT: ", TEXT_NORMAL));
     rutCita.add(new Phrase(SPACE_NORMAL2, "                    ", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, rut, TEXT_SUPERTITULONORMAL));
    celda = new Cell(rutCita);
    celda.setBorderWidth(0);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph nombreCita = new Paragraph();
    nombreCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, "          NOMBRE: ", TEXT_NORMAL));
     nombreCita.add(new Phrase(SPACE_NORMAL2, "             ", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, paciente_nombres, TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombreCita);
    celda.setBorderWidth(0);
   
   
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_NORMAL2, "DATOS DE LA CITA:", TEXT));
     celda.setBackgroundColor(new Color(68, 114, 196));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph fechaCita = new Paragraph();
     fechaCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, "          FECHA: ", TEXT_NORMAL));
    fechaCita.add(new Phrase(SPACE_NORMAL2, "                 ", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, formateadorFecha.format(fecha).toUpperCase(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(fechaCita);
    celda.setBorderWidth(0);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph horaCita = new Paragraph();
     horaCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, "           HORA: ", TEXT_NORMAL));
     horaCita.add(new Phrase(SPACE_NORMAL2, "                 ", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, hora, TEXT_SUPERTITULONORMAL));
    celda = new Cell(horaCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph especialidadCita = new Paragraph();
     especialidadCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, "          ESPECIALIDAD: ", TEXT_NORMAL));
     especialidadCita.add(new Phrase(SPACE_NORMAL2, "        ", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, especialidad, TEXT_SUPERTITULONORMAL));
    celda = new Cell(especialidadCita);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph medicoCita = new Paragraph();
     medicoCita.add(new Phrase(SPACE_NORMAL2, "         ", TEXT_SUPERTITULONORMAL));
    medicoCita.add(new Phrase(SPACE_TITULO, "        MEDICO: ", TEXT_NORMAL));
    medicoCita.add(new Phrase(SPACE_NORMAL2, "                ", TEXT_SUPERTITULONORMAL));
    medicoCita.add(new Phrase(SPACE_TITULO, doctor, TEXT_SUPERTITULONORMAL));
    celda = new Cell(medicoCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph AtencionGlosa = new Paragraph();
      AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, "           TIPO ATENCION: ", TEXT_NORMAL));
      AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, tipoatencion, TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_NORMAL2, "INDICACIONES:", TEXT));
    celda.setBackgroundColor(new Color(68, 114, 196));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    
    celda = new Cell(new Phrase(SPACE_NORMAL2, "                    * DEBE PRESENTARSE CON SU CEDULA DE IDENTIDAD Y DOCUMENTACION DE PREVISION AL DIA.", TEXT_CURSI));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_BOTTOM);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_NORMAL2, "                    * DEBE LLEGAR 30 MINUTOS ANTES DE LA HORA DE ATENCI�N.", TEXT_CURSI));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_BOTTOM);
    tabla_titulo.addCell(celda);


    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);


    Paragraph lugarAtencion = new Paragraph();
    lugarAtencion.add(new Phrase(SPACE_TITULO, "LUGAR DE ATENCION: ", TEXT_NORMAL));
    lugarAtencion.add(new Phrase(SPACE_TITULO, dondeAtencion, TEXT_NORMAL));
    celda = new Cell(lugarAtencion);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    Paragraph direccion = new Paragraph();
    direccion.add(new Phrase(SPACE_TITULO, "Direccion: ", TEXT_NORMAL));
    direccion.add(new Phrase(SPACE_TITULO, dondedireccion, TEXT_NORMAL2));
    celda = new Cell(direccion);
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    Paragraph comoLlegar = new Paragraph();
    comoLlegar.add(new Phrase(SPACE_TITULO, "Transantiago: ",TEXT_NORMAL));
    comoLlegar.add(new Phrase(SPACE_TITULO, transantiago, TEXT_NORMAL2));
    celda = new Cell(comoLlegar);
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    Paragraph telefono = new Paragraph();
    telefono.add(new Phrase(SPACE_TITULO, "Telefono: ", TEXT_NORMAL));
    telefono.add(new Phrase(SPACE_TITULO, dondetelefono, TEXT_NORMAL2));
    celda = new Cell(telefono);
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
 
    celda = new Cell(new Phrase(SPACE_TITULO, "__________________________________________________", Linea));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
    tabla_titulo.addCell(celda);
    
    document.add(tabla_titulo);

    document.close();
    try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
        DataOutput output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
//output.flush();            
//output.close();
    } catch (Exception exstream) {
    }

    st.close();
    cn.close();
%>