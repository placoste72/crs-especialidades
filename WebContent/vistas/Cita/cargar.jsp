<%-- 
    Document   : cargar
    Created on : 21-08-2019, 17:24:45
    Author     : Placoste
--%>

<%@page import="java.util.Properties"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="estructura.Conexion" %>
<%@page import="estructura.Proceso" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.DriverManager" %>
<%@page import="java.sql.SQLException" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Carga Masiva</title>
        <link rel="stylesheet" href="css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
        <%@ page language="Java" import="java.sql.*" %>  
        <jsp:useBean id="basedatos" scope="request" class="estructura.Conexion" />
        <jsp:useBean id="acciones" scope="request" class="estructura.Proceso" />
        <jsp:setProperty name="basedatos" property="*" />
        <jsp:setProperty name="acciones" property="*" />
        <%   
          ResultSet resultado = null ; 
          ResultSet resultadoLecturaCupos = null;
          ResultSetMetaData resultadoMetaDatos = null ; 
          int numerolineas ; 
          int i;  
        %>

<center>  
<h2> Planilla Paciente /Asignación </h2>  
<hr>  
<br><br>  
<table>

<% 
   basedatos.connect();
    
try {
  resultado = basedatos.execSQL("select distinct * from agenda.bitacora_detalle where detalle_estado=1 and observaciones=''");  
  
  
  } catch(SQLException e) { 
    throw new ServletException("Your query is not working", e);    
    }  

%>
<table class="table table-sm table table-striped table-bordered">
    <thead>
    <tr  class="table-info">
        <th scope="col" class="lineaEncabezado">RUN</th>
        <th scope="col" class="lineaEncabezado">NOMBRE</th>
        <th scope="col" class="lineaEncabezado">TELEFONO 1</th>
        <th scope="col" class="lineaEncabezado">TELEFONO 2</th>
        <th scope="col" class="lineaEncabezado">NOMBRE ATENCION</th>
        <th scope="col" class="lineaEncabezado">NOMBRE ESPECIALIDAD</th>
        <th scope="col" class="lineaEncabezado">DISPONIBILIDAD</th>
        <th scope="col" class="lineaEncabezado"></th>
    </tr>
    </thead>
    <tbody>
<% while (resultado.next()) { %>    
    <tr>
        <td valign="middle"><%=resultado.getString("rut") %></td>
        <td valign="middle"><%=resultado.getString("Nombre") %> <%=resultado.getString("apellido_paterno") %> <%=resultado.getString("apellido_materno") %></td>
        <td valign="middle"><%=resultado.getString("Telefono_1") %></td>
        <td valign="middle"><%=resultado.getString("Telefono_2") %></td>
        <td valign="middle"><%=resultado.getString("atencion_nombre") %></td>
        <td valign="middle"><%=resultado.getString("especialidad_descripcion") %></td>
        <td>
<%
    int codigoatencion = Integer.parseInt(resultado.getString("id_atencion"));      
    int codigoespecialidad = Integer.parseInt(resultado.getString("id_especialidad"));
    resultadoLecturaCupos = basedatos.execSQL(acciones.lecturaCuposDisponibles(codigoatencion,codigoespecialidad)); 
    if (resultadoLecturaCupos.isAfterLast()) {
        
    
%>
        <select class="form-control form-control-sm">
<%
    while (resultadoLecturaCupos.next()) {
%> 
<option value="<%=resultadoLecturaCupos.getInt("id_oferta") %>">
    <%=resultadoLecturaCupos.getString("totalfecha") %>
    <%=resultadoLecturaCupos.getString("hora") %>
    <%=resultadoLecturaCupos.getString("doctor") %>
</option>
<% }} %>    
        </td>
        <td>
            <input type="checkbox" name="">
        </td>
    </tr>
    <% } %>
    </tbody>
</table>


    </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
