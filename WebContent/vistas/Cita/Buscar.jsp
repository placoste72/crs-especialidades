<%-- 
    Document   : Buscar
    Created on : 26-01-2017, 04:49:04 PM
    Author     : Informatica
--%>

<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>

<!DOCTYPE html>
<%
    String paciente = request.getParameter("paciente");
    controlador_paciente cp = new controlador_paciente();
    controlador_cita c = new controlador_cita();
    paciente p = cp.buscarpacienteporrut(paciente);

    if (p.getRut() != null) {
%>

<table  style="border: #619fd8 5px solid;">
    <input type="text" name="f" id="f" value="<%=p.getRut()%>" hidden>
    <tr><td>
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon">Nombre:</i></span>
        </td>
        <td>
            <div class="col-md-8">
                <label id="nombre"><%=p.getNombre() + "  " + p.getApellido_paterno() + " " + p.getApellido_moderno()%></label>
            </div>
        </td>
        <td></td>

        <td></td>
        <td></td>
         <td></td>
    </tr>
    <tr>

        <td>
            <span class="col-md-1 col-md-offset-2 "><i class="fa fa-envelope bigicon" style="font-size: 26px ">Email</i></span>
        </td>   
        <td>
            <div class="col-md-8">
                <label><%=p.getEmail()%></label>
            </div>

        </td>
        <td>
         </td>
        <td><a href="DetalleDatosPaciente.jsp?rut=<%=p.getRut()%>"> <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 15px"> Hitos </i> <img src="../../public/imagenes/icon/datos.png" alt="" width="40px" height="40px" onclick=""/></span></a></td>
        <td><a href="<%=cp.getLocal()%>DetallesDatosPaciente?rut=<%=p.getRut()%>" onClick="return popup2(this, 'notes')"> <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 15px">Historial</i> <img src="../../public/imagenes/icon/carpeta_fichero.jpeg" alt="" width="40px" height="40px" onclick=""/></span></a></td>
         <td><a onClick="window.open('<%=cp.getLocallink()%>Ficha/citasdelPaciente.jsp?rut=<%=p.getRut()%>', 'Citas de Paciente', 'width=1200, height=1000')">  <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 15px" >Citas </i> <img src="../../public/imagenes/estatus/citas.jpg" width="40px" height="40px" alt=""/> </span></a></td>

    </tr>
    <tr>

        <td>

            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon">Contactos:</i></span>
        </td><td>  
            <div class="col-md-8">
                <label><%=p.getContacto1() + " " + p.getContacto2()%></label>
            </div>
        </td>
        <td></td>

        <td></td>
        <td></td>
         <td></td>
    </tr>
    <tr>
        <td><span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-folder bigicon">Prevision:</i></span></td>

        <td><div class="col-md-8">
                <label><%=p.getTemporal1()%></label>
            </div></td>
        <td></td>

        <td></td>
        <td></td>
         <td></td>


    </tr>
</table>
<br>
<legend class="text-center header">Ultimas 5 Citas</legend>
<table class="table table-striped" style=" width: 100%;">
    <thead>
        <tr >
            <th>Doctor</th>

            <th>Especialidad</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Motivo</th>
            <th>Tipo Atencion</th>
            <th>Comprobante de Cita</th>


        </tr>
    </thead>
    <% for (cita temp : c.Ultimas5Citasdeunpaciente(p.getRut())) {%>

    <tr>
        <td><%=temp.getRut_doctor()%></td>
        <td><%=temp.getTemporales1()%></td>
        <td><%=temp.getTemporales3()%></td>
        <td><%=temp.getTemporales2()%></td>
        <td><%=temp.getMotivo()%></td>
        <td><%=temp.getTemporales()%></td>

        <td align="center">
            <a href="<%=cp.getLocal()%>ComprobanteCita?idcita=<%=temp.getId_cita()%>" onClick="return popup2(this, 'notes')">

                <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="Imprimir Comprobante" border="0" width="16" height="16"/>
            </a>

        </td>


    </tr>
    <%}%>


</table>



<%

} else {
%>
<table class=" table table-striped " style="border: #619fd8 5px solid;">
    <tr>
        <th>
            <p style="color:#FF0000; font-size:15px">Paciente no registrado</p>
            <a href='<%=cp.getLocallink()%>Cita/RegistrarPaciente.jsp'>Registra Aqui�</a>
        </th>
    </tr>
</table> 
<%
    }


%>