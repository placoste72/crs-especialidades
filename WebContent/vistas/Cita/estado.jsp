<%-- 
    Document   : estado
    Created on : 23-10-2017, 11:50:16
    Author     : a
--%>

<%@page import="Modelos.lugar"%>
<%@page import="Controlador.controlador_cita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    String rut = request.getParameter("rut");
    controlador_cita cc = new controlador_cita();
    rut = cc.FormatearRUT(rut);


%>
<table class="table table-striped " style="width: 100%" >
    <thead>
        <tr style=" font-size: 20px; " class=" letra ">
            <th>
                Estado Cita 
            </th>
            <th>
                Estado Paciente  
            </th>
            <th>

            </th>
        </tr>

    </thead>

    <%        for (lugar l : cc.buscarEstatusPaciente(rut)) {

            if (l.getEstatu() == 1) { %>
    <tr style=" text-align: left">
        <td>Cita Asignada Satisfactoriamente</td>
        <td>Citado ; en espera de recepción.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/images.jpg" alt=""/></td>
    </tr>
    <%} else if (l.getEstatu() == 2) { %>
    <tr style=" text-align: left">
        <td>Cita Recepcionada</td>
        <td>Recepcionado ; en espera de atención.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/checklist.jpg" alt=""/></td>

    </tr>
    <%} else if (l.getEstatu() == 5) {%>

    <tr style=" text-align: left">
        <td>Cita en Proceso</td>
        <td>Paciente en Atencion.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/descarga.png" alt=""/></td>

    </tr>
    <%} else if (l.getEstatu() == 4) {%>
    <tr style=" text-align: left">
        <td>Cita Terminada Satifactoriamente</td>
        <td>Atendido ; Paciente dado de Alta.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/like-1174811_960_720.png" alt=""/></td>
    </tr>
    <%} else if (l.getEstatu() == 3) {%>
    <tr style=" text-align: left">
        <td>Cita Cancelada por el Profesional</td>
        <td>Ausente ; Se llamo al paciente luego de su recepción y no se encuentra.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/advertencia_318-139534.jpg" alt=""/></td>
    </tr>
    <%} else if (l.getEstatu() == 6) {%>
    <tr style=" text-align: left">
        <td>Cita Cancelada Temporalmente</td>
        <td>Ausente ; Se llamo al paciente  y no se encuentra.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/advertencia_318-139534.jpg" alt=""/></td>
    </tr>
    <%} else if (l.getEstatu() == 0) {%>

    <tr style=" text-align: left">
        <td>Cita Cancelada </td>
        <td>Ausente ; Cancelo la Cita.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/citas.jpg" alt=""/></td>
    </tr>
    <%} else {%>
    <tr style=" text-align: left">
        <td>Sin Cita para Hoy </td>
        <td>No Citado.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/boton-eliminar_318-85849.jpg" alt=""/></td>
    </tr>
    <%}
        }%>

</table>