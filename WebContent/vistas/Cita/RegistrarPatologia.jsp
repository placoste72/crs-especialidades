<%-- 
    Document   : ProcedimientoCita
    Created on : 20-12-2016, 01:11:04 PM
    Author     : Informatica
--%>

<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.patologia"%>
<%@page import="Controlador.controlador_patologia"%>
<%@page import="Modelos.procedimiento"%>
<%@page import="Controlador.controlador_procedimiento"%>

<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<script>
   function validar() {
        var x = document.forms["patologia"]["descripcion"].value;
        var y = document.forms["patologia"]["especialidad"].value
        if (x == null || x == "" || y == null || y == "-1" || y == -1) {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }
    }
</script>

<% 
    controlador_patologia cp = new controlador_patologia();
    controlador_especialidad ce = new controlador_especialidad();
    String mensaje = "";
    int es;
    es = 0;
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Registrar Causal de la Citaci�n">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>
        <%
    }%>

<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>

<div class="container"> 
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#crear">Crear</a></li>
        <li><a data-toggle="tab" href="#lista">Listado</a></li>

    </ul>
    <div class="tab-content">
        <div id="crear" class="tab-pane fade in active">


            <form name="patologia" class="form-horizontal" id="patologia" action='<%=cp.getLocal()%>ingresar_patologia' method="post" onsubmit="return validar()" >

                <fieldset>
                    <legend class="text-center header">Registra Causal de la Citaci�n</legend>

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil bigicon"></i></span>
                        <div class="col-md-8">
                            <input type="text" id="descripcion" name="descripcion" class="form-control" placeholder="Descripcion" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Descripcion es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list bigicon"></i></span>


                        <div class="col-md-8" > 
                            <select class="form-control" id="especialidad" name="especialidad" title="Especialidad" 

                                    >  
                                <option value="-1"> Seleccione Especialidad
                                    <%
                                        for (especialidades especialidad : ce.buscarEspecialdadTodas()) {

                                            if (es == especialidad.getId_especialidad()) {%>
                                <option selected value="<%=especialidad.getId_especialidad()%>"><%=especialidad.getNombre()%><%
                                } else {%>
                                <option value="<%=especialidad.getId_especialidad()%>"><%=especialidad.getNombre()%><%
                                        }

                                    }


                                    %>
                            </select>


                        </div>
                    </div>  
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">Crear </button>

                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                        </div>
                    </div>


                </fieldset>

            </form>


        </div>

        <div id="lista" class="tab-pane fade">
            <fieldset>
                <legend class="text-center header">Lista de Causales de la Citaci�n</legend>

                <table class="table table-striped" style="width: 100%">
                    <thead>
                        <tr >
                            <th>Codigo</th>

                            <th>Descripcion</th>

                            <th>Actualizar</th>
                            <th>Eliminar</th>

                        </tr>
                    </thead>
                    <% for (patologia temp
                                : cp.buscarPatologia()) {%>

                    <tr>
                        <td><%=temp.getId()%></td>

                        <td><%=temp.getDescripcion()%></td>

                        <td align="center">
                            <a href="ActualizarPatologia.jsp?cod=<%=temp.getId()%>" onClick="return popup2(this, 'notes')" class="button3">
                                Actualizar
                            </a>


                        </td>
                        <td align="center">
                            <a href='<%=cp.getLocal()%>SEliminarPatologia?cod=<%=temp.getId()%>' class="button2">
                                Eliminar
                            </a>
                        </td>


                    </tr>
                    <%}%>


                </table>
            </fieldset>
        </div>
    </div>
</div>

<jsp:include page="../comunes/Footer.jsp"/>
