
<%@page import="Controlador.controlador_cupos"%>
<%@page import="Modelos.oferta"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.cita"%>
<%@page import="java.util.Vector"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.prevision"%>

<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>


<link href="../../public/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script>

    $(function () {
        $("#txt").datepicker();
    });

    function buscaProvincia() {

        if (document.getElementById('region').value == -1)
        {

            Alert.render("Debe Completar los Datos para Continuar");
            document.getElementById('region').focus();
        } else
        {

            var region = document.getElementById('region').value;

            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("provincia1");
                xmlhttp.open("post", "buscarprovincia.jsp?region=" + region);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '<b>Favor espere </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }
    function buscaComuna(provincia) {

        if (document.getElementById('provincia').value == -1)
        {

            Alert.render("Debe Completar los Datos para Continuar");
            document.getElementById('provincia').focus();
        } else
        {




            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("comuna1");
                xmlhttp.open("post", "buscarcomuna.jsp?provincia=" + provincia);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b></b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }


</script>

<%    /*controladores*/
    controlador_cita cc = new controlador_cita();
    controlador_cupos cu = new controlador_cupos();
    controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
    controlador_paciente cp = new controlador_paciente();
    /*variables */
    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
    boolean pacienteDisponible = false;
    Vector<cita> vc = new Vector<cita>();
    String mensaje = "";
    oferta o = new oferta();
    int procedencia1 = 0;
    paciente p = new paciente();
    int region = 0;
    String nombre = "";
    String apellidop = "";
    String apellidom = "";
    String fechanacimiento = "";
    String genero = "";
    String previcion = "";
    /*1 fonasa , 2 isapre, 3 praim*/
    int codigoprevicion = 0;
    String error = "";
    String fecha = "";
    int generoe = 0;
    int t = 0;
    String tramo = "";
    int comuna = 0;
    String contacto1 = "";
    String contacto2 = "";
    String email = "";
    String direccion = "";
    int provincia = 0;
    int idnacionalidad = 213;
    String link = cp.getLocallink() + "Cita/buscarFonasaCita.jsp";
    String nombresocial = "";
    /*parametros llegan*/
    String rut = request.getParameter("paciente");
    String rutsinpunto = request.getParameter("rut");
    String dv = request.getParameter("dv");
    //String motivo = request.getParameter("motivo");

    rut = cp.FormatearRUT(rut);

    p = cp.consumirWsFonasa(Integer.parseInt(rutsinpunto), dv, rut);

//    p = cp.buscarpacienteporrut(rut);
    if (p.getRut() != null) {
        nombre = p.getNombre();
        apellidop = p.getApellido_paterno();
        apellidom = p.getApellido_moderno();
        fecha = formatoFecha.format(p.getFecha_nacimiento());
        generoe = p.getGenero();
        codigoprevicion = p.getProvision();
        contacto1 = p.getContacto1();
        contacto2 = p.getContacto2();
        email = p.getEmail();
        direccion = p.getDireccion();
        region = p.getId_region();
        comuna = p.getId_comuna();
        provincia = p.getId_provincia();
        procedencia1 = p.getProcedencia();
        t = p.getTramo();
        idnacionalidad = p.getIdnacionalidad();
        nombresocial = p.getNombresocial();

    }
    if (request.getParameter("oferta") != null) {
        String oferta = request.getParameter("oferta");
        o = cu.buscarOfertaporIdparaconsulta(Integer.parseInt(oferta));
    }
    String especialidad = request.getParameter("especialidad");

    /*busqueda*/
    //ArrayList citas = caco.traerlasultimas5citas(rut, Integer.parseInt(motivo));
    //Iterator it2 = citas.iterator();
    vc = cc.citasproximasdeunpaciente(rut);

    /*primera pasada*/
    if (request.getParameter("fecha") != null) {

        String fecha2 = request.getParameter("fecha");
        String a = fecha2.substring(6);
        String d = fecha2.substring(0, 2);
        String m = fecha2.substring(3, 5);

        String fecha3 = m + "/" + d + "/" + a;
        /*busco si el paciente  no tiene 
        cita para hoy en la misma especialidad   */
        //pacienteDisponible = cc.buscarCitaRutFecha(fecha3, rut, o.getHora());

    }
    if (vc.size() > 0) {


%>
<p class="letra" style=" text-align: center; font-size: 25px ">Proximas Citas del Paciente.</p>
<table class=" table-striped" style=" border-bottom:   #619fd8 2px solid;border-top:   #619fd8 2px solid;">

    <tr>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Especialidad</th>
        <th>Tipo de Atencion</th>
        <th>Profesional </th>
    </tr>
    <%  for (int i = 0; i < vc.size(); ++i) {

    %>
    <tr>
        <td><%=vc.get(i).getTemporales()%></td>
        <td><%=vc.get(i).getTemporales1()%></td>
        <td><%=vc.get(i).getTemporales2()%></td>
        <td><%=vc.get(i).getTemporales3()%></td>
        <td><%=vc.get(i).getTemporales4()%></td>
    </tr>
    <%}%>
</table>
<%
    }

    if (pacienteDisponible == true) {

%>

<table class=" table table-striped ">
    <tr>
        <th>
            <p style="color:#FF0000; font-size:15px">Paciente ya contiene Cita ese dia...</p>
        </th>
    </tr>

</table> 
<%} else {
%>

<input type="text" name="f" id="f" value="<%=rut%>" hidden>

<table>
    <tr>
        <td ><span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span></td>
        <td whidth="200" >

            <div class="col-md-6">

                <input type="text" id="nombrepaciente" value="<%=nombre%>" name="nombrepaciente" class="form-control" placeholder="Nombre Paciente" onkeypress="return validarsololetra(event)"  required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
            </div>  
        </td>
        <td whidth="300">
            <div class="col-md-6">
                <input type="text" id="apellidoppaciente" name="apellidoppaciente" value="<%=apellidop%>" class="form-control" placeholder="Apellido Paterno Paciente" onkeypress="return validarsololetra(event)"  required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">

            </div>  

        </td>
        <td whidth="300">
            <div class="col-md-6">
                <input type="text" id="apellidomdoctor" name="apellidompaciente" value="<%=apellidom%>" class="form-control" placeholder="Apellido Materno Paciente" >
            </div> 
        </td>


    </tr>
    <tr>
         <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon" style=" font-size: 14px">Nombre Social</i></span></td> 
       
        <td colspan="3">
            <div class="form-group">
                <div class="col-md-8">
                    <input type="text" id="nombresocial" name="nombresocial" value="<%=nombresocial%>"   class="form-control" onkeypress="return validarsololetra(event)" placeholder="Nombre Social"   >
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar-o bigicon"></i></span>
        </td> 
        <td whidth="200" >
            <table>
                <tr>
                    <td>
                        <div class="col-md-8">
                            <input type="text" name="txt" id="txt" value="<%=fecha%>"  placeholder="Fecha de Nacimiento (dd/mm/yy) " class="form-control"  maxlength="13" size="12" required oninvalid="setCustomValidity('El campo Fecha de Nacimiento es obligatorio')" oninput="setCustomValidity('')" >
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td colspan="2" >
            <div class="col-md-8" >
                <select class="form-control" id="genero" name="genero">
                    <option value="0">Genero
                        <% if (generoe == 1) {
                        %>
                    <option selected value="<%=generoe%>">Femenino

                        <%
                        } else if (generoe == 2) {%>
                    <option selected value="<%=generoe%>">Masculino
                    <option  value="1">Femenino<%
                    } else {%>
                    <option  value="1">Femenino
                    <option value="2"> Masculino
                        <%}%> 
                </select>
            </div>  
        </td>

    </tr>
    <tr>

        <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span></td> 

        <td whidth="200" >
            <div class="col-md-8">
                <input type="tel" id="telefonoPaciente" name="telefonoPaciente" value="<%=contacto1%>" class="form-control" placeholder="+56 9 1111 1111"  maxlength="11" required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
            </div>
        </td>
        <td whidth="300" >
            <div class="col-md-8">
                <input type="tel" id="telefonoPaciente1" name="telefonoPaciente1" value="<%=contacto2%>" class="form-control" placeholder="+56 9 2222 2222" maxlength="11">
            </div>
        </td>
        <td whidth="300" >
            <div class="col-md-8">
                <input type="text" id="emaildoctor" name="emaildoctor" value="<%=email%>" class="form-control" placeholder="Email Paciente" >
            </div> 
        </td>
    </tr>
    <tr >
        <td><span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon"></i></span></td>
        <td colspan="4" >

            <div class="col-md-8">
                <input type="text"  rows="3" id="direccion" name="direccion" value="<%=direccion%>"   class="form-control" placeholder="Direccion" required oninvalid="setCustomValidity('El campo direccion es obligatorio')" oninput="setCustomValidity('')">


            </div>  
        </td>
    </tr>
    <tr>
        <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-flag bigicon" style=" font-size: 14px" >Nacionalidad</i></span></td> 

        <td colspan="2">
            <div class="form-group">
                <div class="col-md-8" >
                    <select class="form-control" id="nacionalidad" name="nacionalidad"   >
                        <option value=-1>Elegir nacionalidad
                            <%    for (lugar l : cp.buscarNacionalidades()) {

                                    if (l.getId_lugar() == idnacionalidad) {%>
                        <option selected value="<%=l.getId_lugar()%>"><%=l.getDescripcion()%><%
                        } else {%>
                        <option  value="<%=l.getId_lugar()%>"><%=l.getDescripcion()%><%
                                }

                            }

                            %>
                    </select>
                </div> 


            </div>
        </td>
    </tr>
    <tr>
        <td>
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
        </td>
        <td whidth="200" >
            <div class="col-md-8" >
                <select class="form-control" id="region" name="region" required onchange=" return buscaProvincia()"  >
                    <option value=-1>Elegir Regi�n
                        <%                            for (lugar regiones : cp.buscarregion()) {

                                if (regiones.getId_lugar() == region) {%>
                    <option selected value="<%=regiones.getId_lugar()%>"><%=regiones.getNombre()%><%
                    } else {%>
                    <option value="<%=regiones.getId_lugar()%>"><%=regiones.getNombre()%><%
                            }

                        }
                        %>
                </select>
            </div> 
        </td>

        <td whidth="300" >
            <div id="provincia1">
                <div class="form-group">
                    <!--  <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>-->
                    <div class="col-md-8"  >
                        <select class="form-control" id="provincia" name="provincia" required onchange=" return buscaComuna(this.value)">
                            <option value="-1">Elegir provincia
                                <%
                                    for (lugar l : cp.buscarProvidencia(region)) {

                                        if (l.getId_lugar() == provincia) {%>
                            <option selected value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                            } else {%>
                            <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                                    }

                                }
                                %>
                        </select>
                    </div>
                </div>

            </div> 
        </td>
        <td whidth="300" >
            <div id="comuna1">
                <div class="form-group">
                    <!--  <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>-->
                    <div class="col-md-8" >
                        <select class="form-control" id="comuna" name="comuna" required>
                            <option value="-1">Elegir Comuna
                                <%
                                    for (lugar temp : cp.buscarComuna(provincia)) {

                                        if (temp.getId_lugar() == comuna) {%>
                            <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                            } else {%>
                            <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                    }
                                }%>
                        </select>
                    </div>
                </div>

            </div> 
        </td>
    </tr>
    <tr>
        <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span></td>
        <td whidth="300" ><div class="col-md-8" >
                <select class="form-control" id="procedencia" name="procedencia" required>
                    <option value="0">Elegir establecimiento de origen
                        <%
                            for (lugar procedencia : cp.buscarProcedencia()) {

                                if (procedencia.getId_lugar() == procedencia1) {%>
                    <option selected value="<%=procedencia.getId_lugar()%>"><%=procedencia.getNombre()%><%
                    } else {%>
                    <option value="<%=procedencia.getId_lugar()%>"><%=procedencia.getNombre()%><%
                            }

                        }
                        %>
                </select>
            </div></td>
        <td whidth="300" ><div class="col-md-8" >
                <select class="form-control" id="provision" name="provision" onchange="javascript:
                                var pre = document.forms['cita']['provision'].value;
                        if (pre == 1) {

                            document.getElementById('t').style.display = 'block';
                        } else
                            document.getElementById('t').style.display = 'none';
                        " required>
                    <option value="0">Prevision

                        <% for (prevision pre : cp.buscarPrevicion()) {

                                if (pre.getId_prevision() == codigoprevicion) {%>
                    <option selected  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                        <%} else {%>
                    <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                        <% }
                            }%>


                </select>
            </div></td>
            <td whidth="300" ><div class="col-md-8" id="t" name="t" style="display: " >
                <select class="form-control" id="tramo" name="tramo" required>
                    <option value="0">Tramo
                        <% for (prevision pre : cp.buscarTramo()) {

                                if (pre.getId_prevision() == t) {%>
                    <option selected value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>  
                        <%} else {%>

                    <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                        <%}
                            }%>
                </select>
            </div></td>

    </tr>

</table>



<div class="form-group">
    <div class="col-md-12 text-center">
        <button type="button" id="asignar" onclick="javascritp: validar()" class="btn btn-primary btn-lg">Crear </button>

        <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
    </div>
</div>

<%}%>





