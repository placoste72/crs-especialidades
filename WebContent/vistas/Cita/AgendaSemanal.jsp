
<%@page import="Modelos.lugar"%>
<%-- 
    Document   : AgendaSemanal
    Created on : 11-01-2017, 04:47:55 PM
    Author     : Informatica
--%>

<%@page import="Modelos.funcionario"%>
<%@page import="Controlador.controlador_usuario"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.especialidades"%>
<%@page import="java.util.Vector"%>
<%@page import="Controlador.controlador_cupos"%>
<%@page import="Modelos.planificar"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="Controlador.General"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<title>Agenda Semanal</title>
<jsp:include page="../comunes/Header.jsp"/>

<%
    controlador_usuario cu = new controlador_usuario();
    controlador_especialidad ce = new controlador_especialidad();
    General g = new General();

    String nombre = "Todas las Agendas";
    boolean tienepermiso = false;
    tienepermiso = g.buscarsipuedeCitar(g.getUsuarioLoggin());

    int es = -1;
    if (request.getParameter("especialidad") != null) {
        es = Integer.parseInt(request.getParameter("especialidad"));
        especialidades espe = ce.buscarEspecialidadCodigo(es);
        if (espe != null) {
            nombre = "Agenda de " + espe.getNombre();
        }
    }

    String doc = "-1";
    if (request.getParameter("funcionario") != null) {
        doc = request.getParameter("funcionario");
    }

    int aten = -1;
    if (request.getParameter("atencion") != null) {
        aten = Integer.parseInt(request.getParameter("atencion"));
    }

    //fecha actual
    Calendar fechaActual = Calendar.getInstance();
    int ann = fechaActual.get(Calendar.YEAR);
    int mes = fechaActual.get(Calendar.MONDAY);
    //instancia
    Calendar c1 = Calendar.getInstance();
    String anno;
    
    anno = Integer.toString(c1.get(Calendar.YEAR));
    //manejo de a�os
    int anoactual = 2017;
    int finanno = Integer.parseInt(anno) + 10;
    // arreglos mes y semanas
    String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
    String dias[] = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};

    String ann1 = " ";
    String m1 = "";

    String fechallego = "";
    Date diaIncio = null;
    //fecha de inicio
    if (request.getParameter("inicio") != null) {
        fechallego = request.getParameter("inicio");
        diaIncio = new Date(Integer.parseInt(fechallego.substring(6, 10)) - 1900, Integer.parseInt(fechallego.substring(3, 5)) - 1, Integer.parseInt(fechallego.substring(0, 2)), 0, 0, 0);

    } else {
        diaIncio = fechaActual.getTime();

    }
    //retorcedo o adelanto
    Calendar sumar = Calendar.getInstance();
    sumar.setTime(diaIncio);

    Date inicio = null;

    if (request.getParameter("hacer") != null) {

        int trae = Integer.parseInt(request.getParameter("hacer"));

        if (trae == 1) {

            sumar.add(Calendar.DATE, -7);
            diaIncio = sumar.getTime();
            m1 = String.valueOf(diaIncio.getMonth());
            ann1 = Integer.toString((diaIncio.getYear()));

            int diadelasemana = diaIncio.getDay();
            Calendar f = Calendar.getInstance();
            f.setTime(diaIncio);
            f.add(Calendar.DATE, -diadelasemana);
            f.add(f.DATE, 1);
            inicio = f.getTime();

        } else if (trae == 0) {

            sumar.add(Calendar.DATE, +7);
            diaIncio = sumar.getTime();
            m1 = String.valueOf(diaIncio.getMonth());
            ann1 = Integer.toString((diaIncio.getYear()));
            int diadelasemana = diaIncio.getDay();
            Calendar f = Calendar.getInstance();
            f.setTime(diaIncio);
            f.add(Calendar.DATE, -diadelasemana);
            f.add(f.DATE, 1);
            inicio = f.getTime();
        } else {
            //pregunto por los request

            if (request.getParameter("mes") != null) {

                m1 = request.getParameter("mes");
            } else {
                m1 = String.valueOf(diaIncio.getMonth());
            }

            if (request.getParameter("anno") != null) {
                ann1 = request.getParameter("anno");
            } else {
                ann1 = Integer.toString((diaIncio.getYear()));
            }
            inicio = new Date(Integer.parseInt(ann1) - 1900, Integer.parseInt(m1), 1, 0, 0, 0);

        }

    } else {
        int diadelasemana = diaIncio.getDay();
        Calendar f = Calendar.getInstance();
        f.setTime(diaIncio);
        f.add(Calendar.DATE, -diadelasemana);
        f.add(f.DATE, 1);
        inicio = f.getTime();

    }

    //buscar agenda
    String cad_valor = String.valueOf(inicio.getDate());

    if (cad_valor.length() < 2) {
        cad_valor = "0" + cad_valor;
    }
    // debo colocar mas 1 porque inicio trae junio
    m1 = Integer.toString(inicio.getMonth());
    ann1 = Integer.toString(inicio.getYear() + 1900);
    String cad_valor2 = String.valueOf(inicio.getMonth() + 1);
    if (cad_valor2.length() < 2) {
        cad_valor2 = "0" + cad_valor2;
    }
    ArrayList semana;
    semana = g.generarAgendaporSemana(inicio);
    int dia_inicio = inicio.getDay();
    controlador_cupos cc = new controlador_cupos();
    //intero las fechas
    Iterator it = semana.iterator();
    //para buscar el total por semana
    Date fin = null;
    Calendar fi = Calendar.getInstance();
    fi.setTime(inicio);
    fi.add(Calendar.DATE, 6);
    fin = fi.getTime();

    planificar totalsemana = cc.totalporsemana(inicio, fin, es, doc, aten);
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");


%>
<div id="dialog-message" title="Agenda Semanal">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>

<%
    }

    if (dia_inicio == 0) {
        dia_inicio = 6;
    } else {
        dia_inicio = dia_inicio - 1;
    }


%>



<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>

<div class="container">
    <form id="agendasemanal" name="agendasemanal" method="POST" action="AgendaSemanal.jsp"> 
        <fieldset style="margin-top: 35px">
            <legend class="text-center header"><%=nombre%></legend>
            <div class="text-center letra"> Semana del <%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%> </div>
            <div class="text-center letra"> Utilizados/Disponibles:( <%=totalsemana.getId_programa()%> / <%=totalsemana.getDuracion()%> )</div>
            <div class="form-group"> 
                <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-list bigicon"></i></span> 


                <div class="col-md-2" > 
                    <select class="form-control" id="especialidad" name="especialidad" title="Especialidad"  onchange="javascript: document.agendasemanal.action = 'AgendaSemanal.jsp?inicio=<%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%>';
                            document.agendasemanal.submit();">  
                        <option value="-1">Elegir Especialidad 
                            <%  for (especialidades espe : ce.buscarEspecialdadTodas()) {

                                    if (es == espe.getId_especialidad()) {%>
                        <option selected value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                        } else {%>
                        <option value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                }

                            }
                            %>
                    </select>


                </div>

                <span class="col-md-1 col-md-offset-1 " ><i class="fa fa-user-md bigicon" ></i></span>

                <div class="col-md-2" >
                    <select class="form-control" id="funcionario" name="funcionario" title="Doctor" onchange="javascript: document.agendasemanal.action = 'AgendaSemanal.jsp?inicio=<%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%>';
                            document.agendasemanal.submit();">
                        <option value="-1">Profesional
                            <%  for (funcionario f : cu.BuscarFuncionaroconDoctorporEspecialidad(es)) {

                                    if (doc.equals(f.getRut())) {%>
                        <option selected value="<%=f.getRut()%>"><%=f.getNombre()%><%
                        } else {%>
                        <option value="<%=f.getRut()%>"><%=f.getNombre()%><%
                                }

                            }
                            %>
                    </select>
                </div> 

                <span class="col-md-1 col-md-offset-1 " ><i class="fa fa-list bigicon" ></i></span>

                <div class="col-md-2" >
                    <select class="form-control" id="atencion" name="atencion" title="Atencion" onchange="javascript: document.agendasemanal.action = 'AgendaSemanal.jsp?inicio=<%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%>';
                            document.agendasemanal.submit();">
                        <option value="-1">Elegir Atencion
                            <%
                                for (lugar atencion : g.buscarAtencionparaAgendarCupos()) {

                                    if (atencion.getId_lugar() == aten) {%>
                        <option selected value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                        } else {%>
                        <option value="<%=atencion.getId_lugar()%>"><%=atencion.getNombre()%><%
                                }

                            }
                            %> 
                    </select>
                </div> 



            </div> 
            <br>
            <br>
            <div class="form-group">    

                <span class="col-md-1 col-md-offset-3 text-center"><i class="fa fa-calendar bigicon"></i></span>
                <div class="col-md-2" >
                    <select class="form-control" id="mes" name="mes" onchange="javascript: document.agendasemanal.action = 'AgendaSemanal.jsp?inicio=<%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%>&hacer=3';
                            document.agendasemanal.submit();">

                        <%   for (int i = 0; i < meses.length; ++i) {

                                if (i == Integer.parseInt(m1)) {%>
                        <option selected value="<%=i%>"><%=meses[i]%>
                            <%
                            } else {%>


                        <option value="<%=i%>"><%=meses[i]%>

                            <%
                                    }
                                }


                            %>
                    </select>
                </div>
                <div class="col-md-2" >
                    <select class="form-control" id="anno" name="anno" onchange="javascript: document.agendasemanal.action = 'AgendaSemanal.jsp?inicio=<%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%>&hacer=3&especialidad=<%=es%>';
                            document.agendasemanal.submit();">

                        <% while (anoactual <= finanno) {

                                if (anoactual == Integer.parseInt(ann1)) {%>
                        <option selected value="<%=anoactual%>"><%=anoactual%><%
                        } else {%>
                        <option value="<%=anoactual%>"><%=anoactual%>
                            <%}
                                    ++anoactual;
                                }%>
                    </select>

                </div>
            </div>
            <br>
            <br>
            <div class="form-group">
                <span class="col-md-1 col-md-offset-1 "><i class="fa fa-chevron-left bigicon" onclick="javascript: document.agendasemanal.action = 'AgendaSemanal.jsp?inicio=<%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%>&hacer=1&especialidad=<%=es%>';
                        document.agendasemanal.submit();"></i></span> 


                <span class="col-md-1 col-md-offset-8 "><i class="fa fa-chevron-right bigicon" onclick="javascript: document.agendasemanal.action = 'AgendaSemanal.jsp?inicio=<%=cad_valor + '/' + cad_valor2 + '/' + (inicio.getYear() + 1900)%>&hacer=0&especialidad=<%=es%>';
                        document.agendasemanal.submit();"></i></span>

            </div>

            <table class="table table-striped" style="width: 100%" >
                <thead>
                    <tr>
                        <%for (int i = 0; i < dias.length; ++i) {%>
                        <th>

                            <%=dias[i]%>
                        </th>
                        <%}%>
                    </tr>
                </thead>
                <%
                    boolean pasa;
                    pasa = false;
                    for (int column = 0; column < 2; ++column) {

                        int filas = 0;

                %>
                <tr style="padding: 27px;">

                    <%                        try {
                            while ((filas < 7) && (it.hasNext())) {
                                if ((filas == dia_inicio) || (pasa == true)) {
                                    Date d = (Date) it.next();
                                    String cad_valor3 = String.valueOf(d.getDate());

                                    String cad_valor4 = String.valueOf(d.getMonth() + 1);
                                    if (cad_valor3.length() < 2) {
                                        cad_valor3 = "0" + cad_valor3;
                                    }

                                    if (cad_valor4.length() < 2) {
                                        cad_valor4 = "0" + cad_valor4;
                                    }
                                    String dia = cad_valor3 + "/" + cad_valor4 + "/" + Integer.toString(d.getYear() + 1900);
                                    Date fechaA = new Date();
                                    SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
                                    String fechaSistema = formateador.format(fechaA);
                                    Date fechaDate1 = formateador.parse(dia);
                                    Date fechaDate2 = formateador.parse(fechaSistema);

                                    Vector<planificar> lista_vacia = cc.traerdatosporsemana(d, es, doc, aten);
                                    if (lista_vacia.size() > 0) {


                    %>

                    <td  style="background-color:#ffcccc; text-align:center; width: 160px; height: 190px" class="letra"><strong> <%=d.getDate()%></strong>
                        <br>
                        <br>
                        <%

                            for (planificar p : cc.traerdato(d, aten, es, doc)) {

                                if (p.getNumero_cupo() != 0) {
                                    if (!doc.equals("-1") && !fechaDate1.before(fechaDate2) && aten != -1) {%>   
                        <%if (tienepermiso == true) {%>
                        <a href="AsignarCita.jsp?atencion=<%=p.getId_atencion()%>&especialidad=<%=es%>&funcionario=<%=doc%>&txt_inicial=<%=dia%>" onClick="return popup1(this, 'notes')"> 
                            <%}%>
                            <p style="font-size: x-small">  <img src="../../public/imagenes/<%=p.getId_atencion()%>.png" alt=""  width="25px" height="25px" /><%=p.getPrograma()%> :(<%=p.getId_programa()%>/<%=p.getDuracion()%>)</p></a>
                                <%
                                } else {%>

                        <p style="font-size: x-small">  <img src="../../public/imagenes/<%=p.getId_atencion()%>.png" alt=""  width="25px" height="25px" onclick="javascript : reset();  alertify.error('Debe seleccionar especialidad , doctor , atencion y una fecha Mayor o igual a Hoy ');" /><%=p.getPrograma()%> :(<%=p.getId_programa()%>/<%=p.getDuracion()%>)</p>
                            <%
                                        }
                                    }
                                }
                            %>

                    </td>  


                    <%  } else {

                    %>
                    <td  style="background-color:#ccffcc; text-align:center; width: 160px; height: 190px" class="letra"><strong> <%=d.getDate()%></strong>
                        <%}

                            pasa = true;
                        } else {
                        %>
                    <td style=" background-color:#ccffcc; padding: 27px; width: 160px; height: 190px"> .</td>
                    <%
                                }
                                ++filas;
                            }
                        } catch (Exception ex) {
                        }%>
                </tr>
                <%}%>

            </table>
        </fieldset>
    </form>
</div>   
</body>

<jsp:include page="../comunes/Footer.jsp"/>