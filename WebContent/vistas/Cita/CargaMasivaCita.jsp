<%-- 
    Document   : AsignarCita
    Created on : 13-12-2016, 04:22:49 PM
    Author     : Informatica
--%>


<%@page import="Modelos.rol"%>
<%@page import="java.util.Vector"%>
<%@page import="Modelos.procedimiento"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Modelos.motivo_consulta"%>
<%@page import="Modelos.patologia"%>
<%@page import="Controlador.controlador_patologia"%>
<%@page import="Modelos.lugar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="Controlador.controlador_cupos"%>
<%@page import="Modelos.planificar"%>
<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>

<!DOCTYPE html>

<jsp:include page="../comunes/headerwindows.jsp"/>

<html>
    <head>


        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="../../public/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/bootstrap-datepicker.min.js" type="text/javascript"></script>


    </head>
    <body>



        <%
            controlador_cita cc = new controlador_cita();
            String Ges[] = {"Seleccione ", "Si", "No"};
            int ges = 0;
            controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();

            controlador_cupos cp = new controlador_cupos();
            controlador_patologia cpato = new controlador_patologia();

            Calendar fe = Calendar.getInstance();
            int es = 0;
            String doctor = "";

            String fecha = "";
            int atencion1 = 0;
            String fecha2 = "";

            Date fechadate = new Date();
            int patologia = 0;

            if (request.getParameter("especialidad") != null) {
                es = Integer.parseInt(request.getParameter("especialidad"));
            }

            if (request.getParameter("atencion") != null) {
                atencion1 = Integer.parseInt(request.getParameter("atencion"));
            }

            if (request.getParameter("txt_inicial") != null) {
                fecha = request.getParameter("txt_inicial");
                fechadate = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);

                String a = fecha.substring(6);
                String d = fecha.substring(0, 2);
                String m = fecha.substring(3, 5);
                /*para base de datos local*/
                fecha2 = m + "/" + d + "/" + a;

            } else {

                int a�o = fe.get(Calendar.YEAR);
                int mes = fe.get(Calendar.MONTH) + 1;
                int dia = fe.get(Calendar.DAY_OF_MONTH);

                String cad_valor = String.valueOf(dia);

                if (cad_valor.length() < 2) {
                    cad_valor = "0" + cad_valor;
                }

                String cad_mes = String.valueOf(mes);

                if (cad_mes.length() < 2) {
                    cad_mes = "0" + cad_mes;
                }

                fecha = cad_valor + "/" + cad_mes + "/" + a�o;

            }

            if (request.getParameter("funcionario") != null) {
                doctor = request.getParameter("funcionario");
            }

            if (request.getParameter("patologia") != null) {
                patologia = Integer.parseInt(request.getParameter("patologia"));
            }

            String mensaje = "";
            if (request.getParameter("error") != null) {
                mensaje = request.getParameter("men");
                if (request.getParameter("error").equals("0")) {

                    String traerut = request.getParameter("paciente");
        %>
        <script  type="text/javascript">
            window.open('DetalleDatosPaciente.jsp?rut=<%=request.getParameter("paciente")%>&men=<%=mensaje%>')
            window.opener.location.href = "<%=cc.getLocallink()%>Cita/AgendaSemanal.jsp?especialidad=<%=request.getParameter("especialidad")%>&funcionario=<%=request.getParameter("funcionario")%>&inicio=<%=request.getParameter("txt_inicial")%>&atencion=<%=request.getParameter("atencion")%>";

        </script>


        <%
                }
            }
            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");


        %>
        <div id="dialog-message" title="Citacion Paciente">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>

        <%
            }
        %>

        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <form style="border: #619fd8 5px solid;" id="cita" name="cita"  method="post"  onsubmit="return false;"  >
            <fieldset>
                <legend class="text-center header">Asignar Cita</legend>

                <table>

                    <tr>
                        <td>

                            <span class="col-md-1 "><i class="fa fa-list bigicon"></i></span>
                        </td>      
                        <% controlador_especialidad ce = new controlador_especialidad();
                            especialidades e = ce.buscarEspecialidadCodigo(es);
                        %>
                        <td>
                            <div class="col-md-4" > 
                                <input type="text" id="especialidad" name="especialdiad" value="<%=e.getId_especialidad()%>" hidden >

                                <label style=" font-size: 16px "><%=e.getNombre()%></label>


                            </div>
                        </td>       
                        <td>
                            <span class="col-md-1  "><i class="fa fa-calendar-o bigicon"></i></span>
                        </td>
                        <td>
                            <div class="col-md-4">
                                <input type="text" name="txt_inicial" id="txt_inicial" value="<%=fecha%>"  hidden />
                                <label style=" font-size: 16px "><%=fecha%></label>
                            </div>


                        </td>     
                        <td>

                            <span class="col-md-1 "><i class="fa fa-hospital-o bigicon"></i></span>
                        </td>
                        <td>

                            <div class="col-md-4" >

                                <% for (lugar atencion
                                            : ce.buscarAtencion(atencion1)) {
                                %>

                                <input type="text" id="atencion" name="atencion" value="<%=atencion1%>" hidden>
                                <label style=" font-size: 16px "><%=atencion.getNombre()%></label>


                                <%

                                    }
                                %> 

                            </div>
                        </td>
                        <td>
                            <span class="col-md-1 "><i class="fa fa-user-md bigicon"></i></span>
                        </td>
                        <td>
                            <div class="col-md-4" >
                                <%
                                    controlador_doctor cd = new controlador_doctor();
                                    doctor d = cd.traerdoctor(doctor);
                                %>
                                <input type="text" id="funcionario" name="funcionario" value="<%=d.getRut()%>" hidden>
                                <label style=" font-size: 16px "><%=d.getNombre() + "-" + d.getApellido_paterno()%></label>

                            </div>

                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan="2">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 "><i class="fa fa-clock-o  bigicon" style="font-size: 16px ">Horas Disponibles:</i></span> 

                                <div class="col-md-4" >
                                    <select style=" font-size: 16px " class="form-control" id="oferta" name="oferta">
                                        <option value="-1">Horas Disponibles
                                            <%
                                                for (planificar temp
                                                        : cp.buscarofertas(atencion1, fechadate, doctor, es)) {


                                            %>
                                        <option  value="<%= temp.getId_planificacion()%>"><%= temp.getRut_doctor()%>

                                            <%
                                                }%>
                                    </select>
                                </div>
                            </div>
                        </td>


                        <td colspan="2">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 "><i class="fa fa-server  bigicon" style="font-size: 16px ">Diagnostico:</i></span> 
                                <div class="col-md-4"  >
                                    <select name="diagnosticocita" id="diagnosticocita" class="form-control"
                                            onchange="javascript:
                                                            var indice = document.forms['cita']['diagnosticocita'].selectedIndex;
                                                    var valor = document.forms['cita']['diagnosticocita'].options[indice].id;

                                                    document.forms['cita']['patologiages'].value = valor;
                                                    buscargesCausal();

                                            ">
                                        <option  value="0" id="0" selected>Diagnostico

                                            <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(es)) {%>
                                        <option id="<%=lu.getGes()%>"  value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                            <% }%>  
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <table>

                    <tr>
                        <td>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 "><i class="fa fa-list bigicon" style="font-size: 16px ">Ges</i></span>
                                <div class="col-md-4" >
                                    <select  style=" font-size: 16px " class="form-control" id="patologiages" name="patologiages"  onchange="buscargesCausal();

                                             ">
                                        <% for (int i = 0; i < Ges.length; ++i) {
                                        %>
                                        <option value="<%=i%>"><%=Ges[i]%>

                                            <%}%>

                                    </select>
                                </div>
                            </div>
                        </td>


                        <td id="pato" style=" display: none">
                            <div class="form-group">

                                <div class="col-md-4" >
                                    <span class="col-md-1 col-md-offset-2 "><i class="fa fa-list  bigicon" style="font-size: 16px ">Causal</i></span>
                                    <select style=" font-size: 16px " class="form-control" id="patologia" name="patologia" title="Causal de la Citaci�n"    >
                                        <option value="0">Causal de la Citaci�n
                                            <%     for (patologia pato
                                                        : cpato.buscarPatologiaporEspecialidad(es)) {

                                                    if (pato.getId() == patologia) {%>
                                        <option selected value="<%=pato.getId()%>"><%=pato.getDescripcion()%><%
                                        } else {%>
                                        <option value="<%=pato.getId()%>"><%=pato.getDescripcion()%><%
                                                }

                                            }
                                            %> 
                                    </select>
                                </div>
                            </div>  

                        </td>

                        <%if (atencion1 == 3) {%>

                        <td>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2"  ><i class="fa fa-list bigicon" style="font-size: 16px;" >Prestaci�n:</i> </span>

                                <div class="col-md-4"   >

                                    <select class="form-control" id="prestacion" name="prestacion" title="Prestacion">
                                        <option value="-1">Elegir Prestacion
                                            <%
                                                for (procedimiento p : cp.buscarPrestaciones(e.getId_especialidad(), atencion1)) {

                                            %>
                                        <option value="<%=p.getId()%>"><%=p.getPrestacionesfonasa() + " " + p.getDescripcion()%><%
                                            }


                                            %> 
                                    </select>

                                </div>
                            </div>
                        </td>

                        <% }%> 
                        <%if (atencion1 == 4) {%>

                        <td>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2"  ><i class="fa fa-list bigicon" style="font-size: 16px;" >Procedimiento a Realizar:</i> </span>

                                <div class="col-md-4"   >

                                    <select class="form-control" id="procedimientos" name="procedimientos" title="Procedimientos a Solicitar ">
                                        <option value="-1">Elegir Procedimiento
                                            <%for (lugar lu : ce.buscarlosProcedimientosporEspecialidad(e.getId_especialidad())) {%>
                                        <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%></option>

                                        <%}%>
                                    </select>

                                </div>
                            </div>
                        </td>

                        <% }%> 
                        <%if (atencion1 == 5) {%>

                        <td>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2"  ><i class="fa fa-list bigicon" style="font-size: 16px;" >Examenes a Realizar:</i> </span>

                                <div class="col-md-4"   >

                                    <select class="form-control" id="examen" name="examen" title="Examen a Realizar ">
                                        <option value="-1">Elegir Examen
                                            <%
                                                for (lugar lu : ce.buscarlosExamenesporEspecialidad(e.getId_especialidad())) {

                                            %>
                                        <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%><%
                                            }


                                            %> 
                                    </select>

                                </div>
                            </div>
                        </td>

                        <% }%> 
                    </tr>

                </table>
                <table>
                    <tr>
                        <td>
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                        </td>
                        <td colspan="5">
                            <div class="col-md-4">

                                <input type="text" id="rutpaciente" name="rutpaciente" style="text-transform:uppercase;" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                        return false" onkeyup="formateaRut(this.value);" autocomplete=off onclick="javascript:document.getElementById('rutpaciente').select();" maxlength="12" class="form-control" placeholder="Rut Paciente">
                                <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                                <input value="d" name="txtDV" id="txtDV" type="hidden">

                            </div></td>
                        <td colspan="3"><button type="button" class="btn btn-primary " onclick="javascript : buscarFonasa()">Buscar</button> </td>


                    </tr>





                </table>


                <div id="busqueda">            

                </div>   


                <div class="form-group">
                    <div class="col-md-4 text-center">
                        <button type="button" id="asignar" onclick="javascritp:  document.cita.action = '<%=cc.getLocal()%>AsignarCita';
                                document.cita.submit();" class="btn btn-primary btn-lg" style="align-content: center; display: none" >Asignar Cita</button>


                    </div>
                </div>







            </fieldset>




        </form>

        <script>


            $('body').keyup(function (e) {

                if (e.keyCode == 13) {


                    return buscarFonasa();
                }


            });




            function buscargesCausal() {
                var ges = document.forms['cita']['patologiages'].value;
                if (ges == 1) {

                    document.getElementById('pato').style.display = 'block';
                } else
                    document.getElementById('pato').style.display = 'none';
            }


            function buscaProvincia() {

                if (document.getElementById('region').value == -1)
                {
                    Alert.render("Debe Completar los Datos para Continuar");
                    document.getElementById('region').focus();
                } else
                {

                    var region = document.getElementById('region').value;

                    try
                    {
                        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                    } catch (e)
                    {
                        try
                        {
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (E) {
                            xmlhttp = false;
                        }
                    }
                    if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                        xmlhttp = new XMLHttpRequest();
                    }

                    if (xmlhttp) {
                        var objeto_recibidor = document.getElementById("provincia1");
                        xmlhttp.open("post", "buscarprovincia.jsp?region=" + region);
                        xmlhttp.send("");
                        if (xmlhttp.readyState == 1) {
                            objeto_recibidor.innerHTML = '</br></br><b> </b>';

                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                objeto_recibidor.innerHTML = xmlhttp.responseText;



                            }
                            if (xmlhttp.status != 200) {
                                objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                            }
                        }
                    }

                }

            }
            function buscaComuna(provincia) {

                if (document.getElementById('provincia').value == -1)
                {
                    Alert.render("Debe Completar los Datos Eliga una Provincia");
                    document.getElementById('provincia').focus();
                } else
                {
                    try
                    {
                        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                    } catch (e)
                    {
                        try
                        {
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (E) {
                            xmlhttp = false;
                        }
                    }
                    if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                        xmlhttp = new XMLHttpRequest();
                    }

                    if (xmlhttp) {
                        var objeto_recibidor = document.getElementById("comuna1");
                        xmlhttp.open("post", "buscarcomuna.jsp?provincia=" + provincia);
                        xmlhttp.send("");
                        if (xmlhttp.readyState == 1) {
                            objeto_recibidor.innerHTML = '</br></br><b></b>';

                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                objeto_recibidor.innerHTML = xmlhttp.responseText;


                            }
                            if (xmlhttp.status != 200) {
                                objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                            }
                        }
                    }

                }

            }



            function formateaRut(Rut)
            {
                var sRut = new String(Rut);
                var sRutFormateado = '';
                sRut = quitaFormato(sRut);
                var sDV = sRut.charAt(sRut.length - 1);
                sRut = sRut.substring(0, sRut.length - 1);
                document.forms["cita"].txtRutSinDV.value = sRut;
                document.forms["cita"].txtDV.value = sDV;
                while (sRut.length > 3)
                {
                    sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
                    sRut = sRut.substring(0, sRut.length - 3);
                }
                sRutFormateado = sRut + sRutFormateado;
                if (sRutFormateado != "")
                    sRutFormateado += "-";
                sRutFormateado += sDV;
                if (document.forms["cita"].rutpaciente.value != sRutFormateado)
                    document.forms["cita"].rutpaciente.value = sRutFormateado;
            }
            function quitaFormato(Nro)
            {
                var strNro = new String(Nro);
                while (strNro.indexOf(".") != - 1)
                    strNro = strNro.replace(".", "");
                strNro = strNro.replace("-", "");
                return strNro;
            }
            function DigitoVerificadorRut(strRut) {
                var rut = 0;
                var s = 0;
                var l_dv = "";

                rut = strRut;
                for (i = 2; i < 8; i++) {
                    s = s + (rut % 10) * i;
                    rut = (rut - (rut % 10)) / 10;
                }
                s = s + (rut % 10) * 2;
                rut = (rut - (rut % 10)) / 10;
                s = s + (rut % 10) * 3;
                rut = (rut - (rut % 10)) / 10;
                s = 11 - (s % 11);
                if (s == 10)
                    l_dv = "K";
                else
                if (s == 11)
                    l_dv = "0"
                else
                    l_dv = s + "";
                return(l_dv);
            }


            function validaRut() {
                var sRut = new String(document.forms["cita"]["rutpaciente"].value);
                sRut = quitaFormato(sRut);
                var sDV = new String(sRut.charAt(sRut.length - 1));
                sRut = sRut.substring(0, sRut.length - 1);
                if (sDV.toUpperCase() == DigitoVerificadorRut(sRut))
                {
                    return true;
                }
                if (sDV.toUpperCase() != DigitoVerificadorRut(sRut))
                {
                    return false;
                }
            }



            function buscarFonasa()

            {

                document.getElementById("asignar").style.display = "none";
                var fecha = document.forms["cita"]["txt_inicial"].value;

                var paciente = document.forms["cita"]["rutpaciente"].value;

                var doctor = document.forms["cita"]["funcionario"].value;

                var oferta = document.forms["cita"]["oferta"].value;

                var atencion = document.forms["cita"]["atencion"].value;

                var ges = document.forms["cita"]["patologiages"].value;

                var diagnosticoespecialidad = document.forms["cita"]["diagnosticocita"].value;


                var e = document.forms["cita"]["especialidad"].value;

                var prestacion = "-1";
                var examen = "-1";
                var procedimiento = "-1";


                if (atencion == 3) {
                    prestacion = document.forms["cita"]["prestacion"].value;

                }
                if (atencion == 4) {
                    procedimiento = document.forms["cita"]["procedimientos"].value;

                }
                if (atencion == 5) {
                    examen = document.forms["cita"]["examen"].value;

                }

                //  var prestacion = document.forms["cita"]["prestacion"].value;


                /*if antencion no es tres debo preguntar por el motivo de la consulta y si es tres por la prestacion y la lateralidad*/

                if (fecha == null || fecha == "" || paciente == null || paciente == "" || paciente.length == 0 || doctor == 0 || doctor == "" || atencion == 0 || atencion == "" || ges == 0 || ges == "" || oferta == -1 || oferta == "" || diagnosticoespecialidad == 0)
                {
                    Alert.render("Debe Completar los Datos para Continuar");

                } else if (atencion == "3" && (prestacion == "-1")) {

                    Alert.render("Debe Elegir la Prestacion");



                } else if (atencion == "4" && (procedimiento == "-1")) {

                    Alert.render("Debe Elegir el Procedimiento a Realizar");



                } else if (atencion == "5" && (examen == "-1")) {

                    Alert.render("Debe Elegir la Examen a Realizar");



                } else {
                    if (validaRut(document.forms["cita"]["rutpaciente"].value))
                    {
                        entre = true;
                        var sRut = new String(paciente);
                        sRut = quitaFormato(sRut);
                        var sDV = sRut.charAt(sRut.length - 1);
                        sRut = sRut.substring(0, sRut.length - 1);

                        var r = sRut;


                        var dv = sDV;


                        try
                        {
                            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                        } catch (e)
                        {
                            try
                            {
                                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                            } catch (E) {
                                xmlhttp = false;
                            }
                        }
                        if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                            xmlhttp = new XMLHttpRequest();
                        }

                        if (xmlhttp) {
                            var objeto_recibidor = document.getElementById("busqueda");
                            xmlhttp.open("post", "buscarFonasaCita.jsp?fecha=" + fecha + "&paciente=" + paciente + "&rut=" + r + "&dv=" + dv + "&especialidad=" + e + "&oferta=" + oferta);
                            xmlhttp.send("");
                            if (xmlhttp.readyState == 1) {
                                objeto_recibidor.innerHTML = '<b>Favor espere... </b>';

                            }
                            xmlhttp.onreadystatechange = function () {
                                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                                    objeto_recibidor.innerHTML = xmlhttp.responseText;


                                }
                                if (xmlhttp.status != 200) {



                                    objeto_recibidor.innerHTML = mivariable;
                                    document.getElementById("asignar").style.display = "none";

                                    document.getElementById("nombrepaciente").removeAttribute("readonly", false);
                                    document.getElementById("apellidoppaciente").removeAttribute("readonly", false);
                                    document.getElementById("apellidompaciente").removeAttribute("readonly", false);
                                    document.getElementById("telefonoPaciente").removeAttribute("readonly", false);
                                    document.getElementById("telefonoPaciente1").removeAttribute("readonly", false);
                                    document.getElementById("email").removeAttribute("readonly", false);
                                    document.getElementById("direccion").removeAttribute("readonly", false);
                                    document.getElementById("calle").removeAttribute("readonly", false);

                                    document.getElementById("txt_inicial").removeAttribute("readonly", false);


                                    error = 1;

                                }



                                /*TERMINA EL ERROR*/

                            }
                        }


                    } else {
                        Alert.render("Rut Invalida");
                    }
                    /*rut invalida*/
                }
            }





            function validar() {
                var x = document.forms["cita"]["genero"].value;
                var z = document.forms["cita"]["provision"].value;
                var k = document.forms["cita"]["region"].value;

                var b = document.forms["cita"]["comuna"].value;
                var paciente = document.forms["cita"]["rutpaciente"].value;
                var direccion = document.forms["cita"]["direccion"].value;
                var telefono = document.forms["cita"]["telefonoPaciente"].value;
                var tramo = document.forms["cita"]["tramo"].value;
                var atencion = document.forms["cita"]["atencion"].value;
                 var prestacion = "-1";

                var motivoconsulta = "-1";

                if (atencion == 3) {
                    prestacion = document.forms["cita"]["prestacion"].value;

                }


                if (telefono.length < 9 || paciente == null || paciente == "" || direccion == null || direccion == "" || b == "-1" || x == "0" || z == "0" || k == "-1") {

                    Alert.render("Revise los Datos, formato de telefono!!, Direccion, Prevision!!  Debe Completar los Datos para continuar");


                } else if (tramo == "0" && z == "1") {
                    Alert.render("Si es Fonasa debe colocar el Tramo!! Debe Completar los Datos para Continuar");
                } else if (atencion == "3" && (prestacion == "-1")) {
                    Alert.render("Debe Elegir la  Prestacion para continuar ");
                } else {

                    document.cita.action = '<%=cc.getLocal()%>AsignarCita';
                    document.cita.submit();

                }


            }

            /*buscar prestacion*/



        </script>



    </body>

</html>

