<%-- 
    Document   : SolicitudesdeExamenesyProcedimientos
    Created on : 18-12-2018, 15:18:13
    Author     : a
--%>

<%@page import="Modelos.solicitudExamenes_Procedimientos"%>
<%@page import="java.util.Vector"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<%
    controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
%>

<div class="tab-content">
    <div class="container">
        <div class="tab-content" >
            <fieldset>

                <legend class="text-center header">Seguimiento a solicitudes de examenes y procedimientos Oftalmologicos</legend>
                <table class="table table-striped" style="width: 100%">
                    <strong>
                        <tr>
                            <th>Fecha Atención</th>
                            <th>Profesional Solicitante</th>
                            <th>Paciente</th>
                            <th>Examenes</th>
                            <th>Procedimientos</th>
                            <th>Ver</th>
                        </tr>
                    </strong>


                    <tr>
                        <% for (solicitudExamenes_Procedimientos sp : caco.buscarSolicitudesParaSeguimientos()) {%>
                        <td><%=sp.getFecha()%></td>
                        <td><%=sp.getNombreprofesional()%></td>
                        <td><%=sp.getRutpaciente() + ' ' + sp.getNombrepaciente()%></td>
                        <td>
                            <table>

                                <%for (solicitudExamenes_Procedimientos s : caco.solicitudesdetalle(sp.getIdsolicitud(), 1)) {
                                %>
                                <tr>
                                    <td>
                                        <%=s.getVariable1()%>  <input type="checkbox"  name="" id="" value="checkbox">
                                    </td>
                                </tr>
                                <%  }%> 

                            </table>
                        </td>
                        <td><table>

                                <%for (solicitudExamenes_Procedimientos s : caco.solicitudesdetalle(sp.getIdsolicitud(), 2)) {
                                %>
                                <tr>
                                    <td>
                                        <%=s.getVariable1()%><input type="checkbox"  name="" id="" value="checkbox">
                                    </td>
                                </tr>
                                <%  }%> 

                            </table></td>
                        <td><img src="../../public/imagenes/icon/file_pdf_download-128.png" onclick="javascript:    window.open('<%=caco.getLocal()%>solicituddeexamenesoftalmologico?idatencion=<%=sp.getIdatencionoftalmologia()%>', 'Solicitud de Examenes o Procedimiento Oftalmologico', 'width=600,height=450')" alt=""/></td>
                            <%}%>
                    </tr>


                </table>
            </fieldset>
        </div>
    </div>
</div>
<jsp:include page="../comunes/Footer.jsp"/>
