<%-- 
    Document   : cargarRut
    Created on : 02-04-2019, 8:53:22
    Author     : a
--%>

<%@page import="Controlador.General"%>
<%-- 
    Document   : paso1
    Created on : 13-ene-2015, 14:57:36
    Author     : Informatica
--%>

<jsp:include page="../comunes/Header.jsp" />
<% General g = new General();%>
<script>
    $(function () {
        $("#fecha_inicio").datepicker();
    });
    $(function () {
        $("#fecha_fin").datepicker();
    });

    function tipoArchivo(archivo, fecha1, fecha2)
    {


        if (fecha1 == "" || fecha2 == "" || archivo == "") {

            alert("Debe Completar los Datos para Continuar");
            return false;
        } else
            array_fecha = fecha1.split("/")

        var dia = array_fecha[0]
        var mes = (array_fecha[1] - 1)
        var ano = (array_fecha[2])
        var fechaDate = new Date(ano, mes, dia)

        array_fecha2 = fecha2.split("/")

        var dia2 = array_fecha2[0]
        var mes2 = (array_fecha2[1] - 1)
        var ano2 = (array_fecha2[2])
        var fechaDate2 = new Date(ano2, mes2, dia2)


        if (fechaDate > fechaDate2) {

            alert("Rango de Fecha Incorrecto !!Debe Completar los Datos para Continuar");
            return false;
        }



        extensiones_permitidas = new Array(".xls");
        mierror = "";

        //recupero la extensi�n de este nombre de archivo
        extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
        //alert (extension);
        //compruebo si la extensi�n est� entre las permitidas
        permitida = false;
        for (var i = 0; i < extensiones_permitidas.length; i++) {
            //  alert(extension+' '+extensiones_permitidas[i] );
            if (extensiones_permitidas[i] == extension) {
                permitida = true;
                break;
            }
        }
        if (!permitida) {
            mierror = "Comprueba la extensi�n de los archivos a subir. \nS�lo se pueden subir archivos con extension:\n" + extensiones_permitidas.join();
            alert(mierror);
            return false;
        } else {
            return true
        }

    }

</script>

<body>
    <fieldset>
        <legend class="text-center header">Reporte de Citas, Hitos y Protocolos</legend>





        <form action="<%=g.getLocal()%>Ingresarrutreporte" method=post enctype='multipart/form-data' onsubmit="return tipoArchivo(document.getElementById('file1').value, document.getElementById('fecha_inicio').value, document.getElementById('fecha_fin').value)"  >

            <table  class="table" style="width: 80%">

                <tr>
                     <td>
                        &nbsp;
                    </td>
                    <td > <span class="col-md-4 col-md-offset-2 "><i class="fa fa-calendar bigicon" style="font-size: 18px">Fecha Inicio</i></span>
                        <div class="col-md-6"> 
                            <input type="text" maxlength="13"  id="fecha_inicio" name="fecha_inicio" value="" autocomplete="off" >
                        </div>
                    </td> 
                    <td><span class="col-md-4 col-md-offset-2 "><i class="fa fa-calendar bigicon" style="font-size: 18px">Fecha Fin</i></span>
                        <div class="col-md-6">   
                            <input type="text" maxlength="13"  id="fecha_fin" name="fecha_fin" value="" autocomplete="off" >

                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>

                <tr>
                     <td>
                        &nbsp;
                    </td>

                    <td>
                        <span class="col-md-4 col-md-offset-2 ">
                            <a href="../../public/document/ejemplo.xls" target="a_blank"  >Descargar ejemplo</a>
                        </span>
                    </td>
                    <td>
                        <div class="form-group">

                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-archive bigicon"></i></span>
                            <div class="col-md-9" >

                                <input  id="file1" name="file1" class="file"  type="file" >
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>

                    <td>


                    </td>
                </tr>

            </table>
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-lg" >Generar</button>

                    <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                </div>
            </div>




            <br> 
            <br><br>


        </form>

        <br><br><br>
    </fieldset>

    <jsp:include page="../comunes/Footer.jsp" />

