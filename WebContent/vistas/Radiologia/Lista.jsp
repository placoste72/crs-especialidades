<%-- 
    Document   : Lista
    Created on : 30-04-2018, 9:14:45
    Author     : a
--%>

<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="refresh" content="20" />

        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/crs.js" type="text/javascript"></script>

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>
        <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">-->

        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        <link href="../../public/css/estilos.css" rel="stylesheet">
        <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
        <style>
            .text {
                font-size:12px;
                font-family:helvetica;
                font-weight:bold;
                color:#ff0000;
                text-transform:uppercase;
            }
            .parpadea {

                animation-name: parpadeo;
                animation-duration: 30s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;

                -webkit-animation-name:parpadeo;
                -webkit-animation-duration: 0s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;
            }

            @-moz-keyframes parpadeo{  
                0% { opacity: 1.0; }
                50% { opacity: 0.0; }
                100% { opacity: 1.0; }
            }

            @-webkit-keyframes parpadeo {  
                0% { opacity: 1.0; }
                50% { opacity: 0.0; }
                100% { opacity: 1.0; }
            }

            @keyframes parpadeo {  
                0% { opacity: 1.0; }
                50% { opacity: 0.0; }
                100% { opacity: 1.0; }
            }
        </style>

    </head>


    <body>
        <% controlador_cita c = new controlador_cita();%>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">
                    <fieldset>
                        <form name="lista" id="lista" method="post" >
                            <legend class="text-center header">Pacientes en Espera de Atención </legend>
                            <table class="table table-striped" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>RUT</th>
                                        <th>Paciente</th>
                                        <th>Edad</th>
                                        <th>Sexo</th>
                                        <th>Especialidad</th>
                                        <th>Tipo de Cita</th>
                                        <th>Hipótesis Diagnóstica</th>
                                        <th>Profesional</th>
                                        <th>Fecha y Hora</th>
                                        <th>Accion</th>
                                    </tr>
                                    <%
                                        for (cita temp : c.buscarcitasConfirmadasparaRadiologia()) {
                                    %>
                                    <tr>
                                        <td><%= temp.getMotivo_cancela()%></td>
                                        <td><%= temp.getRut_paciente()%></td>
                                        <td><%= temp.getTemporales2()%></td>
                                        <td><%= temp.getTemporales1()%></td>

                                        <td><%= temp.getTemporales()%> </td>
                                        <td><%= temp.getTemporales3()%>  </td>
                                        <td><%= temp.getTemporales4()%>  </td>
                                        <td><%= temp.getRut_doctor()%></td>
                                        <td><%= temp.getMotivo()%></td>
                                        
                                        <td style="width: 280px; "> <a class="button3"  href="<%=c.getLocallink()%>Radiologia/AtencionRadiologia.jsp?cita=<%=temp.getId_cita()%>"
                                                                       target="_top" >Atender </a>
                                                                       <a class="button2" onclick="javascript: window.open('<%=c.getLocallink()%>Radiologia/cancelarAtencionRadiologia.jsp?cita=<%= temp.getId_cita()%>&tipo=C', 'Motivo', 'width=600,height=450')">Cancelar </a> </td>          
                

                                        </td>
                                       

                                    </tr>

                                    <%}%>



                                </thead>


                            </table>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </body>
</html>