<%-- 
    Document   : ListaPacienteCitadosparaHoy
    Created on : 31-10-2017, 17:15:24
    Author     : a
--%>

<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<fieldset>
   <%
    controlador_cita cc = new controlador_cita();
    String rutdoctor = cc.getUsuarioLoggin();
   %> 
    <legend class="text-center header">Lista de Paciente Para Vicio Refraccion </legend>
    <table class="table table-striped" style=" width: 100%">
        <thead>
            <tr>
                <th> Rut</th>
                <th>Nombre</th>
                <th>Tipo Atención</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Estatus</th>
            </tr>
        </thead>
        <%for(cita c : cc.buscarCitasdeVicioRefraccionParaHoy()){ %>
        <tr>
            <td><%=c.getRut_paciente() %></td> 
            <td><%=c.getTemporales() %></td>
            <td><%=c.getTemporales1() %></td>
            <td><%=c.getTemporales2() %></td>
            <td><%=c.getTemporales3() %></td>
            <td><%=c.getTemporales4() %></td>
        </tr>
        <%}%>

    </table>


    <jsp:include page="../comunes/Footer.jsp"/>
</fieldset>