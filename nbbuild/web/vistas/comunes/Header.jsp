<%-- 
    Document   : Header
    Created on : 27-09-2016, 09:25:26 AM
    Author     : Informatica
--%>

<%@page import="java.util.Date"%>
<%@page import="Modelos.opciones"%>
<%@page import="java.util.Vector"%>
<%@page import="Controlador.controlador_menu"%>
<%@page import="Controlador.General"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>


<!DOCTYPE html voy a colocar aqui el menu>

<html >
    <head>
        <!--css-->
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="../../public/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="../../public/imagenes/icon.png">
        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css"> 
        <link href="../../public/css/estilos.css" rel="stylesheet"> 
        <link href="../../public/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/> 
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
        <!--script-->
        <script src="../../public/js/script2.js" type="text/javascript"></script>
        <script src="../../public/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>


        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/> 
        <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
        
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/fileinput.js" type="text/javascript"></script>
        <script src="../../public/js/fileinput_locale_fr.js" type="text/javascript"></script>
        <script src="../../public/js/fileinput_locale_es.js" type="text/javascript"></script>
        <link href="../../public/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>

        <script src="../../public/js/crs.js" type="text/javascript"></script>
        <style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
        <script type="text/javascript" src="../../public/js/calendar.js"></script>
        <script type="text/javascript" src="../../public/js/calendar-es.js"></script>

        <script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
       
       
       
        <%
            General g = new General();
            Authentication auth = SecurityContextHolder.getContext()
                    .getAuthentication();
            
            
            if (auth.getName().equals(null)) {

                response.sendRedirect("../../j_spring_security_logout");

            }

            controlador_menu cm = new controlador_menu();
            boolean tiene = false;


        %>

    <div class="navbar-wrapper">
        <div class="container-fluid">
            <nav class="navbar navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">

                        <a class="navbar-brand" href="<%=g.getLocal()%>"><img src="../../public/imagenes/CRSlogo.png" width="40px" height="40px"/></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">

                            <% int tipo = cm.buscarperfil(auth.getName());
                                if (auth.getName() != null) {
                                    tiene = cm.versiTieneMenu1(auth.getName());
                                    if (tiene == true) {

                            %>

                            <li class=" dropdown">
                                <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <%   for (opciones temp : cm.buscarmenu1(auth.getName())) {


                                    %>

                                    <li class=" dropdown">
                                        <a href='<%=g.getLocallink() + temp.getUrl()%>'><%=temp.getNombre()%></a>
                                    </li>
                                    <%
                                        }

                                    %>  
                                </ul>
                            </li>
                            <%}
                                tiene = cm.versiTieneMenu2(auth.getName());
                                if (tiene == true) {
                            %>

                            <li class=" dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gesti�n de Cupos<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <%  for (opciones temp2 : cm.buscarmenu2(auth.getName())) {

                                    %>
                                    <li><a href='<%=g.getLocallink() + temp2.getUrl()%>'><%=temp2.getNombre()%></a></li>
                                        <%

                                            }
                                        %>  

                                </ul>
                            </li>
                            <%
                                    }
                                }

                                boolean t = cm.versitieneMenu3(auth.getName());
                                if (t == true) {
                            %> 
                            <li class=" dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gesti�n de Pacientes<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <%
                                        for (opciones temp3 : cm.buscarmenu3(auth.getName())) {


                                    %>
                                    <li><a href='<%=g.getLocallink() + temp3.getUrl()%>'><%=temp3.getNombre()%></a></li>
                                        <%

                                            }

                                        %>  

                                </ul>
                            </li>

                            <%}
                                boolean tieneinforme = cm.buscarsitienemenu4(auth.getName());
                                if (tieneinforme == true) {
                            %>
                            <li class=" dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Informes<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                     <li><a href='http://localhost:8084/agenda/'>Estadisticos modulo_agenda</a></li>
                                   
                                    <%     for (opciones temp4 : cm.buscarmenu4(auth.getName())) {


                                    %>
                                    <li><a href='<%=g.getLocallink() + temp4.getUrl()%>'><%=temp4.getNombre()%></a></li>
                                        <%

                                            }

                                        %>  

                                </ul>
                            </li>
                            <%  } else {
                                    response.sendRedirect("../../j_spring_security_logout");
                                }
                               
                            %>
                            <li class=""><a href="<%=g.getLocal()%>vistas/Planificar/Agenda.jsp">Agenda</a></li>
                            <% if (tipo == 1) { %>
                                <li class=""><a href="<%=g.getLocal()%>vistas/Planificar/CargaMasivaCitas.jsp">Carga Masiva de Citas</a></li>    
                            <% } %>
                            
                        </ul>

                        <ul class="nav navbar-nav pull-right">

                            <li class=" dropdown"><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true">Ayuda<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <% if (tipo == 1) {
                                    %>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/administrarsistema.jsp">�Como Administrar el Sistema?</a></li>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/gestioncupos.jsp">�Como Gestionar Cupos?</a></li>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/gestionpaciente.jsp">�Como Gestionar Pacientes?</a></li>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/informes.jsp">�Como Generar Informe?</a></li>
                                        <%
                                        } else if (tipo == 3) {
                                        %>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/perfilgestion.jsp">�Como Usar el Sistema?</a></li>
                                        <%
                                        } else if (tipo == 4 || tipo == 2) {
                                        %>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/gestionpaciente.jsp">�Como Gestionar Pacientes?</a></li>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/informes.jsp">�Como Generar Informe?</a></li>
                                        <%
                                        } else if (tipo == 12) {
                                        %>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/perfiltecnico.jsp">�Como Usar el Sistema?</a></li>

                                    <%
                                    } else if (tipo == 14) {
                                    %>
                                    <li><a href="<%=g.getLocal()%>vistas/Ayudas/perfiltecnologo.jsp">�Como Usar el Sistema?</a></li>

                                    <%

                                        }

                                    %>
                                </ul>
                            </li>
                        

                            <li class=" dropdown"><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true">Cuenta<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<%=g.getLocal()%>vistas/Planificar/CambiarContrasena.jsp">Cambiar Contrase�a</a></li>
                                    <li><a href="<%=g.getLocal()%>vistas/Planificar/subirfoto.jsp">Cambiar Foto</a></li>
                                    <li class=""><a href="../../j_spring_security_logout">Salir</a></li>
                                </ul>
                            </li>
                            <li class=" dropdown"  style=" color: white ; font-size:16px ">   <img src='<%=g.getLocal()%>FotoPerfil' width="40px" height="40px"> <%=g.buscarombreDelUsuairioensession()%></li>
                      

                        </ul>
                    </div>
                </div>
            </nav>

        </div>
    </div>


</head>
<body>





