

<%@page import="Modelos.atencion"%>
<%@page import="Controlador.controlador_atencion"%>
<%@page import="Controlador.controlador_doctor"%>
<%@ page import="java.io.FileOutputStream,java.io.*,java.awt.Color,java.util.Vector,java.util.Date,java.text.DateFormat,java.util.Locale" %>
<%@ page import="com.lowagie.text.*,  com.lowagie.text.pdf.*,com.lowagie.text.pdf.PdfStamper,com.lowagie.text.pdf.PdfPageEventHelper" %>
<%@ page import="com.lowagie.text.pdf.PdfEncryptor,com.lowagie.text.pdf.PdfReader,com.lowagie.text.pdf.PdfWriter,com.lowagie.text.Chunk" %>
<%@ page import="com.lowagie.text.Document,com.lowagie.text.Element,com.lowagie.text.ExceptionConverter,com.lowagie.text.Font"%>
<%@ page import="com.lowagie.text.Image,com.lowagie.text.PageSize,com.lowagie.text.Rectangle,com.lowagie.text.pdf.BaseFont"%>
<%@ page import="com.lowagie.text.pdf.PdfContentByte,com.lowagie.text.pdf.PdfGState,com.lowagie.text.pdf.PdfPTable" %>
<%@ page import="com.lowagie.text.HeaderFooter,com.lowagie.text.Header,com.lowagie.text.pdf.PdfWriter"%>
<%@ page import="java.sql.*,java.util.GregorianCalendar,java.util.Calendar" %>

<%@include file="../comunes/pdfconpieyfoto.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%    Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
    DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

    String foliot = "";
    String fechat = "";
    String nombret = "";
    String rutt = "";
    String fechanacimientot = "";
    String edadt = "";
    String sexot = "";
    String direcciont = "";
    String comunat = "";
    String telefono1t = "";
    String emailt = "";
    String sederivaparat = "";
    String seeviaparat = "";
    String diagnosticot = "";
    String examenesrealizadost = "";
    String nombredoctort = "";
    String rutdoctort = "";
    String confirmacionvicio = "";
    String pfs = "";
    String fundamento = "";

    String llego = request.getParameter("idatencion");
    controlador_atencion ca = new controlador_atencion();
    atencion a = ca.informeProcesoDiagnostico(Integer.parseInt(llego));

    foliot = a.getFolio();
    fechat = a.getFormatofecha();
    nombret = a.getNombrepaciente();
    rutt = a.getRutpaciente();
    fechanacimientot = a.getFechanacimiento();
    edadt = a.getEdadpaciente();
    sexot = a.getSexo();
    confirmacionvicio = a.getConf();
    diagnosticot = a.getDetallereceta();
    examenesrealizadost = a.getVesicula();
    nombredoctort = a.getProfesion();
    fundamento = a.getFundamentodeldiagnostico();
    rutdoctort = a.getRutdoctor();
    pfs = a.getProfesion();
            response.setContentType("application/pdf");
    Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    PdfWriter writer = PdfWriter.getInstance(document, buffer);

    int SPACE_TITULO = 1;
    int SPACE_NORMAL = 12;
    int SPACE_NORMAL2 = 17;
    int SPACE_ESPACIO = 4;
    Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(68, 117, 196));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0));
    Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
    Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0, 0, 0));
    Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
    Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
    Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD, new Color(68, 114, 196));
    Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
    writer.setPageEvent(new PageNumbersWatermark());
    document.open();

    Table tabla_titulo;
    Cell celda;

    tabla_titulo = new Table(3);
    tabla_titulo.setBorderWidth(0);
    tabla_titulo.setPadding(1);
    tabla_titulo.setSpacing(0);
    tabla_titulo.setWidth(100);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    String serviciot = "";
    String establecimientot = "";
    String especialidadt = "";
    String unidadt = "";

    serviciot = "Metropolitano Central(SSMC)";
    establecimientot = "Centro de Referencia de Salud de Maipú";
    especialidadt = "Oftalmología";
    unidadt = "Oftalmología";

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "INFORME DEL PROCESO DIAGNÓSTICO - IPD", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph rutCita = new Paragraph();
    rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, "Folio:", TEXT_NORMAL));
    rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, foliot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(rutCita);
    celda.setBorderWidth(0);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph nombreCita = new Paragraph();
    nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, "Fecha y hora del Informe: ", TEXT_NORMAL));
    nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, fechat, TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombreCita);
    celda.setBorderWidth(0);

    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Prestador", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph fechaCita = new Paragraph();
    fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, "Servicio de Salud: ", TEXT_NORMAL));
    fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, serviciot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(fechaCita);
    celda.setBorderWidth(0);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph horaCita = new Paragraph();
    horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, "Establecimiento: ", TEXT_NORMAL));
    horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, establecimientot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(horaCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph especialidadCita = new Paragraph();
    especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, "Especialidad: ", TEXT_NORMAL));
    especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, especialidadt, TEXT_SUPERTITULONORMAL));
    celda = new Cell(especialidadCita);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph medicoCita = new Paragraph();
    medicoCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    medicoCita.add(new Phrase(SPACE_TITULO, "Unidad:", TEXT_NORMAL));
    medicoCita.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
    medicoCita.add(new Phrase(SPACE_TITULO, unidadt, TEXT_SUPERTITULONORMAL));
    celda = new Cell(medicoCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /*datos del paciente*/
    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Paciente", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph AtencionGlosa = new Paragraph();
    AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
    AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, nombret, TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa);
    //celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph AtencionGlosa1 = new Paragraph();
    AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
    AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    AtencionGlosa1.add(new Phrase(SPACE_TITULO, rutt, TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa1);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph fechanacimiento = new Paragraph();
    fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechanacimiento.add(new Phrase(SPACE_TITULO, "Fecha de nacimiento: ", TEXT_NORMAL));
    fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fechanacimiento.add(new Phrase(SPACE_TITULO, fechanacimientot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(fechanacimiento);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph edad = new Paragraph();
    edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    edad.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
    edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    edad.add(new Phrase(SPACE_TITULO, edadt + " años", TEXT_SUPERTITULONORMAL));
    celda = new Cell(edad);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph sexo = new Paragraph();
    sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    sexo.add(new Phrase(SPACE_TITULO, "Sexo: ", TEXT_NORMAL));
    sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    sexo.add(new Phrase(SPACE_TITULO, sexot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(sexo);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /*Datos de derivacion*/
    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Datos Clínicos", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph sederiva = new Paragraph();
    sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    sederiva.add(new Phrase(SPACE_TITULO, "Problema de Salud AUGE: ", TEXT_NORMAL));
    sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    sederiva.add(new Phrase(SPACE_TITULO, "Vicios de Refracción", TEXT_SUPERTITULONORMAL));
    celda = new Cell(sederiva);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph seenvia = new Paragraph();
    seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    seenvia.add(new Phrase(SPACE_TITULO, "¿Confirma  que el Diagnóstico pertenece al sistema AUGE? ", TEXT_NORMAL));
    seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    seenvia.add(new Phrase(SPACE_TITULO, confirmacionvicio, TEXT_SUPERTITULONORMAL));
    celda = new Cell(seenvia);
    //  celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph diagnostico = new Paragraph();
    diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    diagnostico.add(new Phrase(SPACE_TITULO, "Diagnóstico: ", TEXT_NORMAL));
    diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    diagnostico.add(new Phrase(SPACE_TITULO, diagnosticot, TEXT_SUPERTITULONORMAL));
    celda = new Cell(diagnostico);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph fd = new Paragraph();
    fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fd.add(new Phrase(SPACE_TITULO, "Fundamentos del diagnóstico: ", TEXT_NORMAL));
    fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    fd.add(new Phrase(SPACE_TITULO, fundamento, TEXT_SUPERTITULONORMAL));
    celda = new Cell(fd);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph examenes = new Paragraph();
    examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    examenes.add(new Phrase(SPACE_TITULO, "Tratamiento e indicaciones: ", TEXT_NORMAL));
    examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    examenes.add(new Phrase(SPACE_TITULO, examenesrealizadost, TEXT_SUPERTITULONORMAL));
    celda = new Cell(examenes);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Profesional Tratante", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph nombredoctor = new Paragraph();
    nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
    nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    nombredoctor.add(new Phrase(SPACE_TITULO, nombredoctort, TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombredoctor);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph rutdoctor = new Paragraph();
    rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutdoctor.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
    rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    rutdoctor.add(new Phrase(SPACE_TITULO, rutdoctort, TEXT_SUPERTITULONORMAL));
    celda = new Cell(rutdoctor);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph profesiondoctor = new Paragraph();
    profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    profesiondoctor.add(new Phrase(SPACE_TITULO, "Profesión: ", TEXT_NORMAL));
    profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
    profesiondoctor.add(new Phrase(SPACE_TITULO, pfs, TEXT_SUPERTITULONORMAL));
    celda = new Cell(profesiondoctor);
    // celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    document.add(tabla_titulo);
    /*para la firma del doctor*/
    controlador_doctor cdoctor = new controlador_doctor();

    byte[] imag = cdoctor.obtenImagenDoctor(rutdoctort);
    if (imag != null) {
        Image image22 = Image.getInstance(imag);

        image22.setAbsolutePosition(15f, 15f);
        document.add(image22);
    }

    document.close();
    try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
        DataOutput output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
//output.flush();            
//output.close();
    } catch (Exception exstream) {
    }

   
%>