<%-- 
    Document   : indexEstadisticos
    Created on : 10-03-2017, 11:50:50
    Author     : a
--%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.controlador_usuario"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Modelos.funcionario"%>
<%@page import="Controlador.General"%>
<!DOCTYPE html >
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../comunes/Header.jsp"/>
<link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/crs.js" type="text/javascript"></script>
<title>Estadisticos</title>

<body>
    <%
        General g = new General();
        controlador_usuario cu = new controlador_usuario();
        controlador_especialidad ce = new controlador_especialidad();
        String tipo = "-1";
        int es = -1;
        String doc = "-1";
        if (request.getParameter("funcionario") != null) {
            doc = request.getParameter("funcionario");
        }
        if (request.getParameter("especialidad") != null) {
            es = Integer.parseInt(request.getParameter("especialidad"));
        }
        if (request.getParameter("informe") != null) {
            tipo = request.getParameter("informe");

        }
        String inicio = "";
        String fin = "";
        if (request.getParameter("fecha_inicio") != null) {
            inicio = request.getParameter("fecha_inicio");
        }
        if (request.getParameter("fecha_fin") != null) {
            fin = request.getParameter("fecha_fin");
        }

        String citas[] = {"Seleccionar", "Citas", "Hitos del Paciente", "Nomina de pacientes con cita canceladas por bloqueo", "Pacientes que no se Presentaron",
            "Lista de Pacientes por Profesional", "Lista de Pacientes para Portada de Ficha Archivo", "Atenciones Electrocardiograma", "Atenciones Examenes de Laboratorio",
            "Citas con Detalle de Atención", "Planificaciones de Profesionales con Prestaciones", "Tabla Quirurgica","Lista de Controles","Lista de Lentes Recetados","Lista Solicitudes de Pabellon"};


    %>

   
    <style>
        .hk {
            background-color: red;
            height: 10px;
        }
    
       
        

    </style>

    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>

    <div class="container"> 

        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">

                    <form id="infomes" name="infomes"  action=""  method="post" onsubmit="return vali()" > 
                        <fieldset style="margin-top: 35px ">
                            <legend class="text-center header">Informe</legend>								

                            <table>
                                <tr>
                                    <td>

                                        <span class="col-md-1 col-md-offset-2 "><i class="fa fa-list-alt bigicon" ></i></span>
                                        <div class="col-md-8"  > 

                                            <select class="form-control" id="informe" name="informe" title="Tipo Informe">  

                                                <%
                                                    for (int i = 0; i < citas.length; ++i) {
                                                        if (Integer.parseInt(tipo) == i) {
                                                %>
                                                <option selected value="<%=i%>"> <%=citas[i]%>
                                                    <% } else {%>
                                                <option value="<%=i%>"><%=citas[i]%></option>   
                                                <%}
                                                          }%>



                                            </select>

                                        </div>

                                    </td>

                                    <td>

                                        <span class="col-md-1 col-md-offset-2 "><i class="fa fa-list-alt bigicon" ></i></span>

                                        <div class="col-md-8" > 
                                            <select class="form-control" id="especialidad" name="especialidad" title="Especialidad"  onchange="javascript: document.infomes.action = 'indexEstadisticos.jsp';
                                                    document.infomes.submit();">  
                                                <option value="-1">Todas 
                                                    <%     for (especialidades espe
                                                                : ce.buscarEspecialdadTodas()) {

                                                            if (es == espe.getId_especialidad()) {%>
                                                <option selected value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                } else {%>
                                                <option value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                        }

                                                    }
                                                    %>
                                            </select>


                                        </div>  
                                    </td>
                                    <td>


                                        <span class="col-md-1 col-md-offset-2 " ><i class="fa fa-user-md bigicon" ></i></span>

                                        <div class="col-md-8" >
                                            <select class="form-control" id="funcionario" name="funcionario" title="Doctor">
                                                <option value="-1">Doctor
                                                    <%         for (funcionario f
                                                                : cu.BuscarFuncionaroconDoctorporEspecialidad(es)) {

                                                            if (doc.equals(f.getRut())) {%>
                                                <option selected value="<%=f.getRut()%>"><%=f.getNombre()%><%
                                                } else {%>
                                                <option value="<%=f.getRut()%>"><%=f.getNombre()%><%
                                                        }

                                                    }
                                                    %>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br>

                                    </td>
                                </tr>
                                <tr>

                                    <td > <span class="col-md-4 col-md-offset-2 "><i class="fa fa-calendar bigicon" style="font-size: 18px">Fecha Inicio</i></span>
                                        <div class="col-md-6"> 
                                            <input type="text" maxlength="13"  id="fecha_inicio" name="fecha_inicio" value="" autocomplete="off" >
                                        </div>
                                    </td> 
                                    <td><span class="col-md-4 col-md-offset-2 "><i class="fa fa-calendar bigicon" style="font-size: 18px">Fecha Fin</i></span>
                                        <div class="col-md-6">   
                                            <input type="text" maxlength="13"  id="fecha_fin" name="fecha_fin" value="" autocomplete="off" >

                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8" >
                                            <img src="../../public/imagenes/icon/file_Exel_download-128.png" alt="" onclick="javascrit: return vali();" /> 


                                            <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascrit: return valiPdf();" /> 
                                        </div>

                                    </td></tr>


                            </table>






                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div> 


    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <script>
    $(function () {
        $("#fecha_inicio").datepicker();
    });
    $(function () {
        $("#fecha_fin").datepicker();
    });
    
     function vali() {


            var fechaIncio = document.forms["infomes"]["fecha_inicio"].value;
            var fechaFin = document.forms["infomes"]["fecha_fin"].value;
            array_fecha = fechaIncio.split("/")

            var dia = array_fecha[0]
            var mes = (array_fecha[1] - 1)
            var ano = (array_fecha[2])
            var fechaDate = new Date(ano, mes, dia)

            array_fecha2 = fechaFin.split("/")

            var dia2 = array_fecha2[0]
            var mes2 = (array_fecha2[1] - 1)
            var ano2 = (array_fecha2[2])
            var fechaDate2 = new Date(ano2, mes2, dia2)
            var tipo = document.forms["infomes"]["informe"].value;
            var especialidad = document.forms["infomes"]["especialidad"].value;
            var doctor = document.forms["infomes"]["funcionario"].value;
            if (fechaIncio == "" || fechaFin == "" || tipo == -1) {

                Alert.render("Debe Completar los Datos para Continuar");
                return false;
            } else if (fechaDate > fechaDate2) {

                Alert.render("Rango de Fecha Incorrecto !!Debe Completar los Datos para Continuar");
                return false;
            }

            /*nuevo codigo*/
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }



            if (tipo == 1)
            {

                document.infomes.action = "<%=ce.getLocal()%>citas?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }

            if (tipo == 2)
            {

                document.infomes.action = "<%=ce.getLocal()%>hitospacientes?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            if (tipo == 3) {

                document.infomes.action = "<%=ce.getLocal()%>Nominapaciente?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            if (tipo == 4) {

                document.infomes.action = "<%=ce.getLocal()%>citasNSP?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            if (tipo == 5) {

                Alert.render("Solo esta disponible para PDF");
                return false;
            }
            if (tipo == 6) {

                document.infomes.action = "<%=g.getLocal()%>PortadaparaFichasArchivo?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            if (tipo == 7) {

                document.infomes.action = "<%=g.getLocal()%>PacienteAtendido?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            if (tipo == 8) {

                document.infomes.action = "<%=g.getLocal()%>PacienteAtendidosExamenes?inicio=" + fechaIncio + "&fin=" + fechaFin;
                document.infomes.submit();
            }
            if (tipo == 9) {

                document.infomes.action = "<%=g.getLocal()%>InformeGeneraldeAtenciones?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            if (tipo == 10) {

                document.infomes.action = "<%=g.getLocal()%>InformeplanificacionporFecha?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            
             if (tipo == 11) {

                document.infomes.action = "<%=g.getLocal()%>TablaQuirurgica?inicio=" + fechaIncio + "&fin=" + fechaFin + "";
                document.infomes.submit();
            }
             if (tipo == 12) {

                document.infomes.action = "<%=g.getLocal()%>listacontroles?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
             if (tipo == 13) {

                document.infomes.action = "<%=g.getLocal()%>InformedeLentes?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }
            if (tipo == 14) {

                document.infomes.action = "<%=g.getLocal()%>listasolicitudesQuirofano?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "";
                document.infomes.submit();
            }






        }
        //cuando tque en pdf

        function valiPdf() {


            var fechaIncio = document.forms["infomes"]["fecha_inicio"].value;
            var fechaFin = document.forms["infomes"]["fecha_fin"].value;
            var tipo = document.forms["infomes"]["informe"].value;

            var especialidad = document.forms["infomes"]["especialidad"].value;

            var doctor = document.forms["infomes"]["funcionario"].value;

            if (fechaIncio == "" || fechaFin == "" || tipo == -1) {

                Alert.render("Debe Completar los Datos para Continuar");
                return false;
            } else if (fechaIncio > fechaFin) {

                Alert.render("Rango de Fecha Incorrecto !!Debe Completar los Datos para Continuar");
                return false;
            }

            /*nuevo codigo*/
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }



            if (tipo == 1)
            {



                window.open("<%=ce.getLocal()%>citasPDF?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "");

                // document.infomes.action = "citaPdf.jsp?inicio=" + fechaIncio + "&fin=" + fechaFin + "";
                // document.infomes.submit();
            }

            if (tipo == 2)
            {

                window.open("<%=ce.getLocal()%>hitospacientespdf?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "");

            }
            if (tipo == 3) {

                window.open("<%=ce.getLocal()%>Nominapacientepdf?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "");

            }
            if (tipo == 4) {

                window.open("<%=ce.getLocal()%>citasNSPpdf?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "");

            }

            if (tipo == 5) {
                if (especialidad == -1 || doctor == -1) {

                    Alert.render("Debe Completar los Datos para Continuar");
                    return false;
                } else {

                    window.open("<%=ce.getLocal()%>lista_paciente_doctor?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "");

                }
            }
            if (tipo == 6) {
                if (especialidad == -1 || doctor == -1) {

                    Alert.render("Debe Completar los Datos para Continuar");
                    return false;
                } else {

                    window.open("<%=ce.getLocal()%>PortadaparaFichaArchivoPdf?inicio=" + fechaIncio + "&fin=" + fechaFin + "&especialidad=" + especialidad + "&doctor=" + doctor + "");

                }
            }
            if (tipo == 7) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }
            if (tipo == 8) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }
            if (tipo == 9) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }
            if (tipo == 10) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }
             if (tipo == 11) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }
             if (tipo == 12) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }
             if (tipo == 13) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }
             if (tipo == 14) {

                Alert.render("Solo esta disponible para Excel");
                return false;
            }




        }

    
    
    </script>

    <jsp:include page="../comunes/Footer.jsp"/>
