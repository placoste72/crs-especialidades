<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>
<%@ page import="java.io.FileOutputStream,java.io.*,java.awt.Color,java.util.Vector,java.util.Date,java.text.DateFormat,java.util.Locale" %>
<%@ page import="com.lowagie.text.*,com.lowagie.text.pdf.*,com.lowagie.text.pdf.PdfStamper,com.lowagie.text.pdf.PdfPageEventHelper" %>
<%@ page import="com.lowagie.text.pdf.PdfEncryptor,com.lowagie.text.pdf.PdfReader,com.lowagie.text.pdf.PdfWriter,com.lowagie.text.Chunk" %>
<%@ page import="com.lowagie.text.Document,com.lowagie.text.Element,com.lowagie.text.ExceptionConverter,com.lowagie.text.Font"%>
<%@ page import="com.lowagie.text.Image,com.lowagie.text.PageSize,com.lowagie.text.Rectangle,com.lowagie.text.pdf.BaseFont"%>
<%@ page import="com.lowagie.text.pdf.PdfContentByte,com.lowagie.text.pdf.PdfGState,com.lowagie.text.pdf.PdfPTable" %>
<%@ page import="com.lowagie.text.HeaderFooter,com.lowagie.text.Header,com.lowagie.text.pdf.PdfWriter"%>
<%@ page import="java.sql.*,java.util.GregorianCalendar,java.util.Calendar,java.text.SimpleDateFormat" %>


<%@include file="../comunes/pdfconpieyfoto.jsp" %>
<%    Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
    DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

    SimpleDateFormat formateaDMA = new SimpleDateFormat("dd-MM-yyyy", currentLocale);
    SimpleDateFormat formateaDMY_hhmm = new SimpleDateFormat("dd/MM/yyyy kk:mm", currentLocale);

 

//funciones objeto = new funciones();
    String paciente_nombres = "";
    String paciente_apellidop = "";
    String paciente_apellidom = "";
    int paciente_sexo = 0;
    String paciente_sexo_palabras = "";
    Date paciente_fecha_nac = new Date();
    String paciente_direccion = "";
    String paciente_telefono1 = "";
    String paciente_telefono2 = "";
    Date paciente_fecha_creacion = new Date();
    int paciente_prevision_tipo = 0;
    String paciente_prevision = "";
    String paciente_tramo = "Sin Tramo";
    String servicioSaludProcedencia = "";
    String previsionFull = "";
    String edadFull = "";

    controlador_paciente cp = new controlador_paciente();
    String rut = "";

    if (request.getParameter("rut") != null) {
        rut = request.getParameter("rut");
    }
    paciente paciente = cp.BuscarDetallePaciente(rut);
    paciente_nombres = paciente.getNombre();
    paciente_apellidop = paciente.getApellido_paterno();
    paciente_apellidom = paciente.getApellido_moderno();
    paciente_sexo_palabras = paciente.getTemporal1();

    paciente_fecha_nac = paciente.getFecha_nacimiento();
    paciente_direccion = paciente.getDireccion();
    paciente_telefono1 = paciente.getContacto1();
    paciente_telefono2 = paciente.getContacto2();
    paciente_fecha_creacion = paciente.getFecharegistro();
    paciente_prevision = paciente.getTemporal2();

    paciente_tramo = paciente.getTemporal4();
    servicioSaludProcedencia = paciente.getTemporal6();

    edadFull = paciente.getTemporal7();

    response.setContentType("application/pdf");
    Document document = new Document(PageSize.LETTER, 40, 30, 70, 50);
//ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//PdfWriter writer = PdfWriter.getInstance( document, buffer );
    PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());

    int SPACE_TITULO = 10;
    int SPACE_NORMAL = 8;
    int SPACE_ESPACIO = 10;
    int SPACE_RUT = 20;
    int SPACE_RUTVERTICAL = 50;

    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(255, 255, 255));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(0, 0, 3));
    Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
    Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 15, Font.UNDERLINE | Font.BOLD, new Color(68, 117, 196));
    Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 15, Font.BOLD, new Color(0, 0, 3));
    Font TEXT_SUPERTITULONORMALRUT = FontFactory.getFont(FontFactory.HELVETICA, 30, Font.BOLD, new Color(0, 0, 3));
    Font TEXT_SUPERTITULONORMALRUTVERTICAL = FontFactory.getFont(FontFactory.HELVETICA, 60, Font.BOLD, new Color(0, 0, 3));
    Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.ITALIC, new Color(0, 0, 3));

    writer.setPageEvent(new PageNumbersWatermark());
    document.open();

    Table tabla_titulo;
    Cell celda;

    tabla_titulo = new Table(3);
    tabla_titulo.setBorderWidth(0);
    tabla_titulo.setPadding(2);
    tabla_titulo.setSpacing(0);
    tabla_titulo.setWidth(100);

    Paragraph rutPaciente = new Paragraph();
    rutPaciente.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
    rutPaciente.add(new Phrase(SPACE_RUT, rut.toUpperCase(), TEXT_SUPERTITULONORMALRUT));
    celda = new Cell(rutPaciente);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
//celda = new Cell(new Phrase(new Chunk(image39ext, 0, 0)));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "DATOS DEL PACIENTE", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);
    celda = new Cell("\n");
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, paciente_nombres.toUpperCase() + " " + paciente_apellidop.toUpperCase() + " " + paciente_apellidom.toUpperCase(), TEXT_SUPERTITULONORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "Sexo: ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, paciente_sexo_palabras, TEXT_SUPERTITULONORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "Fecha de Nacimiento: ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, formateadorFechaSimple.format(paciente_fecha_nac), TEXT_SUPERTITULONORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
//celda = new Cell(new Phrase(SPACE_TITULO,String.valueOf(objeto.calcula_edad(paciente_fecha_nac)),TEXT_SUPERTITULONORMAL));
   
//celda = new Cell(new Phrase(SPACE_TITULO,String.valueOf(edadFull.indexOf(":")),TEXT_SUPERTITULONORMAL));
    celda = new Cell(new Phrase(SPACE_TITULO, edadFull, TEXT_SUPERTITULONORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, " ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "Previsi�n: ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, paciente_prevision, TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "Fecha de Creaci�n: ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, formateadorFecha.format(paciente_fecha_creacion), TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "Lugar de Creaci�n: ", TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, servicioSaludProcedencia.toUpperCase(), TEXT_NORMAL));
    celda.setBorderWidth(0);
    celda.setColspan(2);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
//celda = new Cell(new Phrase(SPACE_NORMAL,"Fecha de Impresion: "+formateadorFecha.format(new Date()).toUpperCase()+" a las "+formateaHora.format(new Date())+" Hrs.",TEXT_NORMAL)); 
    celda = new Cell(new Phrase(SPACE_NORMAL, "", TEXT_NORMAL));

    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    document.add(tabla_titulo);

    int NumColumns0 = 6;
    PdfPTable datatable0 = new PdfPTable(NumColumns0);
    int headerwidths0[] = {5, 15, 15, 35, 15, 15};
    datatable0.setWidths(headerwidths0);
    datatable0.setWidthPercentage(100);
    datatable0.getDefaultCell().setPadding(3);
    datatable0.getDefaultCell().setBorderWidth(1);
    datatable0.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
    datatable0.getDefaultCell().setColspan(6);
    datatable0.getDefaultCell().setBackgroundColor(new Color(68, 117, 196));
    datatable0.addCell(new Phrase(SPACE_ESPACIO, "OTROS FONOS Y DIRECCIONES", TEXT_TITULO));
    datatable0.getDefaultCell().setColspan(1);
    datatable0.addCell(new Phrase(SPACE_ESPACIO, "#", TEXT_TITULO));
    datatable0.addCell(new Phrase(SPACE_ESPACIO, "FONO1", TEXT_TITULO));
    datatable0.addCell(new Phrase(SPACE_ESPACIO, "FONO2", TEXT_TITULO));
    datatable0.addCell(new Phrase(SPACE_ESPACIO, "DIRECCION", TEXT_TITULO));
    datatable0.addCell(new Phrase(SPACE_ESPACIO, "COMUNA", TEXT_TITULO));
    datatable0.addCell(new Phrase(SPACE_ESPACIO, "FECHA INGRESO", TEXT_TITULO));

    datatable0.setHeaderRows(2);
    datatable0.getDefaultCell().setBorderWidth(1);
    datatable0.getDefaultCell().setBackgroundColor(Color.WHITE);

    int contador = 0;
    for (paciente temp : cp.buscarHistorialPaciente(rut)) {
        {
            contador++;
            datatable0.addCell(new Phrase(SPACE_NORMAL, String.valueOf(contador), TEXT_NORMAL));
            datatable0.addCell(new Phrase(SPACE_NORMAL, temp.getContacto1(), TEXT_NORMAL));
            datatable0.addCell(new Phrase(SPACE_NORMAL, temp.getContacto2(), TEXT_NORMAL));
            datatable0.addCell(new Phrase(SPACE_NORMAL, temp.getDireccion(), TEXT_NORMAL));
            datatable0.addCell(new Phrase(SPACE_NORMAL, temp.getNombre(), TEXT_NORMAL));
            datatable0.addCell(new Phrase(SPACE_NORMAL, temp.getTemporal3(), TEXT_NORMAL));
        }
       
    }
     if (contador == 0) {
            datatable0.getDefaultCell().setColspan(6);
            datatable0.addCell(new Phrase(SPACE_NORMAL, "NO HAY OTROS FONOS O DIRECCIONES PARA ESTE PACIENTE", TEXT_NORMAL));
            datatable0.getDefaultCell().setColspan(1);
        }

    document.add(datatable0);

    int NumColumns2 = 5;
    PdfPTable datatable2 = new PdfPTable(NumColumns2);
    int headerwidths2[] = {4, 15, 51, 20, 10};
    datatable2.setWidths(headerwidths2);
    datatable2.setWidthPercentage(100);
    datatable2.getDefaultCell().setPadding(3);
    datatable2.getDefaultCell().setBorderWidth(1);
    datatable2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
    datatable2.getDefaultCell().setColspan(5);
    datatable2.getDefaultCell().setBackgroundColor(new Color(68, 117, 196));
    datatable2.addCell(new Phrase(SPACE_ESPACIO, "HITOS DEL PACIENTE", TEXT_TITULO));
    datatable2.getDefaultCell().setColspan(1);
    datatable2.addCell(new Phrase(SPACE_ESPACIO, "#", TEXT_TITULO));
    datatable2.addCell(new Phrase(SPACE_ESPACIO, "FECHA", TEXT_TITULO));
    datatable2.addCell(new Phrase(SPACE_ESPACIO, "DETALLE", TEXT_TITULO));
    datatable2.addCell(new Phrase(SPACE_ESPACIO, "USUARIO", TEXT_TITULO));
    datatable2.addCell(new Phrase(SPACE_ESPACIO, "ID", TEXT_TITULO));

    datatable2.setHeaderRows(2);
    datatable2.getDefaultCell().setBorderWidth(1);
    datatable2.getDefaultCell().setBackgroundColor(Color.WHITE);

    contador = 0;
    for (paciente temp : cp.buscarHitosdePaciente(rut)) {
        {
            contador++;
            datatable2.addCell(new Phrase(SPACE_NORMAL, String.valueOf(contador), TEXT_NORMAL));
            datatable2.addCell(new Phrase(SPACE_NORMAL, temp.getTemporal3(), TEXT_NORMAL));
            datatable2.addCell(new Phrase(SPACE_NORMAL, temp.getNombre(), TEXT_NORMAL));
            datatable2.addCell(new Phrase(SPACE_NORMAL, temp.getTemporal1(), TEXT_NORMAL));
            datatable2.addCell(new Phrase(SPACE_NORMAL, temp.getTemporal2(), TEXT_NORMAL));
        }
       
    }
     if (contador == 0) {
            datatable2.getDefaultCell().setColspan(5);
            datatable2.addCell(new Phrase(SPACE_NORMAL, "NO HAY HITOS REGISTRADOS PARA ESTE PACIENTE", TEXT_NORMAL));
            datatable2.getDefaultCell().setColspan(1);
        }
    document.add(datatable2);

    int NumColumns = 9;
    PdfPTable datatable = new PdfPTable(NumColumns);
    int headerwidths[] = {4, 10, 11, 11, 11, 11, 11, 11, 10};
    datatable.setWidths(headerwidths);
    datatable.setWidthPercentage(100);
    datatable.getDefaultCell().setPadding(3);
    datatable.getDefaultCell().setBorderWidth(1);
    datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
    datatable.getDefaultCell().setColspan(9);
    datatable.getDefaultCell().setBackgroundColor(new Color(68, 117, 196));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "ATENCIONES ESPECIALIDADES MEDICAS", TEXT_TITULO));
    datatable.getDefaultCell().setColspan(1);
    datatable.addCell(new Phrase(SPACE_ESPACIO, "#", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "ESPECIALIDAD", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "PROFESIONAL", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "FECHA ATENCION", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "HORA CITA", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "TIPO ATENCION", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "ESTADO ATENCION", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "MOTIVO ATENCION", TEXT_TITULO));
    datatable.addCell(new Phrase(SPACE_ESPACIO, "PROGRAMA ASISTENCIAL", TEXT_TITULO));

    datatable.setHeaderRows(2);
    datatable.getDefaultCell().setBorderWidth(1);
    datatable.getDefaultCell().setBackgroundColor(Color.WHITE);
     controlador_cita cc = new controlador_cita();
     int  contador2 = 0;
        for(cita cit : cc.buscarCitasdeunPaciente(rut)) {
            contador2++;
            datatable.addCell(new Phrase(SPACE_NORMAL, String.valueOf(contador), TEXT_NORMAL));
            datatable.addCell(new Phrase(SPACE_NORMAL, cit.getTemporales(), TEXT_NORMAL));
            datatable.addCell(new Phrase(SPACE_NORMAL, cit.getRut_doctor(), TEXT_NORMAL));
            datatable.addCell(new Phrase(SPACE_NORMAL, formateaDMA.format(cit.getFecha()), TEXT_NORMAL));
            datatable.addCell(new Phrase(SPACE_NORMAL, cit.getTemporales4(), TEXT_NORMAL));
            String horaAtencion = "";
            

            datatable.addCell(new Phrase(SPACE_NORMAL, cit.getTemporales1(), TEXT_NORMAL));
            datatable.addCell(new Phrase(SPACE_NORMAL, cit.getTemporales2(), TEXT_NORMAL));
            datatable.addCell(new Phrase(SPACE_NORMAL, cit.getRut_paciente(), TEXT_NORMAL));
            datatable.addCell(new Phrase(SPACE_NORMAL, cit.getTemporales3(), TEXT_NORMAL));
            
       
        }
         if (contador2 == 0) {
            datatable.getDefaultCell().setColspan(10);
            datatable.addCell(new Phrase(SPACE_NORMAL, "NO HAY ATENCIONES EN ESPECIALIDADES MEDICAS REGISTRADAS PARA ESTE PACIENTE", TEXT_NORMAL));
            datatable.getDefaultCell().setColspan(1);
        }

    document.add(datatable);


    document.close();

   
%>

