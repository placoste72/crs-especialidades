<%-- 
    Document   : ActualizarRol
    Created on : 30-11-2016, 01:01:42 PM
    Author     : Informatica
--%>

<%@page import="Modelos.opciones"%>
<%@page import="Modelos.rol"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controlador.controlador_rol"%>
<jsp:include page="../comunes/headerwindows.jsp"/> 
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <title>Actualizar Diagnostico con Especialidades</title>

    </head>


    <body>
        <script>


            function asignar() {
                var x = document.forms["rdiagnostico"]["nombre"].value;
                if (x == null || x == "") {
                    alert("Debe ingresar un Diagnostico");
                }
            }

            function pone() {

                if (!isSelected("#fromSelectBox")) {
                    return;
                }
                //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
                $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
                return false;

            }
            function saca() {

                //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
                if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
                    return;
                }
                //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
                $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

            }

            function todos() {
                selectAll('#fromSelectBox');
                pone();
            }

            function ninguno() {
                selectAll('#toSelectBox');
                saca();
            }

            function selecciona_todos() {

                selectAll('toSelectBox');
            }

            //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
            function isSelected(thisObj) {
                if (!$(thisObj + " option:selected").length) {
                    Alert.render("Debe Completar los Datos para Continuar");
                    return 0;
                }
                return 1;
            }

            //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
            function noOptions(thisObj) {
                if (!$(thisObj + " option").length) {
                    // alert("There are no options to select/move");
                    return 0;
                }
                return 1;
            }

            //Below function is to de-select all items if any of the item(s) are selected
            function clearAll(thisObj) {
                $('#' + thisObj).each(function () {
                    $(this).find('option:selected').removeAttr("selected");
                });
            }//function close

//Below function is to select all items
            function selectAll(thisObj) {
                obj = document.getElementById(thisObj);
                for (var i = 0; i < obj.options.length; i++) {
                    obj.options[i].selected = true;
                }
            }

        </script>




        <%

            int id = Integer.parseInt(request.getParameter("cod"));
            controlador_rol cr = new controlador_rol();

            rol r = new rol();

            r = cr.buscarDiagnosticoporid(id);
            ArrayList lista_opciones = cr.buscarListaespecialidadessinDiagnostico(id);
            Iterator it = lista_opciones.iterator();

            ArrayList lista_vacia = cr.buscarListaEspecialidadesconDiagnostico(id);
            Iterator it2 = lista_vacia.iterator();
            String[] ges = {"Ges", "Si", "No"};
            String mensaje = "";
            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Actualizar Rol">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>


        <%
            }

        %>

        <form  style="border: #619fd8 5px solid;" id="rol" nombre="rol" class="form-horizontal" action='<%=cr.getLocal()%>ingresar_diagnostico?tipo=2' method="POST" onsubmit="return selecciona_todos()">
            <fieldset>
                <div id="dialogoverlay"></div>
                <div id="dialogbox">
                    <div>
                        <div id="dialogboxhead"></div>
                        <div id="dialogboxbody"></div>
                        <div id="dialogboxfoot"></div>
                    </div>
                </div>
                <legend class="text-center header">Actualizar Diagnostico </legend>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-1 "><i class="fa fa-pencil bigicon"></i></span>
                    <div class="col-md-4 col-md-4" >
                        <input  type="text" id="nombre" name="cod"  value="<%=r.getIdRol()%>" hidden  >

                        <input  type="text" id="nombre" name="nombre"  value="<%=r.getNombreRol()%>" class="form-control" placeholder="Nombre Perfil" required oninvalid="setCustomValidity('El campo nombre perfil es obligatorio')" oninput="setCustomValidity('')"  >


                    </div>
                    <span class="col-md-1 col-md-offset-1 "><i class="fa fa-list bigicon"></i></span>
                    <div class="col-md-2">
                        <select id="ges"   name="ges" >
                            <% for (int i = 0; i < ges.length; ++i) {
                                    if (i == r.getEstatus()) {
                            %>
                            <option selected value="<%=i%>"> <%=ges[i]%> </option>
                            <%} else {%>

                            <option value="<%=i%>"> <%=ges[i]%> </option>

                            <%}
                                }%>
                        </select>  
                    </div>




                </div>




             


                <div  style=" padding-left: 150px">

                    <h4 class="letra2" style="padding-right: 25px;"> Eliga Las Especialidades para el Diagnostico </h4>
                    <div class="col-md-8 text-center">
                        <table class="table">
                            <tr>
                                <td>
                                    <select  id="fromSelectBox" name="opciones"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px;  width: 260px ">

                                        <%

                                            while (it.hasNext()) {
                                                opciones o = (opciones) it.next();
                                                out.write("   <option value='" + o.getIdopcion() + "' >" + o.getNombre() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>
                                <td style="width:5px">
                                    <span class='cell' >
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon " onclick="pone();

                                                return false"></i>
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca();

                                                return false"></i>

                                    </span>

                                </td>       
                                <td>
                                    <select  id="toSelectBox" name="opciones2"  size="10" multiple="multiple"  style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px ">

                                        <%
                                            while (it2.hasNext()) {
                                                opciones o2 = (opciones) it2.next();
                                                out.write("   <option value='" + o2.getIdopcion() + "' >" + o2.getNombre() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>    

                            </tr> 

                        </table>








                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Actualizar</button>

                        <button type="reset" class="btn btn-primary btn-lg" onclick="javascript: window.close();">Cancelar</button>
                    </div>
                </div>

            </fieldset>
        </form> 


    </body>

</html>
