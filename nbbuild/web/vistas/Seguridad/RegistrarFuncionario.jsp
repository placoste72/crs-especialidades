<%-- 
    Document   : RegistrarFuncionario
    Created on : 27-09-2017, 14:32:38
    Author     : a
--%>

<%@page import="Controlador.General"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<script src="../../public/js/registrarfuncionario.js" type="text/javascript"></script>
<script>
   
     $(document).ready(function () {
        $("#rutpaciento").on('paste', function (e) {
            e.preventDefault();
            Alert.render('Esta acción está prohibida');
        })

        $("#rutpaciento").on('copy', function (e) {
            e.preventDefault();
            Alert.render('Esta acción está prohibida');
        })
    })


</script> 
<%
    General g = new General();
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Registrar Funcionario">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>


<%
    }
%>
<div class="tab-content" style="text-align:center;">
    <div id="crear" class="tab-pane fade in active" style="text-align:center;">



        <form name="funcionario" class="form" style="border: #619fd8 2px solid; margin: 50px; margin-bottom: 20px" id="paciente" action='<%=g.getLocal()%>registrarfuncionario' method="post" onsubmit="return validar()" >

            <fieldset>
                <legend class="text-center header">Registrar Funcionario</legend>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-credit-card bigicon"></i></span>
                    <div class="col-md-6">

                        <input type="text" id="rutpaciento" name="rutpaciento" style="text-transform:uppercase;" onmouseover="showToolTip(event, '¡Escriba su RUT sin puntos ni guiones!');
                                return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                        <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                        <input value="d" name="txtDV" id="txtDV" type="hidden">
                    </div>

                </div>
                <div>
                    <br>
                    <br>
                </div>



                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                    <div class="col-md-6">
                        <input type="text" id="nombrepaciente" name="nombrepaciente"   class="form-control" onkeypress="return validarsololetra(event)" placeholder="Nombre "  required oninvalid="setCustomValidity('El campo Nombre es obligatorio')" oninput="setCustomValidity('')">
                    </div>
                </div>
                <div>
                    <br>
                    <br>
                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                    <div class="col-md-6">
                        <input type="text" id="apellidoppaciente" name="apellidoppaciente"   class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Paterno"  required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">
                    </div>
                </div>
                <div>
                    <br>
                    <br>
                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                    <div class="col-md-6">
                        <input type="text" id="apellidompaciente" name="apellidompaciente"  class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Materno" required oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
                    </div>
                </div>
                <div>
                    <br>
                    <br>
                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span>
                    <div class="col-md-6">

                        <input type="tel" id="telefonoPaciente"  name="telefonoPaciente" class="form-control" placeholder="+56 9 1111 1111" maxlength="11">
                    </div>
                </div>
                <div>
                    <br>
                    <br>
                </div>

                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Registrar</button>

                        <button type="reset" class="btn btn-primary btn-lg" >Cancelar</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>

<jsp:include page="../comunes/Footer.jsp"/>