<%-- 
    Document   : monitoreoProfesional
    Created on : 26-12-2018, 11:44:23
    Author     : a
--%>

<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.cita"%>
<%@page import="Modelos.doctor"%>
<%@page import="java.util.Vector"%>
<%@page import="Controlador.General"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
        <style>
            * {box-sizing: border-box}
            body {font-family: "Lato", sans-serif;}

            /* Style the tab */
            .tab {
                float: left;
                border: 1px solid #ccc;
                background-color: #f1f1f1;
                width: 30%;
                height: 300px;
            }

            /* Style the buttons inside the tab */
            .tab button {
                display: block;
                background-color: inherit;
                color: black;
                padding: 22px 16px;
                width: 100%;
                border: none;
                outline: none;
                text-align: left;
                cursor: pointer;
                font-size: 17px;
            }

            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #ddd;
            }

            /* Create an active/current "tab button" class */
            .tab button.active {
                background-color: #ccc;
            }

            /* Style the tab content */
            .tabcontent {
                float: left;
                padding: 0px 12px;
                border: 1px solid #ccc;
                width: 70%;
                border-left: none;
                height: 300px;
                display: none;
            }

            /* Clear floats after the tab */
            .clearfix::after {
                content: "";
                clear: both;
                display: table;
            }
        </style>
        

    <%
        General g = new General();
        controlador_cita cc = new controlador_cita();

        int k = 1;
        Vector<doctor> doc = g.BuscarProfesionalesqueinicaronsesion();

    %>
<fieldset>
    <legend class="text-center header">Lista de profesionales con sesion</legend>

        <div class="tab">
            <%for (int i = 0; i < doc.size(); ++i) {%>
            <button class="tablinks" onmouseover="openCity(event, '<%=doc.get(i).getRut()%>')"><%=doc.get(i).getNombre()%></button>
            <%}%>
        </div>
        <%for (int i = 0; i < doc.size(); ++i) {%>
        <div id="<%=doc.get(i).getRut()%>" class="tabcontent">
            <h3><%=doc.get(i).getNombre()%></h3>
            <p>Hora inicio sesion: <%=doc.get(i).getNombrefirmadoctor()%></p>
            <legend class="text-center header">Lista de Paciente Para Hoy</legend>
            <h3>Fecha: <%=cc.fechadeldia()%></h3>
            <table class="table table-striped" style=" width: 100%">
                <thead>
                    <tr>
                        <th> N° </th>
                        <th> Rut</th>
                        <th>Nombre</th>
                        <th>Tipo Atención</th>

                        <th>Hora Citado</th>
                        <th>Hora Recepcionado</th>
                        <th>Estatus</th>
                    </tr>
                </thead>
                <%for (cita c : cc.buscarCitasdeDoctorParaHoy(doc.get(i).getRut())) {%>
                <tr>
                    <td><%=k%></td>
                    <td><%=c.getRut_paciente()%></td> 
                    <td><%=c.getTemporales()%></td>
                    <td><%=c.getTemporales1()%></td>

                    <td><%=c.getTemporales3()%></td>
                    <td><%=c.getTemporales5()%></td>

                    <%if (c.getEstatus() == 2) {%>
                    <td style=" background-color: #ff0000"><%=c.getTemporales4()%></td>
                    <%} else if (c.getEstatus() == 4) {%>
                    <td style=" background-color: #33cc33" ><%=c.getTemporales4()%></td>
                    <%} else if (c.getEstatus() == 1) {%>
                    <td style=" background-color: #ffff00" ><%=c.getTemporales4()%></td>
                    <%} else if (c.getEstatus() == 3 || c.getEstatus() == 6) {%>
                    <td style=" background-color: #0000ff" ><%=c.getTemporales4()%></td>
                    <%} else {%>
                    <td style=" background-color: #ff3300" ><%=c.getTemporales4()%></td>
                    <%}%>
                </tr>
                <%++k;
            }%>

            </table>
        </div>
        <%}%>


        <div class="clearfix"></div>
       </fieldset>

    <script>
        function openCity(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the link that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
    <jsp:include page="../comunes/Footer.jsp"/>