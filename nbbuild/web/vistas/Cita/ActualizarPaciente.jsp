<%-- 
    Document   : ActualizarPaciente
    Created on : 09-12-2016, 03:59:34 PM
    Author     : Informatica
--%>

<%@page import="Modelos.prevision"%>
<%@page import="Modelos.lugar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>

<jsp:include page="../comunes/headerwindows.jsp"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Actualizar Paciente</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    </head>

    <body>

        <%           
            String mensaje = "";

            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Actualizar Paciente">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>

        <%
            }
            controlador_paciente cp = new controlador_paciente();
            String rut = request.getParameter("cod");
            paciente p = cp.buscarpacienteporrut(rut);
            DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
            String fecha = formatoFecha.format(p.getFecha_nacimiento());


        %>
        <script>
            $(function () {
                $("#txt_inicial").datepicker();
            });

        </script>
        <div class="container"> 


            <div id="crear" class="tab-pane fade in active">



                <form style="border: #619fd8 5px solid;" name="paciente" class="form-horizontal" id="paciente" action='<%=cp.getLocal()%>actualizar_paciente' method="post" onsubmit="return validar()" >

                    <fieldset>
                        <legend class="text-center header">Actualizar Paciente</legend>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-credit-card bigicon"></i></span>
                            <div class="col-md-8">

                                <input type="text" value="<%=p.getRut()%>" readonly="readonly"  id="rutpaciente" name="rutpaciento" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                        return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                                <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                                <input value="d" name="txtDV" id="txtDV" type="hidden">
                            </div>
                        </div>
                        <div id="busca">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                <div class="col-md-8">
                                    <input readonly type="text" id="nombrepaciente" name="nombrepaciente" value="<%=p.getNombre()%>" class="form-control" placeholder="Nombre Paciente" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                <div class="col-md-8">
                                    <input readonly type="text" id="apellidoppaciente" value="<%=p.getApellido_paterno()%>" name="apellidoppaciente" class="form-control" placeholder="Apellido Paterno Paciente" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                <div class="col-md-8">
                                    <input  readonly type="text" id="apellidomdoctor" value="<%=p.getApellido_moderno()%>" name="apellidompaciente" class="form-control" placeholder="Apellido Materno Paciente" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
                                </div>
                            </div>
                            <div class="form-group" >
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                                <div class="col-md-8" >
                                    <select class="form-control" id="genero" name="genero" >
                                        <option value="0">Genero
                                            <% if (p.getGenero() == 1) {
                                            %>
                                        <option selected value="<%=p.getGenero()%>">Femenino
                                        <option value="2"> Masculino<%
                                        } else if (p.getGenero() == 2) {%>
                                        <option selected value="<%=p.getGenero()%>">Masculino
                                        <option  value="1">Femenino<%
                                            }%>   
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar-o bigicon"></i></span>
                                <div class="col-md-8">
                                    <input type="text" name="txt_inicial" id="txt_inicial" value="<%=fecha%>" placeholder="Fecha de Nacimiento" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span>
                                <div class="col-md-8">
                                    <input type="tel" id="telefonoPaciente" name="telefonoPaciente" value="<%=p.getContacto1()%>" class="form-control" placeholder="+56 9 1111 1111"  required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                <div class="col-md-8">
                                    <input type="tel" id="telefonoPaciente1" name="telefonoPaciente1" value="<%=p.getContacto2()%>" class="form-control" placeholder="+56 9 2222 2222">
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
                                <div class="col-md-8">
                                    <input type="email" id="emaildoctor" name="emaildoctor" value="<%=p.getEmail()%>" class="form-control" placeholder="Email Doctor" required oninvalid="setCustomValidity('El campo email es obligatorio')" oninput="setCustomValidity('')">
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon"></i></span>
                                <div class="col-md-8">
                                    <input type="texarea" value="<%=p.getDireccion()%>"    rows="3" id="direccion" name="direccion"  class="form-control" >


                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                            <div class="col-md-8" >
                                <select class="form-control" id="comuna" name="comuna">
                                    <option value="0">Elegir Comuna
                                        <%

                                            for (lugar temp : cp.buscarTodasLasComuna()) {

                                                if (temp.getId_lugar() == p.getId_comuna()) {%>
                                    <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                    } else {%>
                                    <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                            }

                                        }
                                        %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                            <div class="col-md-8" >
                                <select class="form-control" id="procedencia" name="procedencia" required>
                                    <option value="0">Elegir Consultorio de donde procede
                                        <%
                                            try {
                                                String procedencia = " SELECT id_servicio_salud, nombre_servicio_salud FROM  agenda.procedencia where estado_servicio_salud = 1 ; ";

                                                ResultSet rs_procedencia = st.executeQuery(procedencia);

                                                while (rs_procedencia.next()) {

                                                    if (rs_procedencia.getInt("id_servicio_salud") == p.getProcedencia()) {%>
                                    <option selected value="<%=rs_procedencia.getInt("id_servicio_salud")%>"><%=rs_procedencia.getString("nombre_servicio_salud")%><%
                                    } else {%>
                                    <option value="<%=rs_procedencia.getInt("id_servicio_salud")%>"><%=rs_procedencia.getString("nombre_servicio_salud")%><%
                                                }

                                            }
                                        } catch (SQLException ex) {
                                        }        %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                            <div class="col-md-8" >
                                <select class="form-control" id="provision" name="provision"  >
                                    <option value="0">Prevision


                                        <%
                                            for (prevision pre : cp.buscarPrevicion()) {
                                                if (p.getProvision() == pre.getId_prevision()) {

                                        %>


                                    <option selected value="<%=pre.getId_prevision()%>" ><%=pre.getNombre()%>
                                        <%
                                        } else {%>
                                    <option  value="<%=pre.getId_prevision()%>" ><%=pre.getNombre()%>

                                        <%

                                                }
                                            }%>         
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="tramo" >
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                            <div class="col-md-8" >
                                <select class="form-control" id="tramo" name="tramo" >
                                    <option value="0">Tramo
                                        <%
                                            for (prevision tramo : cp.buscarTramo()) {

                                                if (tramo.getId_prevision() == p.getTramo()) {%>
                                    <option selected value="<%=tramo.getId_prevision()%>"><%=tramo.getNombre()%><%
                                    } else {%>
                                    <option value="<%=tramo.getId_prevision()%>"><%=tramo.getNombre()%><%
                                            }

                                        }
                                        %>
                                </select>
                            </div>
                        </div>        

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Actualizar</button>

                                <button type="reset" class="btn btn-primary btn-lg" onclick="javascript: window.close();">Cancelar</button>
                            </div>
                        </div>


                    </fieldset>

                </form>


            </div>

        </div>
    </body>
</html>
