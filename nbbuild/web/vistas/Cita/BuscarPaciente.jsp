<%-- 
    Document   : BuscarPaciente
    Created on : 26-01-2017, 03:50:23 PM
    Author     : Informatica
--%>


<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<script>
    function formateaRut(Rut)
    {
        var sRut = new String(Rut);
        var sRutFormateado = '';
        sRut = quitaFormato(sRut);
        var sDV = sRut.charAt(sRut.length - 1);
        sRut = sRut.substring(0, sRut.length - 1);
        document.getElementById('txtRutSinDV').value = sRut;
        document.getElementById('txtDV').value = sDV;
        //document.forms[0].txtRutSinDV.value = sRut;
        //document.forms[0].txtDV.value = sDV;

        while (sRut.length > 3)
        {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
        }
        sRutFormateado = sRut + sRutFormateado;
        if (sRutFormateado != "")
            sRutFormateado += "-";
        sRutFormateado += sDV;
        if (document.getElementById('rutpaciente').value != sRutFormateado)
            document.getElementById('rutpaciente').value = sRutFormateado;
    }
    function quitaFormato(Nro)
    {
        var strNro = new String(Nro);
        while (strNro.indexOf(".") != - 1)
            strNro = strNro.replace(".", "");
        strNro = strNro.replace("-", "");
        return strNro;
    }
    
     function DigitoVerificadorRut(strRut) {
        var rut = 0;
        var s = 0;
        var l_dv = "";

        rut = strRut;
        for (i = 2; i < 8; i++) {
            s = s + (rut % 10) * i;
            rut = (rut - (rut % 10)) / 10;
        }
        s = s + (rut % 10) * 2;
        rut = (rut - (rut % 10)) / 10;
        s = s + (rut % 10) * 3;
        rut = (rut - (rut % 10)) / 10;
        s = 11 - (s % 11);
        if (s == 10)
            l_dv = "K";
        else
        if (s == 11)
            l_dv = "0"
        else
            l_dv = s + "";
        return(l_dv);
    }
    
    function validaRut() {
        var sRut = new String(document.getElementById('rutpaciente').value);
        sRut = quitaFormato(sRut);
        var sDV = new String(sRut.charAt(sRut.length - 1));
        sRut = sRut.substring(0, sRut.length - 1);
        if (sDV.toUpperCase() == DigitoVerificadorRut(sRut))
        {
            return true;
        }
        if (sDV.toUpperCase() != DigitoVerificadorRut(sRut))
        {
            return false;
        }
    }
   
    function buscaPaciente() {
         var paciente = document.forms["buscarpaciente"]["rutpaciente"].value;
        
        if (paciente == null || paciente == "" || paciente.length == 0 )
        {
            Alert.render("Debe Completar los Datos para Continuar");
            document.getElementById('rutpaciente').focus();
        } else
        {
            if (validaRut(document.getElementById('rutpaciente').value))
            {
                var paciente = document.getElementById('rutpaciente').value;

                try
                {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e)
                {
                    try
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (E) {
                        xmlhttp = false;
                    }
                }
                if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                    xmlhttp = new XMLHttpRequest();
                }

                if (xmlhttp) {
                    var objeto_recibidor = document.getElementById("busqueda");
                    xmlhttp.open("post", "Buscar.jsp?paciente=" + paciente);
                    xmlhttp.send("");
                    if (xmlhttp.readyState == 1) {
                        objeto_recibidor.innerHTML = '</br></br><b>Favor espere... </b>';

                    }
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            objeto_recibidor.innerHTML = xmlhttp.responseText;

                            document.getElementById("busqueda").value = null;
                        }
                        if (xmlhttp.status != 200) {
                            objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                        }
                    }
                }
            } else {
                Alert.render("Rut Invalida!!Debe Completar los Datos para Continuar");
                return false;
            }
        }

    }

    $('body').keyup(function (e) {
        if (e.keyCode == 13) {

            return buscaPaciente();
        }


    });
   

</script>
<body>
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <div class="container"> 

        <form  id="buscarpaciente" name="buscarpaciente"  method="post" onsubmit="return false;"  >
            <fieldset>
                <legend class="text-center header">Buscar Paciente</legend>
                <div class="form-group">

                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                    <div class="col-md-4">

                        <input type="text" id="rutpaciente" name="rutpaciente" style="text-transform:uppercase; cursor:pointer;" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                return false" onkeyup="formateaRut(this.value);" autocomplete=off onclick="javascript:document.getElementById('rutpaciente').select();" maxlength="12" class="form-control" placeholder="Rut Paciente">
                        <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                        <input value="d" name="txtDV" id="txtDV" type="hidden">

                    </div>

                    <button type="button" class="btn  btn-primary " onclick="javascript : buscaPaciente()">Buscar</button>
                </div>
                <div id="busqueda">            

                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </fieldset>
        </form>
    </div>

</body>
<jsp:include page="../comunes/Footer.jsp"/>