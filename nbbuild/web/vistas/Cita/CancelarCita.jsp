<%-- 
    Document   : ConfirmarCita
    Created on : 15-12-2016, 04:55:08 PM
    Author     : Informatica
--%>


<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp" />
<title>Cancelar Cita Paciente</title>
<script>
    function formateaRut(Rut)
    {
        var sRut = new String(Rut);
        var sRutFormateado = '';
        sRut = quitaFormato(sRut);
        var sDV = sRut.charAt(sRut.length - 1);
        sRut = sRut.substring(0, sRut.length - 1);
        document.forms["cancelarcita"].txtRutSinDV.value = sRut;
        document.forms["cancelarcita"].txtDV.value = sDV;
        while (sRut.length > 3)
        {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
        }
        sRutFormateado = sRut + sRutFormateado;
        if (sRutFormateado != "")
            sRutFormateado += "-";
        sRutFormateado += sDV;
        if (document.forms["cancelarcita"].rutpaciente.value != sRutFormateado)
            document.forms["cancelarcita"].rutpaciente.value = sRutFormateado;
    }
    function quitaFormato(Nro)
    {
        var strNro = new String(Nro);
        while (strNro.indexOf(".") != - 1)
            strNro = strNro.replace(".", "");
        strNro = strNro.replace("-", "");
        return strNro;
    }
    function DigitoVerificadorRut(strRut) {
        var rut = 0;
        var s = 0;
        var l_dv = "";

        rut = strRut;
        for (i = 2; i < 8; i++) {
            s = s + (rut % 10) * i;
            rut = (rut - (rut % 10)) / 10;
        }
        s = s + (rut % 10) * 2;
        rut = (rut - (rut % 10)) / 10;
        s = s + (rut % 10) * 3;
        rut = (rut - (rut % 10)) / 10;
        s = 11 - (s % 11);
        if (s == 10)
            l_dv = "K";
        else
        if (s == 11)
            l_dv = "0"
        else
            l_dv = s + "";
        return(l_dv);
    }


    function validaRut() {
        var sRut = new String(document.forms["cancelarcita"]["rutpaciente"].value);
        sRut = quitaFormato(sRut);
        var sDV = new String(sRut.charAt(sRut.length - 1));
        sRut = sRut.substring(0, sRut.length - 1);
        if (sDV.toUpperCase() == DigitoVerificadorRut(sRut))
        {
            return true;
        }
        if (sDV.toUpperCase() != DigitoVerificadorRut(sRut))
        {
            return false;
        }
    }

   
    function  buscaPaciente()
    {

        var paciente = document.forms["cancelarcita"]["rutpaciente"].value;


        if (paciente == null || paciente == "" || paciente.length == 0 || paciente.length < 9)
        {
            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        } else
        {
            if (validaRut(document.forms["cancelarcita"]["rutpaciente"].value))
            {
                entre = true;
                try
                {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e)
                {
                    try
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (E) {
                        xmlhttp = false;
                    }
                }
                if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                    xmlhttp = new XMLHttpRequest();
                }

                if (xmlhttp) {
                    var objeto_recibidor = document.getElementById("buscar");
                    xmlhttp.open("post", "DatosPacienteCancelar.jsp?paciente=" + paciente);
                    xmlhttp.send("");
                    if (xmlhttp.readyState == 1) {
                        objeto_recibidor.innerHTML = '<b>Favor espere... </b>';

                    }
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            objeto_recibidor.innerHTML = xmlhttp.responseText;

                        }
                        if (xmlhttp.status != 200) {
                            objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                        }
                    }
                }

            } else {
                Alert.render("Rut Invalida");
            }
        }
    }

    $('body').keyup(function (e) {
        if (e.keyCode == 13) {

            return buscaPaciente();
        }


    });

</script>  

<%
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Cancelar Cita">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>


<%
    }
%> 


<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>
<div class="container"> 

    <div class="tab-content">
        <div id="crear" class="tab-pane fade in active">


            <form name="cancelarcita" class="form-horizontal" id="cancelarcita"  method="post" onsubmit="return false;"  >

                <fieldset>
                    <legend class="text-center header">Cancelar Cita Paciente</legend>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon">Rut</i></span>
                        <div class="col-md-4">

                            <input type="text" id="rutpaciente" style="text-transform:uppercase;" name="rutpaciente" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                    return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                            <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                            <input value="d" name="txtDV" id="txtDV" type="hidden">

                        </div>

                        <button type="button" class="btn btn-primary" onclick="javascript : buscaPaciente()">Buscar</button>
                    </div>

                    <div id="buscar"></div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
<jsp:include page="../comunes/Footer.jsp" />