<%-- 
    Document   : DetalleDatosPaciente
    Created on : 30-01-2017, 10:18:27 AM
    Author     : Informatica
--%>

<%@page import="Modelos.lugar"%>
<%@page import="Controlador.controlador_paciente"%>
<%@page import="Modelos.paciente"%>
<%@ page import="java.util.Date" %> 

<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<script>


    function validar() {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = document.forms["detallepaciente"]["email"].value;
        var telefonoPaciente1 = document.forms["detallepaciente"]["telefonoPaciente"].value;
        if (telefonoPaciente1.length < 9 || email=="") {
            Alert.render("Debe  un telefono con el formato correcto , email con formato correcto  !! Debe Completar los Datos para Continuar");
            return false;
        }
        var k = document.forms["detallepaciente"]["region"].value;
        if (k == "-1") {
            Alert.render("Debe  seleccionar una region !! Debe Completar los Datos para Continuar");
            return false;
        } 
        else{
            var b = document.forms["detallepaciente"]["provincia"].value;
            if (b == "-1") {
                Alert.render("Debe  seleccionar una provincia !! Debe Completar los Datos para Continuar");
                return false;
            } else {
                var a = document.forms["detallepaciente"]["comuna"].value;
                if (a == "-1") {
                    Alert.render("Debe  seleccionar una comuna !! Debe Completar los Datos para Continuar");
                    return false;
                }
            }
        }




    }

    function buscaProvincia() {

        if (document.getElementById('region').value == -1)
        {

            Alert.render("Debe Completar los Datos para Continuar");
            document.getElementById('region').focus();
        } else
        {

            var region = document.getElementById('region').value;
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("provincia1");
                xmlhttp.open("post", "buscarprovincia2.jsp?region=" + region);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b> </b>';
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;
                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';
                    }
                }
            }

        }

    }
    function buscaComuna(provincia) {

        if (document.getElementById('provincia').value == -1)
        {

            Alert.render("Debe Completar los Datos para Continuar, Debe Elegir Provincia");
            document.getElementById('provincia').focus();
        } else
        {




            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("comuna1");
                xmlhttp.open("post", "buscarcomuna2.jsp?provincia=" + provincia);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b></b>';
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;
                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';
                    }
                }
            }

        }

    }



</script>
<body>


    <% int comuna = 0;
        controlador_paciente cp = new controlador_paciente();
        String rut = "";
        Date paciente_fecha_creacion = new Date();
        if (request.getParameter("rut") != null) {
            rut = request.getParameter("rut");
        }
        paciente paciente = cp.BuscarDetallePaciente(rut);

        String mensaje = "";
        if (request.getParameter("men") != null) {
            mensaje = request.getParameter("men");
    %>
    <div id="dialog-message" title="Detalle Paciente">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

        </p>
        <p>
            <b><%=mensaje%></b>.
        </p>
    </div>


    <%
        }

    %>
    <div class="container"> 

        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <fieldset>
            <legend class="text-center header">Detalle Paciente</legend>


            <table class="table-striped" style="width: 100%">

                <tr><th class="letra">Rut</th> 
                    <th class="letra">Nombre</th>
                    <th class="letra">Apellido Paterno</th>
                    <th class="letra">Apellido Paterno</th>
                    <th class="letra">Genero</th>
                    <th class="letra">Telefono</th></tr>
                <tr>
                    <td><label id="rut"><%=rut%></label></td>
                    <td><%=paciente.getNombre()%></td>
                    <td><%=paciente.getApellido_paterno()%></td>
                    <td><%=paciente.getApellido_moderno()%></td>
                    <td><%=paciente.getTemporal1()%></td>
                    <td><%=paciente.getContacto1() + '/' + paciente.getContacto2()%></td>

                </tr>
                <br>
                <tr>
                    <th class="letra">Fecha de Nacimiento </th>
                    <th class="letra" >Edad </th>
                    <th class="letra">Comuna </th>
                    <th class="letra">Direccion</th>
                    <th class="letra">Procedencia </th>
                    <th class="letra">Previsi�n </th>
                    <th class="letra">Tramo </th>
                </tr>
                <tr>


                    <td><%=paciente.getFecha_nacimiento()%></td>
                    <td ><%=paciente.getTemporal7()%></td>
                    <td><%=paciente.getTemporal5()%></td>
                    <td><%=paciente.getDireccion()%></td>
                    <td><%=paciente.getTemporal6()%></td>
                    <td><%=paciente.getTemporal2()%></td>
                    <td><%=paciente.getTemporal4()%></td>
                </tr>
                <br>


            </table> 



            <div class="container">      
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#historial">Historial de Telefonos y Direccion</a></li>
                    <li><a data-toggle="tab" href="#hitos">Hitos</a></li>

                </ul>
                <div class="tab-content">
                    <div id="historial" class="tab-pane fade in active">
                        <form  id="detallepaciente" name="detallepaciente" action='<%=cp.getLocal()%>ingresarHistoiralPaciente' onsubmit="return validar()"  method="post" >  
                            <legend class="text-center header">Ingresar Telefonos y Direccion</legend>
                            <input name="rut" id="rut" value="<%=rut%>" hidden >
                            <div class="form-group">

                                <table style="border: #619fd8 5px solid;" >
                                    <tr>
                                        <td>

                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-phone bigicon" style="font-size: 20px">Telefonos</i></span>
                                            <br>
                                            <div class="col-md-10">
                                                <input type="tel" id="telefonoPaciente" name="telefonoPaciente" class="form-control" placeholder="+56 9 1111 1111"  required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">


                                                <input type="tel" id="telefonoPaciente1" name="telefonoPaciente1" class="form-control" placeholder="+56 9 2222 2222">
                                            </div>

                                        </td><td>

                                            <br>
                                            <span class="col-md-1 col-md-offset-1 "><i class="fa fa-edit bigicon" style="font-size: 20px" >Email</i></span>
                                            <br>
                                            <div class="col-md-10">
                                                <input type="email"   id="email" name="email"  class="form-control" placeholder="email" >


                                            </div>

                                        </td><td>

                                            <br>
                                            <span class="col-md-1 col-md-offset-1 "><i class="fa fa-edit bigicon" style="font-size: 20px" >Direccion</i></span>
                                            <br>
                                            <div class="col-md-10">
                                                <textarea type="text"  rows="3" id="direccion" name="direccion"  class="form-control" placeholder="Direccion" required oninvalid="setCustomValidity('El campo direccion es obligatorio')" oninput="setCustomValidity('')"></textarea>


                                            </div>

                                        </td>


                                        <td>
                                            <table> 
                                                <tr><td>

                                                        <div class="form-group">
                                                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                                                            <div class="col-md-8" >
                                                                <select class="form-control" id="region" name="region" required onchange=" return buscaProvincia()"  >
                                                                    <option value=-1>Elegir Regi�n
                                                                        <%

                                                                            for (lugar temp : cp.buscarregion()) {

                                                                                if (temp.getId_lugar() == comuna) {%>
                                                                    <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                                                    } else {%>
                                                                    <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                                                            }

                                                                        }
                                                                        %>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr><tr>
                                                    <td>
                                                        <div id="provincia1">

                                                        </div>

                                                    </td>
                                                </tr><tr><td>
                                                        <div id="comuna1">


                                                        </div>

                                                    </td>
                                                </tr>
                                            </table>  
                                        </td>
                                    </tr>


                                    <tr> 
                                        <td colspan="3">
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-primary btn-lg">Agregar</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>


                                </table>
                                <table class="table table-striped" style="width: 100%">
                                    <thead>
                                        <tr >
                                            <th>Fecha Ingreso</th>
                                            <th>Contacto 1</th>
                                            <th>Contacto 2</th>

                                            <th>Direccion</th>
                                            <th>Region </th>
                                            <th>Provincia</th>
                                            <th>Comuna</th>

                                            <th>Funcionario Que registro</th>


                                        </tr>
                                    </thead>
                                    <%
                                        for (paciente temp : cp.buscarHistorialpacienteAntiguos(rut)) {
                                    %>

                                    <tr>
                                        <td><%=temp.getTemporal3()%></td>
                                        <td><%=temp.getContacto1()%></td>
                                        <td><%=temp.getContacto2()%></td>
                                        <td><%=temp.getDireccion()%></td>

                                        <td><%=temp.getApellido_moderno()%></td>
                                        <td><%=temp.getApellido_paterno()%></td>
                                        <td><%=temp.getNombre()%></td>


                                        <td><%=temp.getTemporal2()%></td>


                                    </tr>
                                    <%}%>
                                </table>

                            </div>
                        </form>     
                    </div>

                    <div id="hitos" class="tab-pane fade">
                        <form id="hit" name="hit" action='<%=cp.getLocal()%>registrarhitos' method="post"  > 
                            <input name="rut" id="rut" value="<%=rut%>" hidden >
                            <legend class="text-center header">Hitos </legend>
                            <div class="form-group">

                                <table style="border: #619fd8 5px solid;" >
                                    <tr>
                                        <td colspan="3">
                                            <span class="col-md-1 col-md-offset-1 "><i class="fa fa-edit bigicon"  ></i></span>

                                            <div class="col-md-8">
                                                <textarea type="text"  rows="3" id="hito" name="hito"  class="form-control" placeholder="Hito" required oninvalid="setCustomValidity('El campo hito es obligatorio')" oninput="setCustomValidity('')"></textarea>


                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>


                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-list-alt bigicon" style="font-size: 20px">LLamada</i></span>
                                            <br> <br>
                                            <div class="col-md-10" >
                                                <select class="form-control" id="llamada" name="llamada" >
                                                    <option value="0">Elegir tipo llamada 
                                                        <%
                                                            for (lugar opcionhito : cp.buscaropcionHito()) {

                                                                if (opcionhito.getId_lugar() == comuna) {%>
                                                    <option selected value="<%=opcionhito.getId_lugar()%>"><%=opcionhito.getNombre()%><%
                                                    } else {%>
                                                    <option value="<%=opcionhito.getId_lugar()%>"><%=opcionhito.getNombre()%><%
                                                            }

                                                        }
                                                        %>
                                                </select>
                                            </div>




                                        </td>
                                        <td>


                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-list-alt bigicon" style="font-size: 20px">Carta</i></span>
                                            <br> <br>
                                            <div class="col-md-10" >
                                                <select class="form-control" id="carta" name="carta" >
                                                    <option value="0">Elegir tipo Carta 
                                                        <%
                                                            for (lugar opcionhito : cp.buscaropcionHito2()) {

                                                                if (opcionhito.getId_lugar() == comuna) {%>
                                                    <option selected value="<%=opcionhito.getId_lugar()%>"><%=opcionhito.getNombre()%><%
                                                    } else {%>
                                                    <option value="<%=opcionhito.getId_lugar()%>"><%=opcionhito.getNombre()%><%
                                                            }

                                                        }%>
                                                </select>
                                            </div>




                                        </td>
                                        <td>


                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-list-alt bigicon" style="font-size: 20px">Examen</i></span>
                                            <br> <br>
                                            <div class="col-md-10" >
                                                <select class="form-control" id="examen" name="examen">
                                                    <option value="0">Elegir tipo Examen 
                                                        <%
                                                            for (lugar opcionhito : cp.buscaropcionHito3()) {

                                                                if (opcionhito.getId_lugar() == comuna) {%>
                                                    <option selected value="<%=opcionhito.getId_lugar()%>"><%=opcionhito.getNombre()%><%
                                                    } else {%>
                                                    <option value="<%=opcionhito.getId_lugar()%>"><%=opcionhito.getNombre()%><%
                                                            }

                                                        }%>
                                                </select>
                                            </div>




                                        </td>


                                    </tr>

                                    <tr> 
                                        <td colspan="3">
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-primary btn-lg">Agregar</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-striped" style="width: 100%">
                                    <thead>
                                        <tr >

                                            <th>Fecha</th>
                                            <th>Descripcion</th>

                                            <th>Tipo Llamada</th>
                                            <th>Tipo Carta</th>
                                            <th>Tipo Examen</th>
                                            <th>Funcionario que Registro</th>


                                        </tr>
                                    </thead>
                                    <%
                                        for (paciente temp : cp.buscarHitosdePaciente(rut)) {
                                    %>
                                    <tr>
                                        <td><%=temp.getTemporal3()%></td>
                                        <td><%=temp.getNombre()%></td>
                                        <td><%=temp.getApellido_moderno()%></td>
                                        <td><%=temp.getApellido_paterno()%></td>
                                        <td><%=temp.getDireccion()%></td>
                                        <td><%=temp.getTemporal2()%></td>

                                    </tr>
                                    <%}%>
                                </table>

                            </div>
                        </form>
                    </div>

                </div>

            </div>
            <br>
            <br>

        </fieldset>





    </div>

</div>

</div>

</body>
<jsp:include page="../comunes/Footer.jsp"/>
