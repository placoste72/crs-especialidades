<%-- 
    Document   : DatosPacienteConfirma
    Created on : 16-12-2016, 10:07:48 AM
    Author     : Informatica
--%>

<%@page import="Modelos.cita"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_cita"%>

<!DOCTYPE html>
<%
String pacienterut = request.getParameter("paciente");

controlador_cita cc= new controlador_cita();



%>
<table class="table table-striped" style=" width: 100%">
    <thead>
        <tr>
            <th>Codigo Cita</th>
            <th>Especialidad</th>
            <th>Paciente</th>
            <th>Doctor</th>
            <th>Fecha Cita</th>
            <th>Hora Cita</th>
            <th>Tipo Oferta</th>
            <th>Atencion</th>
            <th>Patologia</th>
           
            <th>GES</th>
            <th></th>
        </tr>
        
        
    </thead>
    <% for(cita temp : cc.buscarcitasParaCancelardePacientedeHoyenAdelante(pacienterut)){ %>
    <tr>
        <td><%= temp.getId_cita() %></td> 
        <td><%= temp.getTemporales() %></td> 
        <td><%= temp.getRut_paciente() %></td> 
        <td><%= temp.getRut_doctor() %></td> 
        <td><%= temp.getTemporales3() %></td> 
        <td><%= temp.getTemporales4() %></td>
        <td><%= temp.getTemporales5() %></td>
        <td><%= temp.getTemporales1() %></td> 
        <td><%= temp.getTemporales2() %></td> 
       
        <% if (temp.getGes() == 1) { %>
        <td>SI</td>
        <%}else{%>
         <td>NO</td> 
        <%}%>
        <td><a href='<%=cc.getLocallink()%>Cita/CancelarCitaComentario.jsp?cod=<%=temp.getId_cita() %>' onClick="return popup(this, 'notes')">
                <img src="../../public/imagenes/eliminar.gif" width="16" height="16" alt="Cancelar Cita Paciente" title="Cancelar Cita Paciente" border="0" />
          </a> </td>
    </tr>
    <%}%>
    
</table>