<%-- 
    Document   : cargar
    Created on : 21-08-2019, 17:24:45
    Author     : Placoste
--%>

<%@page import="java.util.Properties"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="estructura.Conexion" %>
<%@page import="estructura.Proceso" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.DriverManager" %>
<%@page import="java.sql.SQLException" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Carga Masiva</title>
        <link rel="stylesheet" href="css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
        <%@ page language="Java" import="java.sql.*" %>  
        <jsp:useBean id="basedatos" scope="request" class="estructura.Conexion" />
        <jsp:useBean id="acciones" scope="request" class="estructura.Proceso" />
        <jsp:setProperty name="basedatos" property="*" />
        <jsp:setProperty name="acciones" property="*" />
        <%  
          ResultSet resultado = null ; 
          ResultSetMetaData resultadoMetaDatos = null ; 
          int numerolineas ; 
          int i;  
        %>

<center>  
<h2> Carga Masiva de Citas - Validación </h2>  
<hr>  
<br><br>
<form method="post" action="cargar.jsp">
<%     
    acciones.leerExcel(request.getParameter("txtarchivo"));
    int valoractualizacion=0;
    int valorBitacora=0;
    String salida=null;
    basedatos.connect();
    try { 
        valoractualizacion = basedatos.updateSQL(acciones.IngresotablaBitacoraMasivo("placoste", "archivo.xlsx")); 
        if (valoractualizacion==1) {
            resultado = basedatos.execSQL("SELECT MAX(id_bitacora) FROM agenda.bitacora_masivo;");
            if(resultado.next()) {   
                valorBitacora=resultado.getInt(1);
            }
            resultado.close();
            if (valoractualizacion==1) {
                salida = acciones.validaRegistrosDetalleBitacora(valorBitacora); 
            }
        }
    } catch(SQLException e) { 
        throw new ServletException("Your query is not working", e);    
    }
    resultado = basedatos.execSQL("select * from agenda.bitacora_detalle where id_bitacora="+valorBitacora);
%>
<table class="table table-sm table table-striped table-bordered">
    <thead>
    <tr  class="table-info">
        <th scope="col" class="lineaEncabezado">RUN</th>
        <th scope="col" class="lineaEncabezado">NOMBRE</th>
        <th scope="col" class="lineaEncabezado">APELLIDO PATERNO</th>
        <th scope="col" class="lineaEncabezado">APELLIDO MATERNO</th>
        <th scope="col" class="lineaEncabezado">TELEFONO_1</th>
        <th scope="col" class="lineaEncabezado">TELEFONO_2</th>
        <th scope="col" class="lineaEncabezado">NOMBRE ATENCION</th>
        <th scope="col" class="lineaEncabezado">NOMBRE ESPECIALIDAD</th>
        <th scope="col" class="lineaEncabezado">OBSERVACIONES</th>
    </tr>
    </thead>
    <tbody>
<% while (resultado.next()) { %>    
    <tr>
        <td valign="middle" align="center"><%=resultado.getString("rut") %></td>
        <td valign="middle" align="center"><%=resultado.getString("Nombre") %></td>
        <td valign="middle" align="center"><%=resultado.getString("apellido_paterno") %></td>
        <td valign="middle" align="center"><%=resultado.getString("apellido_materno") %></td>
        <td valign="middle" align="center"><%=resultado.getString("Telefono_1") %></td>
        <td valign="middle" align="center"><%=resultado.getString("Telefono_2") %></td>
        <td valign="middle" align="center"><%=resultado.getString("atencion_nombre") %></td>
        <td valign="middle" align="center"><%=resultado.getString("especialidad_descripcion") %></td>
        <td valign="middle" align="center"><%=resultado.getString("observaciones") %></td>
    </tr>
<% } %>
    </tbody>
</table>
    <table>
        <tr>
            
            <td class="col-lg-2"><input class="btn btn-primary btn-sm" type="submit" value="Citar"></td>
        </tr>
    </table>
        </form>
</div>        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
