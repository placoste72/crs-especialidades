<%-- 
   Document   : RegistrarPaciente
   Created on : 09-12-2016, 10:36:35 AM
   Author     : Informatica
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.prevision"%>
<%@page import="Controlador.controlador_paciente"%>
<%@page import="Modelos.paciente"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>

<!DOCTYPE html>

<jsp:include page="../comunes/Header.jsp"/>
<%    int comuna = 0;
    int tramo = 0;
    int procedencia1 = 0;
    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
    controlador_paciente cp = new controlador_paciente();
    paciente p = new paciente();
    int region = 0;
    String nombre = "";
    String apellidop = "";
    String apellidom = "";
    int idnacionalidad = 213;
    String nombresocial = "";

    /*1 fonasa , 2 isapre, 3 praim*/
    int codigoprevicion = 0;

    String fecha = "";
    int generoe = 0;
    int t = 0;

    String contacto1 = "";
    String contacto2 = "";
    String email = "";
    String direccion = "";
    int provincia = 0;
    String rutcompleta = "";
    String[] gen = {"Genero", "Femenino", "Masculino"};

    if (request.getParameter("rutpaciento") != null) {
        rutcompleta = cp.FormatearRUT(request.getParameter("rutpaciento"));
        String rut = "";
        boolean validar = cp.validarRut(rutcompleta);
        if (validar == true) {
            rut = rutcompleta.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);
            p = cp.consumirWsFonasa(rutAux, String.valueOf(dv), rutcompleta);
            try {
                if (p.getRut() != null) {
                    nombre = p.getNombre();
                    apellidop = p.getApellido_paterno();
                    apellidom = p.getApellido_moderno();
                    fecha = formatoFecha.format(p.getFecha_nacimiento());
                    generoe = p.getGenero();
                    codigoprevicion = p.getProvision();
                    contacto1 = p.getContacto1();
                    contacto2 = p.getContacto2();
                    email = p.getEmail();
                    direccion = p.getDireccion();
                    region = p.getId_region();
                    comuna = p.getId_comuna();
                    provincia = p.getId_provincia();
                    procedencia1 = p.getProcedencia();
                    t = p.getTramo();
                    idnacionalidad = p.getIdnacionalidad();
                    nombresocial= p.getNombresocial();
                    
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

    }

%>
<script>

    $(function () {
        $("#txt_inicial").datepicker();
    });


    function tramo() {
        var pre = document.forms["paciente"]["provision"].value;
        if (pre == 1) {

            document.getElementById("t").style.display = "block";
        } else
            document.getElementById("t").style.display = "none";

    }

    function formateaRut(Rut)
    {
        var sRut = new String(Rut);
        var sRutFormateado = '';
        sRut = quitaFormato(sRut);
        var sDV = sRut.charAt(sRut.length - 1);
        sRut = sRut.substring(0, sRut.length - 1);
        document.forms["paciente"].txtRutSinDV.value = sRut;
        document.forms["paciente"].txtDV.value = sDV;
        while (sRut.length > 3)
        {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
        }
        sRutFormateado = sRut + sRutFormateado;
        if (sRutFormateado != "")
            sRutFormateado += "-";
        sRutFormateado += sDV;
        if (document.forms["paciente"].rutpaciento.value != sRutFormateado)
            document.forms["paciente"].rutpaciento.value = sRutFormateado;
    }




    function quitaFormato(Nro)
    {
        var strNro = new String(Nro);
        while (strNro.indexOf(".") != - 1)
            strNro = strNro.replace(".", "");
        strNro = strNro.replace("-", "");
        return strNro;
    }



    function DigitoVerificadorRut(strRut) {
        var rut = 0;
        var s = 0;
        var l_dv = "";

        rut = strRut;
        for (i = 2; i < 8; i++) {
            s = s + (rut % 10) * i;
            rut = (rut - (rut % 10)) / 10;
        }
        s = s + (rut % 10) * 2;
        rut = (rut - (rut % 10)) / 10;
        s = s + (rut % 10) * 3;
        rut = (rut - (rut % 10)) / 10;
        s = 11 - (s % 11);
        if (s == 10)
            l_dv = "K";
        else
        if (s == 11)
            l_dv = "0"
        else
            l_dv = s + "";
        return(l_dv);
    }

    function validaRut() {
        var sRut = new String(document.forms["paciente"]["rutpaciento"].value);
        sRut = quitaFormato(sRut);
        var sDV = new String(sRut.charAt(sRut.length - 1));
        sRut = sRut.substring(0, sRut.length - 1);
        if (sDV.toUpperCase() == DigitoVerificadorRut(sRut))
        {
            return true;
        }
        if (sDV.toUpperCase() != DigitoVerificadorRut(sRut))
        {
            return false;
        }
    }



    function buscarFonasa() {
        var paciente = document.forms["paciente"]["rutpaciento"].value;


        if (paciente == null || paciente == "" || paciente.length == 0)
        {

            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }
        if (validaRut() == false) {
            Alert.render("Debe Introducir una rut valida");
            return false;
        }
    }






    function validarcombos() {
        var x = document.forms["paciente"]["genero"].value;
        var z = document.forms["paciente"]["provision"].value;
        var k = document.forms["paciente"]["region"].value;
        var a = document.forms["paciente"]["comuna"].value;
        var b = document.forms["paciente"]["provincia"].value;
        var c = document.forms["paciente"]["txt_inicial"].value;
        var tramo = document.forms["paciente"]["tramo"].value;
        var nacionalidad = document.forms["paciente"]["nacionalidad"].value;



        if (a == "-1" || b == "-1" || x == "0" || z == "-1" || k == "-1" || c == "" || c == null || nacionalidad =="-1") {
            return false;

        }
        if (z == "1" && tramo == "0") {
            return false;
        }

    }

    function validardatos() {
         
        var direccion = document.forms["paciente"]["direccion"].value;
        var email = document.forms["paciente"]["email"].value;
        var telefonoPaciente1 = document.forms["paciente"]["telefonoPaciente"].value;

        var apellidompaciente = document.forms["paciente"]["apellidompaciente"].value;
        var apellidoppaciente = document.forms["paciente"]["apellidoppaciente"].value;
        var nombrepaciente = document.forms["paciente"]["nombrepaciente"].value;
        var rutpaciento = document.forms["paciente"]["rutpaciento"].value;

        if (direccion == "" || direccion == null || email==""|| telefonoPaciente1.length<9 || telefonoPaciente1 == null || apellidompaciente == "" || apellidompaciente == null || apellidoppaciente == "" || apellidoppaciente == null || nombrepaciente == "" || nombrepaciente == null || rutpaciento == "" || rutpaciento == null) {

            return false;
        }


    }
    
   

    function validar() {
        if (validarcombos() == false) {
            Alert.render("Revise los Combos,Si es Fonasa debe tener un Tramo!! Debe Completar los Datos para Continuar");
            return false;
        } else if (validardatos() == false) {
            Alert.render("Debe completar la direccion , un telefono con el formato correcto , email con formato correcto y datos personales !! Debe Completar los Datos para Continuar");
            return false;
        } else {
            document.paciente.action = '<%=cp.getLocal()%>ingresar_paciente';
            document.paciente.submit();
        }
    }



    function buscaProvincia() {

        if (document.getElementById('region').value == -1)
        {

            Alert.render("Debe Elegir Region!! Debe Completar los Datos para Continuar");
            document.getElementById('region').focus();
        } else
        {

            var region = document.getElementById('region').value;

            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("provincia1");
                xmlhttp.open("post", "buscarprovincia2.jsp?region=" + region);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b> </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }
    function buscaComuna(provincia) {

        if (document.getElementById('provincia').value == -1)
        {

            Alert.render("Debe Elegir Provincia!! Debe Completar los Datos para Continuar");
            document.getElementById('provincia').focus();
        } else
        {




            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("comuna1");
                xmlhttp.open("post", "buscarcomuna2.jsp?provincia=" + provincia);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b></b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }


</script>
<%    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Registrar Paciente">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>


<%
    }


%>


<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>

<div class="container"> 
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#crear">Crear o Modificar</a></li>


    </ul>
    <div class="tab-content" style="text-align:center;">
        <div id="crear" class="tab-pane fade in active" style="text-align:center;">



            <form name="paciente" class="form" style="border: #619fd8 2px solid; margin: 50px; margin-bottom: 20px" id="paciente"  method="post" action="RegistrarPaciente.jsp" onsubmit="return buscarFonasa()"  >

                <fieldset>
                    <legend class="text-center header">Crear o modificar Paciente</legend>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-credit-card bigicon" style=" font-size:14px">RUT</i></span>
                        <div class="col-md-6">

                            <input type="text" id="rutpaciento" name="rutpaciento" style="text-transform:uppercase;" value="<%=rutcompleta%>"  onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                    return false" onkeyup="formateaRut(this.value);" maxlength="12" class="form-control" placeholder="Rut Paciente">
                            <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                            <input value="d" name="txtDV" id="txtDV" type="hidden">
                        </div>

                        <button style="margin-left:90px" type="summit" class="btn btn-primary" >Buscar</button>
                    </div>



                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon" style=" font-size:14px">Nombre</i></span>
                        <div class="col-md-8">
                            <input type="text" id="nombrepaciente" name="nombrepaciente" value="<%=nombre%>"   class="form-control" onkeypress="return validarsololetra(event)" placeholder="Nombre Paciente"   oninvalid="setCustomValidity('El campo Nombre es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon" style=" font-size:14px">Apellido P</i></span>
                        <div class="col-md-8">
                            <input type="text" id="apellidoppaciente" name="apellidoppaciente" value="<%=apellidop%>"   class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Paterno"   oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon" style=" font-size:14px">Apellido M</i></span>
                        <div class="col-md-8">
                            <input type="text" id="apellidompaciente" name="apellidompaciente"  value="<%=apellidom%>" class="form-control" onkeypress="return validarsololetra(event)" placeholder="Apellido Materno"  oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon" style=" font-size:14px">Nombre Social</i></span>
                        <div class="col-md-8">
                            <input type="text" id="nombresocial" name="nombresocial" value="<%=nombresocial%>"   class="form-control" onkeypress="return validarsololetra(event)" placeholder="Nombre Social"   >
                        </div>
                    </div>


                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar-o bigicon" style=" font-size:14px">Fecha Nacimiento</i></span>
                        <div class="col-md-8">
                            <input type="text" name="txt_inicial" id="txt_inicial" value="<%=fecha%>"  placeholder="Fecha de Nacimiento (dd/mm/yy) " class="form-control"  maxlength="13" size="12"  oninvalid="setCustomValidity('El campo Fecha de Nacimiento es obligatorio')" oninput="setCustomValidity('')" >
                        </div>
                    </div>


                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon" style=" font-size:14px">Genero</i></span>
                        <div class="col-md-8" >
                            <select class="form-control" id="genero" name="genero"  >
                                <%for (int i = 0; i < gen.length; ++i) {
                                        if (generoe == i) {%>

                                <option selected value="<%=i%>"><%=gen[i]%> 

                                    <%    } else {%>

                                <option value="<%=i%>"><%=gen[i]%>
                                    <%}
                                        }%>
                            </select>
                        </div>  
                    </div>
                    <table>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon" style=" font-size:14px">Telefono</i></span>
                                    <div class="col-md-8">

                                        <input type="tel" id="telefonoPaciente"  name="telefonoPaciente" value="<%=contacto1%>" class="form-control" placeholder="+56 9 1111 1111" maxlength="11"  minlength="8"  oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon" style=" font-size:14px">Otro Telefono</i></span>
                                    <div class="col-md-8">

                                        <input type="tel" id="telefonoPaciente1"  name="telefonoPaciente1" value="<%=contacto2%>" class="form-control" placeholder="+56 9 2222 2222" maxlength="11">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon" style=" font-size:14px">Email</i></span>
                        <div class="col-md-8">
                            <input type="email" id="email" name="email"  value="<%=email%>" class="form-control" placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon" style=" font-size:14px">Direcci�n</i></span>
                        <div class="col-md-8">
                            <textarea  rows="3" id="direccion" name="direccion" value="<%=direccion%>"   class="form-control" placeholder="Direccion"  oninvalid="setCustomValidity('El campo direccion es obligatorio')" oninput="setCustomValidity('')"><%=direccion%> </textarea>


                        </div>
                    </div >
                    
                     <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon" style=" font-size:14px">Nacionalidad</i></span>
                        <div class="col-md-8" >
                            <select class="form-control" id="nacionalidad" name="nacionalidad"   >
                                <option value=-1>Elegir nacionalidad
                                    <%    for (lugar l : cp.buscarNacionalidades()) {

                                            if (l.getId_lugar() == idnacionalidad) {%>
                                <option selected value="<%=l.getId_lugar()%>"><%=l.getDescripcion()%><%
                                } else {%>
                                <option  value="<%=l.getId_lugar()%>"><%=l.getDescripcion()%><%
                                        }

                                    }

                                    %>
                            </select>
                        </div> 


                    </div>
                    
                    
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon" style=" font-size:14px">Region</i></span>
                        <div class="col-md-8" >
                            <select class="form-control" id="region" name="region"  onchange=" return buscaProvincia()"  >
                                <option value=-1>Elegir Regi�n
                                    <%    for (lugar l : cp.buscarregion()) {

                                            if (l.getId_lugar() == region) {%>
                                <option selected value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                                } else {%>
                                <option  value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                                        }

                                    }

                                    %>
                            </select>
                        </div> 


                    </div>



                    <div id="provincia1">
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon" style=" font-size:14px" >Provincia</i></span>
                            <div class="col-md-8"  >
                                <select class="form-control" id="provincia" name="provincia" required onchange=" return buscaComuna(this.value)">
                                    <option value=-1>Elegir provincia
                                        <%    for (lugar l : cp.buscarProvidencia(region)) {

                                                if (l.getId_lugar() == provincia) {%>
                                    <option selected value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                                    } else {%>
                                    <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                                            }

                                        }
                                        %>
                                </select>
                            </div>
                        </div>

                    </div> 

                    <div id="comuna1">
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon" style=" font-size:14px">Comuna</i></span>
                            <div class="col-md-8" >
                                <select class="form-control" id="comuna" name="comuna" required>
                                    <option value="-1">Elegir Comuna
                                        <%
                                            for (lugar temp : cp.buscarComuna(provincia)) {

                                                if (temp.getId_lugar() == comuna) {%>
                                    <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                    } else {%>
                                    <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                            }

                                        }
                                        %>
                                </select>
                            </div>
                        </div>

                    </div> 

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-left"><i class="fa fa-list-alt bigicon" style=" font-size:14px">Procedencia</i></span>
                        <div class="col-md-8" >

                            <select class="form-control" id="procedencia" name="procedencia" >
                                <option value="0">Elegir Consultorio
                                    <%      for (lugar temp : cp.buscarProcedencia()) {

                                            if (temp.getId_lugar() == procedencia1) {%>
                                <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                } else {%>
                                <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                        }

                                    }%>
                            </select>

                        </div>        
                    </div>

                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon" style=" font-size:14px">Previsi�n</i></span>
                        <div class="col-md-8" >
                            <select class="form-control" id="provision" name="provision"  onchange="javascript:
                                            var pre = document.forms['paciente']['provision'].value;
                                    if (pre == 1) {

                                        document.getElementById('t').style.display = 'block';
                                    } else
                                        document.getElementById('t').style.display = 'none';
                                    ">


                                <option  value="-1">Prevision
                                    <% for (prevision pre : cp.buscarPrevicion()) {

                                            if (codigoprevicion == pre.getId_prevision()) {
                                    %>
                                <option selected  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                                    <% } else {%>
                                <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                                    <%}
                                        }%>

                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="t" style="display: none" >
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon" style=" font-size:14px">Tramo</i></span>
                        <div class="col-md-8" >
                            <select class="form-control" id="tramo" name="tramo" >
                                <option value="0">Tramo
                                    <% for (prevision pre : cp.buscarTramo()) {

                                            if (pre.getId_prevision() == t) {%>
                                <option selected value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>  
                                    <%} else {%>

                                <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                                    <%}
                                        }%>
                            </select>
                        </div>


                    </div>   



                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="button" onclick="javascript: validar();" class="btn btn-primary btn-lg">Guardar</button>

                            <button type="reset" class="btn btn-primary btn-lg" >Cancelar</button>
                        </div>
                    </div>


                </fieldset>


            </form>


        </div>

    </div>
</div>
<script>
    tramo();
</script>


</body>

<jsp:include page="../comunes/Footer.jsp"/>
