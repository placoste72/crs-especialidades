<%-- 
    Document   : DatosPacienteConfirma
    Created on : 16-12-2016, 10:07:48 AM
    Author     : Informatica
--%>

<%@page import="Modelos.lugar"%>
<%@page import="Controlador.controlador_paciente"%>
<%@page import="Modelos.cita"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_cita"%>

<!DOCTYPE html>

<%
    int comuna = 0;
    int provincia = 0;
    int region = 0;
    String pacienterut = request.getParameter("paciente");

    controlador_cita cc = new controlador_cita();
    controlador_paciente cp = new controlador_paciente();
    paciente p = cp.buscarpacienteporrut(pacienterut);
    Vector<cita> tengo = cc.buscarcitasdePacientedeHoyenAdelante(pacienterut);

    region = p.getId_region();
    provincia = p.getId_provincia();
    comuna = p.getId_comuna();
    if (tengo.size() > 0) {
%>

<table style="border:  #619fd8 2px solid; ">
    <tr><td> &nbsp;</td></tr>
    <tr><td colspan="2"><div class=" letra" style=" font-size: 20px">Datos de Paciente para Modificar</div></td></tr>

    <tr>

        <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span></td> 

        <td whidth="200" >
            <div class="col-md-8">
                <input type="tel" id="telefonoPaciente" name="telefonoPaciente" value="<%=p.getContacto1()%>" class="form-control" placeholder="+56 9 1111 1111"  maxlength="11" required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
            </div>
        </td>
        <td whidth="300" >
            <div class="col-md-8">
                <input type="tel" id="telefonoPaciente1" name="telefonoPaciente1" value="<%=p.getContacto2()%>" class="form-control" placeholder="+56 9 2222 2222" maxlength="11">
            </div>
        </td>
        <td whidth="300" >
            <div class="col-md-8">
                <input type="text" id="emaildoctor" name="emaildoctor" value="<%=p.getEmail()%>" class="form-control" placeholder="Email Paciente" >
            </div> 
        </td>
    </tr>
    <tr >
        <td><span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon"></i></span></td>
        <td colspan="4" >

            <div class="col-md-10">
                <input type="text" style=" height: 71px; width: 884px" id="direccion" name="direccion" value="<%=p.getDireccion()%>"   class="form-control" placeholder="Direccion" required oninvalid="setCustomValidity('El campo direccion es obligatorio')" oninput="setCustomValidity('')">


            </div>  
        </td>
    </tr>
    <tr>
        <td>
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
        </td>
        <td whidth="200" >
            <div class="col-md-8" >
                <select class="form-control" id="region" name="region" required onchange=" return buscaProvincia()"  >
                    <option value=-1>Elegir Regi�n
                        <%
                            for (lugar regiones : cp.buscarregion()) {

                                if (regiones.getId_lugar() == p.getId_region()) {%>
                    <option selected value="<%=regiones.getId_lugar()%>"><%=regiones.getNombre()%><%
                    } else {%>
                    <option value="<%=regiones.getId_lugar()%>"><%=regiones.getNombre()%><%
                            }

                        }
                        %>
                </select>
            </div> 
        </td>

        <td whidth="300" >
            <div id="provincia1">
                <div class="form-group">
                    <!--  <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>-->
                    <div class="col-md-8"  >
                        <select class="form-control" id="provincia" name="provincia" required onchange=" return buscaComuna(this.value)">
                            <option value="-1">Elegir provincia
                                <%
                                    for (lugar l : cp.buscarProvidencia(region)) {

                                        if (l.getId_lugar() == p.getId_provincia()) {%>
                            <option selected value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                            } else {%>
                            <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                                    }

                                }
                                %>
                        </select>
                    </div>
                </div>

            </div> 
        </td>
        <td whidth="300" >
            <div id="comuna1">
                <div class="form-group">
                    <!--  <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>-->
                    <div class="col-md-8" >
                        <select class="form-control" id="comuna" name="comuna" required>
                            <option value="-1">Elegir Comuna
                                <%
                                    for (lugar temp : cp.buscarComuna(provincia)) {

                                        if (temp.getId_lugar() == p.getId_comuna()) {%>
                            <option selected value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                            } else {%>
                            <option value="<%=temp.getId_lugar()%>"><%=temp.getNombre()%><%
                                    }

                                }
                                %>
                        </select>
                    </div>
                </div>

            </div> 
        </td>
    </tr>
</table>
<table class="table table-striped" style=" width: 100%">
    <thead>
        <tr>
            <th>Codigo Cita</th>
            <th>Especialidad</th>
            <th>Paciente</th>
            <th>Doctor</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Atencion</th>
            <th>Patologia</th>

            <th>GES</th>
            <th></th>
        </tr>


    </thead>
    <% for (cita temp : cc.buscarcitasdePacientedeHoyenAdelante(pacienterut)) {
            lugar l = cc.avisodepagoenAdmision(temp.getId_cita());
            if (l.getRut() != null) {
    %>
    <tr>
        <td colspan="10">
            <span class="col-md-16 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 15px; color: #ff0000">&nbsp;&nbsp;        *******************************************     Aviso de "PAGO"    ***************************** <br> Prevision : <%=l.getDescripcion() + " " + l.getIndicaciones()%> &nbsp;   Prestaci�n: <%=l.getNombre()%>&nbsp;<br><p style=" text-align:  center"> TOTAL A PAGAR :<%=l.getVariable1()%></p></i> </span>


        </td>
    </tr>


    <%}%>
    <tr>
        <td><%= temp.getId_cita()%></td> 
        <td><%= temp.getTemporales()%></td> 
        <td><%= temp.getRut_paciente()%></td> 
        <td><%= temp.getRut_doctor()%></td> 
        <td><%= temp.getTemporales15()%></td> 
        <td><%= temp.getTemporales4()%></td>
        <td><%= temp.getTemporales1()%></td> 
        <td><%= temp.getTemporales2()%></td> 

        <% if (temp.getGes() == 1) { %>
        <td>SI</td>
        <%} else {%>
        <td>NO</td>
        <%}%>
        <td><a onclick="return confirmar(<%=temp.getId_cita()%>)" class="button3">

                Confirmar </a> </td>
    </tr>
    <%}%>

</table>

<%} else {
%>
<table>
    <tr style=" text-align: left">
        <td>Sin Cita para Hoy </td>
        <td>No Citado.</td>
        <td>
            <img style="width: 40px; height: 40px" src="../../public/imagenes/estatus/boton-eliminar_318-85849.jpg" alt=""/></td>
    </tr>
</table>
<%
    }%>