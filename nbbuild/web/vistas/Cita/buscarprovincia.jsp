<%-- 
    Document   : buscarprovincia
    Created on : 09-03-2017, 12:07:17
    Author     : a
--%>

<%@page import="Modelos.lugar"%>
<%@page import="Controlador.General"%>
<%@page import="java.sql.ResultSet"%>


<!DOCTYPE html>

<%    General g = new General();
    String region = request.getParameter("region");
    int comuna = 0;
%>
<div class="form-group">
    <!--  <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>-->
    <div class="col-md-8"  >
        <select class="form-control" id="provincia" name="provincia" required onchange=" return buscaComuna(this.value)">
            <option value="-1">Elegir provincia
                <%
                    for (lugar l : g.buscarProvidencia(Integer.parseInt(region))) {

                        if (l.getId_lugar() == comuna) {%>
            <option selected value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
            } else {%>
            <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%><%
                    }

                }
                %>
        </select>
    </div>
</div>
