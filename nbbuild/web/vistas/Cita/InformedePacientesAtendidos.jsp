<%-- 
    Document   : InformedePacientesAtendidos
    Created on : 18-04-2017, 14:19:56
    Author     : a
--%>

<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<%controlador_cita cc = new controlador_cita();
String[] datos = cc.buscarPerfilyNombredelloggeado();


%>

<div class="container">

    <div class="tab-content">

        <fieldset>
            <legend class="text-center header"></legend>
            <table class="table table-striped" style="width: 100%">
                <thead>

                    <tr>
                       <th>Perfil:          <%=datos[1] %></th>

                        <th>Usuario:         <%=datos[0]%></th>


                    </tr>
                </thead>
            </table>   
            <div class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; font-size: 20px;border-radius: 8px; height: 45px; text-align: left" >N�MINA DE PACIENTES INGRESADOS A ETAPA SIGUIENTE DE ATENCI�N</div>

            <table class="table table-bordered" style="width: 100%">
                <thead><tr>
                        
                    </tr>
                    <tr>
                        <th>RUT</th>
                        <th>Paciente</th>
                        <th>Edad</th>
                        <th>Sexo</th>
                        <th>Especialidad</th>
                        <th>Tipo de Cita</th>
                        <th>Profesional</th>
                        <th>Fecha y Hora</th>

                    </tr>

                </thead>
                <%
                        for (cita temp : cc.buscarcitasAtendidas()) {
                    %>
                <tr>
                    <td><%= temp.getMotivo_cancela()%></td>
                    <td><%= temp.getRut_paciente()%></td>
                    <td><%= temp.getTemporales1()%></td>
                    <td><%= temp.getTemporales2()%></td>
                    <td><%= temp.getTemporales()%> </td>
                    <td><%= temp.getTemporales3()%>  </td>
                    <td><%= temp.getRut_doctor()%></td>
                    <td><%= temp.getMotivo()%></td>

                </tr>
                <%}%>
            </table>
        </fieldset>
    </div>
</div>
                   
<jsp:include page="../comunes/Footer.jsp"/>