<%-- 
    Document   : DatosPaciente
    Created on : 14-12-2016, 03:01:38 PM
    Author     : Informatica
--%>

<%@page import="java.util.Vector"%>
<%@page import="Controlador.controlador_cupos"%>
<%@page import="Modelos.oferta"%>
<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.prevision"%>
<%@page import="Modelos.lugar"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_paciente"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>


<style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
<script type="text/javascript" src="../../public/js/calendar.js"></script>
<script type="text/javascript" src="../../public/js/calendar-es.js"></script>

<script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
<script>
    $(function () {
        $("#txt").datepicker();
    });

    function buscaProvincia() {

        if (document.getElementById('region').value == -1)
        {

            Alert.render("Debe Completar los Datos para Continuar");
            document.getElementById('region').focus();
        } else
        {

            var region = document.getElementById('region').value;

            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("provincia1");
                xmlhttp.open("post", "buscarprovincia.jsp?region=" + region);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '<b>Favor espere </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }
    function buscaComuna(provincia) {

        if (document.getElementById('provincia').value == -1)
        {

            Alert.render("Debe Completar los Datos para Continuar");
            document.getElementById('provincia').focus();
        } else
        {




            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("comuna1");
                xmlhttp.open("post", "buscarcomuna.jsp?provincia=" + provincia);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br></br><b></b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;


                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }

    }


</script>

<!DOCTYPE html>
<%  /*controladores*/
    controlador_cupos cu = new controlador_cupos();
    controlador_cita cc = new controlador_cita();
    controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
    controlador_paciente cp = new controlador_paciente();

    /*variables */
    Vector<cita> vc = new Vector<cita>();
    String mensaje = "";
    oferta o = new oferta();

    /*parametros*/
    String fecha = request.getParameter("fecha");
    String a = fecha.substring(6);
    String d = fecha.substring(0, 2);
    String m = fecha.substring(3, 5);
    //String motivo = request.getParameter("motivo");
    String especialidad = request.getParameter("especialidad");
    String fecha2 = m + "/" + d + "/" + a;
    String paciente = request.getParameter("paciente");
    if (request.getParameter("oferta") != null) {
        String oferta = request.getParameter("oferta");
        o = cu.buscarOfertaporIdparaconsulta(Integer.parseInt(oferta));
    }

    /*busqueda*/
    vc = cc.citasproximasdeunpaciente(paciente);
    boolean pacienteDisponible = cc.buscarCitaRutFecha(fecha2, paciente, o.getHora());
    paciente p = cp.buscarpacienteporrut(paciente);
   // ArrayList citas = caco.traerlasultimas5citas(paciente, Integer.parseInt(motivo));
   // Iterator it2 = citas.iterator();

    if (vc.size() > 0) {


%>
<p class="letra" style=" text-align: center; font-size: 25px ">Proximas Citas del Paciente.</p>
<table class=" table-striped" style=" border-bottom:   #619fd8 2px solid;border-top:   #619fd8 2px solid;">

    <tr>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Especialidad</th>
        <th>Tipo de Atencion</th>
        <th>Profesional </th>
    </tr>
    <%  for (int i = 0; i < vc.size(); ++i) {

    %>
    <tr>
        <td><%=vc.get(i).getTemporales()%></td>
        <td><%=vc.get(i).getTemporales1()%></td>
        <td><%=vc.get(i).getTemporales2()%></td>
        <td><%=vc.get(i).getTemporales3()%></td>
        <td><%=vc.get(i).getTemporales4()%></td>
    </tr>
    <%}%>

</table>
<br>
<br>

<%
    }

   // if (citas.size() > 0) {
%>

<%//}

    if (pacienteDisponible == true) {


%>

<table class=" table table-striped   " style="width: 100%">
    <tr>
        <th>
            <p style="color:#FF0000; font-size:15px">Paciente ya contiene Cita ese dia...</p>
        </th>
    </tr>
</table> 
<%        } else if (p.getRut() != null) {

%>

<table>
    <input type="text" name="f" id="f" value="<%=p.getRut()%>" hidden>

    <tr>
        <td>
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i>Nombre:</span>

        </td>

        <td colspan="4">

            <div class="col-md-8">
                <label id="nombre"><%=p.getNombre() + "  " + p.getApellido_paterno() + " " + p.getApellido_moderno()%></label>
            </div>
        </td>
        <td>
            <span class="col-md-1 col-md-offset-2 text-center "><i class="fa fa-calendar bigicon"> </i>  Fecha Nacimiento:</span>  
        </td>  

        <td colspan="4">

            <div class="col-md-8">
                <label><%=p.getFecha_nacimiento()%></label>
            </div>
        </td>
    </tr>
    <tr>
        <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i>Email:</span></td>
        <td colspan="4">

            <div class="col-md-8">
                <input type="text" id="emaildoctor" name="emaildoctor" value="<%=p.getEmail()%>" class="form-control" placeholder="Email Paciente" >

            </div>

        </td>
        <td><span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i>Contactos:</span></td>
        <td colspan="4">


            <div class="col-md-8">
                <input type="tel" value="<%=p.getContacto1()%>" id="telefonoPaciente" name="telefonoPaciente" class="form-control" placeholder="+56 9 1111 1111" >

            </div>
        </td>

    </tr>

</table>

<div class="form-group">
    <div class="col-md-4 text-center">
        <button type="button" id="asignar" onclick="javascritp:  document.cita.action = '<%=cc.getLocal()%>AsignarCita';
                document.cita.submit();" class="btn btn-primary btn-lg" style="align-content: center; display: block" >Asignar Cita</button>


    </div>
</div>
<%
} else {
    int comuna = 0;
    int tramo = -1;
    int procedencia1 = 0;

%>
<table>
    <tr>
        <th>    
            <p style="color:#FF0000; font-size:15px; align-content: center">Paciente no registrado debe ingresarlo</p>
        </th>
    </tr> 
</table>



<form  style="border: #619fd8 5px solid;" name="paciente" class="form-horizontal" id="paciente" action='' method="post" onsubmit="return validar()" >

    <fieldset>
        <table>
            <tr>
                <td ><span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span></td>
                <td colspan="1" >

                    <div class="col-md-6">

                        <input type="text" id="nombrepaciente" name="nombrepaciente" class="form-control" placeholder="Nombre Paciente" onkeypress="return validarsololetra(event)"  required oninvalid="setCustomValidity('El campo nombre es obligatorio')" oninput="setCustomValidity('')">
                    </div>  
                </td>
                <td colspan="2">
                    <div class="col-md-6">
                        <input type="text" id="apellidoppaciente" name="apellidoppaciente" class="form-control" placeholder="Apellido Paterno Paciente" onkeypress="return validarsololetra(event)"  required oninvalid="setCustomValidity('El campo Apellido Paterno es obligatorio')" oninput="setCustomValidity('')">

                    </div>  

                </td>
                <td colspan="3">
                    <div class="col-md-6">
                        <input type="text" id="apellidomdoctor" name="apellidompaciente" class="form-control" placeholder="Apellido Materno Paciente" onkeypress="return validarsololetra(event)" required oninvalid="setCustomValidity('El campo Apellido Materno es obligatorio')" oninput="setCustomValidity('')">
                    </div> 
                </td>

            </tr>
            <tr>
                <td>
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-calendar-o bigicon"></i></span>
                </td> 
                <td>
                    <table>
                        <tr>

                            <td >

                                <input type="text"   name="txt" id="txt" value="" placeholder="Fecha de Nacimiento" class="form-control"   required>
                            </td>

                    </table>
                </td>
                <td colspan="3">
                    <div class="col-md-8" >
                        <select class="form-control" id="genero" name="genero">
                            <option>Genero

                            <option  value="1">Femenino
                            <option value="2"> Masculino
                        </select>
                    </div>  
                </td>

            </tr>
            <tr>

                <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone bigicon"></i></span></td> 

                <td>
                    <div class="col-md-8">
                        <input type="tel" id="telefonoPaciente" name="telefonoPaciente" class="form-control" placeholder="+56 9 1111 1111"  maxlength="11" required oninvalid="setCustomValidity('El campo telefono es obligatorio')" oninput="setCustomValidity('')">
                    </div>
                </td>
                <td colspan="2">
                    <div class="col-md-8">
                        <input type="tel" id="telefonoPaciente1" name="telefonoPaciente1" class="form-control" placeholder="+56 9 2222 2222" maxlength="11">
                    </div>
                </td>
                <td>
                    <div class="col-md-8">
                        <input type="email" id="emaildoctor" name="emaildoctor" class="form-control" placeholder="Email Paciente" >
                    </div> 
                </td>
            </tr>
            <tr >
                <td><span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon"></i></span></td>
                <td colspan="4" >

                    <div class="col-md-8">
                        <textarea type="text"  rows="3" id="direccion" name="direccion"  class="form-control" placeholder="Direccion" required oninvalid="setCustomValidity('El campo direccion es obligatorio')" oninput="setCustomValidity('')"></textarea>


                    </div>  
                </td>
            </tr>
            <tr>
                <td>
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span>
                </td>
                <td>
                    <div class="col-md-8" >
                        <select class="form-control" id="region" name="region" required onchange=" return buscaProvincia()"  >
                            <option value="-1">Elegir Regi�n
                                <%    for (lugar region : cp.buscarregion()) {

                                        if (region.getId_lugar() == comuna) {%>
                            <option selected value="<%=region.getId_lugar()%>"><%=region.getNombre()%><%
                                        } else {%>
                            <option value="<%=region.getId_lugar()%>"><%=region.getNombre()%><%
                                    }

                                }
                                %>
                        </select>
                    </div> 
                </td>
                <td colspan="2">
                    <div id="provincia1">

                    </div> 
                </td>
                <td>
                    <div id="comuna1">


                    </div> 
                </td>
            </tr>
            <tr>
                <td> <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list-alt bigicon"></i></span></td>
                <td><div class="col-md-8" >
                        <select class="form-control" id="procedencia" name="procedencia" required>
                            <option  value="0">Elegir Consultorio de donde procede
                                <%
                                    for (lugar procedencia : cp.buscarProcedencia()) {

                                        if (procedencia.getId_lugar() == procedencia1) {%>
                            <option selected value="<%=procedencia.getId_lugar()%>"><%=procedencia.getNombre()%><%
                                        } else {%>
                            <option value="<%=procedencia.getId_lugar()%>"><%=procedencia.getNombre()%><%
                                    }

                                }
                                %>
                        </select>
                    </div></td>
                <td colspan="2"><div class="col-md-8" >
                        <select class="form-control" id="provision" name="provision" onchange="javascript:
                                                    var pre = document.forms['cita']['provision'].value;
                                            if (pre == 1) {

                                                document.getElementById('tramo').style.display = 'block';
                                            } else
                                                document.getElementById('tramo').style.display = 'none';
                                ">
                            <option value="0">Prevision

                                <% for (prevision pre : cp.buscarPrevicion()) {

                                        if (pre.getId_prevision() == tramo) {%>
                            <option selected  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                                <%} else {%>
                            <option  value="<%=pre.getId_prevision()%>"><%=pre.getNombre()%>
                                <% }
                                }%>

                        </select>
                    </div></td>
                <td><div class="col-md-8" >
                        <select class="form-control" id="tramo" name="tramo"  style=" display: none">
                            <option value="0">Tramo
                                <%

                                    for (prevision tra : cp.buscarTramo()) {

                                        if (tra.getId_prevision() == tramo) {%>
                            <option selected value="<%=tra.getId_prevision()%>"><%=tra.getNombre()%><%
                                        } else {%>
                            <option value="<%=tra.getId_prevision()%>"><%=tra.getNombre()%><%
                                    }

                                }
                                %>
                        </select>
                    </div></td>

            </tr>

        </table>



        <div class="form-group">
            <div class="col-md-12 text-center">
                <button type="button" id="asignar" onclick="javascritp: validar()" class="btn btn-primary btn-lg">Crear </button>

                <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
            </div>
        </div>


    </fieldset>

</form>




<script>
    Calendar.setup(
            {inputField: "txt_inicial", ifFormat: "%d/%m/%Y", button: "boton_fecha_inicio"}

    );

</script> 






<%
    }

%>