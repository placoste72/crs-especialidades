<%-- 
    Document   : Entrevista_Preoperatoria
    Created on : 07-05-2018, 16:38:30
    Author     : a
--%>

<%@page import="Modelos.atencion"%>
<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Modelos.entrevista_preoperatoria"%>
<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.lugar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<script src="../../public/js/encuesta.js" type="text/javascript"></script>


<%
    controlador_especialidad ce = new controlador_especialidad();

    String cita = request.getParameter("idcita");
    controlador_cita cc = new controlador_cita();

    cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
    /*buscar lo que escribio la tens*/
    String fechaComoCadena = "";
    String motivotexto = "";
    boolean tengoSC = cc.buscarsiExistecontrol(Integer.parseInt(cita));

    if (tengoSC == true) {
        atencion a = cc.buscarControlporIdcita(Integer.parseInt(cita));

        fechaComoCadena = a.getFormatofecha();
        motivotexto = a.getDetallereceta();
    }

    Integer idep = ce.idEntrevistaPreoperatoriaporIdCita(Integer.parseInt(cita));
    entrevista_preoperatoria ep = ce.buscarEntrevista(idep);

    Integer[] idpertinencia = {1, 0};
    String[] nombrepertinencia = {"SI", "NO"};

    Integer[] idcombo = {1, 0, -1};
    String[] combo = {"Si", "No", "Seleccione"};

%>
<script>
    function numeros(e)  // 1

    {

        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
            return true;

        return /\d/.test(String.fromCharCode(keynum));

    }
</script>
<style>
    table{
        font-size: 10px;
    }
    th, td {
        padding: 0px;
    }

</style>
<section  class="section">
    <nav class="nav1 ">
        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos del Paciente</th>
                </tr>
                <tr>
                    <th>RUT</th>
                    <th>Paciente</th>
                    <th>Edad</th>
                    <th>Sexo</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getMotivo_cancela()%></td>
                <td><%= c.getRut_paciente()%></td>
                <td><%= c.getTemporales2()%></td>
                <td><%= c.getTemporales1()%></td>


            </tr>
        </table>

        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="6" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos de la Citaciòn</th>
                </tr>
                <tr>
                    <th>Especialidad</th>
                    <th>Tipo de Cita</th>
                    <th>Diagnóstica</th>


                    <th>Fecha y Hora</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getTemporales()%> </td>
                <td><%= c.getTemporales3()%>  </td>
                <td><%= c.getTemporales4()%></td>


                <td><%= c.getMotivo()%></td>


            </tr>
        </table>
        <p class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; font-size: 10px; text-align: left">Datos otras atenciones</p>
        <table class="table-striped" >
            <tr>
                <th>Fecha</th>
                <th>Atencion</th>
                <th>Especialidad</th>
                <th>Profesional</th>
                <th>Detalle</th>
            </tr>
            <%for (atencion_clinica_tecnologo at : cc.BuscarAtencionesdeunPaciente(c.getMotivo_cancela())) {

            %>
            <tr>
                <td><%=at.getAuxiliarcinco()%></td>
                <td><%=at.getAuxiliarseis()%></td>
                <td><%=at.getAuxiliardos()%></td>
                <td><%=at.getAuxiliartres()%></td>
                <%if (at.getAuxiliarcuatro() == 1) {
                %>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 2) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 3) {%>
                <td><a onClick="window.open('<%=cc.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 4) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 5) {%>
                <td> </td>
                <%} else if (at.getAuxiliarcuatro() == 6) {%>
                <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"   >

                        Protocolo  </a> 

                    <%int ide = cc.idepicrisis(at.getId_atencion_tecnologo());%>
                    <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    >

                        Epicrisis  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 7) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 8) {%>
                <td><a  onClick="window.open('<%=cc.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenología', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 9) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%}%>
            </tr>
            <%

                }%>

        </table>

    </nav>
    <article class="article">
        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <div class="container">
            <form id="entrevistaproperatorio" name="entrevistaproperatorio"  action='<%=ce.getLocal()%>/registrarentrevista_preoperatoria' onsubmit="" >
                <div class="tab-content">
                    <fieldset>
                        <legend class="text-center header">Entrevista Preoperatoria de Enfermería</legend>


                        <table style="border-top:  #619fd8 2px solid; " >


                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <%if (c.getId_tipoatencion() == 1) {%>  
                            <tr>
                                <td colspan="2">
                                    <span class="col-md-8   col-md-offset-4 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >¿Es pertinente la derivación a su atención?</i></span>
                                </td>
                                <td>
                                    <div class="col-md-8">
                                        <select id="pertinencia" name="pertinencia" > 


                                            <option selected  value="-1"> Seleccionar </option>

                                            <% for (int i = 0; i < idpertinencia.length; ++i) {
                                                    if (idpertinencia[i] == ep.getPertinencia()) {%>
                                            <option selected value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                <%} else {%>

                                            <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                <% }
                                                    }
                                                %>

                                        </select>  
                                    </div>
                                </td>
                            </tr>
                            <%}%>

                            <input id="cita" name="cita" value="<%=cita%>" hidden>
                            <input id="identrevista" name="identrevista" value="<%=idep%>" hidden>
                            <input id="idatencioncita" name="idatencioncita" value="<%=c.getId_tipoatencion()%>" hidden>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Diagnostico:</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >
                                        <input type="text" style=" height: 100px" required oninvalid="setCustomValidity('El campo Diagnostico es obligatorio')" oninput="setCustomValidity('')"  rows="3" id="diagnosticoenfermera" name="diagnosticoenfermera"  class="form-control" placeholder="Diagnostico"  <%if ((!ep.getDiagnostico().equals("null")) && (ep.getDiagnostico() != "")) {%> value='<%=ep.getDiagnostico()%>' <%}%> >


                                    </div>  
                                </td>
                                <td style=" width: 20px"><span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Cirugía Propuesta:</i></span>

                                </td>
                                <td>

                                    <div class="col-md-12" id="detalle" >
                                        <input type="text" style=" height: 100px"  rows="3" id="cirugiapropuesta" <%if ((!ep.getCirugia_propuesta().equals("null")) && (ep.getCirugia_propuesta() != "")) {%> value='<%=ep.getCirugia_propuesta()%>' <%}%> name="cirugiapropuesta"  class="form-control" placeholder="Cirugia Propuesta" required oninvalid="setCustomValidity('El campo Cirugia Propuesta es obligatorio')" oninput="setCustomValidity('')"  >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>
                        <table>
                            <tr>
                                <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Peso:</i></span>



                                    <div class="col-md-6" id="detalle" >
                                        <input type="text" required oninvalid="setCustomValidity('El campo Peso es obligatorio')" oninput="setCustomValidity('')" id="peso" name="peso" value="<%=ep.getPeso()%>"  class="form-control" placeholder="Peso kg"  onchange="javascritp:

                                                        var peso, altura, imc;

                                                peso = document.getElementById('peso').value;
                                                altura = document.getElementById('talla').value;

                                                if (peso != '' && altura != '') {

                                                    altura = parseFloat(altura);

                                                    imc = parseFloat(peso / (altura * altura));
                                                    document.getElementById('imc').value = imc.toFixed(2);
                                                }" >


                                    </div>  
                                </td>
                                <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Talla:</i></span>



                                    <div class="col-md-6" id="detalle">
                                        <input type="text"  required  id="talla" name="talla"  oninvalid="setCustomValidity('El campo Talla es obligatorio')" oninput="setCustomValidity('')" value="<%=ep.getTalla()%>"  class="form-control" placeholder="Talla MT" onchange="javascritp:

                                                        var peso, altura, imc;

                                                peso = document.getElementById('peso').value;
                                                altura = document.getElementById('talla').value;

                                                if (peso != '' && altura != '') {

                                                    altura = parseFloat(altura);

                                                    imc = parseFloat(peso / (altura * altura));
                                                    document.getElementById('imc').value = imc.toFixed(2);
                                                }"  >


                                    </div>  
                                </td>
                                <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >IMC:</i></span>



                                    <div class="col-md-6" id="detalle" >
                                        <input type="text"  required oninvalid="setCustomValidity('El campo ICM es obligatorio')" oninput="setCustomValidity('')"   id="imc" name="imc" value="<%=ep.getImc()%>"  class="form-control" placeholder="IMC"  >


                                    </div>  
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >PA:</i></span>



                                    <div class="col-md-6" id="detalle" >
                                        <input type="text"  required oninvalid="setCustomValidity('El campo PA es obligatorio')" oninput="setCustomValidity('')"  id="pa" name="pa" value="<%=ep.getPa()%>"  class="form-control" placeholder="PA"  >


                                    </div>  
                                </td>
                                <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >F. CARDÌACA:</i></span>



                                    <div class="col-md-6" id="detalle" >
                                        <input type="text" required oninvalid="setCustomValidity('El campo F. CARDiACA: es obligatorio')" oninput="setCustomValidity('')"   id="cardiaca" name="cardiaca" value="<%=ep.getCardiaca()%>"  class="form-control" placeholder="F. CARDÌACA:"  >


                                    </div>  
                                </td>
                                <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Sat02:</i></span>



                                    <div class="col-md-6" id="detalle" >
                                        <input type="text"  required oninvalid="setCustomValidity('El campo Sat02 es obligatorio')" oninput="setCustomValidity('')" id="sat" name="sat" value="<%=ep.getSat()%>"  class="form-control" placeholder="Sat02"  >


                                    </div>  
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>

                        </table>
                        <table>
                            <tr>
                                <td style=" width: 100px"> <p class="letra" style=" font-size: 14px">Retrocontrol</p></td> <td>
                                    <input  type="checkbox" id="retrocontrol" name="retrocontrol" value="checkbox" onchange=" javascritp: var ret = document.getElementById('retrocontrol').checked;

                                            if (ret) {
                                                document.getElementById('retro').style.display = 'block';
                                            } else {
                                                document.getElementById('retro').style.display = 'none';
                                            }
                                            ">
                                </td>

                            </tr>
                            <tr>
                                <td colspan="4" >
                                    <table id="retro" name="retro" style=" display: none">



                                        <tr>
                                            <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >PA:</i></span>



                                                <div class="col-md-6" id="detalle" >
                                                    <input type="text"    id="paretro" name="paretro" <%if ((!ep.getParetro().equals("null")) && (ep.getParetro() != "")) {%> value='<%=ep.getParetro()%>' <%}%>  class="form-control" placeholder="PA"  >


                                                </div>  
                                            </td>
                                            <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >F. CARDÌACA:</i></span>



                                                <div class="col-md-6" id="detalle" >
                                                    <input type="text"  id="cardiacaretro" name="cardiacaretro" <%if ((!ep.getCardiacaretro().equals("null")) && (ep.getCardiacaretro() != "")) {%> value='<%=ep.getCardiacaretro()%>' <%}%>  class="form-control" placeholder="F. CARDÌACA:"  >


                                                </div>  
                                            </td>
                                            <td><span class="col-md-3   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Sat02:</i></span>



                                                <div class="col-md-6" id="detalle" >
                                                    <input type="text"   id="satretro" name="satretro" <%if ((!ep.getSatretro().equals("null")) && (ep.getCardiacaretro() != "")) {%> value='<%=ep.getSatretro()%>' <%}%>  class="form-control" placeholder="Sat02"  >


                                                </div>  
                                            </td>

                                        </tr>
                                        <tr><td> &nbsp;</td></tr>

                                    </table>
                                </td>
                            </tr>
                        </table>

                        <% if (ep.getRetrocontrol() == 1) {
                        %>
                        <script type="text/javascript">

                            document.getElementById('retrocontrol').checked = 1;
                            var ret = document.getElementById('retrocontrol').checked;

                            if (ret) {
                                document.getElementById('retro').style.display = 'block';
                            } else {
                                document.getElementById('retro').style.display = 'none';
                            }
                        </script>

                        <%
                            }%>

                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <tr><td><div class=" letra" style=" font-size: 20px">Antecedentes Morbidos-Ventilatorios:</div></td></tr>

                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td   ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Asma:</i></span>

                                    <div class="col-md-8">
                                        <select id="asma" name="asma" > 

                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getAsma() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>



                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Tuberculosis:</i></span>

                                    <div class="col-md-6">
                                        <select id="tuberculosis" name="tuberculosis" > 

                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getTuberculosis() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>




                                        </select>  
                                    </div>
                                </td>
                                <td ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Ira:</i></span>

                                    <div class="col-md-8">
                                        <select id="ira" name="ira" > 

                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getIra() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>


                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Rinitis Alergica:</i></span>

                                    <div class="col-md-8">
                                        <select id="renitis" name="renitis" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getRinitis() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>

                                <td ><span class="col-md-4 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Tabaco/<br>cant:</i></span>

                                    <div class="col-md-6" >
                                           <input type="text" onKeypress="return numeros(event)"  style=" width: 120px"   id="tabaco" name="tabaco"  class="form-control" placeholder="Cantidad" <%if (ep.getTabaco()
                                                       != 0) {%> value='<%=ep.getTabaco()%>' <%} else {%> value='0'<%}%> > 

                                    </div>




                                </td>
                                <td>

                                    <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 14px" >Desde:</i></span>

                                    <div class="col-md-6" >
                                           <input type="text" style=" width: 70px"   id="desde" name="desde"   class="form-control" placeholder="Desde" <%if ((!ep.getDesde()
                                                       .equals("null")) && (ep.getDesde() != "")) {%> value='<%=ep.getDesde()%>' <%}%>  > 

                                    </div> 
                                </td>
                                <td>
                                    <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 14px" >Años:</i></span>

                                    <div class="col-md-6" >
                                           <input type="text" style=" width: 60px"   id="annos" name="annos" <%if ((!ep.getAnos()
                                                       .equals("null")) && (ep.getAnos() != "")) {%> value='<%=ep.getAnos()%>' <%} else {%> value="0" <%}%> class="form-control" placeholder="Años"  > 

                                    </div>
                                </td>
                                <td>
                                    <span class="col-md-4 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 14px" >IPA:</i></span>

                                    <div class="col-md-6" >
                                        <input type="text" style=" width: 60px"   id="ipa" name="ipa"  class="form-control" placeholder="IPA" value='<%=ep.getIpa()%>'  > 

                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Descripcion</i></span>
                                </td>
                                <td>
                                    <div class="col-md-8" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="descripcion_ventilatorios" name="descripcion_ventilatorios" <%if ((!ep.getDescripcion_ventilatorios()
                                                           .equals("null")) && (ep.getDescripcion_ventilatorios() != "")) {%> value='<%=ep.getDescripcion_ventilatorios()%>' <%}%>  class="form-control" placeholder="Descripcion" >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <tr><td><div class=" letra" style=" font-size: 20px ">Antecedentes Morbidos - Metabólicos/Generales</div></td></tr>

                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td   ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Diabetes M:</i></span>

                                    <div class="col-md-6">
                                        <select id="diabetes" name="diabetes" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getDiabetes() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>


                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Obesidad:</i></span>

                                    <div class="col-md-6">
                                        <select id="obesidad" name="obesidad" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getObesidad() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                                <td colspan="2"   ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >RGE:</i></span>

                                    <div class="col-md-6">
                                        <select id="rge" name="rge" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getRge() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size:14px" >Hipo/ Hipertiroidismo:</i></span>

                                    <div class="col-md-6">
                                        <select id="hipo" name="hipo" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getHipo() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Ulcera-gastritis:</i></span>

                                    <div class="col-md-6">
                                        <select id="ulcera" name="ulcera" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getUlcera() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td  ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Alergias:</i></span>

                                    <div class="col-md-8">
                                        <select id="alergias" name="alergias" onchange="javascript: var ale = document.getElementById('alergias').value;


                                                if (ale == 1) {
                                                    document.getElementById('cualesalergias').style.backgroundColor = '#ffff00';
                                                } else {
                                                    document.getElementById('cualesalergias').style.backgroundColor = '';

                                                }" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getAlergias() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>


                                <td colspan="2">
                                    <span class="col-md-3 col-md-offset-1 "><i class="fa  bigicon" style=" font-size: 14px" >Cuales:</i></span>

                                    <div class="col-md-8" >
                                           <input type="text" style=" height: 80px; background-color: #ebedee"    id="cualesalergias" name="cualesalergias" <%if ((!ep.getCualesalergias()
                                                       .equals("null")) && (ep.getCualesalergias() != "")) {%> value='<%=ep.getCualesalergias()%>' <%}%>  class="form-control" placeholder="Alergias"  > 

                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Descripcion</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="descripcion_metabolicos"  name="descripcion_metabolicos" <%if ((!ep.getDescripcion_metabolicos()
                                                           .equals("null")) && (ep.getDescripcion_metabolicos() != "")) {%> value='<%=ep.getDescripcion_metabolicos()%>' <%}%>  class="form-control" placeholder="Descripcion" >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <tr><td><div class=" letra" style=" font-size: 20px ">Antecedentes Morbidos - Cardiovasculares</div></td></tr>

                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Coronario:</i></span>

                                    <div class="col-md-6">
                                        <select id="coronario" name="coronario" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getCoronario() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>



                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Arritmias:</i></span>

                                    <div class="col-md-6">
                                        <select id="arritmias" name="arritmias" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getArritmias() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>


                                        </select>  
                                    </div>
                                </td>
                                <td colspan="2"><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Coagulapatía:</i></span>

                                    <div class="col-md-6">
                                        <select id="coagulapatia" name="coagulapatia" > 

                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getCoagulopatia() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Valvular:</i></span>

                                    <div class="col-md-6">
                                        <select id="valvular" name="valvular" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getValvular() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Marcapasos:</i></span>

                                    <div class="col-md-6">
                                        <select id="marcapasos" name="marcapasos" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getMarcapasos() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Dislipidemia:</i></span>

                                    <div class="col-md-6">
                                        <select id="dislipidemia" name="dislipidemia" >
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getDislipidemia() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >HTA:</i></span>

                                    <div class="col-md-6">
                                        <select id="hta" name="hta" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getHta() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Descripcion</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="descripcion_cardiovasculares"  name="descripcion_cardiovasculares" <%if ((!ep.getDescripcion_cardiovasculares()
                                                           .equals("null")) && (ep.getDescripcion_cardiovasculares() != "")) {%> value='<%=ep.getDescripcion_cardiovasculares()%>' <%}%>   class="form-control" placeholder="Descripcion" >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <tr><td><div class=" letra" style=" font-size: 20px">Antecedentes Morbidos: Neuro - Psiquiátricos</div></td></tr>

                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td   ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >ECV:</i></span>

                                    <div class="col-md-6">
                                        <select id="ecv" name="ecv" > 

                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getEcv() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-6 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Depresión:</i></span>

                                    <div class="col-md-2">
                                        <select id="depresion" name="depresion" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getDepresion() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>
                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Esquizofrenia:</i></span>

                                    <div class="col-md-6">
                                        <select id="esquizofrenia" name="esquizofrenia" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getEsquizofrenia() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>

                            </tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>

                                <td  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Epilepsia:</i></span>

                                    <div class="col-md-6">
                                        <select id="epilepsia" name="epilepsia" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getEpilepsia() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                                <td   ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Alcohol:</i></span>

                                    <div class="col-md-6">
                                        <select id="alcohol" name="alcohol" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getAlcohol() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                                <td ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Especificación de Alcohol:</i></span>

                                    <div class="col-md-6">
                                           <input type="text" style="height: 80px ; width: 100%" <%if ((!ep.getOtrasenfpsiq()
                                                       .equals("null")) && (ep.getOtrasenfpsiq() != "")) {%> value='<%=ep.getOtrasenfpsiq()%>' <%}%>  placeholder="Especificación  de Alcohol" id="especificaciondealcohol" name="especificaciondealcohol"> 
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"  ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Otro Enf.Psiq:</i></span>

                                    <div class="col-md-6">
                                           <input type="text" style="height: 80px; width: 310px" <%if ((!ep.getOtrasenfpsiq()
                                                       .equals("null")) && (ep.getOtrasenfpsiq() != "")) {%> value='<%=ep.getOtrasenfpsiq()%>' <%}%>  placeholder="Otras Enf. Psiq" id="otraenfermedad" name="otraenfermedad"> 
                                    </div>
                                </td>

                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Descripcion</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="descripcionneuro"  name="descripcionneuro" <%if ((!ep.getDescripcionneuro()
                                                           .equals("null")) && (ep.getDescripcionneuro() != "")) {%> value='<%=ep.getDescripcionneuro()%>' <%}%>   class="form-control" placeholder="Descripcion" >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <tr><td><div class=" letra" style=" font-size:  20px ">Antecedentes Morbidos: Nefro- Urinarios</div></td></tr>

                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Nefropatía:</i></span>

                                    <div class="col-md-6">
                                        <select id="nefropatia" name="nefropatia" > 

                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getNefropatia() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>

                                        </select>  
                                    </div>
                                </td>
                                <td><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Uropatía Obstructiva:</i></span>

                                    <div class="col-md-6">
                                        <select id="uropatia" name="uropatia" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getUropatiaobstructiva() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>
                                <td colspan="2" ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size:14px" >Infección Urinaria:</i></span>


                                    <div class="col-md-6">
                                        <select id="infeccionurinaria" name="infeccionurinaria" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getInfeccionurinaria() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>

                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Descripcion</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="descripcion_urinarios"   name="descripcion_urinarios" <%if ((!ep.getDescripcion_urinarios()
                                                           .equals("null")) && (ep.getDescripcion_urinarios() != "")) {%> value='<%=ep.getDescripcion_urinarios()%>' <%}%>   class="form-control" placeholder="Descripcion" >


                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Otros Antecedentes Morbidos</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="otrosantecedentesmorbidos"   name="otrosantecedentesmorbidos" <%if ((!ep.getOtrosantecedentesmorbidos()
                                                           .equals("null")) && (ep.getOtrosantecedentesmorbidos() != "")) {%> value='<%=ep.getOtrosantecedentesmorbidos()%>' <%}%>  class="form-control" placeholder="otros antecedentes morbidos" >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>               

                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <tr><td><div class=" letra" style="font-size: 20px">Antecedentes Morbidos: Quirúrgicos</div></td></tr>

                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Intervención:</i></span>

                                    <div class="col-md-6">
                                        <select id="intervencion" name="intervencion" > 

                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getIntervencion() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>
                                <td><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Antecedentes Anestesicos:</i></span>

                                    <div class="col-md-6">
                                        <select id="anastesico" name="anastesico" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getAntecendetes_anestesicos() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>
                                <td colspan="2" ><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Especificar:</i></span>


                                    <div class="col-md-6">
                                           <input type="text" style="height: 80px" <% if ((!ep.getEspecificar()
                                                       .equals("null")) && (ep.getEspecificar() != "")) {%> value='<%=ep.getEspecificar()%>' <%}%> id="otraanastesico" placeholder="Especificar" name="otraanastesico"> 
                                    </div>
                                </td>

                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style=" width: 20px">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Descripcion</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="descripcion_antecedentes"    name="descripcion_antecedentes" <%if ((!ep.getDescripcion_antecedentes()
                                                           .equals("null")) && (ep.getDescripcion_antecedentes() != "")) {%> value='<%=ep.getDescripcion_antecedentes()%>' <%}%> placeholder="Descripcion" >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>
                        <%
                            boolean tengoexamenes = ce.buscarsitengoexamenesdeEntrevista(idep);
                            lugar l = new lugar();
                            String hematocrito = "";
                            String rectoblaco = "";
                            String plaquetas = "";
                            String gruporh = "";
                            String glicemia = "";
                            String buncreat = "";
                            String nak = "";
                            String orinacompleta = "";
                            String protrombina = "";
                            String ttpk = "";
                            String ecg = "";
                            String otros = "";
                            if (tengoexamenes == true) {
                                l = ce.buscarexamentesdePARAREGISTROEntrevista(idep);
                                hematocrito = l.getComuna();
                                rectoblaco = l.getDescripcion();
                                plaquetas = l.getDetalle_de_la_derivacion();
                                gruporh = l.getDiagnostico();
                                glicemia = l.getDireccion();
                                buncreat = l.getEdad();
                                nak = l.getEmail();
                                orinacompleta = l.getExamenesrealizados();
                                protrombina = l.getFechanacimiento();
                                ttpk = l.getFolio();
                                ecg = l.getIndicaciones();
                                otros = l.getNombre();
                            }

                        %>       

                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="6"> &nbsp;</td></tr>
                            <tr><td style="width: 30px"><div class=" letra " style=" font-size: 20px">Examenes</div></td></tr>
                            <tr>


                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Hemotocrito:</i></span>
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <input type="text"  style=" height: 71px; width:146%" rows="2"  id="hemotocrito" name="hemotocrito"   value='<%= hematocrito%>'   class="form-control" placeholder="Hemotocrito (%)"  > 


                                    </div>  
                                </td>

                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Glicemia:</i></span>
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%"  rows="2" id="glicemia" name="glicemia"  value='<%=glicemia%>'  class="form-control" placeholder="Glicemia (mg/dl)"  >


                                    </div>  
                                </td>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Protrobina(%):</i></span>
                                </td>
                                <td> 


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%" rows="2" id="protobina" name="protobina"  value='<%=protrombina%>'  class="form-control" placeholder="Protrombina(%)"  >


                                    </div>  
                                </td>

                            </tr> 
                            <tr><td> &nbsp;</td></tr>
                            <tr>


                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Recto Blancos:</i></span>
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%" rows="2" id="rectoblancos" name="rectoblancos"  value='<%=rectoblaco%>'   class="form-control" placeholder="Recto Blancos (10^3/µL)"  >


                                    </div>  
                                </td>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >BUN/Creat:</i></span>
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <input type="text"  rows="2" style=" height: 71px; width: 146%" id="bun" name="bun"  value='<%=buncreat%>'   class="form-control" placeholder="BUN/Creat (mg/dL/0,8)"  >


                                    </div>  
                                </td>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >TTPK:</i></span>
                                </td>
                                <td> 


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%"  rows="2" id="ttpk" name="ttpk" value='<%=ttpk%>'   class="form-control" placeholder="TTPK (Segundos)"  >


                                    </div>  
                                </td>

                            </tr> 
                            <tr><td> &nbsp;</td></tr>
                            <tr>


                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Plaquetas:</i></span>
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%"  rows="2" id="plaquetas" name="plaquetas" value='<%=plaquetas%>'   class="form-control" placeholder="Plaquetas (10^3/µL)"  >


                                    </div>  
                                </td>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Na/K*:</i></span>
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <input type="text"  style=" height: 71px; width: 146%" rows="2" id="nak" name="nak"  value='<%=nak%>'   class="form-control" placeholder="Na/K*"  >


                                    </div>  
                                </td>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >ECG:</i></span>
                                </td>
                                <td> 


                                    <div class="col-md-12">
                                        <input type="text"  style=" height: 71px; width: 146%" rows="2" id="ecg" name="ecg" value='<%=ecg%>'  class="form-control" placeholder="ECG"  >


                                    </div>  
                                </td>

                            </tr> 
                            <tr><td> &nbsp;</td></tr>
                            <tr>

                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Grupo/RH:</i></span>
                                </td>

                                <td>


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%"   rows="2" id="gruporh" name="gruporh"  value='<%=gruporh%>'  class="form-control" placeholder="Grupo/ RH"  >


                                    </div>  
                                </td>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Orina Completa:</i></span>
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%"  rows="2" id="orinacompleta" name="orinacompleta"  value='<%=orinacompleta%>'  class="form-control" placeholder="Orina Completa"  >


                                    </div>  
                                </td>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >HbA1c</i></span>
                                </td>
                                <td> 


                                    <div class="col-md-12">
                                        <input type="text" style=" height: 71px; width: 146%"  rows="2" id="otrosexamenes" name="otrosexamenes"  value='<%=otros%>'   class="form-control" placeholder="HbA1c"  >


                                    </div>  
                                </td>

                            </tr>
                            <table>
                                <tr>
                                    <td style=" width: 20%">
                                        <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Descripcion</i></span>
                                    </td>
                                    <td>
                                        <div class="col-md-12" id="detalle" >

                                                   <input type="text" style=" height: 91px; width: 100%"  rows="3" id="descripcion_examenes"    name="descripcion_examenes" <%if ((!ep.getDescripcion_examenes()
                                                               .equals("null")) && (ep.getDescripcion_examenes() != "")) {%> value='<%=ep.getDescripcion_examenes()%>' <%}%>  placeholder="Descripcion" >


                                        </div>  
                                    </td>
                                </tr>
                                <tr><td> &nbsp;</td></tr>
                            </table>

                        </table>
                        <table style="border-top:  #619fd8 2px solid; " >
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <input id="cita" name="cita" value="" hidden>
                            <tr>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Tratamiento:</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >
                                           <input type="text"  style=" height: 91px; width: 100%" id="tratamiento" <%if ((!ep.getTratamieto()
                                                       .equals("null")) && (ep.getTratamieto() != "")) {%> value='<%=ep.getTratamieto()%>' <%}%> required oninvalid="setCustomValidity('El campo Tratamiento es obligatorio')" oninput="setCustomValidity('')" name="tratamiento"   class="form-control" placeholder="Indicar Nombre medicamento, dosis, frecuencia, via" >


                                    </div>  
                                </td>
                            </tr>
                            <tr><td> &nbsp;</td></tr>
                        </table>
                        <table style="border-top:  #619fd8 2px solid; " >
                            <tr><td colspan="4"> &nbsp;</td></tr>
                            <tr>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Red de Apoyo:</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                        <input type="text" style=" height: 91px; width: 100%"  rows="3" id="reddeapoyo"   name="reddeapoyo"  <%if ((!ep.getReddeapoyo().equals("null")) && (ep.getReddeapoyo() != "")) {%>value='<%=ep.getReddeapoyo()%>' <%}%> class="form-control" placeholder="Red de Apoyo" >


                                    </div>  
                                </td>
                            </tr>

                            <tr>
                                <td style=" width: 20%">
                                    <span class="col-md-1   col-md-offset-1 "><i class="fa fa-edit bigicon"  style=" font-size: 14px" >Observaciones:</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12" id="detalle" >

                                               <input type="text" style=" height: 91px; width: 100%"  rows="3" id="observaciones"  <%if ((!ep.getObservaciones()
                                                           .equals("null")) && (ep.getObservaciones() != "")) {%> value='<%=ep.getObservaciones()%>' <%}%> name="observaciones" required oninvalid="setCustomValidity('El campo Observaciones es obligatorio')" oninput="setCustomValidity('')"  class="form-control" placeholder="¿Requiere evaluacion por Medico?,¿Se puedo operar?" >


                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td><span class="col-md-5 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 14px" >Se da pase de enfermería para cirugía propuesta:</i></span>
                                </td>
                                <td>
                                    <div class="col-md-12">
                                        <select id="pase" name="pase" > 
                                            <% for (int i = 0; i < combo.length; ++i) {
                                                    if (ep.getPase() == idcombo[i]) {%>
                                            <option selected  value="<%=idcombo[i]%>"><%=combo[i]%> </option>
                                            <% } else {%>
                                            <option   value="<%=idcombo[i]%>"><%=combo[i]%></option>
                                            <% }
                                                }%>
                                        </select>  
                                    </div>
                                </td>   
                            </tr>
                        </table>
                        <table style="border-top:  #619fd8 2px solid; ">
                            <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Solicitud de Control</div></td></tr>

                        </table>
                        <table style=" width: 100%">

                            <tr>
                                <td colspan="6">
                                    <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-check bigicon" style="font-size:18px">Fecha Control </i></span>
                                    <div class="col-md-8">
                                        <input type="text" maxlength="13" size="12" id="fechacontrol" name="fechacontrol" value="<%=fechaComoCadena%>" ><br> </td>
                                    </div>  
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                    <div class="col-md-8">
                                        <textarea type="text"  rows="3" id="motivocontrol" name="motivocontrol"  class="form-control" placeholder="Motivo Control"  ><%=motivotexto%></textarea>


                                    </div>  
                                </td>
                            </tr>
                        </table>

                        <br>
                        <br>
                        <br>
                        <table>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button type="button" onclick=" javascript: return validar();" class="btn btn-primary btn-lg">Finalizar </button>

                                            <button type="button" onclick=" javascript: document.entrevistaproperatorio.action = '<%=ce.getLocal()%>/guardarEntrevistaPreoperatoria';
                                                    document.entrevistaproperatorio.submit()" class="btn btn-primary btn-lg">Guardar </button>

                                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                        </div>
                                    </div> 
                                </td>
                            </tr>


                        </table>



                    </fieldset>
                </div>
            </form>
        </div>
    </article>
</section>
<jsp:include page="../comunes/Footer.jsp"/>