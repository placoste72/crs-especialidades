<%-- 
    Document   : AtencionRadiologia
    Created on : 06-08-2018, 13:27:04
    Author     : a
--%>


<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Controlador.controlador_atencion"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
<script>
    function validar() {
        var bazo = document.getElementById('bazo').value;
        var aorta = document.getElementById('aorta').value;
        var otrobrazo1 = document.getElementById('otrobrazo1').value;
        var otroaorta2 = document.getElementById('otroaorta2').value;
        if (bazo == 0 || aorta == 0) {
            Alert.render("Debe Seleccionar Bazo y Oarta,Complete los datos para continuar !!");
            return false;
        }

        if (bazo == -1 && otrobrazo1 == "") {
            Alert.render("Coloco Otro Bazo debe indicar cual,Complete los datos para continuar !!");
            return false;
        }
        if (aorta == -1 && otroaorta2 == "") {
            Alert.render("Coloco Otro Aorta debe indicar cual,complete los datos para continuar !!");
            return false;
        }



    }

</script>
<%
    String cita = request.getParameter("cita");
    controlador_cita cc = new controlador_cita();
    cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
    controlador_atencion ca = new controlador_atencion();
%>
<style>
    table{
        font-size: 10px;
    }
    th, td {
        padding: 0px;
    }

</style>
<section  class="section">
    <nav class="nav1 ">
        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos del Paciente</th>
                </tr>
                <tr>
                    <th>RUT</th>
                    <th>Paciente</th>
                    <th>Edad</th>
                    <th>Sexo</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getMotivo_cancela()%></td>
                <td><%= c.getRut_paciente()%></td>
                <td><%= c.getTemporales2()%></td>
                <td><%= c.getTemporales1()%></td>


            </tr>
        </table>

        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="6" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos de la Citaci�n</th>
                </tr>
                <tr>
                    <th>Especialidad</th>
                    <th>Tipo de Cita</th>
                    <th>Diagn�stica</th>


                    <th>Fecha y Hora</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getTemporales()%> </td>
                <td><%= c.getTemporales3()%>  </td>
                <td><%= c.getTemporales4()%></td>


                <td><%= c.getMotivo()%></td>


            </tr>
        </table>
        <p class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; font-size: 10px; text-align: left">Datos otras atenciones</p>
        <table class="table-striped" >
            <tr>
                <th>Fecha</th>
                <th>Atencion</th>
                <th>Especialidad</th>
                <th>Profesional</th>
                <th>Detalle</th>
            </tr>
            <%for (atencion_clinica_tecnologo at : cc.BuscarAtencionesdeunPaciente(c.getMotivo_cancela())) {

            %>
            <tr>
                <td><%=at.getAuxiliarcinco()%></td>
                <td><%=at.getAuxiliarseis()%></td>
                <td><%=at.getAuxiliardos()%></td>
                <td><%=at.getAuxiliartres()%></td>
                <%if (at.getAuxiliarcuatro() == 1) {
                %>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 2) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 3) {%>
                <td><a onClick="window.open('<%=cc.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 4) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 5) {%>
                <td> </td>
                <%} else if (at.getAuxiliarcuatro() == 6) {%>
                <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"   >

                        Protocolo  </a> 

                    <%int ide = cc.idepicrisis(at.getId_atencion_tecnologo());%>
                    <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    >

                        Epicrisis  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 7) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 8) {%>
                <td><a  onClick="window.open('<%=cc.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenolog�a', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 9) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%}%>
            </tr>
            <%

                }%>

        </table>

    </nav>
    <article class="article">

        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <div class="container">
            <form id="hojadiaria" name="hojadiaria" action="<%=cc.getLocal()%>registrarAtencionRadiologia" method="post" onsubmit="return validar()">
                <div class="tab-content">
                    <fieldset>
                        <legend class="text-center header">Atencion en Box Radiolog�a</legend>



                        <table style="border-top:  #619fd8 2px solid; ">
                            <input hidden id="cita" name="cita" value="<%=cita%>">
                            <tr>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">Hipotesis Diagnostica:</i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="hipotesis_diagnostica" name="hipotesis_diagnostica"  class="form-control" placeholder="Hipotesis Diagnostica"   ></textarea>

                                    </div>  
                                <td>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">Profesional Solicitante:</i></span> 
                                </td>
                                <td colspan="2">


                                    <div class="col-md-12">
                                        <input id="profesional_solicitante" name="profesional_solicitante" placeholder="Profesional Solicitante">
                                    </div>  
                                <td>

                            </tr>
                            <tr>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">H�gado:</i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="higado" name="higado"  class="form-control" placeholder="H�gado" required oninvalid="setCustomValidity('El campo H�gado es obligatorio')" oninput="setCustomValidity('')"  ></textarea>


                                    </div>  
                                </td>

                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">V�a Biliar</i></span> 
                                </td>
                                <td colspan="2">


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="viabiliar" name="viabiliar"  class="form-control" placeholder="V�a Biliar" required oninvalid="setCustomValidity('El campo V�a Biliar es obligatorio')" oninput="setCustomValidity('')"  ></textarea>


                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">Ves�cula</i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="vesicula" name="vesicula"  class="form-control" placeholder="Ves�cula" required oninvalid="setCustomValidity('El campo Ves�cula es obligatorio')" oninput="setCustomValidity('')"  ></textarea>


                                    </div>  
                                </td>

                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">P�ncreas</i></span> 
                                </td>
                                <td colspan="2">


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="pancreas" name="pancreas"  class="form-control" placeholder="P�ncreas" required oninvalid="setCustomValidity('El campo P�ncreas es obligatorio')" oninput="setCustomValidity('')"  ></textarea>


                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">Bazo</i></span> 
                                </td>
                                <td>

                                    <div class="col-md-8">
                                        <select name="bazo" id="bazo" class="form-control" onchange="javascript:

                                                        var bazo = document.getElementById('bazo').value;
                                                if (bazo == -1) {


                                                    document.getElementById('otrobazo').style.display = 'block';
                                                } else
                                                    document.getElementById('otrobazo').style.display = 'none';" >
                                            <option  value="0" selected>BAZO
                                                <%for (lugar lu : ca.buscarbazoActivos()) {%>
                                            <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                <% }%>

                                        </select>
                                    </div>

                                    <div class="col-md-12" id="otrobazo" style=" display: none">
                                        <textarea type="text"  rows="1" id="otrobrazo1" name="otrobrazo1"  class="form-control" placeholder="Otro Bazo"  ></textarea>


                                    </div>  

                                </td>


                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">Aorta</i></span> 
                                </td>
                                <td colspan="2">

                                    <div class="col-md-8">
                                        <select name="aorta" id="aorta" class="form-control" onchange="javascript:

                                                        var aorta = document.getElementById('aorta').value;
                                                if (aorta == -1) {


                                                    document.getElementById('otroaorta').style.display = 'block';
                                                } else
                                                    document.getElementById('otroaorta').style.display = 'none';" >
                                            <option  value="0" selected>AORTA
                                                <%for (lugar lu : ca.buscarAortaActivos()) {%>
                                            <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                <% }%>

                                        </select>
                                    </div>

                                    <div class="col-md-12" id="otroaorta" style=" display: none">
                                        <textarea type="text"  rows="1" id="otroaorta2" name="otroaorta2"  class="form-control" placeholder="Otro Aorta"  ></textarea>


                                    </div>  

                                </td>   
                            </tr>
                            <tr>
                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">Ri�ones</i></span> 
                                </td>
                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="rinones" name="rinones"  class="form-control" placeholder="Ri�ones" required oninvalid="setCustomValidity('El campo Ri�ones es obligatorio')" oninput="setCustomValidity('')"  ></textarea>


                                    </div>  
                                </td>

                                <td style="width: 10px">
                                    <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style="font-size: 16px">Conclusi�n</i></span> 
                                </td>
                                <td colspan="2">


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="3" id="conclusion" name="conclusion"  class="form-control" placeholder="Conclusi�n" required oninvalid="setCustomValidity('El campo Conclusi�n es obligatorio')" oninput="setCustomValidity('')"  ></textarea>


                                    </div>  
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary btn-lg">Guardar </button>

                                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                        </div>
                                    </div> 
                                </td>
                            </tr>


                        </table>
                    </fieldset>
                </div>
            </form>
        </div>
    </article>
</section>
<jsp:include page="../comunes/Footer.jsp"/>