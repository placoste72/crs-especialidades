<%-- 
    Document   : Documentos
    Created on : 17-07-2017, 13:54:31
    Author     : a
--%>


<%@page import="Modelos.hojadiaria"%>
<%@page import="Controlador.controlador_especialidad"%>
<!DOCTYPE html>
<head>


    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


    <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

    <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
    <link href="../../public/css/estilos.css" rel="stylesheet">
    <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
    <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
    <script src="../../public/js/crs.js" type="text/javascript"></script>

    <script src="../../public/js/jquery.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">-->

    <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
    <link href="../../public/css/estilos.css" rel="stylesheet">
    <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
    <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
    <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>

</head>

<%

    controlador_especialidad ce = new controlador_especialidad();
    String hojadiario = request.getParameter("hojadiario");
    hojadiaria hd = ce.BuscarHojaDiaria(Integer.parseInt(hojadiario));

%>
<div class="container"> 
    <form name="buscarsic" class="form-horizontal" id="buscarsic" action='' method="post"  >

        <br>
        <fieldset>
            <legend class="text-center header">Documentos a Imprimir</legend>
            <br>
            <br>
            <table class="table table-striped" style="width: 100%"  >

                <thead>
                    <%if ((hd.getSic() == 1) && (hd.getConstancia() == 1) && (hd.isSolicitudpabellon() == true)) {%>  
                <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hojadiario%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hojadiario%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hojadiario%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico <br> Solicitud de Pabellón Quirurgico <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hojadiario%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento<br> Informado Intervenciones Quirúrgicas  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hojadiario%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>

                <%} else if ((hd.getSic() == 1) && (hd.getConstancia() == 1)) {%>
                <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hojadiario%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hojadiario%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hojadiario%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                        <%} else if ((hd.getSic() == 1) && (hd.isSolicitudpabellon() == true)) {%>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hojadiario%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento <br>Informado Intervenciones Quirúrgicas  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hojadiario%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>
                <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hojadiario%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                        <%} else if ((hd.getConstancia() == 1) && (hd.isSolicitudpabellon() == true)) {%>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hojadiario%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento<br> Informado Intervenciones Quirúrgicas  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hojadiario%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hojadiario%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hojadiario%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                        <%} else if (hd.getConstancia() == 1) {%>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hojadiario%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hojadiario%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
                        <%} else if ((hd.getSic() == 1)) {%>
                <th><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hojadiario%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
                        <%} else if (hd.isSolicitudpabellon() == true) {%>
                <th><p  class="letra" > Solicitud de Pabellón Quirurgico  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hojadiario%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
                <th><p  class="letra" > Formulario de consentimiento <br> Informado Intervenciones Quirúrgicas <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hojadiario%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>

                <%}%>
                <th></th>
                </tr>


                </thead>
            </table>  
        </fieldset>
    </form>
    <button type="button" style="margin-left:  500px" class="btn btn-primary" onclick="javascript : location.href = '<%=ce.getLocallink()%>Especialidades/ListaPacientes.jsp';">Atender Paciente</button>
</div> 
<br>
<br>
<br>
<br>
<br>
<br>


