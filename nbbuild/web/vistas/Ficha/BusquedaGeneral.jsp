<%-- 
    Document   : BuscarPaciente
    Created on : 26-01-2017, 03:50:23 PM
    Author     : Informatica
--%>


<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<script src="../../public/js/busquedageneral.js" type="text/javascript"></script>
<script>
 

    $('body').keyup(function (e) {
        if (e.keyCode == 13) {

            return buscaPaciente();
        }


    });
   

</script>
<body>
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <div class="container"> 

        <form  id="buscarpaciente" name="buscarpaciente"  method="post" onsubmit="return false;"  >
            <fieldset>
                <legend class="text-center header">Ficha Paciente</legend>
                <div class="form-group">

                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                    <div class="col-md-4">

                        <input type="text" id="rutpaciente" name="rutpaciente" style="text-transform:uppercase; cursor:pointer;" onmouseover="showToolTip(event, '�Escriba su RUT sin puntos ni guiones!');
                                return false" onkeyup="formateaRut(this.value);" autocomplete=off onclick="javascript:document.getElementById('rutpaciente').select();" maxlength="12" class="form-control" placeholder="Rut Paciente">
                        <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                        <input value="d" name="txtDV" id="txtDV" type="hidden">

                    </div>

                    <button type="button" class="btn  btn-primary " onclick="javascript : buscaPaciente()">Buscar</button>
                </div>
                <div id="busqueda">            

                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </fieldset>
        </form>
    </div>

</body>
<jsp:include page="../comunes/Footer.jsp"/>