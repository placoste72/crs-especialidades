<%-- 
    Document   : HitosHistorial
    Created on : 19-07-2018, 13:02:40
    Author     : a
--%>

<%@page import="Controlador.controlador_paciente"%>
<%@page import="Modelos.paciente"%>
<%@page import="Controlador.controlador_cita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../comunes/headerwindows.jsp"/>
<!DOCTYPE html>
<%
    String paciente = request.getParameter("rut");
    controlador_paciente cp = new controlador_paciente();

%>
<html>
  
    <body>
         <legend class="text-center header">Hitos y  Historiales</legend>
        <table class="table table-striped" style="width: 100%">
           
            <tr>
                
                <td>
                    <table class="table table-striped" style="width: 100%">
                        <thead>
                             <tr><td colspan="2"><div class=" letra">Hitos</div></td></tr>
                            <tr >

                                <th>Fecha</th>
                                <th>Descripcion</th>

                                <th>Tipo Llamada</th>
                                <th>Tipo Carta</th>
                                <th>Tipo Examen</th>
                                <th>Funcionario que Registro</th>


                            </tr>
                        </thead>
                        <%   for (paciente temp : cp.buscarHitosdePaciente(paciente)) {
                        %>
                        <tr>
                            <td><%=temp.getTemporal3()%></td>
                            <td><%=temp.getNombre()%></td>
                            <td><%=temp.getApellido_moderno()%></td>
                            <td><%=temp.getApellido_paterno()%></td>
                            <td><%=temp.getDireccion()%></td>
                            <td><%=temp.getTemporal2()%></td>

                        </tr>
                        <%}%>
                    </table>  
                </td>
                <td style="BORDER-LEFT:#619fd8 2px solid;">
                   
                    <table  class="table table-striped" style="width: 100%">
                        <thead>
                             <tr><td colspan="2"><div class=" letra">Historial</div></td></tr>
                            <tr >
                                <th>Fecha Ingreso</th>
                                <th>Contacto 1</th>
                                <th>Contacto 2</th>

                                <th>Direccion</th>
                                <th>Region </th>
                                <th>Provincia</th>
                                <th>Comuna</th>

                                <th>Funcionario Que registro</th>


                            </tr>
                        </thead>
                        <%
                            for (paciente temp : cp.buscarHistorialpacienteAntiguos(paciente)) {
                        %>

                        <tr>
                            <td><%=temp.getTemporal3()%></td>
                            <td><%=temp.getContacto1()%></td>
                            <td><%=temp.getContacto2()%></td>
                            <td><%=temp.getDireccion()%></td>

                            <td><%=temp.getApellido_moderno()%></td>
                            <td><%=temp.getApellido_paterno()%></td>
                            <td><%=temp.getNombre()%></td>


                            <td><%=temp.getTemporal2()%></td>


                        </tr>
                        <%}%>
                    </table>
                   
                </td>
            </tr>

        </table>
    </body>
</html>
