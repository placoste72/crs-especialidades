<%-- 
    Document   : AtencionenProceso
    Created on : 04-08-2017, 8:34:44
    Author     : a
--%>

<%@page import="Modelos.lugar"%>
<%@page import="Controlador.General"%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Modelos.paciente"%>


<!DOCTYPE html>


<head>


    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>


    <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>

    <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
    <link href="../../public/css/estilos.css" rel="stylesheet">
    <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
    <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/crs.css" rel="stylesheet" type="text/css"/>
    <script src="../../public/js/crs.js" type="text/javascript"></script>

    <script src="../../public/js/jquery.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">-->

    <script src="../../public/js/bootstrap-select.js" type="text/javascript"></script>
    <link href="../../public/css/estilos.css" rel="stylesheet">
    <link href="../../public/css/bootstrap.min.css" rel="stylesheet" > 
    <link href="../../public/css/menu.css" rel="stylesheet" type="text/css"/>
    <link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
    <link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>

</head>
<%    int proceden = 0;
    String atencion = request.getParameter("atencion");
    General ca = new General();
    lugar a = ca.buscaratencioCompleta(Integer.parseInt(atencion));
    paciente p = ca.buscardatosdecabeceraporAdmision(a.getIdadmision());

%>




<script>




    function check(id) {

        document.getElementById(id).checked = 1;
    }

</script>
<fieldset>
    <legend class="text-center header">Atenci&oacute;n Cl&iacute;nica en Box Odontol&oacute;gico</legend>
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>


    <form id="diagnostico" name="diagnostico" method="post" action='<%=ca.getLocal()%>ingresarAtencionUrgencia' >





        <table style=" width: 100%" class="table table-bordered" border="0">
            <thead><tr>
                    <th colspan="10" class="btn-primary" style=" border-radius: 8px;  text-align: left">RESUMEN DE LA ATENCIÓN</th>

                <tr>
                    <th>Fecha Atención</th>
                    <th>Hora Admisión</th>
                    <th>Hora Atención</th>
                    <th>Motivo Consulta</th>
                    <th>Diagnostico</th>
                    <th>Ges</th>
                    <th>Intervención</th>
                    <th>Tratamiento y/o Examenes</th>
                    <th>Indicaciones de Alta</th>
                    <th>Profesional Tratante</th>
                </tr>
            </thead>
            <tr>
                <td><%=a.getVariable1()%></td>
                <td><%=a.getVariable2()%></td>
                <td><%=a.getVariable4()%></td>
                <td><%=a.getVariable3()%></td>
                <td><%=a.getDiagnostico()%></td>
                <td><%=a.getDescripcion()%></td>
                <td><%=a.getIntervencio() + " " + a.getPrescripcion()%></td>
                <td><%=a.getTratamiento()%></td>
                <td><%=a.getIndicaciones()%></td>
                <td><%=a.getNombredoctor()%></td>
            </tr>
        </table>

        <fieldset>
            <legend class="text-center header">Documentos a Imprimir</legend>
            <br>
            <br>
            <table class="table table-striped" style="width: 100%"  >

                <thead>

                <th ><p class="letra">Informe de Prestacion Realizada  <br>Servicio de Odontología<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('http://10.8.4.11:9090/CRS-Seguridad/InformedePrestacionRealizadaUrge?idatencion=<%=atencion%>', 'Informe de Prestacion realizadas - Servicio de Odontología', 'width=600,height=450')"/></p> </th>

                <%if ((a.isInterconsulta() == true)) {%>  
                <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('http://10.8.4.11:9090/CRS-Seguridad/SolicitudIntercondultapdfUrge?idatencion=<%=atencion%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>

                <%}
                    if (a.getGes() == 1) {%>
                <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('http://10.8.4.11:9090/CRS-Seguridad/ConstanciaGesUrge?idatencion=<%=atencion%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
                <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('http://10.8.4.11:9090/CRS-Seguridad/informeProcesoDiagnosticoUrge?idatencion=<%=atencion%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>

                <%}%>
                <th></th>
                </tr>


                </thead>
            </table>  
        </fieldset>

    </form>
</fieldset>



