<%-- 
    Document   : vistaHojaDiaria
    Created on : 19-07-2018, 13:42:12
    Author     : a
--%>

<%@page import="Modelos.hojadiaria"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/headerwindows.jsp"/>
<%
    String idatencion = request.getParameter("idatencion");
    controlador_especialidad ce = new controlador_especialidad();
    hojadiaria hd = ce.BuscarHojaDiaria(Integer.parseInt(idatencion));
    int hojadiario = Integer.parseInt(idatencion);
    String ges = "";
    if (hd.getGes() == 1) {
        ges = "Si Ges";
    } else {
        ges = "No Ges";
    }
    String diagnostico = ce.buscarlosdiagnosticosdeunaHojaDiariareporte(Integer.parseInt(idatencion));
    String procedimiento = ce.buscarProcedimientosdeUnaHojaReporte(Integer.parseInt(idatencion));
    String examenes = ce.buscarExamenesdeUnaHojaReporte(Integer.parseInt(idatencion));
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>
    <legend class="text-center header">Estadistica Diaria</legend>
    <table class="table table-striped" style="width: 100%">
        <thead>
        <th>Casos Ges</th>
        <th>Diagnostico</th>
        <th>Procedimiento</th>
        <th>Examenes</th>
        <th>Destino Paciente</th>

    </thead>
    <td><%=ges%></td>
    <td><%=diagnostico%></td>
    <td><%=procedimiento%></td>
    <td><%=examenes%></td>
    <td><%=hd.getAuxiliar1()%></td>

</table>

<legend class="text-center header">Documentos</legend>
<br>
<br>
<table class="table table-striped" style="width: 100%"  >

    <thead>
        <%if (hd.getSic() == 1) {%>  
    <th ><p class="letra"> Solicitud de Interconsulta  <br>o Derivación<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudIntercondultapdf2?idhojadiaria=<%=hojadiario%>', 'Solicitud de Interconsulta o Derivación', 'width=600,height=450')"/></p> </th>
            <%}
                      if (hd.getConstancia() == 1) {%>

    <th><p class="letra"> Formulario de Constancia  <br>Información al paciente GES<img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>ConstanciaGes?idhojadiaria=<%=hojadiario%>', 'Formulario de Costancia Información al Paciente GES', 'width=600,height=450')"/></p> </th>
    <th><p  class="letra" > Informe del Proceso <br> Diagnóstico - IPD <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>informeProcesoDiagnostico?idhojadiaria=<%=hojadiario%>', 'Informe del proceso Diagnóstico - IPD', 'width=600,height=450')"/></p></th>
            <%}
                     if (hd.isSolicitudpabellon() == true) {%>

    <th><p  class="letra" > Solicitud de Pabellón Quirurgico  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>SolicitudpabellonQuirurgico?idhojadiaria=<%=hojadiario%>', 'Solicitud Pabellon Quirurgico', 'width=600,height=450')"/></p></th>
    <th><p  class="letra" > Formulario de consentimiento<br> Informado Intervenciones Quirúrgicas  <img src="../../public/imagenes/icon/file_pdf_download-128.png" alt="" onclick="javascript:  window.open('<%=ce.getLocal()%>intervencionesquirurgicas?idhojadiaria=<%=hojadiario%>', 'Formulacio de Consentimiento Informado', 'width=600,height=450')"/></p></th>

    <%}%>
    <th></th>
</tr>


</thead>
</table> 

</body>
</html>
