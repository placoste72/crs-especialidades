<%-- 
    Document   : ExamenesdeLaboratorio
    Created on : 03-07-2018, 9:55:13
    Author     : a
--%>

<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Modelos.cita"%>
<%@page import="Modelos.examenes_grupos"%>
<%@page import="Controlador.controlador_examenes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>


<%
    String cita = request.getParameter("idcita");
    controlador_examenes cex = new controlador_examenes();
    controlador_cita cc = new controlador_cita();
    cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));

%>
<script>
    function validarsiclic(valor) {
        if (valor == 45) {
            var clic = document.getElementById('examen45').checked;

            if (clic) {
                document.getElementById('examen44').checked = 1;
                document.getElementById('examen46').checked = 1;
                document.getElementById('examen48').checked = 1;
                document.getElementById('examen49').checked = 1;
                document.getElementById('examen143').checked = 1;
            } else {
                document.getElementById('examen44').checked = 0;
                document.getElementById('examen46').checked = 0;
                document.getElementById('examen48').checked = 0;
                document.getElementById('examen49').checked = 0;
                document.getElementById('examen143').checked = 0;
            }
        } else if (valor == 47) {
            var clic = document.getElementById('examen47').checked;

            if (clic) {
                document.getElementById('examen50').checked = 1;

            } else {
                document.getElementById('examen50').checked = 0;

            }
        }
    }
</script>
<style>
    table{
        font-size: 10px;
    }
    th, td {
        padding: 0px;
    }

</style>
<section  class="section">
    <nav class="nav1 ">
        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos del Paciente</th>
                </tr>
                <tr>
                    <th>RUT</th>
                    <th>Paciente</th>
                    <th>Edad</th>
                    <th>Sexo</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getMotivo_cancela()%></td>
                <td><%= c.getRut_paciente()%></td>
                <td><%= c.getTemporales2()%></td>
                <td><%= c.getTemporales1()%></td>


            </tr>
        </table>

        <table class="table table-bordered" style="width: 100%">
            <thead><tr>
                    <th colspan="6" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos de la Citaciòn</th>
                </tr>
                <tr>
                    <th>Especialidad</th>
                    <th>Tipo de Cita</th>
                    <th>Diagnóstica</th>


                    <th>Fecha y Hora</th>


                </tr>

            </thead>

            <tr>
                <td><%= c.getTemporales()%> </td>
                <td><%= c.getTemporales3()%>  </td>
                <td><%= c.getTemporales4()%></td>


                <td><%= c.getMotivo()%></td>


            </tr>
        </table>
        <p class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; font-size: 10px; text-align: left">Datos otras atenciones</p>
        <table class="table-striped" >
            <tr>
                <th>Fecha</th>
                <th>Atencion</th>
                <th>Especialidad</th>
                <th>Profesional</th>
                <th>Detalle</th>
            </tr>
            <%for (atencion_clinica_tecnologo at : cc.BuscarAtencionesdeunPaciente(c.getMotivo_cancela())) {

            %>
            <tr>
                <td><%=at.getAuxiliarcinco()%></td>
                <td><%=at.getAuxiliarseis()%></td>
                <td><%=at.getAuxiliardos()%></td>
                <td><%=at.getAuxiliartres()%></td>
                <%if (at.getAuxiliarcuatro() == 1) {
                %>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 2) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 3) {%>
                <td><a onClick="window.open('<%=cc.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 4) {%>
                <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 5) {%>
                <td> </td>
                <%} else if (at.getAuxiliarcuatro() == 6) {%>
                <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"   >

                        Protocolo  </a> 

                    <%int ide = cc.idepicrisis(at.getId_atencion_tecnologo());%>
                    <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    >

                        Epicrisis  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 7) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 8) {%>
                <td><a  onClick="window.open('<%=cc.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenología', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%} else if (at.getAuxiliarcuatro() == 9) {%>
                <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   >

                        VER  </a> </td>
                        <%}%>
            </tr>
            <%

                }%>

        </table>

    </nav>
    <article class="article">

        <fieldset>

            <legend class="text-center header">Examenes Realizados</legend>

            <form name="examenes" id="examenes" action="<%=cc.getLocal()%>atenderExamendeLaboratorio" >
                <table class=" table table-striped" style="width: 100%">
                    <input id="cita" name="cita" value="<%=cita%>" hidden>
                    <tr>
                        <%for (examenes_grupos grupo : cex.buscarGrupoPantallas(1)) {%>
                        <td>
                            <table >
                                <tr>
                                    <td class=" letra"><%=grupo.getDescripciongrupo()%></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <%for (examenes_grupos eg : cex.buscarExamesparaPantallas(1, grupo.getIdgrupo())) {
                                            %>
                                            <tr>
                                                <td style="background-color: <%=eg.getColortuboexamen()%> "> <input type="checkbox" name="examen<%=eg.getId_examengrupo()%>" id="examen<%=eg.getId_examengrupo()%>" value="checkbox" onchange="javascript: var numero = <%=eg.getId_examengrupo()%>;
                                                        validarsiclic(numero)">
                                                    <%=eg.getDescripcionexamen()%>
                                                </td>
                                            </tr>  
                                            <%}%>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <%}%>
                    </tr>
                </table>
                <div class="container"> 
                    <div class="tab">
                        <a class="tablinks" onclick="javascript: var tengo = document.getElementById('Examenes').style.display;
                                if (tengo == 'none') {
                                    document.getElementById('Examenes').style.display = 'block';

                                } else {
                                    document.getElementById('Examenes').style.display = 'none';

                                }
                                ;">OTROS EXAMENES</a>

                    </div>

                    <div id="Examenes" class="tabcontent" style="display: none">
                        <table class=" table table-striped" style="width: 100%">
                            <tr>
                                <%for (examenes_grupos grupo : cex.buscarGrupoPantallas(2)) {%>
                                <td>
                                    <table >
                                        <tr>
                                            <td class=" letra"><%=grupo.getDescripciongrupo()%></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <%for (examenes_grupos eg : cex.buscarExamesparaPantallas(2, grupo.getIdgrupo())) {
                                                    %>
                                                    <tr>
                                                        <td style="background-color: <%=eg.getColortuboexamen()%> "> <input type="checkbox" name="examen<%=eg.getId_examengrupo()%>" id="examen<%=eg.getId_examengrupo()%>" value="checkbox">
                                                            <%=eg.getDescripcionexamen()%>
                                                        </td>
                                                    </tr>  
                                                    <%}%>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <%}%>
                            </tr>
                        </table>
                    </div>




                </div>
                <table>
                    <tr><td colspan="4"><div class=" letra"> Observaciones:</div></td></tr>
                    <tr>
                        <td colspan="4"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                            <div class="col-md-8">
                                <input type="text"  rows="3" value="" style=" height: 80px" id="observaciones" value="Sin observacion" name="observaciones"  class="form-control" placeholder="Observaciones">


                            </div>  
                        </td>

                    </tr>
                </table>
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Guardar</button>

                        <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                    </div>
                </div>
            </form>
        </fieldset>
    </article>
</section>
    <jsp:include page="../comunes/Footer.jsp"/>