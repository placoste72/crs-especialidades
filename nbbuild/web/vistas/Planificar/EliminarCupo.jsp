<%@page import="Modelos.doctor"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Controlador.controlador_doctor"%>

.<%-- 
    Document   : DuplicarCupo
    Created on : 19-10-2016, 04:12:42 PM
    Author     : Informatica
--%>

<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<%@ page import="java.sql.*,java.net.URL,java.util.Date,java.util.GregorianCalendar,java.util.Vector,java.text.DateFormat,java.util.Locale,java.util.Calendar" %>
<title>Eliminar cupos</title>
<style type="text/css">@import url(../../public/css/calendar-win2k-1.css);</style>
<script type="text/javascript" src="../../public/js/calendar.js"></script>
<script type="text/javascript" src="../../public/js/calendar-es.js"></script>
<%@page import="Controlador.General" %>
<script type="text/javascript" src="../../public/js/calendar-setup.js"></script>
<link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
<link href="../../public/css/jquery.alerts.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/jquery.alerts.js" type="text/javascript"></script>
<link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>

<%    controlador_doctor cd = new controlador_doctor();
    controlador_especialidad ce = new controlador_especialidad();
    String rut = "";
    String traef = "";
    Calendar fe = Calendar.getInstance();
    if (request.getParameter("funcionario") != null) {
        rut = request.getParameter("funcionario");
    }
    if (request.getParameter("fecha_inicio") != null) {
        traef = request.getParameter("fecha_inicio");
    } else {

        int a�o = fe.get(Calendar.YEAR);
        int mes = fe.get(Calendar.MONTH) + 1;
        int dia = fe.get(Calendar.DAY_OF_MONTH);

        String cad_valor = String.valueOf(dia);

        if (cad_valor.length() < 2) {
            cad_valor = "0" + cad_valor;
        }

        String cad_mes = String.valueOf(mes);

        if (cad_mes.length() < 2) {
            cad_mes = "0" + cad_mes;
        }

        traef = cad_valor + "/" + cad_mes + "/" + a�o;

    }

    General g = new General();
    String e = "";
    int es;

    if (request.getParameter("especialidad") == null || request.getParameter("especialidad") == "") {
        es = 0;
    } else {
        e = request.getParameter("especialidad");
        es = Integer.parseInt(e);
    }
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Eliminar Cupos">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>

<%
    }


%>




<body>

    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <div class="container"> 

        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">

                    <form id="eliminarcupos" name="eliminarcupos" method="post"  > 
                        <fieldset>
                            <legend class="text-center header">Eliminar Planificacion del Doctor</legend>								
                            <legend class="text-center header" style=" font-size: 22px">Cupos Planificados</legend>
                            <table class="center-block2 " style="margin: 10 auto; text-align: center;" >
                                <tr style="text-align: center;">
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-list bigicon"></i></span>


                                    <div class="col-md-8" style=" width:40%" > 
                                        <select class="form-control" id="especialidad" name="especialidad" title="Especialidad" onchange="javascript: document.eliminarcupos.action = 'EliminarCupo.jsp';
                                                document.eliminarcupos.submit();
                                                ">  
                                            <option> Seleccione Especialidad
                                                <%                                                        for (especialidades esp : ce.buscarEspecialdadTodas()) {

                                                        if (es == esp.getId_especialidad()) {%>
                                            <option selected value="<%=esp.getId_especialidad()%>"><%=esp.getNombre()%><%
                                                } else {%>
                                            <option value="<%=esp.getId_especialidad()%>"><%=esp.getNombre()%><%
                                                    }

                                                }
                                                %>
                                        </select>


                                    </div>
                                </div>     

                                </td>
                                </tr>
                                <tr>
                                    <td class="letra" >Fecha planificado: <input type="text" maxlength="13" size="12" id="fecha_inicio" name="fecha_inicio" value="<%=traef%>" ><br>

                                    <td>
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-user-md bigicon"></i></span>


                                            <div class="col-md-8" > 
                                                <select class="form-control" id="funcionario" name="funcionario" title="Doctor">
                                                    <option value="-1">Profesional
                                                        <%

                                                            for (doctor doc : cd.buscarDoctoresporEspecialidad(es)) {

                                                                if (doc.getRut() == rut) {%>
                                                    <option selected value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                        } else {%>
                                                    <option value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                            }

                                                        }
                                                        %>
                                                </select>



                                            </div> 
                                        </div>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary" onclick="javascript : buscaCupos()">Buscar</button>
                                    </td>
                                </tr>
                            </table>
                            <div id="busqueda">            
                                <p class="letra">� Realizar la Busqueda !</p>
                            </div>



                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>


        $(function () {
            $("#fecha_inicio").datepicker();
        });

        var entre = false;
        function buscaCupos()
        {
            var fecha = document.forms["eliminarcupos"]["fecha_inicio"].value;
            var funcionario = document.forms["eliminarcupos"]["funcionario"].value;
            var esp = document.forms["eliminarcupos"]["especialidad"].value;
            if (fecha == null || fecha == "" || funcionario == null || funcionario == "" || esp == null || esp == "" || esp == 0)
            {

                Alert.render("Debe Completar los Datos para Continuar");
            } else
            {
                entre = true;
                try
                {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e)
                {
                    try
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (E) {
                        xmlhttp = false;
                    }
                }
                if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                    xmlhttp = new XMLHttpRequest();
                }

                if (xmlhttp) {
                    var objeto_recibidor = document.getElementById("busqueda");
                    xmlhttp.open("post", "resumen.jsp?fecha=" + fecha + "&funcionario=" + funcionario + "&especialidad=" + esp);
                    xmlhttp.send("");
                    if (xmlhttp.readyState == 1) {
                        objeto_recibidor.innerHTML = '</br><b>Favor espere... </b>';

                    }
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            objeto_recibidor.innerHTML = xmlhttp.responseText;

                        }
                        if (xmlhttp.status != 200) {
                            objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                        }
                    }
                }

            }
        }



    </script>
</body>

<jsp:include page="../comunes/Footer.jsp" />

