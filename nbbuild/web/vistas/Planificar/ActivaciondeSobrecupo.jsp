<%@page import="Modelos.doctor"%>
<%@page import="Controlador.controlador_doctor"%>
<%@page import="Modelos.especialidades"%>
<%@page import="Controlador.controlador_especialidad"%>

.<%-- 
    Document   : DuplicarCupo
    Created on : 19-10-2016, 04:12:42 PM
    Author     : Informatica
--%>


<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<%@page import="Controlador.General" %>
<%@ page import="java.sql.*,java.net.URL,java.util.Date,java.util.GregorianCalendar,java.util.Vector,java.text.DateFormat,java.util.Locale,java.util.Calendar" %>


<link href="../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../../public/js/jquery-ui.js" type="text/javascript"></script>
<link href="../../public/css/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css"/>
<title>Sobre-Cupos</title>

<%    controlador_especialidad ce = new controlador_especialidad();
    controlador_doctor cd = new controlador_doctor();
    String rut = "";
    String traef = "";
    Calendar fe = Calendar.getInstance();
    if (request.getParameter("funcionario") != null) {
        rut = request.getParameter("funcionario");
    }
    if (request.getParameter("fecha_inicio") != null) {
        traef = request.getParameter("fecha_inicio");
    } else {

        int a�o = fe.get(Calendar.YEAR);
        int mes = fe.get(Calendar.MONTH) + 1;
        int dia = fe.get(Calendar.DAY_OF_MONTH);

        String cad_valor = String.valueOf(dia);

        if (cad_valor.length() < 2) {
            cad_valor = "0" + cad_valor;
        }

        String cad_mes = String.valueOf(mes);

        if (cad_mes.length() < 2) {
            cad_mes = "0" + cad_mes;
        }

        traef = cad_valor + "/" + cad_mes + "/" + a�o;

    }

    General g = new General();
    String e = "";
    int es;

    if (request.getParameter("especialidad") == null || request.getParameter("especialidad") == "") {
        es = 0;
    } else {
        e = request.getParameter("especialidad");
        es = Integer.parseInt(e);
    }
    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>

<div id="dialog-message" title="Sobre Cupo">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>



<%
    }


%>
<style>
    nav1 {
        float: right;
        max-width: 550px;
        margin: 0px;
        padding: 1em;
        margin-top: 10px;
    }
    article {

        border-left: 1px solid #bce8f1;
        padding: 1em;
        overflow: hidden;
        margin-top: 0px;
        height: 900px;
        overflow: scroll;
    }



</style>


<script>


    $(function () {
        $("#fecha_inicio").datepicker();
    });

    var entre = false;
    function buscaCupos()
    {
        var fecha = document.forms["activarsobrecupos"]["fecha_inicio"].value;
        var funcionario = document.forms["activarsobrecupos"]["funcionario"].value;
        var esp = document.forms["activarsobrecupos"]["especialidad"].value;
        if (fecha == null || fecha == "" || funcionario == null || funcionario == "" || esp == null || esp == "" || esp == 0)
        {

            Alert.render("Debe Completar los Datos para Continuar");
        } else
        {
            entre = true;
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                xmlhttp = new XMLHttpRequest();
            }

            if (xmlhttp) {
                var objeto_recibidor = document.getElementById("busqueda");
                xmlhttp.open("post", "buscarSobrecuposBloqueados.jsp?fecha=" + fecha + "&funcionario=" + funcionario + "&especialidad=" + esp);
                xmlhttp.send("");
                if (xmlhttp.readyState == 1) {
                    objeto_recibidor.innerHTML = '</br><b>Favor espere... </b>';

                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        objeto_recibidor.innerHTML = xmlhttp.responseText;

                    }
                    if (xmlhttp.status != 200) {
                        objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';

                    }
                }
            }

        }
    }

    function validar() {
        var m = document.forms["activarsobrecupos"]["fecha_inicio"].value;
        var motivo = document.forms["activarsobrecupos"]["motivo"].value;
        if (m == null || m == "" || motivo == null || motivo == "" || entre == false) {


            Alert.render("Debe Completar los Datos para Continuar");
            return false;
        }


    }

</script>

<br>
<br>
<br>
<br>
    <div class="container"> 
        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">

                    <form id="activarsobrecupos" name="activarsobrecupos" method="post" action='<%=g.getLocal()%>activar_sobrecupos' onsubmit="return validar()"> 
                        <fieldset>
                            <legend class="text-center header" style=" font-size: 22px">Activaci�n de Sobrecupos</legend>


                            <table class="center-block2 " style="margin: 10 auto; text-align: center;" >
                                <tr>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <span class="col-md-4 col-md-offset-1"><i class="fa fa-list bigicon" style=" font-size: 16px">Especialidad:</i></span>


                                            <div class="col-md-4" > 
                                                <select class="form-control" id="especialidad" name="especialidad" title="Especialidad" onchange="javascript: document.activarsobrecupos.action = 'ActivaciondeSobrecupo.jsp';
                                                        document.activarsobrecupos.submit();
                                                        ">  
                                                    <option> Seleccione Especialidad
                                                        <%
                                                            for (especialidades espe : ce.buscarEspecialdadTodas()) {

                                                                if (es == espe.getId_especialidad()) {%>
                                                    <option selected value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                    } else {%>
                                                    <option value="<%=espe.getId_especialidad()%>"><%=espe.getNombre()%><%
                                                            }

                                                        }
                                                        %>
                                                </select>


                                            </div>
                                        </div>     
                                    </td> 
                                </tr>
                                <tr>
                                    <td class="letra"  style=" width: 400px" >
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-calendar bigicon" style=" font-size: 16px">Fecha planificado:</i></span>
                                            <div class="col-md-8" >    
                                                <input type="text" maxlength="13" size="12" id="fecha_inicio" name="fecha_inicio" value="<%=traef%>" ><br>
                                            </div>
                                        </div>



                                    </td>

                                    <td class="letra">
                                        <div class="form-group">
                                            <span class="col-md-1 col-md-offset-1 "><i class="fa fa-user-md bigicon" style=" font-size: 16px">Profesional:</i></span>


                                            <div class="col-md-8" style=" margin-left:  65px " > 
                                                <select class="form-control" id="funcionario" name="funcionario" title="Doctor">
                                                    <%
                                                        for (doctor doc : cd.buscarDoctoresporEspecialidad(es)) {

                                                            if (doc.getRut() == rut) {%>
                                                    <option selected value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                    } else {%>
                                                    <option value="<%=doc.getRut()%>"><%=doc.getNombre()%><%
                                                            }

                                                        }
                                                        %>
                                                </select>



                                            </div> 
                                        </div>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary" onclick="javascript : buscaCupos()">Buscar</button>
                                    </td>
                                </tr>
                            </table>
                            <div id="busqueda">            
                                <p class="letra">� Realizar la Busqueda !</p>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Activar</button>

                                    <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                </div>
                            </div>                        

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>



<jsp:include page="../comunes/Footer.jsp" />

