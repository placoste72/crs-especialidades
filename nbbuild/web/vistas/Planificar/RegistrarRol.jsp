<%-- 
    Document   : Registrar Rol
    Created on : 26-09-2016, 03:37:04 PM
    Author     : Informatica
--%>

<%@page import="Modelos.rol"%>
<%@page import="Modelos.opciones"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>

<jsp:include page="../comunes/Header.jsp" />  

<%@page import="Controlador.controlador_rol" %>


<html>
  <title>Registrar Rol</title>
    <%        String mensaje = "";
        if (request.getParameter("men") != null) {
            mensaje = request.getParameter("men");
    %>
    <div id="dialog-message" title="Registrar Rol">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

        </p>
        <p>
            <b><%=mensaje%></b>.
        </p>
    </div>


    <%
        }
        int opciones;
        opciones = 0;
        controlador_rol cr = new controlador_rol();
        ArrayList lista_opciones = cr.buscaropciones();
        Iterator it = lista_opciones.iterator();

        ArrayList lista_vacia = cr.buscarListaopcionesconRol(-1);
        Iterator it2 = lista_vacia.iterator();


    %>
    <script>
        function validar() {
            var x = document.forms["rol"]["nombre"].value;
            var y = document.forms["rol"]["opciones2"].value
            if (x == null || x == "" || y == null || y == "" || y == 0) {

                Alert.render("Debe Completar los Datos para Continuar");
                return false;
            }
            if(validateIndustry() == false){
                 Alert.render("Debe seleccionar por lo menos una opcion!!Debe Completar los Datos para Continuar");
                return false;
            }
        }


        function pone() {

            if (!isSelected("#fromSelectBox")) {
                return;
            }
            //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
            $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
            return false;

        }
        function saca() {

            //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
            if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
                return;
            }
            //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
            $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

        }

        function todos() {
            selectAll('#fromSelectBox');
            pone();
        }

        function ninguno() {
            selectAll('#toSelectBox');
            saca();
        }

        function selecciona_todos() {
            selectAll('#toSelectBox');
        }

        //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
        function isSelected(thisObj) {
            if (!$(thisObj + " option:selected").length) {

                Alert.render("Debe Completar los Datos para Continuar");
                return 0;
            }
            return 1;
        }

        //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
        function noOptions(thisObj) {
            if (!$(thisObj + " option").length) {
                // alert("There are no options to select/move");
                return 0;
            }
            return 1;
        }

        //Below function is to de-select all items if any of the item(s) are selected
        function clearAll(thisObj) {
            $('#' + thisObj).each(function () {
                $(this).find('option:selected').removeAttr("selected");
            });
        }//function close

//Below function is to select all items
        function selectAll(thisObj) {
            if (!noOptions("#" + thisObj)) {
                return;
            }
            $('#' + thisObj + ' option').each(function () {
                $(this).attr("selected", "selected");
            });
        }
        
          function validateIndustry()
    {
         
            if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
                return false;
               
            }
           
    }

    </script>
  

        <div class="container"> 
            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#crear">Crear</a></li>
                <li><a data-toggle="tab" href="#lista">Listado</a></li>

            </ul>
            <div class="tab-content">
                <div id="crear" class="tab-pane fade in active">




                    <form id="rol" name="rol" class="form-horizontal" action='<%=cr.getLocal()%>ingresar_rol' method="POST" onsubmit="return validar()">
                        <fieldset>
                            <legend class="text-center header">Registra Perfil</legend>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil bigicon"></i></span>
                                <div class="col-md-8" style="margin: 5px">
                                    <input  type="text" id="nombre" name="nombre"  class="form-control" placeholder="Nombre Perfil" required oninvalid="setCustomValidity('El campo nombre perfil es obligatorio')" oninput="setCustomValidity('')"  >

                                </div>




                            </div>


                            <div style=" padding-left: 240px">

                                <h4 class="letra2"> Eliga Las Opciones que tendr� el Perfil </h4>
                                <div class="col-md-12">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <select  id="fromSelectBox" name="opciones"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px;  width: 260px "  >

                                                    <%

                                                        while (it.hasNext()) {
                                                            opciones o = (opciones) it.next();
                                                            out.write("   <option value='" + o.getIdopcion() + "' >" + o.getNombre() + "</option>");
                                                        }
                                                    %>

                                                </select>
                                            </td>
                                            <td>
                                                <span class='cell' >
                                                    <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon"  onclick="pone()"></i>
                                                    <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca()"></i>

                                                </span>

                                            </td>       
                                            <td >
                                                <select  id="toSelectBox" name="opciones2"  size="10" multiple="multiple"  style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px " >

                                                    <%
                                                        out.write("   <option value= -1  > Seleccione una Opcion</option>");
                                                        while (it2.hasNext()) {
                                                            opciones o2 = (opciones) it2.next();
                                                            out.write("   <option value='" + o2.getIdopcion() + "' >" + o2.getNombre() + "</option>");
                                                        }
                                                    %>

                                                </select>
                                            </td>    

                                        </tr> 

                                    </table>


                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Crear</button>

                                    <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>

                <div id="lista" class="tab-pane fade">

                    <fieldset>
                        <legend class="text-center header">Lista de Perfiles</legend>

                        <table class="table table-striped" style="width: 100%">
                            <thead>
                                <tr><th><strong>Codigo</strong></th>
                                    <th><strong>Nombre</strong></th>
                                    <th><strong>Actualizar</strong></th>
                                    <th><strong>Eliminar</strong></th>

                                </tr>
                            </thead>
                            <%
                                for (rol temp : cr.buscarroles()) {
                            %>

                            <tr>



                                <td><%=temp.getIdRol()%></td>
                                <td><%=temp.getNombreRol()%></td>


                                <td align="center">
                                    <a href="ActualizarRol.jsp?cod=<%=temp.getIdRol()%>" onClick="return popup2(this, 'notes')" class="button3">
                                        Actualizar
                                    </a>


                                </td>
                                <td align="center">
                                    <a href='<%=cr.getLocal()%>SEliminarRol?cod=<%=temp.getIdRol()%>'  class="button2"> Eliminar

                                    </a>
                                </td>


                            </tr>
                            <%}%>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>


    <jsp:include page="../comunes/Footer.jsp" />  
