<%-- 
    Document   : foto
    Created on : 13-10-2016, 12:09:01 PM
    Author     : Informatica
--%>

<%@page import="Modelos.usuario"%>
<%@page import="Controlador.controlador_usuario"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="org.apache.tomcat.util.http.fileupload.FileItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Controlador.General" %>
<!DOCTYPE html>
<%
    /*formdata*/
    Authentication auth = SecurityContextHolder.getContext()
            .getAuthentication();

    ServletRequestContext src = new ServletRequestContext(request);
    controlador_usuario cu = new controlador_usuario();
    List lista2 = null;
    String nombre = auth.getName();
    usuario u = cu.buscarPorRut(nombre);
    //Si el formulario es enviado con Multipart
    if (ServletFileUpload.isMultipartContent(src)) {

        ServletContext context = this.getServletConfig().getServletContext();
        String path2 = context.getRealPath("").replace("nbbuild\\web/", "WebContent\\fotos");

        File destino = new File(path2);
        DiskFileItemFactory factory = new DiskFileItemFactory((1024 * 1024), destino);
        //Creamos un FileUpload
        ServletFileUpload upload = new ServletFileUpload(factory);
        //Procesamos el request para que nos devuelva una lista
        //con los parametros y ficheros.
        List lista = upload.parseRequest(src);

        lista2 = lista;
        File file = null;
        //Recorremos la lista.
        Iterator it = lista.iterator();

        boolean sw_observacion = false;
        boolean sw_observacion_revision = false;
        while (it.hasNext()) {
            //Rescatamos el fileItem
            FileItem item = (FileItem) it.next();

            //Comprobamos si es un campo de formulario
            if (item.isFormField()) //Hacemos lo que queramos con el.
            {

            } else if (item.getName().length() > 0) {

                //Si no, es un fichero y lo subimos al servidor.
                //Primero creamos un objeto file a partir del nombre del fichero.
                file = new File(item.getName());

                item.write(new File(destino, item.getName()));
                String simbolo = " ";
                simbolo = "\\";
                String et = destino + "\\" + item.getName();

                cu.registrarfoto(u.getId_usuairo(), u.getNombre_usuario(), et);
                // pro.insertarProducto("hola", );

                //Lo escribimos en el disco
                // usando la ruta donde se guardara el fichero
                // y cogiendo el nombre del file
                // Nota: Se podria hacer usando el objeto item en vez del file directamente
                // Pero esto puede causar incompatibilidades segun que navegador, ya que
                // algunos solo pasan el nombre del fichero subido, pero otros
                // como Iexplorer, pasan la ruta absoluta, y esto crea un pequeño problema al escribir
                // el fichero en el servidor.
                response.sendRedirect("subirfoto.jsp");
            }
        }

    }


%>