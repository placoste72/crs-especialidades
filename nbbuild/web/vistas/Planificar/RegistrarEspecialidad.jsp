<%-- 
    Document   : RegistrarEspecialidad
    Created on : 18-10-2016, 09:19:51 AM
    Author     : Informatica
--%>

<%@page import="Controlador.controlador_especialidad"%>

<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp" />
<title>Registrar Especialidad</title>
<%@page import="Controlador.General" %>
<%@page import="Modelos.especialidades" %>


<%

    String mensaje = "";
    if (request.getParameter("men") != null) {
        mensaje = request.getParameter("men");
%>
<div id="dialog-message" title="Registrar Especialidad">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

    </p>
    <p>
        <b><%=mensaje%></b>.
    </p>
</div>


<%
    }
    controlador_especialidad ce = new controlador_especialidad();
    String nombre;
    if (request.getParameter("nombre") == null) {
        nombre = "";
    } else {
        nombre = request.getParameter("nombre");
    }

%>



<div class="container"> 
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody"></div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#crear">Crear</a></li>
        <li><a data-toggle="tab" href="#lista">Listado</a></li>

    </ul>
    <div class="tab-content">
        <div id="crear" class="tab-pane fade in active">

            <form enctype='multipart/form-data' action='guarda.jsp' method="post">
                <fieldset>
                    <legend class="text-center header">Registra Especialidad</legend>
                    <table>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input style="width: 660px" id="nombre" name="nombre"  type="text" placeholder="Nombre Especialidad" class="form-control">
                                    </div>
                                </div>
                            </td>    
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">

                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-photo bigicon"></i></span>
                                    <div class="col-md-9" >

                                        <input  id="file-0a" name="file-0a" class="file"  type="file" multiple data-min-file-count="1">
                                    </div>
                                </div>
                            </td>    
                        </tr>

                    </table>

                    <br>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Crear</button>
                            <button type="reset" class="btn btn-primary">Cancelar</button>
                        </div>
                    </div>



                </fieldset>
            </form>
        </div>
        <br>


    

    <div id="lista" class="tab-pane fade">
        <fieldset>
            <legend class="text-center header">Lista de Especialidades</legend>
            <table class="table table-striped" style="width: 100%">
                <thead>
                    <tr><th><strong>Codigo</strong></th>
                        <th><strong>Nombre</strong></th>
                        <th><strong>Imagen</strong></th>
                        <th><strong>Actualizar</strong></th>
                        <th><strong>Eliminar</strong></th>

                    </tr>
                </thead>
                <%                        for (especialidades temp : ce.buscarEspecialidades()) {
                %>

                <tr>



                    <td><%=temp.getId_especialidad()%></td>
                    <td><%=temp.getNombre()%></td>
                    <td align="center">
                        <a href="MostrarImagen.jsp?cod=<%=temp.getId_especialidad()%>&nombreEspecialidad=<%=temp.getNombre()%>" onClick="return popup(this, 'notes')" class="button3">
                            Ver Imagen
                        </a>
                    </td>

                    <td align="center">
                        <a href="ActualizarEspecialidad.jsp?cod=<%=temp.getId_especialidad()%>" onClick="return popup2(this, 'notes')" class="button3">
                            Actualizar
                        </a>


                    </td>
                    <td align="center">
                        <a href='<%=ce.getLocal()%>SEliminarEspecialidad?cod=<%=temp.getId_especialidad()%>' class="button2">
                            Eliminar
                        </a>
                    </td>


                </tr>
                <%}%>
            </table>
        </fieldset>


    </div>
</div>
</div>

<script>
    $('#file-fr').fileinput({
        language: 'fr',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
    });
    $('#file-es').fileinput({
        language: 'es',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
    });
    $("#file-0").fileinput({
        'allowedFileExtensions': ['jpg', 'png', 'gif'],
    });
    $("#file-1").fileinput({
        uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    /*
     $(".file").on('fileselect', function(event, n, l) {
     alert('File Selected. Name: ' + l + ', Num: ' + n);
     });
     */
    $("#file-3").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-lg",
        fileType: "any",
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
    });
    $("#file-4").fileinput({
        uploadExtraData: {kvId: '10'}
    });
    $(".btn-warning").on('click', function () {
        if ($('#file-4').attr('disabled')) {
            $('#file-4').fileinput('enable');
        } else {
            $('#file-4').fileinput('disable');
        }
    });
    $(".btn-info").on('click', function () {
        $('#file-4').fileinput('refresh', {previewClass: 'bg-info'});
    });
    /*
     $('#file-4').on('fileselectnone', function() {
     alert('Huh! You selected no files.');
     });
     $('#file-4').on('filebrowse', function() {
     alert('File browse clicked for #file-4');
     });
     */
    $(document).ready(function () {
        $("#test-upload").fileinput({
            'showPreview': false,
            'allowedFileExtensions': ['jpg', 'png', 'gif'],
            'elErrorContainer': '#errorBlock'
        });
        /*
         $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
         alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
         });
         */
    });
</script>
<jsp:include page="../comunes/Footer.jsp" />