<%-- 
    Document   : CambiarContraseña
    Created on : 26-09-2016, 04:28:26 PM
    Author     : Informatica
--%>

<%@page import="Controlador.General"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <jsp:include page="../comunes/Header.jsp" />  

    <script>
        


        function validar() {
            var x = document.forms["passwordForm"]["password1"].value;
            var y = document.forms["passwordForm"]["password2"].value;
            if (x == null || x == "" || y == null || y == "" || x.length <8 || y.length <8) {
                Alert.render("Debe Completar los Datos para Continuar");
                return false;
            }
        }
    </script>
    <%
        General g = new General();
        String mensaje = "";
        if (request.getParameter("men") != null) {
            mensaje = request.getParameter("men");
    %>
    <div id="dialog-message" title="Cambiar Contraseña">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

        </p>
        <p>
            <b><%=mensaje%></b>.
        </p>
    </div>

    <%
        }
    %>

        <div class="container"> 

            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">


                        <form method="post" id="passwordForm" action='<%=g.getLocal()%>cambiar_contrasena' onsubmit="return validar()">
                            <fieldset>
                                <legend class="text-center header">Cambiar contraseña</legend>
                                <table>
                                    <TR>
                                        <TD>
                                            <div class="form-group">
                                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-lock bigicon"></i></span>
                                                <div class="col-md-8">
                                                    <input type="password" class="form-control" name="password1" id="password1" placeholder="Nueva Contraseña" autocomplete="off">

                                                </div>


                                                <div class="col-md-2 col-md-offset-3  col-sm-4">

                                                    <span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> 8 Caracteres<br>
                                                    <span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra Mayúscula
                                                </div>
                                                <div class=" col-md-2 col-md-offset-4 col-sm-4">
                                                    <span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra Minúscula<br>
                                                    <span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Un Número

                                                </div>
                                            </div>
                                        </td>
                                    </TR>
                                    <hr>
                                    <TR>
                                        <TD>
                                            <div class="form-group">
                                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-lock bigicon"></i></span>
                                                <div class="col-md-8">
                                                    <input type="password" class="form-control" name="password2" id="password2" placeholder="Repita Contraseña" autocomplete="off">
                                                </div>
                                                <div class="col-md-2 col-md-offset-3  col-sm-4">
                                                    <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> La contraseña coinciden
                                                </div>

                                            </div>   
                                        </TD>
                                    </TR>

                                    <hr>
                                </table>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Cambiar</button>

                                    <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div><!--/col-sm-6-->
                </div><!--/row-->
            </div>
        </div>


        <script type="text/javascript">

            $("input[type=password]").keyup(function () {

                var ucase = new RegExp("[A-Z]+");
                var lcase = new RegExp("[a-z]+");
                var num = new RegExp("[0-9]+");

                if ($("#password1").val().length >= 8) {
                    $("#8char").removeClass("glyphicon-remove");
                    $("#8char").addClass("glyphicon-ok");
                    $("#8char").css("color", "#00A41E");
                }

                if (ucase.test($("#password1").val())) {
                    $("#ucase").removeClass("glyphicon-remove");
                    $("#ucase").addClass("glyphicon-ok");
                    $("#ucase").css("color", "#00A41E");
                } else {
                    $("#ucase").removeClass("glyphicon-ok");
                    $("#ucase").addClass("glyphicon-remove");
                    $("#ucase").css("color", "#FF0004");
                }

                if (lcase.test($("#password1").val())) {
                    $("#lcase").removeClass("glyphicon-remove");
                    $("#lcase").addClass("glyphicon-ok");
                    $("#lcase").css("color", "#00A41E");
                } else {
                    $("#lcase").removeClass("glyphicon-ok");
                    $("#lcase").addClass("glyphicon-remove");
                    $("#lcase").css("color", "#FF0004");
                }

                if (num.test($("#password1").val())) {
                    $("#num").removeClass("glyphicon-remove");
                    $("#num").addClass("glyphicon-ok");
                    $("#num").css("color", "#00A41E");
                } else {
                    $("#num").removeClass("glyphicon-ok");
                    $("#num").addClass("glyphicon-remove");
                    $("#num").css("color", "#FF0004");
                }

                if ($("#password1").val() == $("#password2").val()) {
                    $("#pwmatch").removeClass("glyphicon-remove");
                    $("#pwmatch").addClass("glyphicon-ok");
                    $("#pwmatch").css("color", "#00A41E");
                } else {
                    $("#pwmatch").removeClass("glyphicon-ok");
                    $("#pwmatch").addClass("glyphicon-remove");
                    $("#pwmatch").css("color", "#FF0004");
                }
            });

        </script>
        <jsp:include page="../comunes/Footer.jsp" />  
 
