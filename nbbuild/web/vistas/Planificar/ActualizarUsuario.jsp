<%-- 
    Document   : ActualizarUsuario
    Created on : 01-12-2016, 11:28:16 AM
    Author     : Informatica
--%>
<!DOCTYPE html>
<%@page import="Modelos.rol"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.usuario"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Controlador.controlador_usuario"%>
<%@page import="Controlador.General" %>
<%@page import="Modelos.especialidades" %>
<jsp:include page="../comunes/headerwindows.jsp"/>

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    </head>
    <body>

        <%
            controlador_usuario cu = new controlador_usuario();
            String id = request.getParameter("cod");
            usuario u = cu.buscarusuario(Integer.parseInt(id));
            ArrayList lista_opciones = cu.buscarListaRolsinUsuario(Integer.parseInt(id));
            Iterator it = lista_opciones.iterator();

            ArrayList lista_vacia = cu.buscarListaRolconUsuario(Integer.parseInt(id));
            Iterator it2 = lista_vacia.iterator();
            String mensaje = "";
            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <div id="dialog-message" title="Actualizar Usuario">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>

            </p>
            <p>
                <b><%=mensaje%></b>.
            </p>
        </div>

        <%
            }

        %>
        <script>
            //para elegir

            function pone() {

                if (!isSelected("#fromSelectBox")) {
                    return;
                }
                //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'toSelectBox' (select box)
                $('#fromSelectBox option:selected').remove().appendTo('#toSelectBox');
                return false;

            }
            function saca() {

                //If no items are present in 'toSelectBox' (or) if none of the items are selected inform the user using an alert
                if (!noOptions("#toSelectBox") || !isSelected("#toSelectBox")) {
                    return;
                }
                //If atleast one of the item is selected, initially the selected option would be 'removed' and then it is appended to 'fromSelectBox' (select box)
                $('#toSelectBox option:selected').remove().appendTo('#fromSelectBox');

            }

            function todos() {
                selectAll('#fromSelectBox');
                pone();
            }

            function ninguno() {
                selectAll('#toSelectBox');
                saca();
            }

            function selecciona_todos() {
                selectAll('toSelectBox');
            }

            //Below function is to validate the select box, if none of the item(s) is selected then it alerts saying 'Please select atleast one option' if user selects an item then it returns true
            function isSelected(thisObj) {
                if (!$(thisObj + " option:selected").length) {
                    Alert.render("Debe Completar los Datos para Continuar");
                    return 0;
                }
                return 1;
            }

            //Below function is to validate the select box, if none of the item(s) where present in the select box provided then it alerts saying 'There are no options to select/move' if select box has more than one item it returns true
            function noOptions(thisObj) {
                if (!$(thisObj + " option").length) {
                    // alert("There are no options to select/move");
                    return 0;
                }
                return 1;
            }

            //Below function is to de-select all items if any of the item(s) are selected
            function clearAll(thisObj) {
                $('#' + thisObj).each(function () {
                    $(this).find('option:selected').removeAttr("selected");
                });
            }//function close

            //Below function is to select all items
            function selectAll(thisObj) {
                obj = document.getElementById(thisObj);
                for (var i = 0; i < obj.options.length; i++) {
                    obj.options[i].selected = true;
                }
            }
        </script>
        <form style="border: #619fd8 5px solid;" id="f" name="f" class="form-horizontal" action='<%=cu.getLocal()%>actualizar_usuario' method="post" onsubmit="return selecciona_todos()">
            <fieldset>
                <div id="dialogoverlay"></div>
                <div id="dialogbox">
                    <div>
                        <div id="dialogboxhead"></div>
                        <div id="dialogboxbody"></div>
                        <div id="dialogboxfoot"></div>
                    </div>
                </div>
                <legend class="text-center header">Actualizar Usuario</legend>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                    <div class="col-md-8" >
                        <input type="text" id="id" name="id" value="<%=id%>" hidden>
                        <input type="text" id="funcionario"  class="form-control" name="funcionario" value="<%=u.getId_funcionario()%>" readonly="readonly">
                    </div>
                </div>

                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope bigicon"></i></span>
                    <div class="col-md-8" >
                        <input type="email" id="email" name="email" class="form-control" placeholder="Email" value="<%=u.getEmail()%>" onchange="return validarEmail(this.from)" required oninvalid="setCustomValidity('El campo email es obligatorio')" oninput="setCustomValidity('')">
                    </div>
                </div>


                <div  style=" padding-left: 240px">
                    <h4 class="letra2"> Eliga los Perfiles del Usuario </h4>
                    <div class="col-md-8 text-center">

                        <table class="table">
                            <tr>
                                <td>
                                    <select  id="fromSelectBox" name="roles2"  size="10" multiple="multiple" style="padding-right: 25px;  margin: -3px 13px -19px;  width: 260px ">

                                        <%

                                            while (it.hasNext()) {
                                                rol r = (rol) it.next();
                                                out.write("   <option value='" + r.getIdRol() + "' >" + r.getNombreRol() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>
                                <td style="width:5px">
                                    <span class='cell' >
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-right bigicon " onclick="pone();
                                                return false"></i>
                                        <br><br>&nbsp;&nbsp;<i class="fa fa-chevron-left bigicon" onclick="saca();
                                                return false"></i>

                                    </span>

                                </td>       
                                <td>
                                    <select  id="toSelectBox" name="roles"  size="10" multiple="multiple"  style="padding-right: 15px;  margin: -3px 15px -19px; width: 260px ">

                                        <%
                                            while (it2.hasNext()) {
                                                rol r2 = (rol) it2.next();
                                                out.write("   <option value='" + r2.getIdRol() + "' >" + r2.getNombreRol() + "</option>");
                                            }
                                        %>

                                    </select>
                                </td>    

                            </tr> 

                        </table>


                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Actualizar</button>

                        <button type="reset" class="btn btn-primary btn-lg" onclick="javascript: window.close();" >Cancelar</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </body>
</html>
