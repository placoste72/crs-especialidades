<%-- 
    Document   : solicitud
    Created on : 31-05-2018, 13:11:47
    Author     : a
--%>

<%@page import="Modelos.tipo_anestesia"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.lugar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    controlador_especialidad ce = new controlador_especialidad();
    String cantidadsolicitud = request.getParameter("cantidad");
    String especialidad = request.getParameter("especialidad");


%>
<table class="table table-striped" >
    <thead>
        <tr>
            <th>
                Diagnostico
            </th>
             <th>
                 &nbsp;
            </th>
            <th>
                Lateralidad
            </th>
           
            <th>
                Intervención
            </th>
            <th>
                Anestesia Solicitada
            </th>
            <th>
                Requerimientos Especiales
            </th>
            <th>
                Observación
            </th>
        </tr>
    </thead>
    <% for (int i = 0; i < Integer.parseInt(cantidadsolicitud); ++i) {%>
    <tr>

        <td  >


            <select style=" width: 150% " name="diagnosticos<%=i%>" id="diagnosticos<%=i%>"  onchange="javascript:

                            var diagnostico = document.getElementById('diagnosticos<%=i%>').value;
                    if (diagnostico == -1) {


                        document.getElementById('otrodiagnosticosicmuestro<%=i%>').style.display = 'block';
                    } else
                        document.getElementById('otrodiagnosticosicmuestro<%=i%>').style.display = 'none';" >
                <option  value="0" selected>Diagnostico
                <option  value="-1" >Otro
                    <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(Integer.parseInt(especialidad))) {%>
                <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                    <% }%>  
            </select>
        </td>
        <td>
            <div class="col-md-12" id="otrodiagnosticosicmuestro<%=i%>" style=" display: none">
                <textarea type="text" style=" width: 150px"  rows="1" id="otrodiagnostico<%=i%>" name="otrodiagnostico<%=i%>"  class="form-control" placeholder="Otro Diagnostico"  ></textarea>


            </div>  

        </td>
        <td>
            <select id="lateralidades<%=i%>" name="lateralidades<%=i%>" onchange="javascript:

                            var diagnostico = document.getElementById('diagnosticos<%=i%>').value;
                             var lateralidad = document.getElementById('lateralidades<%=i%>').value;
                            
                                var la = '';
                                if (lateralidad == 1) {
                                    la = 'OJO DERECHO';
                                } else if (lateralidad == 2) {
                                    la = 'OJO IZQUIERDO';
                                }

                                if ((diagnostico == 2) || (diagnostico == 31)) {
                                    document.getElementById('intervencion<%=i%>').value = 'FACOERESIS CON IMPLANTE DE LENTE INTRAOCULAR ' + la;
                                } else {
                                    document.getElementById('intervencion<%=i%>').value = '';
                                }
                    ">
                <option selected value="0" >Lateralidad</option>

                <option value="1">Ojo Derecho</option>
                <option value="2">Ojo Izquierdo</option>
                <option value="3">Ambos Ojos</option>

            </select>
        </td>

        <td>
            <textarea rows="2" cols="30" name="intervencion<%=i%>" id="intervencion<%=i%>" ></textarea>
        </td>
        <td>



            <select name="anestesia<%=i%>"  id="anestesia<%=i%>">
                <option value="0">Tipo de Anestesia
                    <%for (tipo_anestesia temp : ce.tiposdeanestesia()) {
                    %>
                <option value="<%=temp.getIdtipo_atencion()%>"><%=temp.getNombretipo()%></option> 
                <%}%>
            </select>



        </td>
        <td>
            <textarea rows="2"  name="requerimientos<%=i%>" id="requerimientos<%=i%>" > </textarea>
        </td>
        <td>
            <textarea rows="2"  name="observacion<%=i%>" id="observacion<%=i%>" ></textarea>
        </td>

    </tr>
    <%}%> 

</table>


