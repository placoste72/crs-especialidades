<%-- 
    Document   : atender
    Created on : 28-03-2019, 12:04:48
    Author     : a
--%>

<%@page import="Modelos.lugar"%>
<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="Controlador.controlador_cita"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_especialidad"%>

<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>

<style>
    .columna {
        width:33%;
        float:left;
    }

    @media (max-width: 500px) {

        .columna {
            width:auto;
            float:none;
        }

    }  

    table{
        font-size: 12px;
    }
    th, td {
        padding: 0px;
    }

</style>
<%

    controlador_atencion_clinica_oftalmologia otrocontrolador = new controlador_atencion_clinica_oftalmologia();
    String cita = request.getParameter("cita");
    controlador_cita cc = new controlador_cita();
    cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
    controlador_especialidad ce = new controlador_especialidad();
    int idespecialidad = 0;
    idespecialidad = c.getId_patologia();

    Integer[] idpertinencia = {1, 0};
    String[] nombrepertinencia = {"SI", "NO"};
    int pre = -1;

    String[] lateralidad = {"", "Ojo derecho", "Ojo izquierdo", "Ambos ojos"};

%>
</head>
<body>
    <section  class="section">
        <nav class="nav1 ">
            <table class="table table-bordered" style="width: 100%">
                <thead><tr>
                        <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos del Paciente</th>
                    </tr>
                    <tr>
                        <th>RUT</th>
                        <th>Paciente</th>
                        <th>Edad</th>
                        <th>Sexo</th>


                    </tr>

                </thead>

                <tr>
                    <td><%= c.getMotivo_cancela()%></td>
                    <td><%= c.getRut_paciente()%></td>
                    <td><%= c.getTemporales2()%></td>
                    <td><%= c.getTemporales1()%></td>


                </tr>
            </table>

            <table class="table table-bordered" style="width: 100%">
                <thead><tr>
                        <th colspan="6" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos de la Citaci�n</th>
                    </tr>
                    <tr>
                        <th>Especialidad</th>
                        <th>Tipo de Cita</th>
                        <th>Diagn�stica</th>


                        <th>Fecha y Hora</th>


                    </tr>

                </thead>

                <tr>
                    <td><%= c.getTemporales()%> </td>
                    <td><%= c.getTemporales3()%>  </td>
                    <td><%= c.getTemporales4()%></td>


                    <td><%= c.getMotivo()%></td>


                </tr>
            </table>
            <p class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; font-size: 10px; text-align: left">Datos otras atenciones</p>

            <table class="table-striped" >
                <tr>
                    <th>Fecha</th>
                    <th>Atencion</th>
                    <th>Especialidad</th>
                    <th>Profesional</th>
                    <th>Detalle</th>
                </tr>
                <%for (atencion_clinica_tecnologo at : cc.BuscarAtencionesdeunPaciente(c.getMotivo_cancela())) {

                %>
                <tr>
                    <td><%=at.getAuxiliarcinco()%></td>
                    <td><%=at.getAuxiliarseis()%></td>
                    <td><%=at.getAuxiliardos()%></td>
                    <td><%=at.getAuxiliartres()%></td>
                    <%if (at.getAuxiliarcuatro() == 1) {
                    %>
                    <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 2) {%>
                    <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 3) {%>
                    <td><a onClick="window.open('<%=cc.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 4) {%>
                    <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 5) {%>
                    <td> </td>
                    <%} else if (at.getAuxiliarcuatro() == 6) {%>
                    <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"   >

                            Protocolo  </a> 

                        <%int ide = cc.idepicrisis(at.getId_atencion_tecnologo());%>
                        <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    >

                            Epicrisis  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 7) {%>
                    <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 8) {%>
                    <td><a  onClick="window.open('<%=cc.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenolog�a', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 9) {%>
                    <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%}%>
                </tr>
                <%

                    }%>

            </table>

        </nav>
        <article class="article">


            <div id="dialogoverlay"></div>
            <div id="dialogbox">
                <div>
                    <div id="dialogboxhead"></div>
                    <div id="dialogboxbody"></div>
                    <div id="dialogboxfoot"></div>
                </div>
            </div>
            <div class="container">


                <form id="hojaatencionoftalmologia" name="hojaatencionoftalmologia" action="<%=cc.getLocal()%>registraratencionoftalmologia"  method="post" onsubmit="return validar()">
                    <div class="tab-content">

                        <legend class="text-center header">Atencion en Box </legend>



                        <%if (c.getId_tipoatencion() == 1) {%>  
                        <table>
                            <tr>
                            <tr><td> &nbsp;</td></tr>
                            <tr>
                                <td colspan="2">
                                    <span class="col-md-8   col-md-offset-4 "><i class="fa fa-edit bigicon"  style=" font-size: 20px" >�Es pertinente la derivaci�n a su atenci�n?</i></span>
                                </td>
                                <td>
                                    <div class="col-md-8">
                                        <select id="pertinencia" name="pertinencia" > 


                                            <option selected  value="-1"> Seleccionar </option>

                                            <% for (int i = 0; i < idpertinencia.length; ++i) {

                                                    if (idpertinencia[i] == pre) {%>
                                            <option selected value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                <%} else {%>

                                            <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                <% }
                                                    }
                                                %>

                                        </select>  
                                    </div>
                                </td>
                            </tr>

                        </table>
                        <%}%>


                        <table style="border-top:  #619fd8 2px solid; ">




                            <input id="idcita" name="idcita" value="<%=cita%>" hidden >
                            <input id="especialidad" name="especialidad" value="<%=idespecialidad%>" hidden >
                            <input id="idatencioncita" name="idatencioncita" value="<%=c.getId_tipoatencion()%>" hidden>

                            <tr> <td style="width: 30px"><div class=" letra " style=" font-size: 20px">Resumen de la Atenci�n</div></td></tr>
                            <tr>

                                <td>


                                    <div class="col-md-12">
                                        <textarea type="text"  rows="4" id="resumenatencion" name="resumenatencion"  class="form-control" placeholder="Resumen Atencion" required oninvalid="setCustomValidity('El campo Resumen de la Atenci�n es obligatorio')" oninput="setCustomValidity('')" ></textarea>


                                    </div>  
                                </td>
                            </tr>
                        </table>
                        <div class="container">

                            <ul class="nav nav-tabs">
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'dpt')" id="defaultOpen" >Diagnosticos</a></li>
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'dept')" id="defaultOpen">Derivaci�n</a></li>
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'eg')" id="defaultOpen">Excepci�n <BR> de Garant�a</a></li>
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'ept')" id="defaultOpen">Examenes <BR> Realizados</a></li>
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'ipt')" id="defaultOpen">Indicaciones <BR> (Receta Medica)</a></li>

                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'nrpt')" id="defaultOpen">No Indicaci�n<BR> de Cirug�a</a></li>

                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'ppt')" id="defaultOpen">Procedimientos <BR>Realizados</a></li>
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'rm')" id="defaultOpen">Receta<BR>Lente</a></li> 
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'seo')" id="defaultOpen">Solicitud de <BR> Procedimientos<BR>o Examen</a></li>
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'srpt')" id="defaultOpen">Solicitud <BR>de Cirug�a</a></li>
                                <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'sdc')" id="defaultOpen">Solicitud <BR>de Control</a></li>
                            </ul>

                            <div id="dpt" name="dpt" class="tabcontent">
                                <table style="border-top:  #619fd8 2px solid; "> 
                                    <tr>  
                                        <td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Diagnosticos</div></td>


                                    </tr>
                                </table>
                                <table class=" table table-striped" style=" width: 100%"> 
                                    <thead>
                                        <tr>
                                            <th>Diagnostico</th>
                                            <th>Lateralidad</th>
                                            <th>Confirma o Descarta Diagnostico</th>
                                            <th>CASO GES</th>
                                            <th>Requiere Documento  </th>
                                        </tr>
                                    </thead>

                                    <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {%>
                                    <tr>

                                        <td style="font-size:12px">
                                            <input type="checkbox"   name="diagnostico<%=lu.getId_atencion()%>" id="diagnostico<%=lu.getId_atencion()%>" value="checkbox" onchange="javascript:
                                                        var pre = document.getElementById('diagnostico<%=lu.getId_atencion()%>').checked;
                                                if (pre == true) {
                                                    document.getElementById('lateralidad<%=lu.getId_atencion()%>').disabled = false;
                                                    document.getElementById('confirmodecartod<%=lu.getId_atencion()%>').disabled = false;
                                                    document.getElementById('ges<%=lu.getId_atencion()%>').disabled = false;
                                                } else {

                                                    document.getElementById('lateralidad<%=lu.getId_atencion()%>').disabled = true;
                                                    document.getElementById('confirmodecartod<%=lu.getId_atencion()%>').disabled = true;
                                                    document.getElementById('ges<%=lu.getId_atencion()%>').disabled = true;
                                                }
                                                   "><%=lu.getNombre()%>


                                        </td>
                                        <td> 
                                            <select id="lateralidad<%=lu.getId_atencion()%>" disabled  name="lateralidad<%=lu.getId_atencion()%>">
                                                <option selected value="0" >Lateralidad</option>

                                                <option value="1">Ojo Derecho</option>
                                                <option value="2">Ojo Izquierdo</option>
                                                <option value="3">Ambos Ojos</option>
                                            </select>
                                        </td>
                                        <td> 
                                            <div class="col-md-8">
                                                <select id="confirmodecartod<%=lu.getId_atencion()%>" disabled name="confirmodecartod<%=lu.getId_atencion()%>"> 
                                                    <option selected value="0">  Confirma o Descarta </option>

                                                    <option value="1"> Si </option>
                                                    <option value="2"> No </option>

                                                </select>  
                                            </div>
                                        </td>
                                        <td> 
                                            <div class="col-md-8">
                                                <select id="ges<%=lu.getId_atencion()%>" disabled  name="ges<%=lu.getId_atencion()%>" onchange="javascript:

                                                            var pre = document.getElementById('ges<%=lu.getId_atencion()%>').value;
                                                    if (pre == 1) {


                                                        document.getElementById('documentos<%=lu.getId_atencion()%>').style.display = 'block';
                                                    } else
                                                        document.getElementById('documentos<%=lu.getId_atencion()%>').style.display = 'none';
                                                    document.getElementById('datosipd<%=lu.getId_atencion()%>').style.display = 'none';
                                                        ">

                                                    <option selected value="0"> GES </option>

                                                    <option value="1"> Si </option>
                                                    <option value="2"> No </option>

                                                </select>  
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-md-8" id="documentos<%=lu.getId_atencion()%>" name="documento<%=lu.getId_atencion()%>" style=" display: none">
                                                <select id="confirmadescarta<%=lu.getId_atencion()%>" name="confirmadescarta<%=lu.getId_atencion()%>" onchange="javascript:

                                                            var pre = document.getElementById('confirmadescarta<%=lu.getId_atencion()%>').value;
                                                    if (pre == 1) {


                                                        document.getElementById('datosipd<%=lu.getId_atencion()%>').style.display = 'block';
                                                    } else
                                                        document.getElementById('datosipd<%=lu.getId_atencion()%>').style.display = 'none';" >  > 
                                                    <option selected value="0"> Documento </option>

                                                    <option value="1"> Si </option>
                                                    <option value="2"> No </option>

                                                </select>  
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="10">

                                            <table class="table-striped" style=" display: none; width: 100%" id="datosipd<%=lu.getId_atencion()%>">
                                                <tr>
                                                    <td></td>
                                                    <td >
                                                        <div class=" letra " style=" font-size: 16px">Datos para IPD.</div>
                                                    </td>
                                                    <td >
                                                        <div class="col-md-12">
                                                            <textarea type="text"  rows="3" id="fundamentodeldiagnostico<%=lu.getId_atencion()%>" name="fundamentodeldiagnostico<%=lu.getId_atencion()%>"  class="form-control" placeholder="Fundamento del Diagnostico"  ></textarea>

                                                        </div>

                                                    </td >
                                                    <td >
                                                        <div class="col-md-12">
                                                            <textarea type="text"  rows="3" id="tratamientoeindicaciones<%=lu.getId_atencion()%>" name="tratamientoeindicaciones<%=lu.getId_atencion()%>"  class="form-control" placeholder="Tratamiento e Indicaciones"  ></textarea>
                                                        </div>
                                                    </td> 
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <% }%>  
                                </table>


                                <table >

                                    <tr>

                                        <td style="width: 10px">
                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Diagnostico</i></span> 
                                        </td>
                                        <td colspan="3">


                                            <div class="col-md-12">
                                                <textarea type="text"  rows="3" id="otrodiagnostico" name="otrodiagnostico"  class="form-control" placeholder="Otro Diagnostico"  ></textarea>


                                            </div>  
                                        </td>

                                    </tr>  
                                </table>
                            </div>
                            <div id="rm" name="rm" class="tabcontent">
                                <table style="border-top:  #619fd8 2px solid;">

                                    <tr><td colspan="3" ><div class=" letra " style=" font-size: 20px">Receta de Lente</div>


                                            <div  class="col-md-6 col-md-offset-1 text-center">

                                                <select id="lentesolicito" name="lentesolicito" onchange="
                                                    var pre = document.getElementById('lentesolicito').value;
                                                    if (pre == 1) {
                                                        document.getElementById('trata').style.display = 'block';

                                                    } else {
                                                        document.getElementById('trata').style.display = 'none';
                                                        document.getElementById('receta').style.display = 'none';

                                                    }

                                                        " > 


                                                    <option selected  value="-1"> Seleccionar </option>

                                                    <% for (int i = 0; i < idpertinencia.length; ++i) {

                                                            if (idpertinencia[i] == pre) {%>
                                                    <option selected value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                        <%} else {%>

                                                    <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                        <% }
                                                            }
                                                        %>

                                                </select>  
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <div class="col-md-8" id="trata" name="trata" style="  display: none ">

                                                <input type="checkbox" name="tratamientolentesCerca" id="tratamientolentesCerca" value="checkbox" onchange="javascritp:

                                                            var pre = document.getElementById('tratamientolentesCerca').checked;
                                                    var pre1 = document.getElementById('tratamientolentesLejos').checked;
                                                    if (pre || pre1) {
                                                        document.getElementById('receta').style.display = 'block';
                                                    } else {
                                                        document.getElementById('receta').style.display = 'none';
                                                    }">&nbsp;&nbsp;
                                                <i class="fa fa-check bigicon" style="font-size:16px">  Tratamiento con Lentes �pticos Cerca </i>
                                                <br>
                                                <input type="checkbox" name="tratamientolentesLejos" id="tratamientolentesLejos" value="checkbox" onchange="javascritp:

                                                            var pre = document.getElementById('tratamientolentesLejos').checked;
                                                    var pre1 = document.getElementById('tratamientolentesCerca').checked;
                                                    if (pre || pre1) {
                                                        document.getElementById('receta').style.display = 'block';
                                                    } else {
                                                        document.getElementById('receta').style.display = 'none';
                                                    }">&nbsp;&nbsp;<i class="fa fa-check bigicon" style="font-size:16px">  Tratamiento con Lentes �pticos Lejos</i>
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">

                                            <div id="receta" style="border-top:  #619fd8 2px solid;  display: none ">
                                                <div class=" letra"> Receta de Lente:</div>
                                                <div>
                                                    &nbsp;

                                                </div>
                                                <div>
                                                    <input  style="height:40px; width: 200px " id="odf" value="" name="odf" placeholder=" Ojo Derecho Esfera"> 

                                                    <input  style="height:40px ; width: 200px " id="odc" name="odc" value=""  placeholder=" Ojo Derecho Cilindro">

                                                    <input  style=" height:40px ; width: 200px" id="ode" name="ode" value="" placeholder=" Ojo Derecho Eje">
                                                </div>
                                                <div>
                                                    &nbsp;

                                                </div>
                                                <div>

                                                    <input  style=" height: 40px; width: 200px " id="oie" value="" name="oie" placeholder=" Ojo Izquierdo Esfera">

                                                    <input  id="oic" style=" height: 40px ; width: 200px" value="" name="oic"  placeholder=" Ojo Izquierdo Cilindro">

                                                    <input   id="oiej" style=" height: 40px ; width: 200px" value="" name="oiej" placeholder=" Ojo Izquierdo Eje">


                                                </div>
                                                <div>
                                                    &nbsp;

                                                </div>
                                                <div>
                                                    <input style=" height: 40px; width: 200px "   value="" id="add" name="add" placeholder="ADD">
                                                </div>
                                                <div>
                                                    &nbsp;

                                                </div>
                                                <div>
                                                    <div>
                                                        <input  style=" height: 40px; width: 200px" value=""  id="dpl" name="dpl" placeholder="Distancia Pupilar Lejos"> 

                                                        <input  style=" height: 40px; width: 200px" value="" id="dpc" name="dpc" placeholder="Distancia Pupilar Cerca">
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;

                                                </div>
                                                <div>
                                                    <div class=" letra"> Observaciones:</div>
                                                    <span class="col-md-1 col-md-offset-1 text-center">
                                                        <i class="fa fa-edit bigicon"></i></span>


                                                    <div class="col-md-8">
                                                        <input  style=" height: 80px" value="" type="text"  rows="3" id="observacionesreceta" name="observacionesreceta"  class="form-control" placeholder="Observaciones">


                                                    </div>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="ppt" name="ppt" class="tabcontent">  
                                <table style="border-top:  #619fd8 2px solid; ">

                                    <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Procedimiento Realizados</div></td></tr>
                                    <tr>
                                        <td > &nbsp;</td>
                                        <td>
                                            <%for (lugar lu : ce.buscarlosProcedimientosporEspecialidad(idespecialidad)) {%>

                                            <input type="checkbox" id="procedimiento<%=lu.getId_atencion()%>" name="procedimiento<%=lu.getId_atencion()%>" value="checkbox"><%=lu.getNombre()%> &nbsp;
                                            <br>

                                            <% }%>  
                                        </td>

                                        <td style="width: 10px">
                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Procedimiento</i></span> 
                                        </td>
                                        <td>


                                            <div class="col-md-12">
                                                <textarea type="text"  rows="3" id="otroprocedimiento" name="otroprocedimiento"  class="form-control" placeholder="Otro Procedimiento"  ></textarea>


                                            </div>  
                                        </td>

                                    </tr>  
                                </table>
                            </div>
                            <div id="ept" name="ept" class="tabcontent">   
                                <table style="border-top:  #619fd8 2px solid; ">

                                    <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Examenes Realizados</div></td></tr>
                                    <tr>
                                        <td > &nbsp;</td>
                                        <td>
                                            <%for (lugar lu : ce.buscarlosExamenesporEspecialidad(idespecialidad)) {%>

                                            <input type="checkbox" id="examenes<%=lu.getId_atencion()%>" name="examenes<%=lu.getId_atencion()%>" value="checkbox"><%=lu.getNombre()%> &nbsp;

                                            <br>

                                            <% }%>  
                                        </td>

                                        <td style="width: 10px">
                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Examenes</i></span> 
                                        </td>
                                        <td>


                                            <div class="col-md-12">
                                                <textarea type="text"  rows="3" id="otroexamene" name="otroexamene"  class="form-control" placeholder="Otro Examenes"  ></textarea>


                                            </div>  
                                        </td>

                                    </tr>  
                                </table>
                            </div>
                            <div id="dept" name="dept" class="tabcontent">
                                <table style="border-top:  #619fd8 2px solid; " >

                                    <input id="cita" name="cita" value="<%=cita%>" hidden>
                                    <tr>
                                        <td style="width: 300px"><span class="col-md-3   col-md-offset-1 "><i class="fa fa-chevron-down bigicon"  style=" font-size: 16px" >Genera SIC:</i></span>

                                            <div class="col-md-8">
                                                <select id="sic" name="sic" onchange="javascript:

                                                            var pre = document.getElementById('sic').value;
                                                    if (pre == 1) {


                                                        document.getElementById('detalle').style.display = 'block';
                                                    } else
                                                        document.getElementById('detalle').style.display = 'none';" > 
                                                    <option selected value="0" > SIC </option>

                                                    <option value="1"> Si </option>
                                                    <option value="2"> No </option>

                                                </select>  
                                            </div>

                                        </td>
                                        <td >
                                            <div class="col-md-12" id="detalle" style=" display: none ">
                                                <table>
                                                    <tr>
                                                        <td style="width: 3px">
                                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-chevron-down bigicon" style=" font-size: 16px">Diagnostico por el que Derivo</i></span> 

                                                        </td>
                                                        <td>


                                                            <select name="diagnosticosic" id="diagnosticosic" class="form-control" onchange="javascript:

                                                                        var diagnostico = document.getElementById('diagnosticosic').value;
                                                                if (diagnostico == -1) {


                                                                    document.getElementById('otrodiagnosticosicmuestro').style.display = 'block';
                                                                } else
                                                                    document.getElementById('otrodiagnosticosicmuestro').style.display = 'none';" >
                                                                <option  value="0" selected>Diagnostico
                                                                <option  value="-1" >Otro
                                                                    <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {%>
                                                                <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                                    <% }%>  
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12" id="otrodiagnosticosicmuestro" style=" display: none">
                                                                <textarea type="text"  rows="1" id="otrodiagnosticosic" name="otrodiagnosticosic"  class="form-control" placeholder="Otro Diagnostico"  ></textarea>


                                                            </div>  

                                                        </td>
                                                        <td style="width: 3px">
                                                            <span class="col-md-1 col-md-offset-1"><i class="fa bigicon" style=" font-size: 16px">Detalle de Derivacion</i></span> 

                                                        </td>
                                                        <td>
                                                            <textarea type="text"  rows="3" id="detalledederivacion" name="detalledederivacion"  class="form-control" placeholder="Detalle de Derivaci�n"></textarea>

                                                        </td>


                                                    </tr>
                                                </table>
                                            </div>
                                        </td>


                                    </tr>
                                </table>
                                <table>

                                    <tr>
                                        <td style="width: 300px"  ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 16px" >Destino Paciente:</i></span>

                                            <div class="col-md-8">
                                                <select id="destinopaciente" name="destinopaciente" > 
                                                    <option selected value="0"> Destino </option>
                                                    <% for (lugar l : ce.BuscarlosDestinoPacientes()) {%>
                                                    <option value="<%=l.getId_lugar()%>"> <%=l.getNombre()%> </option>
                                                    <%}%>
                                                </select>  
                                            </div>
                                        </td>
                                        <td style="width: 4px">
                                            <span class="col-md-2 col-md-offset-2"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Destino</i></span> 
                                        </td>
                                        <td>


                                            <div class="col-md-12">
                                                <textarea type="text"  rows="3" id="otrodestino" name="otrodestino"  class="form-control" placeholder="Otro Destino"  ></textarea>


                                            </div>  
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div id="ipt" name="ipt" class="tabcontent">
                                <table style="border-top:  #619fd8 2px solid; ">

                                    <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Indicaciones</div></td></tr>


                                </table>
                                <table>
                                    <tr>


                                        <td  colspan="4">
                                            <%        for (lugar lu : ce.buscarlasindicacionesporEspecialidad(idespecialidad)) {

                                                    if (lu.getId_lugar() != 0) {

                                            %>

                                            <div class="col-md-8">

                                                <input type="checkbox" id="indicaciones<%=lu.getId_atencion()%>" name="indicaciones<%=lu.getId_atencion()%>" value="checkbox" onchange="
                                                    var pre = document.getElementById('indicaciones<%=lu.getId_atencion()%>').checked;
                                                    if (pre) {


                                                        document.getElementById('recetadetalle<%=lu.getId_atencion()%>').style.display = 'block';
                                                    } else
                                                        document.getElementById('recetadetalle<%=lu.getId_atencion()%>').style.display = 'none';"><%=lu.getNombre()%> &nbsp;
                                            </div>

                                            <br>
                                            <div class="col-md-12" id="recetadetalle<%=lu.getId_atencion()%>" name="recetadetalle<%=lu.getId_atencion()%>" style=" display: none">
                                                <textarea type="text"  rows="1" id="descripcionreceta<%=lu.getId_atencion()%>" name="descripcionreceta<%=lu.getId_atencion()%>"  class="form-control" placeholder="Dosis Diaria,V�a de admision,Periodo."  ></textarea>


                                            </div> 

                                            <%
                                                    }

                                                }%>  
                                        </td>

                                        <td  style="width: 10px">
                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otras Indicaciones</i></span> 
                                        </td>
                                        <td>


                                            <div class="col-md-12">
                                                <textarea type="text"  rows="3" id="otroindicaciones" name="otroindicaciones"  class="form-control" placeholder="Otra Indicacion"  ></textarea>


                                            </div>  
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div id="srpt" name="srpt" class="tabcontent">

                                <table style="border-top:  #619fd8 2px solid; ">
                                    <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Solicitud Quirurgico</div></td></tr>
                                    <tr>
                                        <td colspan="4">
                                            <input type="checkbox" id="solicituddepabellon" name="solicituddepabellon" value="checkbox"
                                                   onchange="javascritp:

                                                               var pre = document.getElementById('solicituddepabellon').checked;
                                                       if (pre) {


                                                           document.getElementById('numeros').style.display = 'block';
                                                       } else
                                                           document.getElementById('numeros').style.display = 'none';">         Quiere Solicitar Pabell�n Quirurgico

                                        </td>


                                    </tr>  
                                </table>
                                <table>
                                    <tr>
                                        <td>
                                            <div id="numeros" name="numeros" style=" display: none">
                                                <table style=" width: 320px; align-content: center" >
                                                    <tr>

                                                        <td style="width: 10px">
                                                            <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon">Cuantas Solicitudes?</i></span> 
                                                        </td>
                                                        <td width="80px">
                                                            <input id="numerodesolicitudes" size="2" placeholder="Cantidad N�"  onkeypress="return numeros(event)" name="numerodesolicitudes" type="number" >
                                                        </td>      
                                                        <td width="80px">
                                                            <div class="form-group">
                                                                <div class="col-md-6 text-center">
                                                                    <button type="button" class="btn  btn-primary " onclick="javascript :solicitudpabellon()">Solicitar</button> 

                                                                </div> 
                                                            </div>
                                                        </td>
                                                    </tr>  

                                                </table>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div id="respuesta" name="respuesta">

                                                            </div>
                                                        </td>

                                                    </tr>
                                                </table>

                                            </div>        
                                        </td>

                                    </tr>  
                                </table>
                            </div>

                            <div id="nrpt" name="nrpt" class="tabcontent">

                                <table style="border-top:  #619fd8 2px solid; ">
                                    <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">No Indicaci�n de Cirug�a</div></td></tr>

                                </table>
                                <table>
                                    <tr>
                                        <td>

                                            <table style=" width: 520px; align-content: center" >
                                                <tr>

                                                    <td style="width: 100px">
                                                        <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 20px">Cuantas no indicaciones de Cirug�a?</i></span> 
                                                    </td>
                                                    <td width="100px">
                                                        <input id="numerodesolicitudesnrpt" size="2" placeholder="Cantidad N�"  onkeypress="return numeros(event)" name="numerodesolicitudesnrpt" type="number" >
                                                    </td>      
                                                    <td width="100px">
                                                        <div class="form-group">
                                                            <div class="col-md-6 text-center">
                                                                <button type="button" class="btn  btn-primary " onclick="javascript :solicitudnrp()">Solicitar</button> 

                                                            </div> 
                                                        </div>
                                                    </td>
                                                </tr>  

                                            </table>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div id="respuestansp" name="respuestansp">

                                                        </div>
                                                    </td>

                                                </tr>
                                            </table>


                                        </td>

                                    </tr>  
                                </table>

                            </div>
                            <!-- excepciion de garantia y solicitud de examenes -->

                            <div id="eg" name="eg" class="tabcontent" >
                                <table >
                                    <tr><td> &nbsp;</td></tr>
                                    <td >
                                        <span class="col-md-6 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Problema de Salud AUGE:</i></span> 
                                    </td>
                                    <td>
                                        <input  style="width:300px;height:100px"  id="problemaauge"  placeholder="Problema de Salud AUGE"   name="problemaauge" type="text" >
                                    </td>  
                                    <tr><td> &nbsp;</td></tr>
                                    <tr><td><div class=" letra"><strong> Causal de excepcion</strong></div></td></tr>
                                    <tr><td> &nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="rpd" name="rpd" value="checkbox"> &nbsp;&nbsp;
                                            <i class="fa fa-check bigicon" style="font-size:16px"> Rechazo del prestador Designado  </i>
                                        </td>  
                                        <td>
                                            <input type="checkbox" id="rapg" name="rapg" value="checkbox"> &nbsp;&nbsp;
                                            <i class="fa fa-check bigicon" style="font-size:16px"> Rechazo de la atenci�n o procedimiento garantizado  </i>
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="pr" name="pr" value="checkbox"> &nbsp;&nbsp;
                                            <i class="fa fa-check bigicon" style="font-size:16px">Prestaci�n Rechazada  </i>
                                        </td>  
                                        <td>
                                            <input type="checkbox" id="ocdp" name="ocdp" value="checkbox"> &nbsp;&nbsp;
                                            <i class="fa fa-check bigicon" style="font-size:16px"> Otra Causa definida por el paciente </i>
                                        </td>  
                                    </tr>

                                    <tr><td><div class=" letra"> Observacion:</div></td></tr>
                                    <tr><td> &nbsp;</td></tr>
                                    <tr>
                                        <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                            <div class="col-md-8">
                                                <textarea type="text"  rows="3" id="observaciongarantia" name="observaciongarantia"  class="form-control" placeholder="OBSERVACIONES(complementar selecci�n anterior)"  ></textarea>


                                            </div>  
                                        </td>

                                    </tr>

                                    <tr><td> &nbsp;</td></tr>
                                </table>

                            </div>

                            <div id="sdc" name="sdc" class="tabcontent">
                                <table style="border-top:  #619fd8 2px solid; ">
                                    <tr><td colspan="3" style="width: 30px">
                                            <div class=" letra " style=" font-size: 20px">Solicitud de Control</div>


                                            <div  class="col-md-6 col-md-offset-1 text-center">

                                                <select id="controlsolicito" name="controlsolicito" onchange="
                                                    var pre = document.getElementById('controlsolicito').value;
                                                    if (pre == 1) {
                                                        document.getElementById('fechacontrol').disabled = false;
                                                        document.getElementById('motivocontrol').disabled = false;
                                                    } else {
                                                        document.getElementById('fechacontrol').value = '';
                                                        document.getElementById('motivocontrol').value = '';
                                                        document.getElementById('fechacontrol').disabled = true;
                                                        document.getElementById('motivocontrol').disabled = true;
                                                    }

                                                        " > 


                                                    <option selected  value="-1"> Seleccionar </option>

                                                    <% for (int i = 0; i < idpertinencia.length; ++i) {

                                                            if (idpertinencia[i] == pre) {%>
                                                    <option selected value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                        <%} else {%>

                                                    <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                        <% }
                                                            }
                                                        %>

                                                </select>  
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table id="control" name="control" style=" width: 100%; "  >

                                    <tr>
                                        <td colspan="6">
                                            <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-check bigicon" style="font-size:18px">Fecha Control </i></span>
                                            <div class="col-md-8">
                                                <input type="text" maxlength="13" size="12" id="fechacontrol" name="fechacontrol" disabled ><br> </td>
                                            </div>  
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                            <div class="col-md-8">
                                                <textarea type="text"  rows="3" id="motivocontrol" name="motivocontrol"  class="form-control" placeholder="Motivo Control" disabled  ></textarea>


                                            </div>  
                                        </td>
                                    </tr>
                                </table>


                            </div>


                            <div  id="seo" name="seo" class="tabcontent">


                                <table style="border-top:  #619fd8 2px solid; ">
                                    <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Solicitud de Examen o Procedimiento</div></td></tr>

                                </table>
                                <table style=" width: 100%">
                                    <tr>
                                        <td>
                                            <i class="fa fa-check bigicon" style="font-size:16px">Examenes </i>


                                            <select class="selectpicker col-md-6" id="examenes" name="examenes"  title="Examenes a Solicitar ">
                                                <%for (lugar lu : ce.buscarlosExamenesporEspecialidad(idespecialidad)) {%>
                                                <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%></option>

                                                <%}%>
                                            </select>

                                        </td>
                                        <td>
                                            <i class="fa fa-check bigicon" style="font-size:16px">Lateralidad</i> 
                                            <select class="selectpicker col-md-6" id="lateralidadsep" name="lareralidadsep"  title="Lateralidad ">
                                                <%for (int i = 1; i < lateralidad.length; ++i) {%>
                                                <option value="<%=i%>"><%=lateralidad[i]%></option>

                                                <%}%>
                                            </select>
                                        </td>
                                        <td>
                                            <i class="fa fa-check bigicon" style="font-size:16px">Realizar en </i> 
                                            <select class="selectpicker col-md-6" id="tiemposep" name="tiemposep"  title="Realizar en">
                                                <%for (lugar l : otrocontrolador.buscarTiempo()) {%>
                                                <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%></option>

                                                <%}%>
                                            </select>     
                                        </td>
                                        <td>
                                            <input class="btn-primary" type="button" value="Agregar Examenes" name="btn_rut" onclick="javascript: addRow('myTable');" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <table id="myTable" name="myTable" style=" width: 100% " class=" table table-striped" cellspacing="0" border="1">

                                                <tbody>



                                                </tbody>

                                            </table>
                                        </td>

                                    </tr>
                                    <tr>

                                        <td>
                                            <i class="fa fa-check bigicon" style="font-size:16px">Procedimientos</i>

                                            <select class="selectpicker col-md-6" id="procedimientos" name="procedimientos" title="Procedimientos a Solicitar ">
                                                <%for (lugar lu : ce.buscarlosProcedimientosporEspecialidad(idespecialidad)) {%>
                                                <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%></option>

                                                <%}%>
                                            </select>

                                        </td>
                                        <td>
                                            <i class="fa fa-check bigicon" style="font-size:16px">Lateralidad</i> 
                                            <select class="selectpicker col-md-6" id="lateralidadsepro" name="lareralidadsepro"  title="Lateralidad ">
                                                <%for (int i = 1; i < lateralidad.length; ++i) {%>
                                                <option value="<%=i%>"><%=lateralidad[i]%></option>

                                                <%}%>
                                            </select>
                                        </td>
                                        <td>
                                            <i class="fa fa-check bigicon" style="font-size:16px">Realizar en </i> 
                                            <select class="selectpicker col-md-6" id="tiemposepro" name="tiemposepro"  title="Realizar en">
                                                <%for (lugar l : otrocontrolador.buscarTiempo()) {%>
                                                <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%></option>

                                                <%}%>
                                            </select>     
                                        </td>
                                        <td>
                                            <input class="btn-primary" type="button" value="Agregar Procedimiento" name="btn_rut" onclick="javascript: addRow1('myTable1');" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <table id="myTable1" name="myTable1" style=" width: 100% " class=" table table-striped" cellspacing="0" border="1">

                                                <tbody>



                                                </tbody>

                                            </table>
                                        </td>

                                    </tr>
                                    <tr><td><div class=" letra"> Observacion:</div></td></tr>
                                    <tr><td> &nbsp;</td></tr>
                                    <tr>
                                        <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                            <div class="col-md-8">
                                                <textarea type="text"  rows="3" id="observacionsolicitud" name="observacionsolicitud"  class="form-control" placeholder="OBSERVACIONES"  ></textarea>


                                            </div>  
                                        </td>

                                    </tr>
                                </table>

                            </div>

                        </div>
                        <table>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary btn-lg">Guardar </button>

                                            <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                        </div>
                                    </div> 
                                </td>
                            </tr>


                        </table>



                    </div>
                </form>
            </div>
        </article>
    </section>
    <script>


        function numeros(e)  // 1

        {
            var key = window.Event ? e.which : e.keyCode
            return (key >= 48 && key <= 57)
        }


        function solicitudpabellon() {

            var solicitudes = document.forms["hojaatencionoftalmologia"]["numerodesolicitudes"].value;
            var especialidad = document.forms["hojaatencionoftalmologia"]["especialidad"].value;
            if (solicitudes == "") {

                Alert.render("Debe Completar los Datos para Continuar");
                return false;
            } else
            {
                try
                {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e)
                {
                    try
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (E) {
                        xmlhttp = false;
                    }
                }
                if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                    xmlhttp = new XMLHttpRequest();
                }

                if (xmlhttp) {
                    var objeto_recibidor = document.getElementById("respuesta");
                    xmlhttp.open("post", "solicitud.jsp?cantidad=" + solicitudes + "&especialidad=" + especialidad);
                    xmlhttp.send("");
                    if (xmlhttp.readyState == 1) {
                        objeto_recibidor.innerHTML = '</br></br><b> </b>';
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            objeto_recibidor.innerHTML = xmlhttp.responseText;
                        }
                        if (xmlhttp.status != 200) {
                            objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';
                        }
                    }
                }
            }
        }


        function solicitudnrp() {


            var solicitudes = document.forms["hojaatencionoftalmologia"]["numerodesolicitudesnrpt"].value;
            var especialidad = document.forms["hojaatencionoftalmologia"]["especialidad"].value;
            if (solicitudes == "") {

                Alert.render("Debe Completar los Datos para Continuar");
                return false;
            } else
            {
                try
                {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e)
                {
                    try
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (E) {
                        xmlhttp = false;
                    }
                }
                if (!xmlhttp && typeof XMLHttpRequest != "undefined") {
                    xmlhttp = new XMLHttpRequest();
                }

                if (xmlhttp) {
                    var objeto_recibidor = document.getElementById("respuestansp");
                    xmlhttp.open("post", "solicitudnoindicacion.jsp?cantidad=" + solicitudes + "&especialidad=" + especialidad);
                    xmlhttp.send("");
                    if (xmlhttp.readyState == 1) {
                        objeto_recibidor.innerHTML = '</br></br><b> </b>';
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            objeto_recibidor.innerHTML = xmlhttp.responseText;
                        }
                        if (xmlhttp.status != 200) {
                            objeto_recibidor.innerHTML = 'ERROR EN EL SISTEMA... FAVOR LLAMAR A INFORMATICA';
                        }
                    }
                }
            }
        }

        /*validar excepcion de garantia*/
        function validarexcepciongarantia() {

            var problemaauge = document.forms["hojaatencionoftalmologia"]["problemaauge"].value;
            var rpd = document.forms['hojaatencionoftalmologia']['rpd'].checked;
            var rapg = document.forms['hojaatencionoftalmologia']['rapg'].checked;
            var pr = document.forms['hojaatencionoftalmologia']['pr'].checked;
            var ocdp = document.forms['hojaatencionoftalmologiahojaatencionoftalmologia']['ocdp'].checked;
            var observaciongarantia = document.forms["hojaatencionoftalmologia"]["observaciongarantia"].value;
            if (problemaauge != "") {
                if (!rpd && !rapg && !pr && !ocdp && observaciongarantia == "") {
                    return false;
                }
            }
            if (rpd && rapg && pr && ocdp) {
                if (observaciongarantia == "" && problemaauge == "") {
                    return false;
                }
            }

        }

        function validarcontrol() {

            var fechacontrol = document.forms["hojaatencionoftalmologia"]["fechacontrol"].value;
            var motivocontrol = document.forms["hojaatencionoftalmologia"]["motivocontrol"].value;
            var cotrol = document.forms["hojaatencionoftalmologia"]["controlsolicito"].value;
            if (cotrol == "-1") {
                Alert.render("Debe Indicar si el paciente requiere Control -> En solicitud de Control!!Complete los datos para Continuar");
                return false;
            } else if (cotrol == "1") {
                if (fechacontrol == "" || motivocontrol == "") {
                    Alert.render("Debe Indicar Fecha o Motivo del control!!Complete los datos para Continuar");
                    return false;
                }
            }

        }




        function validar() {

            var sic = document.forms["hojaatencionoftalmologia"]["sic"].value;
            var detalledederivacion = document.forms["hojaatencionoftalmologia"]["detalledederivacion"].value;
            var destino = document.forms["hojaatencionoftalmologia"]["destinopaciente"].value;
            var otrodestino = document.forms["hojaatencionoftalmologia"]["otrodestino"].value;
            var diagnosticosic = document.forms["hojaatencionoftalmologia"]["diagnosticosic"].value;
            var otrodiagnosticosic = document.forms["hojaatencionoftalmologia"]["otrodiagnosticosic"].value;
            var numerodesolicitudes = document.forms["hojaatencionoftalmologia"]["numerodesolicitudes"].value;
            var numeronoindicaciones = document.forms["hojaatencionoftalmologia"]["numerodesolicitudesnrpt"].value;

            var atencion = document.forms["hojaatencionoftalmologia"]["idatencioncita"].value;
            // debo preguntar si el tipo de atencion es nuevo.
            //  var pertinencia = document.forms["hojaatencionoftalmologia"]["pertinencia"].value;

            var problemaauge = document.forms["hojaatencionoftalmologia"]["problemaauge"].value;
            var rpd = document.forms["hojaatencionoftalmologia"]["rpd"].checked;
            var rapg = document.forms["hojaatencionoftalmologia"]["rapg"].checked;
            var pr = document.forms["hojaatencionoftalmologia"]["pr"].checked;
            var ocdp = document.forms["hojaatencionoftalmologia"]["ocdp"].checked;
            var observaciongarantia = document.forms["hojaatencionoftalmologia"]["observaciongarantia"].value;
            if (problemaauge != "") {
                if ((!rpd & !rapg & !pr & !ocdp) || observaciongarantia == "") {
                    Alert.render("Debe completar los datos de la Excepcion de Garantia!!Complete los datos para Continuar");
                    return false;
                }
            }
            if (rpd || rapg || pr || ocdp) {

                if (observaciongarantia == "" || problemaauge == "") {
                    Alert.render("Debe completar los datos de la Excepcion de Garantia!!Complete los datos para Continuar");
                    return false;
                }
            }

            if (sic == "1" & (diagnosticosic == "0" || detalledederivacion == "" || (destino == "0" & otrodestino == ""))) {
                Alert.render("Debe Colocar el Destino , el diagnostico por el que deriva y el Detalle de la Derivacion!!Ya que indic� que SIC es SI");
                return false;
            }
            if (sic == "1" & diagnosticosic == "-1" & otrodiagnosticosic == "") {
                Alert.render("Debe indicar Diagnostico por el que deriva!!Complete los datos para Continuar");
                return false;
            }

            // validar indicacion de lentes


            /*validar pertinencia*/






            if (atencion == 1) {
                var pertinencia = document.forms["hojaatencionoftalmologia"]["pertinencia"].value;
                if (pertinencia == -1) {
                    Alert.render("Como su atencion es Nueva debe indicar si �Es pertinente la derivaci�n a su atenci�n? !! Debe Completar los Datos para Continuar");
                    return false;
                }
            }


            //validar diagnostico
            var falta = 0;
            var mensaje = "";
        <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {%>
            var atencion = <%=lu.getId_atencion()%>


            var diagnostico = document.forms["hojaatencionoftalmologia"]["diagnostico" + atencion].checked;
            if (diagnostico) {

                var lateralidad = document.forms["hojaatencionoftalmologia"]["lateralidad" + atencion].value;
                var ges = document.forms["hojaatencionoftalmologia"]["ges" + atencion].value;
                var confirmadescarta = document.forms["hojaatencionoftalmologia"]["confirmadescarta" + atencion].value;
                var tratamientoeindicaciones = document.forms["hojaatencionoftalmologia"]["tratamientoeindicaciones" + atencion].value;
                var fundamentodeldiagnostico = document.forms["hojaatencionoftalmologia"]["fundamentodeldiagnostico" + atencion].value;
                var confirmadescartad = document.forms["hojaatencionoftalmologia"]["confirmodecartod" + atencion].value;
                if (lateralidad == "0" || confirmadescartad == "0" || ges == "0") {
                    mensaje = "Debe Completar los datos del Diagnostico, su lateralidad, si se confirma o Descarta y si es Ges o no Ges!! Complete los datos para continuar ";
                    falta = 1;
                }
                if (confirmadescarta == "1" & (tratamientoeindicaciones == "" || fundamentodeldiagnostico == "")) {
                    mensaje = "Debe Completar el Tratamiento y Fundamento para el IPD y Constancia Ges!! Complete los datos para continuar";
                    falta = 1;
                }
            }

        <%}%>

            if (falta == 1) {

                Alert.render(mensaje);
                return false;
            }


            /*validar receta medica*/
            var faltoreceta = false;
        <%  for (lugar lu : ce.buscarlasindicacionesporEspecialidad(idespecialidad)) {
                    if (lu.getId_lugar() != 0) {%>

            var pre = document.getElementById('indicaciones<%=lu.getId_atencion()%>').checked;
            if (pre) {
                var detalle = document.getElementById('descripcionreceta<%=lu.getId_atencion()%>').value;
                if (detalle == "") {
                    faltoreceta = true;
                }
            }



        <% }
                }%>

            if (faltoreceta == true) {
                Alert.render("Debe colocar el detalle de la indicacion Dosis diaria, Periodo para completar la receta, !! Complete los datos para continuar");
                return false;
            }


            var pre = document.getElementById('solicituddepabellon').checked;
            if (pre) {
                if (numerodesolicitudes == "") {
                    Alert.render("Debe Indicar Cuantas Solicitudes de Pabellon Requiere y dar clic en Solicitar!! Complete los datos para continuar");
                    return false;
                } else {
                    var malo = 0;
                    for (i = 0; i < numerodesolicitudes; ++i) {
                        var d = document.forms["hojaatencionoftalmologia"]["diagnosticos" + i].value;
                        var od = document.forms["hojaatencionoftalmologia"]["otrodiagnostico" + i].value;
                        var inter = document.forms["hojaatencionoftalmologia"]["intervencion" + i].value;
                        var a = document.forms["hojaatencionoftalmologia"]["anestesia" + i].value;
                        var r = document.forms["hojaatencionoftalmologia"]["requerimientos" + i].value;
                        var o = document.forms["hojaatencionoftalmologia"]["observacion" + i].value;
                        var l = document.forms["hojaatencionoftalmologia"]["lateralidades" + i].value;
                        if (d == "0" || inter == "" || r == "" || o == "" || a == "0" || l == "0") {
                            malo = 1;
                        }

                        if (d == "-1") {
                            if (od == "") {
                                malo = 1;
                            }
                        }
                    }

                    if (malo == 1) {
                        Alert.render("Debe Indicar la informacion de las solicitudes de Pabellon!! Complete los datos para continuar");
                        return false;
                    }
                }


            }


            /*validacion para no indicaciones quirurgicas*/



            var falto = 0;
            for (i = 0; i < numeronoindicaciones; ++i) {
                var d = document.forms["hojaatencionoftalmologia"]["diagnosticonrpt" + i].value;
                var od = document.forms["hojaatencionoftalmologia"]["otrodiagnosticonrpt" + i].value;
                var inter = document.forms["hojaatencionoftalmologia"]["intervencionnrpt" + i].value;
                var razones = document.forms["hojaatencionoftalmologia"]["razonesnrpt" + i].value;
                var indicaciones = document.forms["hojaatencionoftalmologia"]["indicacionesnrpt" + i].value;
                var l = document.forms["hojaatencionoftalmologia"]["lateralidadesnrpt" + i].value;
                if (d == "0" || inter == "" || razones == "" || indicaciones == "" || l == "0") {
                    falto = 1;
                }

                if (d == "-1") {
                    if (od == "") {
                        falto = 1;
                    }
                }
            }

            if (falto == 1) {
                Alert.render("Debe Indicar la informacion de las solicitudes de no indicaci�n quir�rgica!! Complete los datos para continuar");
                return false;
            }

            var fechacontrol = document.forms["hojaatencionoftalmologia"]["fechacontrol"].value;
            var motivocontrol = document.forms["hojaatencionoftalmologia"]["motivocontrol"].value;
            var cotrol = document.forms["hojaatencionoftalmologia"]["controlsolicito"].value;
            if (cotrol == "-1") {
                Alert.render("Debe Indicar si el paciente requiere Control -> En solicitud de Control!!Complete los datos para Continuar");
                return false;
            } else if (cotrol == "1") {
                if (fechacontrol == "" || motivocontrol == "") {
                    Alert.render("Debe Indicar Fecha o Motivo del control!!Complete los datos para Continuar");
                    return false;
                }
            }

            /*receta medica*/
        <%if (c.getId_tipoatencion() != 5 || c.getId_tipoatencion() != 4) {%>
            var cotrollente = document.forms["hojaatencionoftalmologia"]["lentesolicito"].value;
            if (cotrollente == "-1") {
                Alert.render("Debe Indicar si el paciente requiere Lentes  -->Receta de Lentes!!Complete los datos para Continuar");
                return false;
            } else if (cotrollente == "1") {

                var tratamientocerca = document.forms["hojaatencionoftalmologia"]["tratamientolentesCerca"].checked;
                var tratamientolejos = document.forms["hojaatencionoftalmologia"]["tratamientolentesLejos"].checked;
                var lejosaddod = document.forms["hojaatencionoftalmologia"]["odf"].value;
                var lejosesfod = document.forms["hojaatencionoftalmologia"]["odc"].value;
                var lejoscylod = document.forms["hojaatencionoftalmologia"]["ode"].value;
                var lejosejeod = document.forms["hojaatencionoftalmologia"]["oie"].value;
                var lejosaddoi = document.forms["hojaatencionoftalmologia"]["oic"].value;
                var lejosesfoi = document.forms["hojaatencionoftalmologia"]["oiej"].value;
                var lejoscyloi = document.forms["hojaatencionoftalmologia"]["add"].value;


                if (tratamientocerca || tratamientolejos) {

                    if (lejosaddod == "" & lejosesfod == "" & lejoscylod == "" & lejosejeod == "" & lejosaddoi == "" & lejosesfoi == "" & lejoscyloi == "") {
                        Alert.render("Debe Ingresar la Receta de Lentes!!Complete los datos para Continuar");
                        return false;

                    }


                } else {
                    Alert.render("Debe Indicar que lentes se va a indicar !!Complete los datos para Continuar");
                    return false;

                }
            }
        <%}%>


        }

        /*ingresar Examenes*/


        function addRow(id) {

            var examenes = document.forms["hojaatencionoftalmologia"]["examenes"].value;

            var combo = document.getElementById("examenes");
            var selected = combo.options[combo.selectedIndex].text;

            var lateralidad = document.forms["hojaatencionoftalmologia"]["lateralidadsep"].value;
            var combo2 = document.getElementById("lateralidadsep");
            var selected2 = combo2.options[combo2.selectedIndex].text;

            var tiempo = document.forms["hojaatencionoftalmologia"]["tiemposep"].value;
            var combo3 = document.getElementById("tiemposep");
            var selected3 = combo3.options[combo3.selectedIndex].text;

            if (examenes != "" && lateralidad != "" && tiempo != "") {

                var tbody = document.getElementById
                        (id).getElementsByTagName("TBODY")[0];
                var row = document.createElement("TR");
                row.setAttribute('style', ' text-align: center');
                var td1 = document.createElement("TD");
                td1.appendChild(document.createTextNode(selected + " " + selected2 + " Realizar en  " + selected3))

                row.appendChild(td1);

                var td2 = document.createElement("TD");

                var boton = document.createElement('button');
                boton.type = "button";

                boton.setAttribute('class', 'button2');
                boton.innerHTML = "x";

                boton.onclick = function () {
                    var td = this.parentNode;
                    var tr = td.parentNode;
                    var table = tr.parentNode;
                    table.removeChild(tr);
                    return false;
                };

                td2.appendChild(boton);
                row.appendChild(td2);

                var td = document.createElement("TD");

                var div = document.createElement("input");
                div.id = "solicitudesexamenes" + examenes;
                div.name = "solicitudesexamenes" + examenes;
                div.value = examenes;
                div.type = "hidden";
                td.appendChild(div);

                var div2 = document.createElement("input");
                div2.id = "lateralidadexamenes" + examenes;
                div2.name = "lateralidadexamenes" + examenes;
                div2.value = lateralidad;
                div2.type = "hidden";
                td.appendChild(div2);
                var div3 = document.createElement("input");
                div3.id = "tiempoexamenes" + examenes;
                div3.name = "tiempoexamenes" + examenes;

                div3.value = tiempo;
                div3.type = "hidden";
                td.appendChild(div3);

                row.appendChild(td);



                tbody.appendChild(row);

            } else {

                Alert.render("Debe Ingresar Examen, Lateralidad y Tiempo en que se debe realizar !! Complete los datos para continuar");
                return false;
            }
        }


        function addRow1(id) {


            var procedimiento = document.forms["hojaatencionoftalmologia"]["procedimientos"].value;


            var combo = document.getElementById("procedimientos");
            var selected = combo.options[combo.selectedIndex].text;


            var lateralidad = document.forms["hojaatencionoftalmologia"]["lateralidadsepro"].value;
            var combo2 = document.getElementById("lateralidadsepro");
            var selected2 = combo2.options[combo2.selectedIndex].text;

            var tiempo = document.forms["hojaatencionoftalmologia"]["tiemposepro"].value;
            var combo3 = document.getElementById("tiemposepro");
            var selected3 = combo3.options[combo3.selectedIndex].text;

            if (procedimiento != "" && lateralidad != "" && tiempo != "") {

                var tbody = document.getElementById
                        (id).getElementsByTagName("TBODY")[0];
                var row = document.createElement("TR");
                row.setAttribute('style', ' text-align: center');
                var td1 = document.createElement("TD");
                td1.appendChild(document.createTextNode(selected + " " + selected2 + " Realizar en  " + selected3))

                row.appendChild(td1);

                var td2 = document.createElement("TD");

                var boton = document.createElement('button');
                boton.type = "button";

                boton.setAttribute('class', 'button2');
                boton.innerHTML = "x";

                boton.onclick = function () {
                    var td = this.parentNode;
                    var tr = td.parentNode;
                    var table = tr.parentNode;
                    table.removeChild(tr);
                    return false;
                };

                td2.appendChild(boton);
                row.appendChild(td2);

                var td = document.createElement("TD");

                var div = document.createElement("input");
                div.id = "solicitudesprocedimiento" + procedimiento;
                div.name = "solicitudesprocedimiento" + procedimiento;
                div.value = procedimiento;
                div.type = "hidden";
                td.appendChild(div);

                var div2 = document.createElement("input");
                div2.id = "lateralidadprocedimiento" + procedimiento;
                div2.name = "lateralidadprocedimiento" + procedimiento;
                div2.value = lateralidad;
                div2.type = "hidden";
                td.appendChild(div2);
                var div3 = document.createElement("input");
                div3.id = "tiempoprocedimiento" + procedimiento;
                div3.name = "tiempoprocedimiento" + procedimiento;

                div3.value = tiempo;
                div3.type = "hidden";
                td.appendChild(div3);

                row.appendChild(td);



                tbody.appendChild(row);

            } else {

                Alert.render("Debe Ingresar Procedimiento, Lateralidad y Tiempo en que se debe realizar !! Complete los datos para continuar");
                return false;
            }
        }


        function borrarFila(t) {
            var td = t.parentNode;
            var tr = td.parentNode;
            var table = tr.parentNode;
            table.removeChild(tr);
        }


        $(function () {
            $("#fechacontrol").datepicker();
        });

        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>

</body>
<jsp:include page="../comunes/Footer.jsp"/>
