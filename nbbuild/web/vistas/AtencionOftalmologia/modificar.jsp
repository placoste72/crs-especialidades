<%-- 
    Document   : modificar
    Created on : 28-03-2019, 12:30:41
    Author     : a
--%>

<%@page import="Modelos.tipo_anestesia"%>
<%@page import="Modelos.atencion_clinica_tecnologo"%>
<%@page import="java.util.Vector"%>
<%@page import="Modelos.solicitudExamenes_Procedimientos"%>
<%@page import="Modelos.lugar"%>
<%@page import="Modelos.excepcion_garantia"%>
<%@page import="Modelos.receta_lente_opticos"%>
<%@page import="Modelos.atencion"%>
<%@page import="Controlador.controlador_atencion_clinica_oftalmologia"%>
<%@page import="Controlador.controlador_especialidad"%>
<%@page import="Modelos.cita"%>
<%@page import="Controlador.controlador_cita"%>

<!DOCTYPE html>
<jsp:include page="../comunes/Header.jsp"/>
    <head>
        
        <style>
    .columna {
        width:33%;
        float:left;
    }

    @media (max-width: 500px) {

        .columna {
            width:auto;
            float:none;
        }

    }  

    table{
        font-size: 12px;
    }
    th, td {
        padding: 0px;
    }

</style>
        <%

            String cita = request.getParameter("cita");
            controlador_cita cc = new controlador_cita();
            cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
            controlador_especialidad ce = new controlador_especialidad();
            int idespecialidad = 0;
            idespecialidad = c.getId_patologia();

            Integer[] idpertinencia = {1, 0};
            String[] nombrepertinencia = {"SI", "NO"};
            int pre = -1;
            controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();

            String[] lateralidad = {"Lateralidad", "Ojo derecho", "Ojo izquierdo", "Ambos ojos"};

            atencion atencion = caco.buscaratencionporIdcita(Integer.parseInt(cita));
            pre = atencion.getPertinencia();
            boolean tengoreceta = caco.tengorecetaIdAtencion(atencion.getIdatencion(), 1);
            int r = -1;
            if (tengoreceta == true) {
                r = 1;
            } else {
                r = 0;
            }
            receta_lente_opticos re = caco.buscarRecetadeIdAtencion(atencion.getIdatencion(), 1);
            excepcion_garantia eg = new excepcion_garantia();
            eg = caco.buscarGarantiaporIddeAtencion(atencion.getIdatencion(), 2);
            lugar control = caco.BuscarSolicituddeControl(Integer.parseInt(cita));
            String cfecha = "";
            String cmotivo = "";
            String problemaaugereceta = "";
            String observacionreceta = "";

            if (control.getEstatu() != 0) {
                if ((!control.getFechanacimiento().equals("null")) && (control.getFechanacimiento() != "")) {
                    cfecha = control.getFechanacimiento();
                }
                if ((!control.getDescripcion().equals("null")) && (control.getDescripcion() != "")) {
                    cmotivo = control.getDescripcion();
                }
            }

            if (eg.getIdexcepcion_garantia() != 0) {
                if ((!eg.getProblemaauge().equals("null")) && (eg.getProblemaauge() != "")) {
                    problemaaugereceta = eg.getProblemaauge();
                }
                if ((!eg.getObservaciones().equals("null")) && (eg.getObservaciones() != "")) {
                    observacionreceta = eg.getObservaciones();
                }

            }

            solicitudExamenes_Procedimientos sep = caco.buscarexamenesProcedimientos(atencion.getIdatencion());
            Vector<lugar> cantidadsolicitud = caco.buscarSolicitudes(atencion.getIdatencion());
            Vector<lugar> cantidadnosolicitud = caco.buscarNoindicaciones(atencion.getIdatencion());

            String obseresolicitud = "";
            if (sep.getIdsolicitud() != 0) {

                if ((sep.getObservacion().equals("null")) && (sep.getObservacion() != "")) {
                    obseresolicitud = sep.getObservacion();
                }
            }

        %>
    </head>
    <body>
        <section  class="section">
        <nav class="nav1 ">
            <table class="table table-bordered" style="width: 100%">
                <thead><tr>
                        <th colspan="4" class="btn-primary" style="width: 50%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos del Paciente</th>
                    </tr>
                    <tr>
                        <th>RUT</th>
                        <th>Paciente</th>
                        <th>Edad</th>
                        <th>Sexo</th>


                    </tr>

                </thead>

                <tr>
                    <td><%= c.getMotivo_cancela()%></td>
                    <td><%= c.getRut_paciente()%></td>
                    <td><%= c.getTemporales2()%></td>
                    <td><%= c.getTemporales1()%></td>


                </tr>
            </table>

            <table class="table table-bordered" style="width: 100%">
                <thead><tr>
                        <th colspan="6" class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; text-align: left">Datos de la Citaci�n</th>
                    </tr>
                    <tr>
                        <th>Especialidad</th>
                        <th>Tipo de Cita</th>
                        <th>Diagn�stica</th>


                        <th>Fecha y Hora</th>


                    </tr>

                </thead>

                <tr>
                    <td><%= c.getTemporales()%> </td>
                    <td><%= c.getTemporales3()%>  </td>
                    <td><%= c.getTemporales4()%></td>


                    <td><%= c.getMotivo()%></td>


                </tr>
            </table>
            <p class="btn-primary" style="width: 100%; padding: 6px 12px;margin: 4px 2px; border-radius: 8px; height: 25px; font-size: 10px; text-align: left">Datos otras atenciones</p>

            <table class="table-striped" >
                <tr>
                    <th>Fecha</th>
                    <th>Atencion</th>
                    <th>Especialidad</th>
                    <th>Profesional</th>
                    <th>Detalle</th>
                </tr>
                <%for (atencion_clinica_tecnologo at : cc.BuscarAtencionesdeunPaciente(c.getMotivo_cancela())) {

                %>
                <tr>
                    <td><%=at.getAuxiliarcinco()%></td>
                    <td><%=at.getAuxiliarseis()%></td>
                    <td><%=at.getAuxiliardos()%></td>
                    <td><%=at.getAuxiliartres()%></td>
                    <%if (at.getAuxiliarcuatro() == 1) {
                    %>
                    <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaUrgenciaDental.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Urgencia Dental', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 2) {%>
                    <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaVicioRefraccion.jsp?atencion=<%=at.getId_atencion_tecnologo()%>', 'Vicio de Refraccion', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 3) {%>
                    <td><a onClick="window.open('<%=cc.getLocal()%>EntrevistaPreoperatoria?identrevista=<%=at.getId_atencion_tecnologo()%>', 'Entrevista Preoperatorio', 'width=1200, height=1000')"  >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 4) {%>
                    <td><a onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaExamenes.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Examenes de Laboratorio', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 5) {%>
                    <td> </td>
                    <%} else if (at.getAuxiliarcuatro() == 6) {%>
                    <td><a onClick="window.open('http://10.8.4.11:9090/Pabellon/ProtocoloNuevoPdf?id_protocolo_operatorio=<%=at.getId_atencion_tecnologo()%>', 'Protocolo Operatorio', 'width=1200, height=1000')"   >

                            Protocolo  </a> 

                        <%int ide = cc.idepicrisis(at.getId_atencion_tecnologo());%>
                        <a onClick="window.open('http://10.8.4.11:9090/Pabellon/epicrisisNuevoPdf?id_epicrisis=<%=ide%>', 'Epicrisis Operatorio', 'width=1200, height=1000')"    >

                            Epicrisis  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 7) {%>
                    <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/vistaHojaDiaria.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Hoja Diaria', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 8) {%>
                    <td><a  onClick="window.open('<%=cc.getLocal()%>InformedePrestacionRealizada?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Imagenolog�a', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%} else if (at.getAuxiliarcuatro() == 9) {%>
                    <td><a  onClick="window.open('<%=cc.getLocallink()%>Ficha/DocumentosOftalmologia.jsp?idatencion=<%=at.getId_atencion_tecnologo()%>', 'Atencion Oftalmologia', 'width=1200, height=1000')"   >

                            VER  </a> </td>
                            <%}%>
                </tr>
                <%

                    }%>

            </table>

        </nav>
                    <article class="article">


        <div id="dialogoverlay"></div>
        <div id="dialogbox">
            <div>
                <div id="dialogboxhead"></div>
                <div id="dialogboxbody"></div>
                <div id="dialogboxfoot"></div>
            </div>
        </div>
        <div class="container">


            <form id="hojaatencionoftalmologia" name="hojaatencionoftalmologia" action="<%=cc.getLocal()%>modificacionAtencionOftalmologia"  method="post" onsubmit="return validar()">
                <div class="tab-content">

                    <legend class="text-center header">Atencion en Box </legend>



                    <%if (c.getId_tipoatencion() == 1) {%>  
                    <table>
                        <tr>
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td colspan="2">
                                <span class="col-md-8   col-md-offset-4 "><i class="fa fa-edit bigicon"  style=" font-size: 20px" >�Es pertinente la derivaci�n a su atenci�n?</i></span>
                            </td>
                            <td>
                                <div class="col-md-8">
                                    <select id="pertinencia" name="pertinencia" > 


                                        <option selected  value="-1"> Seleccionar </option>

                                        <% for (int i = 0; i < idpertinencia.length; ++i) {

                                                if (idpertinencia[i] == pre) {%>
                                        <option selected value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                            <%} else {%>

                                        <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                            <% }
                                                }
                                            %>

                                    </select>  
                                </div>
                            </td>
                        </tr>

                    </table>
                    <%}%>


                    <table style="border-top:  #619fd8 2px solid; ">




                        <input id="idcita" name="idcita" value="<%=cita%>" hidden >
                        <input id="especialidad" name="especialidad" value="<%=idespecialidad%>" hidden >
                        <input id="idatencioncita" name="idatencioncita" value="<%=c.getId_tipoatencion()%>" hidden>

                        <tr> <td style="width: 30px"><div class=" letra " style=" font-size: 20px">Resumen de la Atenci�n</div></td></tr>
                        <tr>

                            <td>


                                <div class="col-md-12">
                                    <textarea type="text"  rows="4" id="resumenatencion" name="resumenatencion"  class="form-control" placeholder="Resumen Atencion" required oninvalid="setCustomValidity('El campo Resumen de la Atenci�n es obligatorio')" oninput="setCustomValidity('')" ><%=atencion.getResumenatenicon()%> </textarea>


                                </div>  
                            </td>
                        </tr>
                    </table>
                    <div class="container">

                        <ul class="nav nav-tabs">
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'dpt')" id="defaultOpen" >Diagnosticos</a></li>
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'dept')" id="defaultOpen">Derivaci�n</a></li>
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'eg')" id="defaultOpen">Excepci�n <BR> de Garant�a</a></li>
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'ept')" id="defaultOpen">Examenes <BR> Realizados</a></li>
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'ipt')" id="defaultOpen">Indicaciones <BR> (Receta Medica)</a></li>

                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'nrpt')" id="defaultOpen">No Indicaci�n<BR> de Cirug�a</a></li>

                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'ppt')" id="defaultOpen">Procedimientos <BR>Realizados</a></li>
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'rm')" id="defaultOpen">Receta<BR>Lente</a></li> 
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'seo')" id="defaultOpen">Solicitud de <BR> Procedimientos<BR>o Examen</a></li>
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'srpt')" id="defaultOpen">Solicitud <BR>de Cirug�a</a></li>
                            <li><a style=" font-size: 12px; font:bold 10px" class="tablinks letra" onclick="openCity(event, 'sdc')" id="defaultOpen">Solicitud <BR>de Control</a></li>
                        </ul>

                        <div id="dpt" name="dpt" class="tabcontent">
                            <table style="border-top:  #619fd8 2px solid; "> 
                                <tr>  
                                    <td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Diagnosticos</div></td>


                                </tr>
                            </table>
                            <table class=" table table-striped" style=" width: 100%"> 
                                <thead>
                                    <tr>
                                        <th>Diagnostico</th>
                                        <th>Lateralidad</th>
                                        <th>Confirma o Descarta Diagnostico</th>
                                        <th>CASO GES</th>
                                        <th>Requiere Documento  </th>
                                    </tr>
                                </thead>

                                <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {%>
                                <tr>

                                    <td style="font-size:12px">
                                        <input type="checkbox"   name="diagnostico<%=lu.getId_atencion()%>" id="diagnostico<%=lu.getId_atencion()%>" value="checkbox" onchange="javascript:
                                                        var pre = document.getElementById('diagnostico<%=lu.getId_atencion()%>').checked;
                                                if (pre == true) {
                                                    document.getElementById('lateralidad<%=lu.getId_atencion()%>').disabled = false;
                                                    document.getElementById('confirmodecartod<%=lu.getId_atencion()%>').disabled = false;
                                                    document.getElementById('ges<%=lu.getId_atencion()%>').disabled = false;
                                                } else {

                                                    document.getElementById('lateralidad<%=lu.getId_atencion()%>').disabled = true;
                                                    document.getElementById('confirmodecartod<%=lu.getId_atencion()%>').disabled = true;
                                                    document.getElementById('ges<%=lu.getId_atencion()%>').disabled = true;
                                                }
                                               "><%=lu.getNombre()%>


                                    </td>
                                    <td> 
                                        <select id="lateralidad<%=lu.getId_atencion()%>" disabled  name="lateralidad<%=lu.getId_atencion()%>">
                                            <option selected value="0" >Lateralidad</option>

                                            <option value="1">Ojo Derecho</option>
                                            <option value="2">Ojo Izquierdo</option>
                                            <option value="3">Ambos Ojos</option>
                                        </select>
                                    </td>
                                    <td> 
                                        <div class="col-md-8">
                                            <select id="confirmodecartod<%=lu.getId_atencion()%>" disabled name="confirmodecartod<%=lu.getId_atencion()%>"> 
                                                <option selected value="0">  Confirma o Descarta </option>

                                                <option value="1"> Si </option>
                                                <option value="2"> No </option>

                                            </select>  
                                        </div>
                                    </td>
                                    <td> 
                                        <div class="col-md-8">
                                            <select id="ges<%=lu.getId_atencion()%>" disabled  name="ges<%=lu.getId_atencion()%>" onchange="javascript:

                                                            var pre = document.getElementById('ges<%=lu.getId_atencion()%>').value;
                                                    if (pre == 1) {


                                                        document.getElementById('documentos<%=lu.getId_atencion()%>').style.display = 'block';
                                                    } else
                                                        document.getElementById('documentos<%=lu.getId_atencion()%>').style.display = 'none';
                                                    document.getElementById('datosipd<%=lu.getId_atencion()%>').style.display = 'none';
                                                    ">

                                                <option selected value="0"> GES </option>

                                                <option value="1"> Si </option>
                                                <option value="2"> No </option>

                                            </select>  
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8" id="documentos<%=lu.getId_atencion()%>" name="documento<%=lu.getId_atencion()%>" style=" display: none">
                                            <select id="confirmadescarta<%=lu.getId_atencion()%>" name="confirmadescarta<%=lu.getId_atencion()%>" onchange="javascript:

                                                            var pre = document.getElementById('confirmadescarta<%=lu.getId_atencion()%>').value;
                                                    if (pre == 1) {


                                                        document.getElementById('datosipd<%=lu.getId_atencion()%>').style.display = 'block';
                                                    } else
                                                        document.getElementById('datosipd<%=lu.getId_atencion()%>').style.display = 'none';" >  > 
                                                <option selected value="0"> Documento </option>

                                                <option value="1"> Si </option>
                                                <option value="2"> No </option>

                                            </select>  
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="10">

                                        <table class="table-striped" style=" display: none; width: 100%" id="datosipd<%=lu.getId_atencion()%>">
                                            <tr>
                                                <td></td>
                                                <td >
                                                    <div class=" letra " style=" font-size: 16px">Datos para IPD.</div>
                                                </td>
                                                <td >
                                                    <div class="col-md-12">
                                                        <textarea   rows="3" id="fundamentodeldiagnostico<%=lu.getId_atencion()%>" name="fundamentodeldiagnostico<%=lu.getId_atencion()%>"  class="form-control" placeholder="Fundamento del Diagnostico"  ></textarea>

                                                    </div>

                                                </td >
                                                <td >
                                                    <div class="col-md-12">
                                                        <textarea rows="3" id="tratamientoeindicaciones<%=lu.getId_atencion()%>" name="tratamientoeindicaciones<%=lu.getId_atencion()%>"  class="form-control" placeholder="Tratamiento e Indicaciones"  ></textarea>
                                                    </div>
                                                </td> 
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <% }%>  
                            </table>





                            <table >

                                <tr>

                                    <td style="width: 10px">
                                        <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Diagnostico</i></span> 
                                    </td>
                                    <td colspan="3">


                                        <div class="col-md-12">
                                            <textarea type="text"  rows="3" id="otrodiagnostico" name="otrodiagnostico"  class="form-control" placeholder="Otro Diagnostico"  ><%=atencion.getOtrodiagnostico()%></textarea>


                                        </div>  
                                    </td>

                                </tr>  
                            </table>
                        </div>
                        <div id="rm" name="rm" class="tabcontent">
                            <table style="border-top:  #619fd8 2px solid;">

                                <tr><td colspan="3" ><div class=" letra " style=" font-size: 20px">Receta de Lente</div>


                                        <div  class="col-md-6 col-md-offset-1 text-center">

                                            <select id="lentesolicito" name="lentesolicito" onchange="
                                                    var pre = document.getElementById('lentesolicito').value;
                                                    if (pre == 1) {
                                                        document.getElementById('trata').style.display = 'block';

                                                    } else {
                                                        document.getElementById('trata').style.display = 'none';
                                                        document.getElementById('receta').style.display = 'none';

                                                    }

                                                    " > 


                                                <option selected  value="-1"> Seleccionar </option>

                                                <% for (int i = 0; i < idpertinencia.length; ++i) {

                                                        if (idpertinencia[i] == r) {%>
                                                <option selected value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                    <%} else {%>

                                                <option value="<%=idpertinencia[i]%>"><%=nombrepertinencia[i]%>
                                                    <% }
                                                        }
                                                    %>

                                            </select>  
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <div class="col-md-8" id="trata" name="trata" style="  display: none ">

                                            <input type="checkbox" name="tratamientolentesCerca" id="tratamientolentesCerca" value="checkbox" onchange="javascritp:

                                                            var pre = document.getElementById('tratamientolentesCerca').checked;
                                                    var pre1 = document.getElementById('tratamientolentesLejos').checked;
                                                    if (pre || pre1) {
                                                        document.getElementById('receta').style.display = 'block';
                                                    } else {
                                                        document.getElementById('receta').style.display = 'none';
                                                    }">&nbsp;&nbsp;
                                            <i class="fa fa-check bigicon" style="font-size:16px">  Tratamiento con Lentes �pticos Cerca </i>
                                            <br>
                                            <input type="checkbox" name="tratamientolentesLejos" id="tratamientolentesLejos" value="checkbox" onchange="javascritp:

                                                            var pre = document.getElementById('tratamientolentesLejos').checked;
                                                    var pre1 = document.getElementById('tratamientolentesCerca').checked;
                                                    if (pre || pre1) {
                                                        document.getElementById('receta').style.display = 'block';
                                                    } else {
                                                        document.getElementById('receta').style.display = 'none';
                                                    }">&nbsp;&nbsp;<i class="fa fa-check bigicon" style="font-size:16px">  Tratamiento con Lentes �pticos Lejos</i>
                                        </div>  
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">

                                        <div id="receta" style="border-top:  #619fd8 2px solid;  display: none ">
                                            <div class=" letra"> Receta de Lente:</div>
                                            <div>
                                                &nbsp;

                                            </div>
                                            <div>
                                                <input  style="height:40px; width: 200px " id="odf" value="<%=re.getOjoderechoesfera()%>" name="odf" placeholder=" Ojo Derecho Esfera"> 

                                                <input  style="height:40px ; width: 200px " id="odc" name="odc" value="<%=re.getOjoderechocilindro()%>"  placeholder=" Ojo Derecho Cilindro">

                                                <input  style=" height:40px ; width: 200px" id="ode" name="ode" value="<%=re.getOjoderechoeje()%>" placeholder=" Ojo Derecho Eje">
                                            </div>
                                            <div>
                                                &nbsp;

                                            </div>
                                            <div>

                                                <input  style=" height: 40px; width: 200px " id="oie" value="<%=re.getOjoizquierdoesfera()%>" name="oie" placeholder=" Ojo Izquierdo Esfera">

                                                <input  id="oic" style=" height: 40px ; width: 200px" value="<%=re.getOjoiaquierdacilindro()%>" name="oic"  placeholder=" Ojo Izquierdo Cilindro">

                                                <input   id="oiej" style=" height: 40px ; width: 200px" value="<%=re.getOjoizquierdaeje()%>" name="oiej" placeholder=" Ojo Izquierdo Eje">


                                            </div>
                                            <div>
                                                &nbsp;

                                            </div>
                                            <div>
                                                <input style=" height: 40px; width: 200px "   value="<%=re.getAadd()%>" id="add" name="add" placeholder="ADD">
                                            </div>
                                            <div>
                                                &nbsp;

                                            </div>
                                            <div>
                                                <div>
                                                    <input  style=" height: 40px; width: 200px" value="<%=re.getDistanciapupilarlejos()%>"  id="dpl" name="dpl" placeholder="Distancia Pupilar Lejos"> 

                                                    <input  style=" height: 40px; width: 200px" value="<%=re.getDistanciapupilarcerca()%>" id="dpc" name="dpc" placeholder="Distancia Pupilar Cerca">
                                                </div>
                                            </div>
                                            <div>
                                                &nbsp;

                                            </div>
                                            <div>
                                                <div class=" letra"> Observaciones:</div>
                                                <span class="col-md-1 col-md-offset-1 text-center">
                                                    <i class="fa fa-edit bigicon"></i></span>


                                                <div class="col-md-8">
                                                    <input  style=" height: 80px" value="" type="text"  rows="3" id="observacionesreceta" name="observacionesreceta"  class="form-control" placeholder="Observaciones" value="<%=re.getObservaciones()%>">


                                                </div>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="ppt" name="ppt" class="tabcontent">  
                            <table style="border-top:  #619fd8 2px solid; ">

                                <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Procedimiento Realizados</div></td></tr>
                                <tr>
                                    <td > &nbsp;</td>
                                    <td>
                                        <%for (lugar lu : ce.buscarlosProcedimientosporEspecialidad(idespecialidad)) {%>

                                        <input type="checkbox" id="procedimiento<%=lu.getId_atencion()%>" name="procedimiento<%=lu.getId_atencion()%>" value="checkbox"><%=lu.getNombre()%> &nbsp;
                                        <br>

                                        <% }%>  
                                    </td>

                                    <td style="width: 10px">
                                        <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Procedimiento</i></span> 
                                    </td>
                                    <td>


                                        <div class="col-md-12">
                                            <textarea type="text"  rows="3" id="otroprocedimiento" name="otroprocedimiento"  class="form-control" placeholder="Otro Procedimiento"  ><%=atencion.getOtroprocedimiento()%></textarea>


                                        </div>  
                                    </td>

                                </tr>  
                            </table>
                        </div>
                        <div id="ept" name="ept" class="tabcontent">   
                            <table style="border-top:  #619fd8 2px solid; ">

                                <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Examenes Realizados</div></td></tr>
                                <tr>
                                    <td > &nbsp;</td>
                                    <td>
                                        <%for (lugar lu : ce.buscarlosExamenesporEspecialidad(idespecialidad)) {%>

                                        <input type="checkbox" id="examenes<%=lu.getId_atencion()%>" name="examenes<%=lu.getId_atencion()%>" value="checkbox"><%=lu.getNombre()%> &nbsp;

                                        <br>

                                        <% }%>  
                                    </td>

                                    <td style="width: 10px">
                                        <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Examenes</i></span> 
                                    </td>
                                    <td>


                                        <div class="col-md-12">
                                            <textarea type="text"  rows="3" id="otroexamene" name="otroexamene"  class="form-control" placeholder="Otro Examenes"  ><%=atencion.getOtroexamenes()%></textarea>


                                        </div>  
                                    </td>

                                </tr>  
                            </table>
                        </div>
                        <div id="dept" name="dept" class="tabcontent">
                            <table style="border-top:  #619fd8 2px solid; " >

                                <input id="cita" name="cita" value="<%=cita%>" hidden>
                                <tr>
                                    <td style="width: 300px"><span class="col-md-3   col-md-offset-1 "><i class="fa fa-chevron-down bigicon"  style=" font-size: 16px" >Genera SIC:</i></span>

                                        <div class="col-md-8">
                                            <select id="sic" name="sic" onchange="javascript:

                                                            var pre = document.getElementById('sic').value;
                                                    if (pre == 1) {


                                                        document.getElementById('detalle').style.display = 'block';
                                                    } else
                                                        document.getElementById('detalle').style.display = 'none';" > 
                                                <option selected value="0" > SIC </option>
                                                <%if (atencion.getSic() == 1) {%>
                                                <option selected value="1"> Si </option>
                                                <option value="2"> No </option>
                                                <%} else if (atencion.getSic() == 2) {%>
                                                <option selected value="2"> No </option>
                                                <option value="1"> Si </option>
                                                <%} else {%>
                                                <option  value="1"> Si </option>
                                                <option value="2"> No </option>
                                                <%}%>
                                            </select>  
                                        </div>

                                    </td>
                                    <td >
                                        <div class="col-md-12" id="detalle" style=" display: none ">
                                            <table>
                                                <tr>
                                                    <td style="width: 3px">
                                                        <span class="col-md-1 col-md-offset-1"><i class="fa fa-chevron-down bigicon" style=" font-size: 16px">Diagnostico por el que Derivo</i></span> 

                                                    </td>
                                                    <td>


                                                        <select name="diagnosticosic" id="diagnosticosic" class="form-control" onchange="javascript:

                                                                        var diagnostico = document.getElementById('diagnosticosic').value;
                                                                if (diagnostico == -1) {


                                                                    document.getElementById('otrodiagnosticosicmuestro').style.display = 'block';
                                                                } else
                                                                    document.getElementById('otrodiagnosticosicmuestro').style.display = 'none';" >
                                                            <option  value="0" selected>Diagnostico
                                                            <option  value="-1" >Otro
                                                                <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {
                                                                        if (lu.getId_atencion() == atencion.getDiagnosticosic()) {
                                                                %>
                                                            <option selected value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                                <% } else {
                                                                %>
                                                            <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                                <%
                                                                        }

                                                                    }%>  
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="col-md-12" id="otrodiagnosticosicmuestro" style=" display: none">
                                                            <textarea type="text"  rows="1" id="otrodiagnosticosic" name="otrodiagnosticosic"  class="form-control" placeholder="Otro Diagnostico"  ><%= atencion.getOtrodiagnosticosic()%></textarea>


                                                        </div>  

                                                    </td>
                                                    <td style="width: 3px">
                                                        <span class="col-md-1 col-md-offset-1"><i class="fa bigicon" style=" font-size: 16px">Detalle de Derivacion</i></span> 

                                                    </td>
                                                    <td>
                                                        <textarea type="text"  rows="3" id="detalledederivacion" name="detalledederivacion"  class="form-control" placeholder="Detalle de Derivaci�n"><%=atencion.getDetallederivacion()%></textarea>

                                                    </td>


                                                </tr>
                                            </table>
                                        </div>
                                    </td>


                                </tr>
                            </table>
                            <table>

                                <tr>
                                    <td style="width: 300px"  ><span class="col-md-3 col-md-offset-1 "><i class="fa fa-chevron-down bigicon" style=" font-size: 16px" >Destino Paciente:</i></span>

                                        <div class="col-md-8">
                                            <select id="destinopaciente" name="destinopaciente" > 
                                                <option selected value="0"> Destino </option>
                                                <% for (lugar l : ce.BuscarlosDestinoPacientes()) {
                                                        if (l.getId_lugar() == atencion.getIddestinopaciente()) {%>
                                                <option selected value="<%=l.getId_lugar()%>"> <%=l.getNombre()%> </option>
                                                <%} else {%>
                                                <option value="<%=l.getId_lugar()%>"> <%=l.getNombre()%> </option>
                                                <%

                                                        }
                                                    }%>
                                            </select>  
                                        </div>
                                    </td>
                                    <td style="width: 4px">
                                        <span class="col-md-2 col-md-offset-2"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otro Destino</i></span> 
                                    </td>
                                    <td>


                                        <div class="col-md-12">
                                            <textarea type="text"  rows="3" id="otrodestino" name="otrodestino"  class="form-control" placeholder="Otro Destino"  ><%=atencion.getOtrodestino()%></textarea>


                                        </div>  
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="ipt" name="ipt" class="tabcontent">
                            <table style="border-top:  #619fd8 2px solid; ">

                                <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Indicaciones</div></td></tr>


                            </table>
                            <table>
                                <tr>


                                    <td  colspan="4">
                                        <%        for (lugar lu : ce.buscarlasindicacionesporEspecialidad(idespecialidad)) {


                                        %>

                                        <div class="col-md-8">

                                            <input type="checkbox" id="indicaciones<%=lu.getId_atencion()%>" name="indicaciones<%=lu.getId_atencion()%>" value="checkbox" onchange="
                                                    var pre = document.getElementById('indicaciones<%=lu.getId_atencion()%>').checked;
                                                    if (pre) {


                                                        document.getElementById('recetadetalle<%=lu.getId_atencion()%>').style.display = 'block';
                                                    } else
                                                        document.getElementById('recetadetalle<%=lu.getId_atencion()%>').style.display = 'none';"><%=lu.getNombre()%> &nbsp;
                                        </div>

                                        <br>
                                        <div class="col-md-12" id="recetadetalle<%=lu.getId_atencion()%>" name="recetadetalle<%=lu.getId_atencion()%>" style=" display: none">
                                            <textarea type="text"  rows="1" id="descripcionreceta<%=lu.getId_atencion()%>" name="descripcionreceta<%=lu.getId_atencion()%>"  class="form-control" placeholder="Dosis Diaria,V�a de admision,Periodo."  ></textarea>


                                        </div> 

                                        <%

                                            }%>  
                                    </td>

                                    <td  style="width: 10px">
                                        <span class="col-md-1 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Otras Indicaciones</i></span> 
                                    </td>
                                    <td>


                                        <div class="col-md-12">
                                            <textarea type="text"  rows="3" id="otroindicaciones" name="otroindicaciones"  class="form-control" placeholder="Otra Indicacion"  ><%=atencion.getOtrasindicaciones()%></textarea>


                                        </div>  
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div id="srpt" name="srpt" class="tabcontent">

                            <table style="border-top:  #619fd8 2px solid; ">
                                <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Solicitud Quirurgico</div></td></tr>
                                <tr>
                                    <td colspan="4">
                                        <button type="button" onclick="addSolicitud('sq')">  Agregar solicitud</button>

                                    </td>


                                </tr>  
                            </table>
                            <table>
                                <tr>
                                    <td>

                                        <table class="table table-striped" id="sq" name="sq"   >
                                            <tbody>
                                            <input type="text" id="numerodesolicitudes" name="numerodesolicitudes"   value="<%=cantidadsolicitud.size()%>" hidden >
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Diagnostico
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                    <th>
                                                        Lateralidad
                                                    </th>

                                                    <th>
                                                        Intervenci�n
                                                    </th>
                                                    <th>
                                                        Anestesia Solicitada
                                                    </th>
                                                    <th>
                                                        Requerimientos Especiales
                                                    </th>
                                                    <th>
                                                        Observaci�n
                                                    </th>
                                                </tr>
                                            </thead>
                                            <% int i = 1;
                                                for (lugar l : caco.buscarSolicitudes(atencion.getIdatencion())) {

                                            %>
                                            <tr>

                                                <td  >


                                                    <select style=" width: 130% " name="diagnosticos<%=i%>" id="diagnosticos<%=i%>"  onchange="javascript:

                                                                    var diagnostico = document.getElementById('diagnosticos<%=i%>').value;
                                                            if (diagnostico == -1) {


                                                                document.getElementById('otrodiagnosticosicmuestro<%=i%>').style.display = 'block';
                                                            } else
                                                                document.getElementById('otrodiagnosticosicmuestro<%=i%>').style.display = 'none';" >
                                                        <option  value="0" selected>Diagnostico

                                                            <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {

                                                                    if (l.getId_diagnostico_urgencia() == lu.getId_atencion()) {
                                                            %>
                                                        <option selected value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>
                                                            <%} else {%>
                                                        <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                            <% }
                                                                }%>  
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="col-md-12" id="otrodiagnosticosicmuestro<%=i%>" style=" display: none">
                                                        <textarea type="text" style=" width: 150px"  rows="1" id="otrodiagnostico<%=i%>" name="otrodiagnostico<%=i%>"  class="form-control" placeholder="Otro Diagnostico"  ><%%></textarea>


                                                    </div>  

                                                </td>
                                                <td>
                                                    <select id="lateralidades<%=i%>" name="lateralidades<%=i%>" onchange="javascript:

                                                                    var diagnostico = document.getElementById('diagnosticos<%=i%>').value;
                                                            var lateralidad = document.getElementById('lateralidades<%=i%>').value;

                                                            var la = '';
                                                            if (lateralidad == 1) {
                                                                la = 'OJO DERECHO';
                                                            } else if (lateralidad == 2) {
                                                                la = 'OJO IZQUIERDO';
                                                            }

                                                            if ((diagnostico == 2) || (diagnostico == 31)) {
                                                                document.getElementById('intervencion<%=i%>').value = 'FACOERESIS CON IMPLANTE DE LENTE INTRAOCULAR ' + la;
                                                            } else {
                                                                document.getElementById('intervencion<%=i%>').value = '';
                                                            }
                                                            ">

                                                        <%for (int k = 0; k < lateralidad.length; ++k) {

                                                                if (k == l.getId_centro()) {
                                                        %>
                                                        <option selected value="<%=k%>"><%=lateralidad[k]%></option>
                                                        <%} else {%>
                                                        <option value ="<%=k%>"><%=lateralidad[k]%></option>
                                                        <% }
                                                            }%>


                                                    </select>
                                                </td>

                                                <td>
                                                    <textarea rows="2" cols="30" name="intervencion<%=i%>" id="intervencion<%=i%>" ><%=l.getIntervencio()%></textarea>
                                                </td>
                                                <td>



                                                    <select name="anestesia<%=i%>"  id="anestesia<%=i%>">
                                                        <option value="0">Tipo de Anestesia
                                                            <%for (tipo_anestesia temp : ce.tiposdeanestesia()) {

                                                                    if (l.getId_lugar() == temp.getIdtipo_atencion()) {
                                                            %>
                                                        <option selected value="<%=temp.getIdtipo_atencion()%>"><%=temp.getNombretipo()%></option> 
                                                        <%} else {%>
                                                        <option value="<%=temp.getIdtipo_atencion()%>"><%=temp.getNombretipo()%></option> 
                                                        <%}
                                                            }%>
                                                    </select>



                                                </td>
                                                <td>
                                                    <textarea rows="2"  name="requerimientos<%=i%>" id="requerimientos<%=i%>" ><%=l.getVariable1()%> </textarea>
                                                </td>
                                                <td>
                                                    <textarea rows="2"  name="observacion<%=i%>" id="observacion<%=i%>" ><%=l.getVariable2()%> </textarea>
                                                </td>
                                                <td>
                                                    <button onclick="borrarFila(this)">x</button>
                                                </td>

                                            </tr>
                                            <%++i;
                                                }%> 
                                            </tbody>
                                        </table>  
                                    </td>

                                </tr>  
                            </table>
                        </div>

                        <div id="nrpt" name="nrpt" class="tabcontent">

                            <table style="border-top:  #619fd8 2px solid; ">
                                <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">No Indicaci�n de Cirug�a</div></td></tr>

                                <td colspan="4">
                                    <button type="button" onclick="addnoInc('ni')">  Agregar solicitud</button>

                                </td>

                                </tr>

                            </table>
                            <table>

                                <tr>
                                    <td>

                                        <table class="table table-striped" id="ni" name="ni" >
                                            <tbody>
                                            <input type="text" id="numerodesolicitudesnrpt" name="numerodesolicitudesnrpt" value="<%=cantidadnosolicitud.size()%>" hidden>
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Diagnostico
                                                    </th>
                                                    <th>

                                                    </th>
                                                    <th>
                                                        Lateralidad
                                                    </th>

                                                    <th>
                                                        Intervenci�n Quir�rgica
                                                    </th>
                                                    <th>
                                                        Razones por las que no requiere Cirug�a
                                                    </th>
                                                    <th>
                                                        Indicaciones al Paciente
                                                    </th>

                                                </tr>

                                            </thead>
                                            <%  int k = 1;
                                                for (lugar l : caco.buscarNoindicaciones(atencion.getIdatencion())) {

                                            %>
                                            <tr>

                                                <td>


                                                    <select name="diagnosticonrpt<%=k%>" id="diagnosticonrpt<%=k%>" class="form-control" onchange="javascript:

                                                                    var diagnostico = document.getElementById('diagnosticonrpt<%=k%>').value;
                                                            if (diagnostico == -1) {


                                                                document.getElementById('otrodiagnosticosicmuestro<%=k%>').style.display = 'block';
                                                            } else
                                                                document.getElementById('otrodiagnosticosicmuestro<%=k%>').style.display = 'none';" >
                                                        <option  value="0" selected>Diagnostico
                                                        <option  value="-1" >Otro
                                                            <%for (lugar lu : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {
                                                                    if (l.getId_diagnostico_urgencia() == lu.getId_atencion()) {%>

                                                        <option selected value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>


                                                            <% } else {%>
                                                        <option  value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%>
                                                            <%  }
                                                                }%>  
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="col-md-12" id="otrodiagnosticosicmuestro<%=k%>" style=" display: none">
                                                        <textarea type="text" style=" width: 150px"  rows="1" id="otrodiagnosticonrpt<%=i%>" name="otrodiagnosticonrpt<%=i%>"  class="form-control" placeholder="Otro Diagnostico"  ><%=l.getVariable3()%></textarea>


                                                    </div>  

                                                </td>
                                                <td>
                                                    <select id="lateralidadesnrpt<%=k%>" name="lateralidadesnrpt<%=k%>" onchange="javascript:

                                                                    var diagnostico = document.getElementById('diagnosticonrpt<%=k%>').value;
                                                            var lateralidad = document.getElementById('lateralidadesnrpt<%=k%>').value;
                                                            var la = '';
                                                            if (lateralidad == 1) {
                                                                la = 'OJO DERECHO';
                                                            } else if (lateralidad == 2) {
                                                                la = 'OJO IZQUIERDO';
                                                            }

                                                            if ((diagnostico == 2) || (diagnostico == 31)) {
                                                                document.getElementById('intervencionnrpt<%=k%>').value = 'FACOERESIS CON IMPLANTE INTRAOCULAR ' + la;
                                                            } else {
                                                                document.getElementById('intervencionnrpt<%=k%>').value = '';
                                                            }
                                                            ">
                                                        <%for (int j = 0; j < lateralidad.length; ++j) {
                                                                if (l.getId_centro() == j) {
                                                        %>
                                                        <option selected value="<%=j%>"><%=lateralidad[j]%> </option>
                                                        <%} else {%>
                                                        <option  value="<%=j%>"><%=lateralidad[j]%> </option>
                                                        <% }
                                                            }%>

                                                    </select>
                                                </td>

                                                <td>
                                                    <textarea rows="2" cols="30" name="intervencionnrpt<%=k%>" id="intervencionnrpt<%=k%>" ><%=l.getIntervencio()%></textarea>
                                                </td>

                                                <td>
                                                    <textarea rows="2" cols="30" name="razonesnrpt<%=k%>" id="razonesnrpt<%=k%>" ><%=l.getVariable4()%> </textarea>
                                                </td>
                                                <td>
                                                    <textarea rows="2" cols="30" name="indicacionesnrpt<%=k%>" id="indicacionesnrpt<%=k%>" ><%=l.getVariable2()%></textarea>
                                                </td>
                                                <td>
                                                    <button onclick="borrarFilasolicitud(this)">x</button>
                                                </td>


                                            </tr>
                                            <% ++k;
                                                }%> 
                                            </tbody>
                                        </table>

                                    </td>

                                </tr>  

                            </table>

                        </div>
                        <!-- excepciion de garantia y solicitud de examenes -->

                        <div id="eg" name="eg" class="tabcontent" >
                            <table >
                                <tr><td> &nbsp;</td></tr>
                                <td >
                                    <span class="col-md-6 col-md-offset-1"><i class="fa fa-edit bigicon" style=" font-size: 16px">Problema de Salud AUGE:</i></span> 
                                </td>
                                <td>
                                    <input  style="width:300px;height:100px"  id="problemaauge"  placeholder="Problema de Salud AUGE" value="<%=problemaaugereceta%>"   name="problemaauge" type="text" >
                                </td>  
                                <tr><td> &nbsp;</td></tr>
                                <tr><td><div class=" letra"><strong> Causal de excepcion</strong></div></td></tr>
                                <tr><td> &nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" id="rpd" name="rpd" value="checkbox"> &nbsp;&nbsp;
                                        <i class="fa fa-check bigicon" style="font-size:16px"> Rechazo del prestador Designado  </i>
                                    </td>  
                                    <td>
                                        <input type="checkbox" id="rapg" name="rapg" value="checkbox"> &nbsp;&nbsp;
                                        <i class="fa fa-check bigicon" style="font-size:16px"> Rechazo de la atenci�n o procedimiento garantizado  </i>
                                    </td>  
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" id="pr" name="pr" value="checkbox"> &nbsp;&nbsp;
                                        <i class="fa fa-check bigicon" style="font-size:16px">Prestaci�n Rechazada  </i>
                                    </td>  
                                    <td>
                                        <input type="checkbox" id="ocdp" name="ocdp" value="checkbox"> &nbsp;&nbsp;
                                        <i class="fa fa-check bigicon" style="font-size:16px"> Otra Causa definida por el paciente </i>
                                    </td>  
                                </tr>

                                <tr><td><div class=" letra"> Observacion:</div></td></tr>
                                <tr><td> &nbsp;</td></tr>
                                <tr>
                                    <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                        <div class="col-md-8">
                                            <textarea type="text"  rows="3" id="observaciongarantia" name="observaciongarantia"  class="form-control" placeholder="OBSERVACIONES(complementar selecci�n anterior)"  ><%=observacionreceta%></textarea>


                                        </div>  
                                    </td>

                                </tr>

                                <tr><td> &nbsp;</td></tr>
                            </table>

                        </div>

                        <div id="sdc" name="sdc" class="tabcontent">
                            <table style="border-top:  #619fd8 2px solid; ">
                                <tr><td colspan="3" style="width: 30px">
                                        <div class=" letra " style=" font-size: 20px">Solicitud de Control</div>


                                        <div  class="col-md-6 col-md-offset-1 text-center">

                                            <select id="controlsolicito" name="controlsolicito" onchange="
                                                    var pre = document.getElementById('controlsolicito').value;
                                                    if (pre == 1) {
                                                        document.getElementById('fechacontrol').disabled = false;
                                                        document.getElementById('motivocontrol').disabled = false;
                                                    } else {
                                                        document.getElementById('fechacontrol').value = '';
                                                        document.getElementById('motivocontrol').value = '';
                                                        document.getElementById('fechacontrol').disabled = true;
                                                        document.getElementById('motivocontrol').disabled = true;
                                                    }

                                                    " > 


                                                <option selected  value="-1"> Seleccionar </option>

                                                <% for (int l = 0; l < idpertinencia.length; ++l) {

                                                        if (idpertinencia[l] == control.getEstatu()) {%>
                                                <option selected value="<%=idpertinencia[l]%>"><%=nombrepertinencia[l]%>
                                                    <%} else {%>

                                                <option value="<%=idpertinencia[l]%>"><%=nombrepertinencia[l]%>
                                                    <% }
                                                        }
                                                    %>

                                            </select>  
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table id="control" name="control" style=" width: 100%; "  >

                                <tr>
                                    <td colspan="6">
                                        <span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-check bigicon" style="font-size:18px">Fecha Control </i></span>
                                        <div class="col-md-8">
                                            <input type="text" maxlength="13" size="12" id="fechacontrol" name="fechacontrol" disabled  value='<%=cfecha%>'> <br> </td>
                                        </div>  
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                        <div class="col-md-8">
                                            <textarea type="text"  rows="3" id="motivocontrol" name="motivocontrol"   class="form-control" placeholder="Motivo Control" disabled ><%=cmotivo%></textarea>


                                        </div>  
                                    </td>
                                </tr>
                            </table>


                        </div>


                        <div  id="seo" name="seo" class="tabcontent">


                            <table style="border-top:  #619fd8 2px solid; ">
                                <tr><td colspan="3" style="width: 30px"><div class=" letra " style=" font-size: 20px">Solicitud de Examen o Procedimiento</div></td></tr>

                            </table>
                            <table style=" width: 100%">
                                <tr>
                                    <td>
                                        <i class="fa fa-check bigicon" style="font-size:16px">Examenes </i>


                                        <select class="selectpicker col-md-6" id="examenes" name="examenes"  title="Examenes a Solicitar ">
                                            <%for (lugar lu : ce.buscarlosExamenesporEspecialidad(idespecialidad)) {%>
                                            <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%></option>

                                            <%}%>
                                        </select>

                                    </td>
                                    <td>
                                        <i class="fa fa-check bigicon" style="font-size:16px">Lateralidad</i> 
                                        <select class="selectpicker col-md-6" id="lateralidadsep" name="lareralidadsep"  title="Lateralidad ">
                                            <%for (int n = 1; n < lateralidad.length; ++n) {%>
                                            <option value="<%=n%>"><%=lateralidad[n]%></option>

                                            <%}%>
                                        </select>
                                    </td>
                                    <td>
                                        <i class="fa fa-check bigicon" style="font-size:16px">Realizar en </i> 
                                        <select class="selectpicker col-md-6" id="tiemposep" name="tiemposep"  title="Realizar en">
                                            <%for (lugar l : caco.buscarTiempo()) {%>
                                            <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%></option>

                                            <%}%>
                                        </select>     
                                    </td>
                                    <td>
                                        <input class="btn-primary" type="button" value="Agregar Examenes" name="btn_rut" onclick="javascript: addRow('myTable');" />
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table id="myTable" name="myTable" style=" width: 100% " class=" table table-striped" cellspacing="0" border="1">

                                            <tbody>
                                                <%for (solicitudExamenes_Procedimientos se : caco.solicitudesdetalle(sep.getIdsolicitud(), 1)) {%>
                                                <tr style="text-align: center" >
                                                    <td>
                                                        <%=  se.getVariable1() + "  " + se.getVariable2() + " " + se.getVariable3()%>
                                                    </td>
                                                    <td>
                                                        <button onclick="borrarFilaotro(this)">x</button>
                                                    </td>
                                                    <td>
                                                        <input id="solicitudesexamenes<%=se.getIdexamen()%>" name="solicitudesexamenes<%=se.getIdexamen()%>" hidden value="<%=se.getIdexamen()%>" >

                                                        <input id="lateralidadexamenes<%=se.getIdexamen()%>" name="lateralidadexamenes<%=se.getIdexamen()%>" hidden value="<%= se.getIdlateralidad()%>" >

                                                        <input id="tiempoexamenes<%=se.getIdexamen()%>" name="tiempoexamenes<%=se.getIdexamen()%>" hidden value="<%=se.getIdtiempo()%>" >
                                                    </td>

                                                </tr>


                                                <%}%>


                                            </tbody>

                                        </table>
                                    </td>

                                </tr>
                                <tr>

                                    <td>
                                        <i class="fa fa-check bigicon" style="font-size:16px">Procedimientos</i>

                                        <select class="selectpicker col-md-6" id="procedimientos" name="procedimientos" title="Procedimientos a Solicitar ">
                                            <%for (lugar lu : ce.buscarlosProcedimientosporEspecialidad(idespecialidad)) {%>
                                            <option value="<%=lu.getId_atencion()%>"><%=lu.getNombre()%></option>

                                            <%}%>
                                        </select>

                                    </td>
                                    <td>
                                        <i class="fa fa-check bigicon" style="font-size:16px">Lateralidad</i> 
                                        <select class="selectpicker col-md-6" id="lateralidadsepro" name="lareralidadsepro"  title="Lateralidad ">
                                            <%for (int p = 1; p < lateralidad.length; ++p) {%>
                                            <option value="<%=p%>"><%=lateralidad[p]%></option>

                                            <%}%>
                                        </select>
                                    </td>
                                    <td>
                                        <i class="fa fa-check bigicon" style="font-size:16px">Realizar en </i> 
                                        <select class="selectpicker col-md-6" id="tiemposepro" name="tiemposepro"  title="Realizar en">
                                            <%for (lugar l : caco.buscarTiempo()) {%>
                                            <option value="<%=l.getId_lugar()%>"><%=l.getNombre()%></option>

                                            <%}%>
                                        </select>     
                                    </td>
                                    <td>
                                        <input class="btn-primary" type="button" value="Agregar Procedimiento" name="btn_rut" onclick="javascript: addRow1('myTable1');" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table id="myTable1" name="myTable1" style=" width: 100% " class=" table table-striped" cellspacing="0" border="1">

                                            <tbody>
                                                <%for (solicitudExamenes_Procedimientos se : caco.solicitudesdetalle(sep.getIdsolicitud(), 2)) {%>
                                                <tr style="text-align: center">
                                                    <td>
                                                        <%=  se.getVariable1() + "  " + se.getVariable2() + " " + se.getVariable3()%>
                                                    </td>
                                                    <td>
                                                        <button onclick="borrarFilaotro(this)">x</button>
                                                    </td>
                                                    <td>
                                                        <input id="solicitudesprocedimiento<%=se.getIdexamen()%>" name="solicitudesprocedimiento<%=se.getIdexamen()%>" hidden value="<%=se.getIdexamen()%>" >

                                                        <input id="lateralidadprocedimiento<%=se.getIdexamen()%>" name="lateralidadprocedimiento<%=se.getIdexamen()%>" hidden value="<%= se.getIdlateralidad()%>" >

                                                        <input id="tiempoprocedimiento<%=se.getIdexamen()%>" name="tiempoprocedimiento<%=se.getIdexamen()%>" hidden value="<%=se.getIdtiempo()%>" >
                                                    </td>

                                                </tr>


                                                <%}%>





                                            </tbody>

                                        </table>
                                    </td>

                                </tr>
                                <tr><td><div class=" letra"> Observacion:</div></td></tr>
                                <tr><td> &nbsp;</td></tr>
                                <tr>
                                    <td colspan="6"><span class="col-md-1 col-md-offset-1 text-center"><i class="fa fa-edit bigicon"></i></span>


                                        <div class="col-md-8">
                                            <textarea type="text"  rows="3" id="observacionsolicitud" name="observacionsolicitud"  class="form-control" placeholder="OBSERVACIONES"  > <%=obseresolicitud%></textarea>


                                        </div>  
                                    </td>

                                </tr>
                            </table>

                        </div>

                    </div>
                    <table>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-lg">Guardar </button>

                                        <button type="reset" class="btn btn-primary btn-lg">Cancelar</button>
                                    </div>
                                </div> 
                            </td>
                        </tr>


                    </table>



                </div>
            </form>
        </div>
    </article>
        </section>
        <%if (control.getEstatu() == 1) {%>
        <script>
            document.getElementById('fechacontrol').disabled = false;
            document.getElementById('motivocontrol').disabled = false;
        </script>

        <%}
            if (atencion.getSic() == 1) {

        %>                                        
        <script>
            document.getElementById('detalle').style.display = 'block';
        </script>

        <%}%>

        <% if (eg.getRechazo_atencion_garantizado() == 1) {
        %>
        <script>
            document.getElementById('rapg').checked = 1;
        </script>
        <%}%>  

        <% if (eg.getOtra_causa() == 1) {
        %>
        <script>
            document.getElementById('ocdp').checked = 1;
        </script>
        <%}%> 

        <% if (eg.getRechazo_prestador_designado() == 1) {
        %>
        <script>
            document.getElementById('rpd').checked = 1;
        </script>
        <%}%> 
        <% if (eg.getPrestacion_rechazada() == 1) {
        %>
        <script>
            document.getElementById('pr').checked = 1;
        </script>
        <%}%> 


        <%
            for (atencion da : caco.buscarAtencionOftalmologia(atencion.getIdatencion())) {

        %>
        <script type = "text/javascript" >
    document.getElementById('diagnostico<%=da.getDiagnostico()%>').checked = 1;
    document.getElementById('lateralidad<%=da.getDiagnostico()%>').disabled = false;
    document.getElementById('confirmodecartod<%=da.getDiagnostico()%>').disabled = false;
    document.getElementById('ges<%=da.getDiagnostico()%>').disabled = false;
    document.getElementById('lateralidad<%=da.getDiagnostico()%>').value = <%=da.getLateralidad()%>
    document.getElementById('confirmodecartod<%=da.getDiagnostico()%>').value = <%=da.getConfirmadescartadiagnostico()%>
    document.getElementById('ges<%=da.getDiagnostico()%>').value = <%=da.getGes()%>
            <%if (da.getGes() == 1) {%>
    document.getElementById('documentos<%=da.getDiagnostico()%>').style.display = 'block';
    document.getElementById('confirmadescarta<%=da.getDiagnostico()%>').value = <%=da.getConfirmadescartadocumentos()%>;


            <%if (da.getConfirmadescartadocumentos() == 1) {
            %>
    document.getElementById('datosipd<%=da.getDiagnostico()%>').style.display = 'block';

    document.getElementById('fundamentodeldiagnostico<%=da.getDiagnostico()%>').value = '<%=da.getFundamentodeldiagnostico()%>';
    document.getElementById('tratamientoeindicaciones<%=da.getDiagnostico()%>').value = '<%=da.getTratamientoeindicaciones()%>';
            <%}
        }%>


        </script>

        <%  }
            if (atencion.getTratamientocerca() == 1 || atencion.getTratamientolejos() == 1) {
        %>
        <script type="text/javascript">
            document.getElementById('trata').style.display = 'block';
        </script>

        <%
            if (atencion.getTratamientocerca() == 1) {

        %>
        <script type="text/javascript">
            document.getElementById('tratamientolentesCerca').checked = 1;
        </script>

        <%}
            if (atencion.getTratamientocerca() == 1) {
        %>
        <script type="text/javascript">
            document.getElementById('tratamientolentesLejos').checked = 1;
        </script>

        <%
            }
        %>
        <script type="text/javascript">
            document.getElementById('receta').style.display = 'block';
            document.getElementById('odf').value = '<%=re.getOjoderechoesfera()%>';
            document.getElementById('odc').value = '<%=re.getOjoderechocilindro()%>';
            document.getElementById('ode').value = '<%=re.getOjoderechoeje()%>';
            document.getElementById('oie').value = '<%=re.getOjoizquierdoesfera()%>';
            document.getElementById('oic').value = '<%=re.getOjoiaquierdacilindro()%>';
            document.getElementById('oiej').value = '<%=re.getOjoizquierdaeje()%>';
            document.getElementById('add').value = '<%=re.getOjoderechoesfera()%>';
            document.getElementById('dpl').value = '<%=re.getDistanciapupilarlejos()%>';
            document.getElementById('dpc').value = '<%=re.getDistanciapupilarcerca()%>';
            document.getElementById('observacionesreceta').value = '<%=re.getObservaciones()%>';



        </script>

        <%

            }

            for (atencion p : caco.buscarProcedimientosAtencionOftalmologia(atencion.getIdatencion())) {

        %>
        <script type="text/javascript">
    document.getElementById('procedimiento<%=p.getIddiagnostico()%>').checked = 1;
        </script>

        <%}

            for (atencion e : caco.buscarExamenesAtencionOftalmologia(atencion.getIdatencion())) {
        %>
        <script type="text/javascript">
    document.getElementById('examenes<%=e.getIddiagnostico()%>').checked = 1;
        </script>

        <%
            }

            for (atencion h : caco.buscarindicacionesAtencionOftalmologia(atencion.getIdatencion())) {
        %>
        <script type="text/javascript">
    document.getElementById('indicaciones<%=h.getIddiagnostico()%>').checked = 1;
    document.getElementById('recetadetalle<%=h.getIddiagnostico()%>').style.display = 'block';
    document.getElementById('descripcionreceta<%=h.getIddiagnostico()%>').value = '<%=h.getOtrodiagnostico()%>';
        </script>

        <%
            }

        //esto es para cuando venga

        %>

        <script>


            function numeros(e)  // 1

            {
                var key = window.Event ? e.which : e.keyCode
                return (key >= 48 && key <= 57)
            }





            /*validar excepcion de garantia*/
            function validarexcepciongarantia() {

                var problemaauge = document.forms["hojaatencionoftalmologia"]["problemaauge"].value;
                var rpd = document.forms['hojaatencionoftalmologia']['rpd'].checked;
                var rapg = document.forms['hojaatencionoftalmologia']['rapg'].checked;
                var pr = document.forms['hojaatencionoftalmologia']['pr'].checked;
                var ocdp = document.forms['hojaatencionoftalmologiahojaatencionoftalmologia']['ocdp'].checked;
                var observaciongarantia = document.forms["hojaatencionoftalmologia"]["observaciongarantia"].value;
                if (problemaauge != "") {
                    if (!rpd && !rapg && !pr && !ocdp && observaciongarantia == "") {
                        return false;
                    }
                }
                if (rpd && rapg && pr && ocdp) {
                    if (observaciongarantia == "" && problemaauge == "") {
                        return false;
                    }
                }

            }

            function validarcontrol() {

                var fechacontrol = document.forms["hojaatencionoftalmologia"]["fechacontrol"].value;
                var motivocontrol = document.forms["hojaatencionoftalmologia"]["motivocontrol"].value;
                var cotrol = document.forms["hojaatencionoftalmologia"]["controlsolicito"].value;
                if (cotrol == "-1") {
                    Alert.render("Debe Indicar si el paciente requiere Control -> En solicitud de Control!!Complete los datos para Continuar");
                    return false;
                } else if (cotrol == "1") {
                    if (fechacontrol == "" || motivocontrol == "") {
                        Alert.render("Debe Indicar Fecha o Motivo del control!!Complete los datos para Continuar");
                        return false;
                    }
                }

            }




            function validar() {

                var sic = document.forms["hojaatencionoftalmologia"]["sic"].value;
                var detalledederivacion = document.forms["hojaatencionoftalmologia"]["detalledederivacion"].value;
                var destino = document.forms["hojaatencionoftalmologia"]["destinopaciente"].value;
                var otrodestino = document.forms["hojaatencionoftalmologia"]["otrodestino"].value;
                var diagnosticosic = document.forms["hojaatencionoftalmologia"]["diagnosticosic"].value;
                var otrodiagnosticosic = document.forms["hojaatencionoftalmologia"]["otrodiagnosticosic"].value;
                var numerodesolicitudes = document.forms["hojaatencionoftalmologia"]["numerodesolicitudes"].value;
                var numeronoindicaciones = document.forms["hojaatencionoftalmologia"]["numerodesolicitudesnrpt"].value;

                var atencion = document.forms["hojaatencionoftalmologia"]["idatencioncita"].value;
        // debo preguntar si el tipo de atencion es nuevo.
        //  var pertinencia = document.forms["hojaatencionoftalmologia"]["pertinencia"].value;

                var problemaauge = document.forms["hojaatencionoftalmologia"]["problemaauge"].value;
                var rpd = document.forms["hojaatencionoftalmologia"]["rpd"].checked;
                var rapg = document.forms["hojaatencionoftalmologia"]["rapg"].checked;
                var pr = document.forms["hojaatencionoftalmologia"]["pr"].checked;
                var ocdp = document.forms["hojaatencionoftalmologia"]["ocdp"].checked;
                var observaciongarantia = document.forms["hojaatencionoftalmologia"]["observaciongarantia"].value;
                if (problemaauge != "") {
                    if ((!rpd & !rapg & !pr & !ocdp) || observaciongarantia == "") {
                        Alert.render("Debe completar los datos de la Excepcion de Garantia!!Complete los datos para Continuar");
                        return false;
                    }
                }
                if (rpd || rapg || pr || ocdp) {

                    if (observaciongarantia == "" || problemaauge == "") {
                        Alert.render("Debe completar los datos de la Excepcion de Garantia!!Complete los datos para Continuar");
                        return false;
                    }
                }

                if (sic == "1" & (diagnosticosic == "0" || detalledederivacion == "" || (destino == "0" & otrodestino == ""))) {
                    Alert.render("Debe Colocar el Destino , el diagnostico por el que deriva y el Detalle de la Derivacion!!Ya que indic� que SIC es SI");
                    return false;
                }
                if (sic == "1" & diagnosticosic == "-1" & otrodiagnosticosic == "") {
                    Alert.render("Debe indicar Diagnostico por el que deriva!!Complete los datos para Continuar");
                    return false;
                }

        // validar indicacion de lentes


                /*validar pertinencia*/






                if (atencion == 1) {
                    var pertinencia = document.forms["hojaatencionoftalmologia"]["pertinencia"].value;
                    if (pertinencia == -1) {
                        Alert.render("Como su atencion es Nueva debe indicar si �Es pertinente la derivaci�n a su atenci�n? !! Debe Completar los Datos para Continuar");
                        return false;
                    }
                }


        //validar diagnostico
                var falta = 0;
                var mensaje = "";
            <%for (lugar lu
                        : ce.buscarDiagnosticosporEspecialidad(idespecialidad)) {%>
                var atencion = <%=lu.getId_atencion()%>


                var diagnostico = document.forms["hojaatencionoftalmologia"]["diagnostico" + atencion].checked;
                if (diagnostico) {

                    var lateralidad = document.forms["hojaatencionoftalmologia"]["lateralidad" + atencion].value;
                    var ges = document.forms["hojaatencionoftalmologia"]["ges" + atencion].value;
                    var confirmadescarta = document.forms["hojaatencionoftalmologia"]["confirmadescarta" + atencion].value;
                    var tratamientoeindicaciones = document.forms["hojaatencionoftalmologia"]["tratamientoeindicaciones" + atencion].value;
                    var fundamentodeldiagnostico = document.forms["hojaatencionoftalmologia"]["fundamentodeldiagnostico" + atencion].value;
                    var confirmadescartad = document.forms["hojaatencionoftalmologia"]["confirmodecartod" + atencion].value;
                    if (lateralidad == "0" || confirmadescartad == "0" || ges == "0") {
                        mensaje = "Debe Completar los datos del Diagnostico, su lateralidad, si se confirma o Descarta y si es Ges o no Ges!! Complete los datos para continuar ";
                        falta = 1;
                    }
                    if (confirmadescarta == "1" & (tratamientoeindicaciones == "" || fundamentodeldiagnostico == "")) {
                        mensaje = "Debe Completar el Tratamiento y Fundamento para el IPD y Constancia Ges!! Complete los datos para continuar";
                        falta = 1;
                    }
                }

            <%}%>

                if (falta == 1) {

                    Alert.render(mensaje);
                    return false;
                }


                /*validar receta medica*/
                var faltoreceta = false;
            <%  for (lugar lu
                        : ce.buscarlasindicacionesporEspecialidad(idespecialidad)) {
                    if (lu.getId_lugar() != 0) {%>

                var pre = document.getElementById('indicaciones<%=lu.getId_atencion()%>').checked;
                if (pre) {
                    var detalle = document.getElementById('descripcionreceta<%=lu.getId_atencion()%>').value;
                    if (detalle == "") {
                        faltoreceta = true;
                    }
                }



            <% }
                }%>

                if (faltoreceta == true) {
                    Alert.render("Debe colocar el detalle de la indicacion Dosis diaria, Periodo para completar la receta, !! Complete los datos para continuar");
                    return false;
                }




                var malo = 0;

                for (i = 1; i <= numerodesolicitudes; ++i) {


                    var d = document.forms["hojaatencionoftalmologia"]["diagnosticos" + i].value;
                    var od = document.forms["hojaatencionoftalmologia"]["otrodiagnostico" + i].value;
                    var inter = document.forms["hojaatencionoftalmologia"]["intervencion" + i].value;
                    var a = document.forms["hojaatencionoftalmologia"]["anestesia" + i].value;
                    var r = document.forms["hojaatencionoftalmologia"]["requerimientos" + i].value;
                    var o = document.forms["hojaatencionoftalmologia"]["observacion" + i].value;
                    var l = document.forms["hojaatencionoftalmologia"]["lateralidades" + i].value;

                    if (d == "0" || inter == "" || r == "" || o == "" || a == "0" || l == "0") {
                        malo = 1;
                    }


                }

                if (malo == 1) {
                    Alert.render("Debe Indicar la informacion de las solicitudes de Pabellon!! Complete los datos para continuar");
                    return false;
                }






                /*validacion para no indicaciones quirurgicas*/



                var falto = 0;

                for (i = 1; i <= numeronoindicaciones; ++i) {

                    var d = document.forms["hojaatencionoftalmologia"]["diagnosticonrpt" + i].value;

                    var inter = document.forms["hojaatencionoftalmologia"]["intervencionnrpt" + i].value;
                    var razones = document.forms["hojaatencionoftalmologia"]["razonesnrpt" + i].value;
                    var indicaciones = document.forms["hojaatencionoftalmologia"]["indicacionesnrpt" + i].value;
                    var l = document.forms["hojaatencionoftalmologia"]["lateralidadesnrpt" + i].value;


                    if (d == "0" || inter == "" || razones == "" || indicaciones == "" || l == "0") {
                        falto = 1;
                    }

                }

                if (falto == 1) {
                    Alert.render("Debe Indicar la informacion de las solicitudes de no indicaci�n quir�rgica!! Complete los datos para continuar");
                    return false;
                }

                var fechacontrol = document.forms["hojaatencionoftalmologia"]["fechacontrol"].value;
                var motivocontrol = document.forms["hojaatencionoftalmologia"]["motivocontrol"].value;
                var cotrol = document.forms["hojaatencionoftalmologia"]["controlsolicito"].value;
                if (cotrol == "-1") {
                    Alert.render("Debe Indicar si el paciente requiere Control -> En solicitud de Control!!Complete los datos para Continuar");
                    return false;
                } else if (cotrol == "1") {
                    if (fechacontrol == "" || motivocontrol == "") {
                        Alert.render("Debe Indicar Fecha o Motivo del control!!Complete los datos para Continuar");
                        return false;
                    }
                }

                /*receta medica*/
            <%if (c.getId_tipoatencion() != 5 || c.getId_tipoatencion() != 4) {%>

                var cotrollente = document.forms["hojaatencionoftalmologia"]["lentesolicito"].value;
                if (cotrollente == "-1") {
                    Alert.render("Debe Indicar si el paciente requiere Lentes  -->Receta de Lentes!!Complete los datos para Continuar");
                    return false;
                } else if (cotrollente == "1") {

                    var tratamientocerca = document.forms["hojaatencionoftalmologia"]["tratamientolentesCerca"].checked;
                    var tratamientolejos = document.forms["hojaatencionoftalmologia"]["tratamientolentesLejos"].checked;
                    var lejosaddod = document.forms["hojaatencionoftalmologia"]["odf"].value;
                    var lejosesfod = document.forms["hojaatencionoftalmologia"]["odc"].value;
                    var lejoscylod = document.forms["hojaatencionoftalmologia"]["ode"].value;
                    var lejosejeod = document.forms["hojaatencionoftalmologia"]["oie"].value;
                    var lejosaddoi = document.forms["hojaatencionoftalmologia"]["oic"].value;
                    var lejosesfoi = document.forms["hojaatencionoftalmologia"]["oiej"].value;
                    var lejoscyloi = document.forms["hojaatencionoftalmologia"]["add"].value;


                    if (tratamientocerca || tratamientolejos) {

                        if (lejosaddod == "" & lejosesfod == "" & lejoscylod == "" & lejosejeod == "" & lejosaddoi == "" & lejosesfoi == "" & lejoscyloi == "") {
                            Alert.render("Debe Ingresar la Receta de Lentes!!Complete los datos para Continuar");
                            return false;

                        }


                    } else {
                        Alert.render("Debe Indicar que lentes se va a indicar !!Complete los datos para Continuar");
                        return false;

                    }
                }
            <%}%>


            }

            /*ingresar Examenes*/


            function addRow(id) {

                var examenes = document.forms["hojaatencionoftalmologia"]["examenes"].value;

                var combo = document.getElementById("examenes");
                var selected = combo.options[combo.selectedIndex].text;

                var lateralidad = document.forms["hojaatencionoftalmologia"]["lateralidadsep"].value;
                var combo2 = document.getElementById("lateralidadsep");
                var selected2 = combo2.options[combo2.selectedIndex].text;

                var tiempo = document.forms["hojaatencionoftalmologia"]["tiemposep"].value;
                var combo3 = document.getElementById("tiemposep");
                var selected3 = combo3.options[combo3.selectedIndex].text;

                if (examenes != "" && lateralidad != "" && tiempo != "") {

                    var tbody = document.getElementById
                            (id).getElementsByTagName("TBODY")[0];
                    var row = document.createElement("TR");
                    row.setAttribute('style', ' text-align: center');
                    var td1 = document.createElement("TD");
                    td1.appendChild(document.createTextNode(selected + " " + selected2 + " Realizar en  " + selected3))

                    row.appendChild(td1);

                    var td2 = document.createElement("TD");

                    var boton = document.createElement('button');
                    boton.type = "button";


                    boton.innerHTML = "x";

                    boton.onclick = function () {
                        var td = this.parentNode;
                        var tr = td.parentNode;
                        var table = tr.parentNode;
                        table.removeChild(tr);
                        return false;
                    };

                    td2.appendChild(boton);
                    row.appendChild(td2);

                    var td = document.createElement("TD");

                    var div = document.createElement("input");
                    div.id = "solicitudesexamenes" + examenes;
                    div.name = "solicitudesexamenes" + examenes;
                    div.value = examenes;
                    div.type = "hidden";
                    td.appendChild(div);

                    var div2 = document.createElement("input");
                    div2.id = "lateralidadexamenes" + examenes;
                    div2.name = "lateralidadexamenes" + examenes;
                    div2.value = lateralidad;
                    div2.type = "hidden";
                    td.appendChild(div2);
                    var div3 = document.createElement("input");
                    div3.id = "tiempoexamenes" + examenes;
                    div3.name = "tiempoexamenes" + examenes;

                    div3.value = tiempo;
                    div3.type = "hidden";
                    td.appendChild(div3);

                    row.appendChild(td);



                    tbody.appendChild(row);

                } else {

                    Alert.render("Debe Ingresar Examen, Lateralidad y Tiempo en que se debe realizar !! Complete los datos para continuar");
                    return false;
                }
            }


            function addRow1(id) {


                var procedimiento = document.forms["hojaatencionoftalmologia"]["procedimientos"].value;
                var combo = document.getElementById("procedimientos");
                var selected = combo.options[combo.selectedIndex].text;
                var lateralidad = document.forms["hojaatencionoftalmologia"]["lateralidadsepro"].value;
                var combo2 = document.getElementById("lateralidadsepro");
                var selected2 = combo2.options[combo2.selectedIndex].text;
                var tiempo = document.forms["hojaatencionoftalmologia"]["tiemposepro"].value;
                var combo3 = document.getElementById("tiemposepro");
                var selected3 = combo3.options[combo3.selectedIndex].text;

                if (procedimiento != "" && lateralidad != "" && tiempo != "") {

                    var tbody = document.getElementById
                            (id).getElementsByTagName("TBODY")[0];
                    var row = document.createElement("TR");
                    row.setAttribute('style', ' text-align: center');
                    var td1 = document.createElement("TD");
                    td1.appendChild(document.createTextNode(selected + " " + selected2 + " Realizar en  " + selected3))

                    row.appendChild(td1);

                    var td2 = document.createElement("TD");

                    var boton = document.createElement('button');
                    boton.type = "button";


                    boton.innerHTML = "x";

                    boton.onclick = function () {
                        var td = this.parentNode;
                        var tr = td.parentNode;
                        var table = tr.parentNode;
                        table.removeChild(tr);
                        return false;
                    };

                    td2.appendChild(boton);
                    row.appendChild(td2);

                    var td = document.createElement("TD");

                    var div = document.createElement("input");
                    div.id = "solicitudesprocedimiento" + procedimiento;
                    div.name = "solicitudesprocedimiento" + procedimiento;
                    div.value = procedimiento;
                    div.type = "hidden";
                    td.appendChild(div);

                    var div2 = document.createElement("input");
                    div2.id = "lateralidadprocedimiento" + procedimiento;
                    div2.name = "lateralidadprocedimiento" + procedimiento;
                    div2.value = lateralidad;
                    div2.type = "hidden";
                    td.appendChild(div2);
                    var div3 = document.createElement("input");
                    div3.id = "tiempoprocedimiento" + procedimiento;
                    div3.name = "tiempoprocedimiento" + procedimiento;

                    div3.value = tiempo;
                    div3.type = "hidden";
                    td.appendChild(div3);

                    row.appendChild(td);



                    tbody.appendChild(row);

                } else {

                    Alert.render("Debe Ingresar Procedimiento, Lateralidad y Tiempo en que se debe realizar !! Complete los datos para continuar");
                    return false;
                }
            }




            function borrarFila(t) {
                var td = t.parentNode;
                var tr = td.parentNode;
                var table = tr.parentNode;
                table.removeChild(tr);
                var solicitud = document.getElementById('numerodesolicitudes').value;
                document.getElementById('numerodesolicitudes').value = parseInt(solicitud) - parseInt(1);
            }


            function borrarFilasolicitud(t) {
                var td = t.parentNode;
                var tr = td.parentNode;
                var table = tr.parentNode;
                table.removeChild(tr);
                var solicitud = document.getElementById('numerodesolicitudesnrpt').value;
                document.getElementById('numerodesolicitudesnrpt').value = parseInt(solicitud) - (1);
            }

            function borrarFilaotro(t) {
                var td = t.parentNode;
                var tr = td.parentNode;
                var table = tr.parentNode;
                table.removeChild(tr);

            }


            $(function () {
                $("#fechacontrol").datepicker();
            });

            function openCity(evt, cityName) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }
                document.getElementById(cityName).style.display = "block";
                evt.currentTarget.className += " active";
            }

        // Get the element with id="defaultOpen" and click on it
            document.getElementById("defaultOpen").click();


            function addSolicitud(id) {

                var yea = document.getElementById(id).rows.length;

                var solicitud = document.getElementById('numerodesolicitudes').value;

                document.getElementById('numerodesolicitudes').value = parseInt(solicitud) + parseInt(1);
                var tbody = document.getElementById
                        (id).getElementsByTagName("TBODY")[0];
                var row = document.createElement("TR");
                row.setAttribute('style', ' text-align: center');
                var td1 = document.createElement("TD");
                var selectd = document.createElement('select');

                selectd.id = "diagnosticos" + yea;
                selectd.name = "diagnosticos" + yea;
                selectd.setAttribute('style', 'width: 80%');
            <%for (lugar lud : ce.buscarlosDiagnosticoEspecialidad(idespecialidad)) {

            %>

                var nombre = '<%=lud.getNombre()%>';
                selectd.options[selectd.options.length] = new Option(nombre,<%=lud.getId_atencion()%>);


            <%}%>

                td1.appendChild(selectd);
                row.appendChild(td1);


                /*otro diagnostico*/
                var td6 = document.createElement("TD");
                var div = document.createElement("div");
                div.id = "otrodiagnosticosicmuestro" + yea;
                div.style.display = "none";
                var text = document.createElement('textarea');
                text.id = "otrodiagnostico" + yea;
                text.name = "otrodiagnostico" + yea;
                text.rows = "2";
                text.cols = "15";
                div.appendChild(text);
                td6.appendChild(div);
                row.appendChild(td6);


                var td3 = document.createElement("TD");
                var select3 = document.createElement('select');
                select3.id = "lateralidades" + yea;
                select3.name = "lateralidades" + yea;
            <%for (int m = 0; m < lateralidad.length; ++m) {%>

                var option3 = document.createElement("option");

                option3.textContent = '<%=lateralidad[m]%>';
                option3.value = <%=m%>
                select3.add(option3);


            <%}%>
                select3.onchange = function () {
                    var diagnostico = document.getElementById('diagnosticos' + yea).value;
                    var lateralidad = document.getElementById('lateralidades' + yea).value;
                    var la = '';
                    if (lateralidad == 1) {
                        la = 'OJO DERECHO';
                    } else if (lateralidad == 2) {
                        la = 'OJO IZQUIERDO';
                    }

                    if ((diagnostico == 2) || (diagnostico == 31)) {
                        document.getElementById("intervencion" + yea).value = 'FACOERESIS CON IMPLANTE INTRAOCULAR ' + la;
                    } else {
                        document.getElementById("intervencion" + yea).value = ' ';

                    }

                }

                td3.appendChild(select3);
                row.appendChild(td3);

                var td4 = document.createElement("TD");
                var text = document.createElement('textarea');
                text.id = "intervencion" + yea
                text.name = "intervencion" + yea
                text.rows = "2";
                text.cols = "15";
                td4.appendChild(text);
                row.appendChild(td4);

                /*anestesia*/
                var td7 = document.createElement("TD");
                var select7 = document.createElement('select');
                select7.id = "anestesia" + yea;
                select7.name = "anestesia" + yea;
            <%for (tipo_anestesia temp : ce.tiposdeanestesia()) {
            %>

                var option7 = document.createElement("option");

                option7.textContent = '<%=temp.getNombretipo()%>';
                option7.value = <%=temp.getIdtipo_atencion()%>
                select7.add(option7);


            <%}%>

                td7.appendChild(select7);
                row.appendChild(td7);
                /**/

                var td5 = document.createElement("TD");
                var text = document.createElement('textarea');
                text.id = "requerimientos" + yea
                text.name = "requerimientos" + yea
                text.rows = "2";
                text.cols = "15";
                td5.appendChild(text);
                row.appendChild(td5);

                var td6 = document.createElement("TD");
                var text = document.createElement('textarea');
                text.id = "observacion" + yea;
                text.name = "observacion" + yea;
                td6.appendChild(text);
                row.appendChild(td6);





                var td2 = document.createElement("TD");

                var boton = document.createElement('button');
                boton.type = "button";


                boton.innerHTML = "x";

                boton.onclick = function () {
                    var td = this.parentNode;
                    var tr = td.parentNode;
                    var table = tr.parentNode;
                    table.removeChild(tr);
                    var solicitud = document.getElementById('numerodesolicitudes').value;
                    document.getElementById('numerodesolicitudes').value = parseInt(solicitud)-parseInt(1);
                    return false;
                    
                };

                td2.appendChild(boton);
                row.appendChild(td2);




                tbody.appendChild(row);


            }



            function addnoInc(id) {

                var yea = document.getElementById(id).rows.length;

                var nosolicitud = document.getElementById('numerodesolicitudesnrpt').value;

                document.getElementById('numerodesolicitudesnrpt').value = parseInt(nosolicitud) + parseInt(1);

                var tbody = document.getElementById
                        (id).getElementsByTagName("TBODY")[0];
                var row = document.createElement("TR");
                row.setAttribute('style', ' text-align: center');
                var td1 = document.createElement("TD");

                /*diagnosticos*/
                var selectd = document.createElement('select');

                selectd.id = "diagnosticonrpt" + yea;
                selectd.name = "diagnosticonrpt" + yea;
                selectd.setAttribute('style', 'width: 70%');
            <%for (lugar lud : ce.buscarlosDiagnosticoEspecialidad(idespecialidad)) {

            %>

                var nombre = '<%=lud.getNombre()%>';
                selectd.options[selectd.options.length] = new Option(nombre,<%=lud.getId_atencion()%>);


            <%}%>

                td1.appendChild(selectd);
                row.appendChild(td1);


                /*otro diagnostico*/
                var td6 = document.createElement("TD");
                var div = document.createElement("div");
                div.id = "otrodiagnosticosicmuestro" + yea;
                div.style.display = "none";
                var text = document.createElement('textarea');
                text.id = "otrodiagnosticosicmuestro" + yea;
                text.name = "otrodiagnosticosicmuestro" + yea;
                text.rows = "2";
                text.cols = "30";

                div.appendChild(text);
                td6.appendChild(div);
                row.appendChild(td6);


                /*lo que mostrare*/
                var td3 = document.createElement("TD");
                var select3 = document.createElement('select');
                select3.id = "lateralidadesnrpt" + yea;
                select3.name = "lateralidadesnrpt" + yea;
            <%for (int m = 0; m < lateralidad.length; ++m) {%>

                var option3 = document.createElement("option");

                option3.textContent = '<%=lateralidad[m]%>';
                option3.value = <%=m%>
                select3.add(option3);


            <%}%>
                select3.onchange = function () {
                    var diagnostico = document.getElementById("diagnosticonrpt" + yea).value;
                    var lateralidad = document.getElementById("lateralidadesnrpt" + yea).value;
                    var la = '';
                    if (lateralidad == 1) {
                        la = 'OJO DERECHO';
                    } else if (lateralidad == 2) {
                        la = 'OJO IZQUIERDO';
                    }

                    if ((diagnostico == 2) || (diagnostico == 31)) {
                        document.getElementById("intervencionnrpt" + yea).value = 'FACOERESIS CON IMPLANTE INTRAOCULAR ' + la;
                    } else {
                        document.getElementById("intervencionnrpt" + yea).value = '';
                    }
                };


                td3.appendChild(select3);
                row.appendChild(td3);



                /*lo ue escribo*/


                var td4 = document.createElement("TD");
                var text2 = document.createElement("textarea");
                text2.id = "intervencionnrpt" + yea;
                text2.name = "intervencionnrpt" + yea;
                text2.rows = "2";
                text2.cols = "15";


                td4.appendChild(text2);
                row.appendChild(td4);



                /*anestesia*/
                var td7 = document.createElement("TD");
                var text3 = document.createElement("textarea");
                text3.id = "razonesnrpt" + yea;
                text3.name = "razonesnrpt" + yea;
                text3.rows = "2";
                text3.cols = "15";

                td7.appendChild(text3);
                row.appendChild(td7);
                /**/

                var td5 = document.createElement("TD");
                var text4 = document.createElement("textarea");
                text4.id = "indicacionesnrpt" + yea;
                text4.name = "indicacionesnrpt" + yea;
                text4.rows = "2";
                text4.cols = "15";

                td5.appendChild(text4);
                row.appendChild(td5);

                var td2 = document.createElement("TD");

                var boton = document.createElement('button');
                boton.type = "button";


                boton.innerHTML = "x";

                boton.onclick = function () {
                    var td = this.parentNode;
                    var tr = td.parentNode;
                    var table = tr.parentNode;
                    table.removeChild(tr);
                    var nosolicitud = document.getElementById('numerodesolicitudesnrpt').value;
                    document.getElementById('numerodesolicitudesnrpt').value = parseInt(nosolicitud) - parseInt(1);
                    return false;

                };

                td2.appendChild(boton);
                row.appendChild(td2);




                tbody.appendChild(row);




            }

        </script>
    </body>
<jsp:include page="../comunes/Footer.jsp"/>
