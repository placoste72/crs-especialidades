<%-- 
    Document   : MostrarImagen
    Created on : 28-11-2016, 02:54:33 PM
    Author     : Informatica
--%>

<%@page import="Controlador.General"%>

<!DOCTYPE html>
<%
    String codigo_cita = request.getParameter("cita");
    String tipo = request.getParameter("tipo");
    General g = new General();
%>
<html>
    <head>

        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <script src="../../public/lib/alertify.min.js" type="text/javascript"></script>
        <link href="../../public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="../../public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../public/css/bootstrap-select.css">
        <link href="../../public/css/estilos.css" rel="stylesheet">

        <link href="../../public/css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="../../public/js/jquery-1.10.2.js" type="text/javascript"></script>
        <link href="../../public/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <script src="../../public/js/jquery.min.js"></script>
        <script src="../../public/js/bootstrap.min.js"></script>

        <title></title>
        <style type="text/css">

            body {

                margin-left: 0px;
                margin-top: 0px;
                margin-right: 0px;
                margin-bottom: 0px;
            }
        </style>
    </head>

    <% String mensaje = "";
        if (request.getParameter("men") != null) {
            mensaje = request.getParameter("men");
    %>
    <script  type="text/javascript">

        window.opener.location.href = "<%=g.getLocallink()%>AtencionOftalmologia/Lista.jsp?men=<%=mensaje%>&idatencion=0";
            window.close();

    </script>


    <%
    } else {%>

    <form style="border: #619fd8 5px solid;" name="cancelaratencion" class="form-horizontal" id="cancelarantencion" action='<%=g.getLocal()%>cancelaratencionOftalmologo' method="post"  >
        <center>
            <input type="text" value="<%=codigo_cita%>" id="cita" name="cita" hidden>
            <input type="text" value="<%=tipo%>" id="tipo" name="tipo" hidden>
            <div class="form-group">
                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-edit bigicon">Motivo</i></span>
                <div class="col-md-6">
                    <textarea style=" width:300px;"   rows="3" id="motivo" name="motivo"  class="form-control" placeholder="Motivo" required oninvalid="setCustomValidity('El campo motivo es obligatorio')" oninput="setCustomValidity('')"></textarea>


                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12 text-center">
                    <%if (tipo.equalsIgnoreCase("C")) {%> 
                    <button type="submit" class="btn btn-primary btn-lg">Cancelar Cita </button>
                    <%} else {%>
                    <button type="submit" class="btn btn-primary btn-lg">Reanudar Cita </button>
                    <%}%> 

                </div>
            </div>
        </center>

    </form>
    <%}%>                 
</body>
</html>
