<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Recuperar Contraseña</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <script src="public/lib/alertify.min.js" type="text/javascript"></script>
        <link href="public/themes/alertify.core.css" rel="stylesheet" type="text/css"/>


        <link href="public/themes/alertify.default.css" rel="stylesheet" type="text/css"/>

        <link href="public/css/bootstrap.min.css" rel="stylesheet">


        <link href="public/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="public/imagenes/icon.png">
    </head>

    <body>
        <script>

            function reset() {
                $("#toggleCSS").attr("href", "public/themes/alertify.default.css");
                alertify.set({
                    labels: {
                        ok: "OK",
                        cancel: "Cancel"
                    },
                    delay: 5000,
                    buttonReverse: false,
                    buttonFocus: "ok"
                });
            }

            function formateaRut(Rut)
            {
                var sRut = new String(Rut);
                var sRutFormateado = '';
                sRut = quitaFormato(sRut);
                var sDV = sRut.charAt(sRut.length - 1);
                sRut = sRut.substring(0, sRut.length - 1);
                document.forms["f"].txtRutSinDV.value = sRut;
                document.forms["f"].txtDV.value = sDV;
                while (sRut.length > 3)
                {
                    sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
                    sRut = sRut.substring(0, sRut.length - 3);
                }
                sRutFormateado = sRut + sRutFormateado;
                if (sRutFormateado != "")
                    sRutFormateado += "-";
                sRutFormateado += sDV;
                if (document.forms["f"].usuario.value != sRutFormateado)
                    document.forms["f"].usuario.value = sRutFormateado;
            }
            function quitaFormato(Nro)
            {
                var strNro = new String(Nro);
                while (strNro.indexOf(".") != - 1)
                    strNro = strNro.replace(".", "");
                strNro = strNro.replace("-", "");
                return strNro;
            }
        </script>
        <%
            String mensaje = "";

            if (request.getParameter("men") != null) {
                mensaje = request.getParameter("men");
        %>
        <script>
            var me;

            me = "<%=mensaje%>";

            reset();
            alertify.success(me);
        </script>


        <%
        }%>
   
        <!--login modal-->
        <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h1 class="text-center"><img src="public/imagenes/logo.png" WIDTH=150 HEIGHT=130></h1>
                    </div>
                    <div class="modal-body">
                        <p style="color: #33adff">${sessionScope['ok']}</p>
                        <form class="form col-md-12 center-block" id="f" name="f" action="recuperar_contrasena" method="POST"  >
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" placeholder="Usuario" id="usuario" style="text-transform:uppercase;" name="usuario"  maxlength="12" onkeyup="formateaRut(this.value);" required oninvalid="setCustomValidity('El campo usuario es obligatorio')" oninput="setCustomValidity('')">
                                <input value="dfaddsds" name="txtRutSinDV" id="txtRutSinDV" type="hidden">
                                <input value="d" name="txtDV" id="txtDV" type="hidden">
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>

                                <button class="btn btn-primary btn-lg btn-block" type="submit" onclick="location = 'login.html'">Ingreso al Sistema</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12">

                        </div>	
                    </div>
                </div>
            </div>
        </div>
        <!-- script references -->
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap.min.js"></script>
    </body>
</html>