/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;
import Controlador.controlador_doctor;
import Controlador.controlador_especialidad;
import Modelos.lugar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Vector;
import Modelos.pdfconpieyfoto;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class informeProcesoDiagnostico extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException, SQLException {
        Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
    DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

    String llego = request.getParameter("idhojadiaria");
    controlador_especialidad ce = new controlador_especialidad();
    lugar l = ce.BuscarIpdPARAhojaDiaria(Integer.parseInt(llego));
    Vector<lugar> diagnosticos = ce.buscarDiagnosticosdeHojaparaIPsinblancos(Integer.parseInt(llego));
    
    if(l.getDiagnostico() != ""){
      lugar otrodiagnostico = new lugar();
      diagnosticos.add(otrodiagnostico);
      
    }
  
    response.setContentType("application/pdf");
    Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    PdfWriter writer = PdfWriter.getInstance(document, buffer);

    int SPACE_TITULO = 1;
    int SPACE_NORMAL = 12;
    int SPACE_NORMAL2 = 17;
    int SPACE_ESPACIO = 4;
    Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(68, 117, 196));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
    Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
    Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(0, 0, 0));
    Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
    Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
    Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD, new Color(68, 114, 196));
    Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
    writer.setPageEvent(new pdfconpieyfoto());
    document.open();

    String serviciot = "";
    String establecimientot = "";
    String especialidadt = "";
    String unidadt = "";

    serviciot = "Metropolitano Central(SSMC)";
    establecimientot = "Centro de Referencia de Salud de Maipú";
    especialidadt = "Oftalmología";
    unidadt = "Especialidades";
    boolean tengocataratas = false;

    for (int d = 0; d < diagnosticos.size(); ++d) {

        if ((diagnosticos.get(d).getId_diagnostico_urgencia() == 2) || (diagnosticos.get(d).getId_diagnostico_urgencia() == 31)) {
            tengocataratas = true;
        }

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "INFORME DEL PROCESO DIAGNÓSTICO - IPD", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /**/
        Paragraph rutCita = new Paragraph();
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, "Folio:", TEXT_NORMAL));
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, l.getFolio(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutCita);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /**/
        Paragraph nombreCita = new Paragraph();
        nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita.add(new Phrase(SPACE_TITULO, "Fecha y hora del Informe: ", TEXT_NORMAL));
        nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita.add(new Phrase(SPACE_TITULO, l.getDescripcion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombreCita);
        celda.setBorderWidth(0);

        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Prestador", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph fechaCita = new Paragraph();
        fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechaCita.add(new Phrase(SPACE_TITULO, "Servicio de Salud: ", TEXT_NORMAL));
        fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechaCita.add(new Phrase(SPACE_TITULO, serviciot, TEXT_SUPERTITULONORMAL));
        celda = new Cell(fechaCita);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph horaCita = new Paragraph();
        horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        horaCita.add(new Phrase(SPACE_TITULO, "Establecimiento: ", TEXT_NORMAL));
        horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        horaCita.add(new Phrase(SPACE_TITULO, establecimientot, TEXT_SUPERTITULONORMAL));
        celda = new Cell(horaCita);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph especialidadCita = new Paragraph();
        especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        especialidadCita.add(new Phrase(SPACE_TITULO, "Especialidad: ", TEXT_NORMAL));
        especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        especialidadCita.add(new Phrase(SPACE_TITULO, l.getPrescripcion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(especialidadCita);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph medicoCita = new Paragraph();
        medicoCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        medicoCita.add(new Phrase(SPACE_TITULO, "Unidad:", TEXT_NORMAL));
        medicoCita.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
        medicoCita.add(new Phrase(SPACE_TITULO, unidadt, TEXT_SUPERTITULONORMAL));
        celda = new Cell(medicoCita);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*datos del paciente*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Paciente", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa = new Paragraph();
        AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
        AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa.add(new Phrase(SPACE_TITULO, l.getNombre(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa);
        //celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa1 = new Paragraph();
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, l.getRut(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph fechanacimiento = new Paragraph();
        fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechanacimiento.add(new Phrase(SPACE_TITULO, "Fecha de nacimiento: ", TEXT_NORMAL));
        fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechanacimiento.add(new Phrase(SPACE_TITULO, l.getFechanacimiento(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(fechanacimiento);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph edad = new Paragraph();
        edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
        edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad.add(new Phrase(SPACE_TITULO, l.getEdad() + " años", TEXT_SUPERTITULONORMAL));
        celda = new Cell(edad);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph sexo = new Paragraph();
        sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo.add(new Phrase(SPACE_TITULO, "Sexo: ", TEXT_NORMAL));
        sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo.add(new Phrase(SPACE_TITULO, l.getSexo(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(sexo);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*Datos de derivacion*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos Clínicos", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph sederiva = new Paragraph();
        sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva.add(new Phrase(SPACE_TITULO, "Problema de Salud AUGE: ", TEXT_NORMAL));
        sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        celda = new Cell(sederiva);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph seenvia = new Paragraph();
        seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia.add(new Phrase(SPACE_TITULO, "¿Confirma  que el Diagnóstico pertenece al sistema AUGE? ", TEXT_NORMAL));
        seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        celda = new Cell(seenvia);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph diagnostico = new Paragraph();
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, "Diagnóstico: ", TEXT_NORMAL));
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, diagnosticos.get(d).getDiagnostico(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(diagnostico);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        if (tengocataratas == true) {
            Paragraph fd = new Paragraph();
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, "Fundamentos del diagnóstico: ", TEXT_NORMAL));
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, l.getVariable1(), TEXT_SUPERTITULONORMAL));
           
            celda = new Cell(fd);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph examenes = new Paragraph();
            examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            examenes.add(new Phrase(SPACE_TITULO, "Tratamiento e indicaciones: ", TEXT_NORMAL));
            examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            examenes.add(new Phrase(SPACE_TITULO, l.getVariable2(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(examenes);
        } else {

            Paragraph fd = new Paragraph();
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, "Fundamentos del diagnóstico: ", TEXT_NORMAL));
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, "_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________", TEXT_SUPERTITULONORMAL));

            celda = new Cell(fd);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph examenes = new Paragraph();
            examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            examenes.add(new Phrase(SPACE_TITULO, "Tratamiento e indicaciones: ", TEXT_NORMAL));
            examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            examenes.add(new Phrase(SPACE_TITULO, "____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________", TEXT_SUPERTITULONORMAL));
            celda = new Cell(examenes);
        }
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Profesional Tratante", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph nombredoctor = new Paragraph();
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, l.getNombredoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombredoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph rutdoctor = new Paragraph();
        rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor.add(new Phrase(SPACE_TITULO, l.getRutdoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutdoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph profesiondoctor = new Paragraph();
        profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesiondoctor.add(new Phrase(SPACE_TITULO, "Profesión: ", TEXT_NORMAL));
        profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesiondoctor.add(new Phrase(SPACE_TITULO, l.getSederivapara(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(profesiondoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        /*para la firma del doctor*/
        controlador_doctor cdoctor = new controlador_doctor();

        byte[] imag = cdoctor.obtenImagenDoctor(l.getRutdoctor());
        if (imag != null) {
            Image image22 = Image.getInstance(imag);

            image22.setAbsolutePosition(15f, 15f);
            document.add(image22);
        }

        document.add(tabla_titulo);
        document.newPage();
        tengocataratas = false;

    }

    document.close();
    try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
        DataOutput output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
//output.flush();            
//output.close();
    } catch (Exception exstream) {
    }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(informeProcesoDiagnostico.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(informeProcesoDiagnostico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(informeProcesoDiagnostico.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(informeProcesoDiagnostico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
