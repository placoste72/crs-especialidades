/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;

import Controlador.controlador_doctor;
import Controlador.controlador_especialidad;
import Modelos.hojadiaria;
import Modelos.lugar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Vector;
import Modelos.pdfcopia1de2;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class intervencionesquirurgicas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.MEDIUM, currentLocale);
        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);

        DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

        Date fechaDate = new Date();
        String llego = request.getParameter("idhojadiaria");

        controlador_especialidad ce = new controlador_especialidad();
        hojadiaria hd = ce.BuscarconstanciaGes(Integer.parseInt(llego));
        Vector<lugar> vl = ce.buscarlosdiagnosticosdeunaHojaDiaria(Integer.parseInt(llego));
        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 50);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        int SPACE_TITULO = 1;
        int SPACE_NORMAL = 1;
        int SPACE_NORMAL2 = 4;
        int SPACE_ESPACIO = 10;

        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0));

        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULO2 = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_FIRMA = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, new Color(0, 0, 0));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
        writer.setPageEvent(new pdfcopia1de2());
        document.open();
        for (lugar solicitud : ce.buscarLasSolicitudesdePabellondeyunaHojaDiaria(Integer.parseInt(llego))) {
            Table tabla_titulo;
            Cell celda;

            tabla_titulo = new Table(3);
            tabla_titulo.setBorderWidth(0);
            tabla_titulo.setPadding(0);
            tabla_titulo.setSpacing(0);
            tabla_titulo.setWidth(100);

            String uno = "";
            String dos = "";
            String tres = "";
            String cuatro = "";
            String cinco = "";
            String seis = "";
            String siete = "";
            String ocho = "";
            String unodenegacion = "";
            String dosdenegacion = "";
            String tresdenegacion = "";
            String cuatrodenegacion = "";

            unodenegacion = "1.    Declaro haber recibido toda la información en forma satisfactoria sobre mi cuadro clínico, de la "
                    + "     naturaleza del procedimiento diagnóstico y/o tratamiento. Se me ha informado de los riesgos y "
                    + "     complicaciones posibles, así como las alternativas terapéuticas. ";

            dosdenegacion = "2. he entendido la información y he tenido la oportunidad de formular preguntas y resolver mis dudas. ";
            tresdenegacion = "3. Teniendo presente lo expuesto anteriormente Deniego/Revoco en forma libre y consciente el intervencion:";
            cuatrodenegacion = " 4. En forma libre, voluntaria, expresa e informada asumo las responsabilidades que puedan derivase de esta decision.";

            uno = "1. Que se me ha informado que presento el siguiente  diagnóstico :";
            dos = "2. En mi calidad de paciente, representante o acompañante he recibido toda la información necesaria "
                    + " para decidir en forma libre, voluntaria, expresa e informada, la realización de la siguiente intervención:";

            tres = "3. El profesional responsable me ha dado a conocer los objetivos de la intervención, sus características "
                    + " y la necesidad de realizar dicho procedimiento considerando mi diagnóstico, los potencialies riesgos "
                    + " asociados a mi condición actual de salud, u la probabilidad de presentar posibles complicaciones, las  "
                    + " que he comprendido completamente.";

            cuatro = "4. Me han informado que esta intervención propuesta es parte del tratamiento disponible en la intitucion,"
                    + "necesarios para mi recuperación.";
            cinco = "5. Me han explicado el pronóstico esperado, y respecto del proceso previsible luego del procedimiento o "
                    + "post operatorio, considerando mi edad, estado de salud físico y condición personal y emocional.";

            seis = "6. Comprendo que este consentimiento puede ser revocado libremente si así lo decidiera.";

            siete = "7. Por lo tanto, en pleno uso de mis facultades, doy mi consentimiento para la realización de la "
                    + " intervención señalada.";
            ocho = "8. Se me entregó información complementaria acerca de intervencion a realizar ";

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "FORMULARIO DE CONSENTIMIENTO INFORMADO INTERVENCIONES QUIRÚRGICAS", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo.addCell(celda);

            /**/
 /**/
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            Paragraph nombreCita = new Paragraph();
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, "Fecha : ", TEXT_SUPERTITULO));
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, formateadorFecha.format(hd.getFecha_registro()), TEXT_NORMAL));
            celda = new Cell(nombreCita);
            celda.setBorderWidth(0);

            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph AtencionGlosa = new Paragraph();
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "NOMBRE PACIENTE: ", TEXT_SUPERTITULO));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, hd.getOtro_procedimiento(), TEXT_NORMAL));
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "  ", TEXT_SUPERTITULO));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, " ", TEXT_SUPERTITULONORMAL));
            celda = new Cell(AtencionGlosa);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph AtencionGlosa2 = new Paragraph();
            AtencionGlosa2.add(new Phrase(SPACE_NORMAL2, "RUT PACIENTE: ", TEXT_SUPERTITULO));
            AtencionGlosa2.add(new Phrase(SPACE_TITULO, hd.getOtrodestino_paciente(), TEXT_NORMAL));
            AtencionGlosa2.add(new Phrase(SPACE_NORMAL2, "  ", TEXT_SUPERTITULO));
            AtencionGlosa2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(AtencionGlosa2);

            //celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);

            Paragraph domicilio = new Paragraph();
            domicilio.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            domicilio.add(new Phrase(SPACE_TITULO, "A través del presente FORMULARIO DE CONSENTIMIENTO INFORMADO declaro: ", TEXT_NORMAL));
            domicilio.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(domicilio);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph telefonos = new Paragraph();
            telefonos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            telefonos.add(new Phrase(SPACE_TITULO, uno, TEXT_NORMAL));
            telefonos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            telefonos.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(telefonos);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph correo = new Paragraph();
            correo.add(new Phrase(SPACE_NORMAL2, solicitud.getComuna(), TEXT_NORMAL));
            correo.add(new Phrase(SPACE_TITULO, " ", TEXT_NORMAL));
            correo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(correo);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph dos1 = new Paragraph();
            dos1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            dos1.add(new Phrase(SPACE_TITULO, dos, TEXT_NORMAL));
            dos1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            dos1.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(dos1);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph sederiva = new Paragraph();
            sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            sederiva.add(new Phrase(SPACE_TITULO, solicitud.getIntervencio(), TEXT_NORMAL));
            sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            String diagnostico = " ";
            sederiva.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(sederiva);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph seenvia = new Paragraph();
            seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            seenvia.add(new Phrase(SPACE_TITULO, tres, TEXT_NORMAL));
            seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(seenvia);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph see = new Paragraph();
            see.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            see.add(new Phrase(SPACE_TITULO, cuatro, TEXT_NORMAL));
            see.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(see);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph seenvia2 = new Paragraph();
            seenvia2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            seenvia2.add(new Phrase(SPACE_TITULO, cinco, TEXT_NORMAL));
            seenvia2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(seenvia2);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph seis1 = new Paragraph();
            seis1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            seis1.add(new Phrase(SPACE_TITULO, seis, TEXT_NORMAL));
            seis1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(seis1);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph siete1 = new Paragraph();
            siete1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            siete1.add(new Phrase(SPACE_TITULO, siete, TEXT_NORMAL));
            siete1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(siete1);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph ocho1 = new Paragraph();
            ocho1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            ocho1.add(new Phrase(SPACE_TITULO, ocho + "         SI [x]  NO [ ] ", TEXT_NORMAL));
            ocho1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(ocho1);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph nap = new Paragraph();
            nap.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nap.add(new Phrase(SPACE_TITULO, " Nombre y apellido del Profesional informante:  " + hd.getOtro_diagnostico(), TEXT_NORMAL));
            nap.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(nap);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph nar = new Paragraph();
            nar.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nar.add(new Phrase(SPACE_TITULO, " Rut del Profesional:  " + hd.getOtro_examen(), TEXT_NORMAL));
            nar.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(nar);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);


            /*firmas y pie*/
            Paragraph raya = new Paragraph();
            raya.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            // nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));

            raya.add(new Phrase(SPACE_TITULO, "__________________                _______________________                     __________________________", TEXT_FIRMA));
            celda = new Cell(raya);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            /*pie de documento*/
            Paragraph pie = new Paragraph();
            pie.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            pie.add(new Phrase(SPACE_TITULO, "Firma del  Paciente                    Firma del  Profesional                            Firma de representante", TEXT_FIRMA));
            // pie.add(new Phrase(SPACE_TITULO, "Firma de la persona que notifica                                    Firma o huella digital del paciente o representante", TEXT_NORMAL2));

            celda = new Cell(pie);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph nar3 = new Paragraph();
            nar3.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nar3.add(new Phrase(SPACE_TITULO, "                                                                                Nombre y apellido:    ", TEXT_FIRMA));
            nar3.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(nar3);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo.addCell(celda);

            Paragraph nar4 = new Paragraph();
            nar4.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nar4.add(new Phrase(SPACE_TITULO, "                                                                             ________________________", TEXT_FIRMA));
            nar4.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

            celda = new Cell(nar4);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            /*Seguda Hoja*/

 /*Segunda pagina*/
            document.add(tabla_titulo);
            document.newPage();

            Table tabla_titulo2;

            tabla_titulo2 = new Table(3);
            tabla_titulo2.setBorderWidth(0);
            tabla_titulo2.setPadding(0);
            tabla_titulo2.setSpacing(0);
            tabla_titulo2.setWidth(100);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Apartado para Denegación/Revocación", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo2.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            /**/
            Paragraph rutCita2 = new Paragraph();
            rutCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutCita2.add(new Phrase(SPACE_TITULO, unodenegacion, TEXT_NORMAL));
            rutCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutCita2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(rutCita2);
            celda.setBorderWidth(0);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo2.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            /**/
            Paragraph nombreCita2 = new Paragraph();
            nombreCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita2.add(new Phrase(SPACE_TITULO, dosdenegacion, TEXT_NORMAL));
            nombreCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(nombreCita2);
            celda.setBorderWidth(0);

            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            Paragraph fechaCita2 = new Paragraph();
            fechaCita2.add(new Phrase(SPACE_NORMAL2, tresdenegacion, TEXT_NORMAL));
            fechaCita2.add(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
            fechaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fechaCita2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(fechaCita2);
            celda.setBorderWidth(0);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            Paragraph inter = new Paragraph();
            inter.add(new Phrase(SPACE_NORMAL2, solicitud.getIntervencio(), TEXT_NORMAL));
            inter.add(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
            inter.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            inter.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(inter);
            celda.setBorderWidth(0);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            Paragraph horaCita2 = new Paragraph();
            horaCita2.add(new Phrase(SPACE_NORMAL2, cuatrodenegacion + "\n", TEXT_NORMAL));
            horaCita2.add(new Phrase(SPACE_TITULO, "Desisto de mi aceptación previa. Asumo las consecuencias que ello me / le puediera ocasionar ", TEXT_NORMAL));
            horaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            horaCita2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(horaCita2);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            Paragraph especialidadCita2 = new Paragraph();
            especialidadCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            especialidadCita2.add(new Phrase(SPACE_TITULO, "Nombre y apellido Paciente/representante: " + hd.getOtro_procedimiento() + " Rut: " + hd.getOtrodestino_paciente(), TEXT_NORMAL));
            especialidadCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            especialidadCita2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(especialidadCita2);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo2.addCell(celda);
            // celda.setBackgroundColor(new Color(217, 225, 242));

            Paragraph medicoCita2 = new Paragraph();
            medicoCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            medicoCita2.add(new Phrase(SPACE_TITULO, "Nombre y apellido médico/profesional:                                  Rut: ", TEXT_NORMAL));
            medicoCita2.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
            // medicoCita.add(new Phrase(SPACE_TITULO, "Dr."+nombredoctort + "," + especialidadt + ", RUT " + rutdoctort, TEXT_SUPERTITULONORMAL));
            celda = new Cell(medicoCita2);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            Paragraph medCita2 = new Paragraph();
            medCita2.add(new Phrase(SPACE_NORMAL2, "__________________________                                              __________________________", TEXT_FIRMA));
            // medicoCita.add(new Phrase(SPACE_TITULO, "Nombre persona que notifica en representación del prestador:", TEXT_NORMAL));
            medCita2.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
            // medCita2.add(new Phrase(SPACE_TITULO, "Firma paciente/representante ", TEXT_SUPERTITULO2));
            celda = new Cell(medCita2);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo2.addCell(celda);

            Paragraph firma = new Paragraph();
            //medCita2.add(new Phrase(SPACE_NORMAL2, "__________________________  ", TEXT_SUPERTITULO2));
            // medicoCita.add(new Phrase(SPACE_TITULO, "Nombre persona que notifica en representación del prestador:", TEXT_NORMAL));
            firma.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
            firma.add(new Phrase(SPACE_TITULO, "Firma paciente/representante                                                Firma médico/profesional", TEXT_FIRMA));
            celda = new Cell(firma);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tabla_titulo2.addCell(celda);

            /*datos del paciente*/
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo2.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Fecha de Revocación:________________________________________ ", TEXT_FIRMA));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo2.addCell(celda);

            document.add(tabla_titulo2);
            document.newPage();
        }
        document.close();
        try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }
//output.flush();            
//output.close();
        } catch (Exception exstream) {
        }

      
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(intervencionesquirurgicas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(intervencionesquirurgicas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
