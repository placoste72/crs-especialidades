/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;

import Controlador.controlador_especialidad;
import Modelos.lugar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Vector;
import Modelos.pdfconpieyfoto;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class SolicitudpabellonQuirurgico extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
        DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

        String llego = request.getParameter("idhojadiaria");
        controlador_especialidad ce = new controlador_especialidad();
        lugar l = ce.BuscardatosparaSoliciatudpabellon(Integer.parseInt(llego));
        Vector<lugar> diagnosticos = ce.buscarlosdiagnosticosdeunaHojaDiaria(Integer.parseInt(llego));
        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        int SPACE_TITULO = 5;

        int SPACE_NORMAL2 = 17;
        int SPACE_ESPACIO = 4;
        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(0, 0, 0));
        //Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
        writer.setPageEvent(new pdfconpieyfoto());
        document.open();

        for (lugar solicitud : ce.buscarLasSolicitudesdePabellondeyunaHojaDiaria(Integer.parseInt(llego))) {

            Table tabla_titulo;
            Cell celda;

            tabla_titulo = new Table(3);
            tabla_titulo.setBorderWidth(0);
            tabla_titulo.setPadding(1);
            tabla_titulo.setSpacing(0);
            tabla_titulo.setWidth(100);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo.addCell(celda);

            String serviciot = "";
            String establecimientot = "";
            String especialidadt = "";
            String unidadt = "";

            serviciot = "Metropolitano Central(SSMC)";
            establecimientot = "Centro de Referencia de Salud de Maipú";
            especialidadt = "Oftalmología";
            unidadt = "Especialidades";

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "SOLICITUD DE PABELLÓN QUIRURGICO", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            Paragraph nombreCita = new Paragraph();
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, "Fecha y hora del Informe: ", TEXT_NORMAL));
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, l.getDescripcion(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(nombreCita);
            celda.setBorderWidth(0);

            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph AtencionGlosa = new Paragraph();
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre Usuario: ", TEXT_NORMAL));
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, l.getNombre(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(AtencionGlosa);
            //celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph AtencionGlosa1 = new Paragraph();
            AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_TITULO, l.getRut(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(AtencionGlosa1);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph edad = new Paragraph();
            edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            edad.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
            edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            edad.add(new Phrase(SPACE_TITULO, l.getEdad() + " años", TEXT_SUPERTITULONORMAL));
            celda = new Cell(edad);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            String diagnos = "";
            for (int i = 0; i < diagnosticos.size(); i++) {
                diagnos = diagnos + diagnosticos.get(i).getDiagnostico() + " ";
            }

            Paragraph sexo = new Paragraph();
            sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            sexo.add(new Phrase(SPACE_TITULO, "Diagnostico: ", TEXT_NORMAL));
            sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            sexo.add(new Phrase(SPACE_TITULO, solicitud.getComuna(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(sexo);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph sederiva = new Paragraph();
            sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            sederiva.add(new Phrase(SPACE_TITULO, "Intervencion: ", TEXT_NORMAL));
            sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            sederiva.add(new Phrase(SPACE_TITULO, solicitud.getIntervencio(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(sederiva);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph seenvia = new Paragraph();
            seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            seenvia.add(new Phrase(SPACE_TITULO, "Anestesia Solicitada: ", TEXT_NORMAL));
            seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            seenvia.add(new Phrase(SPACE_TITULO, solicitud.getDireccion(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(seenvia);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph fd = new Paragraph();
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, "Requerimientos Especiales: ", TEXT_NORMAL));
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, solicitud.getDiagnostico(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(fd);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph examenes = new Paragraph();
            examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            examenes.add(new Phrase(SPACE_TITULO, "Observaciones: ", TEXT_NORMAL));
            examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            examenes.add(new Phrase(SPACE_TITULO, solicitud.getIndicaciones(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(examenes);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph nombredoctor = new Paragraph();
            nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombredoctor.add(new Phrase(SPACE_TITULO, "Cirujano Solicitante: ", TEXT_NORMAL));
            nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombredoctor.add(new Phrase(SPACE_TITULO, l.getRutdoctor() + " " + l.getNombredoctor(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(nombredoctor);
            // celda.setBackgroundColor(new Color(217, 225, 242));

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph firmadoctor = new Paragraph();
            firmadoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            firmadoctor.add(new Phrase(SPACE_TITULO, "_______________________ ", TEXT_NORMAL));
            firmadoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            firmadoctor.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(firmadoctor);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            Paragraph firmadoctor2 = new Paragraph();
            firmadoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            firmadoctor2.add(new Phrase(SPACE_TITULO, "Firma o timbre Cirujano ", TEXT_NORMAL));
            firmadoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            firmadoctor2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
            celda = new Cell(firmadoctor2);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            document.add(tabla_titulo);
            document.newPage();

        }

        document.close();
        try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }
//output.flush();            
//output.close();
        } catch (Exception exstream) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(SolicitudpabellonQuirurgico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(SolicitudpabellonQuirurgico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
