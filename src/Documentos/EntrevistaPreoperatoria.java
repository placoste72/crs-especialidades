/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;

import Controlador.controlador_especialidad;
import Modelos.entrevista_preoperatoria;
import Modelos.lugar;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.pdfconpieyfoto;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;

/**
 *
 * @author a
 */
public class EntrevistaPreoperatoria extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);

        DateFormat fF = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat fH = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);

        // String llego = request.getParameter("idatencion");
          response.setHeader("charset=UTF-8", " attachment; filename=\"Informe.pdf\"");
        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        /*busco la informacion*/
        String llego = request.getParameter("identrevista");
        controlador_especialidad ce = new controlador_especialidad();
        entrevista_preoperatoria ep = ce.buscarEntrevista(Integer.parseInt(llego));

        int SPACE_TITULO = 12;
        int SPACE_NORMAL = 12;
        int SPACE_FECHA = 10;
        int SPACE_SUBTITULO = 4;
        int celda_especial = 3;
        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new Color(0, 0, 1));
        Font TEXT_SUBTITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(0, 0, 2));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_ESPECIAL = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.NORMAL, new Color(0, 0, 3));
        Font TEXT_ESPECIAL_NEGRITA = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_ESPECIAL_PEQUENO = FontFactory.getFont(FontFactory.HELVETICA, 5, Font.NORMAL, new Color(0, 0, 3));
        Font TEXT_NORMAL_NEGRITA = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_ESPECIAL_SUBRAYADO = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.UNDERLINE, new Color(0, 0, 3));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
        writer.setPageEvent(new pdfconpieyfoto());
        document.open();

        PdfPTable table = new PdfPTable(10);
        int headerwidths[] = {20, 20, 20, 20, 20, 20, 20, 20, 20, 20};
        table.setWidths(headerwidths);
        table.setWidthPercentage(100);

        table.getDefaultCell().setPadding(10);
        table.getDefaultCell().setBorderWidth(0);
        table.getDefaultCell().setColspan(10);
        table.addCell("");

        table.getDefaultCell().setPadding(10);
        table.getDefaultCell().setBorderWidth(0);
        table.getDefaultCell().setColspan(10);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new Phrase(SPACE_NORMAL, "Entrevista Preoperatoria de Enfermería", TEXT_TITULO));

        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setBorderWidth(0);
        table.getDefaultCell().setColspan(5);
        
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        
      
        
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes Personales", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(5);
        
          table.getDefaultCell().setColspan(5);
        table.addCell(new Phrase(SPACE_NORMAL, " ", TEXT_SUBTITULO));

        //
        table.addCell(new Phrase(SPACE_NORMAL, "Fecha: " + formateadorFecha.format(ep.getFecharegistro()) + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Nombre:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(4);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getAuxiliar1() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Rut: ", TEXT_NORMAL_NEGRITA));
        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getAuxiliar2() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Edad:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(4);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getAuxiliar3() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Telefono:", TEXT_NORMAL_NEGRITA));
        table.getDefaultCell().setColspan(4);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getAuxiliar4() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Diagnóstico:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(3);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDiagnostico() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Cirugia Propuesta:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(3);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getCirugia_propuesta() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Peso:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getPeso() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Talla:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getTalla() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "IMC", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getImc() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        String pa = "";
        String cardiaca = "";
        String sat = "";
        if (ep.getRetrocontrol() == 1) {
            pa = ep.getParetro();
            cardiaca = ep.getCardiacaretro();
            sat = ep.getSatretro();
        } else {
            pa = ep.getPa();
            cardiaca = ep.getCardiaca();
            sat = ep.getSat();
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "PA:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + pa + " mmHG ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "F. Cardíaca:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + cardiaca + " Lpm", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "SatO2", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + sat + " %", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes Mórbidos: Ventilatorios", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(6);
        table.addCell(new Phrase(SPACE_NORMAL, "   ", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        /*tres en tres*/
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Asma:", TEXT_NORMAL_NEGRITA));
        String variable = "";
        if (ep.getAsma() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Tuberculosis:", TEXT_NORMAL_NEGRITA));
        if (ep.getTuberculosis() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));
        
         table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "IRA:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getIra() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));
        
            table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Rinitis Alérgica:", TEXT_NORMAL_NEGRITA));
        if (ep.getRinitis() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

       

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Tabaco/Cantidad:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getTabaco() + "", TEXT_NORMAL));

    

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Desde:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDesde() + " ", TEXT_NORMAL));

        
         table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Años:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getAnos() + " ", TEXT_NORMAL));

        
        
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "IPA", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getIpa() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Descripción:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDescripcion_ventilatorios() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        /*otros*/
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes Mórbidos: Metabólicos/Generales", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(6);
        table.addCell(new Phrase(SPACE_NORMAL, "   ", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Diabetes M:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getDiabetes() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Obesidad:", TEXT_NORMAL_NEGRITA));
        if (ep.getObesidad() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "RGE:", TEXT_NORMAL_NEGRITA));
        if (ep.getRge() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Hipo/Hipertiroidismo:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getHipo() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Ulcera-gastritis:", TEXT_NORMAL_NEGRITA));
        if (ep.getUlcera() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Alergias:", TEXT_NORMAL_NEGRITA));
        if (ep.getAlergias() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Cuales Alergias:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getCualesalergias() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Descripción:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDescripcion_metabolicos() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes Mórbidos: Cardiovasculares", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(6);
        table.addCell(new Phrase(SPACE_NORMAL, "   ", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Coronario:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getCoronario() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Arritmias:", TEXT_NORMAL_NEGRITA));
        if (ep.getArritmias() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Coagulopatía", TEXT_NORMAL_NEGRITA));
        if (ep.getCoagulopatia() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Vascular:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getValvular() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Marcapasos:", TEXT_NORMAL_NEGRITA));
        if (ep.getMarcapasos() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Dislipidemia", TEXT_NORMAL_NEGRITA));
        if (ep.getDislipidemia() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "HTA", TEXT_NORMAL_NEGRITA));
        if (ep.getHta() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

         table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        
        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Descripción:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDescripcion_cardiovasculares() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes Mórbidos: Neuro-Psiquiátricos", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(6);
        table.addCell(new Phrase(SPACE_NORMAL, "   ", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "ECV:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getEcv() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Depresión:", TEXT_NORMAL_NEGRITA));
        if (ep.getDepresion() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "Alcohol:", TEXT_NORMAL_NEGRITA));
        if (ep.getAlcohol() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(3);
        table.addCell(new Phrase(SPACE_NORMAL, "Especificaciones Alcohol:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(7);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getEspecificacionalcohol() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Esquizofrenia:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getEsquizofrenia() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Epilepsia:", TEXT_NORMAL_NEGRITA));
        if (ep.getEpilepsia() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Otras Enf.Psiq:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getOtrasenfpsiq() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Descripción:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDescripcionneuro() + " ", TEXT_NORMAL));

       

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes Mórbidos: Nefro-Urinarios", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(6);
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));
        table.addCell(new Phrase(SPACE_NORMAL, "   ", TEXT_NORMAL_NEGRITA));
        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Nefropatía:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getNefropatia() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Uropatía Obstructiva:", TEXT_NORMAL_NEGRITA));
        if (ep.getUropatiaobstructiva() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Infección Urinaria:", TEXT_NORMAL_NEGRITA));
        if (ep.getInfeccionurinaria() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Descripción:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDescripcion_urinarios() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Otros Antecedentes Morbidos:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getOtrosantecedentesmorbidos() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes : Quirúrgicos", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(6);
        table.addCell(new Phrase(SPACE_NORMAL, " " + ep.getDescripcion_antecedentes() + "  ", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Intervención:", TEXT_NORMAL_NEGRITA));
        variable = "";
        if (ep.getIntervencion() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Antecedentes Anestésicos:", TEXT_NORMAL_NEGRITA));
        if (ep.getAntecendetes_anestesicos() == 1) {
            variable = "SI";
        } else {
            variable = "NO";
        }
        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Especificar:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getEspecificar() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Descripción:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDescripcion_antecedentes() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        lugar l = ce.buscarexamentesdeEntrevista(Integer.parseInt(llego));

        /*examenes*/
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(new Phrase(SPACE_NORMAL, "Exámenes ", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(6);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "", TEXT_SUBTITULO));

        table.addCell(new Phrase(SPACE_NORMAL, "   ", TEXT_NORMAL_NEGRITA));
        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Hematocrito:", TEXT_NORMAL_NEGRITA));
        variable = "";

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getComuna() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Glicemia:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getDireccion() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Protrombina(%):", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getFechanacimiento() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Recto Blancos:", TEXT_NORMAL_NEGRITA));
        variable = "";

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getDescripcion() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "BUN /Creat:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getEdad() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "TTPK:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getFolio() + "", TEXT_NORMAL));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Plaquetas:", TEXT_NORMAL_NEGRITA));
        variable = "";

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getDetalle_de_la_derivacion() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Na/K+:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getEmail() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "ECG:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getIndicaciones() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Grupo/RH:", TEXT_NORMAL_NEGRITA));
        variable = "";

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getDiagnostico() + " ", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Orina Completa:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getExamenesrealizados() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "HbA1C:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(1);
        table.addCell(new Phrase(SPACE_NORMAL, "" + l.getNombre() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(2);
        table.addCell(new Phrase(SPACE_NORMAL, "Descripción:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(8);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getDescripcion_examenes() + " ", TEXT_NORMAL));

     

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        /*tratamiento y observaciones*/
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "Tratamiento Farmacológico:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getTratamieto().toUpperCase() + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "Red de Apoyo:", TEXT_NORMAL_NEGRITA));
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getReddeapoyo() + "", TEXT_NORMAL));
       
        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));
        table.addCell(new Phrase(SPACE_NORMAL, "Observaciones:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getObservaciones().toUpperCase() + "", TEXT_NORMAL));
      
         table.getDefaultCell().setColspan(3);
        table.addCell(new Phrase(SPACE_NORMAL, "Se da pase de enfermería para cirugía propuesta:", TEXT_NORMAL_NEGRITA));

        if (ep.getPase() == 1) {
            variable = "SI";
        } else if (ep.getPase() == 0) {
            variable = "NO";
        } else {
              variable = " ";   
          }

        table.getDefaultCell().setColspan(7);
        table.addCell(new Phrase(SPACE_NORMAL, "" + variable + "", TEXT_NORMAL));

        table.getDefaultCell().setColspan(10);
        table.addCell(new Phrase(SPACE_NORMAL, "\n", TEXT_SUBTITULO));

        table.getDefaultCell().setColspan(4);
        table.addCell(new Phrase(SPACE_NORMAL, "Nombre y Firma de Enfermera o/ Responsable:", TEXT_NORMAL_NEGRITA));

        table.getDefaultCell().setColspan(7);
        table.addCell(new Phrase(SPACE_NORMAL, "" + ep.getAuxiliar6() + " /" + ep.getAuxiliar5().toUpperCase() + "", TEXT_NORMAL));

        document.add(table);
        document.close();

        PdfReader reader = new PdfReader(buffer.toByteArray());
        DataOutputStream output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
        output.flush();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(EntrevistaPreoperatoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(EntrevistaPreoperatoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
