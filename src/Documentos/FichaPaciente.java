/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;

import Controlador.General;
import Modelos.lugar;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.pdfconpieyfoto;
import com.lowagie.text.Chunk;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.Barcode;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.Barcode39;
import com.lowagie.text.pdf.BarcodeEAN;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;

/**
 *
 * @author a
 */
public class FichaPaciente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
        DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.MEDIUM, currentLocale);

        String rut = "";
        String paciente_nombres = "";
        String paciente_apellidop = "";
        String paciente_apellidom = "";
        String paciente_sexo = "";

        Date paciente_fecha_nac = new Date();
        String paciente_direccion = "";
        String paciente_telefono1 = "";
        String paciente_telefono2 = "";
        String paciente_email = "";
        Date paciente_fecha_creacion = new Date();

        String paciente_prevision = "";
        String paciente_tramo = "Sin Tramo";
        String servicioSaludProcedencia = "";
        String previsionFull = "";
        String edadFull = "";
        String llego = request.getParameter("rut");
        General g = new General();

        lugar l = g.FichaPaciente(llego);

        rut = l.getRut();
        paciente_nombres = l.getNombre();
        paciente_apellidop = l.getVariable1();
        paciente_apellidom = l.getVariable2();
        paciente_sexo = l.getVariable3();

        paciente_fecha_nac = l.getFecha();
        paciente_direccion = l.getDireccion();
        paciente_email = l.getEmail();
        paciente_telefono1 = l.getVariable4();
        paciente_telefono2 = l.getComuna();
        paciente_fecha_creacion=l.getFecha2();
        paciente_prevision = l.getPrescripcion();

        paciente_tramo = l.getTratamiento();
        servicioSaludProcedencia = l.getSederivapara();

        edadFull = l.getEdad();

        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 30, 30, 50, 50);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        int SPACE_TITULO = 12;
        int SPACE_NORMAL = 12;
        int SPACE_ESPACIO = 10;
        int SPACE_RUT = 20;
        int SPACE_RUTVERTICAL = 50;

        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0, 0, 1));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new Color(0, 0, 3));
        Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.UNDERLINE, new Color(0, 0, 3));
        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 15, Font.UNDERLINE | Font.BOLD, new Color(0, 0, 3));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 15, Font.BOLD, new Color(0, 0, 3));
        Font TEXT_SUPERTITULONORMALRUT = FontFactory.getFont(FontFactory.HELVETICA, 30, Font.BOLD, new Color(0, 0, 3));
        Font TEXT_SUPERTITULONORMALRUTVERTICAL = FontFactory.getFont(FontFactory.HELVETICA, 60, Font.BOLD, new Color(0, 0, 3));
        Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.ITALIC, new Color(0, 0, 3));

        writer.setPageEvent(new pdfconpieyfoto());
        document.open();

        PdfContentByte cb = writer.getDirectContent();
        BarcodeEAN codeEAN = new BarcodeEAN();
        codeEAN.setCodeType(Barcode.EAN13);
        Barcode128 code128 = new Barcode128();

        Barcode39 code39ext = new Barcode39();
        code39ext.setCode(rut);
        code39ext.setExtended(true);
        code39ext.setAltText("");
        code39ext.setChecksumText(true);
        Image image39ext = code39ext.createImageWithBarcode(cb, null, null);

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(5);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(2);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        String rutVertical = "";
        for (int i = 0; i < rut.length(); i++) {
            rutVertical = rutVertical + String.valueOf(rut.charAt(i)).toUpperCase() + "\n";
        }

        celda = new Cell(new Phrase(SPACE_RUTVERTICAL, rutVertical, TEXT_SUPERTITULONORMALRUTVERTICAL));
        celda.setBorderWidth(0);
        celda.setRowspan(22);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_RUTVERTICAL, "", TEXT_SUPERTITULONORMALRUTVERTICAL));
        celda.setBorderWidth(0);
        celda.setRowspan(21);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
        tabla_titulo.addCell(celda);

        Paragraph rutPaciente = new Paragraph();
        rutPaciente.add(new Phrase(SPACE_TITULO, "NUMERO FICHA/RUT: ", TEXT_NORMAL));
        rutPaciente.add(new Phrase(SPACE_RUT, rut.toUpperCase(), TEXT_SUPERTITULONORMALRUT));
        celda = new Cell(rutPaciente);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
//celda = new Cell(new Phrase(SPACE_TITULO,"\n",TEXT_TITULO)); 
        celda = new Cell(new Phrase(new Chunk(image39ext, 0, 0)));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "FICHA CLINICA", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "DATOS DEL PACIENTE:", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Apellido Paterno: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, paciente_apellidop.toUpperCase(), TEXT_SUPERTITULONORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Apellido Materno: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, paciente_apellidom.toUpperCase(), TEXT_SUPERTITULONORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Nombres: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, paciente_nombres.toUpperCase(), TEXT_SUPERTITULONORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Sexo: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, paciente_sexo, TEXT_SUPERTITULONORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Fecha de Nacimiento: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, formateadorFechaSimple.format(paciente_fecha_nac), TEXT_SUPERTITULONORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
//celda = new Cell(new Phrase(SPACE_TITULO,String.valueOf(objeto.calcula_edad(paciente_fecha_nac)),TEXT_SUPERTITULONORMAL));

//celda = new Cell(new Phrase(SPACE_TITULO,String.valueOf(edadFull.indexOf(":")),TEXT_SUPERTITULONORMAL));
        celda = new Cell(new Phrase(SPACE_TITULO, edadFull, TEXT_SUPERTITULONORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Domicilio: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, paciente_direccion, TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Telefonos: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, paciente_telefono1 + " | " + paciente_telefono2, TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Previsión: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, paciente_prevision, TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Fecha de Creación: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, formateadorFecha.format(paciente_fecha_creacion), TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "Lugar de Creación: ", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, servicioSaludProcedencia.toUpperCase(), TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(2);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n\n\n\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_NORMAL, "Fecha de Impresion: " + formateadorFecha.format(new Date()).toUpperCase() + " a las " + formateaHora.format(new Date()) + " Hrs.", TEXT_NORMAL));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        document.add(tabla_titulo);

        document.close();
        PdfReader reader = new PdfReader(buffer.toByteArray());
        DataOutputStream output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
        output.flush();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(FichaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(FichaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
