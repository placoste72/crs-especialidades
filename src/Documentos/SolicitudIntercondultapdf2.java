/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;

import Controlador.controlador_doctor;
import Controlador.controlador_especialidad;
import Modelos.lugar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Vector;
import Modelos.pdfpagina;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author a
 */
public class SolicitudIntercondultapdf2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException, SQLException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
        DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

        String llego = request.getParameter("idhojadiaria");
        controlador_especialidad ce = new controlador_especialidad();
        lugar l = ce.buscarDatosparaSICporhojaDiaria(Integer.parseInt(llego));

        Vector<lugar> examenes = ce.buscarExamenesdeUnaHoja(Integer.parseInt(llego));
        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        int SPACE_TITULO = 1;

        int SPACE_NORMAL2 = 17;
        int SPACE_ESPACIO = 4;

        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0));

        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0));

//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
        writer.setPageEvent(new pdfpagina());
        document.open();

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        String serviciot = "";
        String establecimientot = "";
        String especialidadt = "";
        String unidadt = "";

        serviciot = "Metropolitano Central(SSMC)";
        establecimientot = "Centro de Referencia de Salud de Maipú";
        especialidadt = "Odontología";
        unidadt = "Especialidades";

        celda = new Cell(new Phrase(SPACE_TITULO, "SOLICITUD DE INTERCONSULTA O DERIVACIÓN", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /**/
        Paragraph rutCita = new Paragraph();
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, "Folio:", TEXT_NORMAL));
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, l.getFolio(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutCita);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /**/
        Paragraph nombreCita = new Paragraph();
        nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita.add(new Phrase(SPACE_TITULO, "Fecha y hora de la solicitud: ", TEXT_NORMAL));
        nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita.add(new Phrase(SPACE_TITULO, l.getDescripcion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombreCita);
        celda.setBorderWidth(0);

        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos del establecimiento de origen", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph fechaCita = new Paragraph();
        fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechaCita.add(new Phrase(SPACE_TITULO, "Servicio de Salud: ", TEXT_NORMAL));
        fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechaCita.add(new Phrase(SPACE_TITULO, serviciot, TEXT_SUPERTITULONORMAL));
        celda = new Cell(fechaCita);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph horaCita = new Paragraph();
        horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        horaCita.add(new Phrase(SPACE_TITULO, "Establecimiento: ", TEXT_NORMAL));
        horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        horaCita.add(new Phrase(SPACE_TITULO, establecimientot, TEXT_SUPERTITULONORMAL));
        celda = new Cell(horaCita);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph especialidadCita = new Paragraph();
        especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        especialidadCita.add(new Phrase(SPACE_TITULO, "Especialidad: ", TEXT_NORMAL));
        especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        especialidadCita.add(new Phrase(SPACE_TITULO, l.getPrescripcion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(especialidadCita);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph medicoCita = new Paragraph();
        medicoCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        medicoCita.add(new Phrase(SPACE_TITULO, "Unidad:", TEXT_NORMAL));
        medicoCita.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
        medicoCita.add(new Phrase(SPACE_TITULO, unidadt, TEXT_SUPERTITULONORMAL));
        celda = new Cell(medicoCita);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*datos del paciente*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Paciente", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa = new Paragraph();
        AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
        AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa.add(new Phrase(SPACE_TITULO, l.getNombre(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa);
        //celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa1 = new Paragraph();
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, l.getRut(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph fechanacimiento = new Paragraph();
        fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechanacimiento.add(new Phrase(SPACE_TITULO, "Fecha de nacimiento: ", TEXT_NORMAL));
        fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechanacimiento.add(new Phrase(SPACE_TITULO, l.getFechanacimiento(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(fechanacimiento);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph edad = new Paragraph();
        edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
        edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad.add(new Phrase(SPACE_TITULO, l.getEdad() + " años", TEXT_SUPERTITULONORMAL));
        celda = new Cell(edad);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph sexo = new Paragraph();
        sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo.add(new Phrase(SPACE_TITULO, "Sexo: ", TEXT_NORMAL));
        sexo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo.add(new Phrase(SPACE_TITULO, l.getSexo(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(sexo);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph domicilio = new Paragraph();
        domicilio.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        domicilio.add(new Phrase(SPACE_TITULO, "Domicilio: ", TEXT_NORMAL));
        domicilio.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        domicilio.add(new Phrase(SPACE_TITULO, l.getDireccion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(domicilio);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph comuna = new Paragraph();
        comuna.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        comuna.add(new Phrase(SPACE_TITULO, "Comuna: ", TEXT_NORMAL));
        comuna.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        comuna.add(new Phrase(SPACE_TITULO, l.getComuna(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(comuna);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph telefonos = new Paragraph();
        telefonos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        telefonos.add(new Phrase(SPACE_TITULO, "Telefonos: ", TEXT_NORMAL));
        telefonos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        telefonos.add(new Phrase(SPACE_TITULO, l.getTelefono1(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(telefonos);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph correo = new Paragraph();
        correo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        correo.add(new Phrase(SPACE_TITULO, "Correo electronico: ", TEXT_NORMAL));
        correo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        correo.add(new Phrase(SPACE_TITULO, l.getEmail(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(correo);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*Datos de derivacion*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos de la derivación", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph sederiva = new Paragraph();
        sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva.add(new Phrase(SPACE_TITULO, "Se deriva para atención en: ", TEXT_NORMAL));
        sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva.add(new Phrase(SPACE_TITULO, l.getDetalle_de_la_derivacion() + " " + l.getIntervencio(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(sederiva);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph seenvia = new Paragraph();
        seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia.add(new Phrase(SPACE_TITULO, "Se envía paciente para : ", TEXT_NORMAL));
        seenvia.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia.add(new Phrase(SPACE_TITULO, l.getSeeviapara(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(seenvia);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph diagnostico = new Paragraph();
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, "Diagnósticos del paciente: ", TEXT_NORMAL));
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, l.getDiagnostico(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(diagnostico);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        String examenesrealizadost = "";
        for (int j = 0; j < examenes.size(); ++j) {
            examenesrealizadost = examenesrealizadost + "; " + examenes.get(j).getDiagnostico();
        }

        Paragraph examenes1 = new Paragraph();
        examenes1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        examenes1.add(new Phrase(SPACE_TITULO, "Exámenes realizados: ", TEXT_NORMAL));
        examenes1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        examenes1.add(new Phrase(SPACE_TITULO, examenesrealizadost + " " + l.getExamenesrealizados(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(examenes1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Profesional Tratante", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph nombredoctor = new Paragraph();
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, l.getNombredoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombredoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph rutdoctor = new Paragraph();
        rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor.add(new Phrase(SPACE_TITULO, l.getRutdoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutdoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);


        /*para la firma del doctor*/
 /*para la firma del doctor*/
        controlador_doctor cdoctor = new controlador_doctor();

        byte[] imag = cdoctor.obtenImagenDoctor(l.getRutdoctor());
        if (imag != null) {
            Image image22 = Image.getInstance(imag);

            image22.setAbsolutePosition(15f, 15f);
            document.add(image22);
        }

        document.add(tabla_titulo);
        document.newPage();
        /*segunda pagina*/

        Table tabla_titulo2;

        tabla_titulo2 = new Table(3);
        tabla_titulo2.setBorderWidth(0);
        tabla_titulo2.setPadding(1);
        tabla_titulo2.setSpacing(0);
        tabla_titulo2.setWidth(100);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "SOLICITUD DE INTERCONSULTA O DERIVACIÓN", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        /**/
        Paragraph rutCita2 = new Paragraph();
        rutCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita2.add(new Phrase(SPACE_TITULO, "Folio:", TEXT_NORMAL));
        rutCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita2.add(new Phrase(SPACE_TITULO, l.getFolio(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutCita2);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        /**/
        Paragraph nombreCita2 = new Paragraph();
        nombreCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita2.add(new Phrase(SPACE_TITULO, "Fecha y hora de la solicitud: ", TEXT_NORMAL));
        nombreCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombreCita2.add(new Phrase(SPACE_TITULO, l.getDescripcion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombreCita2);
        celda.setBorderWidth(0);

        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos del establecimiento de origen", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph fechaCita2 = new Paragraph();
        fechaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechaCita2.add(new Phrase(SPACE_TITULO, "Servicio de Salud: ", TEXT_NORMAL));
        fechaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechaCita2.add(new Phrase(SPACE_TITULO, serviciot, TEXT_SUPERTITULONORMAL));
        celda = new Cell(fechaCita2);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph horaCita2 = new Paragraph();
        horaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        horaCita2.add(new Phrase(SPACE_TITULO, "Establecimiento: ", TEXT_NORMAL));
        horaCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        horaCita2.add(new Phrase(SPACE_TITULO, establecimientot, TEXT_SUPERTITULONORMAL));
        celda = new Cell(horaCita2);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph especialidadCita2 = new Paragraph();
        especialidadCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        especialidadCita2.add(new Phrase(SPACE_TITULO, "Especialidad", TEXT_NORMAL));
        especialidadCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        especialidadCita2.add(new Phrase(SPACE_TITULO, l.getPrescripcion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(especialidadCita2);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph medicoCita2 = new Paragraph();
        medicoCita2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        medicoCita2.add(new Phrase(SPACE_TITULO, "Unidad:", TEXT_NORMAL));
        medicoCita2.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
        medicoCita2.add(new Phrase(SPACE_TITULO, unidadt, TEXT_SUPERTITULONORMAL));
        celda = new Cell(medicoCita2);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        /*datos del paciente*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Paciente", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph AtencionGlosa2 = new Paragraph();
        AtencionGlosa2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa2.add(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
        AtencionGlosa2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa2.add(new Phrase(SPACE_TITULO, l.getNombre(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa2);
        //celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph AtencionGlosa12 = new Paragraph();
        AtencionGlosa12.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa12.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        AtencionGlosa12.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa12.add(new Phrase(SPACE_TITULO, l.getRut(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa12);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph fechanacimiento2 = new Paragraph();
        fechanacimiento2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechanacimiento2.add(new Phrase(SPACE_TITULO, "Fecha de nacimiento: ", TEXT_NORMAL));
        fechanacimiento2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        fechanacimiento2.add(new Phrase(SPACE_TITULO, l.getFechanacimiento(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(fechanacimiento2);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph edad2 = new Paragraph();
        edad2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad2.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
        edad2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        edad2.add(new Phrase(SPACE_TITULO, l.getEdad() + " años", TEXT_SUPERTITULONORMAL));
        celda = new Cell(edad2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph sexo2 = new Paragraph();
        sexo2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo2.add(new Phrase(SPACE_TITULO, "Sexo: ", TEXT_NORMAL));
        sexo2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sexo2.add(new Phrase(SPACE_TITULO, l.getSexo(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(sexo2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph domicilio2 = new Paragraph();
        domicilio2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        domicilio2.add(new Phrase(SPACE_TITULO, "Domicilio: ", TEXT_NORMAL));
        domicilio2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        domicilio2.add(new Phrase(SPACE_TITULO, l.getDireccion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(domicilio2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph comuna2 = new Paragraph();
        comuna2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        comuna2.add(new Phrase(SPACE_TITULO, "Comuna: ", TEXT_NORMAL));
        comuna2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        comuna2.add(new Phrase(SPACE_TITULO, l.getComuna(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(comuna2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph telefonos2 = new Paragraph();
        telefonos2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        telefonos2.add(new Phrase(SPACE_TITULO, "Telefonos: ", TEXT_NORMAL));
        telefonos2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        telefonos2.add(new Phrase(SPACE_TITULO, l.getTelefono1(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(telefonos2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph correo2 = new Paragraph();
        correo2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        correo2.add(new Phrase(SPACE_TITULO, "Correo electronico: ", TEXT_NORMAL));
        correo2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        correo2.add(new Phrase(SPACE_TITULO, l.getEmail(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(correo2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        /*Datos de derivacion*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Datos de la derivación", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph sederiva2 = new Paragraph();
        sederiva2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva2.add(new Phrase(SPACE_TITULO, "Se deriva para atención en: ", TEXT_NORMAL));
        sederiva2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        sederiva2.add(new Phrase(SPACE_TITULO, l.getDetalle_de_la_derivacion() + " " + l.getIntervencio(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(sederiva2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph seenvia2 = new Paragraph();
        seenvia2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia2.add(new Phrase(SPACE_TITULO, "Se envía paciente para : ", TEXT_NORMAL));
        seenvia2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        seenvia2.add(new Phrase(SPACE_TITULO, l.getSeeviapara(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(seenvia2);
        //  celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph diagnostico2 = new Paragraph();
        diagnostico2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico2.add(new Phrase(SPACE_TITULO, "Diagnósticos del paciente: ", TEXT_NORMAL));
        diagnostico2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico2.add(new Phrase(SPACE_TITULO, l.getDiagnostico(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(diagnostico2);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph examenes2 = new Paragraph();
        examenes2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        examenes2.add(new Phrase(SPACE_TITULO, "Exámenes realizados: ", TEXT_NORMAL));
        examenes2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        examenes2.add(new Phrase(SPACE_TITULO, examenesrealizadost + " " + l.getExamenesrealizados(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(examenes2);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "Profesional Tratante", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph nombredoctor2 = new Paragraph();
        nombredoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor2.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
        nombredoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor2.add(new Phrase(SPACE_TITULO, l.getNombredoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombredoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        Paragraph rutdoctor2 = new Paragraph();
        rutdoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor2.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        rutdoctor2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor2.add(new Phrase(SPACE_TITULO, l.getRutdoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutdoctor2);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo2.addCell(celda);

        document.add(tabla_titulo2);
        byte[] imag2 = cdoctor.obtenImagenDoctor(l.getRutdoctor());
        if (imag2 != null) {
            Image image223 = Image.getInstance(imag2);

            image223.setAbsolutePosition(15f, 15f);
            document.add(image223);
        }

        document.close();
        try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }
//output.flush();            
//output.close();
        } catch (Exception exstream) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(SolicitudIntercondultapdf2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SolicitudIntercondultapdf2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(SolicitudIntercondultapdf2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SolicitudIntercondultapdf2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
