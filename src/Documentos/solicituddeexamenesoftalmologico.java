/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;

import Controlador.controlador_atencion;
import Controlador.controlador_atencion_clinica_oftalmologia;
import Modelos.atencion;
import Modelos.pdfconpieyfoto;
import Modelos.solicitudExamenes_Procedimientos;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class solicituddeexamenesoftalmologico extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        String atencion = request.getParameter("idatencion");
        PdfWriter writer = PdfWriter.getInstance(document, buffer);
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        solicitudExamenes_Procedimientos sep = caco.buscarexamenesProcedimientos(Integer.parseInt(atencion));
        int SPACE_TITULO = 1;
        int SPACE_NORMAL = 12;
        int SPACE_NORMAL2 = 17;
        int SPACE_ESPACIO = 4;
        Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
        Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD, new Color(68, 114, 196));
        Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
        writer.setPageEvent(new pdfconpieyfoto());
        document.open();

        for (int s = 1; s <= 2; ++s) {

            Table tabla_titulo;
            Cell celda;

            tabla_titulo = new Table(3);
            tabla_titulo.setBorderWidth(0);
            tabla_titulo.setPadding(1);
            tabla_titulo.setSpacing(0);
            tabla_titulo.setWidth(100);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "SOLICITUD DE EXAMENES O PROCEDIMIENTOS OFTALMOLOGICOS", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            Paragraph AtencionGlosa = new Paragraph();
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, sep.getNombrepaciente(), TEXT_NORMAL2));
            celda = new Cell(AtencionGlosa);
            //celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph AtencionGlosa1 = new Paragraph();
            AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_TITULO, sep.getRutpaciente(), TEXT_NORMAL2));
            celda = new Cell(AtencionGlosa1);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph EDAD = new Paragraph();
            EDAD.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            EDAD.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
            EDAD.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            EDAD.add(new Phrase(SPACE_TITULO, sep.getVariable1() + " Años", TEXT_SUPERTITULONORMAL));
            celda = new Cell(EDAD);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph nombreCita = new Paragraph();
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, "Fecha: ", TEXT_NORMAL));
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, sep.getFecha(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(nombreCita);
            celda.setBorderWidth(0);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            controlador_atencion ca = new controlador_atencion();
            Vector<atencion> diagnosticos = ca.BuscarDiagnosticosdeAtencionOftalmologica(Integer.parseInt(atencion));
            String d = "";
            if (diagnosticos.size() > 0) {
                for (int i = 0; i < diagnosticos.size(); ++i) {
                    d = d + diagnosticos.get(i).getConf() + "  " + diagnosticos.get(i).getOtrodiagnostico() + " " + diagnosticos.get(i).getLateralidads() + " " + diagnosticos.get(i).getCasoges() + " ; ";
                }
            }
            Paragraph diagnostico = new Paragraph();
            diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnostico.add(new Phrase(SPACE_TITULO, "Diagnostico: ", TEXT_NORMAL));
            diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnostico.add(new Phrase(SPACE_TITULO, d, TEXT_SUPERTITULONORMAL));
            celda = new Cell(diagnostico);
            celda.setBorderWidth(0);

            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            /*buscar los examenes y los procedimientos */
            Vector<solicitudExamenes_Procedimientos> see = caco.solicitudesdetalle(sep.getIdsolicitud(), 1);

            if (see.size() > 0) {
                Paragraph fd = new Paragraph();
                fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                fd.add(new Phrase(SPACE_TITULO, "Examenes: ", TEXT_SUPERTITULO));
                fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                fd.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
                celda = new Cell(fd);
                // celda.setBackgroundColor(new Color(217, 225, 242));
                celda.setBorderWidth(0);
                celda.setColspan(3);
                celda.setHorizontalAlignment(Element.ALIGN_LEFT);
                tabla_titulo.addCell(celda);

                for (int k = 0; k < see.size(); ++k) {
                    int e = k + 1;
                    Paragraph examenes = new Paragraph();
                    examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                    examenes.add(new Phrase(SPACE_TITULO, "" + e + "-" + see.get(k).getVariable1() + " " + see.get(k).getVariable2() + " " + see.get(k).getVariable3() + ".", TEXT_NORMAL));
                    examenes.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                    examenes.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
                    celda = new Cell(examenes);
                    // celda.setBackgroundColor(new Color(217, 225, 242));
                    celda.setBorderWidth(0);
                    celda.setColspan(3);
                    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
                    tabla_titulo.addCell(celda);
                    ++e;
                }
            }

            /*buscar los examenes y los procedimientos */
            Vector<solicitudExamenes_Procedimientos> sepp = caco.solicitudesdetalle(sep.getIdsolicitud(), 2);

            if (sepp.size() > 0) {

                Paragraph fd2 = new Paragraph();
                fd2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                fd2.add(new Phrase(SPACE_TITULO, "Procedimiento: ", TEXT_SUPERTITULO));
                fd2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                fd2.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
                celda = new Cell(fd2);
                // celda.setBackgroundColor(new Color(217, 225, 242));
                celda.setBorderWidth(0);
                celda.setColspan(3);
                celda.setHorizontalAlignment(Element.ALIGN_LEFT);
                tabla_titulo.addCell(celda);

                for (int i = 0; i < sepp.size(); ++i) {
                    int e = i + 1;
                    Paragraph procedimiento = new Paragraph();
                    procedimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                    procedimiento.add(new Phrase(SPACE_TITULO, "" + e + "-" + sepp.get(i).getVariable1() + "  " + sepp.get(i).getVariable2() + " " + sepp.get(i).getVariable3() + "", TEXT_NORMAL));
                    procedimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
                    procedimiento.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
                    celda = new Cell(procedimiento);
                    // celda.setBackgroundColor(new Color(217, 225, 242));
                    celda.setBorderWidth(0);
                    celda.setColspan(3);
                    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
                    tabla_titulo.addCell(celda);
                    ++e;
                }
            }

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph fd = new Paragraph();
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, "OBSERVACIONES: ", TEXT_SUPERTITULO));
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, sep.getObservacion(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(fd);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Medico Solicitante", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph nombredoctor = new Paragraph();
            nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
            nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombredoctor.add(new Phrase(SPACE_TITULO, sep.getNombreprofesional(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(nombredoctor);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph rutdoctor = new Paragraph();
            rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutdoctor.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
            rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutdoctor.add(new Phrase(SPACE_TITULO, sep.getRutprofesional(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(rutdoctor);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            document.add(tabla_titulo);
            document.newPage();

        }

        document.close();

        try {

            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }

        } catch (Exception exstream) {
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(solicituddeexamenesoftalmologico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(solicituddeexamenesoftalmologico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
