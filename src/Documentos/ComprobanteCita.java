/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos;

import Controlador.controlador_cita;
import Modelos.cita;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import Modelos.pdfconpieyfoto;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author a
 */
public class ComprobanteCita extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
    Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
    DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);

    String rut = "";
    String paciente_nombres = "";

    Date fecha = new Date();
    String hora = "";
    String especialidad = "";
    String doctor = "";
    String tipoatencion = "";

    String llego = request.getParameter("idcita");
    controlador_cita cc = new controlador_cita();
    cita c = cc.buscarComprobanteCita(llego);

  
        rut = c.getRut_paciente();
        paciente_nombres = c.getTemporales();

        fecha = c.getFecha();
        hora = c.getTemporales2();
        especialidad = c.getTemporales3();
        doctor = c.getTemporales4();
        tipoatencion = c.getTemporales5();

   
    response.setContentType("application/pdf");
    Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(document, buffer);

    int SPACE_TITULO = 4;
    int SPACE_NORMAL = 12;
    int SPACE_NORMAL2 = 17;
    int SPACE_ESPACIO = 10;
    Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(68, 117, 196));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
    Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 22, Font.BOLD, new Color(32, 55, 100));
    Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.NORMAL, new Color(68, 117, 196));
    Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
    Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD, new Color(68, 114, 196));
    Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
    writer.setPageEvent(new pdfconpieyfoto());
    document.open();

    Table tabla_titulo;
    Cell celda;

    tabla_titulo = new Table(3);
    tabla_titulo.setBorderWidth(0);
    tabla_titulo.setPadding(1);
    tabla_titulo.setSpacing(0);
    tabla_titulo.setWidth(100);

    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    String dondeAtencion = "";
    String dondedireccion = "";
    String transantiago = "";
    String dondetelefono = "";

    dondeAtencion = "CENTRO DE REFERENCIA DE SALUD MAIPU (CRS Maipu)";
    dondedireccion = "Camino Rinconada # 1001";
    transantiago = "108-109-109n-118-348-350-I03-I05-I09-I09c-I18-I24";
    dondetelefono = "574 6450";

    Paragraph Fecha = new Paragraph();
    Fecha.add(new Phrase(SPACE_TITULO, "FECHA:" + formateadorFecha.format(new Date()).toUpperCase(), fecha1));

    celda = new Cell(Fecha);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "____________________", Linea));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "COMPROBANTE DE CITA", TEXT_SUPERTITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "__________________________________________________", Linea));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_NORMAL, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);


    
     
    celda = new Cell(new Phrase(SPACE_NORMAL2, "DATOS DEL PACIENTE:", TEXT));
    celda.setBackgroundColor(new Color(68, 114, 196));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph rutCita = new Paragraph();
     rutCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, "          RUT: ", TEXT_NORMAL));
     rutCita.add(new Phrase(SPACE_NORMAL2, "                    ", TEXT_SUPERTITULONORMAL));
    rutCita.add(new Phrase(SPACE_TITULO, rut, TEXT_SUPERTITULONORMAL));
    celda = new Cell(rutCita);
    celda.setBorderWidth(0);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    /**/
    Paragraph nombreCita = new Paragraph();
    nombreCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, "          NOMBRE: ", TEXT_NORMAL));
     nombreCita.add(new Phrase(SPACE_NORMAL2, "             ", TEXT_SUPERTITULONORMAL));
    nombreCita.add(new Phrase(SPACE_TITULO, paciente_nombres, TEXT_SUPERTITULONORMAL));
    celda = new Cell(nombreCita);
    celda.setBorderWidth(0);
   
   
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_NORMAL2, "DATOS DE LA CITA:", TEXT));
     celda.setBackgroundColor(new Color(68, 114, 196));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph fechaCita = new Paragraph();
     fechaCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, "          FECHA: ", TEXT_NORMAL));
    fechaCita.add(new Phrase(SPACE_NORMAL2, "                 ", TEXT_SUPERTITULONORMAL));
    fechaCita.add(new Phrase(SPACE_TITULO, formateadorFecha.format(fecha).toUpperCase(), TEXT_SUPERTITULONORMAL));
    celda = new Cell(fechaCita);
    celda.setBorderWidth(0);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph horaCita = new Paragraph();
     horaCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, "           HORA: ", TEXT_NORMAL));
     horaCita.add(new Phrase(SPACE_NORMAL2, "                 ", TEXT_SUPERTITULONORMAL));
    horaCita.add(new Phrase(SPACE_TITULO, hora, TEXT_SUPERTITULONORMAL));
    celda = new Cell(horaCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph especialidadCita = new Paragraph();
     especialidadCita.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, "          ESPECIALIDAD: ", TEXT_NORMAL));
     especialidadCita.add(new Phrase(SPACE_NORMAL2, "        ", TEXT_SUPERTITULONORMAL));
    especialidadCita.add(new Phrase(SPACE_TITULO, especialidad, TEXT_SUPERTITULONORMAL));
    celda = new Cell(especialidadCita);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph medicoCita = new Paragraph();
     medicoCita.add(new Phrase(SPACE_NORMAL2, "         ", TEXT_SUPERTITULONORMAL));
    medicoCita.add(new Phrase(SPACE_TITULO, "        MEDICO: ", TEXT_NORMAL));
    medicoCita.add(new Phrase(SPACE_NORMAL2, "                ", TEXT_SUPERTITULONORMAL));
    medicoCita.add(new Phrase(SPACE_TITULO, doctor, TEXT_SUPERTITULONORMAL));
    celda = new Cell(medicoCita);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    Paragraph AtencionGlosa = new Paragraph();
      AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, "           TIPO ATENCION: ", TEXT_NORMAL));
      AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "       ", TEXT_SUPERTITULONORMAL));
    AtencionGlosa.add(new Phrase(SPACE_TITULO, tipoatencion, TEXT_SUPERTITULONORMAL));
    celda = new Cell(AtencionGlosa);
     celda.setBackgroundColor(new Color(217, 225, 242));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_NORMAL2, "INDICACIONES:", TEXT));
    celda.setBackgroundColor(new Color(68, 114, 196));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);

    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    
    celda = new Cell(new Phrase(SPACE_NORMAL2, "                    * DEBE PRESENTARSE CON SU CEDULA DE IDENTIDAD Y DOCUMENTACION DE PREVISION AL DIA.", TEXT_CURSI));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_BOTTOM);
    tabla_titulo.addCell(celda);
    celda = new Cell(new Phrase(SPACE_NORMAL2, "                    * DEBE LLEGAR 30 MINUTOS ANTES DE LA HORA DE ATENCIÓN.", TEXT_CURSI));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_BOTTOM);
    tabla_titulo.addCell(celda);


    celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
    tabla_titulo.addCell(celda);


    Paragraph lugarAtencion = new Paragraph();
    lugarAtencion.add(new Phrase(SPACE_TITULO, "LUGAR DE ATENCION: ", TEXT_NORMAL));
    lugarAtencion.add(new Phrase(SPACE_TITULO, dondeAtencion, TEXT_NORMAL));
    celda = new Cell(lugarAtencion);
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    Paragraph direccion = new Paragraph();
    direccion.add(new Phrase(SPACE_TITULO, "Direccion: ", TEXT_NORMAL));
    direccion.add(new Phrase(SPACE_TITULO, dondedireccion, TEXT_NORMAL2));
    celda = new Cell(direccion);
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    Paragraph comoLlegar = new Paragraph();
    comoLlegar.add(new Phrase(SPACE_TITULO, "Transantiago: ",TEXT_NORMAL));
    comoLlegar.add(new Phrase(SPACE_TITULO, transantiago, TEXT_NORMAL2));
    celda = new Cell(comoLlegar);
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
    Paragraph telefono = new Paragraph();
    telefono.add(new Phrase(SPACE_TITULO, "Telefono: ", TEXT_NORMAL));
    telefono.add(new Phrase(SPACE_TITULO, dondetelefono, TEXT_NORMAL2));
    celda = new Cell(telefono);
    celda.setBorderWidth(0);
    celda.setColspan(1);
    celda.setHorizontalAlignment(Element.ALIGN_LEFT);
    tabla_titulo.addCell(celda);
 
    celda = new Cell(new Phrase(SPACE_TITULO, "__________________________________________________", Linea));
    celda.setBorderWidth(0);
    celda.setColspan(3);
    celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
    tabla_titulo.addCell(celda);
    
    document.add(tabla_titulo);

    document.close();
    try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
        DataOutput output = new DataOutputStream(response.getOutputStream());
        byte[] bytes = buffer.toByteArray();
        response.setContentLength(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            output.writeByte(bytes[i]);
        }
//output.flush();            
//output.close();
    } catch (Exception exstream) {
    }

   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(ComprobanteCita.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(ComprobanteCita.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
