/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estadisticos;

import Controlador.controlador_atencion_clinica_oftalmologia;

import Controlador.controlador_doctor;
import Modelos.atencion_clinica_tecnologo;


import Modelos.excepcion_garantia;



import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import java.text.DateFormat;

import java.util.Locale;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.pdfconpieyfoto;
import Modelos.receta_lente_opticos;
import Modelos.vicio_refraccion;
import com.lowagie.text.Image;
import java.sql.SQLException;

/**
 *
 * @author a
 */
public class InformedePrestacionRealizadaOftalmologia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException, SQLException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
        DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);
        String llego = request.getParameter("idatencion");
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        atencion_clinica_tecnologo act = caco.buscarencabezadoparaInformedeAtencion(Integer.parseInt(llego));
        vicio_refraccion vr = new vicio_refraccion();

        vr = caco.buscarVicioRefraccionporIdAtencion(Integer.parseInt(llego));

        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        int SPACE_TITULO = 1;
        int SPACE_NORMAL = 12;
        int SPACE_NORMAL2 = 17;
        int SPACE_ESPACIO = 4;
        Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.UNDEFINED, new Color(0, 0, 0));
        Font TEXT_NORMAL3 = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
        Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new Color(68, 114, 196));
        Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
        writer.setPageEvent(new pdfconpieyfoto());
        document.open();

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        String serviciot = "";
        String establecimientot = "";
        String especialidadt = "";
        String unidadt = "";

        serviciot = " Metropolitano Central(SSMC)";
        establecimientot = " Centro de Referencia de Salud de Maipú";
        especialidadt = " Odontología";
        unidadt = " Oftalmologia";

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "INFORME DE PRESTACIONES REALIZADAS - SERVICIO DE OFTALMOLOGIA", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /**/
        Paragraph rutCita = new Paragraph();
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, "Fecha :", TEXT_NORMAL));
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, act.getAuxiliaruno(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutCita);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "    DATOS DEL PACIENTE", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "___________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa1 = new Paragraph();
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, act.getAuxiliarcinco() + "            NOMBRE Y APELLIDOS:   " + act.getAuxiliartres() + "            EDAD:  " + act.getAuxiliarsiete() + " años", TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*Datos de la atencion*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        /*coloco la informacion de vicio de refraccion*/
        celda = new Cell(new Phrase(SPACE_TITULO, "    RESUMEN PRESTACIÓN REALIZADA", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "___________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph diagnostico = new Paragraph();
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, "MOTIVO DE LA CONSULTA: ", TEXT_NORMAL2));
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, vr.getMotivoconsulta(), TEXT_NORMAL3));
        celda = new Cell(diagnostico);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph ges2 = new Paragraph();
        ges2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        ges2.add(new Phrase(SPACE_TITULO, "AM:  HTA (" + vr.getAmhta() + ");  DM (" + vr.getAmdm() + ")  " + vr.getAnosam() + "  años;  Glaucoma(" + vr.getAmglaucoma() + ");", TEXT_NORMAL2));
        ges2.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

        celda = new Cell(ges2);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph ges3 = new Paragraph();
        ges3.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));

        ges3.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        ges3.add(new Phrase(SPACE_TITULO, " OTROS :" + vr.getAmotro(), TEXT_NORMAL2));
        celda = new Cell(ges3);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph inte = new Paragraph();
        inte.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        inte.add(new Phrase(SPACE_TITULO, "PIO(Aire/Aplanática):  OD:  " + vr.getPiod() + " mmHg ", TEXT_NORMAL2));
        inte.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        inte.add(new Phrase(SPACE_TITULO, " ; OI:" + vr.getPioi() + " mmHg ", TEXT_NORMAL2));
        celda = new Cell(inte);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph te = new Paragraph();
        te.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        te.add(new Phrase(SPACE_TITULO, "AV PL SC OD: " + vr.getPlscod() + "      AV PL CC OD:" + vr.getPlccod() + "     CC ODI:" + vr.getPlccadi(), TEXT_NORMAL2));
        te.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        // te.add(new Phrase(SPACE_TITULO, "AV PL SC OI:" + vr.getPlscoi() + "        AV PL CC OI:" + vr.getPlccoi(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(te);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph te1 = new Paragraph();
        te1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        //te.add(new Phrase(SPACE_TITULO, "AV PL SC OD: " + vr.getPlscod() + "      AV PL CC OD:" + vr.getPlccod() + "     CC ODI:" + vr.getPlccadi(), TEXT_NORMAL));
        te1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        te1.add(new Phrase(SPACE_TITULO, "AV PL SC OI:" + vr.getPlscoi() + "        AV PL CC OI:" + vr.getPlccoi(), TEXT_NORMAL2));
        celda = new Cell(te1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        /*receta de lentes si esta??*/
        int buscar = caco.buscarRecetadeLentes(Integer.parseInt(llego),0);

        if (buscar != 0) {
            receta_lente_opticos rlo = new receta_lente_opticos();
            rlo = caco.buscarRecetadeIdAtencion(Integer.parseInt(llego),0);

            Paragraph rc = new Paragraph();
            rc.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rc.add(new Phrase(SPACE_TITULO, "     RECETA DE LENTES : ", TEXT_SUPERTITULO));
            rc.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            // rc.add(new Phrase(SPACE_TITULO, vr.getMotivoconsulta(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(rc);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_TITULO, "____________________________________________", Linea));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            // celda.setBackgroundColor(new Color(217, 225, 242));

            Paragraph octg = new Paragraph();
            octg.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            octg.add(new Phrase(SPACE_TITULO, "OD ESFERA : " + rlo.getOjoderechoesfera() + "  OD CILINDRO:   " + rlo.getOjoderechocilindro() + "  OD EJE :   " + rlo.getOjoderechoeje(), TEXT_NORMAL2));
            octg.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            // octg.add(new Phrase(SPACE_TITULO, tex, TEXT_SUPERTITULONORMAL));
            celda = new Cell(octg);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph afga = new Paragraph();
            afga.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            afga.add(new Phrase(SPACE_TITULO, "OI ESFERA:  " + rlo.getOjoizquierdoesfera() + "   OI CILINDRO :  " + rlo.getOjoiaquierdacilindro() + "  OI EJE :  " + rlo.getOjoizquierdaeje(), TEXT_NORMAL2));
            afga.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            // afga.add(new Phrase(SPACE_TITULO, tex, TEXT_SUPERTITULONORMAL));
            celda = new Cell(afga);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph add = new Paragraph();
            add.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            add.add(new Phrase(SPACE_TITULO, "ADD :   " + rlo.getAadd(), TEXT_NORMAL2));
            add.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            // add.add(new Phrase(SPACE_TITULO, tex, TEXT_SUPERTITULONORMAL));
            celda = new Cell(add);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph dp = new Paragraph();
            dp.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            dp.add(new Phrase(SPACE_TITULO, "DP :    " + rlo.getDistanciapupilarlejos() + "/" + rlo.getDistanciapupilarcerca(), TEXT_NORMAL2));
            dp.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            // dp.add(new Phrase(SPACE_TITULO, tex, TEXT_SUPERTITULONORMAL));
            celda = new Cell(dp);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

        }
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        Paragraph afga = new Paragraph();
        afga.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        afga.add(new Phrase(SPACE_TITULO, "RFM : ", TEXT_NORMAL2));
        afga.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        afga.add(new Phrase(SPACE_TITULO, vr.getReflejofotomotor(), TEXT_NORMAL2));
        celda = new Cell(afga);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph EA = new Paragraph();
        EA.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        EA.add(new Phrase(SPACE_TITULO, "ROJO PUPILAR: ", TEXT_NORMAL2));
        EA.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        EA.add(new Phrase(SPACE_TITULO, vr.getRojopopular(), TEXT_NORMAL2));
        celda = new Cell(EA);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph EB = new Paragraph();
        EB.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        EB.add(new Phrase(SPACE_TITULO, "BMC + POLO POSTERIOR: ", TEXT_NORMAL2));
        EB.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        EB.add(new Phrase(SPACE_TITULO, vr.getBmcpoloposterior(), TEXT_NORMAL2));
        celda = new Cell(EB);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph RTG = new Paragraph();
        RTG.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        RTG.add(new Phrase(SPACE_TITULO, "INDICACIONES : ", TEXT_NORMAL2));
        RTG.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        RTG.add(new Phrase(SPACE_TITULO, vr.getIndicaciones(), TEXT_NORMAL2));
        celda = new Cell(RTG);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph profesional = new Paragraph();
        profesional.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesional.add(new Phrase(SPACE_TITULO, "    PROFESIONAL: ", TEXT_SUPERTITULO));
        profesional.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        //profesional.add(new Phrase(SPACE_TITULO, vr.getMotivoconsulta(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(profesional);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "___________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph nombredoctor = new Paragraph();
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, "NOMBRE PROFESIONAL: ", TEXT_NORMAL));
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, act.getAuxiliarcatorce(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombredoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph rutdoctor = new Paragraph();
        rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutdoctor.add(new Phrase(SPACE_TITULO, act.getAuxiliarquince(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutdoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph profesiondoctor = new Paragraph();
        profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesiondoctor.add(new Phrase(SPACE_TITULO, "PROFESIÓN: ", TEXT_NORMAL));
        profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesiondoctor.add(new Phrase(SPACE_TITULO, act.getTecnologo(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(profesiondoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        document.add(tabla_titulo);
        /*para la firma del doctor*/
 /*para la firma del doctor*/
        controlador_doctor cdoctor = new controlador_doctor();

        byte[] imag = cdoctor.obtenImagenDoctor(act.getAuxiliarquince());
        if (imag != null) {
            Image image22 = Image.getInstance(imag);

            image22.setAbsolutePosition(15f, 15f);
            document.add(image22);
        }

        document.close();
        try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }
//output.flush();            
//output.close();
        } catch (Exception exstream) {
        }

        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(InformedePrestacionRealizadaOftalmologia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InformedePrestacionRealizadaOftalmologia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(InformedePrestacionRealizadaOftalmologia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InformedePrestacionRealizadaOftalmologia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
