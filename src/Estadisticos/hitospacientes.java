/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estadisticos;

import Controlador.controlador_cita;
import Modelos.lugar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

/**
 *
 * @author a
 */
public class hitospacientes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fechainicio = request.getParameter("inicio");
    String fechafin = request.getParameter("fin");
    Date fecha1 = new Date(Integer.parseInt(fechainicio.substring(6, 10)) - 1900, Integer.parseInt(fechainicio.substring(3, 5)) - 1, Integer.parseInt(fechainicio.substring(0, 2)), 0, 0, 0);
    Date fecha2 = new Date(Integer.parseInt(fechafin.substring(6, 10)) - 1900, Integer.parseInt(fechafin.substring(3, 5)) - 1, Integer.parseInt(fechafin.substring(0, 2)), 0, 0, 0);
    Locale currentLocale = new Locale("es", "CL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateadorFecha2 = DateFormat.getDateInstance(DateFormat.SHORT, currentLocale);
    Locale currentLocale2 = new Locale("es", "Ch");
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.DEFAULT, currentLocale2);
    Locale currentLocale3 = new Locale("en", "US");
    java.text.DateFormat formateadorFechaIngles = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, currentLocale3);

    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment; filename=hitos_paciente.xls");

//Creamos el archivo de excell
    HSSFWorkbook wb = new HSSFWorkbook();
    HSSFSheet sheet1 = wb.createSheet("                                                                                                        NOMINA PACIENTE CON CITAS CANCELADAS POR BLOQUEO DE CUPO");

// ***************************************[CREO TOTDOS LOS FONT Y LOS STYLE QUE NECESITE]****************************************/
    HSSFFont fontDatos = wb.createFont();
    fontDatos.setFontHeightInPoints((short) 8);
    fontDatos.setFontName("Vereda");
    fontDatos.setColor((short) HSSFColor.LIGHT_BLUE.index);
    HSSFCellStyle style_datos = wb.createCellStyle();
    style_datos.setFont(fontDatos);
    style_datos.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
    style_datos.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
    style_datos.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);

    /*para la linea*/
    HSSFFont fontDatos2 = wb.createFont();
    fontDatos2.setFontHeightInPoints((short) 8);
    fontDatos2.setFontName("Vereda");
    fontDatos2.setColor((short) HSSFColor.LIGHT_BLUE.index);
    HSSFCellStyle style_datos2 = wb.createCellStyle();
    style_datos2.setFont(fontDatos2);

    style_datos2.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);

    /**
     * **********************************************
     */
    HSSFFont fontTitulo = wb.createFont();
    fontTitulo.setFontHeightInPoints((short) 22);
    fontTitulo.setFontName("Calibri");
    fontTitulo.setColor((short) HSSFColor.INDIGO.index);

    fontTitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
    HSSFCellStyle style_Titulo = wb.createCellStyle();
    style_Titulo.setFont(fontTitulo);
    style_Titulo.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);
    // style_Titulo.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
    /*ESTILO PARA LA FECHA*/
    HSSFFont fontfecha = wb.createFont();
    fontfecha.setFontHeightInPoints((short) 16);
    fontfecha.setFontName("Calibri");
    fontfecha.setColor((short) HSSFColor.INDIGO.index);

    fontfecha.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
    HSSFCellStyle style_fecha = wb.createCellStyle();
    style_fecha.setFont(fontfecha);
    style_fecha.setAlignment((short) HSSFCellStyle.ALIGN_RIGHT);
    //style_fecha.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);

    /**
     * **********************************************
     */
    HSSFFont fontSubtitulo3 = wb.createFont();
    HSSFCellStyle style_Subtitulo3 = wb.createCellStyle();

    fontSubtitulo3.setFontHeightInPoints((short) 1);
    fontSubtitulo3.setFontName("Calibri");
    fontSubtitulo3.setColor((short) HSSFColor.WHITE.index);
    //fontSubtitulo.setItalic(true);
    fontSubtitulo3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
    style_Subtitulo3.setFont(fontSubtitulo3);
    style_Subtitulo3.setWrapText(true);
    style_Subtitulo3.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);
    //style_Subtitulo3.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
    //style_Subtitulo3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
    style_Subtitulo3.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
    style_Subtitulo3.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
    style_Subtitulo3.setTopBorderColor(new HSSFColor.LIGHT_BLUE().getIndex());
    //style_Subtitulo2.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
    //  style_Subtitulo2.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
    style_Subtitulo3.setFont(fontSubtitulo3);

    /**/
    HSSFFont fontSubtitulo = wb.createFont();
    HSSFCellStyle style_Subtitulo = wb.createCellStyle();

    fontSubtitulo.setFontHeightInPoints((short) 11);
    fontSubtitulo.setFontName("Calibri");
    fontSubtitulo.setColor((short) HSSFColor.WHITE.index);
    //fontSubtitulo.setItalic(true);
    fontSubtitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
    style_Subtitulo.setFont(fontSubtitulo);
    style_Subtitulo.setWrapText(true);
    style_Subtitulo.setAlignment((short) HSSFCellStyle.ALIGN_CENTER);

    style_Subtitulo.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
    style_Subtitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
    style_Subtitulo.setFillBackgroundColor(new HSSFColor.AQUA().getIndex());
    style_Subtitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

//Esta es el Header y el Footer
    HSSFHeader cabecera = sheet1.getHeader();
    cabecera.setLeft("hitos de paciente");
    HSSFFooter pie = sheet1.getFooter();
    pie.setCenter("Pagina " + HSSFFooter.page() + " de " + HSSFFooter.numPages());//N° de Pagina

//*********************************************************************************************
// Sheet1.createFreezePane (3, 2, 3, 2);
    sheet1.createFreezePane(0, 6);
    sheet1.setDisplayGridlines(false);
    /**
     * ***********************[ESTE ES TODO EL
     * ENCABEZADO]**************************************
     */
    HSSFRow rowTitulo = sheet1.createRow((short) 3); //Esta es la primera fila de la hoja
    HSSFRow rowHoy = sheet1.createRow((short) 1); //esta el la segunda
    HSSFRow rowlinea = sheet1.createRow((short) 4);
    HSSFRow rowlineafecha = sheet1.createRow((short) 2);
    HSSFCell cellTitulo = rowTitulo.createCell((short) 0); //TITULO
    cellTitulo.setCellValue("            INFORME HITOS DE LOS PACIENTES");
    cellTitulo.setCellStyle(style_Titulo);//le doy el estilo 

    HSSFCell celllinea = rowlinea.createCell((short) 0);
    HSSFCell celllineafecha = rowlineafecha.createCell((short) 6);

    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 0); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 1); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 2); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 3); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 4); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 5); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 6); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 7); //crea la celda tipo
    celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 8); //crea la celda tipo
     celllinea.setCellValue("");
    celllinea.setCellStyle(style_Subtitulo3);
    celllinea = rowlinea.createCell((short) 9); //crea la celda tipo

    HSSFCell cellHoy = rowHoy.createCell((short) 0); //FECHA DE GEERACION DEL INFORME
    cellHoy.setCellValue("FECHA: " + formateadorFecha.format(new Date()));
    cellHoy.setCellStyle(style_fecha);//le doy el estilo  

    celllineafecha.setCellValue("");
    celllineafecha.setCellStyle(style_Subtitulo3);
    celllineafecha = rowlineafecha.createCell((short) 4); //crea la celda tipo
    celllineafecha.setCellValue("");
    celllineafecha.setCellStyle(style_Subtitulo3);
    celllineafecha = rowlineafecha.createCell((short) 5); //crea la celda tipo
    celllineafecha.setCellValue("");
    celllineafecha.setCellStyle(style_Subtitulo3);
    celllineafecha = rowlineafecha.createCell((short) 6);

    Region region = new Region(1, (short) 0, 1, (short) 5);
    sheet1.addMergedRegion(region); //es para combinar celdas
    region = new Region(4, (short) 0, 4, (short) 9);
    sheet1.addMergedRegion(region); //es para combinar celdas



    short controlFila = 5;
    HSSFRow row1 = sheet1.createRow(controlFila); //crea el renglon o fila
    HSSFCell cell0 = row1.createCell((short) 0); //crea la celda tipo
    cell0.setCellValue("FECHA HITOS");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 1); //crea la celda tipo
    cell0.setCellValue("RUT PACIENTE");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 2); //crea la celda tipo
    cell0.setCellValue("PACIENTE");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 3); //crea la celda tipo
    cell0.setCellValue("DETALLE HITO");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 4); //crea la celda tipo
    cell0.setCellValue("DETALLE LLAMADA");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 5); //crea la celda tipo
    cell0.setCellValue("DETALLE CARTA");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 6); //crea la celda tipo
    cell0.setCellValue("DETALLE EXAMENES");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 7); //crea la celda tipo
    cell0.setCellValue("RESPONSABLE");
    cell0.setCellStyle(style_Subtitulo);
    cell0 = row1.createCell((short) 8); //crea la celda tipo
    cell0.setCellValue("IDHITO");
    cell0.setCellStyle(style_Subtitulo);
   cell0 = row1.createCell((short) 9); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo);
    //crea la celda tipo

    controlFila++;
    controlador_cita cc = new controlador_cita();
    

        int controlLinea = 1;
        String error = "";
        int x = 0;
        for (lugar hitos : cc.buscarhitosparaestadistico(fecha1,fecha2)) {
            try {
                row1 = sheet1.createRow(controlFila); //crea el renglon o fila
            } catch (Exception errorEx) {
                error = String.valueOf(errorEx);
            }
            cell0 = row1.createCell((short) 0); //crea la celda tipo
            cell0.setCellValue(formateadorFecha.format(hitos.getFecha()));
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 1); //crea la celda tipo
            cell0.setCellValue(hitos.getRut());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 2); //crea la celda tipo
            cell0.setCellValue(hitos.getNombre());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 3); //crea la celda tipo
            cell0.setCellValue(hitos.getDescripcion());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 4); //crea la celda tipo
            cell0.setCellValue(hitos.getVariable1());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 5); //crea la celda tipo
            cell0.setCellValue(hitos.getVariable2());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 6); //crea la celda tipo
            cell0.setCellValue(hitos.getVariable3());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 7); //crea la celda tipo
            cell0.setCellValue(hitos.getVariable4());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 8); //crea la celda tipo
            cell0.setCellValue(hitos.getId_lugar());
            if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

             cell0 = row1.createCell((short) 9); //crea la celda tipo
            cell0.setCellValue("");
             if (x % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            
            controlFila++;
            controlLinea++;
            x++;
        }
    

    /**
     * *******************************************************************************************************************
     */
    row1 = sheet1.createRow(controlFila);
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 0); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 1); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 2); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 3); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 4); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 5); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 6); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 7); //crea la celda tipo
    cell0.setCellValue("");
    cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 8); //crea la celda tipo
    cell0.setCellValue("");
     cell0.setCellStyle(style_Subtitulo3);
    cell0 = row1.createCell((short) 9); //crea la celda tipo
    cell0.setCellValue("");

    sheet1.setColumnWidth((short) 0, (short) 5700);
    sheet1.setColumnWidth((short) 1, (short) 4700);
    sheet1.setColumnWidth((short) 2, (short) 7500);
    sheet1.setColumnWidth((short) 3, (short) 13500);
    sheet1.setColumnWidth((short) 4, (short) 7500);
    sheet1.setColumnWidth((short) 5, (short) 7500);
    sheet1.setColumnWidth((short) 6, (short) 7500);
    sheet1.setColumnWidth((short) 7, (short) 4500);
    sheet1.setColumnWidth((short) 8, (short) 4500);
    sheet1.setColumnWidth((short) 9, (short) 4500);

    HSSFPrintSetup printSetup = sheet1.getPrintSetup();
    printSetup.setLandscape(true);
    printSetup.setPaperSize(printSetup.LETTER_PAPERSIZE);
    printSetup.setScale((short) 60);

    /**
     * *******************************************************************************************************************
     */
    wb.write(response.getOutputStream());

   
    response.flushBuffer();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
