/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estadisticos;

import Controlador.controlador_cita;
import Modelos.lugar;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import java.text.DateFormat;

import java.util.Locale;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.pdfconpieyfoto;
import Modelos.receta_lente_opticos;
import Modelos.vicio_refraccion;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfPTable;
import java.sql.SQLException;
import java.util.Date;
/**
 *
 * @author a
 */
public class hitospacientespdf extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
        DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);
        /*llego */

        String fechainicio = request.getParameter("inicio");
        String fechafin = request.getParameter("fin");
        Date fecha1 = new Date(Integer.parseInt(fechainicio.substring(6, 10)) - 1900, Integer.parseInt(fechainicio.substring(3, 5)) - 1, Integer.parseInt(fechainicio.substring(0, 2)), 0, 0, 0);
        Date fecha2 = new Date(Integer.parseInt(fechafin.substring(6, 10)) - 1900, Integer.parseInt(fechafin.substring(3, 5)) - 1, Integer.parseInt(fechafin.substring(0, 2)), 0, 0, 0);

        /*pinto el encabezado*/
        response.setContentType("application/pdf");
        Document document = new Document(PageSize.A0, 50, 50, 50, 50);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        int SPACE_TITULO = 2;
        int SPACE_NORMAL = 30;
        int SPACE_NORMAL2 = 15;
        int SPACE_ESPACIO = 10;

        Font TEXT_TITULO = FontFactory.getFont(FontFactory.TIMES_ROMAN, 45, Font.BOLD, new Color(32, 55, 100));
        Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 50, Font.BOLD, new Color(68, 114, 196));
        Font fecha = FontFactory.getFont(FontFactory.TIMES_ROMAN, 25, Font.BOLD, new Color(32, 55, 100));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.TIMES_ROMAN, 34, Font.BOLD, new Color(255, 255, 255));
        Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 28, Font.NORMAL, new Color(255, 255, 255));
        Font TEXT_Datos = FontFactory.getFont(FontFactory.TIMES_ROMAN, 28, Font.NORMAL, new Color(68, 114, 196));

        writer.setPageEvent(new pdfconpieyfoto());
        document.open();

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        Paragraph Fecha = new Paragraph();
        Fecha.add(new Phrase(SPACE_TITULO, "FECHA:" + formateadorFecha.format(new Date()).toUpperCase(), fecha));

        celda = new Cell(Fecha);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "__________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        Paragraph Titulo = new Paragraph();
        Titulo.add(new Phrase(SPACE_TITULO, "INFORME HITOS DEL PACIENTE", TEXT_TITULO));

        celda = new Cell(Titulo);
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "__________________________________________________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        /*incluyo las celdas al docuemnto*/
        document.add(tabla_titulo);

        /*creo la tabla*/
        int NumColumns = 9;
        PdfPTable datatable = new PdfPTable(NumColumns);
        int headerwidths[] = {90, 90, 93, 99, 94, 93,99, 94, 93};
        datatable.setWidths(headerwidths);
        datatable.setWidthPercentage(101);
        datatable.getDefaultCell().setPadding(2);
        datatable.getDefaultCell().setBorderWidth(0);
        datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

        datatable.getDefaultCell().setColspan(1);
        datatable.getDefaultCell().setBackgroundColor(new Color(68, 114, 196));
        datatable.addCell(new Phrase(SPACE_NORMAL, "FECHA HITO", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "RUT PACIENTE", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "PACIENTE", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "DETALLE HITO", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "DETALLE LLAMADA", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "DETALLE CARTA ", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "DETALLE EXAMENES ", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "RESPONSABLE", TEXT_NORMAL2));
        datatable.addCell(new Phrase(SPACE_NORMAL, "IDHITOS", TEXT_NORMAL2));

        controlador_cita cc = new controlador_cita();
        /*Query en pdf*/
         int x= 0;
            for (lugar hitos : cc.buscarhitosparaestadistico(fecha1, fecha2)) {
                datatable.getDefaultCell().setColspan(1);
                if (x % 2 == 0) {
                    datatable.getDefaultCell().setBackgroundColor(new Color(217, 225, 242));
                } else {
                    datatable.getDefaultCell().setBackgroundColor(new Color(255, 255, 255));
                }
                datatable.addCell(new Phrase(SPACE_NORMAL, formateadorFecha.format(hitos.getFecha()), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, hitos.getRut(), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, hitos.getNombre(), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, hitos.getDescripcion(), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, hitos.getVariable1(), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, hitos.getVariable2(), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, hitos.getVariable3(), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, hitos.getVariable4(), TEXT_Datos));
                datatable.addCell(new Phrase(SPACE_NORMAL, String.valueOf(hitos.getId_lugar()), TEXT_Datos));

                ++x;

            }
        
        document.add(datatable);

        document.close();
        try {
//PdfReader reader = new PdfReader(buffer.toByteArray());
//DataOutputStream output = new DataOutputStream(response.getOutputStream());
            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }
//output.flush();            
//output.close();
        } catch (Exception exstream) {
        }

       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(hitospacientespdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(hitospacientespdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
