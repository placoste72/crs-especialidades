/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estadisticos;

import Controlador.controlador_cupos;
import Controlador.controlador_paciente;
import Modelos.cita;
import Modelos.oferta;
import Modelos.paciente;
import Modelos.planificar;
import Modelos.protocolo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.Region;

/**
 *
 * @author a
 */
public class TablaQuirurgica extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, WriteException {
        Date fecha_del_dia = new Date();
        Locale currentLocale = new Locale("es", "CL");
        java.text.DateFormat formateadorFecha = java.text.DateFormat.getDateInstance(java.text.DateFormat.MEDIUM, currentLocale);
        controlador_cupos cc = new controlador_cupos();
        String fechainicio = request.getParameter("inicio");
        String fechafin = request.getParameter("fin");
        Date fecha1 = new Date(Integer.parseInt(fechainicio.substring(6, 10)) - 1900, Integer.parseInt(fechainicio.substring(3, 5)) - 1, Integer.parseInt(fechainicio.substring(0, 2)), 0, 0, 0);
        Date fecha2 = new Date(Integer.parseInt(fechafin.substring(6, 10)) - 1900, Integer.parseInt(fechafin.substring(3, 5)) - 1, Integer.parseInt(fechafin.substring(0, 2)), 0, 0, 0);

        String nombre_ayudante = "";

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=tablaquirurgica" + formateadorFecha.format(fecha_del_dia) + ".xls");
        WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
        HSSFWorkbook wb = new HSSFWorkbook();
        int cont = 0;

        WritableSheet sheet = workbook.createSheet("Tabla Quirurgica", cont);
        WritableCellFormat formatoCodigobarra = new WritableCellFormat(NumberFormats.INTEGER);
        WritableFont tahoma16font = new WritableFont(WritableFont.createFont("Calibri"), 22, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.LIGHT_BLUE);
        WritableCellFormat formatoTitulos = new WritableCellFormat(tahoma16font);
        formatoTitulos.setAlignment(Alignment.CENTRE);
        formatoTitulos.setVerticalAlignment(jxl.write.VerticalAlignment.CENTRE);
        formatoTitulos.setBackground(Colour.WHITE);

//formatoTitulos.setShrinkToFit(true);
        WritableFont tahoma13font = new WritableFont(WritableFont.ARIAL, 13, WritableFont.BOLD, false);
        WritableCellFormat formatoCeldaTitulos = new WritableCellFormat(tahoma13font);

        WritableFont Item = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.WHITE);
        WritableCellFormat FormatoItem = new WritableCellFormat(Item);
        FormatoItem.setBorder(Border.ALL, BorderLineStyle.NONE);
        FormatoItem.setAlignment(Alignment.CENTRE);
        FormatoItem.setBackground(Colour.LIGHT_BLUE);
        FormatoItem.setWrap(true);

        WritableFont Datos = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatos = new WritableCellFormat(Datos);
        FormatoDatos.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatos.setAlignment(Alignment.LEFT);
        FormatoDatos.setBackground(Colour.WHITE);

        WritableFont Datoshitos = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatoshitos = new WritableCellFormat(Datoshitos);
        FormatoDatoshitos.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatoshitos.setAlignment(Alignment.LEFT);
        FormatoDatoshitos.setBackground(Colour.LIGHT_BLUE);

        WritableFont Datoscitas = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatoscitas = new WritableCellFormat(Datoscitas);
        FormatoDatoscitas.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatoscitas.setAlignment(Alignment.LEFT);
        FormatoDatoscitas.setBackground(Colour.LIGHT_GREEN);

        WritableFont Datosprotocolos = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatosprotocolos = new WritableCellFormat(Datosprotocolos);
        FormatoDatosprotocolos.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatosprotocolos.setAlignment(Alignment.LEFT);
        FormatoDatosprotocolos.setBackground(Colour.LIGHT_ORANGE);

        WritableFont Datosprestaciones = new WritableFont(WritableFont.createFont("Calibri"), 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatosprestaciones = new WritableCellFormat(Datosprestaciones);
        FormatoDatosprestaciones.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatosprestaciones.setAlignment(Alignment.LEFT);
        FormatoDatosprestaciones.setBackground(Colour.WHITE);
        FormatoDatosprestaciones.setWrap(true);

        /*para el doctor*/
        WritableFont Datosdoctor = new WritableFont(WritableFont.createFont("Calibri"), 8, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.BLACK);
        WritableCellFormat FormatoDatosdoctor = new WritableCellFormat(Datosdoctor);
        FormatoDatosdoctor.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
        FormatoDatosdoctor.setAlignment(Alignment.CENTRE);
        FormatoDatosdoctor.setBackground(Colour.WHITE);
        FormatoDatosprestaciones.setWrap(true);

        WritableFont Datos_centrado = new WritableFont(WritableFont.createFont("Calibri"), 8, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.BLACK);
        WritableCellFormat FormatoDatos_centrado = new WritableCellFormat(Datos_centrado);
        FormatoDatos_centrado.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatos_centrado.setAlignment(Alignment.CENTRE);
        FormatoDatos_centrado.setBackground(Colour.WHITE);
        FormatoDatos_centrado.setWrap(true);

        WritableFont DatosFecha = new WritableFont(WritableFont.createFont("Calibri"), 20, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.BLACK);
        WritableCellFormat FormatoDatosFechaGeneracion = new WritableCellFormat(DatosFecha);
        FormatoDatosFechaGeneracion.setAlignment(Alignment.LEFT);
        FormatoDatosFechaGeneracion.setBackground(Colour.WHITE);
        Label fechaGeneracion = new Label(6, 2, "CENTRO DE REFERENCIA DE SALUD DE MAIPÚ", FormatoDatosFechaGeneracion);
        sheet.addCell(fechaGeneracion);

        WritableFont tabla = new WritableFont(WritableFont.createFont("Calibri"), 14, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.BLACK);
        WritableCellFormat Formatotabla = new WritableCellFormat(tabla);
        Formatotabla.setAlignment(Alignment.CENTRE);
        Formatotabla.setBackground(Colour.BLACK);
        Formatotabla.setBorder(Border.ALL, BorderLineStyle.THIN);
        int controldecomienzo = 0;
        int fila = 1;
        int j = 1;
        int k = 0;

        String fechaguarda = "";
        int duracion = 0;
        int idsala = 0;
        String rutdoctoruarda = "";

        /*buscar las planificaciones */
        int columna = 9;
        int n = 1;
        for (oferta oft : cc.buscarPlanificacionycitasparaReportedeTablaQuirurgica(fecha1, fecha2)) {

            if (!oft.getTemporal6().equals(fechaguarda) && !oft.getTemporal3().equals(rutdoctoruarda)) {

                if (!fechaguarda.equals("")) {
                    controldecomienzo = controldecomienzo + 7;
                    columna = 9;
                    n = 1;
                    fila = 1 + controldecomienzo;
                }
                Label fecha = new Label(j, 4, "TABLA QUIRÚRGICA " + oft.getTemporal6(), Formatotabla);
                sheet.addCell(fecha);
                // sheet.mergeCells(1, 5, 7, 6);
                sheet.mergeCells(1 + controldecomienzo, 4, 7 + controldecomienzo, 4);

                Label tabla2 = new Label(j, 5, oft.getTemporal4(), Formatotabla);
                sheet.addCell(tabla2);
                // sheet.mergeCells(1, 5, 7, 6);
                sheet.mergeCells(1 + controldecomienzo, 5, 7 + controldecomienzo, 6);

                Label tabla3 = new Label(j, 7, "FRECUENCIA DE ATENCIÓN:" + String.valueOf(oft.getDuracion()), Formatotabla);
                sheet.addCell(tabla3);
                // sheet.mergeCells(j, 7, 7, 7);
                sheet.mergeCells(1 + controldecomienzo, 7, 7 + controldecomienzo, 7);
                Label sub1 = new Label(j, 8, "n°", FormatoDatos_centrado);
                sheet.addCell(sub1);
                ++j;
                Label sub2 = new Label(j, 8, "HORARIO INTERVENCIÓN QUIRURGICA", FormatoDatos_centrado);
                sheet.addCell(sub2);
                ++j;
                Label sub3 = new Label(j, 8, "HORARIO CITACION PACIENTE", FormatoDatos_centrado);
                sheet.addCell(sub3);
                ++j;
                Label sub4 = new Label(j, 8, "CIRUGIA PROGRAMADA", FormatoDatos_centrado);
                sheet.addCell(sub4);
                ++j;
                Label sub5 = new Label(j, 8, "DR. ", FormatoDatosdoctor);
                sheet.addCell(sub5);

                j++;
                Label sub6 = new Label(j, 8, oft.getTemporal5(), FormatoDatosdoctor);
                sheet.addCell(sub6);
                j++;
                Label sub7 = new Label(j, 8, "PRESTACIÓN PLANIFICADA", FormatoDatos_centrado);
                sheet.addCell(sub7);
                j++;
            }
            /*for con la informacion*/

            Label dato_texto;

            dato_texto = new Label(fila, columna, String.valueOf(n), FormatoDatosprestaciones);
            sheet.addCell(dato_texto);
            fila++;
            dato_texto = new Label(fila, columna, oft.getHorapabellon(), FormatoDatosprestaciones);
            sheet.addCell(dato_texto);
            fila++;
            dato_texto = new Label(fila, columna, oft.getHora(), FormatoDatosprestaciones);
            sheet.addCell(dato_texto);
            fila++;
            dato_texto = new Label(fila, columna, oft.getTemporal(), FormatoDatosprestaciones);
            sheet.addCell(dato_texto);
            fila++;
            dato_texto = new Label(fila, columna, oft.getTemporal3(), FormatoDatosprestaciones);
            sheet.addCell(dato_texto);
            fila++;
            dato_texto = new Label(fila, columna, oft.getTemporal1(), FormatoDatosprestaciones);
            sheet.addCell(dato_texto);
            fila++;
            dato_texto = new Label(fila, columna, oft.getTemporal2(), FormatoDatosprestaciones);
            sheet.addCell(dato_texto);
            columna++;
            n++;
            fila = fila - 6;
            fechaguarda = oft.getTemporal6();
            rutdoctoruarda = oft.getTemporal3();
        }

        workbook.write();
        workbook.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (WriteException ex) {
            Logger.getLogger(TablaQuirurgica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (WriteException ex) {
            Logger.getLogger(TablaQuirurgica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
