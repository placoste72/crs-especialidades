/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estadisticos;

import Controlador.controlador_cita;
import Modelos.cita;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Vector;
import Modelos.pdfconpieyfoto;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class lista_paciente_doctor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        response.setContentType("text/html;charset=UTF-8");

    

    Locale currentLocale = new Locale("es", "CL");
    Locale currentLocaleHora = new Locale("es", "CHL");
    DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
    DateFormat formateadorFecha2 = DateFormat.getDateInstance(DateFormat.MEDIUM, currentLocale);
    DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);

     String fechainicio = request.getParameter("inicio");
    String fechafin = request.getParameter("fin");
    Date fecha1 = new Date(Integer.parseInt(fechainicio.substring(6, 10)) - 1900, Integer.parseInt(fechainicio.substring(3, 5)) - 1, Integer.parseInt(fechainicio.substring(0, 2)), 0, 0, 0);
    Date fecha2 = new Date(Integer.parseInt(fechafin.substring(6, 10)) - 1900, Integer.parseInt(fechafin.substring(3, 5)) - 1, Integer.parseInt(fechafin.substring(0, 2)), 0, 0, 0);

    String doctor = request.getParameter("doctor");
    String especialidad = request.getParameter("especialidad");

    Vector id_medico = new Vector();
    Vector apellido_medico = new Vector();
    Vector nombre_medico = new Vector();

    controlador_cita cc = new controlador_cita();

    response.setContentType("application/pdf");
    Document document = new Document(PageSize.LEDGER, 30, 30, 50, 50);
    document.addAuthor("Centro de Referencia de Salud Maipu");
//ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//PdfWriter writer = PdfWriter.getInstance( document, buffer );
    PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());

    int SPACE_TITULO = 10;
    int SPACE_SUBTITULO = 10;
    int SPACE_NORMAL = 10;
    int SPACE_NORMAL2 = 10;
    int SPACE_SIGLAS = 8;

    Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 1));
    Font TEXT_TITULODATOS = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 1));
    Font TEXT_SUBTITULO = FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD, new Color(0, 0, 1));
    Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 3));
    Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL, new Color(0, 0, 3));
    Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
    Font TEXT_SIGLAS = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 3));

//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
    writer.setPageEvent(new pdfconpieyfoto());
    document.open();
    int i = 0;
    for (cita doc : cc.buscarCitasdeunaFechaoEspecialidadoDoctro(fecha1,fecha2,Integer.parseInt(especialidad),doctor)) {
        
        Table tabla_titulo;
        Cell celda;
        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(2);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        Table tabla_titulo_NO;
        Cell celda_NO;
        tabla_titulo_NO = new Table(3);
        tabla_titulo_NO.setBorderWidth(0);
        tabla_titulo_NO.setPadding(2);
        tabla_titulo_NO.setSpacing(0);
        tabla_titulo_NO.setWidth(100);

        if (i > 0) {
            document.newPage();
        }

        celda = new Cell(new Phrase(SPACE_TITULO, "", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "LISTADO DE PACIENTES DEL" +fechainicio + " AL " + fechafin, TEXT_TITULOSUB));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, formateadorFecha.format(new Date()).toUpperCase() + " " + formateaHora.format(new Date()) + " Hrs.", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        celda = new Cell("");
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        int totalNuevos = 0;
        if (doc.getId_tipoatencion() == 1) {
            totalNuevos = doc.getGes();
        }

        celda = new Cell(new Phrase(SPACE_TITULO, "TOTAL NUEVOS: " + String.valueOf(totalNuevos), TEXT_TITULODATOS));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        int totalControl = 0;
        if (doc.getId_tipoatencion() == 2) {
            totalControl = doc.getGes();
        }

        celda = new Cell(new Phrase(SPACE_TITULO, "TOTAL CONTROL: " + String.valueOf(totalControl), TEXT_TITULODATOS));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        //celda = new Cell("FIRMA PROFESIONAL:\n-----------------------------------------------");
        celda = new Cell("USO EXCLUSIVO ARCHIVO");
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setRowspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
        tabla_titulo.addCell(celda);
        int totalExamenes = 0;

        celda = new Cell(new Phrase(SPACE_TITULO, "TOTAL EXAMENES: " + String.valueOf(totalExamenes), TEXT_TITULODATOS));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        int totalProcedimientos = 0;
        if (doc.getId_tipoatencion() == 4) {
            totalProcedimientos = doc.getGes();
        }

        celda = new Cell(new Phrase(SPACE_TITULO, "TOTAL PROCEDIMIENTOS: " + String.valueOf(totalProcedimientos), TEXT_TITULODATOS));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        int totalCI = 0;
        if (doc.getId_tipoatencion() == 3) {
            totalCI = doc.getGes();
        }

        celda = new Cell(new Phrase(SPACE_TITULO, "TOTAL CIRUGIAS: " + String.valueOf(totalCI), TEXT_TITULODATOS));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "TOTAL PACIENTES: " + String.valueOf(doc.getEstatus()), TEXT_TITULODATOS));
        celda.setBorderWidth(0);
        celda.setColspan(1);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        //celda = new Cell(new Phrase(SPACE_SIGLAS,"Destino: 1 - Alta(Consultorio), 2 - Control, 3 - Hospital, 4 - Cirugia   T/C: N - Nueva, C - Control, P - Procedimiento, E - Examen, CI - Cirugia.   I.C=Inter Consulta.     Sospecha GES: S - SI, N - NO",TEXT_SIGLAS));
        celda = new Cell("");
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
        tabla_titulo.addCell(celda);

        document.add(tabla_titulo);

        int NumColumns = 10;
        PdfPTable datatable = new PdfPTable(NumColumns);
        int headerwidths[] = {3, 7, 10, 6, 32, 10, 5, 44, 7, 7};
        datatable.setWidths(headerwidths);
        datatable.setWidthPercentage(100);
        datatable.getDefaultCell().setPadding(3);
        datatable.getDefaultCell().setBorderWidth(1);
        datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        datatable.getDefaultCell().setColspan(5);
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "ESPECIALIDAD: " + doc.getTemporales1(), TEXT_SUBTITULO));
        datatable.getDefaultCell().setColspan(3);
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "PROFESIONAL: " + doc.getTemporales(), TEXT_SUBTITULO));
        datatable.getDefaultCell().setColspan(2);
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "FECHA: "+formateadorFecha.format(new Date()), TEXT_SUBTITULO));
        datatable.getDefaultCell().setColspan(1);
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "N°", TEXT_SUBTITULO));
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "HORA", TEXT_SUBTITULO));
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "RUT", TEXT_SUBTITULO));
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "EDAD", TEXT_SUBTITULO));
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "PACIENTE", TEXT_SUBTITULO));
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "PREV.", TEXT_SUBTITULO));
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "T/C", TEXT_SUBTITULO));
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "ULTIMAS CITAS O UOCE", TEXT_SUBTITULO));
        //datatable.addCell(new Phrase(SPACE_SUBTITULO,"PENULTIMA CITA",TEXT_SUBTITULO));
        datatable.getDefaultCell().setColspan(2);
        datatable.addCell(new Phrase(SPACE_SUBTITULO, "¿FICHA ENTREGADA?", TEXT_SUBTITULO));
        datatable.getDefaultCell().setColspan(1);
        //datatable.addCell(new Phrase(SPACE_SUBTITULO,"DIAGNOSTICO",TEXT_SUBTITULO));
        //datatable.addCell(new Phrase(SPACE_SUBTITULO,"GES",TEXT_SUBTITULO));

        datatable.setHeaderRows(2);
        datatable.getDefaultCell().setBorderWidth(0);

        int control_pagina = 1;
        //int diferencia=0;
        //String numeroFicha="";
        int diaCumple = 0;
        int mesCumple = 0;
        int anoCumple = 0;
        String tipoCitaSigla = "";
        String edadPaciente = "";
        int x = 0;
        for (cita c : cc.buscarListaPacienteporDiaEspecialidadyDoctro2( doc.getId_cita())) {
            if (x % 2 == 0) {
                datatable.getDefaultCell().setBackgroundColor(Color.LIGHT_GRAY);
            }
            datatable.addCell(new Phrase(SPACE_NORMAL2, String.valueOf(control_pagina), TEXT_NORMAL2));
            datatable.addCell(new Phrase(SPACE_NORMAL2, c.getTemporales(), TEXT_NORMAL2));
            datatable.addCell(new Phrase(SPACE_NORMAL2, c.getRut_paciente(), TEXT_NORMAL2));

            datatable.addCell(new Phrase(SPACE_NORMAL2, c.getTemporales1(), TEXT_NORMAL2));
            datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            datatable.addCell(new Phrase(SPACE_NORMAL2, c.getTemporales2(), TEXT_NORMAL2));
            datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            String prevision = "";

            datatable.addCell(new Phrase(SPACE_NORMAL2, c.getTemporales3(), TEXT_NORMAL2));
            int controlControl = 0;

            
            datatable.addCell(new Phrase(SPACE_NORMAL2, c.getTemporales4(), TEXT_NORMAL2));
            int columnas = 5;
            PdfPTable citasAnteriores = new PdfPTable(columnas);
            int cabeceras[] = {20, 20, 20, 20, 20};
            citasAnteriores.setWidths(cabeceras);
            citasAnteriores.setWidthPercentage(100);
            citasAnteriores.getDefaultCell().setPadding(3);
            citasAnteriores.getDefaultCell().setBorderWidth(1);
            citasAnteriores.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            citasAnteriores.getDefaultCell().setBackgroundColor(Color.CYAN);
            citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, "FECHA", TEXT_SUBTITULO));
            citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, "HORA", TEXT_SUBTITULO));
            citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, "ESPE.", TEXT_SUBTITULO));
            citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, "PROF.", TEXT_SUBTITULO));
            citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, "ESTADO", TEXT_SUBTITULO));

            if (x % 2 == 0) {
                citasAnteriores.getDefaultCell().setBackgroundColor(Color.LIGHT_GRAY);
            } else {
                citasAnteriores.getDefaultCell().setBackgroundColor(Color.WHITE);
            }
            citasAnteriores.setHeaderRows(1);
            int controlCitas = 0;
            ++x;
            //if(controlControl>0 || Integer.parseInt(String.valueOf(tipoCita.get(x)))>1)
            //{

            for (cita tem : cc.buscarultimas2citasporPacienteapartirdeunafecha(c.getRut_paciente(), fecha2)) {
                controlCitas++;
                citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, tem.getFecha().toString(), TEXT_SUBTITULO));
                citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, tem.getTemporales3(), TEXT_SUBTITULO));
                citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, tem.getTemporales(), TEXT_SUBTITULO));
                citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, tem.getTemporales2(), TEXT_SUBTITULO));
                citasAnteriores.addCell(new Phrase(SPACE_SUBTITULO, tem.getTemporales1(), TEXT_SUBTITULO));
                ++controlCitas;

            }

            if (controlCitas > 0) {
                datatable.addCell(citasAnteriores);
            } else {
                datatable.addCell(new Phrase(SPACE_NORMAL2, "CITAS: SIN DATOS \n ", TEXT_NORMAL2));
            }

            datatable.getDefaultCell().setBorderWidth(1);
            datatable.addCell(new Phrase(SPACE_NORMAL2, "SI", TEXT_NORMAL2));
            datatable.addCell(new Phrase(SPACE_NORMAL2, "NO", TEXT_NORMAL2));
            datatable.getDefaultCell().setBorderWidth(0);
            control_pagina++;//numeroFicha="";
            datatable.getDefaultCell().setBackgroundColor(Color.white);
      
        } 
       
        if(x == 0){
          
        document.newPage();
        Table tabla_titulo_NOpaciente;
        Cell celda_NOpaciente;
        tabla_titulo_NOpaciente = new Table(3);
        tabla_titulo_NOpaciente.setBorderWidth(0);
        tabla_titulo_NOpaciente.setPadding(2);
        tabla_titulo_NOpaciente.setSpacing(0);
        tabla_titulo_NOpaciente.setWidth(100);

        celda_NOpaciente = new Cell(new Phrase(SPACE_TITULO, "\n\n\n", TEXT_TITULOSUB));
        celda_NOpaciente.setBorderWidth(0);
        celda_NOpaciente.setColspan(3);
        celda_NOpaciente.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NOpaciente.addCell(celda_NOpaciente);
        celda_NOpaciente = new Cell(new Phrase(SPACE_TITULO, formateadorFecha.format(new Date()).toUpperCase() + "\n" + formateaHora.format(new Date()) + " Hrs.", TEXT_TITULO));
        celda_NOpaciente.setBorderWidth(0);
        celda_NOpaciente.setColspan(3);
        celda_NOpaciente.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NO.addCell(celda_NOpaciente);
        celda_NOpaciente = new Cell(new Phrase(SPACE_TITULO, "LISTADO DE PACIENTES PARA EL " +formateadorFecha.format(fecha1) + " AL " + formateadorFecha.format(fecha2), TEXT_TITULOSUB));
        celda_NOpaciente.setBorderWidth(0);
        celda_NOpaciente.setColspan(3);
        celda_NOpaciente.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NOpaciente.addCell(celda_NOpaciente);
        celda_NOpaciente = new Cell(new Phrase(SPACE_TITULO, "NO HAY PACIENTES AGENDADOS PARA EL MEDICO "+ doc.getTemporales(), TEXT_TITULOSUB));
        celda_NOpaciente.setBorderWidth(2);
        celda_NOpaciente.setColspan(3);
        celda_NOpaciente.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NO.addCell(celda_NOpaciente);
        document.add(tabla_titulo_NOpaciente);  
        }
       ++i;  
          document.add(datatable);
    }
    if (i == 0) {

        document.newPage();
        Table tabla_titulo_NO;
        Cell celda_NO;
        tabla_titulo_NO = new Table(3);
        tabla_titulo_NO.setBorderWidth(0);
        tabla_titulo_NO.setPadding(2);
        tabla_titulo_NO.setSpacing(0);
        tabla_titulo_NO.setWidth(100);

        celda_NO = new Cell(new Phrase(SPACE_TITULO, "\n\n\n", TEXT_TITULOSUB));
        celda_NO.setBorderWidth(0);
        celda_NO.setColspan(3);
        celda_NO.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NO.addCell(celda_NO);
        celda_NO = new Cell(new Phrase(SPACE_TITULO, formateadorFecha.format(new Date()).toUpperCase() + "\n" + formateaHora.format(new Date()) + " Hrs.", TEXT_TITULO));
        celda_NO.setBorderWidth(0);
        celda_NO.setColspan(3);
        celda_NO.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NO.addCell(celda_NO);
        celda_NO = new Cell(new Phrase(SPACE_TITULO, "LISTADO DE PACIENTES PARA EL "+ fechainicio + " AL " + fechafin, TEXT_TITULOSUB));
        celda_NO.setBorderWidth(0);
        celda_NO.setColspan(3);
        celda_NO.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NO.addCell(celda_NO);
        celda_NO = new Cell(new Phrase(SPACE_TITULO, "NO HAY PACIENTES AGENDADOS PARA EL MEDICO "   , TEXT_TITULOSUB));
        celda_NO.setBorderWidth(2);
        celda_NO.setColspan(3);
        celda_NO.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo_NO.addCell(celda_NO);
        document.add(tabla_titulo_NO);
    }

    document.close();

        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(lista_paciente_doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(lista_paciente_doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
