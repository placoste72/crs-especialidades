/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estadisticos;

import Controlador.controlador_atencion_clinica_oftalmologia;

import Controlador.controlador_doctor;

import Modelos.excepcion_garantia;

import Modelos.pdfpagina;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import java.text.DateFormat;

import java.util.Locale;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.pdfconpieyfoto;
import com.lowagie.text.Image;

import java.sql.SQLException;

/**
 *
 * @author a
 */
public class ExcepciondeGarantia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);
        DateFormat formateadorFechaSimple = DateFormat.getDateInstance(DateFormat.DATE_FIELD, currentLocale);
        String atencion = request.getParameter("idatencion");
        String tipo = request.getParameter("tipo");
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();

        response.setContentType("application/pdf");
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        PdfWriter writer = PdfWriter.getInstance(document, buffer);

        int SPACE_TITULO = 1;
        int SPACE_NORMAL = 12;
        int SPACE_NORMAL2 = 17;
        int SPACE_ESPACIO = 4;
        Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(68, 117, 196));
        Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
        Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD, new Color(68, 114, 196));
        Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));
//Phrase fecha_movimiento_glosa = new Phrase(SPACE_SUBTITULO,"Fecha Movimiento: "+formateadorFecha.format(fecha_movimiento),TEXT_SUBTITULO);
        writer.setPageEvent(new pdfpagina());
        document.open();

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        String serviciot = "";
        String establecimientot = "";
        String especialidadt = "";
        String unidadt = "";

        excepcion_garantia exg = caco.buscardocumentoexcepciondeGarantia(Integer.parseInt(atencion), Integer.parseInt(tipo));
        serviciot = "Metropolitano Central(SSMC)";
        establecimientot = "Centro de Referencia de Salud de Maipú";
        especialidadt = "Oftalmología";
        unidadt = "Oftalmología";
       
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Excepción de Garantía", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            /**/
            Paragraph rutCita = new Paragraph();
            rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutCita.add(new Phrase(SPACE_TITULO, "Folio:", TEXT_NORMAL));
            rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutCita.add(new Phrase(SPACE_TITULO, exg.getAuxiliar1(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(rutCita);
            celda.setBorderWidth(0);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            /**/
            Paragraph nombreCita = new Paragraph();
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, "Fecha y hora del Informe: ", TEXT_NORMAL));
            nombreCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombreCita.add(new Phrase(SPACE_TITULO, exg.getAuxiliar2(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(nombreCita);
            celda.setBorderWidth(0);

            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Prestador", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph fechaCita = new Paragraph();
            fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fechaCita.add(new Phrase(SPACE_TITULO, "Servicio de Salud: ", TEXT_NORMAL));
            fechaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fechaCita.add(new Phrase(SPACE_TITULO, serviciot, TEXT_SUPERTITULONORMAL));
            celda = new Cell(fechaCita);
            celda.setBorderWidth(0);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph horaCita = new Paragraph();
            horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            horaCita.add(new Phrase(SPACE_TITULO, "Establecimiento: ", TEXT_NORMAL));
            horaCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            horaCita.add(new Phrase(SPACE_TITULO, establecimientot, TEXT_SUPERTITULONORMAL));
            celda = new Cell(horaCita);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph especialidadCita = new Paragraph();
            especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            especialidadCita.add(new Phrase(SPACE_TITULO, "Especialidad: ", TEXT_NORMAL));
            especialidadCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            especialidadCita.add(new Phrase(SPACE_TITULO, especialidadt, TEXT_SUPERTITULONORMAL));
            celda = new Cell(especialidadCita);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph medicoCita = new Paragraph();
            medicoCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            medicoCita.add(new Phrase(SPACE_TITULO, "Unidad:", TEXT_NORMAL));
            medicoCita.add(new Phrase(SPACE_NORMAL2, " ", TEXT_SUPERTITULONORMAL));
            medicoCita.add(new Phrase(SPACE_TITULO, unidadt, TEXT_SUPERTITULONORMAL));
            celda = new Cell(medicoCita);
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            /*datos del paciente*/
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Datos del Paciente", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph AtencionGlosa = new Paragraph();
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, "Nombre Completo: ", TEXT_NORMAL));
            AtencionGlosa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa.add(new Phrase(SPACE_TITULO, exg.getAuxiliar3(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(AtencionGlosa);
            //celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph AtencionGlosa1 = new Paragraph();
            AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            AtencionGlosa1.add(new Phrase(SPACE_TITULO, exg.getAuxiliar4(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(AtencionGlosa1);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph fechanacimiento = new Paragraph();
            fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fechanacimiento.add(new Phrase(SPACE_TITULO, "Fecha de nacimiento: ", TEXT_NORMAL));
            fechanacimiento.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fechanacimiento.add(new Phrase(SPACE_TITULO, exg.getAuxiliar5(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(fechanacimiento);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph edad = new Paragraph();
            edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            edad.add(new Phrase(SPACE_TITULO, "Edad: ", TEXT_NORMAL));
            edad.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            edad.add(new Phrase(SPACE_TITULO, exg.getAuxiliar6() + " años", TEXT_SUPERTITULONORMAL));
            celda = new Cell(edad);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            /*Datos de derivacion*/
            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Datos Clínicos", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph sederiva = new Paragraph();
            sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            sederiva.add(new Phrase(SPACE_TITULO, "Problema de Salud AUGE: ", TEXT_NORMAL));
            sederiva.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            sederiva.add(new Phrase(SPACE_TITULO, exg.getProblemaauge(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(sederiva);
            //  celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph diagnostico = new Paragraph();
            diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnostico.add(new Phrase(SPACE_TITULO, "CAUSAL DEL EXCEPCIÓN: ", TEXT_NORMAL));
            diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnostico.add(new Phrase(SPACE_TITULO, exg.getAuxiliar7(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(diagnostico);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph fd = new Paragraph();
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, "OBSERVACIONES: ", TEXT_NORMAL));
            fd.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            fd.add(new Phrase(SPACE_TITULO, exg.getObservaciones(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(fd);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);
            celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            celda = new Cell(new Phrase(SPACE_TITULO, "Profesional Tratante", TEXT_SUPERTITULO));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph nombredoctor = new Paragraph();
            nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombredoctor.add(new Phrase(SPACE_TITULO, "Nombre: ", TEXT_NORMAL));
            nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            nombredoctor.add(new Phrase(SPACE_TITULO, exg.getAuxiliar8(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(nombredoctor);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph rutdoctor = new Paragraph();
            rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutdoctor.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
            rutdoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            rutdoctor.add(new Phrase(SPACE_TITULO, exg.getAuxiliar9(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(rutdoctor);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            Paragraph profesiondoctor = new Paragraph();
            profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            profesiondoctor.add(new Phrase(SPACE_TITULO, "Profesión: ", TEXT_NORMAL));
            profesiondoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            profesiondoctor.add(new Phrase(SPACE_TITULO, exg.getAuxiliar10(), TEXT_SUPERTITULONORMAL));
            celda = new Cell(profesiondoctor);
            // celda.setBackgroundColor(new Color(217, 225, 242));
            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);

            document.add(tabla_titulo);
            /*para la firma del doctor*/
            controlador_doctor cdoctor = new controlador_doctor();

            byte[] imag = cdoctor.obtenImagenDoctor(exg.getAuxiliar9());
            if (imag != null) {
                Image image22 = Image.getInstance(imag);

                image22.setAbsolutePosition(15f, 15f);
                document.add(image22);
            }

            document.close();
        
        try {

            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }

        } catch (Exception exstream) {
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(ExcepciondeGarantia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ExcepciondeGarantia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(ExcepciondeGarantia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ExcepciondeGarantia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
