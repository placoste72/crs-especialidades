/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.doctor;
import Modelos.especialidades;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Informatica
 */
public class controlador_doctor extends General {

    public doctor traerdoctor(String rut) {
        doctor d = new doctor();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from agenda.doctor where upper(rut) =upper('" + rut + "');");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                d.setRut(this.cnn.getRst().getString("rut"));
                d.setNombre(this.cnn.getRst().getString("nombre"));
                d.setApellido_paterno(this.cnn.getRst().getString("apellido_paterno"));
                d.setApellido_materno(this.cnn.getRst().getString("apellido_materno"));
                d.setTelefono(this.cnn.getRst().getString("telefono"));
                d.setEmail(this.cnn.getRst().getString("email"));
                d.setProfesion(this.cnn.getRst().getString("profesion"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_doctor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return d;

    }

    //insertar al doctro
    public String ingresarDoctorSinFoto(doctor d) throws UnknownHostException, FileNotFoundException, IOException {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO "
                + "  agenda.doctor "
                + "   (  rut,   "
                + "    nombre,  "
                + "    apellido_paterno,  "
                + "     apellido_materno,  "
                + "    telefono, "
                + "    email, "
                + "     estatus, "
                + "      ipusuario, "
                + "      usuario, "
                + "     profesion ) "
                + "VALUES ("
                + "  '" + d.getRut() + "',"
                + "  '" + d.getNombre() + "',"
                + "  '" + d.getApellido_paterno() + "',"
                + "  '" + d.getApellido_materno() + "',"
                + "  '" + d.getTelefono() + "',"
                + "  '" + d.getEmail() + "',"
                + "  " + d.getEstatus() + ","
                + "  '" + getIpUsuarioLoggin() + "' ,"
                + "  '" + getUsuarioLoggin() + "', "
                + "  '" + d.getProfesion() + "'"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Profesional Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

    public String ingresarDoctor(doctor d, String direccionfirma)  {
      this.configurarConexion("");
       String inserto = "";
        Connection cn = null;
        PreparedStatement pr = null;
        String sql = "INSERT INTO \n"
                + "  agenda.doctor\n"
                + "(\n"
                + "  rut,\n"
                + "  nombre,\n"
                + "  apellido_paterno,\n"
                + "  apellido_materno,\n"
                + "  telefono,\n"
                + "  email,\n"
                + "  estatus,\n"
                + "  ipusuario,\n"
                + "  usuario,\n"
                + "  firmadoctor,\n"
                + "  profesion,\n"
                + "  nombrefirma,\n"
                + "  tamanofirma\n"
                + ")";
        sql += "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            Class.forName(this.cnn.getDriver());
            cn = DriverManager.getConnection(this.cnn.getNombreBaseDatos(), this.cnn.getUser(), this.cnn.getPassword());
            pr = cn.prepareStatement(sql);
            pr.setString(1, d.getRut());
            pr.setString(2, d.getNombre());
            pr.setString(3, d.getApellido_paterno());
            pr.setString(4, d.getApellido_materno());
            pr.setString(5, d.getTelefono());
            pr.setString(6, d.getEmail());
            pr.setInt(7, 1);
            pr.setString(8, getIpUsuarioLoggin());
            pr.setString(9, getUsuarioLoggin());
            pr.setString(12, d.getRut() + ".jpg");
            //Parametros de la imagen
            File fichero = new File(direccionfirma);
            FileInputStream streamEntrada = new FileInputStream(fichero);
            int tamañoImagen = streamEntrada.available();
            //Establecer los parametros a la BD
            pr.setInt(13, tamañoImagen);
            pr.setBinaryStream(10, streamEntrada, (int) fichero.length());
            pr.setString(11, d.getProfesion());
            if (pr.executeUpdate() == 1) {
                inserto = "Profesional Registrado Exitosamente";
            } else {
                inserto = "Ocurrio un problema !! Intente mas tarde";
            }
        } catch (Exception ex) {
            inserto = ex.getMessage();
        } finally {
            try {
                pr.close();
                cn.close();
                
            } catch (Exception ex) {

            }
        }

        return inserto;

    }

    /*Buscar lista de Doctores*/
    public Vector<doctor> buscarDoctores() {
        Vector<doctor> doct = new Vector<doctor>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from agenda.doctor d where estatus = 1 order by d.nombre ,"
                + "d.apellido_paterno"
                + "");

        this.cnn.conectar();
        try {

            while (this.cnn.getRst().next()) {
                doctor d = new doctor();
                d.setRut(cnn.getRst().getString("rut"));
                d.setNombre(cnn.getRst().getString("nombre"));
                d.setApellido_paterno(cnn.getRst().getString("apellido_paterno"));
                d.setApellido_materno(cnn.getRst().getString("apellido_materno"));
                d.setTelefono(cnn.getRst().getString("telefono"));
                d.setEmail(cnn.getRst().getString("email"));

                doct.add(d);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_doctor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return doct;
    }

    /*buscar doctores para combo*/
    public Vector<doctor> buscarDoctoresporEspecialidad(int es) {
        Vector<doctor> doctores = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT d.rut, (apellido_paterno ||' '|| apellido_materno||' '|| nombre) as nombre  "
                + " FROM  "
                + " agenda.doctor d inner join agenda.doctor_especialidad de on "
                + "  d.rut = de.rut where id_especialidad = " + es + " and d.estatus = 1  order by nombre; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                doctor d = new doctor();
                d.setRut(this.cnn.getRst().getString("rut"));
                d.setNombre(this.cnn.getRst().getString("nombre"));
                doctores.add(d);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_doctor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return doctores;

    }

    /*eliminar especialidades de doctor*/
    public void eliminarEspecialidadesdeDoctor(String rut) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.doctor_especialidad WHERE upper(rut) = upper('" + rut + "');");
        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    public boolean buscardoctor(String rut) {
        boolean encontre = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from agenda.doctor where upper(rut) = upper('" + rut + "');");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                encontre = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_doctor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return encontre;

    }

    //actualizar
    public String actualizarDoctor(doctor d) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE "
                + "  agenda.doctor  "
                + "SET "
                + "  nombre ='" + d.getNombre() + "',"
                + "  apellido_paterno = '" + d.getApellido_paterno() + "',"
                + "  apellido_materno = '" + d.getApellido_materno() + "',"
                + "  telefono = '" + d.getTelefono() + "',"
                + "  email = '" + d.getEmail() + "' ,"
                + "  profesion = '" + d.getProfesion() + "'  "
                + "WHERE rut = '" + d.getRut() + "';");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Profesional Modificado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    //actualizar
    public String actualizarDoctorconFirma(doctor d, String direccionArchivo)  {
        String actualizo = "";
         this.configurarConexion("");
        Connection cn = null;
        PreparedStatement pr = null;
        String sql = "UPDATE \n"
                + "  agenda.doctor  \n"
                + "SET \n"
                + "  nombre = ?,\n"
                + "  apellido_paterno = ?,\n"
                + "  apellido_materno = ?,\n"
                + "  telefono = ?,\n"
                + "  email = ?,\n"
                + "  ipusuario = ?,\n"
                + "  usuario = ?,\n"
                + "  firmadoctor = ?,\n"
                + "  profesion = ?,\n"
                + "  nombrefirma = ?,\n"
                + "  tamanofirma = ?";
        sql += "WHERE rut=?";
        try {
            Class.forName(this.cnn.getDriver());
            cn = DriverManager.getConnection(this.cnn.getNombreBaseDatos(), this.cnn.getUser(), this.cnn.getPassword());
            pr = cn.prepareStatement(sql);
            pr.setString(1, d.getNombre());
            pr.setString(2, d.getApellido_paterno());
            pr.setString(3, d.getApellido_materno());
            pr.setString(4, d.getTelefono());
            pr.setString(5, d.getEmail());
            pr.setString(6, getIpUsuarioLoggin());
            pr.setString(7, getUsuarioLoggin());
            pr.setString(10, d.getRut() + ".jpg");
            //Parametros de la imagen
            File fichero = new File(direccionArchivo);
            FileInputStream streamEntrada = new FileInputStream(fichero);
            int tamañoImagen = streamEntrada.available();
            //Establecer los parametros a la BD
            pr.setInt(11, tamañoImagen);
            pr.setBinaryStream(8, streamEntrada, (int) fichero.length());
            pr.setString(9, d.getProfesion());
            pr.setString(12, d.getRut());
            if (pr.executeUpdate() == 1) {
                actualizo = "Profesional Modificado Exitosamente";
            } else {
                actualizo = "Ocurrio un error!! Intente mas tarde";
            }
        } catch (Exception ex) {
            actualizo = ex.getMessage();
        } finally {
            try {
                pr.close();
                cn.close();
               
            } catch (Exception ex) {

            }
        }

        return actualizo;
    }

//eliminar
    public String eliminar(String rut) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE "
                + "  agenda.doctor  "
                + "SET  estatus = 0 "
                + " WHERE upper( rut) = upper('" + rut + "') "
                + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Profesional Eliminado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    // registrar las especialidades de un doctor
    public void doctor_especialidad(String rut, int[] especialidad) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        for (int i = 0; i < especialidad.length; ++i) {
            if (especialidad[i] != 0) {
                this.cnn.setSentenciaSQL("INSERT INTO "
                        + "  agenda.doctor_especialidad"
                        + "  VALUES ("
                        + "  '" + rut + "',"
                        + "  " + especialidad[i] + " "
                        + ");");
                this.cnn.conectar();
            }
        }
        this.cnn.cerrarConexion();
    }

    public byte[] obtenImagenDoctor(String rut) throws SQLException {

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " *\n"
                + " FROM \n"
                + "  agenda.doctor where rut= '" + rut + "' ;");
        this.cnn.conectar();
        byte[] buffer = null;
        try {

            while (this.cnn.getRst().next()) {

                // Blob bin = rs.getBlob("fotoproducto");
                // if (bin != null) {
                InputStream inStream = this.cnn.getRst().getBinaryStream("firmadoctor");
                int size = this.cnn.getRst().getInt("tamanofirma");
                buffer = new byte[size];
                int length = -1;
                int k = 0;
                try {
                    inStream.read(buffer, 0, size);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                //}
            }
        } catch (Exception ex) {
            return null;
        } finally {
            this.cnn.cerrarConexion();
        }
        return buffer;
    }

}
