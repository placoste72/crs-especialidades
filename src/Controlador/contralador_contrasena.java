/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.sql.SQLException;

/**
 *
 * @author Informatica
 */
public class contralador_contrasena extends General {
    
    // verifico que la contraseña no sea la misma de la vez pasada, ni de la actual
   public boolean verificarcontrasena(String contrasena, String nombreusuario)
   {
       boolean noingresar = false;
       this.configurarConexion("");
       this.cnn.setEsSelect(true);
       this.cnn.setSentenciaSQL("select * from seguridad.usuario where nombre_usuario = '"+nombreusuario+"';");
       
       this.cnn.conectar();
       try{
           if (this.cnn.getRst().next()){
               if((!this.cnn.getRst().getString("contrasena").equals(contrasena)) && (!this.cnn.getRst().getString("viejacontrasena").equals(contrasena)))
                   noingresar = false;else {
                   noingresar= true;
               }
               
               
           }else
               noingresar = false;
           
       }catch(SQLException ex) {
       }
        this.cnn.cerrarConexion();
       return  noingresar;
   }
   
   //ingresar la contraseña actual como vieja
   public void registrarviejacontrasena(String nombreusuario){
       this.configurarConexion("");
       this.cnn.setEsSelect(false);
       this.cnn.setSentenciaSQL("update seguridad.usuario set viejacontrasena = (select contrasena from seguridad.usuario u where u.nombre_usuario='"+nombreusuario+"'),\n" +
                          "fecharegistro = CURRENT_DATE where nombre_usuario='"+nombreusuario+"';");
      this.cnn.conectar();
       this.cnn.cerrarConexion();
      
   }
   
   //registro nueva contraseña y la actual pasa a ser la vieja
   
   public void registrarcontrasena(String contrasena, String nombreusuario){
    
          registrarviejacontrasena(nombreusuario);
          this.configurarConexion("");
          this.cnn.setEsSelect(false);
          this.cnn.setSentenciaSQL("update seguridad.usuario set contrasena = '"+contrasena+"' where nombre_usuario = '"+nombreusuario+"'");
          this.cnn.conectar();
          this.cnn.cerrarConexion();
          
    
       
   }
   
   /*verificar que la contraseña pertenezca al usuario*/
   
   public boolean verificarsiCoinciden( String usuario){
       boolean verifico = false;
          this.configurarConexion("");
       this.cnn.setEsSelect(true);
       this.cnn.setSentenciaSQL("Select * from seguridad.usuario where nombre_usuario= '"+usuario+"'");
       
       this.cnn.conectar();
       try{
           if (this.cnn.getRst().next()){
              
                   verifico = true;
               }
   
       }catch(SQLException ex) {
       } 
       this.cnn.cerrarConexion();
        return verifico;
        
   }
    
}
