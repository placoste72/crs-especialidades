/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.cita;
import Modelos.lugar;
import Modelos.paciente;
import Modelos.protocolo;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ws.cl.gov.fonasa.certificadorprevisional.CargasTO;
import ws.cl.gov.fonasa.certificadorprevisional.CertificadorPrevisionalSoapProxy;
import ws.cl.gov.fonasa.certificadorprevisional.QueryCertificadorPrevisionalTO;
import ws.cl.gov.fonasa.certificadorprevisional.QueryTO;
import ws.cl.gov.fonasa.certificadorprevisional.ReplyCertificadorPrevisionalTO;

/**
 *
 * @author Informatica
 */
public class controlador_paciente extends General {

    public static int canal = 1;
    public static int tipoEmisor = 1;
    public static int tipoUsuario = 1;
    public static int entidad = 65061030;
    public static int claveEntidad = 6506;

    public String guardarPaciente(paciente p) throws UnknownHostException {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO "
                + "  agenda.paciente "
                + "VALUES ("
                + "  '" + p.getRut().toUpperCase() + "',"
                + "  '" + p.getNombre() + "',"
                + "  '" + p.getApellido_paterno() + "',"
                + "  '" + p.getApellido_moderno() + "',"
                + "  '" + p.getFecha_nacimiento() + "',"
                + "  '" + p.getDireccion() + "',"
                + "  '" + p.getEmail() + "',"
                + "  '" + p.getContacto1() + "',"
                + "  '" + p.getContacto2() + "',"
                + "  " + p.getEstatus() + ","
                + "  " + p.getGenero() + ","
                + "   CURRENT_DATE ,"
                + "  " + p.getId_comuna() + ","
                + "  " + p.getProvision() + ","
                + "  " + p.getTramo() + ","
                + "  " + p.getProcedencia() + ", "
                + "  '" + getIpUsuarioLoggin() + "' ,"
                + "   '" + getUsuarioLoggin() + "', "
                + "  " + p.getId_region() + ", "
                + "  " + p.getId_provincia() + ","
                + "  " + p.getIdnacionalidad() + ", "
                + "  '" + p.getNombresocial() + "' "
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Paciente Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*modificar email y telefono de paciente/
    
    
     */
    public void ModificarEmailyContactoPaciente(String rut, String email, String contacto) throws UnknownHostException {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.paciente  \n"
                + "SET \n"
                + "  email = '" + email + "',\n"
                + "  contacto1 = '" + contacto + "',\n"
                + "  ipusuario = '" + getIpUsuarioLoggin() + "',\n"
                + "  usuario =  '" + getUsuarioLoggin() + "'\n"
                + "  \n"
                + "WHERE upper(rut) = upper('" + rut + "');");
        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*buscar los hitos de paciente */
    public Vector<paciente> buscarHitosdePaciente(String rut) {
        Vector<paciente> lista = new Vector<paciente>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("(SELECT  h.fecha as fechadate, to_char(h.fecha, 'DD/MM/YYYY HH24:MI:SS') as fecha,\n"
                + "h.descripcion,\n"
                + " ol.descripcion as llamada ,\n"
                + " oc.descripcion as carta, \n"
                + " oe.descripcion as examen ,\n"
                + " h.usuario,\n"
                + " case when (select nombre from agenda.vistafuncionario vf  where vf.rut_funcionario = h.usuario limit 1) != '' then\n"
                + " (select nombre from agenda.vistafuncionario vf  where vf.rut_funcionario = h.usuario limit 1) else '' end as nombreusuario\n"
                + "  FROM \n"
                + "  agenda.hito h inner join agenda.opcionhito ol \n"
                + "  on h.id_llamada = ol.id_opcion \n"
                + " inner join agenda.opcionhito2 oc on h.id_carta = oc.id_opcion\n"
                + " inner join agenda.opcionhito3 oe on h.id_examen = oe.id_opcion\n"
                + " where  upper(rut_paciente) = upper('" + rut + "')                                             \n"
                + "union\n"
                + " SELECT \n"
                + " fecha_hito_paciente,           \n"
                + " to_char(fecha_hito_paciente,'DD/MM/YYYY HH24:MI:SS') as fecha_hito_paciente, \n"
                + " descripcion_hito_paciente, \n"
                + " (case  WHEN llamada_hito_paciente='0' THEN 'No marca' \n"
                + "  WHEN llamada_hito_paciente='1' THEN 'Llamada exitosa' \n"
                + "  WHEN llamada_hito_paciente='2' THEN 'Llamada realizada, sin contacto' \n"
                + " WHEN llamada_hito_paciente='3' THEN 'Llamada realizada, solicitan llamar nuevamente' \n"
                + "  WHEN llamada_hito_paciente='4' THEN 'Llamada realizada, y paciente rechaza atención' \n"
                + "  else 'Sin' \n"
                + "   end) as llamada,\n"
                + "  (case WHEN carta_hito_paciente='0' THEN 'No marca'  \n"
                + "  WHEN carta_hito_paciente='1' THEN 'Emision de carta certificada'  \n"
                + "  WHEN carta_hito_paciente='2' THEN 'Envio de carta certificado'  \n"
                + "  else 'Sin'  \n"
                + "   end) as carta,\n"
                + " (case WHEN examen_hito_paciente='0' THEN 'No marca' \n"
                + "  WHEN examen_hito_paciente='1' THEN 'Examenes pre operatorios normales (Con Pase)' \n"
                + "  WHEN examen_hito_paciente='2' THEN 'Examenes pre operatorios alterados (Sin Pase)' \n"
                + "  else 'Sin' \n"
                + "  end) as examen ,\n"
                + "  usuario_hito_paciente, \n"
                + "  (USU.nombres_usuario_agenda||' '||USU.apellido_paterno_usuario_agenda) as usuario\n"
                + " \n"
                + "   FROM  schema_agenda.hito_paciente HIT  \n"
                + "  JOIN schema_urgencia.paciente PAC on (HIT.paciente_hito_paciente=PAC.paciente_rut) \n"
                + "  JOIN schema_agenda.usuario_agenda USU on (HIT.usuario_hito_paciente=USU.id_usuario_agenda) \n"
                + " WHERE HIT.paciente_hito_paciente='" + rut.toUpperCase() + "')  order by fechadate  desc  ");
        this.cnn.conectar();
        //utilizo el objeto de paciente para hacer la cosa mas facil pero en realidad son los datos de su historial
        try {
            while (this.cnn.getRst().next()) {
                paciente p = new paciente();
                p.setTemporal3(this.cnn.getRst().getString("fecha"));
                p.setNombre(this.cnn.getRst().getString("descripcion"));
                p.setApellido_moderno(this.cnn.getRst().getString("llamada"));
                p.setApellido_paterno(this.cnn.getRst().getString("carta"));
                p.setDireccion(this.cnn.getRst().getString("examen"));
                p.setTemporal1(this.cnn.getRst().getString("usuario"));
                p.setTemporal2(this.cnn.getRst().getString("nombreusuario"));
                lista.add(p);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;
    }

    /*insertar hitos de pacientes*/
    public String registarhitodepaciente(String descripcion, String rutpaciente, int llamada, int carta, int examen) throws UnknownHostException {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.hito\n"
                + "( fecha,\n"
                + "  descripcion,\n"
                + "  rut_paciente,\n"
                + "  id_llamada,\n"
                + "  id_carta,\n"
                + "  id_examen,\n"
                + "  ipusuario,\n"
                + "  usuario ) \n"
                + "VALUES ( CURRENT_TIMESTAMP,\n"
                + " '" + descripcion + "',\n"
                + "  '" + rutpaciente + "',\n"
                + "  " + llamada + " ,\n"
                + "  " + carta + ",\n"
                + "  " + examen + ",\n"
                + "  '" + getIpUsuarioLoggin() + "' ,\n"
                + "    '" + getUsuarioLoggin() + "' \n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Hito de paciente Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

    /*registrar historial de telefonos y direccion*/
    public String registrarhistorialpaciente(String rut, String dire, String t1, String t2, int comuna, int region, int provincia, String email) throws UnknownHostException {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO"
                + " agenda.historialpaciente"
                + "( rut_paciente,direccion, telefono, telefono2,"
                + " fecha_registro,comuna, estatus,ipusuario,usuario,id_region,id_provincia, email) "
                + "VALUES ('" + rut + "',"
                + " '" + dire + "',"
                + " '" + t1 + "',"
                + " '" + t2 + "',"
                + "   CURRENT_DATE ,"
                + "" + comuna + " ,"
                + " 1 ,"
                + " '" + getIpUsuarioLoggin() + "' ,"
                + " '" + getUsuarioLoggin() + "',"
                + " " + region + " ,"
                + " " + provincia + ","
                + " '" + email + "') ;");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Historial Paciente Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }


    /*buscar paciente por la rut*/
    public paciente buscarpacienteporrut(String rut) throws ParseException {
        paciente p = new paciente();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  to_char(fecha_nacimiento,'DD/MM/YYYY') as nacimiento ,\n"
                + " case when  provision= 1 then 'FONASA'||' '|| t.nombre  else case when provision = 2 then 'Isapre' else case when provision = 3 then 'Prais' else 'Sin Prevision' end  end end as prevension ,\n"
                + "  rut,\n"
                + "  p.nombre,\n"
                + "  apellido_paterno,\n"
                + "  apellido_moderno,\n"
                + "  fecha_nacimiento,\n"
                + "  COALESCE(direccion,'') as direccion,\n"
                + "  COALESCE (email,'') as email,\n"
                + "  COALESCE (contacto1,'') as contacto1,\n"
                + "  COALESCE(contacto2,'') as contacto2 ,\n"
                + "  estatus,\n"
                + "  genero,\n"
                + "  fecha_registro,\n"
                + "  id_comuna,\n"
                + "  provision,\n"
                + "  tramo,\n"
                + "  procedencia,\n"
                + "  ipusuario,\n"
                + "  usuario,\n"
                + "  id_region,\n"
                + "  id_provincia, id_nacionalidad, COALESCE(nombresocial,' ') AS nombresocial FROM  agenda.paciente p\n"
                + "  inner join agenda.tramo t on t.id = p.tramo \n"
                + "  where upper(rut)=upper('" + rut + "') and estatus = 1;  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                p.setRut(this.cnn.getRst().getString("rut"));
                p.setNombre(this.cnn.getRst().getString("nombre"));
                p.setApellido_paterno(this.cnn.getRst().getString("apellido_paterno"));
                p.setApellido_moderno(this.cnn.getRst().getString("apellido_moderno"));
                String fecha = this.cnn.getRst().getString("nacimiento");
                Date fecha1 = DeStringADate(fecha);
                p.setFecha_nacimiento(fecha1);
                p.setDireccion(this.cnn.getRst().getString("direccion"));
                p.setEmail(this.cnn.getRst().getString("email"));
                p.setContacto1(this.cnn.getRst().getString("contacto1"));
                p.setContacto2(this.cnn.getRst().getString("contacto2"));
                p.setEstatus(this.cnn.getRst().getInt("estatus"));
                p.setGenero(this.cnn.getRst().getInt("genero"));
                p.setFecharegistro(this.cnn.getRst().getDate("fecha_registro"));
                p.setId_comuna(this.cnn.getRst().getInt("id_comuna"));
                p.setProvision(this.cnn.getRst().getInt("provision"));
                p.setTramo(this.cnn.getRst().getInt("tramo"));
                p.setProcedencia(this.cnn.getRst().getInt("procedencia"));
                p.setId_region(this.cnn.getRst().getInt("id_region"));
                p.setId_provincia(this.cnn.getRst().getInt("id_provincia"));
                p.setTemporal1(this.cnn.getRst().getString("prevension"));
                p.setIdnacionalidad(this.cnn.getRst().getInt("id_nacionalidad"));
                p.setNombresocial(this.cnn.getRst().getString("nombresocial"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    public void modificardatosdepacienteporqueseingresohito(paciente p) throws UnknownHostException {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.paciente  \n"
                + "SET \n"
                + "   direccion = '" + p.getDireccion() + "',\n"
                + "   contacto1 = '" + p.getContacto1() + "',\n"
                + "    contacto2 = '" + p.getContacto2() + "',\n"
                + "   fecha_registro = CURRENT_DATE,\n"
                + "  id_comuna = " + p.getId_comuna() + ",\n"
                + "  ipusuario = '" + getIpUsuarioLoggin() + "',\n"
                + "  usuario = '" + getUsuarioLoggin() + "',\n"
                + "  id_region = " + p.getId_region() + ",\n"
                + "  id_provincia = " + p.getId_provincia() + ","
                + "   email = '" + p.getEmail() + "'\n"
                + " \n"
                + "WHERE  upper(rut) = upper('" + p.getRut() + "') ;");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*buscar lista de contanto que tuvo en la agenda antigua*/
    public Vector<paciente> buscarHistorialpacienteAntiguos(String rut) {
        Vector<paciente> lista = new Vector();
        lista = buscarHistorialPaciente(rut);
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idfono,\n"
                + "  rutpaciente,\n"
                + "  contacto1,\n"
                + "  contacto2,\n"
                + "  direccion,\n"
                + "  fecha\n"
                + "FROM \n"
                + "  schema_agenda.historialantiguo where upper(rutpaciente) = upper('" + rut + "') ; ");
        this.cnn.conectar();
        //utilizo el objeto de paciente para hacer la cosa mas facil pero en realidad son los datos de su historial
        try {
            while (this.cnn.getRst().next()) {
                paciente p = new paciente();
                p.setEstatus(this.cnn.getRst().getInt("idfono"));
                p.setRut(this.cnn.getRst().getString("rutpaciente"));
                p.setDireccion(this.cnn.getRst().getString("direccion"));
                p.setContacto1(this.cnn.getRst().getString("contacto1"));
                p.setContacto2(this.cnn.getRst().getString("contacto2"));
                p.setTemporal3(this.cnn.getRst().getString("fecha"));

                p.setNombre("");
                p.setApellido_moderno("");
                p.setApellido_paterno("");
                p.setTemporal1("");
                p.setTemporal2("");

                lista.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;
    }

    /*buscar lista de historial de telefonos y direccion del paciente*/
    public Vector<paciente> buscarHistorialPaciente(String rut) {
        Vector<paciente> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  id_historial,\n"
                + "  rut_paciente,\n"
                + "   direccion,\n"
                + " telefono,\n"
                + "   telefono2,\n"
                + "  to_char(hp.fecha_registro, 'dd/mm/yyyy') as fecha_registro  ,hp.fecha_registro,\n"
                + "    comuna,\n"
                + "  c.nombre,\n"
                + " r.nombre as region,\n"
                + "  p.nombre as provincia, hp.usuario,\n"
                + " case when (select nombre from agenda.vistafuncionario vf  where vf.rut_funcionario = hp.usuario limit 1) != '' then \n"
                + " (select nombre from agenda.vistafuncionario vf  where vf.rut_funcionario = hp.usuario limit 1) else '' end as nombreusuario \n"
                + "  \n"
                + " FROM \n"
                + " agenda.historialpaciente hp inner join agenda.comuna c \n"
                + " on c.id_comuna = hp.comuna\n"
                + "  join agenda.region r on hp.id_region = r.id_region\n"
                + "  join agenda.provincia p on hp.id_provincia = p.id_provincia\n"
                + "  where upper(rut_paciente) = upper('" + rut + "') order by  hp.fecha_registro desc ");
        this.cnn.conectar();
        //utilizo el objeto de paciente para hacer la cosa mas facil pero en realidad son los datos de su historial
        try {
            while (this.cnn.getRst().next()) {
                paciente p = new paciente();
                p.setEstatus(this.cnn.getRst().getInt("id_historial"));
                p.setRut(this.cnn.getRst().getString("rut_paciente"));
                p.setDireccion(this.cnn.getRst().getString("direccion"));
                p.setContacto1(this.cnn.getRst().getString("telefono"));
                p.setContacto2(this.cnn.getRst().getString("telefono2"));
                p.setTemporal3(this.cnn.getRst().getString("fecha_registro"));
                p.setGenero(this.cnn.getRst().getInt("comuna"));
                p.setNombre(this.cnn.getRst().getString("nombre"));
                p.setApellido_moderno(this.cnn.getRst().getString("region"));
                p.setApellido_paterno(this.cnn.getRst().getString("provincia"));
                p.setTemporal1(this.cnn.getRst().getString("usuario"));
                p.setTemporal2(this.cnn.getRst().getString("nombreusuario"));

                lista.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;
    }

    /*BUSCAR LISTA DE PACIENTES*/
    public Vector<paciente> buscarPacientes() {
        Vector<paciente> lista = new Vector();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.paciente where estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                paciente p = new paciente();
                p.setRut(this.cnn.getRst().getString("rut"));
                p.setNombre(this.cnn.getRst().getString("nombre"));
                p.setApellido_paterno(this.cnn.getRst().getString("apellido_paterno"));
                p.setApellido_moderno(this.cnn.getRst().getString("apellido_moderno"));
                p.setFecha_nacimiento(this.cnn.getRst().getDate("fecha_nacimiento"));
                p.setDireccion(this.cnn.getRst().getString("direccion"));
                p.setEmail(this.cnn.getRst().getString("email"));
                p.setContacto1(this.cnn.getRst().getString("contacto1"));
                p.setContacto2(this.cnn.getRst().getString("contacto2"));
                p.setEstatus(this.cnn.getRst().getInt("estatus"));
                p.setFecharegistro(this.cnn.getRst().getDate("fecha_registro"));

                lista.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }
    //eliminar

    public String eliminarPaciente(String rut) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE  agenda.paciente  SET estatus = 0 WHERE upper(rut)=upper('" + rut + "') ; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Paciente Eliminado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*buscar su ya el paciente existe*/
    public boolean buscarpaciente(String rut) {
        boolean siexiste = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.paciente where upper(rut)=upper('" + rut + "') and estatus = 1  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                siexiste = true;
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return siexiste;
    }

    /*modificarcontodo*/
    public String modificartodoelPaciente(paciente p) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("UPDATE agenda.paciente  SET nombre = '" + p.getNombre() + "', apellido_paterno = '" + p.getApellido_paterno() + "',apellido_moderno = '" + p.getApellido_moderno() + "',\n"
                + "  fecha_nacimiento = '" + p.getFecha_nacimiento() + "',\n"
                + "  direccion = '" + p.getDireccion() + "',\n"
                + "  email = '" + p.getEmail() + "',\n"
                + "  contacto1 = '" + p.getContacto1() + "',\n"
                + "  contacto2 = '" + p.getContacto2() + "',\n"
                + "  genero = " + p.getGenero() + ",\n"
                + "  id_comuna = " + p.getId_comuna() + ",\n"
                + " provision = " + p.getProvision() + ",\n"
                + " tramo = " + p.getTramo() + ",\n"
                + "  procedencia = " + p.getProcedencia() + " ,"
                + " id_region = " + p.getId_region() + ",\n"
                + "  id_provincia = " + p.getId_provincia() + ","
                + "  id_nacionalidad = " + p.getIdnacionalidad() + ","
                + " nombresocial = '" + p.getNombresocial() + "' "
                + " \n"
                + "WHERE \n"
                + " upper(rut) = upper('" + p.getRut() + "'); ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Paciente Modificado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    // modificar
    public String modificarPaciente(paciente p) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("UPDATE agenda.paciente  SET nombre = '" + p.getNombre() + "', apellido_paterno = '" + p.getApellido_paterno() + "',apellido_moderno = '" + p.getApellido_moderno() + "',\n"
                + "  fecha_nacimiento = '" + p.getFecha_nacimiento() + "',\n"
                + "  direccion = '" + p.getDireccion() + "',\n"
                + "  email = '" + p.getEmail() + "',\n"
                + "  contacto1 = '" + p.getContacto1() + "',\n"
                + "  contacto2 = '" + p.getContacto2() + "',\n"
                + "  genero = " + p.getGenero() + ",\n"
                + "  id_comuna = " + p.getId_comuna() + ",\n"
                + " provision = " + p.getProvision() + ",\n"
                + " tramo = " + p.getTramo() + ",\n"
                + "  procedencia = " + p.getProcedencia() + ""
                + " \n"
                + "WHERE \n"
                + " upper(rut) = upper('" + p.getRut() + "'); ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Paciente Modificado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*buscar paciente por idcita*/
    public paciente buscarpacienteporcita(int id_cita) throws ParseException {
        paciente p = new paciente();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select p.* from agenda.paciente p inner join agenda.cita c\n"
                + " on p.rut = c.rut_paciente where c.id_cita = '" + id_cita + "'; ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                p.setRut(this.cnn.getRst().getString("rut"));
                p.setNombre(this.cnn.getRst().getString("nombre"));
                p.setApellido_paterno(this.cnn.getRst().getString("apellido_paterno"));
                p.setApellido_moderno(this.cnn.getRst().getString("apellido_moderno"));

                p.setDireccion(this.cnn.getRst().getString("direccion"));
                p.setEmail(this.cnn.getRst().getString("email"));
                p.setContacto1(this.cnn.getRst().getString("contacto1"));
                p.setContacto2(this.cnn.getRst().getString("contacto2"));
                p.setEstatus(this.cnn.getRst().getInt("estatus"));
                p.setGenero(this.cnn.getRst().getInt("genero"));
                p.setFecharegistro(this.cnn.getRst().getDate("fecha_registro"));
                p.setId_comuna(this.cnn.getRst().getInt("id_comuna"));
                p.setProvision(this.cnn.getRst().getInt("provision"));
                p.setTramo(this.cnn.getRst().getInt("tramo"));
                p.setProcedencia(this.cnn.getRst().getInt("procedencia"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*Buscar paciente por ficha de odontologia*/
    public paciente buscarpacienteporDiagnostico(int idDiagnostico) throws ParseException {
        paciente p = new paciente();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select * from agenda.paciente p\n"
                + "inner join dental.ficha_odontologica fo \n"
                + "on p.rut = fo.rut_paciente \n"
                + "join dental.diagnostico d on d.id_ficha = fo.id_ficha_odontologica\n"
                + "where d.id_diagnostico= " + idDiagnostico + " ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                p.setRut(this.cnn.getRst().getString("rut"));
                p.setNombre(this.cnn.getRst().getString("nombre"));
                p.setApellido_paterno(this.cnn.getRst().getString("apellido_paterno"));
                p.setApellido_moderno(this.cnn.getRst().getString("apellido_moderno"));

                p.setDireccion(this.cnn.getRst().getString("direccion"));
                p.setEmail(this.cnn.getRst().getString("email"));
                p.setContacto1(this.cnn.getRst().getString("contacto1"));
                p.setContacto2(this.cnn.getRst().getString("contacto2"));
                p.setEstatus(this.cnn.getRst().getInt("estatus"));
                p.setGenero(this.cnn.getRst().getInt("genero"));
                p.setFecharegistro(this.cnn.getRst().getDate("fecha_registro"));
                p.setId_comuna(this.cnn.getRst().getInt("id_comuna"));
                p.setProvision(this.cnn.getRst().getInt("provision"));
                p.setTramo(this.cnn.getRst().getInt("tramo"));
                p.setProcedencia(this.cnn.getRst().getInt("procedencia"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    public paciente BuscarDetallePaciente(String rut) {
        paciente p = new paciente();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + "  p.rut,"
                + "  p.nombre,"
                + "  p.apellido_paterno,"
                + "   p.apellido_moderno,"
                + "   p.fecha_nacimiento,"
                + "   p.direccion,"
                + "   p.email,"
                + "   p.contacto1,"
                + "   p.contacto2,"
                + "  case when p.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,"
                + "   p.fecha_registro,"
                + "   p.id_comuna,"
                + "   case when  p.provision= 1 then 'FONASA' else case when p.provision = 2 then 'Isapre' else case when p.provision = 3 then 'Prais' else 'Sin Prevision' end  end end as prevension ,"
                + "   p.tramo,"
                + "   p.procedencia, t.nombre as tramo_nombre , "
                + "   c.nombre as comuna,"
                + "   pr.nombre_servicio_salud,"
                + "  to_char(age(CURRENT_DATE, p.fecha_nacimiento),'yy')as edad "
                + " FROM "
                + "  agenda.paciente p inner join agenda.tramo t on p.tramo= t.id "
                + "  inner join agenda.comuna c on p.id_comuna = c.id_comuna "
                + "  inner join agenda.procedencia pr on pr.id_servicio_salud = p.procedencia "
                + "  where upper(rut) = upper('" + rut + "') ;");
        

        try {
            this.cnn.conectar();
            if (this.cnn.getRst().next()) {
                p.setRut(this.cnn.getRst().getString("rut"));
                p.setNombre(this.cnn.getRst().getString("nombre"));
                p.setApellido_paterno(this.cnn.getRst().getString("apellido_paterno"));
                p.setApellido_moderno(this.cnn.getRst().getString("apellido_moderno"));
                p.setFecha_nacimiento(this.cnn.getRst().getDate("fecha_nacimiento"));
                p.setDireccion(this.cnn.getRst().getString("direccion"));
                p.setEmail(this.cnn.getRst().getString("email"));
                p.setContacto1(this.cnn.getRst().getString("contacto1"));
                p.setContacto2(this.cnn.getRst().getString("contacto2"));

                p.setTemporal1(this.cnn.getRst().getString("genero"));

                p.setFecharegistro(this.cnn.getRst().getDate("fecha_registro"));
                p.setTemporal2(this.cnn.getRst().getString("prevension"));
                p.setId_comuna(this.cnn.getRst().getInt("id_comuna"));

                p.setTramo(this.cnn.getRst().getInt("tramo"));
                p.setProcedencia(this.cnn.getRst().getInt("procedencia"));
                p.setTemporal3(this.cnn.getRst().getString("procedencia"));
                p.setTemporal4(this.cnn.getRst().getString("tramo_nombre"));
                p.setTemporal5(this.cnn.getRst().getString("comuna"));
                p.setTemporal6(this.cnn.getRst().getString("nombre_servicio_salud"));
                p.setTemporal7(this.cnn.getRst().getString("edad"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*estadistico de informacion del paciente*/
 /*busco todos los pacientes que tuvieron citas en un rango de fecha*/
    public Vector<paciente> BuscarPacientesconCitas() {
        Vector<paciente> vp = new Vector<paciente>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  rut\n"
                + "FROM \n"
                + "  public.rut ; ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                paciente p = new paciente();
                p.setRut(this.cnn.getRst().getString("rut"));

                vp.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    public Vector<lugar> BuscarHitosdeunPacientePorfecha(String rut, Date Fecha, Date Fecha2) {

        Vector<lugar> vl = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("         (SELECT  h.id_hito, case when h.id_llamada <> 0 then ol.descripcion else case when h.id_carta <> 0\n"
                + "                then oc.descripcion else case when h.id_examen <> 0 then oe.descripcion else 'Sin tipo registrado'\n"
                + "                end end end as tipo, h.descripcion , h.fecha\n"
                + "                  FROM \n"
                + "                 agenda.hito h inner join agenda.opcionhito ol\n"
                + "                  on h.id_llamada = ol.id_opcion \n"
                + "              inner join agenda.opcionhito2 oc on h.id_carta = oc.id_opcion\n"
                + "                inner join agenda.opcionhito3 oe on h.id_examen = oe.id_opcion\n"
                + "               where  upper(rut_paciente) = upper('" + rut + "') and h.fecha BETWEEN '" + Fecha + "' and '" + Fecha2 + "'     \n"
                + "               \n"
                + "                                                 \n"
                + "                union\n"
                + "            SELECT \n"
                + "            hit.id_hito_paciente,\n"
                + "                          \n"
                + "                \n"
                + "               \n"
                + "                 (case  WHEN llamada_hito_paciente='0' THEN 'No marca' \n"
                + "                 WHEN llamada_hito_paciente='1' THEN 'Llamada exitosa' \n"
                + "                  WHEN llamada_hito_paciente='2' THEN 'Llamada realizada, sin contacto' \n"
                + "                WHEN llamada_hito_paciente='3' THEN 'Llamada realizada, solicitan llamar nuevamente' \n"
                + "                  WHEN llamada_hito_paciente='4' THEN 'Llamada realizada, y paciente rechaza atención' \n"
                + "                else 'Sin' \n"
                + "                 end) ||' '||\n"
                + "                 (case WHEN carta_hito_paciente='0' THEN 'No marca'  \n"
                + "                 WHEN carta_hito_paciente='1' THEN 'Emision de carta certificada'  \n"
                + "                  WHEN carta_hito_paciente='2' THEN 'Envio de carta certificado' \n"
                + "                  else 'Sin'  \n"
                + "                  end) ||' '||\n"
                + "                 (case WHEN examen_hito_paciente='0' THEN 'No marca' \n"
                + "                  WHEN examen_hito_paciente='1' THEN 'Examenes pre operatorios normales (Con Pase)' \n"
                + "                  WHEN examen_hito_paciente='2' THEN 'Examenes pre operatorios alterados (Sin Pase)' \n"
                + "                 else 'Sin'\n"
                + "                  end) as examen , descripcion_hito_paciente, fecha_hito_paciente\n"
                + "                \n"
                + "               \n"
                + "                  FROM  schema_agenda.hito_paciente HIT  \n"
                + "                 JOIN schema_urgencia.paciente PAC on (HIT.paciente_hito_paciente=PAC.paciente_rut) \n"
                + "               \n"
                + "                WHERE HIT.paciente_hito_paciente='" + rut + "' and fecha_hito_paciente BETWEEN '" + Fecha + "' and '" + Fecha2 + "' )   order by fecha desc ");

        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("id_hito"));
                l.setObservaciones(this.cnn.getRst().getString("tipo"));
                l.setDescripcion(this.cnn.getRst().getString("descripcion"));
                l.setFecha(this.cnn.getRst().getDate("fecha"));
                vl.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vl;
    }

    /*buscar las citas de una fecha y un rut */
    public Vector<cita> CitasdeunPacienteenunaFecha(String rut, Date Fecha, Date Fecha2) {
        Vector<cita> citas = new Vector<cita>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("(SELECT p.fecha,\n"
                + " (d.nombre  ||' ' || d.apellido_paterno  ||' ' || d.apellido_materno) as doctor,\n"
                + "                     e.nombreespecialidad,\n"
                + "                     \n"
                + "                       \n"
                + "                        \n"
                + "                        case when (p.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP' else\n"
                + "                                        case when c.estatus= 1 then 'Cita Asignada' \n"
                + "                                       else case when c.estatus = 0 then 'Cita Cancelada' else \n"
                + "                                         case when c.estatus = 2 then 'Cita Recepcionada' \n"
                + "                                        else case when c.estatus = 3 then 'Cita Cancelada Tens' else\n"
                + "                                           case when c.estatus = 4 then 'Cita Atendida Satisfractoriamente' else\n"
                + "                                         case when c.estatus = 6 then 'Cita Cancelada por Profesional' else \n"
                + "                                           'Cita en Proceso' end end end end end end end as estatus\n"
                + "                       \n"
                + "                              FROM \n"
                + "                                agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "                                join  agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "                                join agenda.especialidades e on p.id_especialidad = e.id_especialidad\n"
                + "                                  join agenda.doctor d on p.rut_doctor = d.rut \n"
                + "                                  \n"
                + "                            \n"
                + "                             where  upper(c.rut_paciente) = upper('" + rut + "') and   p.fecha BETWEEN '" + Fecha + "' and '" + Fecha2 + "'\n"
                + "                           union  \n"
                + "                             SELECT  fecha, doctor, especialidad,\n"
                + "             \n"
                + "                estadocita\n"
                + "               FROM \n"
                + "                 schema_agenda.citasanteriores where upper(rut)= upper('" + rut + "') and  fecha BETWEEN '" + Fecha + "' and '" + Fecha2 + "')\n"
                + "                             \n"
                + "                             \n"
                + "                              order by fecha desc ");

        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setFecha(this.cnn.getRst().getDate("fecha"));
                c.setTemporales(this.cnn.getRst().getString("doctor"));
                c.setTemporales1(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales2(this.cnn.getRst().getString("estatus"));
                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return citas;
    }

    public Vector<protocolo> BuscarOperacionesdeunPacienteenunaFecha(String rut, Date Fecha, Date Fecha2) {
        Vector<protocolo> ops = new Vector<protocolo>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("(select pro.fecha , ( p.codigo || p.descripcion) as prestacion  from pabellon.protocolo pro \n"
                + "                inner join agenda.cita c on pro.idcita = c.id_cita\n"
                + "                join agenda.paciente pac on c.rut_paciente = pac.rut\n"
                + "                join agenda.cita_prestacion cp on cp.id_cita = c.id_cita\n"
                + "                join caja.prestacion p on cp.id_prestacion = p.oid\n"
                + "                where upper(pac.rut)=upper('" + rut + "') and cp.tipo=1 and pro.fecha BETWEEN '" + Fecha + "' and '" + Fecha2 + "'\n"
                + "\n"
                + "union\n"
                + "select po.fecha_creacion_protocolo_operatorio,\n"
                + "              (pf.codigo_grupo_codigos_tablas_prestaciones_fonasa||'.'|| \n"
                + "                pf.codigo_subgrupo_codigos_tablas_prestaciones_fonasa||'.'|| \n"
                + "                 pf.codigo_prestacion_codigos_tablas_prestaciones_fonasa)  ||''|| \n"
                + "                pf.descripcion_codigos_tablas_prestaciones_fonasa as prestacion\n"
                + "               \n"
                + "                 FROM schema_pabellon.protocolo_operatorio PO\n"
                + "                 inner join \n"
                + "              \n"
                + "                  schema_pabellon.protocolo_prestacion  pp  on pp.idprotocolo_operatorio = po.id_protocolo_operatorio\n"
                + "                   inner join \n"
                + "                 schema_util.id_codigos_tablas_prestaciones_fonasa pf \n"
                + "                  on pp.idprestacion_codigo_fonasa = pf.id_codigos_tablas_prestaciones_fonasa  \n"
                + "                 where upper( po.rut_paciente_protocolo_operatorio)  =upper( '" + rut + "' )and fecha_creacion_protocolo_operatorio BETWEEN '" + Fecha + "' and '" + Fecha2 + "')\n"
                + "              order by fecha desc");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                protocolo p = new protocolo();
                p.setPreOperatorio(this.cnn.getRst().getString("prestacion"));
                p.setFechaRegistro(this.cnn.getRst().getDate("fecha"));
                ops.add(p);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return ops;
    }

    public paciente consumirWsFonasa(int rut, String dgv, String rutcompleto) throws ParseException {
        paciente p = new paciente();
        CertificadorPrevisionalSoapProxy cf = new CertificadorPrevisionalSoapProxy();
        QueryCertificadorPrevisionalTO query = new QueryCertificadorPrevisionalTO();
        query.setCanal(canal);
        query.setClaveEntidad(claveEntidad);
        query.setDgvBeneficiario(dgv);
        query.setEntidad(entidad);
        query.setRutBeneficiario(rut);
        QueryTO qto = new QueryTO();
        qto.setTipoEmisor(tipoEmisor);
        qto.setTipoUsuario(tipoUsuario);
        query.setQueryTO(qto);
        ReplyCertificadorPrevisionalTO respuesta = new ReplyCertificadorPrevisionalTO();
        try {
            respuesta = cf.getCertificadoPrevisional(query);
            if (respuesta.getReplyTO() != null) {
                String error = respuesta.getReplyTO().getErrorM();
                if (error.equals("")) {
                    int estado = respuesta.getReplyTO().getEstado();
                    String fecha = respuesta.getReplyTO().getFecha();
                    int ruttr = respuesta.getAfiliadoTO().getRutafili();
                    int rutb = respuesta.getBeneficiarioTO().getRutbenef();
                    int busco = rut;
                    CargasTO[] losValores = new CargasTO[respuesta.getNumeroCarga()];
                    String ap1 = "";
                    String ap2 = "";
                    String nombre = "";
                    String tramo = "";
                    String fechanacimiento = "";
                    String genero = "";
                    /*Isapre*/
                    String isapre = respuesta.getDesIsapre();
                    String codigoisapre = respuesta.getCdgIsapre();
                    /*Bloqueo*/
                    String codigobloqueo = respuesta.getCodcybl();
                    String descripcionbloqueo = respuesta.getCoddesc();
                    /*prais*/
                    String descripprais = respuesta.getDescprais();
                    String codigoprais = respuesta.getCodigoprais();
                    int previcion = 0;

                    String descrip = respuesta.getBeneficiarioTO().getDesNacionalidad();
                    String telefono = respuesta.getBeneficiarioTO().getTelefono();

                    String direccion = respuesta.getBeneficiarioTO().getDireccion();
                    int rutc = 0;
                    respuesta.getListCargas();
                    if (busco == ruttr) {
                        /*es el mismo afiliado*/
                        ap1 = respuesta.getAfiliadoTO().getApell1();
                        ap2 = respuesta.getAfiliadoTO().getApell2();
                        nombre = respuesta.getAfiliadoTO().getNombres();
                        tramo = respuesta.getAfiliadoTO().getTramo();
                        fechanacimiento = respuesta.getAfiliadoTO().getFecnac();
                        genero = respuesta.getAfiliadoTO().getGenero();

                    } else if (busco == rutb) {
                        /*es un beneficiario*/
                        ap1 = respuesta.getBeneficiarioTO().getApell1();
                        ap2 = respuesta.getBeneficiarioTO().getApell2();
                        nombre = respuesta.getBeneficiarioTO().getNombres();
                        tramo = respuesta.getAfiliadoTO().getTramo();
                        genero = respuesta.getBeneficiarioTO().getGenero();
                        fechanacimiento = respuesta.getBeneficiarioTO().getFechaNacimiento();
                    } else {
                        /*cargo*/
                        for (int i = 0; i < losValores.length; i++) {
                            rutc = losValores[i].getRutcarga();
                            if (busco == rutc) {
                                ap1 = losValores[i].getApell1();
                                ap2 = losValores[i].getApell2();
                                nombre = losValores[i].getNombres();
                                tramo = respuesta.getAfiliadoTO().getTramo();
                                genero = losValores[i].getGenero();
                                fechanacimiento = losValores[i].getFecnac();
                            }
                        }
                    }

                    /*1 fonasa
            2 isapre
            3 prais*/
 /*inicializo en false, si tengo coloco en true*/
                    boolean vacio2 = false;
                    for (int k = 0; k < codigoprais.length(); k++) {
                        if (codigoprais.charAt(k) == '1') {
                            vacio2 = true;
                        }
                    }

                    boolean vacio = false;
                    for (int i = 0; i < tramo.length(); i++) {
                        if (tramo.charAt(i) != ' ') {
                            vacio = true;
                        }
                    }
                    boolean vacio1 = false;
                    for (int k = 0; k < codigoisapre.length(); k++) {
                        if (codigoisapre.charAt(k) != ' ') {
                            vacio1 = true;
                        }
                    }

                    if (vacio2 == true) {
                        previcion = 3;

                    } else if (vacio1 == true) {
                        previcion = 2;

                    } else if (vacio == true) {
                        previcion = 1;

                    } else {
                        previcion = 4;

                    }

                    if (error.equalsIgnoreCase("")) {
                        String FEC_NAC_ano = fechanacimiento.substring(0, 4);
                        String FEC_NAC_mes = fechanacimiento.substring(5, 7);
                        String FEC_NAC_dia = fechanacimiento.substring(8, 10);
                        fecha = FEC_NAC_dia + "/" + FEC_NAC_mes + "/" + FEC_NAC_ano;
                        Date fecha1 = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);
                        p.setRut(rutcompleto);
                        p.setNombre(nombre);
                        p.setApellido_paterno(ap1);
                        p.setApellido_moderno(ap2);
                        p.setFecha_nacimiento(fecha1);
                        p.setProvision(previcion);
                        int t = 0;
                        if (previcion == 1) {
                            if (tramo.equalsIgnoreCase(tramo)) {
                                if (tramo.equalsIgnoreCase("A")) {
                                    t = 1;
                                } else if (tramo.equalsIgnoreCase("B")) {
                                    t = 2;
                                } else if (tramo.equalsIgnoreCase("C")) {
                                    t = 3;
                                } else if (tramo.equalsIgnoreCase("D")) {
                                    t = 4;
                                }
                            }
                        } else {
                            t = 0;
                        }
                        int generoe = 0;
                        if (genero.equalsIgnoreCase("F")) {
                            generoe = 1;
                        } else {
                            generoe = 2;
                        }
                        p.setTramo(t);
                        p.setGenero(generoe);
                        p.setContacto1(telefono);
                        p.setEstatus(2);/*pacienteencontrado*/
                        boolean exi = buscarpaciente(rutcompleto);
                        paciente otro = new paciente();
                        if (exi == true) {
                            otro = buscarpacienteporrut(rutcompleto);
                            p.setDireccion(otro.getDireccion());
                            p.setContacto1(otro.getContacto1());
                            p.setContacto2(otro.getContacto2());
                            p.setProcedencia(otro.getProcedencia());
                            p.setId_comuna(otro.getId_comuna());
                            p.setId_region(otro.getId_region());
                            p.setId_provincia(otro.getId_provincia());
                            p.setEmail(otro.getEmail());
                            p.setIdnacionalidad(otro.getIdnacionalidad());
                            p.setNombresocial(otro.getNombresocial());

                        } else {
                            p.setDireccion("");
                            p.setContacto1("");
                            p.setContacto2("");
                            p.setEmail("");
                            p.setIdnacionalidad(213);
                            p.setNombresocial("");
                        }

                    }
                }
            } else {
                boolean exi = buscarpaciente(rutcompleto);
                if (exi == true) {
                    p = buscarpacienteporrut(rutcompleto);

                }
            }

        } catch (RemoteException e) {
            e.printStackTrace();
            boolean exi = buscarpaciente(rutcompleto);
            if (exi == true) {
                p = buscarpacienteporrut(rutcompleto);

            }
        }
        return p;
    }

    /*buscar nacionalidad*/
    public Vector<lugar> buscarNacionalidades() {

        Vector<lugar> nac = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT nac_id, nac_descripcion, nac_estado\n"
                + "  FROM schemaoirs.nacion where nac_estado = 1 order by nac_descripcion;");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("nac_id"));
                l.setDescripcion(this.cnn.getRst().getString("nac_descripcion"));
                nac.add(l);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_paciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return nac;
    }

}
