/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.atencion;
import Modelos.lugar;
import Modelos.solicitudExamenes_Procedimientos;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author a
 */
public class controlador_atencion extends General {

    /*busco los  combos de bazo y aorta*/
    public Vector<lugar> buscarbazoActivos() {

        Vector<lugar> bazos = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idbazo,\n"
                + "  nombre,\n"
                + "  estatus\n"
                + "FROM \n"
                + "  agenda.bazo where estatus =1 ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idbazo"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                bazos.add(l);

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return bazos;
    }

    public int buscaridreceta(int idatencion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        int idreceta = 0;
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idreceta\n"
                + " FROM \n"
                + "  agenda.recetaoftalmologica where  idatencionoftalmologica = " + idatencion + " ;");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                idreceta = this.cnn.getRst().getInt("idreceta");

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return idreceta;
    }

    public int IncrementarIdReceta(int idatencion) {
        this.configurarConexion("");
        int i = 0;
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.recetaoftalmologica\n"
                + "(\n"
                + "  idatencionoftalmologica\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + idatencion + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            i = 1;
        }
        this.cnn.cerrarConexion();
        return i;
    }

    public atencion BuscarquedocumentosTieneAtencion(int atencion) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT ao.solicitudpabellon,\n"
                + "                             ao.sic,\n"
                + "                               case when (select dao.idatencionoftalmologia from agenda.diagnostoco_atencion_oftalmologia dao\n"
                + "                                 where dao.idatencionoftalmologia = " + atencion + " and dao.casoges= 1 and  dao.confirmadescarta= 1 LIMIT 1)=" + atencion + " then\n"
                + "                              true else false end as tengoipd,\n"
                + "                               case when (select dao.idatencionoftalmologia from agenda.diagnostoco_atencion_oftalmologia dao\n"
                + "                                 where dao.idatencionoftalmologia = " + atencion + " and dao.casoges= 1 and dao.confirmadescartadiagnostico= 1 and dao.confirmadescarta= 1 LIMIT 1)= " + atencion + " then\n"
                + "                               true else false end as tengoconstancia ,\n"
                + "                                case when (select dh.idhojadiaria from agenda.idicaciones_hojadiaria dh\n"
                + "                                 where dh.idhojadiaria  = " + atencion + " and dh.tipoatencion= 2 LIMIT 1)= " + atencion + " then\n"
                + "                               true else false end as tengoreceta , case when (select noic.idatencionoftalmologica from agenda.noindicacionescirugia noic\n"
                + "                                              where noic.idatencionoftalmologica  = " + atencion + " LIMIT 1)= " + atencion + " then\n"
                + "                                             true else false end as tengonoindicaciones  ,"
                + "                               case when ( SELECT id_atencion FROM  agenda.exception_garantia where id_atencion = " + atencion + " and tipo = 2 LIMIT 1)= " + atencion + "\n"
                + "                                               then true else false end as tengoexcepcion,"
                + "                  case when (select dao.idatencionoftalmologia from agenda.solicitudexamenes_procedimientooft dao\n"
                + "                                                 where dao.idatencionoftalmologia = " + atencion + " LIMIT 1)=" + atencion + " then true else false end as tengosolicitudexamenes\n"
                + "                              FROM \n"
                + "                               agenda.atencionoftalmologia ao \n"
                + "                              \n"
                + "                               where ao.idatencionoftalmologia =" + atencion + "");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                a.setSolicitudpabellon(this.cnn.getRst().getBoolean("solicitudpabellon"));
                a.setSic(this.cnn.getRst().getInt("sic"));
                a.setContgipd(this.cnn.getRst().getBoolean("tengoconstancia"));
                a.setTengoipd(this.cnn.getRst().getBoolean("tengoipd"));
                a.setTengoreceta(this.cnn.getRst().getBoolean("tengoreceta"));
                a.setTengoindicacionnoquirurjica(this.cnn.getRst().getBoolean("tengonoindicaciones"));
                a.setTengoexcepcion(this.cnn.getRst().getBoolean("tengoexcepcion"));
                a.setTengosolicitu(this.cnn.getRst().getBoolean("tengosolicitudexamenes"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*busco aorta*/
    public Vector<lugar> buscarAortaActivos() {

        Vector<lugar> aorta = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idaorta,\n"
                + "  nombre,\n"
                + "  estatus\n"
                + "FROM \n"
                + "  agenda.aorta where estatus=1 ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idaorta"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                aorta.add(l);

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return aorta;
    }

    public String ingresarAtencionRadiologia(atencion a) throws UnknownHostException {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.atencion_radiologia\n"
                + "(\n"
                + "  idcita,\n"
                + "  fecha_registro,\n"
                + "  higado,\n"
                + "  viabiliar,\n"
                + "  vesicula,\n"
                + "  pancreas,\n"
                + "  idbazo,\n"
                + "  idaorta,\n"
                + "  otrobazo,\n"
                + "  otroaorta,\n"
                + "  conclusion,\n"
                + "  usuario,\n"
                + "  ipusuario,"
                + "  rinones,\n"
                + "  hipotesis_diagnostica,\n"
                + "  medico_solicitante\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + a.getIdcita() + ",\n"
                + "  CURRENT_TIMESTAMP,\n"
                + "  '" + a.getHigado() + "',\n"
                + "  '" + a.getVia_biliar() + "',\n"
                + "  '" + a.getVesicula() + "',\n"
                + "  '" + a.getPancreas() + "',\n"
                + "  " + a.getIdbazo() + ",\n"
                + "  " + a.getIdaorta() + ",\n"
                + "  '" + a.getOtro_bazo() + "',\n"
                + "  '" + a.getOtro_aorta() + "',\n"
                + "  '" + a.getConclusion() + "',\n"
                + "  '" + getUsuarioLoggin() + "',\n"
                + "  '" + getIpUsuarioLoggin() + "',"
                + "   '" + a.getRinones() + "',"
                + "  '" + a.getHipotesis_diagnostica() + "',"
                + " '" + a.getMedico_solicitante() + "'\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion Radiologia Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public atencion BuscarAtencionparaInformedePrestacion(int idatencion) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT pres.codigo ||'- '|| pres.descripcion as prestacion , ar.fecha_registro, p.rut,\n"
                + "p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as paciente,"
                + "to_char(age (CURRENT_DATE, p.fecha_nacimiento),'YY') as edad ,\n"
                + "ar.higado,\n"
                + "ar.viabiliar,\n"
                + "ar.vesicula,\n"
                + "ar.pancreas,"
                + " COALESCE(ar.rinones,'') as rinones,\n"
                + "  COALESCE(ar.hipotesis_diagnostica,'') as hipotesis,\n"
                + "  COALESCE(ar.medico_solicitante,'') as profesionalsolicitante,\n"
                + "case when ar.idbazo = -1 then ar.otrobazo else b.nombre end as bazo,\n"
                + "case when ar.idaorta = -1 then ar.otroaorta else a.nombre end as aorta,\n"
                + "ar.conclusion, d.rut ||'/'|| d.nombre || d.apellido_paterno as doctor , d.profesion\n"
                + "from agenda.atencion_radiologia ar inner join \n"
                + "agenda.cita c on ar.idcita = c.id_cita\n"
                + "join agenda.paciente p on p.rut = c.rut_paciente  \n"
                + "join agenda.bazo b on b.idbazo = ar.idbazo\n"
                + "join agenda.aorta a on a.idaorta = ar.idaorta  \n"
                + "join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "join agenda.planificar pla on pla.id_planificacion = o.id_plani_sobre\n"
                + "join agenda.doctor d on d.rut= pla.rut_doctor\n"
                + "join caja.prestacion pres on pres.oid = pla.id_prestacion\n"
                + "where ar.idatencionradiologia = " + idatencion + " ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setNombrepaciente(this.cnn.getRst().getString("paciente"));
                a.setHigado(this.cnn.getRst().getString("higado"));
                a.setVia_biliar(this.cnn.getRst().getString("viabiliar"));
                a.setVesicula(this.cnn.getRst().getString("vesicula"));
                a.setPancreas(this.cnn.getRst().getString("pancreas"));
                a.setOtro_bazo(this.cnn.getRst().getString("bazo"));
                a.setOtro_aorta(this.cnn.getRst().getString("aorta"));
                a.setFecha_registro(this.cnn.getRst().getDate("fecha_registro"));
                a.setConclusion(this.cnn.getRst().getString("conclusion"));

                a.setRutdoctor(this.cnn.getRst().getString("doctor"));
                a.setPrestacion(this.cnn.getRst().getString("prestacion"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setRinones(this.cnn.getRst().getString("rinones"));
                a.setHipotesis_diagnostica(this.cnn.getRst().getString("hipotesis"));
                a.setMedico_solicitante(this.cnn.getRst().getString("profesionalsolicitante"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*buscar el idatencion por la cita*/
    public int idatencionporCita(int idcita) {
        int id = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idatencionradiologia\n"
                + "  FROM \n"
                + "  agenda.atencion_radiologia where idcita = " + idcita + " ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                id = this.cnn.getRst().getInt("idatencionradiologia");

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return id;
    }

    /*buscar idatencion de la oftalmologia*/
    public int idatencionporCitaoftalmologia(int idcita) {
        int id = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idatencionoftalmologia\n"
                + "  FROM \n"
                + "  agenda.atencionoftalmologia where idcita=" + idcita + " ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                id = this.cnn.getRst().getInt("idatencionoftalmologia");

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return id;
    }

    public Vector<atencion> BuscarparaEstadisticodeLentes(Date fecha1, Date fecha2, String especialidad, String doctor) {
        String query_consultas = "";
        Vector<atencion> va = new Vector<atencion>();
        if ((!especialidad.equalsIgnoreCase("-1")) && (doctor.equalsIgnoreCase("-1"))) {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.estadisticodelentes  where fecha BETWEEN '" + fecha1 + "' and '" + fecha2 + "' and idespecialidad=" + especialidad + "  order by fecha desc;";
        } else if (!doctor.equalsIgnoreCase("-1")) {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.estadisticodelentes  where fecha BETWEEN '" + fecha1 + "' and '" + fecha2 + "' and idespecialidad=" + especialidad + " and  rutdoctor= '" + doctor + "' order by fecha desc ;";
        } else {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.estadisticodelentes  where fecha BETWEEN '" + fecha1 + "' and '" + fecha2 + "' order by fecha desc ;";

        }

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(query_consultas);
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setFormatofecha(this.cnn.getRst().getString("fechastring"));
                a.setRutpaciente(this.cnn.getRst().getString("rutpaciente"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombrepaaciente"));
                a.setRutdoctor(this.cnn.getRst().getString("nombreprofesional"));
                a.setEspecialidad(this.cnn.getRst().getString("nombreespecialidad"));
                a.setTratamientocerca(this.cnn.getRst().getInt("tratamientocerca"));
                a.setTratamientolejos(this.cnn.getRst().getInt("tratamientolejos"));
                va.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    public void modificarTratamientodeLejosyCerca(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.atencionoftalmologia  \n"
                + "SET \n"
                + "\n"
                + "  tratamientocerca = " + a.getTratamientocerca() + ",\n"
                + "  tratamientolejos = " + a.getTratamientolejos() + "\n"
                + " \n"
                + "WHERE \n"
                + "idatencionoftalmologia= " + a.getIdatencion() + "\n"
                + ";");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    public String registrarAtencionOftalmologo(atencion a) throws UnknownHostException {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.atencionoftalmologia\n"
                + "(\n"
                + "  idcita,\n"
                + "  fecharegistro,\n"
                + "  usuario,\n"
                + "  ipusuario,\n"
                + "  resumenatenicon,\n"
                + "  otrodiagnostico,\n"
                + "  otroexamenes,\n"
                + "  sic,\n"
                + "  iddestinopaciente,\n"
                + "  otrodestino,\n"
                + "  indicalente,\n"
                + "  cantidadlente,\n"
                + "  otrasindicaciones,\n"
                + "  solicitudpabellon,\n"
                + "  estatus,\n"
                + "  detallederivacion,\n"
                + "  diagnosticosic,\n"
                + "  otrodiagnosticosic,\n"
                + "  otroprocedimiento, pertinencia,\n"
                + "  indicaciones_alta"
                + ") \n"
                + "VALUES (\n"
                + "  " + a.getIdcita() + ",\n"
                + "  CURRENT_TIMESTAMP,\n"
                + "  '" + getUsuarioLoggin() + "',\n"
                + "  '" + getIpUsuarioLoggin() + "',\n"
                + "  '" + a.getResumenatenicon() + "',\n"
                + "  '" + a.getOtrodiagnostico() + "',\n"
                + "  '" + a.getOtroexamenes() + "',\n"
                + "  " + a.getSic() + ",\n"
                + "  " + a.getIddestinopaciente() + ",\n"
                + " '" + a.getOtrodestino() + "' ,\n"
                + " " + a.getIndicalente() + " ,\n"
                + "  " + a.getCantidadlente() + ",\n"
                + "  '" + a.getOtrasindicaciones() + "',\n"
                + "  '" + a.isSolicitudpabellon() + "',\n"
                + "  " + a.getEstatus() + ",\n"
                + "  '" + a.getDetallederivacion() + "',\n"
                + "  " + a.getDiagnosticosic() + ",\n"
                + "  '" + a.getOtrodiagnosticosic() + "',\n"
                + "  '" + a.getOtroprocedimiento() + "' ,"
                + "  " + a.getPertinencia() + ", \n"
                + "  '" + a.getIndialt() + "' \n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion  Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

    /*modificar atencion oftalmologia */
    public void modificaratencionoftalmologia(atencion a) {
         String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.atencionoftalmologia\n"
                + "   SET   \n"
                + "       resumenatenicon='" + a.getResumenatenicon() + "', otrodiagnostico='" + a.getOtrodiagnostico() + "', otroexamenes='" + a.getOtroexamenes() + "', \n"
                + "       sic=" + a.getSic() + ", iddestinopaciente=" + a.getIddestinopaciente() + ", otrodestino='" + a.getOtrodestino() + "', \n"
                + "       otrasindicaciones='" + a.getOtrasindicaciones() + "', solicitudpabellon='" + a.isSolicitudpabellon() + "', detallederivacion='" + a.getDetallederivacion() + "', \n"
                + "       diagnosticosic=" + a.getDiagnosticosic() + ", otrodiagnosticosic='" + a.getOtrodiagnosticosic() + "', otroprocedimiento='" + a.getOtroprocedimiento() + "', \n"
                + "       pertinencia=" + a.getPertinencia() + ", tratamientocerca=" + a.getTratamientocerca() + ", tratamientolejos=" + a.getTratamientolejos() + ", indicaciones_alta='"+ a.getIndialt() + "'\n"
                + " WHERE idatencionoftalmologia=" + a.getIdatencion() + ";");

        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion  Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

    }

    /*elimino todos los tablas de relaciones para volver a ingresaar */
    public void eliminardiagnosicosoftalmologia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.diagnostoco_atencion_oftalmologia \n"
                + " WHERE idatencionoftalmologia=" + a.getIdatencion() + ";");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*eliminar solicitudes dequirofano*/
    public void eliminardsolicitudquirofanooftalmologia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.solicitudquirofanohojadiaria\n"
                + " WHERE idhojadiaria=" + a.getIdatencion() + " and tipoatencion=2;");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*eliminar excepcion de gerantia*/
    public void eliminarlasExcepciondegaratia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.exception_garantia\n"
                + " WHERE id_atencion =" + a.getIdatencion() + " and tipo = 2;");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*eliminar no solicitudes quirurgicas*/
    public void eliminarlasNOsolicitudquirofanooftalmologia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.noindicacionescirugia\n"
                + " WHERE idatencionoftalmologica = " + a.getIdatencion() + " ;");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*eliminar receta de lentes*/
    public void eliminarlasrecetadeLENTEoftalmologia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL(" DELETE FROM agenda.receta_lente_opticos\n"
                + " WHERE tipo = 1 and  id_atencion =" + a.getIdatencion() + ";    ");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*Eliminar procedimientos */
    public void eliminarlasprocedimientosOftalmologia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.procedimiento_hojadiaria\n"
                + " WHERE idhojadiaria = " + a.getIdatencion() + " and tipoatencion= 2;");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*Eliminar examenes de oftalmologia*/
    public void eliminarlasExamenesOftalmologia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.hojadiaria_examenes\n"
                + " WHERE idhojadiaria = " + a.getIdatencion() + " and tipoatencion=2;");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*Eliminar indicaciones de oftalmologia*/
    public void eliminarlasIndicacionesOftalmologia(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.idicaciones_hojadiaria\n"
                + " WHERE  idhojadiaria=" + a.getIdatencion() + " and tipoatencion=2;");

        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    public void modificarquetengosolicituddeQuirofano(int idatencion, boolean tiene) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.atencionoftalmologia\n"
                + "   SET  solicitudpabellon='" + tiene + "'\n"
                + " WHERE idatencionoftalmologia = " + idatencion + ";");

        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*eliminar los detalles de diagnsticos y procedimientos*/
    public void eliminardetalleExamanenes_procedimiento(int solicitud) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.detallesolicitudexamen_procedimientooft dp\n"
                + "WHERE dp.idsolicitud = " + solicitud + " ");

        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*eliminar solicitud examenes y procedimientos*/
    public void eliminarSoliitudExamanenes_procedimiento(int atencion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.solicitudexamenes_procedimientooft\n"
                + " WHERE idatencionoftalmologia = " + atencion + " ; ");

        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*eliminar control de oftalmologia*/
    public void eliminarSolicitudControlOftalmologia(int cita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.solicitud_control\n"
                + " WHERE idcita = " + cita + "; ");

        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    public void modificarDetalleExamane(solicitudExamenes_Procedimientos a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.solicitudexamenes_procedimientooft  \n"
                + "SET "
                + "observacion = '" + a.getObservacion() + "'\n"
                + "WHERE \n"
                + " idsolicitud = " + a.getIdsolicitud() + "; ");

        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*modificar solicitud de control*/
    public void modificarSolicituddeControl(atencion a) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.solicitud_control  \n"
                + "SET \n"
                + "  fecha_control = '" + a.getFecha_registro() + "',\n"
                + "  motivo_control = '" + a.getDetallereceta() + "'\n"
                + " \n"
                + "WHERE idcontrol= " + a.getIdatencion() + "\n"
                + "; ");

        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }


    /*insertar diagnostico atencion*/
    public int diagnosticooftalmologia(atencion a) {
        int mensaje = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.diagnostoco_atencion_oftalmologia\n"
                + "(\n"
                + "  idatencionoftalmologia,\n"
                + "  iddiagnostico,\n"
                + "  idlateralidad,\n"
                + "  casoges,\n"
                + "  confirmadescarta,\n"
                + "  tratamiento,\n"
                + "  fundamento,"
                + "  confirmadescartadiagnostico\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + a.getIdatencion() + ",\n"
                + "  " + a.getIddiagnostico() + ",\n"
                + "  " + a.getLateralidad() + ",\n"
                + "  " + a.getGes() + ",\n"
                + "  " + a.getConfirmadescartadocumentos() + ",\n"
                + "  '" + a.getTratamientoeindicaciones() + "',\n"
                + "  '" + a.getFundamentodeldiagnostico() + "',"
                + "  '" + a.getConfirmadescartadiagnostico() + "'     \n"
                + ");");
        try {
            this.cnn.conectar();
            if (this.cnn.getResultadoSQL() == 1) {
                mensaje = 1;
            }
        } catch (Exception ex) {

        } finally {
            this.cnn.cerrarConexion();
        }
        return mensaje;
    }

    /*para informe de atencion */
    public atencion BuscarAtencionparaInformedePrestacionOftalmologia(int idatencion) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "   ao.fecharegistro,\n"
                + "   p.rut,\n"
                + "   p.nombre || ' '||p.apellido_paterno ||' '||p.apellido_moderno as paciente,\n"
                + "   to_char(age(now(),p.fecha_nacimiento),'yy') as edad,\n"
                + "   ao.resumenatenicon,\n"
                + "   case when ao.indicalente=1 then 'SI INDICO' ||' '|| ao.cantidadlente else 'NO INDICO' end as indicalente,\n"
                + "   d.nombredestino_paciente as destino,\n"
                + "   doc.rut ||'/'|| doc.nombre ||' '||doc.apellido_paterno as profesional,\n"
                + "   doc.profesion, ao.indicalente as il , pla.id_atencion,ao.iddestinopaciente,\n"
                + "                   (select pres.codigo ||' '|| pres.descripcion from caja.prestacion as pres where pres.oid= pla.id_prestacion) as prestacion\n"
                + "FROM \n"
                + "  agenda.atencionoftalmologia ao \n"
                + "  inner join agenda.cita c\n"
                + "  on ao.idcita = c.id_cita\n"
                + "  join agenda.oferta o\n"
                + "  on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar pla\n"
                + "  on pla.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.paciente p  on p.rut = c.rut_paciente\n"
                + "  join agenda.destinopaciente d \n"
                + "  on d.iddestino_paciente = ao.iddestinopaciente\n"
                + "  join agenda.doctor doc on doc.rut = pla.rut_doctor where \n"
                + "   ao.idatencionoftalmologia = " + idatencion + " \n"
                + "  ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                a.setFecha_registro(this.cnn.getRst().getDate("fecharegistro"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setNombrepaciente(this.cnn.getRst().getString("paciente"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setResumenatenicon(this.cnn.getRst().getString("resumenatenicon"));
                a.setIndicalenteS(this.cnn.getRst().getString("indicalente"));
                a.setDetallederivacion(this.cnn.getRst().getString("destino"));
                a.setRutdoctor(this.cnn.getRst().getString("profesional"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));
                a.setIndicalente(this.cnn.getRst().getInt("il"));
                a.setIdatencion(this.cnn.getRst().getInt("id_atencion"));
                a.setPrestacion(this.cnn.getRst().getString("prestacion"));
                a.setIddestinopaciente(this.cnn.getRst().getInt("iddestinopaciente"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*Documentos de Atencion de Oftalmologia*/
    public atencion BuscarSolicitudInterconsultaOftalmologia(int idatencion) {

        atencion ant = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select 'SIC-'|| to_char(ao.fecharegistro, 'yyyymmdd')|| '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'||  case when char_length(ao.idatencionoftalmologia)>4 then cast(ao.idatencionoftalmologia as text) else to_char(ao.idatencionoftalmologia, '0000') end  as folio, \n"
                + "         to_char(ao.fecharegistro,'dd-mm-yyyy HH24:MI:SS')  as fecha, \n"
                + "                  pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombre  \n"
                + "                  , pac.rut, \n"
                + "                  to_char( pac.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,\n"
                + "                  to_char(age(pac.fecha_nacimiento),'yy') as edad,   \n"
                + "                  case when pac.genero =1 then 'Femenino'else 'Masculino' end as sexo,\n"
                + "                   pac.direccion,  \n"
                + "                   co.nombre as comuna, \n"
                + "                   pac.contacto1, \n"
                + "                   case when pac.email != null then pac.email else 'Sin Correo' end as email , \n"
                + "                  case when ao.iddestinopaciente = 0 then ao.otrodestino else dp.nombredestino_paciente end as detinopaciente,\n"
                + "                  ao.detallederivacion,\n"
                + "                  case when ao.diagnosticosic = -1 then ao.otrodiagnosticosic else (select d.nombre_diagnostico from agenda.diagnostico_especialidad de\n"
                + "                   inner join agenda.diagnosticos d\n"
                + "                  on de.iddiagnostico= d.iddiagnostico where de.iddiagnodtivo_especialidad= ao.diagnosticosic) end as daignosticosic,\n"
                + "                 d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor, \n"
                + "                  d.rut as rutdoctor ,  \n"
                + "                    e.nombreespecialidad\n"
                + "                    \n"
                + "                  FROM \n"
                + "                  agenda.atencionoftalmologia ao inner join \n"
                + "                  agenda.destinopaciente dp on dp.iddestino_paciente = ao.iddestinopaciente\n"
                + "                 join agenda.cita c on c.id_cita = ao.idcita\n"
                + "                  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "                  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "                  join agenda.especialidades e on e.id_especialidad= p.id_especialidad\n"
                + "                  join agenda.doctor d on d.rut = p.rut_doctor \n"
                + "                 join agenda.paciente pac on pac.rut = c.rut_paciente\n"
                + "                 join agenda.comuna co on co.id_comuna = pac.id_comuna   where ao.idatencionoftalmologia= " + idatencion + "");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                ant.setFolio(this.cnn.getRst().getString("folio"));
                ant.setFormatofecha(this.cnn.getRst().getString("fecha"));
                ant.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                ant.setRutpaciente(this.cnn.getRst().getString("rut"));
                ant.setEdadpaciente(this.cnn.getRst().getString("edad"));
                ant.setSexo(this.cnn.getRst().getString("sexo"));
                ant.setDomicilio(this.cnn.getRst().getString("direccion"));
                ant.setComuna(this.cnn.getRst().getString("comuna"));
                ant.setTelefono(this.cnn.getRst().getString("contacto1"));
                ant.setCorreo_electronico(this.cnn.getRst().getString("email"));
                ant.setOtrodestino(this.cnn.getRst().getString("detinopaciente"));
                ant.setDetallederivacion(this.cnn.getRst().getString("detallederivacion"));
                ant.setOtrodiagnosticosic(this.cnn.getRst().getString("daignosticosic"));
                ant.setUsuario(this.cnn.getRst().getString("nombredoctor"));
                ant.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                ant.setProfesion(this.cnn.getRst().getString("nombreespecialidad"));
                ant.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return ant;

    }

    public Vector<atencion> buscarInformacionparaConstanciaGeseIpd(int idatencion) {
        Vector<atencion> ats = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "'IPD-'|| to_char( ao.fecharegistro, 'yyyymmdd')|| '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'||  case when char_length(ao.idatencionoftalmologia)>4 then cast(ao.idatencionoftalmologia as text) else to_char(ao.idatencionoftalmologia, '0000') end  as folioipd,\n"
                + "  'CONSTGES-'|| to_char( ao.fecharegistro, 'yyyymmdd') || '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'||  case when char_length(ao.idatencionoftalmologia)>4 then cast(ao.idatencionoftalmologia as text) else to_char(ao.idatencionoftalmologia, '0000') end   as foliocosg,\n"
                + " to_char(ao.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha ,\n"
                + "\n"
                + " d.nombre_diagnostico,\n"
                + "  case when idlateralidad= 1 then 'ojo Derecho' else case when idlateralidad = 2 then 'ojo Izquierdo' else ' Ambos ojos'end end as lateralidad,\n"
                + "  case when dao.casoges = 1 then 'si' else 'no' end as ges,\n"
                + "  case when dao.confirmadescartadiagnostico = 1 then 'Confirma' else 'Descarta' end as cd,\n"
                + "  tratamiento,\n"
                + "  fundamento,\n"
                + "  dao.casoges,\n"
                + "  dao.confirmadescarta,\n"
                + "  pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombre   \n"
                + "   , pac.rut, to_char( pac.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,\n"
                + "  to_char(age(pac.fecha_nacimiento),'yy') as edad,  \n"
                + "   case when pac.genero =1 then 'Femenino'else 'Masculino' end as sexo, pac.direccion,\n"
                + "   pac.contacto1 , pac.email, e.nombreespecialidad, \n"
                + "  doc.nombre ||' '|| doc.apellido_paterno ||' '|| doc.apellido_materno as nombredoctor, \n"
                + "    doc.rut as rutdoctor , doc.profesion\n"
                + "FROM \n"
                + "  agenda.diagnostoco_atencion_oftalmologia dao \n"
                + "  inner join agenda.diagnostico_especialidad de\n"
                + "  on dao.iddiagnostico = de.iddiagnodtivo_especialidad\n"
                + "  join agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico\n"
                + "  join agenda.atencionoftalmologia ao on ao.idatencionoftalmologia = dao.idatencionoftalmologia\n"
                + "  join agenda.cita c on c.id_cita = ao.idcita\n"
                + "  join agenda.paciente pac on c.rut_paciente = pac.rut\n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.doctor doc on doc.rut = p.rut_doctor \n"
                + "  join agenda.especialidades e on e.id_especialidad = p.id_especialidad"
                + "  where dao.idatencionoftalmologia=" + idatencion + " and  confirmadescarta=1 and dao.casoges = 1;");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setFolio(this.cnn.getRst().getString("folioipd"));
                a.setConclusion(this.cnn.getRst().getString("foliocosg"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setOtrodiagnostico(this.cnn.getRst().getString("nombre_diagnostico"));
                a.setLateralidads(this.cnn.getRst().getString("lateralidad"));
                a.setCasoges(this.cnn.getRst().getString("ges"));
                a.setConfirmadescartadocumentos(this.cnn.getRst().getInt("confirmadescarta"));
                a.setGes(this.cnn.getRst().getInt("casoges"));
                a.setFundamentodeldiagnostico(this.cnn.getRst().getString("fundamento"));
                a.setTratamientoeindicaciones(this.cnn.getRst().getString("tratamiento"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setSexo(this.cnn.getRst().getString("sexo"));
                a.setDomicilio(this.cnn.getRst().getString("direccion"));
                a.setTelefono(this.cnn.getRst().getString("contacto1"));
                a.setCorreo_electronico(this.cnn.getRst().getString("email"));
                a.setEspecialidad(this.cnn.getRst().getString("nombreespecialidad"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setMedico_solicitante(this.cnn.getRst().getString("nombredoctor"));
                a.setConf(this.cnn.getRst().getString("cd"));
                ats.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return ats;
    }

    /*para las constancias ges */
    public Vector<atencion> buscarInformacionparaConstanciaGese(int idatencion) {
        Vector<atencion> ats = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "'IPD-'|| to_char( ao.fecharegistro, 'yyyymmdd')|| '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'|| ao.idatencionoftalmologia  as folioipd,\n"
                + "  'CONSTGES-'|| to_char( ao.fecharegistro, 'yyyymmdd') || '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'|| ao.idatencionoftalmologia  as foliocosg,\n"
                + " to_char(ao.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha ,\n"
                + "\n"
                + " d.nombre_diagnostico,\n"
                + "  case when idlateralidad= 1 then 'ojo Derecho' else case when idlateralidad = 2 then 'ojo Izquierdo' else ' Ambos ojos'end end as lateralidad,\n"
                + "  case when dao.casoges = 1 then 'si' else 'no' end as ges,\n"
                + "  case when dao.confirmadescartadiagnostico = 1 then 'Confirma' else 'Descarta' end as cd,\n"
                + "  tratamiento,\n"
                + "  fundamento,\n"
                + "  dao.casoges,\n"
                + "  dao.confirmadescarta,\n"
                + "  pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombre   \n"
                + "   , pac.rut, to_char( pac.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,\n"
                + "  to_char(age(pac.fecha_nacimiento),'yy') as edad,  \n"
                + "   case when pac.genero =1 then 'Femenino'else 'Masculino' end as sexo, pac.direccion,\n"
                + "   pac.contacto1 , pac.email, e.nombreespecialidad, \n"
                + "  doc.nombre ||' '|| doc.apellido_paterno ||' '|| doc.apellido_materno as nombredoctor, \n"
                + "    doc.rut as rutdoctor , doc.profesion\n"
                + "FROM \n"
                + "  agenda.diagnostoco_atencion_oftalmologia dao \n"
                + "  inner join agenda.diagnostico_especialidad de\n"
                + "  on dao.iddiagnostico = de.iddiagnodtivo_especialidad\n"
                + "  join agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico\n"
                + "  join agenda.atencionoftalmologia ao on ao.idatencionoftalmologia = dao.idatencionoftalmologia\n"
                + "  join agenda.cita c on c.id_cita = ao.idcita\n"
                + "  join agenda.paciente pac on c.rut_paciente = pac.rut\n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.doctor doc on doc.rut = p.rut_doctor \n"
                + "  join agenda.especialidades e on e.id_especialidad = p.id_especialidad"
                + "  where dao.idatencionoftalmologia=" + idatencion + " and  confirmadescarta=1 and  confirmadescarta=1 and dao.casoges = 1 and dao.confirmadescartadiagnostico=1;");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setFolio(this.cnn.getRst().getString("folioipd"));
                a.setConclusion(this.cnn.getRst().getString("foliocosg"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setOtrodiagnostico(this.cnn.getRst().getString("nombre_diagnostico"));
                a.setLateralidads(this.cnn.getRst().getString("lateralidad"));
                a.setCasoges(this.cnn.getRst().getString("ges"));
                a.setConfirmadescartadocumentos(this.cnn.getRst().getInt("confirmadescarta"));
                a.setGes(this.cnn.getRst().getInt("casoges"));
                a.setFundamentodeldiagnostico(this.cnn.getRst().getString("fundamento"));
                a.setTratamientoeindicaciones(this.cnn.getRst().getString("tratamiento"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setSexo(this.cnn.getRst().getString("sexo"));
                a.setDomicilio(this.cnn.getRst().getString("direccion"));
                a.setTelefono(this.cnn.getRst().getString("contacto1"));
                a.setCorreo_electronico(this.cnn.getRst().getString("email"));
                a.setEspecialidad(this.cnn.getRst().getString("nombreespecialidad"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setMedico_solicitante(this.cnn.getRst().getString("nombredoctor"));
                a.setConf(this.cnn.getRst().getString("cd"));
                ats.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return ats;
    }

    public Vector<lugar> buscarLasSolicitudesdePabellondeyunaAtencionOftalmologia(int idatencion) {
        Vector<lugar> vl = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT    idhojadiaria,sq.iddiagnosticoespecialidad,\n"
                + "                  case when sq.iddiagnosticoespecialidad <> '-1'\n"
                + "                  then ( select d.nombre_diagnostico from\n"
                + "                  agenda.diagnostico_especialidad de \n"
                + "                  join agenda.diagnosticos d on de.iddiagnostico = d.iddiagnostico\n"
                + "                  where de.iddiagnodtivo_especialidad = sq.iddiagnosticoespecialidad) \n"
                + "                  else sq.otrodiagnosticosolicitud  end as diagnostico,\n"
                + "                   sq.intervencion,\n"
                + "                  ta.nombre,\n"
                + "                  idanastesia,\n"
                + "                  requerimiento,\n"
                + "                  observacion,"
                + "                  case when sq.lateralidad = 1 then 'ojo Derecho' else case when  sq.lateralidad = 2 then 'ojo Izquierdo' else 'ambos ojos' end  end  as lateralidad\n"
                + "                  FROM \n"
                + "                  agenda.solicitudquirofanohojadiaria sq inner join\n"
                + "                  pabellon.tipoanestesia ta on ta.idtipoanestesia = sq.idanastesia\n"
                + "                  \n"
                + "                  where idhojadiaria =" + idatencion + " and sq.tipoatencion= 2 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idhojadiaria"));

                l.setComuna(this.cnn.getRst().getString("diagnostico"));
                l.setIntervencio(this.cnn.getRst().getString("intervencion"));
                l.setId_lugar(this.cnn.getRst().getInt("idanastesia"));
                l.setDiagnostico(this.cnn.getRst().getString("requerimiento"));
                l.setIndicaciones(this.cnn.getRst().getString("observacion"));
                l.setDireccion(this.cnn.getRst().getString("nombre"));
                l.setDescripcion(this.cnn.getRst().getString("lateralidad"));
                vl.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vl;
    }

    public atencion EncabezadoparaIntervencion(int idatencion) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "   ao.fecharegistro, to_char(ao.fecharegistro,'dd-mm-yyyy') as fecha ,\n"
                + "   pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombre   \n"
                + "   , pac.rut, to_char(age(now(),pac.fecha_nacimiento),'yy')as edad, \n"
                + "    doc.nombre ||' '|| doc.apellido_paterno ||' '|| doc.apellido_materno as nombredoctor, \n"
                + "    doc.rut as rutdoctor , doc.profesion, ao.otrodiagnostico\n"
                + "   FROM \n"
                + "  agenda.atencionoftalmologia ao \n"
                + "  join agenda.cita c on c.id_cita = ao.idcita\n"
                + "  join agenda.paciente pac on c.rut_paciente = pac.rut\n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.doctor doc on doc.rut = p.rut_doctor \n"
                + "  \n"
                + "  where ao.idatencionoftalmologia =" + idatencion + ";");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setMedico_solicitante(this.cnn.getRst().getString("nombredoctor"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setOtrodiagnostico(this.cnn.getRst().getString("otrodiagnostico"));
                a.setFecha_registro(this.cnn.getRst().getDate("fecharegistro"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*detalle de la atencion diagnostico, procedimiento, indicaciones y examenes*/
    public Vector<atencion> BuscarDiagnosticosdeAtencionOftalmologica(int idatencion) {
        Vector<atencion> va = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " d.nombre_diagnostico,\n"
                + "  case when idlateralidad= 1 then 'OJO DERECHO' else case when idlateralidad= 2 then 'OJO IZQUIERDO' else 'AMBOS OJOS' end end as lateralidad ,\n"
                + "  case when dao.casoges= 1 then 'ES GES ' else 'NO ES GES' end as ges,"
                + "  case when dao.confirmadescartadiagnostico= 1 then 'CONFIRMA' else case when dao.confirmadescartadiagnostico= 2 then 'DESCARTA' else '' end end as conf\n"
                + "FROM \n"
                + "  agenda.diagnostoco_atencion_oftalmologia dao inner join \n"
                + "  agenda.diagnostico_especialidad de on dao.iddiagnostico= de.iddiagnodtivo_especialidad\n"
                + "  join agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico where dao.idatencionoftalmologia=" + idatencion + ";");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion aten = new atencion();
                aten.setOtrodiagnostico(this.cnn.getRst().getString("nombre_diagnostico"));
                aten.setLateralidads(this.cnn.getRst().getString("lateralidad"));
                aten.setCasoges(this.cnn.getRst().getString("ges"));
                aten.setConf(this.cnn.getRst().getString("conf"));

                va.add(aten);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    /*buscar los procedimientos*/
    public Vector<atencion> BuscarProcedimientosdeAtencionOftalmologica(int idatencion) {
        Vector<atencion> va = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "p.nombre_procedimiento    \n"
                + "FROM \n"
                + "  agenda.procedimiento_hojadiaria ph inner join \n"
                + "  agenda.procedimiento_especialidad pe on ph.idprocedimiento_especialidad = pe.idprocedimiento_especialidad\n"
                + "  join agenda.procedimiento p on pe.idprocedimiento = p.idprocedimiento\n"
                + "  where ph.tipoatencion=2 and ph.idhojadiaria = " + idatencion + ";");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion aten = new atencion();
                aten.setOtroprocedimiento(this.cnn.getRst().getString("nombre_procedimiento"));

                va.add(aten);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    public Vector<atencion> BuscarExamenesparaAtencionOftalmologica(int idatencion) {
        Vector<atencion> va = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  e.nombre_examenes  \n"
                + "FROM \n"
                + "  agenda.hojadiaria_examenes he inner join \n"
                + "  agenda.examenes_especialidad ee on he.idexamenes_especialidades = ee.idexamen_especialidad\n"
                + "  join agenda.examenes e on e.id_examenes = ee.idexamenes\n"
                + "  where he.tipoatencion = 2 and idhojadiaria=" + idatencion + "");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion aten = new atencion();
                aten.setOtroexamenes(this.cnn.getRst().getString("nombre_examenes"));

                va.add(aten);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    public Vector<lugar> BuscarnoIndicacionesdecirugia(int idatencion) {
        Vector<lugar> idc = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT    nic.idatencionoftalmologica,\n"
                + "  case when nic.iddiagnosticoespecialidad <> '-1'\n"
                + "  then ( select d.nombre_diagnostico from\n"
                + "   agenda.diagnostico_especialidad de \n"
                + "   join agenda.diagnosticos d on de.iddiagnostico = d.iddiagnostico\n"
                + "    where de.iddiagnodtivo_especialidad = nic.iddiagnosticoespecialidad) \n"
                + "    else nic.otrodiagnostico  end as diagnostico,\n"
                + "    nic.indicaciones,\n"
                + "    nic.intervencion,\n"
                + "     nic.razones,\n"
                + "     case when  nic.lateralidad = 1 then 'OJO DERECHO' else 'OJO IZQUIERDO'  end  as lateralidad\n"
                + "     FROM \n"
                + "      agenda.noindicacionescirugia nic \n"
                + "      where nic.idatencionoftalmologica =" + idatencion + "  ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idatencionoftalmologica"));
                l.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                l.setVariable1(this.cnn.getRst().getString("lateralidad"));
                l.setIndicaciones(this.cnn.getRst().getString("indicaciones"));
                l.setIntervencio(this.cnn.getRst().getString("intervencion"));
                l.setVariable2(this.cnn.getRst().getString("razones"));

                idc.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return idc;
    }

    public Vector<atencion> BuscarIndicacionesparaAtencionenBox(int atencion) {
        Vector<atencion> va = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "i.nombre_indicaciones , case when descripcionindicacion is null then  '' else descripcionindicacion end as descripcionindicacion\n"
                + "FROM \n"
                + "  agenda.idicaciones_hojadiaria ih inner join\n"
                + "  agenda.indicaciones_especialidad ie on ih.idindicaciones_especialidad= ie.idindicaciones_idespecialidades\n"
                + "  join agenda.indicaciones i on i.id_indicaciones = ie.id_indicaciones\n"
                + "  where ih.tipoatencion = 2 and idhojadiaria =" + atencion + "");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion aten = new atencion();
                aten.setOtrasindicaciones(this.cnn.getRst().getString("nombre_indicaciones"));
                aten.setDetallereceta(this.cnn.getRst().getString("descripcionindicacion"));

                va.add(aten);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    /*no indicacion de cirugia*/
    public int noIndicaciondeCirugia(lugar a) {
        int mensaje = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL(" INSERT INTO \n"
                + "  agenda.noindicacionescirugia\n"
                + "(\n"
                + "  idatencionoftalmologica,\n"
                + "  iddiagnosticoespecialidad,\n"
                + "  lateralidad,\n"
                + "  otrodiagnostico,\n"
                + "  intervencion,\n"
                + "  razones,\n"
                + "  indicaciones\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + a.getId_atencion() + ",\n"
                + "  " + a.getId_diagnostico_urgencia() + ",\n"
                + "  " + a.getGes() + ",\n"
                + "  '" + a.getIntervencio() + "',\n"
                + "  '" + a.getDescripcion() + "',\n"
                + "  '" + a.getDiagnostico() + "',\n"
                + "  '" + a.getIndicaciones() + "'\n"
                + ");");
        try {
            this.cnn.conectar();
            if (this.cnn.getResultadoSQL() == 1) {
                mensaje = 1;
            }
        } catch (Exception ex) {

        } finally {
            this.cnn.cerrarConexion();
        }
        return mensaje;
    }

    /*para estadistico*/
    public atencion ConstanciaGesparaAtencionEstadistico(int atencion) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT 'CONSTGES-'|| to_char( act.fecha_registro, 'yyyymmdd')||'--OFT-'|| case when char_length(act.id_atencion_tecnologo)>4 then cast(act.id_atencion_tecnologo as text) else to_char(act.id_atencion_tecnologo, '0000') end  as folio, "
                + "  to_char(act.fecha_registro,'dd-mm-yyyy HH24:MI:SS') as fecha ,  "
                + "   p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,  "
                + "   p.rut,p.direccion as domiciolio, p.contacto1||'/'|| p.contacto2 as telefonos,  "
                + "   p.email, case when rp.tratamientocerca = 1 then 1 else case when rp.tratamientolejos = 1 then 1 else 0 end end as tratamiento_con_lentes,  "
                + "   d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre,  "
                + "   d.rut as rutdoctor , d.profesion  "
                + " FROM   "
                + "     agenda.atencion_clinica_tecnologo act  "
                + "    inner join agenda.cita c on c.id_cita = act.id_cita  "
                + "    join agenda.registropatalogia rp on act.id_atencion_tecnologo = rp.id_atencion "
                + "   join agenda.paciente p on p.rut = c.rut_paciente  "
                + "    join agenda.doctor d on act.tecnolog = d.rut where act.id_atencion_tecnologo = " + atencion + " ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                a.setFolio(this.cnn.getRst().getString("folio"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setDomicilio(this.cnn.getRst().getString("domiciolio"));
                a.setTelefono(this.cnn.getRst().getString("telefonos"));
                a.setCorreo_electronico(this.cnn.getRst().getString("email"));
                a.setTratamientoeindicaciones(this.cnn.getRst().getString("tratamiento_con_lentes"));
                a.setMedico_solicitante(this.cnn.getRst().getString("doctornombre"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*otra constancia*/
    public atencion ConstanciaGesparaAtencionOftalmologiaEstadistico(int atencion) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT 'CONSTGES-'|| to_char( aco.fecha_registro, 'yyyymmdd')||'--OFT-'||  case when char_length(aco.id_atencionoftalmologo)>4 then cast(aco.id_atencionoftalmologo as text) else to_char(aco.id_atencionoftalmologo, '0000') end  as folio, "
                + "to_char(aco.fecha_registro,'dd-mm-yyyy HH24:MI:SS') as fecha , "
                + "p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,  "
                + " p.rut,p.direccion as domiciolio, p.contacto1||'/'|| p.contacto2 as telefonos,  "
                + " p.email,   "
                + " d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre, "
                + " d.rut as rutdoctor, d.profesion , "
                + " tratamiento_quirurgico_cataratas, "
                + " confirma_tratamiento_catarata, "
                + " retinopatia_diabetica, "
                + " confirma_retinopatia_diabetica, "
                + " deprendimiento_retina_traumatico, "
                + " confirma_deprendimiento_retina, "
                + " estrabismo_menores,  "
                + " confirma_estrabismo_menores, "
                + " retinopatia_prematuro, "
                + " confirma_retinopatia_prematuro, "
                + " vicio_refraccion, "
                + " confirma_vicio_refraccion "
                + " FROM  "
                + " agenda.atencion_clinica_oftalmologo aco inner join agenda.cita c on c.id_cita = aco.id_cita "
                + "  join agenda.paciente p on p.rut = c.rut_paciente   "
                + " join agenda.doctor d on aco.doctor = d.rut and aco.id_atencionoftalmologo = " + atencion + " ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                a.setFolio(this.cnn.getRst().getString("folio"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setDomicilio(this.cnn.getRst().getString("domiciolio"));
                a.setTelefono(this.cnn.getRst().getString("telefonos"));
                a.setCorreo_electronico(this.cnn.getRst().getString("email"));

                a.setMedico_solicitante(this.cnn.getRst().getString("doctornombre"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));
                a.setConfirmadescartadiagnostico(this.cnn.getRst().getInt("confirma_tratamiento_catarata"));
                a.setDiagnostico(this.cnn.getRst().getInt("confirma_retinopatia_diabetica"));
                a.setDiagnosticosic(this.cnn.getRst().getInt("confirma_deprendimiento_retina"));
                a.setIndicalente(this.cnn.getRst().getInt("confirma_estrabismo_menores"));
                a.setConfirmadescartadocumentos(this.cnn.getRst().getInt("confirma_retinopatia_prematuro"));
                a.setGes(this.cnn.getRst().getInt("confirma_vicio_refraccion"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*constancia ges urgencia*/
    public atencion ConstanciaGesparaAtencionUrgenciaDentalEstadistico(int atencion) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select 'CONSTGES-'|| to_char( au.fecharegistro, 'yyyymmdd')||'--DNT-'||  case when char_length(au.id_atencion)>4 then cast(au.id_atencion as text) else to_char(au.id_atencion, '0000') end   as folio,  "
                + " to_char(au.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha, pu.nombre ||' '|| pu.apellido_paterno ||' '|| pu.apellido_moderno as nombre  "
                + " , pu.rut, pu.direccion, pu.contacto1|| '/' || pu.contacto2 as telefono, pu.email,  "
                + "  case when du.absceso_submucoso = true then 'Absceso Submucoso,' else '' end  "
                + "  || case when du.gingivitis_ulcero = true then 'Gingivitis Ulcero necrotizante,' else '' end  "
                + "  || case when du.complicacion_post_exodoncia = true then 'Complicaciones post exodoncia,' else '' end  "
                + "  || case when du.traumatismo_dento_alveolares = true then 'Traumatismos dento alveolares,' else '' end  "
                + "   || case when du.pericoronaritis = true then 'Pericoronaritis,' else '' end   "
                + "   || case when du.pulpitis = true then 'Pulpitis.' else '' end as diagnostico ,  "
                + "  au.prescripcion as examenesrealizados, d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor, "
                + "   d.rut as rutdoctor, au.id_atencion  from dental.atencion_urgencia au  "
                + "   inner join dental.admision a on a.id_admision = au.idadmision "
                + "    join agenda.paciente pu on pu.rut = a.rut_paciente  "
                + "    join agenda.comuna c on c.id_comuna = pu.id_comuna "
                + "     join agenda.procedencia p on au.id_centro = p.id_servicio_salud  "
                + "     join dental.diagnostico_urgencia du on au.id_atencion = du.id_atencion  "
                + "    join agenda.doctor d on d.rut = au.rutdoctor where  au.ges = 1 and au.id_atencion = " + atencion + "");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                a.setFolio(this.cnn.getRst().getString("folio"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setDomicilio(this.cnn.getRst().getString("direccion"));
                a.setTelefono(this.cnn.getRst().getString("telefono"));
                a.setCorreo_electronico(this.cnn.getRst().getString("email"));

                a.setMedico_solicitante(this.cnn.getRst().getString("nombredoctor"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setHipotesis_diagnostica(this.cnn.getRst().getString("diagnostico"));
                a.setResumenatenicon(this.cnn.getRst().getString("examenesrealizados"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*prestaciones realizada urge*/
    public lugar InformedePrestacionRealizadaUrge(int atencion) {
        lugar a = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select  to_char(a.fecharegistro, 'dd/mm/yyyy') as fechallegada,  "
                + "  to_char(a.fecharegistro, 'HH24:MI:SS') as horallegada, "
                + "   pu.nombre ||' '|| pu.apellido_paterno ||' '|| pu.apellido_moderno as nombre ,  "
                + "   pu.rut,  "
                + "   to_char( pu.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,"
                + "    to_char(age(pu.fecha_nacimiento),'yy') as edad,  "
                + "    case when pu.genero =1 then 'Femenino'else 'Masculino' end as sexo,  "
                + "    pu.contacto1  ||'/ '|| pu.contacto2 as telefono,  "
                + "  pu.direccion,  "
                + "   c.nombre as comuna,  "
                + "   a.motivo_consulta,  case when pu.provision = 1 then 'Fonasa' else case when pu.provision = 2 then 'Isapre' else case when pu.provision = 4 then'Sin Prevision' else 'Prais'end end end as prevision,   "
                + "    du.descripcion || '-' || case when du.absceso_submucoso = true then ' Absceso Submucoso, ' else '' end  "
                + "   || case when du.gingivitis_ulcero = true then ' Gingivitis Ulcero necrotizante, ' else '' end "
                + "  || case when du.complicacion_post_exodoncia = true then ' Complicaciones post exodoncia, ' else '' end   "
                + "   || case when du.traumatismo_dento_alveolares = true then ' Traumatismos dento alveolares, ' else '' end   "
                + "   || case when du.pericoronaritis = true then ' Pericoronaritis, ' else '' end  "
                + "   || case when du.pulpitis = true then ' Pulpitis. ' else '' end as diagnostico ,  "
                + "  case when pug.urgencia = true then ' 2701012 Urgencia- ' else '' END   "
                + "  || case when au.trepanacion = true then ' Trepanación- ' else '' end  "
                + "  || case when au.pulpotamia = true then ' Pulpotamia- ' else '' end  "
                + "   || case when au.recubrmiento = true then ' Recubrimiento- '  else '' end  "
                + "   || case when au.ferulizacion = true then ' Ferulización- ' else '' end  "
                + "   || case when au.terapia_farmacolofica = true then ' Terapia Farmacologica de Inf. Odontogenicas- ' else '' end  "
                + "  || case when au.cirugia_bucal = true then ' Cirugia Bucal- ' else '' end  "
                + "   || case when pug.exodoncia_permanente = true then ' 2701005 Exodoncia Permanente- ' || au.numero_exodonciap ||'- ' else '' end    "
                + "    || case when pug.kit_aseo_bucal = true then ' 2701013 Examen Salud Oral- ' else '' end   "
                + "    || case when pug.sutura_intraoral = true then ' 2701114 Sutura Intraoral- ' else '' end    "
                + "      || case when pug.obturacion_composite = true then ' 2701010 Exodoncia Temporal- '|| au.numero_exodenciat ||'- ' else '' end   "
                + "       || case when pug.radiografia_retroalvealar = true then ' 2701015 Radiografía Retroalveolar y Bite-Wing - ' || pug.numero_de_rx ||'- ' else '' end  "
                + "      ||  case when pug.obturacion_vidrio_ionomero = true then ' 2701016 Obturación Vidrio Ionómero- ' else '' end  as prestacion,   "
                + "      au.intervencio as intervencion,"
                + "        case when au.ges = 1 then 'Si' else 'No' end as ges,  "
                + "     au.prescripcion as tratamiento,  "
                + "      au.indicaciones as indicaciones,  "
                + "       d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor,  "
                + "       to_char(au.fecharegistro,'HH24:MI:SS') as horaatencion,  "
                + "      d.rut as rutdoctor, au.id_atencion  from dental.atencion_urgencia au   "
                + "       inner join dental.admision a on a.id_admision= au.idadmision   "
                + "        join agenda.paciente pu   "
                + "       on a.rut_paciente = pu.rut    "
                + "        join dental.prestacion_urgenta pug on pug.id_atencion = au.id_atencion "
                + "      join agenda.comuna c on c.id_comuna = pu.id_comuna  "
                + "       join agenda.procedencia p on au.id_centro = p.id_servicio_salud   "
                + "          join dental.diagnostico_urgencia du on au.id_atencion = du.id_atencion  "
                + "       join agenda.doctor d on d.rut = au.rutdoctor where au.id_atencion =  " + atencion + " ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                a.setHora(this.cnn.getRst().getString("horallegada"));

                a.setVariable1(this.cnn.getRst().getString("fechallegada"));

                a.setNombre(this.cnn.getRst().getString("nombre"));

                a.setRut(this.cnn.getRst().getString("rut"));
                a.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                a.setEdad(this.cnn.getRst().getString("edad"));
                a.setSexo(this.cnn.getRst().getString("sexo"));
                a.setTelefono1(this.cnn.getRst().getString("telefono"));
                a.setDireccion(this.cnn.getRst().getString("direccion"));
                a.setComuna(this.cnn.getRst().getString("comuna"));
                a.setVariable2(this.cnn.getRst().getString("motivo_consulta"));
                a.setVariable3(this.cnn.getRst().getString("prevision"));
                a.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                a.setIntervencio(this.cnn.getRst().getString("intervencion"));
                a.setPrescripcion(this.cnn.getRst().getString("prestacion"));
                a.setVariable4(this.cnn.getRst().getString("ges"));
                a.setIndicaciones(this.cnn.getRst().getString("indicaciones"));
                a.setExamenesrealizados(this.cnn.getRst().getString("tratamiento"));
                a.setNombredoctor(this.cnn.getRst().getString("nombredoctor"));
                a.setDescripcion(this.cnn.getRst().getString("horaatencion"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*para quitar conexion multiple de bases de datos */
    public atencion informeProcesoDiagnostico(int llego) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT 'IPD-'|| to_char( act.fecha_registro, 'yyyymmdd')||'--OFT-'|| to_char(act.id_atencion_tecnologo ,'0000')  as folio, "
                + "   to_char(act.fecha_registro,'dd-mm-yyy HH24:MI:SS') as fecha , "
                + "   p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,  "
                + "   p.rut, to_char( p.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento, to_char(age(p.fecha_nacimiento),'yy') as edad,  "
                + "          case when p.genero =1 then 'Femenino'else 'Masculino' end as sexo,   "
                + "    case when  rp.confima = 1 then 'Si' else 'No' end as confirmavicio, "
                + "    case when rp.tratamientocerca = 1 then 'Tratamiento con Lentes Ópticos Cerca' else ' ' end  || "
                + "    case when rp.tratamientolejos = 1 then 'Tratamiento con Lentes Opticos Lejos ' else case when  "
                + "    rp.tratamientoeindicacion != '' then tratamientoeindicacion else '' end end  as   tratamiento,  "
                + "    case when rp.fundamentodeldiagnostico != '' then rp.fundamentodeldiagnostico else 'Examen Clínico' end as fundamentos,  "
                + "    d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre,  "
                + "     d.rut as rutdoctor , d.profesion,  rp.diagnostico as diagnostico  "
                + "     FROM  "
                + "     agenda.atencion_clinica_tecnologo act  "
                + "     inner join agenda.cita c on c.id_cita = act.id_cita  "
                + "     join agenda.registropatalogia rp on rp.id_atencion = act.id_atencion_tecnologo  "
                + "      join agenda.paciente p on p.rut = c.rut_paciente  "
                + "      join agenda.doctor d on act.tecnolog = d.rut where act.id_atencion_tecnologo = " + llego + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {

                a.setFolio(this.cnn.getRst().getString("folio"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setSexo(this.cnn.getRst().getString("sexo"));
                a.setConf(this.cnn.getRst().getString("confirmavicio"));
                a.setDetallereceta(this.cnn.getRst().getString("diagnostico"));
                a.setVesicula(this.cnn.getRst().getString("tratamiento"));
                a.setProfesion(this.cnn.getRst().getString("doctornombre"));
                a.setFundamentodeldiagnostico(this.cnn.getRst().getString("fundamentos"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

}
