/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.entrevista_preoperatoria;
import Modelos.especialidades;
import Modelos.hojadiaria;
import Modelos.lugar;
import Modelos.tipo_anestesia;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Informatica
 */
public class controlador_especialidad extends General {

    public String registrar_especialidad(String nombreespecialidad, String direccionfoto) throws FileNotFoundException, ClassNotFoundException {

        configurarConexion("");
        String inserto = "";
        Connection cn = null;
        PreparedStatement pr = null;
        String sql = "INSERT INTO agenda.especialidades (id_especialidad,nombreespecialidad,estatus, nombreimagen,tamannoimagen, fotoespecialidad) ";
        sql += "VALUES(?,?,?,?,?,?)";
        try {
            Class.forName(cnn.getDriver());
            cn = DriverManager.getConnection(cnn.getNombreBaseDatos(), cnn.getUser(), cnn.getPassword());
            pr = cn.prepareStatement(sql);
            int id = buscarultimaEspecialidad();
            pr.setInt(1, id + 1);

            pr.setString(2, nombreespecialidad);
            pr.setInt(3, 1);

            pr.setString(4, nombreespecialidad + ".jpg");
            //Parametros de la imagen
            File fichero = new File(direccionfoto);
            FileInputStream streamEntrada = new FileInputStream(fichero);
            int tamañoImagen = streamEntrada.available();
            //Establecer los parametros a la BD
            pr.setInt(5, tamañoImagen);
            pr.setBinaryStream(6, streamEntrada, (int) fichero.length());
            if (pr.executeUpdate() == 1) {
                inserto = "Especialidad Registrado Exitosamente";
            } else {
                inserto = "Ocurrio un problema !! Intente mas tarde";
            }
        } catch (Exception ex) {
            inserto = ex.getMessage();
        } finally {
            try {
                pr.close();
                cn.close();

            } catch (Exception ex) {

            }
        }

        return inserto;

    }

    public boolean buscarEspecialidad(String nombre) {
        boolean siexiste = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.especialidades where nombre='" + nombre + "'; ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                siexiste = true;
            } else {
                siexiste = false;
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return siexiste;
    }

    /*buscar especialidad para combo*/
    public Vector<especialidades> buscarEspecialdadTodas() {
        Vector<especialidades> ve = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT \n"
                + "  id_especialidad,\n"
                + "  nombreespecialidad\n"
                + "  FROM \n"
                + "  agenda.especialidades where estatus =1 order by nombreespecialidad ; ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                especialidades e = new especialidades();
                e.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));
                e.setNombre(this.cnn.getRst().getString("nombreespecialidad"));
                ve.add(e);
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return ve;

    }

    public boolean buscarsitengoexamenesdeEntrevista(int identrevista) {
        boolean tiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " * "
                + "FROM \n"
                + "  agenda.entrevista_examenes where identrevista =" + identrevista + " ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                tiene = true;
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();
        return tiene;

    }

    /*buscar los examenes de una entrevista*/
    public lugar buscarexamentesdeEntrevista(int identrevista) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                  identrevista,\n"
                + "                  case when  (hematocrito is null) or (hematocrito= '')  then '' else hematocrito || ' %' end as hematocrito,\n"
                + "                  case when (rectoblancos is null) or (rectoblancos= '') then '' else rectoblancos || ' x10^3/µL' end as rectoblancos,\n"
                + "                 case when (plaquetas is null)  or (plaquetas= '') then '' else plaquetas || ' x10^3/µL' end as plaquetas,\n"
                + "                 case when (gurporh is null)  or (gurporh= '') then '' else gurporh|| 'nkj' end as gurporh ,\n"
                + "                 case when (glicemia is null) or (glicemia= '') then '' else glicemia || ' mg/dl' end as glicemia,\n"
                + "                 case when( buncreat is null)  or (buncreat= '') then '' else buncreat || ' mg/dL/0,8' end as buncreat,\n"
                + "                 case when (nak is null)  or (nak= '') then '' else nak end as nak,\n"
                + "                 case when (orinacompleta is null)  or (orinacompleta= '') then '' else orinacompleta end as orinacompleta,\n"
                + "                 case when (protrombina is null)  or (protrombina= '') then '' else protrombina || ' %' end as protrombina,\n"
                + "                 case when  (ttpk is null)  or (ttpk= '') then '' else ttpk || ' Segundos'  end as ttpk,\n"
                + "                 case when  (ecg is null)  or (ecg= '') then '' else ecg end as ecg,\n"
                + "                 case when (otro is null)   or (otro= '') then '' else otro end as otro\n"
                + "              FROM \n"
                + "                 agenda.entrevista_examenes where identrevista =" + identrevista + ";");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                l.setComuna(this.cnn.getRst().getString("hematocrito"));
                l.setDescripcion(this.cnn.getRst().getString("rectoblancos"));
                l.setDetalle_de_la_derivacion(this.cnn.getRst().getString("plaquetas"));
                l.setDiagnostico(this.cnn.getRst().getString("gurporh"));
                l.setDireccion(this.cnn.getRst().getString("glicemia"));
                l.setEdad(this.cnn.getRst().getString("buncreat"));
                l.setEmail(this.cnn.getRst().getString("nak"));
                l.setExamenesrealizados(this.cnn.getRst().getString("orinacompleta"));
                l.setFechanacimiento(this.cnn.getRst().getString("protrombina"));
                l.setFolio(this.cnn.getRst().getString("ttpk"));
                l.setIndicaciones(this.cnn.getRst().getString("ecg"));
                l.setNombre(this.cnn.getRst().getString("otro"));
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();
        return l;
    }

    public lugar buscarexamentesdePARAREGISTROEntrevista(int identrevista) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                  identrevista,\n"
                + "                  case when  (hematocrito is null) or (hematocrito= '')  then '' else hematocrito  end as hematocrito,\n"
                + "                  case when (rectoblancos is null) or (rectoblancos= '') then '' else rectoblancos  end as rectoblancos,\n"
                + "                 case when (plaquetas is null)  or (plaquetas= '') then '' else plaquetas  end as plaquetas,\n"
                + "                 case when (gurporh is null)  or (gurporh= '') then '' else gurporh end as gurporh ,\n"
                + "                 case when (glicemia is null) or (glicemia= '') then '' else glicemia  end as glicemia,\n"
                + "                 case when( buncreat is null)  or (buncreat= '') then '' else buncreat  end as buncreat,\n"
                + "                 case when (nak is null)  or (nak= '') then '' else nak end as nak,\n"
                + "                 case when (orinacompleta is null)  or (orinacompleta= '') then '' else orinacompleta end as orinacompleta,\n"
                + "                 case when (protrombina is null)  or (protrombina= '') then '' else protrombina  end as protrombina,\n"
                + "                 case when  (ttpk is null)  or (ttpk= '') then '' else ttpk   end as ttpk,\n"
                + "                 case when  (ecg is null)  or (ecg= '') then '' else ecg end as ecg,\n"
                + "                 case when (otro is null)   or (otro= '') then '' else otro end as otro\n"
                + "              FROM \n"
                + "                 agenda.entrevista_examenes where identrevista =" + identrevista + ";");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                l.setComuna(this.cnn.getRst().getString("hematocrito"));
                l.setDescripcion(this.cnn.getRst().getString("rectoblancos"));
                l.setDetalle_de_la_derivacion(this.cnn.getRst().getString("plaquetas"));
                l.setDiagnostico(this.cnn.getRst().getString("gurporh"));
                l.setDireccion(this.cnn.getRst().getString("glicemia"));
                l.setEdad(this.cnn.getRst().getString("buncreat"));
                l.setEmail(this.cnn.getRst().getString("nak"));
                l.setExamenesrealizados(this.cnn.getRst().getString("orinacompleta"));
                l.setFechanacimiento(this.cnn.getRst().getString("protrombina"));
                l.setFolio(this.cnn.getRst().getString("ttpk"));
                l.setIndicaciones(this.cnn.getRst().getString("ecg"));
                l.setNombre(this.cnn.getRst().getString("otro"));
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();
        return l;
    }

    /*insertar hoja diaria*/
    public String insertarHojadiaria(hojadiaria h) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.hojadiaria\n"
                + "(\n"
                + "  idcita,\n"
                + "  fecharegistro,\n"
                + "  sic,\n"
                + "  ges,\n"
                + "  otrodiagnostico,\n"
                + "  otroprocedimiento,\n"
                + "  otroexamenes,\n"
                + "  otroindicaciones,\n"
                + "  iddestinopaciente,\n"
                + "  otrodestinopaciente,\n"
                + "  solicitudpabellon,\n"
                + "  paraquesederive ,"
                + "  diagnosticosic,\n"
                + "  constanciages,\n"
                + "  otrodiagnosticosic,"
                + "  fundamento,\n"
                + "  tratamiento, "
                + "  pertinencia\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + h.getIdcita() + ",\n"
                + "  CURRENT_TIMESTAMP,\n"
                + "  " + h.getSic() + ",\n"
                + "  " + h.getGes() + ",\n"
                + "  '" + h.getOtro_diagnostico() + "',\n"
                + "  '" + h.getOtro_procedimiento() + "',\n"
                + "  '" + h.getOtro_examen() + "',\n"
                + "  '" + h.getOtras_indicaciones() + "',\n"
                + "  " + h.getIddestinopaciente() + ",\n"
                + "  '" + h.getOtrodestino_paciente() + "',"
                + "  '" + h.isSolicitudpabellon() + "',"
                + "  '" + h.getParaquesederive() + "' ,"
                + "  " + h.getDiagnosticosic() + ","
                + "  " + h.getConstancia() + " ,"
                + "  '" + h.getDiagnosticootrosic() + "',"
                + "   '" + h.getFundamentocataratas() + "',"
                + "   '" + h.getTratamientocataratas() + "',"
                + "   '" + h.getPertinencia() + "'\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Hoja Diaria Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

    /*insertar los diagnosticos, procedimientos, examenes e indicaciones */
    public void insertardiagnosticoshojadiaria(int[] diagnosticos, int idhojadiaria) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        for (int i = 0; i < diagnosticos.length; ++i) {
            if (diagnosticos[i] != 0) {
                this.cnn.setSentenciaSQL("INSERT INTO \n"
                        + "  agenda.diagnostico_hojadiaria\n"
                        + "(\n"
                        + "  idhojadiaria,\n"
                        + "  iddiagnostico_especialdad\n"
                        + ") \n"
                        + "VALUES (\n"
                        + "  " + idhojadiaria + ",\n"
                        + "  " + diagnosticos[i] + "\n"
                        + ");");
                this.cnn.conectar();
            }
        }
        this.cnn.cerrarConexion();
    }

    /*insertar procedimiento*/
    public void insertarprocedimientoshojadiaria(int[] procedimientos, int idhojadiaria, int tipo) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        for (int i = 0; i < procedimientos.length; ++i) {
            if (procedimientos[i] != 0) {
                this.cnn.setSentenciaSQL("INSERT INTO \n"
                        + "  agenda.procedimiento_hojadiaria\n"
                        + "(\n"
                        + "  idhojadiaria,\n"
                        + "  idprocedimiento_especialidad,"
                        + "  tipoatencion\n"
                        + ")  VALUES(\n"
                        + "  " + idhojadiaria + ",\n"
                        + "  " + procedimientos[i] + ","
                        + "   " + tipo + "\n"
                        + ");"
                );
                this.cnn.conectar();
            }
        }
        this.cnn.cerrarConexion();
    }

    /*insertar examenes*/
    public void insertarexameneshojadiaria(int[] examenes, int idhojadiaria, int tipo) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        for (int i = 0; i < examenes.length; ++i) {
            if (examenes[i] != 0) {
                this.cnn.setSentenciaSQL("INSERT INTO \n"
                        + "  agenda.hojadiaria_examenes\n"
                        + "(\n"
                        + "  idhojadiaria,\n"
                        + "  idexamenes_especialidades,"
                        + " tipoatencion\n"
                        + ") \n"
                        + "VALUES ("
                        + "  " + idhojadiaria + ",\n"
                        + "  " + examenes[i] + ","
                        + "  " + tipo + "\n"
                        + ");"
                );
                this.cnn.conectar();
            }
        }
        this.cnn.cerrarConexion();
    }

    /*modificar examenes entrevistaPreoperatorio*/
    public void modificarexamenesEntrevista(lugar l) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.entrevista_examenes  \n"
                + "SET \n"
                + "  hematocrito = '" + l.getComuna() + "',\n"
                + "  rectoblancos = '" + l.getDiagnostico() + "',\n"
                + "  plaquetas = '" + l.getEmail() + "',\n"
                + "  gurporh = '" + l.getFolio() + "',\n"
                + "  glicemia = '" + l.getDescripcion() + "',\n"
                + "  buncreat = '" + l.getDireccion() + "',\n"
                + "  nak = '" + l.getExamenesrealizados() + "',\n"
                + "  orinacompleta = '" + l.getIndicaciones() + "',\n"
                + "  protrombina = '" + l.getDetalle_de_la_derivacion() + "',\n"
                + "  ttpk = '" + l.getEdad() + "',\n"
                + "  ecg = '" + l.getFechanacimiento() + "',\n"
                + "  otro = '" + l.getIntervencio() + "'\n"
                + " \n"
                + "WHERE \n"
                + " identrevista = " + l.getId_atencion() + "\n"
                + ";");

        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*insertar entrevisatexamenes*/
    public void insertarexamenesEntrevistaPreoperatorio(lugar l) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.entrevista_examenes\n"
                + "(\n"
                + "  identrevista,\n"
                + "  hematocrito,\n"
                + "  rectoblancos,\n"
                + "  plaquetas,\n"
                + "  gurporh,\n"
                + "  glicemia,\n"
                + "  buncreat,\n"
                + "  nak,\n"
                + "  orinacompleta,\n"
                + "  protrombina,\n"
                + "  ttpk,\n"
                + "  ecg,\n"
                + "  otro\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + l.getId_atencion() + ",\n"
                + "  '" + l.getComuna() + "',\n"
                + "  '" + l.getDiagnostico() + "',\n"
                + "  '" + l.getEmail() + "',\n"
                + "  '" + l.getFolio() + "',\n"
                + "  '" + l.getDescripcion() + "',\n"
                + "  '" + l.getDireccion() + "',\n"
                + "  '" + l.getExamenesrealizados() + "',\n"
                + "  '" + l.getIndicaciones() + "',\n"
                + "  '" + l.getDetalle_de_la_derivacion() + "',\n"
                + "  '" + l.getEdad() + "',\n"
                + "  '" + l.getFechanacimiento() + "',\n"
                + "  '" + l.getIntervencio() + "'\n"
                + ");"
        );
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }


    /*insertar indicaciones*/
    public void insertarindicacioneshojadiaria(int[] indicaciones, int idhojadiaria, int[] cantidad, String[] detalle, int tipo) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        for (int i = 0; i < indicaciones.length; ++i) {
            if (indicaciones[i] != 0) {
                this.cnn.setSentenciaSQL("INSERT INTO \n"
                        + "  agenda.idicaciones_hojadiaria\n"
                        + "(\n"
                        + "  idhojadiaria,\n"
                        + "  idindicaciones_especialidad,\n"
                        + "  cantidad,"
                        + "  tipoatencion,"
                        + " descripcionindicacion\n"
                        + ") \n"
                        + "VALUES (\n"
                        + "  " + idhojadiaria + ",\n"
                        + "  " + indicaciones[i] + ",\n"
                        + "  " + cantidad[i] + ","
                        + "  " + tipo + " ,"
                        + "  '" + detalle[i] + "'\n"
                        + ");");
                this.cnn.conectar();
            }
        }
        this.cnn.cerrarConexion();
    }

    //para buscar el ultima especialidad
    public int buscarultimaEspecialidad() {
        int idespecialidad = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  max(id_especialidad) as especialidad FROM agenda.especialidades;  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                idespecialidad = this.cnn.getRst().getInt("especialidad");
            } else {
                idespecialidad = 0;
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return idespecialidad;
    }

    public String actualizarEspecialidad(int codigo, String nombreespecialidad, String dirArchivo) throws FileNotFoundException, IOException {
        String actualizo = "";
        File fichero = new File(dirArchivo);
        FileInputStream streamEntrada = new FileInputStream(fichero);
        int tamañoImagen = streamEntrada.available();
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("UPDATE  agenda.especialidades SET  nombreespecialidad='" + nombreespecialidad + "',"
                + " nombreimagen='" + nombreespecialidad + ".jpg" + "', tamannoimagen=" + tamañoImagen + ", fotoespecialidad='" + streamEntrada + "' "
                + "WHERE id_especialidad=" + codigo + "");
        try {
            this.cnn.conectar();

            if (this.cnn.getResultadoSQL() == 1) {
                actualizo = "Especialidad Modificado Exitosamente";
            } else {
                actualizo = "Ocurrio un error!! Intente mas tarde";
            }
        } catch (Exception ex) {

        } finally {
            this.cnn.cerrarConexion();
        }

        return actualizo;
    }

    public String eliminarespecialidad(int codigo) {
        String elimino = "";
        this.configurarConexion("");

        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("UPDATE agenda.especialidades SET estatus=0 WHERE id_especialidad=" + codigo + " ");
        try {
            this.cnn.conectar();
            if (this.cnn.getResultadoSQL() == 1) {
                elimino = "Especialidad Eliminado Exitosamente";
            } else {
                elimino = "Ocurrio un problema!! Intente mas tarde";
            }
        } catch (Exception ex) {

        } finally {
            try {
                this.cnn.cerrarConexion();
            } catch (Exception ex) {

            }
        }

        return elimino;
    }

    public especialidades buscarEspecialidadCodigo(int codigo) {
        especialidades es = null;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT * FROM agenda.especialidades WHERE id_especialidad=" + codigo + " and estatus = 1");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                es = new especialidades();
                es.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));
                es.setNombre(this.cnn.getRst().getString("nombreespecialidad"));
                es.setEstatus(this.cnn.getRst().getInt("estatus"));
                es.setNombreimagen(this.cnn.getRst().getString("nombreimagen"));
                es.setTamanoimagen(this.cnn.getRst().getInt("tamannoimagen"));
                es.setFoto(this.cnn.getRst().getBinaryStream("fotoespecialidad"));

            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();
        return es;
    }

    public Vector<especialidades> buscarEspecialidades() {
        Vector<especialidades> vecEs = new Vector<especialidades>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT * FROM agenda.especialidades where estatus = 1");
        try {
            this.cnn.conectar();
            especialidades es = null;
            while (this.cnn.getRst().next()) {
                es = new especialidades();
                es.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));
                es.setNombre(this.cnn.getRst().getString("nombreespecialidad"));
                es.setEstatus(this.cnn.getRst().getInt("estatus"));
                es.setNombreimagen(this.cnn.getRst().getString("nombreimagen"));
                es.setTamanoimagen(this.cnn.getRst().getInt("tamannoimagen"));
                es.setFoto(this.cnn.getRst().getBinaryStream("fotoespecialidad"));
                vecEs.add(es);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.cnn.cerrarConexion();
            } catch (Exception ex) {

            }
        }

        return vecEs;
    }

    public byte[] obtenImagenEspecialiad(int id_especialidad) throws SQLException {

        byte[] buffer = null;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from agenda.especialidades where id_especialidad = " + id_especialidad + "");
        this.cnn.conectar();
        while (this.cnn.getRst().next()) {

            // Blob bin = rs.getBlob("fotoproducto");
            // if (bin != null) {
            InputStream inStream = this.cnn.getRst().getBinaryStream("fotoespecialidad");
            int size = this.cnn.getRst().getInt("tamannoimagen");
            buffer = new byte[size];
            int length = -1;
            int k = 0;
            try {
                inStream.read(buffer, 0, size);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            //}
        }

        return buffer;
    }

    /**
     * para los selectores
     *
     *
     */
    public ArrayList buscarListaEspecialidadessinDoctor(String doctor) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT * FROM "
                + " agenda.especialidades  where id_especialidad not in "
                + " (SELECT  de.id_especialidad FROM  agenda.doctor_especialidad de  inner join agenda.doctor d on "
                + " de.rut = d.rut where upper(d.rut)=upper('" + doctor + "')) and estatus= 1 ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                especialidades es = new especialidades();
                es.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));
                es.setNombre(this.cnn.getRst().getString("nombreespecialidad"));
                lista.add(es);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return lista;

    }

    public ArrayList buscarListaEspecialidadesconDoctor(String doctor) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT  * FROM  agenda.especialidades e  inner join agenda.doctor_especialidad d "
                + "  on  e.id_especialidad = d.id_especialidad where upper(d.rut)=upper('" + doctor + "')");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                especialidades es = new especialidades();
                es.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));
                es.setNombre(this.cnn.getRst().getString("nombreespecialidad"));
                lista.add(es);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return lista;

    }

    /*todo lo de registro de especialidades en box*/
    public Vector<lugar> BuscarlosDestinoPacientes() {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  iddestino_paciente,\n"
                + "  nombredestino_paciente,\n"
                + "  estatus\n"
                + "  FROM \n"
                + "  agenda.destinopaciente where estatus = 1 ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("iddestino_paciente"));
                l.setNombre(this.cnn.getRst().getString("nombredestino_paciente"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;

    }

    public Vector<tipo_anestesia> tiposdeanestesia() {
        Vector<tipo_anestesia> tav = new Vector<tipo_anestesia>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idtipoanestesia,\n"
                + "  nombre,\n"
                + "  estatus\n"
                + "FROM \n"
                + "  pabellon.tipoanestesia where estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                tipo_anestesia ta = new tipo_anestesia();
                ta.setIdtipo_atencion(this.cnn.getRst().getInt("idtipoanestesia"));
                ta.setNombretipo(this.cnn.getRst().getString("nombre"));
                tav.add(ta);

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return tav;
    }

    public Vector<lugar> buscarDiagnosticosporEspecialidad(int especialidad) {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select d.caso, de.iddiagnodtivo_especialidad ,d.iddiagnostico,d.nombre_diagnostico from agenda.diagnostico_especialidad de  "
                + " inner join agenda.diagnosticos d on de.iddiagnostico = d.iddiagnostico where de.idespecialidad = " + especialidad + ""
                + "and d.estatus = 1 order by d.nombre_diagnostico; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("iddiagnodtivo_especialidad"));
                l.setId_lugar(this.cnn.getRst().getInt("iddiagnostico"));
                l.setNombre(this.cnn.getRst().getString("nombre_diagnostico"));
                l.setVariable1(this.cnn.getRst().getString("nombre_diagnostico"));
                l.setGes(this.cnn.getRst().getInt("caso"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;

    }

    /*BUSCAR LOS PROCEDIMIENTOS*/
    public Vector<lugar> buscarlosProcedimientosporEspecialidad(int idespecialidad) {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT pe.idprocedimiento_especialidad,\n"
                + " pro.idprocedimiento,\n"
                + " pro.nombre_procedimiento\n"
                + "FROM \n"
                + "  agenda.procedimiento_especialidad pe inner join agenda.procedimiento\n"
                + "  pro on pro.idprocedimiento = pe.idprocedimiento where pe.idespecialidad = " + idespecialidad + " and pro.estatus = 1 order by pro.nombre_procedimiento ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idprocedimiento_especialidad"));
                l.setId_lugar(this.cnn.getRst().getInt("idprocedimiento"));
                l.setNombre(this.cnn.getRst().getString("nombre_procedimiento"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;
    }

    /*otro*/
    public Vector<lugar> buscarlosDiagnosticoEspecialidad(int idespecialidad) {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " de.iddiagnodtivo_especialidad,\n"
                + " COALESCE(d.nombre_diagnostico,'') as nombre,\n"
                + " d.iddiagnostico\n"
                + "FROM \n"
                + "  agenda.diagnostico_especialidad de inner join\n"
                + "  agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico\n"
                + "  and de.idespecialidad = " + idespecialidad + " and d.estatus=1 ");
        this.cnn.conectar();
        try {

            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("iddiagnodtivo_especialidad"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                l.setId_diagnostico_urgencia(this.cnn.getRst().getInt("iddiagnostico"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;
    }

    public String buscarnombreDiagnotico(int iddiagnostico) {
        this.configurarConexion("");
        String nombre = "";
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  iddiagnostico,\n"
                + "  COALESCE(nombre_diagnostico,'') as nombre_diagnostico\n"
                + "FROM \n"
                + "  agenda.diagnosticos where iddiagnostico = "+iddiagnostico+" ;");
        this.cnn.conectar();
        try {

            if (this.cnn.getRst().next()) {
               nombre = this.cnn.getRst().getString("nombre_diagnostico");
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return nombre;
    }


    /*buscar todos los procedimientos */
    public Vector<lugar> buscarlosProcedimientos() {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT pe.idprocedimiento_especialidad,\n"
                + " pro.idprocedimiento,\n"
                + " pro.nombre_procedimiento\n"
                + "FROM \n"
                + "  agenda.procedimiento_especialidad pe inner join agenda.procedimiento\n"
                + "  pro on pro.idprocedimiento = pe.idprocedimiento where pe.idespecialidad in(1,24,14) and pro.estatus = 1 group by idprocedimiento_especialidad,pro.idprocedimiento,pro.nombre_procedimiento ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idprocedimiento_especialidad"));
                l.setId_lugar(this.cnn.getRst().getInt("idprocedimiento"));
                l.setNombre(this.cnn.getRst().getString("nombre_procedimiento"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;
    }

    /*examenes por especialidad*/
    public Vector<lugar> buscarlosExamenesporEspecialidad(int idespecialidad) {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT ee.idexamen_especialidad, \n"
                + "  e.id_examenes,\n"
                + "  e.nombre_examenes\n"
                + "   FROM \n"
                + "  agenda.examenes_especialidad ee inner join agenda.examenes e\n"
                + "  on ee.idexamenes = e.id_examenes where e.estatus = 1 and ee.idespecialidad =" + idespecialidad + " order by e.nombre_examenes; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idexamen_especialidad"));
                l.setId_lugar(this.cnn.getRst().getInt("id_examenes"));
                l.setNombre(this.cnn.getRst().getString("nombre_examenes"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;
    }

    /*diagnosticos de oftalmologia como tal */
    public Vector<lugar> buscarlosExamenesparaOftalmologia() {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT ee.idexamen_especialidad, \n"
                + "  e.id_examenes,\n"
                + "  e.nombre_examenes\n"
                + "   FROM \n"
                + "  agenda.examenes_especialidad ee inner join agenda.examenes e\n"
                + "  on ee.idexamenes = e.id_examenes where e.estatus = 1 and ee.idespecialidad in(1,14,24)  group by idexamen_especialidad,id_examenes,nombre_examenes; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idexamen_especialidad"));
                l.setId_lugar(this.cnn.getRst().getInt("id_examenes"));
                l.setNombre(this.cnn.getRst().getString("nombre_examenes"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;
    }


    /*Indicaciones por especialidad*/
    public Vector<lugar> buscarlasindicacionesporEspecialidad(int idespecialidad) {
        Vector<lugar> vd = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  ie.idindicaciones_idespecialidades, \n"
                + "  i.id_indicaciones,\n"
                + "  i.nombre_indicaciones\n"
                + "FROM \n"
                + "  agenda.indicaciones_especialidad ie inner join agenda.indicaciones i\n"
                + "  on ie.id_indicaciones = i.id_indicaciones where i.estatus=1 and ie.id_especialidades=" + idespecialidad + " order by i.id_indicaciones ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idindicaciones_idespecialidades"));
                l.setId_lugar(this.cnn.getRst().getInt("id_indicaciones"));
                l.setNombre(this.cnn.getRst().getString("nombre_indicaciones"));
                vd.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vd;
    }

    public boolean buscarsitengoindicaciones(int especialidad) {
        boolean tiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  i.id_indicaciones,\n"
                + "  i.nombre_indicaciones\n"
                + "FROM \n"
                + "  agenda.indicaciones_especialidad ie inner join agenda.indicaciones i\n"
                + "  on ie.id_indicaciones = i.id_indicaciones where i.estatus=1 and ie.id_especialidades=" + especialidad + " ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                tiene = true;
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return tiene;
    }

    public int modificarhojadiaria(hojadiaria hd) {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.hojadiaria  \n"
                + "SET \n"
                + "  sic = " + hd.getSic() + ",\n"
                + "  ges = " + hd.getGes() + ",\n"
                + "  otrodiagnostico = '" + hd.getOtro_diagnostico() + "',\n"
                + "  otroprocedimiento = '" + hd.getOtro_procedimiento() + "',\n"
                + "  otroexamenes ='" + hd.getOtro_examen() + "',\n"
                + "  otroindicaciones = '" + hd.getOtras_indicaciones() + "',\n"
                + "  iddestinopaciente = " + hd.getIddestinopaciente() + ",\n"
                + "  otrodestinopaciente = '" + hd.getOtrodestino_paciente() + "',\n"
                + "  solicitudpabellon = '" + hd.isSolicitudpabellon() + "',\n"
                + "  paraquesederive = '" + hd.getParaquesederive() + "',\n"
                + "  diagnosticosic = " + hd.getDiagnosticosic() + ",\n"
                + "  constanciages = " + hd.getConstancia() + ",\n"
                + "  otrodiagnosticosic = '" + hd.getOtrodiagnosticosolicitud() + "',"
                + "    fundamento = '" + hd.getFundamentocataratas() + "',\n"
                + "  tratamiento = '" + hd.getTratamientocataratas() + "',"
                + "  pertinencia = " + hd.getPertinencia() + "\n"
                + " \n"
                + "WHERE \n"
                + "  idhojadiaria = " + hd.getIdhojadiaria() + "\n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }
        this.cnn.cerrarConexion();
        return exito;
    }

    /*elimino los diagnosticos , procedimientos y examenes para volver a llenar*/
    public int eliminarprocedimientoshojadiaria(int hojadiaria) {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.procedimiento_hojadiaria ph\n"
                + "WHERE  ph.idhojadiaria = " + hojadiaria + " and ph.tipoatencion = 1 \n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }
        this.cnn.cerrarConexion();
        return exito;

    }

    /*eliminar indicacioneshojadiaria*/
    public int eliminarIndicacioneshojadiaria(int hojadiaria) {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.idicaciones_hojadiaria ih\n"
                + "WHERE ih.idhojadiaria = " + hojadiaria + " and ih.tipoatencion = 1\n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }
        this.cnn.cerrarConexion();
        return exito;

    }

    /*eliminar */
    public int eliminarDiagnosticohojadiaria(int hojadiaria) {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.diagnostico_hojadiaria dh \n"
                + "WHERE dh.idhojadiaria = " + hojadiaria + "\n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }
        this.cnn.cerrarConexion();
        return exito;

    }

    /*eliminar examenes hojas*/
    public int eliminarExameneshojadiaria(int hojadiaria) {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.hojadiaria_examenes he\n"
                + "WHERE he.idhojadiaria = " + hojadiaria + "  and he.tipoatencion=1\n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }
        this.cnn.cerrarConexion();
        return exito;

    }

    public int buscariddeHojadiariaporCita(int idcita) {
        int idhojadiaria = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idhojadiaria \n"
                + "FROM \n"
                + "  agenda.hojadiaria where idcita = " + idcita + ";");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                idhojadiaria = this.cnn.getRst().getInt("idhojadiaria");
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return idhojadiaria;

    }

    /*buscar datos para documentos*/
 /*constancia ges */
    public hojadiaria BuscarconstanciaGes(int idhojadiaria) {

        hojadiaria hd = new hojadiaria();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT 'CONSTGES-'|| to_char( fecharegistro, 'yyyymmdd')\n"
                + " || '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'||  case when char_length(idhojadiaria)>4 then cast(idhojadiaria as text) else to_char(idhojadiaria, '0000') end   as folio,"
                + "  to_char(hd.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha , hd.fecharegistro, \n"
                + " d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor,\n"
                + " d.rut as rutdoctor,\n"
                + " pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombrepaciente,\n"
                + " pac.rut,\n"
                + " pac.direccion,\n"
                + " pac.contacto1,\n"
                + " pac.email,\n"
                + " case when hd.otrodiagnostico <> '' then otrodiagnostico  else  '' end as otrodiagnostico,\n"
                + " hd.idhojadiaria\n"
                + " \n"
                + "FROM \n"
                + "  agenda.hojadiaria hd inner join \n"
                + "  agenda.cita c on c.id_cita = hd.idcita\n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.especialidades e on e.id_especialidad= p.id_especialidad\n"
                + "  join agenda.doctor d on d.rut = p.rut_doctor \n"
                + "  join agenda.paciente pac on pac.rut = c.rut_paciente where hd.idhojadiaria = " + idhojadiaria + "; ");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                hd.setOtras_indicaciones(this.cnn.getRst().getString("folio"));
                hd.setFecha_registro(this.cnn.getRst().getDate("fecharegistro"));

                hd.setOtro_diagnostico(this.cnn.getRst().getString("nombredoctor"));
                hd.setOtro_examen(this.cnn.getRst().getString("rutdoctor"));
                hd.setOtro_procedimiento(this.cnn.getRst().getString("nombrepaciente"));
                hd.setOtrodestino_paciente(this.cnn.getRst().getString("rut"));
                hd.setAuxiliar1(this.cnn.getRst().getString("direccion"));
                hd.setAuxiliar2(this.cnn.getRst().getString("contacto1"));
                hd.setAuxiliar3(this.cnn.getRst().getString("email"));
                hd.setAuxiliar4(this.cnn.getRst().getString("otrodiagnostico"));
                hd.setAuxiliar7(this.cnn.getRst().getString("fecha"));
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return hd;
    }

    public Vector<hojadiaria> BuscarTodoslosPacienteVistosporHojaDiaria(String where) {
        Vector<hojadiaria> vhd = new Vector<hojadiaria>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  Select e.nombreespecialidad , hd.fecharegistro, \n"
                + "                 d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor,\n"
                + "                 d.rut as rutdoctor,\n"
                + "                 pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombrepaciente,\n"
                + "                 pac.rut,\n"
                + "                 pac.direccion,\n"
                + "                 pac.contacto1,\n"
                + "                 pac.email,"
                + "                 case when hd.ges= 1 then 'Ges' else 'No Ges' end as ges,\n"
                + "               dest.nombredestino_paciente,\n"
                + "                 case when hd.otrodiagnostico <> '' then otrodiagnostico  else  '' end as otrodiagnostico,\n"
                + "                 hd.idhojadiaria\n"
                + "                \n"
                + "                FROM \n"
                + "                  agenda.hojadiaria hd inner join \n"
                + "                  agenda.cita c on c.id_cita = hd.idcita\n"
                + "                  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "                  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "                  join agenda.especialidades e on e.id_especialidad= p.id_especialidad\n"
                + "                  join agenda.doctor d on d.rut = p.rut_doctor \n"
                + "                  join agenda.paciente pac on pac.rut = c.rut_paciente "
                + "                join agenda.destinopaciente dest on dest.iddestino_paciente = hd.iddestinopaciente \n"
                + "                  where " + where + " order by hd.fecharegistro DESC ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                hojadiaria hd = new hojadiaria();
                hd.setFecha_registro(this.cnn.getRst().getDate("fecharegistro"));
                hd.setAuxiliar5(this.cnn.getRst().getString("nombreespecialidad"));
                hd.setOtro_diagnostico(this.cnn.getRst().getString("nombredoctor"));
                hd.setOtro_examen(this.cnn.getRst().getString("rutdoctor"));
                hd.setOtro_procedimiento(this.cnn.getRst().getString("nombrepaciente"));
                hd.setOtrodestino_paciente(this.cnn.getRst().getString("rut"));
                hd.setAuxiliar1(this.cnn.getRst().getString("direccion"));
                hd.setAuxiliar2(this.cnn.getRst().getString("contacto1"));
                hd.setAuxiliar3(this.cnn.getRst().getString("email"));
                hd.setAuxiliar4(this.cnn.getRst().getString("otrodiagnostico"));
                hd.setAuxiliar6(this.cnn.getRst().getString("ges"));
                hd.setAuxiliar7(this.cnn.getRst().getString("nombredestino_paciente"));

                hd.setIdhojadiaria(this.cnn.getRst().getInt("idhojadiaria"));
                vhd.add(hd);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vhd;

    }

    public Vector<lugar> buscarDiagnosticosdeHojaparaIPsinblancos(int hojadiaria) {
        Vector<lugar> vl = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select d.nombre_diagnostico,  de.iddiagnodtivo_especialidad from agenda.diagnostico_hojadiaria dh inner join \n"
                + "  agenda.hojadiaria h on dh.idhojadiaria = h.idhojadiaria\n"
                + "  join agenda.diagnostico_especialidad de on de.iddiagnodtivo_especialidad = dh.iddiagnostico_especialdad\n"
                + "  join agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico\n"
                + "  where dh.idhojadiaria = " + hojadiaria + "");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setDiagnostico(this.cnn.getRst().getString("nombre_diagnostico"));
                l.setId_diagnostico_urgencia(this.cnn.getRst().getInt("iddiagnodtivo_especialidad"));
                vl.add(l);
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vl;
    }

    public Vector<lugar> buscarlosdiagnosticosdeunaHojaDiaria(int hojadiaria) {
        Vector<lugar> vl = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select d.nombre_diagnostico from agenda.diagnostico_hojadiaria dh inner join \n"
                + "  agenda.hojadiaria h on dh.idhojadiaria = h.idhojadiaria\n"
                + "  join agenda.diagnostico_especialidad de on de.iddiagnodtivo_especialidad = dh.iddiagnostico_especialdad\n"
                + "  join agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico\n"
                + "  where dh.idhojadiaria = " + hojadiaria + ""
                + " union \n"
                + "  select case when hd.otrodiagnostico != '' then hd.otrodiagnostico else ''\n"
                + " end as nombre_diagnostico  from agenda.hojadiaria hd   where hd.idhojadiaria = " + hojadiaria + "");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setDiagnostico(this.cnn.getRst().getString("nombre_diagnostico"));
                vl.add(l);
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vl;
    }

    public String buscarlosdiagnosticosdeunaHojaDiariareporte(int hojadiaria) {
        String diagnostico = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select d.nombre_diagnostico from agenda.diagnostico_hojadiaria dh inner join \n"
                + "  agenda.hojadiaria h on dh.idhojadiaria = h.idhojadiaria\n"
                + "  join agenda.diagnostico_especialidad de on de.iddiagnodtivo_especialidad = dh.iddiagnostico_especialdad\n"
                + "  join agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico\n"
                + "  where dh.idhojadiaria = " + hojadiaria + ""
                + " union \n"
                + "  select case when hd.otrodiagnostico != '' then hd.otrodiagnostico else ''\n"
                + " end as nombre_diagnostico  from agenda.hojadiaria hd   where hd.idhojadiaria = " + hojadiaria + "");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                diagnostico = diagnostico + this.cnn.getRst().getString("nombre_diagnostico") + "-";

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return diagnostico;
    }

    public String buscarProcedimientosdeUnaHojaReporte(int idhojadiaria) {
        String procedimiento = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select p.nombre_procedimiento from \n"
                + "  agenda.procedimiento_hojadiaria ph inner join \n"
                + "  agenda.hojadiaria h on ph.idhojadiaria = h.idhojadiaria\n"
                + "  join agenda.procedimiento_especialidad pe on pe.idprocedimiento_especialidad = ph.idprocedimiento_especialidad\n"
                + "  join agenda.procedimiento p on pe.idprocedimiento =  p.idprocedimiento\n"
                + "  where ph.idhojadiaria = " + idhojadiaria + " and ph.tipoatencion= 1");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                procedimiento = procedimiento + this.cnn.getRst().getString("nombre_procedimiento") + "-";
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return procedimiento;
    }

    public Vector<lugar> buscarProcedimientosdeUnaHoja(int idhojadiaria) {
        Vector<lugar> vl = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select p.nombre_procedimiento from \n"
                + "  agenda.procedimiento_hojadiaria ph inner join \n"
                + "  agenda.hojadiaria h on ph.idhojadiaria = h.idhojadiaria\n"
                + "  join agenda.procedimiento_especialidad pe on pe.idprocedimiento_especialidad = ph.idprocedimiento_especialidad\n"
                + "  join agenda.procedimiento p on pe.idprocedimiento =  p.idprocedimiento\n"
                + "  where ph.idhojadiaria = " + idhojadiaria + " and ph.tipoatencion= 1");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setDiagnostico(this.cnn.getRst().getString("nombre_procedimiento"));
                vl.add(l);
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vl;
    }

    /*buscar examenes de una entrevista*/
 /*procedimeitnos examenes*/
    public Vector<lugar> buscarExamenesdeunaentrevista(int idntrevista) {
        Vector<lugar> vl = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT \n"
                + " e.nombre_examenes\n"
                + "FROM \n"
                + "  agenda.entrevista_examenes ex inner join agenda.examenes_especialidad\n"
                + "  ee on ex.idexamenes_especialidad = ee.idexamen_especialidad\n"
                + "  join agenda.examenes e on e.id_examenes = ee.idexamenes\n"
                + "   where ex.identrevista = " + idntrevista + " ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setDiagnostico(this.cnn.getRst().getString("nombre_examenes"));
                vl.add(l);
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vl;
    }

    /*procedimeitnos examenes*/
    public Vector<lugar> buscarExamenesdeUnaHoja(int idhojadiaria) {
        Vector<lugar> vl = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select e.nombre_examenes from \n"
                + "  agenda.hojadiaria_examenes he inner join \n"
                + "  agenda.hojadiaria h on he.idhojadiaria = h.idhojadiaria\n"
                + "  join agenda.examenes_especialidad ee on he.idexamenes_especialidades = ee.idexamen_especialidad\n"
                + "  join agenda.examenes e on e.id_examenes = ee.idexamenes\n"
                + "  where he.idhojadiaria = " + idhojadiaria + " and he.tipoatencion= 1");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setDiagnostico(this.cnn.getRst().getString("nombre_examenes"));
                vl.add(l);
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vl;
    }

    public String buscarExamenesdeUnaHojaReporte(int idhojadiaria) {
        String examenes = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select e.nombre_examenes from \n"
                + "  agenda.hojadiaria_examenes he inner join \n"
                + "  agenda.hojadiaria h on he.idhojadiaria = h.idhojadiaria\n"
                + "  join agenda.examenes_especialidad ee on he.idexamenes_especialidades = ee.idexamen_especialidad\n"
                + "  join agenda.examenes e on e.id_examenes = ee.idexamenes\n"
                + "  where he.idhojadiaria = " + idhojadiaria + " and he.tipoatencion= 1");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                examenes = this.cnn.getRst().getString("nombre_examenes");

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return examenes;
    }

    /*buscar datos para SIC*/
    public lugar buscarDatosparaSICporhojaDiaria(int hojadiaria) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select 'SIC-'|| to_char( hd.fecharegistro, 'yyyymmdd')|| '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'||  case when char_length(hd.idhojadiaria)>4 then cast(hd.idhojadiaria as text) else to_char(hd.idhojadiaria, '0000') end   as folio, \n"
                + "to_char(hd.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha, \n"
                + "  pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombre   \n"
                + "  , pac.rut, to_char( pac.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,\n"
                + "  to_char(age(pac.fecha_nacimiento),'yy') as edad,   \n"
                + "  case when pac.genero =1 then 'Femenino'else 'Masculino' end as sexo, pac.direccion,  \n"
                + "  co.nombre as comuna, pac.contacto1, pac.email, \n"
                + "  dp.nombredestino_paciente as destino,\n"
                + "  d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor, \n"
                + "  d.rut as rutdoctor ,  hd.otrodestinopaciente, hd.otrodiagnostico,\n"
                + "  hd.otroexamenes, hd.otroindicaciones  , e.nombreespecialidad, paraquesederive ,\n"
                + "                 case when hd.diagnosticosic = -1 then hd.otrodiagnosticosic else ( select d.nombre_diagnostico from agenda.diagnostico_especialidad de  \n"
                + "                 inner join agenda.diagnosticos d on de.iddiagnostico = d.iddiagnostico\n"
                + "                 where de.iddiagnodtivo_especialidad = hd.diagnosticosic) end as diagnostico\n"
                + "   FROM \n"
                + "  agenda.hojadiaria hd inner join \n"
                + "  agenda.destinopaciente dp on dp.iddestino_paciente = hd.iddestinopaciente\n"
                + "  join agenda.cita c on c.id_cita = hd.idcita\n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.especialidades e on e.id_especialidad= p.id_especialidad\n"
                + "  join agenda.doctor d on d.rut = p.rut_doctor \n"
                + "  join agenda.paciente pac on pac.rut = c.rut_paciente\n"
                + "  join agenda.comuna co on co.id_comuna = pac.id_comuna   where hd.idhojadiaria = " + hojadiaria + "; ");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                l.setFolio(this.cnn.getRst().getString("folio"));
                l.setDescripcion(this.cnn.getRst().getString("fecha"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                l.setRut(this.cnn.getRst().getString("rut"));
                l.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                l.setEdad(this.cnn.getRst().getString("edad"));
                l.setSexo(this.cnn.getRst().getString("sexo"));
                l.setDireccion(this.cnn.getRst().getString("direccion"));
                l.setComuna(this.cnn.getRst().getString("comuna"));
                l.setTelefono1(this.cnn.getRst().getString("contacto1"));
                l.setEmail(this.cnn.getRst().getString("email"));
                l.setDetalle_de_la_derivacion(this.cnn.getRst().getString("destino"));
                l.setNombredoctor(this.cnn.getRst().getString("nombredoctor"));
                l.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                l.setIntervencio(this.cnn.getRst().getString("otrodestinopaciente"));
                l.setExamenesrealizados(this.cnn.getRst().getString("otroexamenes"));
                l.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                l.setPrescripcion(this.cnn.getRst().getString("nombreespecialidad"));
                l.setSeeviapara(this.cnn.getRst().getString("paraquesederive"));

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return l;
    }

    public lugar BuscardatosparaSoliciatudpabellon(int hojadiaria) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select  to_char(hd.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha, \n"
                + "                 pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombre   \n"
                + "                  , pac.rut, to_char( pac.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,\n"
                + "                 to_char(age(pac.fecha_nacimiento),'yy') as edad,   \n"
                + "                  case when pac.genero =1 then 'Femenino'else 'Masculino' end as sexo, pac.direccion,  \n"
                + "                  d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor, \n"
                + "                  d.rut as rutdoctor , hd.otrodestinopaciente, hd.otrodiagnostico,\n"
                + "                  hd.otroexamenes, hd.otroindicaciones , e.nombreespecialidad ,d.profesion\n"
                + "                  FROM\n"
                + "                  agenda.hojadiaria hd inner join \n"
                + "                 agenda.cita c on c.id_cita = hd.idcita\n"
                + "                join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "                  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "                  join agenda.especialidades e on e.id_especialidad= p.id_especialidad\n"
                + "                 join agenda.doctor d on d.rut = p.rut_doctor \n"
                + "                 join agenda.paciente pac on pac.rut = c.rut_paciente\n"
                + "                 join agenda.comuna co on co.id_comuna = pac.id_comuna  \n"
                + "                  where hd.idhojadiaria = " + hojadiaria + ";");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {

                l.setDescripcion(this.cnn.getRst().getString("fecha"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                l.setRut(this.cnn.getRst().getString("rut"));
                l.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                l.setEdad(this.cnn.getRst().getString("edad"));
                l.setSexo(this.cnn.getRst().getString("sexo"));
                l.setDireccion(this.cnn.getRst().getString("direccion"));

                l.setNombredoctor(this.cnn.getRst().getString("nombredoctor"));
                l.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                l.setIntervencio(this.cnn.getRst().getString("otrodestinopaciente"));
                l.setExamenesrealizados(this.cnn.getRst().getString("otroexamenes"));
                l.setDiagnostico(this.cnn.getRst().getString("otrodiagnostico"));
                l.setPrescripcion(this.cnn.getRst().getString("nombreespecialidad"));
                l.setSederivapara(this.cnn.getRst().getString("profesion"));

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return l;
    }

    public lugar BuscarIpdPARAhojaDiaria(int hojadiaria) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select 'IPD-'|| to_char( hd.fecharegistro, 'yyyymmdd')|| '-'|| substring(e.nombreespecialidad from 1 for 3)||'-'||  case when char_length(hd.idhojadiaria)>4 then cast(hd.idhojadiaria as text) else to_char(hd.idhojadiaria, '0000') end  as folio, \n"
                + "to_char(hd.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha, \n"
                + " pac.nombre ||' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombre   \n"
                + "  , pac.rut, to_char( pac.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,\n"
                + " to_char(age(pac.fecha_nacimiento),'yy') as edad,   \n"
                + "  case when pac.genero =1 then 'Femenino'else 'Masculino' end as sexo, pac.direccion,  \n"
                + "  d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor, \n"
                + "  d.rut as rutdoctor , hd.otrodestinopaciente, hd.otrodiagnostico,\n"
                + "  hd.otroexamenes, hd.otroindicaciones , e.nombreespecialidad ,d.profesion,  hd.fundamento , hd.tratamiento\n"
                + "  FROM \n"
                + "  agenda.hojadiaria hd inner join \n"
                + "   agenda.cita c on c.id_cita = hd.idcita\n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.especialidades e on e.id_especialidad= p.id_especialidad\n"
                + "  join agenda.doctor d on d.rut = p.rut_doctor \n"
                + "  join agenda.paciente pac on pac.rut = c.rut_paciente\n"
                + "  join agenda.comuna co on co.id_comuna = pac.id_comuna  \n"
                + "   where hd.idhojadiaria = " + hojadiaria + ";");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                l.setFolio(this.cnn.getRst().getString("folio"));
                l.setDescripcion(this.cnn.getRst().getString("fecha"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                l.setRut(this.cnn.getRst().getString("rut"));
                l.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                l.setEdad(this.cnn.getRst().getString("edad"));
                l.setSexo(this.cnn.getRst().getString("sexo"));
                l.setDireccion(this.cnn.getRst().getString("direccion"));

                l.setNombredoctor(this.cnn.getRst().getString("nombredoctor"));
                l.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                l.setIntervencio(this.cnn.getRst().getString("otrodestinopaciente"));
                l.setExamenesrealizados(this.cnn.getRst().getString("otroexamenes"));
                l.setDiagnostico(this.cnn.getRst().getString("otrodiagnostico"));
                l.setPrescripcion(this.cnn.getRst().getString("nombreespecialidad"));
                l.setSederivapara(this.cnn.getRst().getString("profesion"));
                /*solo para cataratas*/
                l.setVariable1(this.cnn.getRst().getString("fundamento"));
                l.setVariable2(this.cnn.getRst().getString("tratamiento"));

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return l;
    }

    public hojadiaria BuscarHojaDiaria(int hojadiaria) {
        hojadiaria hd = new hojadiaria();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                 idhojadiaria,\n"
                + "                  idcita,\n"
                + "                  fecharegistro,\n"
                + "                  sic,\n"
                + "                 ges,\n"
                + "                 otrodiagnostico,\n"
                + "                 otroprocedimiento,\n"
                + "                 otroexamenes,\n"
                + "                  otroindicaciones,\n"
                + "                 iddestinopaciente,\n"
                + "                  otrodestinopaciente,\n"
                + "                  solicitudpabellon,\n"
                + "                  paraquesederive, constanciages,\n"
                + "                 case when iddestinopaciente = 0 then '' else  dp.nombredestino_paciente end as nombredestino_paciente \n"
                + "                FROM \n"
                + "                  agenda.hojadiaria inner join \n"
                + "                  agenda.destinopaciente dp on dp.iddestino_paciente=  iddestinopaciente\n"
                + "                   where idhojadiaria= " + hojadiaria + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                hd.setIdhojadiaria(this.cnn.getRst().getInt("idhojadiaria"));
                hd.setIdcita(this.cnn.getRst().getInt("idcita"));
                hd.setGes(this.cnn.getRst().getInt("ges"));
                hd.setSic(this.cnn.getRst().getInt("sic"));
                hd.setFecha_registro(this.cnn.getRst().getDate("fecharegistro"));
                hd.setOtras_indicaciones(this.cnn.getRst().getString("otroindicaciones"));
                hd.setOtro_diagnostico(this.cnn.getRst().getString("otrodiagnostico"));
                hd.setOtro_procedimiento(this.cnn.getRst().getString("otroprocedimiento"));
                hd.setIddestinopaciente(this.cnn.getRst().getInt("iddestinopaciente"));
                hd.setOtro_examen(this.cnn.getRst().getString("otroexamenes"));
                hd.setSolicitudpabellon(this.cnn.getRst().getBoolean("solicitudpabellon"));
                hd.setParaquesederive(this.cnn.getRst().getString("paraquesederive"));
                hd.setOtrodestino_paciente(this.cnn.getRst().getString("otrodestinopaciente"));
                hd.setConstancia(this.cnn.getRst().getInt("constanciages"));
                hd.setAuxiliar1(this.cnn.getRst().getString("nombredestino_paciente"));
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return hd;
    }

    /*registrar la entrevistapreoperatoria*/
    public void insertarEntrevistaPreoperatoria(entrevista_preoperatoria e) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.entrevista_preoperatoria\n"
                + "(\n"
                + "  idcita,\n"
                + "  fecha_registro,\n"
                + "  estatus,\n"
                + "  diagnostico,\n"
                + "  cirugia_propuesta,\n"
                + "  peso,\n"
                + "  talla,\n"
                + "  imc,\n"
                + "  pa,\n"
                + "  fia_cardiaca,\n"
                + "  sat,\n"
                + "  asma,\n"
                + "  tuberculosis,\n"
                + "  tabaco,\n"
                + "  ira,\n"
                + "  renitis_alergica,\n"
                + "  desde,\n"
                + "  annos,\n"
                + "  diabete,\n"
                + "  hipo_hipertiroidismo,\n"
                + "  alergias,\n"
                + "  obesidad,\n"
                + "  ulcera_gastritis,\n"
                + "  rge,\n"
                + "  cualesalergias,\n"
                + "  coronario,\n"
                + "  valvular,\n"
                + "  arritmias,\n"
                + "  marcapasos,\n"
                + "  coagulapatia,\n"
                + "  dislipidemia,\n"
                + "  ecv,\n"
                + "  esquizofrenia,\n"
                + "  depresion,\n"
                + "  epilepsia,\n"
                + "  alcohol,\n"
                + "  otrasenfpsiq,\n"
                + "  netropatia,\n"
                + "  infeccion_urinaria,\n"
                + "  uraoatia_obstructiva,\n"
                + "  intervencion,\n"
                + "  antecedentes_anastesicos,\n"
                + "  especificar,\n"
                + "  tratamiento,\n"
                + "  observaciones,"
                + "hta,\n"
                + "  paretro,\n"
                + "  cardiacaretro,\n"
                + "  satretro ,\n"
                + "  especificacionalcohol,"
                + "  reddeapoyo, pertinencia, pase\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + e.getIdCita() + ",\n"
                + "  CURRENT_TIMESTAMP,\n"
                + "  1,\n"
                + "  '" + e.getDiagnostico() + "',\n"
                + "  '" + e.getCirugia_propuesta() + "',\n"
                + "  '" + e.getPeso() + "',\n"
                + "  '" + e.getTalla() + "',\n"
                + "  '" + e.getImc() + "',\n"
                + "  '" + e.getPa() + "',\n"
                + "  '" + e.getCardiaca() + "',\n"
                + "  '" + e.getSat() + "',\n"
                + "  " + e.getAsma() + ",\n"
                + "  " + e.getTuberculosis() + ",\n"
                + "  " + e.getTabaco() + ",\n"
                + "  " + e.getIra() + ",\n"
                + "  " + e.getRinitis() + ",\n"
                + "  '" + e.getDesde() + "',\n"
                + "  '" + e.getAnos() + "',\n"
                + " " + e.getDiabetes() + ",\n"
                + "  " + e.getHipo() + ",\n"
                + "  " + e.getAlergias() + ",\n"
                + "  " + e.getObesidad() + ",\n"
                + "  " + e.getUlcera() + ",\n"
                + "  " + e.getRge() + ",\n"
                + "  '" + e.getCualesalergias() + "',\n"
                + "  " + e.getCoronario() + ",\n"
                + "  " + e.getValvular() + ",\n"
                + "  " + e.getArritmias() + ",\n"
                + "  " + e.getMarcapasos() + ",\n"
                + "  " + e.getCoagulopatia() + ",\n"
                + "  " + e.getDislipidemia() + ",\n"
                + "  " + e.getEcv() + ",\n"
                + " " + e.getEsquizofrenia() + ",\n"
                + "  " + e.getDepresion() + ",\n"
                + " " + e.getEpilepsia() + " ,\n"
                + "  " + e.getAlcohol() + ",\n"
                + "  '" + e.getOtrasenfpsiq() + "',\n"
                + "  " + e.getNefropatia() + ",\n"
                + "  " + e.getInfeccionurinaria() + ",\n"
                + "  " + e.getUropatiaobstructiva() + ",\n"
                + "  " + e.getIntervencion() + ",\n"
                + "  " + e.getAntecendetes_anestesicos() + ",\n"
                + "  '" + e.getEspecificar() + "',\n"
                + "  '" + e.getTratamieto() + "',\n"
                + "  '" + e.getObservaciones() + "',"
                + "  " + e.getHta() + ","
                + " '" + e.getParetro() + "',"
                + " '" + e.getCardiacaretro() + "',"
                + " '" + e.getSatretro() + "',"
                + "  '" + e.getEspecificacionalcohol() + "',"
                + "  '" + e.getReddeapoyo() + "',"
                + "   -1 , " + e.getPase() + "\n"
                + ");");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    public int idEntrevistaPreoperatoriaporIdCita(int idcita) {
        int id = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  identrevista_preoperatoria\n"
                + "  FROM \n"
                + "  agenda.entrevista_preoperatoria where idcita=" + idcita + ";");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                id = this.cnn.getRst().getInt("identrevista_preoperatoria");
            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return id;
    }

    public Integer modificarEntrevistaPreoperatoria(entrevista_preoperatoria ep) {
        Integer mensaje = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.entrevista_preoperatoria  \n"
                + "SET \n"
                + "  estatus = 2,\n"
                + "  diagnostico = '" + ep.getDiagnostico() + "',\n"
                + "  cirugia_propuesta = '" + ep.getCirugia_propuesta() + "',\n"
                + "  peso = '" + ep.getPeso() + "',\n"
                + "  talla = '" + ep.getTalla() + "',\n"
                + "  imc = '" + ep.getImc() + "',\n"
                + "  pa = '" + ep.getPa() + "',\n"
                + "  fia_cardiaca = '" + ep.getCardiaca() + "',\n"
                + "  sat = '" + ep.getSat() + "',\n"
                + "  asma = " + ep.getAsma() + ",\n"
                + "  tuberculosis = " + ep.getTuberculosis() + ",\n"
                + "  tabaco = " + ep.getTabaco() + ",\n"
                + "  ira = " + ep.getIra() + ",\n"
                + "  renitis_alergica = " + ep.getRinitis() + ",\n"
                + "  desde = '" + ep.getDesde() + "',\n"
                + "  annos = '" + ep.getAnos() + "',\n"
                + "  diabete = " + ep.getDiabetes() + ",\n"
                + "  hipo_hipertiroidismo = " + ep.getHipo() + ",\n"
                + "  alergias = " + ep.getAlergias() + ",\n"
                + "  obesidad = " + ep.getObesidad() + ",\n"
                + "  ulcera_gastritis = " + ep.getUlcera() + ",\n"
                + "  rge = " + ep.getRge() + ",\n"
                + "  cualesalergias = '" + ep.getCualesalergias() + "',\n"
                + "  coronario = " + ep.getCoronario() + ",\n"
                + "  valvular = " + ep.getValvular() + ",\n"
                + "  arritmias = " + ep.getArritmias() + ",\n"
                + "  marcapasos = " + ep.getMarcapasos() + ",\n"
                + "  coagulapatia = " + ep.getCoagulopatia() + ",\n"
                + "  dislipidemia = " + ep.getDislipidemia() + ",\n"
                + "  ecv = " + ep.getEcv() + ",\n"
                + "  esquizofrenia = " + ep.getEsquizofrenia() + ",\n"
                + "  depresion = " + ep.getDepresion() + ",\n"
                + "  epilepsia = " + ep.getEpilepsia() + ",\n"
                + "  alcohol = " + ep.getAlcohol() + ",\n"
                + "  otrasenfpsiq = '" + ep.getOtrasenfpsiq() + "',\n"
                + "  netropatia = " + ep.getNefropatia() + ",\n"
                + "  infeccion_urinaria = " + ep.getInfeccionurinaria() + ",\n"
                + "  uraoatia_obstructiva = " + ep.getUropatiaobstructiva() + ",\n"
                + "  intervencion = " + ep.getIntervencion() + ",\n"
                + "  antecedentes_anastesicos = " + ep.getAntecendetes_anestesicos() + ",\n"
                + "  especificar = '" + ep.getEspecificar() + "',\n"
                + "  tratamiento = '" + ep.getTratamieto() + "',\n"
                + "  observaciones = '" + ep.getObservaciones() + "',"
                + "   hta = " + ep.getHta() + ",\n"
                + "  paretro = '" + ep.getParetro() + "',\n"
                + "  cardiacaretro = '" + ep.getCardiacaretro() + "',\n"
                + "  satretro = '" + ep.getSatretro() + "',"
                + "   tengoretro = " + ep.getRetrocontrol() + " ,\n"
                + "  especificacionalcohol= '" + ep.getEspecificacionalcohol() + "' ,"
                + "  reddeapoyo = '" + ep.getReddeapoyo() + "',"
                + "  pertinencia = " + ep.getPertinencia() + ","
                + "descripcion_ventilatorios='" + ep.getDescripcion_ventilatorios() + "',\n"
                + "descripcion_metabolicos='" + ep.getDescripcion_metabolicos() + "', \n"
                + "descripcion_cardiovasculares='" + ep.getDescripcion_cardiovasculares() + "', \n"
                + "descripcionneuro='" + ep.getDescripcionneuro() + "',\n"
                + "descripcion_urinarios='" + ep.getDescripcion_urinarios() + "', \n"
                + "otrosantecedentesmorbidos='" + ep.getOtrosantecedentesmorbidos() + "', \n"
                + "descripcion_antecedentes='" + ep.getDescripcion_antecedentes() + "', \n"
                + "descripcion_examenes='" + ep.getDescripcion_examenes() + "', \n"
                + "ipa= " + ep.getIpa() + ","
                + "pase=" + ep.getPase() + "\n"
                + " \n"
                + "WHERE  identrevista_preoperatoria =" + ep.getIdentrevista_preoperatoria() + " ;");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = 1;
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public entrevista_preoperatoria buscarEntrevista(int identrevista) {
        entrevista_preoperatoria ep = new entrevista_preoperatoria();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre  ,\n"
                + "               p.rut,\n"
                + "                 to_char(age(p.fecha_nacimiento),'yy') as edad,\n"
                + "                p.contacto1,\n"
                + "                ep.fecha_registro,\n"
                + "               COALESCE(diagnostico,'') as diagnostico,\n"
                + "                 COALESCE( cirugia_propuesta,'') as cirugia_propuesta,\n"
                + "                 peso,\n"
                + "                talla,\n"
                + "                 imc,\n"
                + "                 pa,\n"
                + "                fia_cardiaca,\n"
                + "                 sat,\n"
                + "                  asma,\n"
                + "                  tuberculosis,\n"
                + "                 tabaco,\n"
                + "                  ira,\n"
                + "                 renitis_alergica,\n"
                + "                 COALESCE( desde ,'') as desde,\n"
                + "                 annos,\n"
                + "                diabete,\n"
                + "                hipo_hipertiroidismo,\n"
                + "                 alergias,\n"
                + "                 obesidad,\n"
                + "               ulcera_gastritis,\n"
                + "                rge,\n"
                + "                COALESCE (cualesalergias, '') AS cualesalergias,\n"
                + "                  coronario,\n"
                + "                 valvular,\n"
                + "                 arritmias,\n"
                + "                marcapasos,\n"
                + "                 coagulapatia,\n"
                + "                 dislipidemia,\n"
                + "                  ecv,\n"
                + "                esquizofrenia,\n"
                + "                 depresion,\n"
                + "                 epilepsia,\n"
                + "                 alcohol,\n"
                + "                  COALESCE (otrasenfpsiq, '') AS otrasenfpsiq,\n"
                + "                 netropatia,\n"
                + "                  infeccion_urinaria,\n"
                + "                 uraoatia_obstructiva,\n"
                + "                 intervencion,\n"
                + "                 antecedentes_anastesicos,\n"
                + "                 COALESCE (especificar, '') AS especificar,\n"
                + "                  COALESCE (tratamiento, '') AS tratamiento,\n"
                + "                 COALESCE (observaciones, '') AS observaciones,\n"
                + "                 doc.nombre||' '||doc.apellido_paterno||' '||doc.apellido_materno as nombredoctor,\n"
                + "                 doc.rut as rutdoctor,\n"
                + "                 hta,\n"
                + "                 COALESCE(paretro, '') AS paratro,\n"
                + "                  COALESCE(cardiacaretro, '') AS cardiaceretro,\n"
                + "                  COALESCE(satretro, '') AS satretro ,  tengoretro ,especificacionalcohol, COALESCE (ep.reddeapoyo, '') AS reddeapoyo,  COALESCE(ep.pertinencia, -1) as pertinencia ,"
                + "                COALESCE (descripcion_ventilatorios, '') AS descripcion_ventilatorios , \n"
                + "                COALESCE (descripcion_metabolicos, '') as descripcion_metabolicos, COALESCE(descripcion_cardiovasculares,'') as descripcion_cardiovasculares,\n"
                + "                COALESCE( descripcionneuro,'') as descripcionneuro, \n"
                + "                COALESCE (descripcion_urinarios, '') as descripcion_urinarios ,  COALESCE (otrosantecedentesmorbidos, '') as otrosantecedentesmorbidos,  \n"
                + "                COALESCE (descripcion_antecedentes, '') as descripcion_antecedentes, \n"
                + "                COALESCE (descripcion_examenes, '') as descripcion_examenes,  COALESCE (ipa, 0) as ipa,  COALESCE(ep.pase, -1) as pase\n"
                + "                FROM \n"
                + "                agenda.entrevista_preoperatoria ep inner join agenda.cita c\n"
                + "                 on ep.idcita = c.id_cita \n"
                + "                join agenda.paciente p on p.rut = c.rut_paciente\n"
                + "                 join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "                 join agenda.planificar pla on pla.id_planificacion = o.id_plani_sobre\n"
                + "                 join agenda.doctor doc on doc.rut = pla.rut_doctor\n"
                + "                where  identrevista_preoperatoria =" + identrevista + "  ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                ep.setAuxiliar1(this.cnn.getRst().getString("nombre"));
                ep.setAuxiliar2(this.cnn.getRst().getString("rut"));
                ep.setAuxiliar3(this.cnn.getRst().getString("edad"));
                ep.setAuxiliar4(this.cnn.getRst().getString("contacto1"));
                ep.setFecharegistro(this.cnn.getRst().getDate("fecha_registro"));
                ep.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                ep.setCirugia_propuesta(this.cnn.getRst().getString("cirugia_propuesta"));
                ep.setPeso(this.cnn.getRst().getString("peso"));
                ep.setTalla(this.cnn.getRst().getString("talla"));
                ep.setImc(this.cnn.getRst().getString("imc"));
                ep.setPa(this.cnn.getRst().getString("pa"));
                ep.setCardiaca(this.cnn.getRst().getString("fia_cardiaca"));
                ep.setSat(this.cnn.getRst().getString("sat"));
                ep.setAsma(this.cnn.getRst().getInt("asma"));
                ep.setTuberculosis(this.cnn.getRst().getInt("tuberculosis"));
                ep.setTabaco(this.cnn.getRst().getInt("tabaco"));
                ep.setIra(this.cnn.getRst().getInt("ira"));
                ep.setRinitis(this.cnn.getRst().getInt("renitis_alergica"));
                ep.setDesde(this.cnn.getRst().getString("desde"));
                ep.setAnos(this.cnn.getRst().getString("annos"));
                ep.setDiabetes(this.cnn.getRst().getInt("diabete"));
                ep.setHipo(this.cnn.getRst().getInt("hipo_hipertiroidismo"));
                ep.setAlergias(this.cnn.getRst().getInt("alergias"));
                ep.setObesidad(this.cnn.getRst().getInt("obesidad"));
                ep.setUlcera(this.cnn.getRst().getInt("ulcera_gastritis"));
                ep.setRge(this.cnn.getRst().getInt("rge"));
                ep.setCualesalergias(this.cnn.getRst().getString("cualesalergias"));
                ep.setCoronario(this.cnn.getRst().getInt("coronario"));
                ep.setValvular(this.cnn.getRst().getInt("valvular"));
                ep.setArritmias(this.cnn.getRst().getInt("arritmias"));
                ep.setMarcapasos(this.cnn.getRst().getInt("marcapasos"));
                ep.setCoagulopatia(this.cnn.getRst().getInt("coagulapatia"));
                ep.setDislipidemia(this.cnn.getRst().getInt("dislipidemia"));
                ep.setEcv(this.cnn.getRst().getInt("ecv"));
                ep.setEsquizofrenia(this.cnn.getRst().getInt("esquizofrenia"));
                ep.setDepresion(this.cnn.getRst().getInt("depresion"));
                ep.setEpilepsia(this.cnn.getRst().getInt("epilepsia"));
                ep.setAlcohol(this.cnn.getRst().getInt("alcohol"));
                ep.setOtrasenfpsiq(this.cnn.getRst().getString("otrasenfpsiq"));
                ep.setNefropatia(this.cnn.getRst().getInt("netropatia"));
                ep.setInfeccionurinaria(this.cnn.getRst().getInt("infeccion_urinaria"));
                ep.setUropatiaobstructiva(this.cnn.getRst().getInt("uraoatia_obstructiva"));
                ep.setIntervencion(this.cnn.getRst().getInt("intervencion"));
                ep.setAntecendetes_anestesicos(this.cnn.getRst().getInt("antecedentes_anastesicos"));
                ep.setEspecificar(this.cnn.getRst().getString("especificar"));
                ep.setTratamieto(this.cnn.getRst().getString("tratamiento"));
                ep.setObservaciones(this.cnn.getRst().getString("observaciones"));
                ep.setAuxiliar5(this.cnn.getRst().getString("nombredoctor"));
                ep.setAuxiliar6(this.cnn.getRst().getString("rutdoctor"));
                ep.setHta(this.cnn.getRst().getInt("hta"));
                ep.setParetro(this.cnn.getRst().getString("paratro"));
                ep.setCardiacaretro(this.cnn.getRst().getString("cardiaceretro"));
                ep.setSatretro(this.cnn.getRst().getString("satretro"));
                ep.setRetrocontrol(this.cnn.getRst().getInt("tengoretro"));
                ep.setEspecificacionalcohol(this.cnn.getRst().getString("especificacionalcohol"));
                ep.setReddeapoyo(this.cnn.getRst().getString("reddeapoyo"));
                ep.setPertinencia(this.cnn.getRst().getInt("pertinencia"));
                ep.setDescripcion_ventilatorios(this.cnn.getRst().getString("descripcion_ventilatorios"));
                ep.setDescripcion_metabolicos(this.cnn.getRst().getString("descripcion_metabolicos"));
                ep.setDescripcion_cardiovasculares(this.cnn.getRst().getString("descripcion_cardiovasculares"));

                ep.setDescripcionneuro(this.cnn.getRst().getString("descripcionneuro"));
                ep.setDescripcion_urinarios(this.cnn.getRst().getString("descripcion_urinarios"));
                ep.setOtrosantecedentesmorbidos(this.cnn.getRst().getString("otrosantecedentesmorbidos"));
                ep.setDescripcion_antecedentes(this.cnn.getRst().getString("descripcion_antecedentes"));
                ep.setDescripcion_examenes(this.cnn.getRst().getString("descripcion_examenes"));
                ep.setIpa(this.cnn.getRst().getDouble("ipa"));
                ep.setPase(this.cnn.getRst().getInt("pase"));
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return ep;
    }

    public Vector<entrevista_preoperatoria> buscarEntrevistasporFechas(String where) {
        Vector<entrevista_preoperatoria> vp = new Vector<entrevista_preoperatoria>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  identrevista_preoperatoria,\n"
                + "  p.rut,\n"
                + "  p.nombre ||' '||p.apellido_paterno as nombre,\n"
                + "  ep.fecha_registro,\n"
                + "  doc.rut ||' /' || doc.nombre ||' '|| doc.apellido_paterno as doctor\n"
                + "\n"
                + "FROM \n"
                + "  agenda.entrevista_preoperatoria ep INNER JOIN agenda.cita c\n"
                + "  on ep.idcita = c.id_cita\n"
                + "  join agenda.paciente p on p.rut = c.rut_paciente\n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar pla on pla.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.doctor doc on doc.rut = pla.rut_doctor\n"
                + "  where " + where + ";");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                entrevista_preoperatoria e = new entrevista_preoperatoria();
                e.setIdentrevista_preoperatoria(this.cnn.getRst().getInt("identrevista_preoperatoria"));
                e.setAuxiliar1(this.cnn.getRst().getString("rut"));
                e.setAuxiliar2(this.cnn.getRst().getString("nombre"));
                e.setAuxiliar4(this.cnn.getRst().getString("fecha_registro"));
                e.setAuxiliar3(this.cnn.getRst().getString("doctor"));
                vp.add(e);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vp;
    }

    public String insertarSolicitudQuirofanoHojaDiaria(lugar solicitud) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.solicitudquirofanohojadiaria\n"
                + "(\n"
                + "  idhojadiaria,\n"
                + "  iddiagnosticoespecialidad,\n"
                + "  intervencion,\n"
                + "  idanastesia,\n"
                + "  requerimiento,\n"
                + "  observacion, "
                + "  otrodiagnosticosolicitud,"
                + "   tipoatencion ,"
                + "   lateralidad\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + solicitud.getId_atencion() + ",\n"
                + "  " + solicitud.getId_diagnostico_urgencia() + ",\n"
                + "  '" + solicitud.getDescripcion() + "',\n"
                + "  " + solicitud.getId_lugar() + ",\n"
                + "  '" + solicitud.getDiagnostico() + "',\n"
                + "  '" + solicitud.getIndicaciones() + "',"
                + " '" + solicitud.getIntervencio() + "',"
                + "   " + solicitud.getEstatu() + " ,"
                + "  " + solicitud.getGes() + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "registrado";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    public Vector<lugar> buscarLasSolicitudesdePabellondeyunaHojaDiaria(int idhojadiaria) {
        Vector<lugar> vl = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT    idhojadiaria,sq.iddiagnosticoespecialidad,\n"
                + "                  case when sq.iddiagnosticoespecialidad <> '-1'\n"
                + "                  then ( select d.nombre_diagnostico from\n"
                + "                  agenda.diagnostico_especialidad de \n"
                + "                 join agenda.diagnosticos d on de.iddiagnostico = d.iddiagnostico\n"
                + "                 where de.iddiagnodtivo_especialidad = sq.iddiagnosticoespecialidad) \n"
                + "                 else sq.otrodiagnosticosolicitud  end as diagnostico,\n"
                + "                   sq.intervencion,\n"
                + "                  ta.nombre,\n"
                + "                  idanastesia,\n"
                + "                  requerimiento,\n"
                + "                 observacion\n"
                + "                FROM \n"
                + "                  agenda.solicitudquirofanohojadiaria sq inner join\n"
                + "                  pabellon.tipoanestesia ta on ta.idtipoanestesia = sq.idanastesia\n"
                + "                \n"
                + "                 where idhojadiaria =" + idhojadiaria + " and sq.tipoatencion= 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_atencion(this.cnn.getRst().getInt("idhojadiaria"));

                l.setComuna(this.cnn.getRst().getString("diagnostico"));
                l.setIntervencio(this.cnn.getRst().getString("intervencion"));
                l.setId_lugar(this.cnn.getRst().getInt("idanastesia"));
                l.setDiagnostico(this.cnn.getRst().getString("requerimiento"));
                l.setIndicaciones(this.cnn.getRst().getString("observacion"));
                l.setDireccion(this.cnn.getRst().getString("nombre"));
                vl.add(l);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return vl;
    }

}
