/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.controlador_cupos;
import Controlador.controlador_paciente;
import Modelos.atencion;
import Modelos.cita;
import Modelos.lugar;
import static com.lowagie.text.pdf.PdfObject.STRING;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import static java.sql.JDBCType.NUMERIC;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *
 * @author Placoste
 */
public class controlador_cargar_masiva_citas extends General {
    
    public static void leerArchivo(String archivo) throws FileNotFoundException, IOException {
        File myFile = new File(archivo);
        DecimalFormat df = new DecimalFormat("##########");
        FileInputStream fis = new FileInputStream(myFile);
        XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        Iterator<Row> rowIterator = mySheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (cell.getCellType()) {
                    case STRING:
                        System.out.print(cell.getStringCellValue().trim() + "\t");
                        break;
                }
            }
        }   
    }
    
   
    public boolean subirTemporal() {
        boolean mensaje = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("CREATE TEMP TABLE citaMasiva(campo1 String,campo2 String);");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = true;
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }
    public boolean validaDatos() {
        return true;
    }
    public boolean ordenarTemporal() {
        boolean mensaje = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("CREATE TEMP TABLE citaMasiva(campo1 String,campo2 String);");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = true;
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }
    public boolean CrearCitas() {
        return true;
    }
    public boolean CrearBitacora() {
        boolean mensaje = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("CREATE TEMP TABLE citaMasiva(campo1 String,campo2 String);");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = true;
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }
    public boolean CrearDetalle() {
        return true;
    }
    public String leerBitacora(String fechaInicio, String fechatermino) {
        String diagnostico = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from bitacora_masivo where ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                diagnostico = diagnostico + this.cnn.getRst().getString("nombre_diagnostico") + "-";

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return diagnostico;
    }
    public String leerDetalleBitacora() {
        return null;
    }
}
