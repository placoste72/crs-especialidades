/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.procedimiento;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Informatica
 */
public class controlador_procedimiento extends General {

    
    /* buscar procedimiento por id*/
    public procedimiento buscarProcedimientoporId(int id){
        procedimiento p = new procedimiento();
         this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.procedimientos where id = "+id+" ;");
        this.cnn.conectar();
        try {
           if (this.cnn.getRst().next()) {
               
                p.setId(this.cnn.getRst().getInt("id"));
                p.setGrupofonasa(this.cnn.getRst().getString("grupofonasa"));
                p.setSubgrupofonasa(this.cnn.getRst().getString("subgrupofonasa"));
                p.setPrestacionesfonasa(this.cnn.getRst().getString("prestacionesfonasa"));
                p.setDescripcion(this.cnn.getRst().getString("descipcion"));
                p.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));

            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

      
        
        return p;
    }
    
    public String guardarProcedimiento(procedimiento p) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.procedimientos\n"
                + "VALUES (\n"
                + "  (SELECT max(id) + 1 FROM agenda.procedimientos),\n"
                + "  '" + p.getGrupofonasa() + "',\n"
                + "  '" + p.getSubgrupofonasa() + "',\n"
                + "  '" + p.getPrestacionesfonasa() + "',\n"
                + "   '" + p.getDescripcion() + "',\n"
                + "  " + p.getEstatus() + ",\n"
                + "  " + p.getId_especialidad() + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Procedimiento Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public Vector<procedimiento> buscarProcedimiento() {
        Vector<procedimiento> lista = new Vector();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.procedimientos where estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                procedimiento p = new procedimiento();
                p.setId(this.cnn.getRst().getInt("id"));
                p.setGrupofonasa(this.cnn.getRst().getString("grupofonasa"));
                p.setSubgrupofonasa(this.cnn.getRst().getString("subgrupofonasa"));
                p.setPrestacionesfonasa(this.cnn.getRst().getString("prestacionesfonasa"));
                p.setDescripcion(this.cnn.getRst().getString("descipcion"));
                p.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));

                lista.add(p);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

        return lista;

    }

    public String eliminarProcedimiento(int id) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.procedimientos  \n"
                + "SET \n"
                + " estatus = 0\n"
                + " WHERE \n"
                + "  id = " + id + " ; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Procedimiento Eliminado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public String modificarProcedimiento(procedimiento p) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.procedimientos  \n"
                + "SET \n"
                + "  grupofonasa = '"+p.getGrupofonasa()+"',\n"
                + "  subgrupofonasa = '"+p.getSubgrupofonasa()+"',\n"
                + "  prestacionesfonasa = '"+p.getPrestacionesfonasa()+"',\n"
                + "  descipcion = '"+p.getDescripcion()+"',\n"
                + "  id_especialidad = "+p.getId_especialidad()+"\n"
                + " \n"
                + "WHERE \n"
                + "  id = "+p.getId()+"" );
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Procedimiento Modificado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

}
