

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.patologia;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Informatica
 */
public class controlador_patologia extends General {
    
      /* buscar procedimiento por id*/
    public patologia buscarpatologiaporId(int id){
         patologia p = new  patologia();
         this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.patologias where id = "+id+" ;");
        this.cnn.conectar();
        try {
           if (this.cnn.getRst().next()) {
               
                p.setId(this.cnn.getRst().getInt("id"));
               
                p.setDescripcion(this.cnn.getRst().getString("descripcion"));
                p.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));

            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

      
        
        return p;
    }
    
    public String guardarpatologia(patologia p) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.patologias\n"
                + "VALUES (\n"
                + "  (SELECT max(id) + 1 FROM agenda.patologias),\n"
               
                + "   " + p.getEstatus() + ",\n"
                + "  '" + p.getDescripcion() + "',\n"
                + "  " + p.getId_especialidad() + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Causal de la Citacion Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public Vector<patologia> buscarPatologia() {
        Vector<patologia> lista = new Vector();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.patologias where estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                patologia p = new patologia();
                 p.setId(this.cnn.getRst().getInt("id"));
              
                p.setDescripcion(this.cnn.getRst().getString("descripcion"));
                p.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));

                lista.add(p);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

        return lista;

    }
    
    
     public Vector<patologia> buscarPatologiaporEspecialidad(int especialidad) {
        Vector<patologia> lista = new Vector();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT  id, " +
                      " descripcion  FROM agenda .patologias where estatus = 1 and id_especialidad = " + especialidad + "");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                patologia p = new patologia();
                 p.setId(this.cnn.getRst().getInt("id"));
              
                p.setDescripcion(this.cnn.getRst().getString("descripcion"));
                

                lista.add(p);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

        return lista;

    }

    public String eliminarpatologia(int id) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.patologias  \n"
                + "SET \n"
                + " estatus = 0\n"
                + " WHERE \n"
                + "  id = " + id + " ; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Causal de la Citacion Eliminado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public String modificarpatologia(patologia p) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("UPDATE \n"
              +"  agenda.patologias  " 
               +" SET  "
              + " descripcion = '"+p.getDescripcion()+"', "
                + " id_especialidad = "+p.getId_especialidad()+" "
                + "WHERE \n"
                + "  id = "+p.getId()+"" );
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Causal de la Citacion Modificado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }
    
}
