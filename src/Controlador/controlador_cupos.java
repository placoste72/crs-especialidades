/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.cita;
import Modelos.oferta;
import Modelos.planificar;
import Modelos.procedimiento;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Informatica
 */
public class controlador_cupos extends General {

    public Vector<procedimiento> buscarPrestaciones(int especialidad, int atencion) {
        Vector<procedimiento> vp = new Vector<procedimiento>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT \n"
                + "                 oid,\n"
                + "                  codigo,\n"
                + "                  descripcion\n"
                + "                  FROM \n"
                + "                caja.prestacion_especialidad_atencion pe \n"
                + "                inner join caja.prestacion p\n"
                + "                on pe.id_prestacion = p.oid\n"
                + "                join agenda.atencion a on \n"
                + "                pe.id_atencion = a.id_atencion\n"
                + "                join agenda.especialidades e\n"
                + "                on e.id_especialidad = pe.id_especialidad\n"
                + "                 where p.estatus = 1 and e.id_especialidad = " + especialidad + " and a.id_atencion = " + atencion + "\n"
                + "                  order by codigo asc  ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                procedimiento p = new procedimiento();
                p.setId(this.cnn.getRst().getInt("oid"));
                p.setPrestacionesfonasa(this.cnn.getRst().getString("codigo"));
                p.setDescripcion(this.cnn.getRst().getString("descripcion"));

                vp.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    public String insertarcupos(planificar p) throws UnknownHostException {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO "
                + "  agenda.planificar "
                + "VALUES ("
                + "  " + p.getId_planificacion() + " ,"
                + "  '" + p.getRut_doctor() + "',"
                + "  '" + p.getHora_inicio() + "',"
                + "  '" + p.getHora_fin() + "',"
                + "  " + p.getId_atencion() + ","
                + "  " + p.getId_programa() + ","
                + "  " + p.getDuracion() + ","
                + "  " + p.getNumero_cupo() + ","
                + "  " + p.getEstatus() + ","
                + "  '" + p.getFecha() + "',"
                + "  " + p.getId_especialidad() + " ,"
                + "  " + p.getCupos_disponibles() + ", "
                + "  '" + getIpUsuarioLoggin() + "' ,"
                + "  '" + getUsuarioLoggin() + "', "
                + "  0 , "
                + "  " + p.getId_prestacion() + " ,"
                + " '" + p.getHorainiciopabellon() + "',"
                + " '" + p.getHorafinpabellon() + "',"
                + " " + p.getPabellon() + ""
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cupo Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*inserta la cita prestacion*/
    public String insertarCitaPrestacion(int idcita, int idprestacion, int tipo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.cita_prestacion\n"
                + "(\n"
                + "  id_cita,\n"
                + "  id_prestacion,\n"
                + "  fecharegistro,\n"
                + "  tipo\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + idcita + ",\n"
                + " " + idprestacion + " ,\n"
                + "  CURRENT_TIMESTAMP,\n"
                + "  " + tipo + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cupo Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*modificar la oferta para que no vuelva*/
    public void cambiarestatusdeoferta(int id_oferta) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "agenda.oferta  \n"
                + "SET estatus = 0 WHERE  id_oferta = " + id_oferta + "; ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();

    }

    /*cambiar estatus a 2 porque se bloquio la oferta */
    public void cambiarestatusdeofertaporBloqueo(int id_oferta, String motivo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "agenda.oferta "
                + " SET motivo_bloqueo = '" + motivo + "',\n"
                + "  usuario_bloqueo = '" + getUsuarioLoggin() + "', \n"
                + " estatus = 2 WHERE  id_oferta = " + id_oferta + "; ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();

    }

    /* por si cancelan la cita */
    public void cancelaroncambiarestatusdeoferta(int id_oferta) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "agenda.oferta  \n"
                + "SET estatus = 1 WHERE  id_oferta = " + id_oferta + "; ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();

    }
    //borrar las planificaciones de un doctor

    public void eliminarofertas(int idplanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.oferta  \n"
                + "SET \n"
                + "\n"
                + "  estatus = 4\n"
                + "\n"
                + " \n"
                + "WHERE \n"
                + " id_plani_sobre = " + idplanificacion + "; ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void eliminarplanificacion(int idplanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.planificar  \n"
                + "SET \n"
                + "\n"
                + "  estatus = 4,\n"
                + "  usuariobloqueaoelimina = '" + getUsuarioLoggin() + "'\n"
                + "\n"
                + " \n"
                + "WHERE \n"
                + "  id_planificacion = " + idplanificacion + " ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void eliminarsobleplanificacion(int idplanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.sobreplanificar  \n"
                + "SET \n"
                + "\n"
                + "  estatus = 4\n"
                + "\n"
                + "WHERE \n"
                + "  id_planificar = " + idplanificacion + "\n"
                + ";");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public Integer BuscarPrestacionporCodigo(String codigo) {
        int idprestacion = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  oid\n"
                + "  FROM \n"
                + "  caja.prestacion where codigo = '" + codigo + "' and estatus = 1 ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                idprestacion = this.cnn.getRst().getInt("oid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return idprestacion;
    }

    public planificar buscarplanificacionytipo(int oferta) {
        planificar p = new planificar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_plani_sobre, procede , p.id_prestacion , p.id_atencion\n"
                + "FROM  agenda.oferta o inner join \n"
                + " agenda.planificar p on p.id_planificacion = o.id_plani_sobre where id_oferta = " + oferta + "; ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                p.setId_planificacion(this.cnn.getRst().getInt("id_plani_sobre"));
                p.setEstatus(this.cnn.getRst().getInt("procede"));
                p.setId_prestacion(this.cnn.getRst().getInt("id_prestacion"));
                p.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    // buscar el ultimo cupo
    public int buscarultimaplanificacion() {
        int idplanificacion = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT  max(id_planificacion) as planificacion FROM agenda.planificar;  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                idplanificacion = this.cnn.getRst().getInt("planificacion");
            } else {
                idplanificacion = 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return idplanificacion;
    }

// duplicar cupo
    public void insertarsobreplanificacion(planificar p) throws UnknownHostException {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO "
                + "  agenda.sobreplanificar  "
                + "  VALUES ("
                + "  " + p.getId_planificacion() + "   ,"
                + "  '" + p.getFecha() + "',"
                + "  '" + p.getHora_inicio() + "',"
                + "  '" + p.getHora_fin() + "',"
                + "  " + p.getId_atencion() + ","
                + "  '" + p.getRut_doctor() + "',"
                + "  " + p.getId_programa() + ","
                + "  " + p.getNumero_cupo() + ","
                + "  " + p.getId_planificacion() + ","
                + "  " + p.getEstatus() + ","
                + "  " + p.getDuracion() + ", "
                + "   " + p.getId_especialidad() + ","
                + "   0 ,"
                + "  '" + getIpUsuarioLoggin() + "' ,"
                + "  '" + getUsuarioLoggin() + "', "
                + "   " + p.getNumero_cupo() + " , "
                + "  " + p.getId_prestacion() + ""
                + ")");
        this.cnn.conectar();
        this.cnn.cerrarConexion();

    }

    public int duracionparaAgendamientoporBloque(planificar p) {
        int inicioMinutos = 0;
        int inicioHoras = 0;
        int finMinutos = 0;
        int finHoras = 0;
        int transcurridoMinutos;
        int transcurridoHoras;
        int horaminutos;
        int totalminutos;
        int dura;
        String horas;
        String minutos;

        inicioMinutos = Integer.parseInt(p.getHora_inicio().substring(3, 5));
        inicioHoras = Integer.parseInt(p.getHora_inicio().substring(0, 2));

        finMinutos = Integer.parseInt(p.getHora_fin().substring(3, 5));
        finHoras = Integer.parseInt(p.getHora_fin().substring(0, 2));

        transcurridoMinutos = finMinutos - inicioMinutos;
        transcurridoHoras = finHoras - inicioHoras;

        if (transcurridoMinutos < 0) {
            transcurridoHoras--;
            transcurridoMinutos = 60 + transcurridoMinutos;
        }

        horas = String.valueOf(transcurridoHoras);
        minutos = String.valueOf(transcurridoMinutos);

        if (horas.length() < 2) {
            horas = "0" + horas;
        }

        if (minutos.length() < 2) {
            minutos = "0" + minutos;
        }

        // debo llevar todo a minutos y dividir por la duracion
        horaminutos = Integer.parseInt(horas) * 60;

        totalminutos = horaminutos + Integer.parseInt(minutos);

        dura = p.getDuracion();

        int totalcupos = totalminutos / dura;
        return Math.round(totalcupos);

    }

    public void guardarofertasparaBloquesdePlanificacion(planificar p, int tipo, int estatus) {
        ArrayList horas = new ArrayList();
        int cupo = p.getNumero_cupo();
        Date fecha = p.getFecha();
        Vector<oferta> horascirugia = horasparaTablaquirurgica(p);
        // int inicioMinutos =Integer.parseInt(p.getHora_inicio().substring(3, 2));
        String hor = p.getHora_inicio();
        String hora = p.getHora_inicio().substring(0, 2);
        String minutos = p.getHora_inicio().substring(3, 5);
        int uno = Integer.parseInt(hora);
        int dos = Integer.parseInt(minutos);

        int duracion = p.getDuracion();
        Calendar sumar = Calendar.getInstance();
        sumar.setTime(fecha);
        sumar.add(Calendar.HOUR, uno);
        sumar.add(Calendar.MINUTE, dos);
        horas.add(sumar);
        int h = sumar.get(Calendar.HOUR_OF_DAY);
        int m = sumar.get(Calendar.MINUTE);
        int s = sumar.get(Calendar.SECOND);
        String tres = h + ":" + m + ":" + s;
        for (int k = 0; k < cupo; ++k) {
            registraroperaciones(p.getId_planificacion(), tres, tipo, estatus, horascirugia.get(0).getHora());
        }
        int cuposmayores = duracionparaAgendamientoporBloque(p);
        for (int i = 1; i < cuposmayores; ++i) {
            sumar.add(Calendar.MINUTE, duracion);
            horas.add(sumar);
            h = sumar.get(Calendar.HOUR_OF_DAY);
            m = sumar.get(Calendar.MINUTE);
            s = sumar.get(Calendar.SECOND);
            tres = h + ":" + m + ":" + s;
            for (int k = 0; k < cupo; ++k) {
                registraroperaciones(p.getId_planificacion(), tres, tipo, estatus, horascirugia.get(k).getHora());
            }
        }

    }

    /*modificar la planificacion para cupos y cupos disponibles*/
    public void updatePlanificacionCuposydisponible(planificar p) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.planificar  \n"
                + "SET \n"
                + "\n"
                + "  cupos =  " + p.getNumero_cupo() + ",\n"
                + " \n"
                + "  cupos_disponibles = " + p.getCupos_disponibles() + "\n"
                + " \n"
                + " \n"
                + "WHERE \n"
                + "  id_planificacion = " + p.getId_planificacion() + "\n"
                + ";");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void updateSobreplanificacionCuposydisponible(planificar p) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.sobreplanificar   \n"
                + "SET \n"
                + "\n"
                + "  cupos =  " + p.getNumero_cupo() + ",\n"
                + " \n"
                + "  cupos_disponibles = " + p.getCupos_disponibles() + "\n"
                + " \n"
                + " \n"
                + "WHERE \n"
                + "  id_sobrecupo = " + p.getId_planificacion() + "\n"
                + ";");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }


    /*calcular las ofertas deben ser el total de cupos y las horas*/
    public void guardarofertasporplanificacion(planificar p, int tipo, int estatus) {
        ArrayList horas = new ArrayList();
        Vector<oferta> horascirugia = horasparaTablaquirurgica(p);
        int cupo = p.getNumero_cupo();
        Date fecha = p.getFecha();
        // int inicioMinutos =Integer.parseInt(p.getHora_inicio().substring(3, 2));
        String hor = p.getHora_inicio();
        String hora = p.getHora_inicio().substring(0, 2);
        String minutos = p.getHora_inicio().substring(3, 5);
        int uno = Integer.parseInt(hora);
        int dos = Integer.parseInt(minutos);

        int duracion = p.getDuracion();
        Calendar sumar = Calendar.getInstance();
        sumar.setTime(fecha);
        sumar.add(Calendar.HOUR, uno);
        sumar.add(Calendar.MINUTE, dos);
        horas.add(sumar);
        int h = sumar.get(Calendar.HOUR_OF_DAY);
        int m = sumar.get(Calendar.MINUTE);
        int s = sumar.get(Calendar.SECOND);
        String tres = h + ":" + m + ":" + s;
        registraroperaciones(p.getId_planificacion(), tres, tipo, estatus, horascirugia.get(0).getHora().toString());
        for (int i = 1; i < cupo; ++i) {
            sumar.add(Calendar.MINUTE, duracion);
            horas.add(sumar);
            h = sumar.get(Calendar.HOUR_OF_DAY);
            m = sumar.get(Calendar.MINUTE);
            s = sumar.get(Calendar.SECOND);
            tres = h + ":" + m + ":" + s;
            registraroperaciones(p.getId_planificacion(), tres, tipo, estatus, horascirugia.get(i).getHora());
        }
    }

    /*calcular hora para pabellon*/
    public Vector<oferta> horasparaTablaquirurgica(planificar p) {
        ArrayList horas = new ArrayList();
        Vector<oferta> hotasquirurgicas = new Vector<oferta>();
        int cupo = p.getNumero_cupo();
        Date fecha = p.getFecha();
        // int inicioMinutos =Integer.parseInt(p.getHora_inicio().substring(3, 2));
        String hor = p.getHorainiciopabellon();
        String hora = p.getHorainiciopabellon().substring(0, 2);
        String minutos = p.getHorainiciopabellon().substring(3, 5);
        int uno = Integer.parseInt(hora);
        int dos = Integer.parseInt(minutos);

        int duracion = p.getDuracion();
        Calendar sumar = Calendar.getInstance();
        sumar.setTime(fecha);
        sumar.add(Calendar.HOUR, uno);
        sumar.add(Calendar.MINUTE, dos);
        horas.add(sumar);
        int h = sumar.get(Calendar.HOUR_OF_DAY);
        int m = sumar.get(Calendar.MINUTE);
        int s = sumar.get(Calendar.SECOND);
        oferta o = new oferta();
        String tres = h + ":" + m + ":" + s;
        o.setHora(tres);
        hotasquirurgicas.add(o);
        for (int i = 1; i < cupo; ++i) {
            oferta ot = new oferta();
            sumar.add(Calendar.MINUTE, duracion);
            horas.add(sumar);
            h = sumar.get(Calendar.HOUR_OF_DAY);
            m = sumar.get(Calendar.MINUTE);
            s = sumar.get(Calendar.SECOND);
            tres = h + ":" + m + ":" + s;
            ot.setHora(tres);
            hotasquirurgicas.add(ot);
        }
        return hotasquirurgicas;
    }

    /*buscando las ofertas depende de la atencion la fecha el doctor y la especialdiad*/
    public Vector<planificar> buscarofertas(int atencion, Date fecha, String doctor, int especialidad) {
        Vector<planificar> oferta = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select \n"
                + "o.id_oferta , to_char(o.hora,'HH24:MI:SS')  ||'-'  as hora, o.procede from agenda.planificar p inner join agenda.oferta o\n"
                + "on p.id_planificacion = o.id_plani_sobre\n"
                + "where p.rut_doctor = '" + doctor + "' and\n"
                + "p.fecha ='" + fecha + "' and p.id_atencion = " + atencion + " and p.id_especialidad = " + especialidad + "\n"
                + "and p.estatus = 1 and p.cupos_disponibles > 0 and o.estatus = 1 and o.procede = 1 \n"
                + "\n"
                + "UNION\n"
                + "\n"
                + "select \n"
                + "o.id_oferta , to_char(o.hora,'HH24:MI:SS') ||'-'|| '(sobrecupo)' as hora,o.procede from agenda.sobreplanificar s inner join agenda.oferta o\n"
                + "on s.id_planificar = o.id_plani_sobre\n"
                + "where s.rut_doctor = '" + doctor + "' and\n"
                + "s.fecha ='" + fecha + "' and s.id_atencion = " + atencion + " and s.id_especialidad = " + especialidad + " \n"
                + "and s.estatus = 1 and s.cupos_disponibles > 0 and o.estatus = 1 and o.procede = 2  order by procede,  hora");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                planificar p = new planificar();
                p.setId_planificacion(this.cnn.getRst().getInt("id_oferta"));
                p.setRut_doctor(this.cnn.getRst().getString("hora"));

                oferta.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return oferta;

    }

    /*registro las ofertas por doctor , tiene un cupo que se divide en varias ofertas dependiendo de la hora*/
    public void registraroperaciones(int planificacion, String hora, int tipo, int estatus, String horapabellon) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL(" INSERT INTO  agenda.oferta\n"
                + "( id_plani_sobre, hora, estatus, procede, horapabellon) \n"
                + "VALUES ( " + planificacion + ",'" + hora + "', " + estatus + "," + tipo + ",'" + horapabellon + "');");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    /**/
    public boolean buscarsitieneOfertasBloqueadasparaquepermitaPlanificar(String rut, Date fecha, String horaI, String horaF) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        boolean tienebloqueado = false;
        this.cnn.setSentenciaSQL(" select * from agenda.oferta o inner join \n"
                + "agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "where p.fecha = '" + fecha + "' and p.rut_doctor ='" + rut + "' \n"
                + "and o.hora BETWEEN '" + horaI + "' and '" + horaF + "'  and o.estatus = 2");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                tienebloqueado = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return tienebloqueado;

    }

    // buscar si el doctor ya tiene cupos a esa hora y ese dia
    public boolean buscarsielprofecionaltienehoraasignada(String rut, Date fecha, String horaI, String horaF) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        boolean tienecupoaesahora = false;
        this.cnn.setSentenciaSQL(" select * from agenda.planificar where rut_doctor='" + rut + "' and "
                + " fecha = '" + fecha + "'  and "
                + " ((hora_inicio < '" + horaF + "' and hora_fin > '" + horaF + "' ) or "
                + " (hora_inicio = '" + horaI + "' and hora_fin = '" + horaF + "') or"
                + " (hora_inicio < '" + horaI + "' and hora_fin > '" + horaF + "') or"
                + " (hora_inicio < '" + horaI + "' and hora_fin > '" + horaI + "') or"
                + " (hora_inicio < '" + horaI + "' and hora_fin = '" + horaF + "') or"
                + " (hora_inicio = '" + horaI + "' and hora_fin > '" + horaF + "')) and estatus = 1 ; ");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                tienecupoaesahora = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return tienecupoaesahora;
    }

    // traigo todo los cupos 
    public ArrayList buscarPlanificacionPorFechayDoctor(Date fecha, String doctor) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        ArrayList lista = new ArrayList();
        this.cnn.setSentenciaSQL("select id_planificacion "
                + " from agenda.planificar  "
                + "  where rut_doctor ='" + doctor + "' and fecha='" + fecha + "' and estatus = 1 ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lista.add(this.cnn.getRst().getInt("id_planificacion"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;
    }

    /*busco las citas de las ofertas de un cupo que sera bloqueado*/
    public void buscarcitaycancelarporbloqueo(int idplanificacion) {
        ArrayList lsita = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        ArrayList lista = new ArrayList();
        controlador_cita cc = new controlador_cita();
        this.cnn.setSentenciaSQL("SELECT   c.id_cita FROM agenda.cita c inner join agenda.oferta o\n"
                + "   on c.id_oferta = o.id_oferta\n"
                + "   where  o.id_plani_sobre = " + idplanificacion + "; ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            System.out.println(ex);

        }
        this.cnn.cerrarConexion();
        Iterator it2 = lista.iterator();
        while (it2.hasNext()) {
            cita c2 = (cita) it2.next();
            insertarentablaporcitascanceladasporbloqueos(c2.getId_cita(), idplanificacion);
            cancelar(c2.getId_cita());
        }

    }

    /*cancelar la cita del paciente*/
    public void cancelar(int idcita) {
        String motivo = "bloqueo de cupo";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 0 ,"
                + " motivo_cancela = '" + motivo + "',"
                + " usuario_cancela ='" + getUsuarioLoggin() + "'\n"
                + " \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    public void buscarCitayCancelarporBloqueodeOferta(int idoferta) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        int cita = 0;
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_cita\n"
                + "  FROM \n"
                + "  agenda.cita where id_oferta = " + idoferta + " and estatus  = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita = this.cnn.getRst().getInt("id_cita");
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();

        if (cita != 0) {
            cancelar(cita);
            insertarentablaporcitascanceladasporbloqueos(cita, idoferta);
            planificar p = buscarplanificacionytipo(idoferta);

            int tipo = p.getEstatus();
            if (tipo == 1) {
                AumentarCuposDoctor(p.getId_planificacion());

            } else {
                AumentarSobreCuposDoctor(p.getId_planificacion());

            }

        }

    }

    /*buscar cantidad de ofertas disponibles de una planificacion*/
    public int buscarCantidaddeOfertasActivas(int idplanificacion) {
        int cantidad = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select count(o.id_oferta)  from agenda.oferta o\n"
                + "inner join agenda.planificar p on o.id_plani_sobre= p.id_planificacion\n"
                + "where p.id_planificacion= " + idplanificacion + " and o.estatus = 1 ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cantidad = this.cnn.getRst().getInt("count");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return cantidad;
    }

    //inserto en tabladecitascanceladasporbloqueos
    public void insertarentablaporcitascanceladasporbloqueos(int cita, int planificacion) {

        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO agenda.citascanceladasporbloqueo\n"
                + "( idcita, idplanificacion ) \n"
                + "VALUES (\n"
                + "  " + cita + ",\n"
                + "  " + planificacion + " ); ");
        this.cnn.conectar();
        cancelar(cita);
        this.cnn.cerrarConexion();

    }

    // bloqueo el cupo
    public String bloquearcupo(int idcupo, String motivo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO "
                + "  agenda.bloqueo "
                + "VALUES ("
                + "  (select max(id_bloqueo) from agenda.bloqueo)+1, "
                + "  " + idcupo + ","
                + "  CURRENT_DATE,"
                + "  '" + motivo + "',"
                + "  1 , '" + getUsuarioLoggin() + "' );");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cupo Bloqueado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        cambairestatuscupo(idcupo);
        cambairestatussobrecupo(idcupo);
        bloquearoferta(idcupo);
        buscarcitaycancelarporbloqueo(idcupo);

        return mensaje;

    }
    //colocar estatus del cupo en 0 porque esta bloqueado

    public void cambairestatuscupo(int idcupo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.planificar SET  estatus = 0 , usuariobloqueaoelimina = '" + getUsuarioLoggin() + "' WHERE  id_planificacion = " + idcupo + " ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    // si bloqueo cupo bloqueo su sobrecupo
    public void cambairestatussobrecupo(int idcupo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE  agenda.sobreplanificar  SET estatus = 0 WHERE  id_planificar =  " + idcupo + " ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    //si bloqueo planificacion, bloqueo oferta*/
    public void bloquearoferta(int idcupo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.oferta  SET estatus = 0 WHERE id_plani_sobre = " + idcupo + ";");
        this.cnn.conectar();
        this.cnn.cerrarConexion();

    }

    // buscar cupos por id
    public planificar buscarplanificacionyarmar(int id_planificacion, Date fecha) {
        int id = buscarultimaplanificacion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from agenda.planificar where id_planificacion = " + id_planificacion + " and estatus = 1 ");
        this.cnn.conectar();
        planificar p = new planificar();
        try {
            int i = 1;

            while (this.cnn.getRst().next()) {
                p.setId_planificacion(id + i);
                p.setRut_doctor(this.cnn.getRst().getString("rut_doctor"));
                p.setHora_inicio(this.cnn.getRst().getString("hora_inicio"));
                p.setHora_fin(this.cnn.getRst().getString("hora_fin"));
                p.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                p.setId_programa(this.cnn.getRst().getInt("id_programa"));
                p.setDuracion(this.cnn.getRst().getInt("duracion"));
                p.setNumero_cupo(this.cnn.getRst().getInt("cupos"));
                p.setEstatus(1);
                p.setFecha(fecha);
                p.setId_especialidad(this.cnn.getRst().getInt("id_especialidad"));
                p.setCupos_disponibles(this.cnn.getRst().getInt("cupos"));
                p.setId_prestacion(this.cnn.getRst().getInt("id_prestacion"));
                p.setHorainiciopabellon(this.cnn.getRst().getString("hora_iniciopabellon"));
                p.setHorafinpabellon(this.cnn.getRst().getString("hora_finpabellon"));
                p.setPabellon(this.cnn.getRst().getInt("idpabellon"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*busco las planificaciones o cupos de un doctor */
    public String buscarRutdeDoctordePlanificacion(int idplanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT rut_doctor FROM  agenda.planificar where id_planificacion = " + idplanificacion + " and estatus = 1 ;  ");
        this.cnn.conectar();
        String rut = "";
        try {
            if (this.cnn.getRst().next()) {
                rut = this.cnn.getRst().getString("rut_doctor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return rut;
    }

    /*aumento y disminuyo los cupos dependiendo de la si se cancela o no la cita*/
    public void disminuirCuposDoctor(int idPlanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.planificar  SET  cupos_disponibles = (select cupos_disponibles from agenda.planificar where id_planificacion = " + idPlanificacion + ") - 1 WHERE id_planificacion = " + idPlanificacion + " ; ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void AumentarCuposDoctor(int idPlanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.planificar  SET  cupos_disponibles = (select cupos_disponibles from agenda.planificar where id_planificacion = " + idPlanificacion + ") + 1 WHERE id_planificacion = " + idPlanificacion + "; ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    /*disminuir y aumentar sobrecupo
     */
    public void disminuirSobreCuposDoctor(int idPlanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.sobreplanificar  SET  cupos_disponibles = (select cupos_disponibles from agenda.sobreplanificar where id_sobrecupo = " + idPlanificacion + ") - 1 WHERE id_sobrecupo = " + idPlanificacion + "; ");

        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void AumentarSobreCuposDoctor(int idPlanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.sobreplanificar  SET  cupos_disponibles = (select cupos_disponibles from agenda.sobreplanificar where id_sobrecupo = " + idPlanificacion + ") + 1 WHERE id_sobrecupo = " + idPlanificacion + "; ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    /* total en la semana*/
    public planificar totalporsemana(Date inicio, Date fin, int es, String doctor, int aten) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        String where = "";
        if ((aten != -1) && (!doctor.equals("-1")) && (es != -1)) {
            where = " where ( p.fecha between  '" + inicio + "' and  '" + fin + "')  and p.estatus = 1  and p.id_especialidad = " + es + "  and p.rut_doctor = '" + doctor + "'  \n"
                    + "  and p.id_atencion = " + aten + " ";

        } else if (!doctor.equals("-1") && (es != -1)) {
            where = " where ( p.fecha between  '" + inicio + "' and  '" + fin + "')  and p.estatus = 1  and p.id_especialidad = " + es + "  and p.rut_doctor = '" + doctor + "'  \n"
                    + "    ";
        } else if (es != -1) {
            where = " where ( p.fecha between  '" + inicio + "' and  '" + fin + "')  and p.estatus = 1  and p.id_especialidad = " + es + "  ";
        } else {
            where = " where ( p.fecha between  '" + inicio + "' and  '" + fin + "')  and p.estatus = 1  ";
        }
        this.cnn.setSentenciaSQL("SELECT coalesce((sum(p.cupos)+sum(s.cupos))- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados)),0) as cupo, coalesce((sum(p.cupos_disponibles)+sum(s.cupos_disponibles)),0) as disponible , \n"
                + "                coalesce (((sum(p.cupos)+sum(s.cupos)- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados))) - (sum(p.cupos_disponibles)+sum(s.cupos_disponibles))),0) as ocupados \n"
                + "                FROM  agenda.planificar p inner join agenda.sobreplanificar s \n"
                + "               on p.id_planificacion = s.id_planificar\n"
                + " " + where + " ;");
        this.cnn.conectar();
        planificar p = new planificar();
        try {

            while (this.cnn.getRst().next()) {

                p.setId_programa(this.cnn.getRst().getInt("ocupados"));
                p.setDuracion(this.cnn.getRst().getInt("disponible"));
                p.setNumero_cupo(this.cnn.getRst().getInt("cupo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*bucar por parte con sobreplanificacion */
    public Vector<planificar> traerdato(Date fecha, int aten, int es, String doctor) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        Vector<planificar> vp = new Vector<planificar>();
        String where = "";
        if ((aten != -1) && (!doctor.equals("-1")) && (es != -1)) {
            where = " where  p.fecha = '" + fecha + "'  and p.estatus = 1  and p.id_especialidad = " + es + "  and p.rut_doctor = '" + doctor + "'  \n"
                    + "  and p.id_atencion = " + aten + " ";

        } else if (!doctor.equals("-1") && (es != -1)) {
            where = " where p.fecha = '" + fecha + "'  and p.estatus = 1  and p.id_especialidad = " + es + "  and p.rut_doctor = '" + doctor + "'  \n"
                    + "    ";
        } else if (es != -1) {
            where = " where  p.fecha = '" + fecha + "'  and p.estatus = 1  and p.id_especialidad = " + es + "  ";
        } else {
            where = " where  p.fecha = '" + fecha + "'  and p.estatus = 1  ";
        }
        this.cnn.setSentenciaSQL("SELECT  a.abreviatura,(sum(p.cupos)+sum(s.cupos))- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados))as cupo, \n"
                + "       (sum(p.cupos_disponibles)+sum(s.cupos_disponibles))  as disponible , \n"
                + "        (((sum(p.cupos)+sum(s.cupos))- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados))) - (sum(p.cupos_disponibles)+sum(s.cupos_disponibles))) as ocupados , p.id_atencion\n"
                + "         FROM  agenda.planificar p inner join agenda.sobreplanificar s \n"
                + "         on p.id_planificacion = s.id_planificar"
                + "          join agenda.atencion a on a.id_atencion = p.id_atencion\n"
                + " " + where + " \n"
                + " GROUP by p.id_atencion,s.id_atencion, abreviatura ");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                planificar p = new planificar();
                p.setPrograma(this.cnn.getRst().getString("abreviatura"));
                p.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                p.setId_programa(this.cnn.getRst().getInt("ocupados"));
                p.setDuracion(this.cnn.getRst().getInt("disponible"));
                p.setNumero_cupo(this.cnn.getRst().getInt("cupo"));

                vp.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    //buscar el sobrecupodeunaoferta
    public int SobrecupodeunaOferta(int idplanificacion, int idoferta) {
        int sobrecupo = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select o.id_oferta  from agenda.oferta o\n"
                + "where o.estatus= 1 and o.procede = 2 and o.id_plani_sobre = " + idplanificacion + " \n"
                + "and  o.hora = (select o2.hora from agenda.oferta o2 where o2.id_oferta = " + idoferta + ") ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                sobrecupo = this.cnn.getRst().getInt("id_oferta");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return sobrecupo;
    }

//para la agenda semanal
    public Vector<planificar> traerdatosporsemana(Date fecha, int es, String doctor, int aten) {

        Vector<planificar> pla = new Vector<planificar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        String where = "";
        if ((aten != -1) && (!doctor.equals("-1")) && (es != -1)) {
            where = " where  p.fecha = '" + fecha + "'  and p.estatus = 1  and p.id_especialidad = " + es + "  and p.rut_doctor = '" + doctor + "'  \n"
                    + "  and p.id_atencion = " + aten + " ";

        } else if (!doctor.equals("-1") && (es != -1)) {
            where = " where p.fecha = '" + fecha + "'  and p.estatus = 1  and p.id_especialidad = " + es + "  and p.rut_doctor = '" + doctor + "'  \n"
                    + "    ";
        } else if (es != -1) {
            where = " where  p.fecha = '" + fecha + "'  and p.estatus = 1  and p.id_especialidad = " + es + "  ";
        } else {
            where = " where  p.fecha = '" + fecha + "'  and p.estatus = 1  ";
        }
        this.cnn.setSentenciaSQL("SELECT (sum(p.cupos)+sum(s.cupos))- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados))as cupo, \n"
                + "       (sum(p.cupos_disponibles)+sum(s.cupos_disponibles))  as disponible , \n"
                + "        (((sum(p.cupos)+sum(s.cupos))- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados))) - (sum(p.cupos_disponibles)+sum(s.cupos_disponibles))) as ocupados , p.id_atencion\n"
                + "         FROM  agenda.planificar p inner join agenda.sobreplanificar s \n"
                + "         on p.id_planificacion = s.id_planificar\n"
                + "  " + where + " \n"
                + " GROUP by p.id_atencion,s.id_atencion ");
        this.cnn.conectar();
        planificar p = new planificar();
        try {

            while (this.cnn.getRst().next()) {

                p.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                p.setId_programa(this.cnn.getRst().getInt("ocupados"));
                p.setDuracion(this.cnn.getRst().getInt("disponible"));
                p.setNumero_cupo(this.cnn.getRst().getInt("cupo"));
                pla.add(p);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return pla;

    }

    /*para hacer bloqueo de ofertas*/
 /*busco las ofertas*/
    public Vector<oferta> buscarlasOfertasdeUnDoctor(String rutdoctor, Date Fecha) {

        Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_oferta,\n"
                + "  id_plani_sobre,\n"
                + "  hora,\n"
                + "  o.estatus,\n"
                + "  procede\n"
                + "FROM \n"
                + "  agenda.oferta o inner join agenda.planificar p\n"
                + "  on o.id_plani_sobre = p.id_planificacion \n"
                + "   join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "    where  o.estatus = 1 and p.fecha = '" + Fecha + "' and upper(rut_doctor) = upper('" + rutdoctor + "') and procede = 1 order by o.id_oferta ;");
        this.cnn.conectar();
        try {

            while (this.cnn.getRst().next()) {
                oferta o = new oferta();
                o.setId_oferta(this.cnn.getRst().getInt("id_oferta"));
                o.setId_plani_sobre(this.cnn.getRst().getInt("id_plani_sobre"));
                o.setHora(this.cnn.getRst().getString("hora"));
                o.setProcede(this.cnn.getRst().getInt("procede"));
                vo.add(o);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;

    }

    /*buscar ofertas para registrar ofertas de tabla de pabellon*/
    public Vector<oferta> buscarlasOfertasparatabla(int idplanificacion, int procede) {

        Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_oferta,\n"
                + "  id_plani_sobre,\n"
                + "  hora,\n"
                + "  o.estatus,\n"
                + "  procede\n"
                + "FROM \n"
                + "  agenda.oferta o inner join agenda.planificar p\n"
                + "  on o.id_plani_sobre = p.id_planificacion \n"
                + "   \n"
                + "    where id_plani_sobre=" + idplanificacion + "  and procede = " + procede + " order by o.id_oferta ;");
        this.cnn.conectar();
        try {

            while (this.cnn.getRst().next()) {
                oferta o = new oferta();
                o.setId_oferta(this.cnn.getRst().getInt("id_oferta"));
                o.setId_plani_sobre(this.cnn.getRst().getInt("id_plani_sobre"));
                o.setHora(this.cnn.getRst().getString("hora"));
                o.setProcede(this.cnn.getRst().getInt("procede"));
                vo.add(o);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;

    }

    /*registrar oferta*/
 /*registrar ofertasdetablaquirurgica*/
 /*buscar los sobrecupos*/
    public Vector<oferta> buscarlasSobrecuposparaactivardeUnDoctor(String rutdoctor, Date Fecha) {

        Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                  id_oferta,\n"
                + "                  id_plani_sobre,\n"
                + "                 hora,\n"
                + "                 o.estatus,\n"
                + "                 procede\n"
                + "                FROM \n"
                + "                agenda.oferta o inner join agenda.planificar p\n"
                + "                  on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                   where   p.fecha = '" + Fecha + "' and upper(rut_doctor) = upper('" + rutdoctor + "') and procede = 2\n"
                + "                    and o.estatus = 2 order by o.id_oferta");
        this.cnn.conectar();
        try {

            while (this.cnn.getRst().next()) {
                oferta o = new oferta();
                o.setId_oferta(this.cnn.getRst().getInt("id_oferta"));
                o.setId_plani_sobre(this.cnn.getRst().getInt("id_plani_sobre"));
                o.setHora(this.cnn.getRst().getString("hora"));
                o.setProcede(this.cnn.getRst().getInt("procede"));
                vo.add(o);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;

    }

    /*modificar la cantidad de cupos bloqueados para la diferencia  */
    public void AumentarlosCuposBloqueados(int idPlanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.planificar  \n"
                + " SET \n"
                + "  cupos_bloqueados = (select cupos_bloqueados from agenda.planificar where id_planificacion = " + idPlanificacion + ")+1\n"
                + " \n"
                + " WHERE \n"
                + "  id_planificacion = " + idPlanificacion + "");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void AumentarlosSobreCuposBloqueados(int idPlanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE  agenda.sobreplanificar\n"
                + " SET   cupos_bloqueados = (select cupos_bloqueados from agenda.sobreplanificar where id_sobrecupo = " + idPlanificacion + ")+1\n"
                + " \n"
                + "    WHERE \n"
                + "   id_sobrecupo = " + idPlanificacion + "");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void DisminuirSobreCuposBloqueados(int idPlanificacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE  agenda.sobreplanificar\n"
                + " SET   cupos_bloqueados = (select cupos_bloqueados from agenda.sobreplanificar where id_sobrecupo = " + idPlanificacion + ")-1\n"
                + " \n"
                + "    WHERE \n"
                + "   id_sobrecupo = " + idPlanificacion + "");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public oferta buscarOfertaporIdparaconsulta(int idoferta) {
        oferta o = new oferta();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " o.id_oferta,\n"
                + " o.id_plani_sobre,\n"
                + " o.hora,\n"
                + " o.estatus,\n"
                + "p.id_atencion,\n"
                + "procede\n"
                + " FROM \n"
                + "agenda.planificar p inner join agenda.oferta o \n"
                + "on o.id_plani_sobre = p.id_planificacion\n"
                + "where o.id_oferta = " + idoferta + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                o.setHora(this.cnn.getRst().getString("hora"));
                o.setId_plani_sobre(this.cnn.getRst().getInt("id_plani_sobre"));
                o.setProcede(this.cnn.getRst().getInt("id_atencion"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return o;
    }

    /*cambiar*/
    public Vector<planificar> buscarPlanificacionesparaReporte(Date fecha1, Date fecha2, String doctor, int especialidad) {
        String resto = "p.fecha  BETWEEN  '" + fecha1 + "' AND '" + fecha2 + "'";
        if (especialidad != -1) {
            resto = resto + " and e.id_especialidad =  " + especialidad + "";
        }

        if (!doctor.equals("-1")) {
            resto = resto + "and doc.rut='" + doctor + "'";
        }

        Vector<planificar> planificaciones = new Vector<planificar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_planificacion,\n"
                + "  e.nombreespecialidad,\n"
                + "  doc.nombre ||' '|| doc.apellido_paterno ||' '||doc.apellido_materno as doctor,\n"
                + "  to_char(fecha,'dd-mm-yyyy') as fecha,\n"
                + "  hora_inicio,\n"
                + "  hora_fin,\n"
                + "  a.nombre as atencion,\n"
                + "  pro.nombre as programa,\n"
                + "  pre.codigo||'-'|| pre.descripcion as prestaciones,\n"
                + "  duracion,\n"
                + "  cupos,\n"
                + "  cupos_disponibles,\n"
                + "  cupos_bloqueados\n"
                + "  \n"
                + "FROM \n"
                + "  agenda.planificar p inner join caja.prestacion pre\n"
                + "  on p.id_prestacion = pre.oid\n"
                + "  inner join agenda.doctor doc on doc.rut = p.rut_doctor\n"
                + "  join agenda.programa pro on pro.id_programa = p.id_programa\n"
                + "  join agenda.atencion a on a.id_atencion = p.id_atencion\n"
                + "  join agenda.especialidades e on e.id_especialidad = p.id_especialidad\n"
                + "   where p.estatus = 1 and " + resto + "\n"
                + "   order by p.fecha, e.nombreespecialidad;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                planificar p = new planificar();
                p.setEspecialidad(this.cnn.getRst().getString("nombreespecialidad"));
                p.setRut_doctor(this.cnn.getRst().getString("doctor"));
                p.setFechaconformato(this.cnn.getRst().getString("fecha"));
                p.setHora_inicio(this.cnn.getRst().getString("hora_inicio"));
                p.setHora_fin(this.cnn.getRst().getString("hora_fin"));
                p.setAtencion(this.cnn.getRst().getString("atencion"));
                p.setPrograma(this.cnn.getRst().getString("programa"));
                p.setPrestacion(this.cnn.getRst().getString("prestaciones"));
                p.setDuracion(this.cnn.getRst().getInt("duracion"));
                p.setNumero_cupo(this.cnn.getRst().getInt("cupos"));
                p.setCupos_disponibles(this.cnn.getRst().getInt("cupos_disponibles"));
                p.setEstatus(this.cnn.getRst().getInt("cupos_bloqueados"));
                planificaciones.add(p);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return planificaciones;
    }

    /*buscar informacion para ver si debo o no sumar bloqueados en sobrecupos */
    public planificar buscarSobreCuposporIdparaconsulta(int idoferta) {
        planificar p = new planificar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " o.id_plani_sobre,\n"
                + "                 o.hora,\n"
                + "                 o.estatus,\n"
                + "                 p.cupos_bloqueados, p.cupos, p.cupos_disponibles\n"
                + "                 FROM \n"
                + "                agenda.sobreplanificar p inner join agenda.oferta o \n"
                + "                on o.id_plani_sobre = p.id_sobrecupo where o.id_oferta = " + idoferta + "");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {

                p.setId_planificacion(this.cnn.getRst().getInt("id_plani_sobre"));
                p.setNumero_cupo(this.cnn.getRst().getInt("cupos"));
                p.setCupos_bloqueados(this.cnn.getRst().getInt("cupos_bloqueados"));
                p.setCupos_disponibles(this.cnn.getRst().getInt("cupos_disponibles"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*buscar para tabla de primero las planificaciones*/
    public Vector<planificar> BuscarplanificacionesparaTablaQuirurgica(Date fechainicio, Date fechafin) {
        Vector<planificar> vp = new Vector<planificar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select  pla.id_planificacion , sp.nombresalapabellon, doc.apellido_paterno||' '|| doc.nombre as doctor, pla.duracion,  to_char( pla.fecha,'DD/MM/YYYY') as fecha from agenda.planificar pla inner join\n"
                + "agenda.doctor doc on pla.rut_doctor = doc.rut\n"
                + "join pabellon.\"salaPabellon\" sp on pla.idpabellon = sp.idsalapabellon\n"
                + "where pla.id_atencion = 3 and fecha between  '" + fechainicio + "' and '" + fechafin + "' order by fecha ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                planificar p = new planificar();
                p.setId_planificacion(this.cnn.getRst().getInt("id_planificacion"));
                p.setPrograma(this.cnn.getRst().getString("nombresalapabellon"));
                p.setRut_doctor(this.cnn.getRst().getString("doctor"));
                p.setDuracion(this.cnn.getRst().getInt("duracion"));
                p.setFechaconformato(this.cnn.getRst().getString("fecha"));
                vp.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    public Vector<oferta> buscarPlanificacionycitasparaReportedeTablaQuirurgica(Date fechainicio, Date fechafin) {
        Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT \n"
                + "                p.id_planificacion , sp.nombresalapabellon,   sp.idsalapabellon,"
                + "            doc.apellido_paterno||' '|| doc.nombre as doctor,\n"
                + "                doc.rut,\n"
                + "                  p.duracion,  to_char( p.fecha,'DD/MM/YYYY') as fecha,\n"
                + "                o.horapabellon, o.hora ,  case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                inner join   agenda.diagnostico_especialidad des\n"
                + "                on di.iddiagnostico = des.iddiagnostico\n"
                + "                 where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + "                FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo ,\n"
                + "                 pac.rut , pac.nombre || ' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombrepaciente,\n"
                + "              pre.codigo  || ' '|| pre.descripcion as prestacion\n"
                + "               FROM \n"
                + "                agenda.cita c\n"
                + "                  inner join agenda.oferta o\n"
                + "                  on c.id_oferta = o.id_oferta\n"
                + "                  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "                  join pabellon.\"salaPabellon\" sp on p.idpabellon = sp.idsalapabellon\n"
                + "                 join agenda.doctor doc on p.rut_doctor = doc.rut\n"
                + "                join agenda.paciente pac on pac.rut = c.rut_paciente\n"
                + "                  join agenda.cita_prestacion cp on cp.id_cita = c.id_cita\n"
                + "                \n"
                + "                 join caja.prestacion pre on pre.oid = cp.id_prestacion where cp.tipo = 1 \n"
                + "                   and p.id_atencion = 3 and p.fecha between  '"+fechainicio+"' and '"+fechafin+"' and\n"
                + "                    c.estatus <> 0 and p.estatus not in(0,4)\n"
                + "                   order by doc.rut, o.hora asc;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                oferta o = new oferta();
                o.setId_plani_sobre(this.cnn.getRst().getInt("id_planificacion"));
                o.setTemporal4(this.cnn.getRst().getString("nombresalapabellon"));
                o.setTemporal5(this.cnn.getRst().getString("doctor"));
                o.setTemporal6(this.cnn.getRst().getString("fecha"));
                o.setTemporal7(this.cnn.getRst().getString("nombremotivo"));
                o.setHorapabellon(this.cnn.getRst().getString("horapabellon"));
                o.setHora(this.cnn.getRst().getString("hora"));
                o.setTemporal(this.cnn.getRst().getString("nombremotivo"));
                o.setTemporal3(this.cnn.getRst().getString("rut"));
                o.setTemporal1(this.cnn.getRst().getString("nombrepaciente"));
                o.setTemporal2(this.cnn.getRst().getString("prestacion"));
                o.setSala(this.cnn.getRst().getInt("idsalapabellon"));
                o.setDuracion(this.cnn.getRst().getInt("duracion"));
                vo.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;
    }

    /*buscar las citas */
    public Vector<oferta> buscarcitasparaReportedeTablaQuirurgica(int idplanificacion) {
        Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "o.horapabellon, o.hora ,  case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , pac.rut , pac.nombre || ' '|| pac.apellido_paterno ||' '|| pac.apellido_moderno as nombrepaciente,\n"
                + "pre.codigo \n"
                + "FROM \n"
                + "  agenda.cita c\n"
                + "  inner join agenda.oferta o\n"
                + "  on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "  join agenda.paciente pac on pac.rut = c.rut_paciente\n"
                + "  join agenda.cita_prestacion cp on cp.id_cita = c.id_cita\n"
                + "  \n"
                + "  join caja.prestacion pre on pre.oid = cp.id_prestacion where cp.tipo = 1 \n"
                + "   and p.id_planificacion = " + idplanificacion + " and c.estatus <> 0;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                oferta o = new oferta();
                o.setHorapabellon(this.cnn.getRst().getString("horapabellon"));
                o.setHora(this.cnn.getRst().getString("hora"));
                o.setTemporal(this.cnn.getRst().getString("nombremotivo"));
                o.setTemporal3(this.cnn.getRst().getString("rut"));
                o.setTemporal1(this.cnn.getRst().getString("nombrepaciente"));
                o.setTemporal2(this.cnn.getRst().getString("codigo"));
                vo.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;
    }

    public Vector<oferta> buscarcuposysobrecupos(Date fecha, int especialidad, String profesional) {
        Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        String sql = "select pn.id_planificacion, pn.rut_doctor,( d.nombre  ||' '|| d.apellido_paterno) as doctor,"
                + " a.nombre as atencion, p.nombre as programa "
                + " , pn.hora_inicio, pn.hora_fin, pn.duracion, pn.cupos, to_char(pn.fecha,'DD/MM/YYYY') as fecha , "
                + " sp.cupos as sobre, pn.cupos_bloqueados , pre.descripcion as prestacion"
                + " from agenda.planificar pn inner join agenda.especialidades e on"
                + "  e.id_especialidad = pn.id_especialidad "
                + "   join agenda.programa p on pn.id_programa = p.id_programa "
                + "   join agenda.doctor d on d.rut = pn.rut_doctor "
                + "  join agenda.atencion a on a.id_atencion = pn.id_atencion "
                + "   join agenda.sobreplanificar sp on pn.id_planificacion = sp.id_planificar "
                + " join caja.prestacion pre on pn.id_prestacion = pre.oid"
                + "   where pn.id_especialidad = " + especialidad + " and pn.fecha = '" + fecha + "' and d.rut = '" + profesional + "' and pn.estatus =1   ";

        this.cnn.setSentenciaSQL(sql);
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                oferta o = new oferta();
                o.setId_oferta(this.cnn.getRst().getInt("id_planificacion"));
                o.setTemporal(this.cnn.getRst().getString("doctor"));
                o.setTemporal1(this.cnn.getRst().getString("fecha"));
                o.setTemporal2(this.cnn.getRst().getString("hora_inicio"));
                o.setTemporal3(this.cnn.getRst().getString("hora_fin"));
                o.setHora(this.cnn.getRst().getString("atencion"));
                o.setHorapabellon(this.cnn.getRst().getString("programa"));
                o.setEstatus(this.cnn.getRst().getInt("duracion"));
                o.setProcede(this.cnn.getRst().getInt("cupos"));
                o.setCupos(this.cnn.getRst().getInt("cupos_bloqueados"));
                o.setId_plani_sobre(this.cnn.getRst().getInt("sobre"));
                o.setTemporal4(this.cnn.getRst().getString("prestacion"));

                vo.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;
    }

    public Vector<oferta> buscarcupos(Date fecha, int especialidad, String profesional) {
        Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        String planificacion = " select pn.id_planificacion, pn.rut_doctor,( d.nombre  ||'  '|| d.apellido_paterno) as doctor, "
                + " e.nombreespecialidad as especialidad, a.nombre as atencion, p.nombre as programa "
                + " , pn.hora_inicio, pn.hora_fin, pn.duracion, pn.cupos, to_char( pn.fecha, 'DD/MM/YYYY') as fecha "
                + ",(((sum(pn.cupos)+sum(s.cupos))- (sum(pn.cupos_bloqueados)+sum(s.cupos_bloqueados))) - (sum(pn.cupos_disponibles)+sum(s.cupos_disponibles))) as ocupados"
                + " from agenda.planificar pn inner join agenda.especialidades e on e.id_especialidad = pn.id_especialidad "
                + " join agenda.sobreplanificar s on pn.id_planificacion = s.id_planificar" 
                + " join agenda.programa p on pn.id_programa = p.id_programa "
                + " join agenda.doctor d on d.rut = pn.rut_doctor "
                + " join agenda.atencion a on a.id_atencion = pn.id_atencion "
                + "  where pn.rut_doctor ='" + profesional + "' and pn.fecha='" + fecha + "' and pn.id_especialidad =" + especialidad + " "
                + "   and pn.estatus = 1"
                + "group by pn.id_planificacion,pn.rut_doctor, doctor, e.nombreespecialidad, a.nombre, p.nombre,pn.hora_inicio, pn.hora_fin, pn.duracion, pn.cupos,pn.fecha " 
                + "order by pn.hora_inicio";

        this.cnn.setSentenciaSQL(planificacion);
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                oferta o = new oferta();
                
                o.setTemporal9(this.cnn.getRst().getInt("ocupados"));
                o.setId_oferta(this.cnn.getRst().getInt("id_planificacion"));
                o.setTemporal(this.cnn.getRst().getString("doctor"));
                o.setTemporal1(this.cnn.getRst().getString("fecha"));
                o.setTemporal2(this.cnn.getRst().getString("hora_inicio"));
                o.setTemporal3(this.cnn.getRst().getString("hora_fin"));
                o.setHora(this.cnn.getRst().getString("atencion"));
                o.setHorapabellon(this.cnn.getRst().getString("programa"));
                o.setEstatus(this.cnn.getRst().getInt("duracion"));
                o.setProcede(this.cnn.getRst().getInt("cupos"));

                vo.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;
    }
    /*
     * @author Ricardo Ramirez
     * @since 30-07-2019
     * @version 1.0.0
     * 
     * Función que obtine las citas de los cupos
     */
     public Vector<oferta> listarcupos(Date fecha, int especialidad, String profesional) {
     Vector<oferta> vo = new Vector<oferta>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        String listacupos = "SELECT p.id_especialidad, pac.rut, (pac.nombre  ||'  '|| pac.apellido_paterno) as paciente," +
            "d.rut,( d.nombre  ||'  '|| d.apellido_paterno) as doctor, " +
            " e.nombreespecialidad as especialidad," +
            " p.hora_inicio, p.hora_fin, to_char( p.fecha, 'DD/MM/YYYY') as fecha " +
            "FROM agenda.cita c " +
            "inner join agenda.oferta o on c.id_oferta = o.id_oferta " +
            "join agenda.planificar p on p.id_planificacion = o.id_plani_sobre " +
            "join agenda.especialidades e on p.id_especialidad = e.id_especialidad " +
            "join agenda.doctor d on p.rut_doctor = d.rut " +
            "join agenda.paciente pac on pac.rut = c.rut_paciente " +
            "WHERE p.rut_doctor = '" + profesional + "' " +
            "and p.fecha= '" + fecha + "' " +
            "and p.id_especialidad =" + especialidad + " "+
            "and p.estatus = 1 ORDER BY paciente ASC";
        
        this.cnn.setSentenciaSQL(listacupos);
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                       oferta o = new oferta();
                o.setTemporal(this.cnn.getRst().getString("doctor"));
                o.setTemporal1(this.cnn.getRst().getString("especialidad"));
                o.setTemporal2(this.cnn.getRst().getString("paciente"));
                o.setTemporal3(this.cnn.getRst().getString("fecha"));
                o.setTemporal4(this.cnn.getRst().getString("hora_inicio"));
                o.setTemporal5(this.cnn.getRst().getString("hora_fin"));
                vo.add(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cupos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vo;
     }

}
