/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.opciones;
import Modelos.rol;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Informatica
 */
public class controlador_rol extends General {

    public String actualizarrol(int rol, String nombre) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE  seguridad.rol  SET nombrerol = '" + nombre + "' WHERE idrol=" + rol + " ; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Perfil Modificado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    //buscar el rol para que no se repita
    public boolean buscarpornombre(String nombrerol) {
        boolean existe = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from seguridad.rol where nombrerol='" + nombrerol + "'");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                existe = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return existe;
    }

    //para registrar el rol
    public String ingresar_rol(rol r) {

        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO  seguridad.rol "
                + "   VALUES ( "
                + "  " + r.getIdRol() + ",'" + r.getNombreRol() + "', " + 1 + " , " + 3 + " ); ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Perfil Insertado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

    //ingreso el rol
    public void Ingresar_rolOpciones(String[] opciones, int idrol) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        int opcion = 0;
        for (int i = 0; i < opciones.length; ++i) {
            opcion = Integer.parseInt(opciones[i]);
            this.cnn.setSentenciaSQL("INSERT INTO  seguridad.rol_opciones "
                    + "   VALUES ( "
                    + "  " + idrol + "," + opcion + "  ); ");
            this.cnn.conectar();
        }
        this.cnn.cerrarConexion();
    }

    //busco el ultimo rol
    public int ultimorol() {
        int rol = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  max(idrol) as rol FROM seguridad.rol;  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                rol = this.cnn.getRst().getInt("rol");
            } else {
                rol = 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return rol;
    }

    public void eliminarrol_opciones(int r) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("delete from seguridad.rol_opciones where idrol= " + r + " ");
        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*bucar las diagnosticos especialidades de diagnostico*/
    public ArrayList buscarListaEspecialidadesconDiagnostico(int iddia) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                             d.nombreespecialidad,\n"
                + "                             d.id_especialidad\n"
                + "                               \n"
                + "                                FROM \n"
                + "                               agenda.especialidades d inner join agenda.diagnostico_especialidad de\n"
                + "                             on de.idespecialidad = d.id_especialidad where de.iddiagnostico=" + iddia + "  order by nombreespecialidad;  ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                opciones op = new opciones();
                op.setIdopcion(this.cnn.getRst().getInt("id_especialidad"));
                op.setNombre(this.cnn.getRst().getString("nombreespecialidad"));

                lista.add(op);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;

    }

    public ArrayList buscarListaopcionesconRol(int rol) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT  * FROM  "
                + "  seguridad.opciones o  inner join seguridad.rol_opciones ro on "
                + "  o.idopciones = ro.idopciones where ro.idrol= " + rol + " ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                opciones op = new opciones();
                op.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                op.setNombre(this.cnn.getRst().getString("nombreopciones"));
                op.setUrl(this.cnn.getRst().getString("url"));
                op.setStatus(1);
                lista.add(op);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;

    }

    //para modificar
    public void Ingresar_UnrolOpciones(int opcion, int idrol) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO  seguridad.rol_opciones "
                + "   VALUES ( "
                + "  " + idrol + "," + opcion + "  ); ");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*insertar un diagnostico*/
    public void Ingresar_diagnostico(opciones o) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.diagnosticos\n"
                + "(  nombre_diagnostico,\n"
                + "  estatus,\n"
                + "  caso\n"
                + ") \n"
                + "VALUES (\n"
                + " \n"
                + " '" + o.getNombre() + "',\n"
                + " 1,\n"
                + "  " + o.getStatus() + "\n"
                + ");");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*eliminar diagnosticos-especialidad*/
    public void EliminarDiagnosticoEspecialdiad(int diag) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("DELETE FROM agenda.diagnostico_especialidad\n"
                + " WHERE iddiagnostico=" + diag + ";");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*modificaar diagnostico*/

    public void ModificarDiagnostico(opciones o) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("UPDATE agenda.diagnosticos\n"
                + "   SET  nombre_diagnostico='"+o.getNombre()+"', caso="+o.getStatus()+"\n"
                + " WHERE iddiagnostico= "+o.getIdopcion()+";");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

        public void EliminarDiagnostico(int o) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("UPDATE agenda.diagnosticos\n"
                + "   SET  estatus=0 \n"
                + " WHERE iddiagnostico= "+o+";");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /**/
    public void ingresarDiagnosticoEspecialdiad(int opciones, int diag) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.diagnostico_especialidad\n"
                + "(\n"
                + "  iddiagnostico,\n"
                + "  idespecialidad\n"
                + " \n"
                + ") \n"
                + "VALUES (\n"
                + "  " + diag + ",\n"
                + "  " + opciones + "\n"
                + "  \n"
                + ");");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*buscar id diagnostico*/
    public int buscarIdDiagnosticoporNombreYcaso(String nombre, int caso) {
        int id = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  iddiagnostico\n"
                + "FROM \n"
                + "  agenda.diagnosticos  where nombre_diagnostico ='" + nombre + "'  and caso=" + caso + ";");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                id = this.cnn.getRst().getInt("iddiagnostico");

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return id;
    }

    /*public buscar especialidades que no esten con diagnstico - especialidades*/
    public ArrayList buscarListaespecialidadessinDiagnostico(int iddiagnostico) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select * from agenda.especialidades e where e.id_especialidad\n"
                + "not in ( select de.idespecialidad from agenda.diagnosticos es\n"
                + "inner join agenda.diagnostico_especialidad de on es.iddiagnostico = de.iddiagnostico\n"
                + "where es.iddiagnostico = " + iddiagnostico + ")");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                opciones op = new opciones();
                op.setIdopcion(this.cnn.getRst().getInt("id_especialidad"));
                op.setNombre(this.cnn.getRst().getString("nombreespecialidad"));

                lista.add(op);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;
    }

    public ArrayList buscarListaopcionessinRol(int rol) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT * FROM \n"
                + "                  seguridad.opciones  where tipo <> 5 and  idopciones not in \n"
                + "                  (SELECT  o.idopciones FROM  seguridad.opciones o  \n"
                + "                  inner join seguridad.rol_opciones ro on \n"
                + "                   o.idopciones = ro.idopciones join seguridad.rol r on\n"
                + "                   r.idrol = ro.idrol  where ro.idrol= " + rol + " and r.id_sistema = 3 ) ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                opciones op = new opciones();
                op.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                op.setNombre(this.cnn.getRst().getString("nombreopciones"));
                op.setUrl(this.cnn.getRst().getString("url"));
                op.setStatus(1);
                lista.add(op);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;

    }

    /*Diagnostico Especialidaades*/
    public Vector<opciones> buscarTodoslosDiagnosticos() {
        Vector<opciones> lista = new Vector<opciones>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT \n"
                + "  iddiagnostico,\n"
                + "  nombre_diagnostico,\n"
                + "  estatus,\n"
                + "  caso\n"
                + "FROM \n"
                + "  agenda.diagnosticos  where estatus = 1 order by nombre_diagnostico; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                opciones op = new opciones();
                op.setIdopcion(this.cnn.getRst().getInt("iddiagnostico"));
                op.setNombre(this.cnn.getRst().getString("nombre_diagnostico"));

                lista.add(op);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;

    }

    /*buscar todos los diagnosticos*/
    public ArrayList buscarEspecialidades() {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT \n"
                + "  id_especialidad,\n"
                + "  nombreespecialidad,\n"
                + "  estatus,\n"
                + "  nombreimagen,\n"
                + "  tamannoimagen,\n"
                + "  fotoespecialidad,\n"
                + "  equivalenteagendavieja,\n"
                + "  tipoparacirugia\n"
                + "FROM \n"
                + "  agenda.especialidades where estatus = 1 order by nombreespecialidad; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                opciones op = new opciones();
                op.setIdopcion(this.cnn.getRst().getInt("id_especialidad"));
                op.setNombre(this.cnn.getRst().getString("nombreespecialidad"));

                lista.add(op);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;

    }


    /**/
    public ArrayList buscaropciones() {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" Select * from seguridad.opciones where estatus = 1 and tipo <> 5 ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                opciones op = new opciones();
                op.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                op.setNombre(this.cnn.getRst().getString("nombreopciones"));
                op.setUrl(this.cnn.getRst().getString("url"));
                op.setStatus(1);
                lista.add(op);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;

    }

    public Vector<rol> buscarroles() {
        Vector<rol> lista = new Vector();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from seguridad.rol where estatus = 1 and id_sistema = 3");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                rol r = new rol();
                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                r.setNombreRol(this.cnn.getRst().getString("nombrerol"));
                r.setEstatus(1);
                lista.add(r);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*Buscar todo lo del diagnostico */
    public rol buscarDiagnosticoporid(int id) {
        rol r = new rol();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  iddiagnostico,\n"
                + "  nombre_diagnostico,\n"
                + "  estatus,\n"
                + "  caso\n"
                + "FROM \n"
                + "  agenda.diagnosticos d  where d.iddiagnostico= " + id + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {

                r.setIdRol(this.cnn.getRst().getInt("iddiagnostico"));
                r.setNombreRol(this.cnn.getRst().getString("nombre_diagnostico"));
                r.setEstatus(this.cnn.getRst().getInt("caso"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return r;
    }

    public rol buscarrolporid(int id) {
        rol r = new rol();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from seguridad.rol where idrol = " + id + " ");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {

                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                r.setNombreRol(this.cnn.getRst().getString("nombrerol"));
                r.setEstatus(1);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_rol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return r;
    }

    // eliminar rol
    public String eliminar(int r) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("update seguridad.rol set estatus = 0 where idrol= " + r + " ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Perfil Eliminado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

}
