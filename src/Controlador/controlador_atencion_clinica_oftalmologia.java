/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.atencion;
import Modelos.atencion_clinica_oftalmologia;

import Modelos.atencion_clinica_tecnologo;
import Modelos.cita;
import Modelos.excepcion_garantia;

import Modelos.lugar;
import Modelos.motivo_consulta;
import Modelos.receta_lente_opticos;
import Modelos.registrodepatologia;
import Modelos.rol;
import Modelos.solicitudExamenes_Procedimientos;
import Modelos.vicio_refraccion;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author a
 */
public class controlador_atencion_clinica_oftalmologia extends General {

    public void eliminarpatologia(int idregistropatologia) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.registropatalogia \n"
                + "WHERE id_registropatologia = " + idregistropatologia + "\n"
                + ";");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void eliminarexcepciondegarantia(int idexcepciongarantia) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.exception_garantia \n"
                + " WHERE idexceptiondegarantia = " + idexcepciongarantia + "\n"
                + ";");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public excepcion_garantia buscardocumentoexcepciondeGarantia(int idatencion, int tipo) {

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        String query = "";
        if (tipo == 1) {
            query = "SELECT 'EG-'|| to_char( act.fecha_registro, 'yyyymmdd')||'--OFT-'|| case when char_length(act.id_atencion_tecnologo)>4 then cast(act.id_atencion_tecnologo as text) else to_char(act.id_atencion_tecnologo, '0000') end  as folio, \n"
                    + "                                    to_char(act.fecha_registro,'dd-mm-yyyy HH24:MI') as fecha , \n"
                    + "                                    p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,  \n"
                    + "                                    p.rut, to_char( p.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento, to_char(age(p.fecha_nacimiento),'yy') as edad,  \n"
                    + "                                    case when exg.rechazo_prestador_designado = 1 then '1- Rechazo del prestador Designado ' else ' ' end ||\n"
                    + "                                    case when exg.rechazo_atencion_garantizado = 1 then '2- Rechazo de la atención o procedimiento garantizado ' else '' end ||\n"
                    + "                                    case when exg.prestacion_rechazada = 1 then ' 3- Prestación Rechazada ' else '' end ||\n"
                    + "                                    case when exg.otra_causa = 1 then ' 4- Otra Causas definida por el Paciente ' else '' end as causa, \n"
                    + "                                    \n"
                    + "                                     exg.observaciones,        \n"
                    + "                                     d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre,  \n"
                    + "                                    d.rut as rutdoctor , d.profesion, exg.tipo, exg.problemaauge \n"
                    + "                                      FROM  \n"
                    + "                                      agenda.atencion_clinica_tecnologo act  \n"
                    + "                                      inner join agenda.cita c on c.id_cita = act.id_cita  \n"
                    + "                                      inner join agenda.exception_garantia exg on exg.id_atencion = act.id_atencion_tecnologo\n"
                    + "                                     join agenda.paciente p on p.rut = c.rut_paciente  \n"
                    + "                                      join agenda.doctor d on act.tecnolog = d.rut where act.id_atencion_tecnologo = " + idatencion + " and exg.tipo= " + tipo + " ;";

        } else {
            query = " SELECT 'EG-'|| to_char( act.fecharegistro, 'yyyymmdd')||'--OFT-'||  case when char_length(act.idatencionoftalmologia)>4 then cast(act.idatencionoftalmologia as text) else to_char(act.idatencionoftalmologia, '0000') end  as folio, \n"
                    + "                                               to_char(act.fecharegistro,'dd-mm-yyyy HH24:MI') as fecha , \n"
                    + "                                                    p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,  \n"
                    + "                                                    p.rut, to_char( p.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento, to_char(age(p.fecha_nacimiento),'yy') as edad,  \n"
                    + "                                                    case when exg.rechazo_prestador_designado = 1 then '1- Rechazo del prestador Designado ' else ' ' end ||\n"
                    + "                                                    case when exg.rechazo_atencion_garantizado = 1 then '2- Rechazo de la atención o procedimiento garantizado ' else '' end ||\n"
                    + "                                                    case when exg.prestacion_rechazada = 1 then ' 3- Prestación Rechazada ' else '' end ||\n"
                    + "                                                    case when exg.otra_causa = 1 then ' 4- Otra Causas definida por el Paciente ' else '' end as causa, \n"
                    + "                                                    \n"
                    + "                                                     exg.observaciones,        \n"
                    + "                                                     d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre,  \n"
                    + "                                                    d.rut as rutdoctor , d.profesion, exg.tipo, exg.problemaauge \n"
                    + "                                                      FROM  \n"
                    + "                                                     agenda.atencionoftalmologia act  \n"
                    + "                                                      inner join agenda.cita c on c.id_cita = act.idcita\n"
                    + "                                                      inner join agenda.exception_garantia exg on exg.id_atencion = act.idatencionoftalmologia\n"
                    + "                                                     join agenda.paciente p on p.rut = c.rut_paciente  \n"
                    + "                                                      join agenda.doctor d on act.usuario = d.rut where act.idatencionoftalmologia = " + idatencion + " and exg.tipo= " + tipo + "";

        }

        this.cnn.setSentenciaSQL(query);
        this.cnn.conectar();
        excepcion_garantia exg = new excepcion_garantia();
        try {
            while (this.cnn.getRst().next()) {
                exg.setAuxiliar1(this.cnn.getRst().getString("folio"));
                exg.setAuxiliar2(this.cnn.getRst().getString("fecha"));
                exg.setAuxiliar3(this.cnn.getRst().getString("nombre"));
                exg.setAuxiliar4(this.cnn.getRst().getString("rut"));
                exg.setAuxiliar5(this.cnn.getRst().getString("fechanacimiento"));
                exg.setAuxiliar6(this.cnn.getRst().getString("edad"));
                exg.setAuxiliar7(this.cnn.getRst().getString("causa"));
                exg.setObservaciones(this.cnn.getRst().getString("observaciones"));
                exg.setAuxiliar8(this.cnn.getRst().getString("doctornombre"));
                exg.setAuxiliar9(this.cnn.getRst().getString("rutdoctor"));
                exg.setAuxiliar10(this.cnn.getRst().getString("profesion"));
                exg.setProblemaauge(this.cnn.getRst().getString("problemaauge"));
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return exg;
    }

    public String eliminarunMotivodeConsulta(int motivo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.motivoconsulta  \n"
                + "SET \n"
                + " \n"
                + "  estatus = 0\n"
                + " \n"
                + "WHERE \n"
                + "  id_motivo = " + motivo + "\n ;");
        this.cnn.conectar();

        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Motivo de Consulta Eliminado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public void eliminartodosLosPerfilesMotivosdeunMotivo(int motivo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + " agenda.motivo_perfil \n"
                + "WHERE id_motivo = " + motivo + " ;");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    public String ActualizarMotivodeConsulta(motivo_consulta m) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.motivoconsulta  \n"
                + "SET \n"
                + "  nombremotivo = '" + m.getNombremotivo() + "',\n"
                + "  id_atencion = " + m.getAtencion_cita() + " \n"
                + " \n"
                + "WHERE \n"
                + "  id_motivo = " + m.getId_motivo() + "\n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Motivo de Consulta Actualizado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    public ArrayList buscarlosrolesQuenoestanenunMotivodeConsulta(int idmotivo) {
        ArrayList ar = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT * FROM \n"
                + " seguridad.rol r \n"
                + " where r.idrol not in (select id_perfil from  agenda.motivo_perfil where id_motivo = " + idmotivo + " )\n"
                + " and   r.estatus = 1 AND r.id_sistema = 3\n"
                + "    ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                rol r = new rol();
                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                r.setNombreRol(this.cnn.getRst().getString("nombrerol"));

                ar.add(r);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return ar;
    }

    public ArrayList buscarlosrolesdeunMotivodeConsulta(int idmotivo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        ArrayList ar = new ArrayList();
        this.cnn.setSentenciaSQL("SELECT * FROM \n"
                + "  seguridad.rol r inner join agenda.motivo_perfil mp \n"
                + "   on r.idrol = mp.id_perfil \n"
                + "    where   r.estatus = 1 AND r.id_sistema = 3\n"
                + "    and mp.id_motivo = " + idmotivo + ";");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                rol r = new rol();
                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                r.setNombreRol(this.cnn.getRst().getString("nombrerol"));

                ar.add(r);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return ar;
    }

    /*ingresar los motivos de consultas*/
    public motivo_consulta buscarMotivodeConsultaporId(int idmotivo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        motivo_consulta m = new motivo_consulta();
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  nombremotivo,\n"
                + "  id_atencion,\n"
                + "  estatus\n"
                + " FROM \n"
                + "  agenda.motivoconsulta where id_motivo = " + idmotivo + " ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {

                m.setAtencion_cita(this.cnn.getRst().getInt("id_atencion"));
                m.setNombremotivo(this.cnn.getRst().getString("nombremotivo"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return m;
    }

    public Vector<motivo_consulta> buscarlosmotivoConsultasActivos() {
        Vector<motivo_consulta> mc = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  m.id_motivo, \n"
                + "  nombremotivo,\n"
                + "  a.nombre\n"
                + "  \n"
                + "FROM \n"
                + "  agenda.motivoconsulta m  \n"
                + "  join agenda.atencion a on a.id_atencion= m.id_atencion \n"
                + "  where m.estatus = 1 ;");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                motivo_consulta m = new motivo_consulta();
                m.setId_motivo(this.cnn.getRst().getInt("id_motivo"));
                m.setNombremotivo(this.cnn.getRst().getString("nombremotivo"));
                m.setNombreatencion(this.cnn.getRst().getString("nombre"));
                mc.add(m);

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return mc;
    }

    public String insertarAtencionRespladoClinica(atencion_clinica_oftalmologia ac) throws UnknownHostException {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.atencion_clinica_oftalmologia2\n"
                + "(\n"
                + "  id_atencion_oftalmologia ,\n"
                + "  id_cita,\n"
                + "  autorefraccion,\n"
                + "  autorefaccionobservaciones,\n"
                + "  presionio,\n"
                + "  presionod,\n"
                + "  numerooi,\n"
                + "  numerood,\n"
                + "  presionobservaciones,\n"
                + "  dilatacionoi,\n"
                + "  dilatacionod,\n"
                + "  dilatacion_colirio,\n"
                + "  dilatacion_observaciones,\n"
                + "  registro_de_observaciones,\n"
                + "  idusuario,\n"
                + "  usario,\n"
                + "  fecha_registro"
                + ") \n"
                + "VALUES (\n"
                + "  '" + ac.getId_atencion_coftalmologia() + "',\n"
                + "  '" + ac.getId_cita() + "',\n"
                + "  " + ac.getAutorefraccion() + ",\n"
                + "  '" + ac.getAutorefaccionobservaciones() + "',\n"
                + "  " + ac.getPresionio() + ",\n"
                + "  " + ac.getPresionod() + ",\n"
                + "  " + ac.getNumerooi() + ",\n"
                + "  " + ac.getNumerood() + ",\n"
                + "  '" + ac.getPresionobservaciones() + "',\n"
                + "  " + ac.getDilatacionoi() + ",\n"
                + "  " + ac.getDilatacionod() + ",\n"
                + "  '" + ac.getDilatacion_colirio() + "' ,\n"
                + "  '" + ac.getDilatacion_observaciones() + "' ,\n"
                + "  '" + ac.getRegistro_de_observaciones() + "',\n"
                + "  '" + ac.getIdusuario() + "' ,"
                + "  '" + ac.getUsuario() + "' , "
                + "   CURRENT_TIMESTAMP "
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion Clinica de Oftalmologia Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    public String eliminarReceta(int idreceta) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("DELETE FROM \n"
                + "  agenda.receta_lente_opticos \n"
                + "WHERE id_receta = " + idreceta + " \n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion Clinica de Oftalmologia Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    public excepcion_garantia buscarGarantiaporIddeAtencion(int idatencion, int tipo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        excepcion_garantia garantia = new excepcion_garantia();
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idexceptiondegarantia,\n"
                + " \n"
                + "  rechazo_prestador_designado,\n"
                + "  rechazo_atencion_garantizado,\n"
                + "  prestacion_rechazada,\n"
                + "  otra_causa,\n"
                + "  observaciones,"
                + "   problemaauge\n"
                + "FROM \n"
                + "  agenda.exception_garantia where id_atencion = " + idatencion + " and tipo="+tipo+" ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                garantia.setIdexcepcion_garantia(this.cnn.getRst().getInt("idexceptiondegarantia"));
                garantia.setRechazo_prestador_designado(this.cnn.getRst().getInt("rechazo_prestador_designado"));
                garantia.setRechazo_atencion_garantizado(this.cnn.getRst().getInt("rechazo_atencion_garantizado"));
                garantia.setPrestacion_rechazada(this.cnn.getRst().getInt("prestacion_rechazada"));
                garantia.setOtra_causa(this.cnn.getRst().getInt("otra_causa"));
                garantia.setObservaciones(this.cnn.getRst().getString("observaciones"));
                garantia.setProblemaauge(this.cnn.getRst().getString("problemaauge"));

            }
        } catch (SQLException ex) {
        }

        this.cnn.cerrarConexion();
        return garantia;

    }

    public void ModificarGarantia(excepcion_garantia eg) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.exception_garantia  \n"
                + "SET \n"
                + "  \n"
                + "  rechazo_prestador_designado = " + eg.getRechazo_prestador_designado() + ",\n"
                + "  rechazo_atencion_garantizado = " + eg.getRechazo_atencion_garantizado() + ",\n"
                + "  prestacion_rechazada = " + eg.getPrestacion_rechazada() + ",\n"
                + "  otra_causa = " + eg.getOtra_causa() + ",\n"
                + "  observaciones = '" + eg.getObservaciones() + "'\n"
                + " \n"
                + "WHERE \n"
                + "idexceptiondegarantia = " + eg.getIdexcepcion_garantia() + " and tipo = " + eg.getTipo() + "\n"
                + ";");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void insertarGarantia(excepcion_garantia eg) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.exception_garantia\n"
                + "(\n"
                + "  id_atencion,\n"
                + "  rechazo_prestador_designado,\n"
                + "  rechazo_atencion_garantizado,\n"
                + "  prestacion_rechazada,\n"
                + "  otra_causa,\n"
                + "  observaciones, tipo, problemaauge\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + eg.getId_atencion() + ",\n"
                + "  " + eg.getRechazo_prestador_designado() + ",\n"
                + "  " + eg.getRechazo_atencion_garantizado() + ",\n"
                + "  " + eg.getPrestacion_rechazada() + ",\n"
                + "  " + eg.getOtra_causa() + ",\n"
                + "  '" + eg.getObservaciones() + "',"
                + "  " + eg.getTipo() + ","
                + "  '" + eg.getProblemaauge() + "'\n"
                + ");");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public String insertarAtencionClinica(atencion_clinica_oftalmologia ac) throws UnknownHostException {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.atencion_clinica_oftalmologia\n"
                + "(\n"
                + "  id_cita,\n"
                + "  autorefraccion,\n"
                + "  autorefaccionobservaciones,\n"
                + "  presionio,\n"
                + "  presionod,\n"
                + "  numerooi,\n"
                + "  numerood,\n"
                + "  presionobservaciones,\n"
                + "  dilatacionoi,\n"
                + "  dilatacionod,\n"
                + "  dilatacion_colirio,\n"
                + "  dilatacion_observaciones,\n"
                + "  registro_de_observaciones,\n"
                + "  idusuario,\n"
                + "  usuario,\n"
                + "  fecha_registro"
                + ") \n"
                + "VALUES (\n"
                + "  '" + ac.getId_cita() + "',\n"
                + "  " + ac.getAutorefraccion() + ",\n"
                + "  '" + ac.getAutorefaccionobservaciones() + "',\n"
                + "  " + ac.getPresionio() + ",\n"
                + "  " + ac.getPresionod() + ",\n"
                + "  " + ac.getNumerooi() + ",\n"
                + "  " + ac.getNumerood() + ",\n"
                + "  '" + ac.getPresionobservaciones() + "',\n"
                + "  " + ac.getDilatacionoi() + ",\n"
                + "  " + ac.getDilatacionod() + ",\n"
                + "  '" + ac.getDilatacion_colirio() + "' ,\n"
                + "  '" + ac.getDilatacion_observaciones() + "' ,\n"
                + "  '" + ac.getRegistro_de_observaciones() + "',\n"
                + "  '" + getIpUsuarioLoggin() + "' ,"
                + "  '" + getUsuarioLoggin() + "' , "
                + "   CURRENT_TIMESTAMP "
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion Clinica de Oftalmologia Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    public String IngresarAtenciontecnologo(atencion_clinica_tecnologo act) {
        String mensaje = "";

        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.atencion_clinica_tecnologo\n"
                + "(  id_cita,\n"
                + "  fecha_registro,\n"
                + "  tecnolog ,  pertinencia ) \n"
                + "VALUES (\n"
                + " '" + act.getId_cita() + "' ,\n"
                + "  CURRENT_TIMESTAMP ,\n"
                + "  '" + getUsuarioLoggin() + "' , " + act.getPertinencia() + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion Clinica de Oftalmologia Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

    public String modificarAtencionTecnologica(atencion_clinica_tecnologo act) {
        String mensaje = "";

        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("UPDATE agenda.atencion_clinica_tecnologo\n"
                + "   SET  pertinencia=" + act.getPertinencia() + "\n"
                + " WHERE id_atencion_tecnologo = " + act.getId_atencion_tecnologo() + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion Clinica de Oftalmologia Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public receta_lente_opticos buscarRecetadeIdAtencion(int idatencion, int tipo) {
        receta_lente_opticos rlo = new receta_lente_opticos();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_receta,\n"
                + "  id_atencion,\n"
                + "  ojoderechoesfera,\n"
                + "  ojoderechocilindro,\n"
                + "  ojoderechoeje,\n"
                + "  ojoizquierdoesfera,\n"
                + "  ojoiaquierdacilindro,\n"
                + "  ojoizquierdaeje,\n"
                + "  aadd,\n"
                + "  distanciapupilarlejos,\n"
                + "  distanciapupilarcerca,\n"
                + "  tipo,\n"
                + "  observaciones\n"
                + "  FROM \n"
                + "  agenda.receta_lente_opticos where id_atencion = " + idatencion + "  and tipo= " + tipo + ";");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                rlo.setId_receta(this.cnn.getRst().getInt("id_receta"));
                rlo.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                rlo.setOjoderechoesfera(this.cnn.getRst().getString("ojoderechoesfera"));
                rlo.setOjoderechocilindro(this.cnn.getRst().getString("ojoderechocilindro"));
                rlo.setOjoderechoeje(this.cnn.getRst().getString("ojoderechoeje"));
                rlo.setOjoizquierdoesfera(this.cnn.getRst().getString("ojoizquierdoesfera"));
                rlo.setOjoiaquierdacilindro(this.cnn.getRst().getString("ojoiaquierdacilindro"));
                rlo.setOjoizquierdaeje(this.cnn.getRst().getString("ojoizquierdaeje"));
                rlo.setAadd(this.cnn.getRst().getString("aadd"));
                rlo.setDistanciapupilarcerca(this.cnn.getRst().getString("distanciapupilarcerca"));
                rlo.setDistanciapupilarlejos(this.cnn.getRst().getString("distanciapupilarlejos"));
                rlo.setObservaciones(this.cnn.getRst().getString("observaciones"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return rlo;
    }

    public Vector<lugar> buscarlosarchivosdepaciente(String rut) {
        Vector<lugar> vl = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT act.fecha_registro , d.rut ||'-'|| d.nombre ||' '|| d.apellido_paterno as doctor,\n"
                + "   r.id_atencion, \n"
                + "   case when r.vicio_refraccion =1 then true else false end as vr ,\n"
                + "   case when r.confima = 1 then true else false end as c,\n"
                + "    case when COALESCE((SELECT id_receta  \n"
                + "    FROM \n"
                + "   agenda.receta_lente_opticos  where id_atencion =r.id_atencion),0) != 0 then true else false end as rc\n"
                + "   \n"
                + "   FROM \n"
                + "      agenda.registropatalogia r inner join agenda.atencion_clinica_tecnologo act\n"
                + "     on r.id_atencion = act.id_atencion_tecnologo\n"
                + "     join agenda.cita c on c.id_cita = act.id_cita \n"
                + "      join agenda.doctor d on d.rut = act.tecnolog\n"
                + "   where upper(c.rut_paciente) =upper('" + rut + "')");

        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                lugar tem = new lugar();
                tem.setFecha(this.cnn.getRst().getDate("fecha_registro"));
                tem.setId_lugar(this.cnn.getRst().getInt("id_atencion"));
                tem.setNombre(this.cnn.getRst().getString("doctor"));
                tem.setTemporal(this.cnn.getRst().getBoolean("vr"));
                tem.setTemporal1(this.cnn.getRst().getBoolean("c"));
                tem.setTemporal2(this.cnn.getRst().getBoolean("rc"));
                vl.add(tem);

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return vl;
    }

    /*ver si na atencion tiene lentes*/
    public boolean tengorecetaIdAtencion(int idatencion, int tipo) {
        boolean tengo = false;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT *  \n"
                + "  FROM \n"
                + "  agenda.receta_lente_opticos  where id_atencion =" + idatencion + " and tipo = " + tipo + " ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                tengo = true;

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return tengo;

    }

    public atencion_clinica_tecnologo buscarencabezadoparaInformedeAtencion(int idatencion) {
        atencion_clinica_tecnologo act = new atencion_clinica_tecnologo();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                  to_char(act.fecha_registro, 'dd/mm/yyyy') as fechallegada, \n"
                + "                  to_char(act.fecha_registro, 'hh:mm:ss') as horallegada,    \n"
                + "                   p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,   \n"
                + "                   p.rut,  \n"
                + "                     to_char( p.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,  \n"
                + "                     to_char(age(p.fecha_nacimiento),'yy') as edad,   \n"
                + "                     case when p.genero =1 then 'Femenino'else 'Masculino' end as sexo,  \n"
                + "                   p.contacto1  ||'/ '|| p.contacto2 as telefono,  \n"
                + "                    p.direccion, \n"
                + "                   cu.nombre as comuna,  \n"
                + "                  act.id_atencion_tecnologo, \n"
                + "                    c.id_cita, \n"
                + "                    case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                     inner join   agenda.diagnostico_especialidad des\n"
                + "                     on di.iddiagnostico = des.iddiagnostico\n"
                + "                      where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + "                     FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as motivo, \n"
                + "                  previ.nombre as prevision, \n"
                + "              \n"
                + "                  d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor, \n"
                + "                   d.rut as rutdoctor,  d.profesion \n"
                + "                   FROM \n"
                + "                  agenda.atencion_clinica_tecnologo act \n"
                + "                 join agenda.cita c on c.id_cita = act.id_cita \n"
                + "                 join agenda.paciente p on p.rut = c.rut_paciente \n"
                + "                 join agenda.comuna cu on p.id_comuna = cu.id_comuna \n"
                + "                 \n"
                + "                join agenda.prevision previ on previ.id_prevision = p.provision \n"
                + "                join agenda.doctor d on act.tecnolog = d.rut where act.id_atencion_tecnologo= " + idatencion + "");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                act.setAuxiliaruno(this.cnn.getRst().getString("fechallegada"));
                act.setAuxiliardos(this.cnn.getRst().getString("horallegada"));
                act.setAuxiliartres(this.cnn.getRst().getString("nombre"));
                act.setAuxiliarcinco(this.cnn.getRst().getString("rut"));
                act.setAuxiliarseis(this.cnn.getRst().getString("fechanacimiento"));
                act.setAuxiliarsiete(this.cnn.getRst().getString("edad"));
                act.setAuxiliarocho(this.cnn.getRst().getString("sexo"));

                act.setAuxiliardiez(this.cnn.getRst().getString("telefono"));
                act.setAuxiliaronce(this.cnn.getRst().getString("comuna"));
                act.setId_atencion_tecnologo(this.cnn.getRst().getInt("id_atencion_tecnologo"));
                act.setId_cita(this.cnn.getRst().getInt("id_cita"));
                act.setAuxiliardoce(this.cnn.getRst().getString("motivo"));
                act.setAuxiliartrece(this.cnn.getRst().getString("prevision"));
                act.setAuxiliarcatorce(this.cnn.getRst().getString("nombredoctor"));
                act.setAuxiliarquince(this.cnn.getRst().getString("rutdoctor"));
                act.setTecnologo(this.cnn.getRst().getString("profesion"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return act;
    }

    public registrodepatologia BuscarRegistrodePatologia(int idatencion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        registrodepatologia rp = new registrodepatologia();
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_registropatologia,\n"
                + "  vicio_refraccion,\n"
                + "  diagnostico,\n"
                + "  fundamentodeldiagnostico,\n"
                + "  tratamientoeindicacion,\n"
                + "  confima,\n"
                + "  tratamientocerca,\n"
                + "  tratamientolejos,\n"
                + "  id_atencion\n"
                + "FROM \n"
                + "  agenda.registropatalogia where id_atencion = " + idatencion + "  ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                rp.setId_registropatologia(this.cnn.getRst().getInt("id_registropatologia"));
                rp.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                rp.setVicio_refraccion(this.cnn.getRst().getInt("vicio_refraccion"));
                rp.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                rp.setFundamentodeldiagnostico(this.cnn.getRst().getString("fundamentodeldiagnostico"));
                rp.setTratamientoeindicacion(this.cnn.getRst().getString("tratamientoeindicacion"));
                rp.setConfima(this.cnn.getRst().getInt("confima"));
                rp.setTratamientocerca(this.cnn.getRst().getInt("tratamientocerca"));
                rp.setTratamientolejos(this.cnn.getRst().getInt("tratamientolejos"));
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return rp;
    }

    public Integer buscaridtencionTecnologoconIdCita(int id_cita) {
        int idatencion = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL(" SELECT \n"
                + "  id_atencion_tecnologo\n"
                + "  FROM \n"
                + "  agenda.atencion_clinica_tecnologo where id_cita = " + id_cita + ";");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                idatencion = this.cnn.getRst().getInt("id_atencion_tecnologo");

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return idatencion;
    }

    /*Modificar atencion del tens*/
    public String ModificarRegistrodelTens(atencion_clinica_oftalmologia aco) throws UnknownHostException {

        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.atencion_clinica_oftalmologia  \n"
                + "SET \n"
                + "  autorefraccion = " + aco.getAutorefraccion() + ",\n"
                + "  autorefaccionobservaciones = '" + aco.getAutorefaccionobservaciones() + "',\n"
                + "  presionio = " + aco.getPresionio() + ",\n"
                + "  presionod = " + aco.getPresionod() + ",\n"
                + "  numerooi = " + aco.getNumerooi() + ",\n"
                + "  numerood = " + aco.getNumerood() + ",\n"
                + "  presionobservaciones = '" + aco.getPresionobservaciones() + "',\n"
                + "  dilatacionoi = " + aco.getDilatacionoi() + ",\n"
                + "  dilatacion_colirio = '" + aco.getDilatacion_colirio() + "',\n"
                + "  dilatacion_observaciones = '" + aco.getDilatacion_observaciones() + "',\n"
                + "  registro_de_observaciones = '" + aco.getRegistro_de_observaciones() + "',\n"
                + "  idusuario = '" + getIpUsuarioLoggin() + "',\n"
                + "  usuario = '" + getUsuarioLoggin() + "',\n"
                + "  fecha_registro = CURRENT_TIMESTAMP,\n"
                + "  dilatacionod = " + aco.getDilatacionod() + "\n"
                + " \n"
                + "WHERE \n"
                + "  id_atencion_oftalmologia = " + aco.getId_atencion_coftalmologia() + "\n"
                + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Atencion Clinica de Oftalmologo Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    public Integer BuscaridAtencionOftalmoloporIdCita(int id_cita) {
        int idatencion = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL(" SELECT id_atencionoftalmologo\n"
                + "FROM \n"
                + "  agenda.atencion_clinica_oftalmologo where id_cita = " + id_cita + " ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                idatencion = this.cnn.getRst().getInt("id_atencionoftalmologo");

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return idatencion;
    }

    public atencion_clinica_oftalmologia BuscarAtencionTecnicoporCita(int cita) {
        atencion_clinica_oftalmologia aco = new atencion_clinica_oftalmologia();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_atencion_oftalmologia ,  \n"
                + "  case when  autorefraccion = 1 then autorefaccionobservaciones else 'sin detalle' end as autorefraccion,\n"
                + "  case when  presionio = 1 then  'OI '|| numerooi else '' end ||\n"
                + "  case when presionod = 1 then ' - OD '|| numerood else '' end ||\n"
                + "  case when  presionobservaciones != '' then  presionobservaciones else ' sin detalle' end as presion,\n"
                + "  case when dilatacionoi = 1 then 'OI ' else '' end ||\n"
                + "  case when dilatacionod =1 then 'OD ' else '' end  ||\n"
                + "  case when  dilatacion_colirio != '' then dilatacion_colirio ||' ;'  else 'sin detalle en Colirio ; ' end ||\n"
                + "  case when  dilatacion_observaciones != '' then dilatacion_observaciones ||' ' else 'sin detalle en Dilatacion' end as dilatacion,\n"
                + "  case when registro_de_observaciones != '' then  registro_de_observaciones else 'sin detalle en Observacion' end as observacionfinal\n"
                + "\n"
                + "FROM \n"
                + "  agenda.atencion_clinica_oftalmologia where id_cita = " + cita + ";");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                aco.setId_atencion_coftalmologia(this.cnn.getRst().getInt("id_atencion_oftalmologia"));
                aco.setAutorefaccionobservaciones(this.cnn.getRst().getString("autorefraccion"));
                aco.setPresionobservaciones(this.cnn.getRst().getString("presion"));
                aco.setDilatacion_observaciones(this.cnn.getRst().getString("dilatacion"));
                aco.setRegistro_de_observaciones(this.cnn.getRst().getString("observacionfinal"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return aco;
    }

    public atencion_clinica_oftalmologia BuscarAtencionTensporCita(int idcita) {
        atencion_clinica_oftalmologia aco = new atencion_clinica_oftalmologia();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion_oftalmologia,\n"
                + "  id_cita,\n"
                + "  autorefraccion,\n"
                + "  autorefaccionobservaciones,\n"
                + "  presionio,\n"
                + "  presionod,\n"
                + "  numerooi,\n"
                + "  numerood,\n"
                + "  presionobservaciones,\n"
                + "  dilatacionoi,\n"
                + "  dilatacion_colirio,\n"
                + "  dilatacion_observaciones,\n"
                + "  registro_de_observaciones,\n"
                + "  idusuario,\n"
                + "  usuario,\n"
                + "  fecha_registro,\n"
                + "  dilatacionod\n"
                + "FROM \n"
                + "  agenda.atencion_clinica_oftalmologia where id_cita = " + idcita + "  ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                aco.setId_atencion_coftalmologia(this.cnn.getRst().getInt("id_atencion_oftalmologia"));
                aco.setId_cita(this.cnn.getRst().getInt("id_cita"));
                aco.setAutorefraccion(this.cnn.getRst().getInt("autorefraccion"));
                aco.setAutorefaccionobservaciones(this.cnn.getRst().getString("autorefaccionobservaciones"));
                aco.setPresionio(this.cnn.getRst().getInt("presionio"));
                aco.setPresionod(this.cnn.getRst().getInt("presionod"));
                aco.setNumerooi(this.cnn.getRst().getDouble("numerooi"));
                aco.setNumerood(this.cnn.getRst().getDouble("numerood"));
                aco.setPresionobservaciones(this.cnn.getRst().getString("presionobservaciones"));
                aco.setDilatacionod(this.cnn.getRst().getInt("dilatacionod"));
                aco.setDilatacionoi(this.cnn.getRst().getInt("dilatacionoi"));
                aco.setDilatacion_observaciones(this.cnn.getRst().getString("dilatacion_observaciones"));
                aco.setRegistro_de_observaciones(this.cnn.getRst().getString("registro_de_observaciones"));
                aco.setDilatacion_colirio(this.cnn.getRst().getString("dilatacion_colirio"));
                aco.setFecha_registro(this.cnn.getRst().getDate("fecha_registro"));
                aco.setUsuario(this.cnn.getRst().getString("usuario"));
                aco.setIdusuario(this.cnn.getRst().getString("idusuario"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return aco;
    }

    public atencion_clinica_oftalmologia BuscarAtencionTensporId(int id) {
        atencion_clinica_oftalmologia aco = new atencion_clinica_oftalmologia();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion_oftalmologia,\n"
                + "  id_cita,\n"
                + "  autorefraccion,\n"
                + "  autorefaccionobservaciones,\n"
                + "  presionio,\n"
                + "  presionod,\n"
                + "  numerooi,\n"
                + "  numerood,\n"
                + "  presionobservaciones,\n"
                + "  dilatacionoi,\n"
                + "  dilatacion_colirio,\n"
                + "  dilatacion_observaciones,\n"
                + "  registro_de_observaciones,\n"
                + "  idusuario,\n"
                + "  usuario,\n"
                + "  fecha_registro,\n"
                + "  dilatacionod\n"
                + "FROM \n"
                + "  agenda.atencion_clinica_oftalmologia where id_atencion_oftalmologia = " + id + "  ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                aco.setId_atencion_coftalmologia(this.cnn.getRst().getInt("id_atencion_oftalmologia"));
                aco.setId_cita(this.cnn.getRst().getInt("id_cita"));
                aco.setAutorefraccion(this.cnn.getRst().getInt("autorefraccion"));
                aco.setAutorefaccionobservaciones(this.cnn.getRst().getString("autorefaccionobservaciones"));
                aco.setPresionio(this.cnn.getRst().getInt("presionio"));
                aco.setPresionod(this.cnn.getRst().getInt("presionod"));
                aco.setNumerooi(this.cnn.getRst().getDouble("numerooi"));
                aco.setNumerood(this.cnn.getRst().getDouble("numerood"));
                aco.setPresionobservaciones(this.cnn.getRst().getString("presionobservaciones"));
                aco.setDilatacionod(this.cnn.getRst().getInt("dilatacionod"));
                aco.setDilatacionoi(this.cnn.getRst().getInt("dilatacionoi"));
                aco.setDilatacion_observaciones(this.cnn.getRst().getString("dilatacion_observaciones"));
                aco.setRegistro_de_observaciones(this.cnn.getRst().getString("registro_de_observaciones"));
                aco.setDilatacion_colirio(this.cnn.getRst().getString("dilatacion_colirio"));
                aco.setFecha_registro(this.cnn.getRst().getDate("fecha_registro"));
                aco.setUsuario(this.cnn.getRst().getString("usuario"));
                aco.setIdusuario(this.cnn.getRst().getString("idusuario"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return aco;

    }

    public String InsertarRecetaLenteOpticos(receta_lente_opticos rlo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.receta_lente_opticos\n"
                + "(\n"
                + "  id_atencion,\n"
                + "  ojoderechoesfera,\n"
                + "  ojoderechocilindro,\n"
                + "  ojoderechoeje,\n"
                + "  ojoizquierdoesfera,\n"
                + "  ojoiaquierdacilindro,\n"
                + "  ojoizquierdaeje,\n"
                + "  aadd,\n"
                + "  distanciapupilarlejos,\n"
                + "  distanciapupilarcerca,\n"
                + "  tipo,\n"
                + "  observaciones\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + rlo.getId_atencion() + ",\n"
                + "  '" + rlo.getOjoderechoesfera() + "',\n"
                + "  '" + rlo.getOjoderechocilindro() + "',\n"
                + "  '" + rlo.getOjoderechoeje() + "',\n"
                + "  '" + rlo.getOjoizquierdoesfera() + "',\n"
                + "  '" + rlo.getOjoiaquierdacilindro() + "',\n"
                + "  '" + rlo.getOjoizquierdaeje() + "',\n"
                + "  '" + rlo.getAadd() + "',\n"
                + "  '" + rlo.getDistanciapupilarlejos() + "',\n"
                + "  '" + rlo.getDistanciapupilarcerca() + "',\n"
                + "  " + rlo.getTipo() + ",\n"
                + "  '" + rlo.getObservaciones() + "'\n"
                + "); ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Receta Lente Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    /*registrar vicio refraccion*/
    public String InsertarvicioRefraccion(vicio_refraccion vr) {

        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.viciorefraccion\n"
                + "(\n"
                + "  id_atencion,\n"
                + "  amhta,\n"
                + "  amdm,\n"
                + "  anosam,\n"
                + "  amglaucoma,\n"
                + "  amotro,\n"
                + "  piod,\n"
                + "  pioi,\n"
                + "  plscod,\n"
                + "  plscoi,\n"
                + "  plccod,\n"
                + "  plccoi,\n"
                + "  plccodi,\n"
                + "  reflejofotomotor,\n"
                + "  rojopopular,\n"
                + "  bmcpoloposterior,\n"
                + "  motivoconsulta, \n"
                + "  indicaciones,"
                + "   ges \n"
                + ") \n"
                + "VALUES (\n"
                + "  " + vr.getId_atencion() + ",\n"
                + "  " + vr.getAmhta() + ",\n"
                + "  " + vr.getAmdm() + ",\n"
                + "  '" + vr.getAnosam() + "',\n"
                + "  " + vr.getAmglaucoma() + ",\n"
                + "  '" + vr.getAmotro() + "',\n"
                + "  '" + vr.getPiod() + "',\n"
                + "  '" + vr.getPioi() + "',\n"
                + "  '" + vr.getPlscod() + "',\n"
                + "  '" + vr.getPlscoi() + "',\n"
                + "  '" + vr.getPlccod() + "',\n"
                + "  '" + vr.getPlccoi() + "',\n"
                + "  '" + vr.getPlccadi() + "',\n"
                + "  '" + vr.getReflejofotomotor() + "',\n"
                + "  '" + vr.getRojopopular() + "',\n"
                + "  '" + vr.getBmcpoloposterior() + "',\n"
                + "  '" + vr.getMotivoconsulta() + "',\n"
                + "  '" + vr.getIndicaciones() + "',\n"
                + "  " + vr.getGes() + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Receta Lente Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    /*para registrar la patologia */
    public String ingresarRegistrodePatologia(registrodepatologia rp) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.registropatalogia\n"
                + "(\n"
                + "  vicio_refraccion,\n"
                + "  diagnostico,\n"
                + "  fundamentodeldiagnostico,\n"
                + "  tratamientoeindicacion,\n"
                + "  confima,\n"
                + "  tratamientocerca,\n"
                + "  tratamientolejos,\n"
                + "  id_atencion\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + rp.getVicio_refraccion() + ",\n"
                + "  '" + rp.getDiagnostico() + "',\n"
                + "  '" + rp.getFundamentodeldiagnostico() + "',\n"
                + "  '" + rp.getTratamientoeindicacion() + "',\n"
                + "  " + rp.getConfima() + ",\n"
                + "  " + rp.getTratamientocerca() + ",\n"
                + "  " + rp.getTratamientolejos() + ",\n"
                + "   " + rp.getId_atencion() + " \n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Receta Lente Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    /*Motivo de la consulta*/
    public String InsertarMotivodeConsulta(motivo_consulta mc) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.motivoconsulta\n"
                + "(\n"
                + "  id_motivo ,\n"
                + "  nombremotivo,\n"
                + "  id_atencion,\n"
                + "  estatus\n"
                + ") \n"
                + "VALUES (\n"
                + "  '" + mc.getId_motivo() + "',\n"
                + "  '" + mc.getNombremotivo() + "',\n"
                + "  " + mc.getAtencion_cita() + ",\n"
                + "  1 \n"
                + "); ");

        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Motivo de Consulta Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    /*guardar los motivos por perfiles */
    public String motivoPerfiles(int motivo, int perfiles) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.motivo_perfil\n"
                + "(\n"
                + "  id_motivo ,\n"
                + "  id_perfil\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + motivo + ",\n"
                + "  " + perfiles + "\n"
                + "); ");

        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Motivo / Perfiles Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    public int buscarUltimoidMotivoConsulta() {
        int ultimo = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  count(id_motivo)\n"
                + "FROM \n"
                + "  agenda.motivoconsulta ; ");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                ultimo = this.cnn.getRst().getInt("count");

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();

        return ultimo;
    }

    /*buscarmotivos de consulta debe ser filtrado por atencion y por perfil del usuario*/
    public Vector<motivo_consulta> buscarMotivo(int atencion, Vector<rol> perfil) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        String p = "";
        for (int i = 0; i < perfil.size(); ++i) {
            if (i == 0) {
                p = Integer.toString(perfil.get(i).getIdRol());
            } else {
                p = p + "," + perfil.get(i).getIdRol();
            }
        }

        this.cnn.setSentenciaSQL("Select m.id_motivo, nombremotivo from "
                + " agenda.motivoconsulta m inner join agenda.motivo_perfil mp"
                + " on m.id_motivo = mp.id_motivo"
                + " where id_atencion= " + atencion + " and mp.id_perfil in(" + p + ") and m.estatus = 1  group by m.id_motivo,nombremotivo ");
        Vector<motivo_consulta> mc = new Vector();

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                motivo_consulta motivo = new motivo_consulta();
                motivo.setId_motivo(this.cnn.getRst().getInt("id_motivo"));
                motivo.setNombremotivo(this.cnn.getRst().getString("nombremotivo"));
                mc.add(motivo);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return mc;

    }

    /*buscar perfil del doctor esto para buscar los motivos */
    public Vector<rol> buscarPerfildeDoctor(String rut) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        Vector<rol> perfil = new Vector<rol>();
        this.cnn.setSentenciaSQL("select r.idrol\n"
                + "from seguridad.usuario u\n"
                + " inner join agenda.vistafuncionario v   on u.id_funcionario = v.rut_funcionario\n"
                + "join seguridad.usuario_rol ur on ur.id_usuario = u.id_usuario \n"
                + "join seguridad.rol r on ur.id_rol= r.idrol where upper(u.id_funcionario)= upper('" + rut + "') and r.id_sistema= 3");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                rol r = new rol();
                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                perfil.add(r);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return perfil;
    }

    public ArrayList traerlasultimas5citas(String ruppaciente, int motivo) {
        ArrayList citas = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + " to_char(p.fecha, 'dd/mm/yyyy')as fecha,\n"
                + " d.rut ||'/' || d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as doctor,\n"
                + " case when c.estatus= 1 then 'Cita Asignada' else case when c.estatus = 0 then 'Cita Cancelada' else case when c.estatus = 2 then 'Cita Recepcionada' \n"
                + "  else case when c.estatus = 3 then 'Cita Cancelada Temporalmente' else case when c.estatus = 4 then 'Cita Atendida Satisfractoriamente' else 'Cita en Proceso' end end end end end as estatus\n"
                + "\n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta  \n"
                + "   join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "   join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "   join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "   join agenda.motivoconsulta  mc on c.id_motivo = mc.id_motivo\n"
                + "   where \n"
                + " upper( c.rut_paciente) = upper('" + ruppaciente + "') and mc.id_motivo = " + motivo + " order by p.fecha desc\n"
                + "   limit 5;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales1(this.cnn.getRst().getString("fecha"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setTemporales(this.cnn.getRst().getString("estatus"));
                citas.add(c);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return citas;

    }

    public void ModificarRecetadeLentes(receta_lente_opticos rlo) {

        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.receta_lente_opticos  \n"
                + "SET \n"
                + "\n"
                + "  ojoderechoesfera = '" + rlo.getOjoderechoesfera() + "',\n"
                + "  ojoderechocilindro = '" + rlo.getOjoderechocilindro() + "',\n"
                + "  ojoderechoeje = '" + rlo.getOjoderechoeje() + "',\n"
                + "  ojoizquierdoesfera = '" + rlo.getOjoizquierdoesfera() + "',\n"
                + "  ojoiaquierdacilindro = '" + rlo.getOjoiaquierdacilindro() + "',\n"
                + "  ojoizquierdaeje = '" + rlo.getOjoizquierdaeje() + "',\n"
                + "  aadd = '" + rlo.getAadd() + "',\n"
                + "  distanciapupilarlejos = '" + rlo.getDistanciapupilarlejos() + "',\n"
                + "  distanciapupilarcerca = '" + rlo.getDistanciapupilarcerca() + "',\n"
                + "  observaciones = '" + rlo.getObservaciones() + "'\n"
                + " \n"
                + "WHERE id_receta = " + rlo.getId_receta() + "\n"
                + ";");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    public void ModificarViciodeRefraccion(vicio_refraccion vf) {

        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.viciorefraccion  \n"
                + "SET \n"
                + "  amhta = " + vf.getAmhta() + ",\n"
                + "  amdm = " + vf.getAmdm() + ",\n"
                + "  anosam = '" + vf.getAnosam() + "',\n"
                + "  amglaucoma = " + vf.getAmglaucoma() + ",\n"
                + "  amotro = '" + vf.getAmotro() + "',\n"
                + "  piod = '" + vf.getPiod() + "',\n"
                + "  pioi = '" + vf.getPioi() + "',\n"
                + "  plscod = '" + vf.getPlscod() + "',\n"
                + "  plscoi = '" + vf.getPlscoi() + "',\n"
                + "  plccod = '" + vf.getPlccod() + "',\n"
                + "  plccoi = '" + vf.getPlccoi() + "',\n"
                + "  plccodi = '" + vf.getPlccadi() + "',\n"
                + "  reflejofotomotor = '" + vf.getReflejofotomotor() + "',\n"
                + "  rojopopular = '" + vf.getRojopopular() + "',\n"
                + "  bmcpoloposterior = '" + vf.getBmcpoloposterior() + "',\n"
                + "  indicaciones = '" + vf.getIndicaciones() + "',\n"
                + " motivoconsulta = '" + vf.getMotivoconsulta() + "', \n"
                + "  ges = " + vf.getGes() + "\n"
                + " \n"
                + "WHERE  id_viciorefraccion = " + vf.getId_viciorefraccion() + "\n"
                + ";");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    public void ModificarregistroPatologia(registrodepatologia rp) {

        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.registropatalogia  \n"
                + "SET \n"
                + " \n"
                + "  vicio_refraccion = " + rp.getVicio_refraccion() + ",\n"
                + "  diagnostico = '" + rp.getDiagnostico() + "',\n"
                + "  fundamentodeldiagnostico = '" + rp.getFundamentodeldiagnostico() + "',\n"
                + "  tratamientoeindicacion = '" + rp.getTratamientoeindicacion() + "',\n"
                + "  confima = " + rp.getConfima() + ",\n"
                + "  tratamientocerca = " + rp.getTratamientocerca() + ",\n"
                + "  tratamientolejos = " + rp.getTratamientolejos() + "\n"
                + "WHERE  id_registropatologia = " + rp.getId_registropatologia() + "\n"
                + ";");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*buscar vicio_refraccion*/
    public vicio_refraccion buscarVicioRefraccionporIdAtencion(int idatenciontecnologo) {
        vicio_refraccion vr = new vicio_refraccion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion,\n"
                + "  id_viciorefraccion,\n"
                + "  amhta,\n"
                + "  amdm,\n"
                + "  anosam,\n"
                + "  amglaucoma,\n"
                + "  amotro,\n"
                + "  piod,\n"
                + "  pioi,\n"
                + "  plscod,\n"
                + "  plscoi,\n"
                + "  plccod,\n"
                + "  plccoi,\n"
                + "  plccodi,\n"
                + "  reflejofotomotor,\n"
                + "  rojopopular,\n"
                + "  bmcpoloposterior,\n"
                + "  indicaciones,\n"
                + "  motivoconsulta,"
                + " ges \n"
                + "FROM \n"
                + "  agenda.viciorefraccion where  id_atencion = " + idatenciontecnologo + " ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                vr.setAmdm(this.cnn.getRst().getInt("amdm"));
                vr.setAmglaucoma(this.cnn.getRst().getInt("amglaucoma"));
                vr.setAmhta(this.cnn.getRst().getInt("amhta"));
                vr.setAmotro(this.cnn.getRst().getString("amotro"));
                vr.setAnosam(this.cnn.getRst().getString("anosam"));
                vr.setBmcpoloposterior(this.cnn.getRst().getString("bmcpoloposterior"));
                vr.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                vr.setId_viciorefraccion(this.cnn.getRst().getInt("id_viciorefraccion"));
                vr.setIndicaciones(this.cnn.getRst().getString("indicaciones"));
                vr.setMotivoconsulta(this.cnn.getRst().getString("motivoconsulta"));
                vr.setPiod(this.cnn.getRst().getString("piod"));
                vr.setPioi(this.cnn.getRst().getString("pioi"));
                vr.setPlccadi(this.cnn.getRst().getString("plccodi"));
                vr.setPlccod(this.cnn.getRst().getString("plccod"));
                vr.setPlccoi(this.cnn.getRst().getString("plccoi"));
                vr.setPlscod(this.cnn.getRst().getString("plscod"));
                vr.setPlscoi(this.cnn.getRst().getString("plscoi"));
                vr.setReflejofotomotor(this.cnn.getRst().getString("reflejofotomotor"));
                vr.setRojopopular(this.cnn.getRst().getString("rojopopular"));
                vr.setMotivoconsulta(this.cnn.getRst().getString("motivoconsulta"));
                vr.setGes(this.cnn.getRst().getInt("ges"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return vr;
    }

    /*buscar la atencion tecnico por id de atencion ; asi mismo debo buscar la receta , viciorefraccion, registro de patalogia*/
    public atencion_clinica_tecnologo buscarAtencionTecnologoporId(int idatenciontecnologo) {
        atencion_clinica_tecnologo act = new atencion_clinica_tecnologo();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion_tecnologo,\n"
                + "  id_cita,\n"
                + "  fecha_registro,\n"
                + "  tecnolog , COALESCE (pertinencia, -1) as pertinencia "
                + "FROM \n"
                + "  agenda.atencion_clinica_tecnologo where   id_atencion_tecnologo = " + idatenciontecnologo + ";");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                act.setId_atencion_tecnologo(this.cnn.getRst().getInt("id_atencion_tecnologo"));
                act.setId_cita(this.cnn.getRst().getInt("id_cita"));

                act.setTecnologo(this.cnn.getRst().getString("tecnolog"));
                act.setFecha_registro(this.cnn.getRst().getDate("fecha_registro"));
                act.setPertinencia(this.cnn.getRst().getInt("pertinencia"));
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return act;

    }

    /*para el finalizar */
 /*buscar id de registro de parches, fondo de ojos, receta de lentes */
    public Integer buscarsiAtenciontieneParchesdeOjo(int atenciontecnologo) {
        int idparchedeojo = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT id_tratamientoparche\n"
                + "FROM \n"
                + "  agenda.tratamiento_parches\n"
                + "   where id_atencion_tecnologo = " + atenciontecnologo + "  ;");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                idparchedeojo = this.cnn.getRst().getInt("id_tratamientoparche");
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return idparchedeojo;
    }

    /*modificar parches de ojo*/
    public void modificarParchedeOjo(int id, int tratamiento, String observacion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.tratamiento_parches  \n"
                + "SET \n"
                + "  tratamiento = " + tratamiento + ",\n"
                + "  observacion = '" + observacion + "'\n"
                + " \n"
                + "WHERE \n"
                + " id_tratamientoparche = " + id + "");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /**/
    public Integer buscarsiVicioRefraccion(int atenciontecnologo) {
        int id = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_viciorefraccion\n"
                + "FROM \n"
                + "  agenda.viciorefraccion where id_atencion= " + atenciontecnologo + ";");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                id = this.cnn.getRst().getInt("id_viciorefraccion");
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return id;
    }

    public Integer buscarRecetadeLentes(int idatencion, int tipo) {
        int id = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_receta\n"
                + "FROM \n"
                + "  agenda.receta_lente_opticos where id_atencion= " + idatencion + " and tipo =" + tipo + " ;");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                id = this.cnn.getRst().getInt("id_receta");
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return id;
    }

    public int buscarregistroPatologia(int idatencion) {
        int id = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_registropatologia\n"
                + " \n"
                + "FROM \n"
                + "  agenda.registropatalogia where  id_atencion = " + idatencion + " ;");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                id = this.cnn.getRst().getInt("id_registropatologia");
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return id;
    }

    public boolean buscarsiCitayaTieneAtencionTecnologo(int idcita) {
        boolean tengo = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  *    \n"
                + "  FROM  \n"
                + "  agenda.atencion_clinica_tecnologo where id_cita = " + idcita + " ;");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                tengo = true;
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return tengo;
    }

    public Integer buscarIdatencionTensporidCita(int idcita) {
        int idatencionTens = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion_oftalmologia\n"
                + "FROM \n"
                + "  agenda.atencion_clinica_oftalmologia where id_cita = " + idcita + " ;");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                idatencionTens = this.cnn.getRst().getInt("id_atencion_oftalmologia");
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return idatencionTens;

    }

    /*busqueda receta de lentes tecnologo*/
    public atencion BuscarRecetadelenteparaTecnologo(int llego, int tipo) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        atencion a = new atencion();
        String query = "";

        if (tipo == 0) {
            query = " SELECT   "
                    + "  'RLO-'|| to_char( aco.fecha_registro, 'yyyymmdd')||'--OFT-'||  case when char_length(rlo.id_receta)>4 then cast(rlo.id_receta as text) else to_char(rlo.id_receta, '0000') end  as folio,  "
                    + "  to_char(aco.fecha_registro,'dd-mm-yyyy HH24:MI:SS') as fecha ,  "
                    + "    p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,   "
                    + "    p.rut, to_char( p.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,   "
                    + "       to_char(age(p.fecha_nacimiento),'yy') as edad,  "
                    + "      case when p.genero =1 then 'Femenino'else 'Masculino' end as sexo,    d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre,  d.rut as rutdoctor,  "
                    + "    id_receta,   "
                    + "    aco.id_atencion_tecnologo,  "
                    + "     "
                    + "   case when  ojoderechoesfera != '' then ojoderechoesfera ||' ESF = ' ELSE '' end || "
                    + "   case when  ojoderechocilindro != '' then  ojoderechocilindro || ' CIL ' ELSE '' end ||  "
                    + "   case when  ojoderechoeje != '' then ' X '|| ojoderechoeje ||' ° ' ELSE '' end  as  ojoderecho,  "
                    + "     case when  ojoizquierdoesfera != '' then ojoizquierdoesfera ||' ESF = '  ELSE '' end ||   "
                    + "     case when  ojoiaquierdacilindro != '' then  ojoiaquierdacilindro ||' CIL ' ELSE '' end ||   "
                    + "    case when  ojoizquierdaeje != '' then ' X '|| ojoizquierdaeje ||' ° '   ELSE '' end as ojoizquierdo,  "
                    + "     case when  aadd != '' then aadd ELSE '' end as losadd, "
                    + "      case when  distanciapupilarlejos != '' then distanciapupilarlejos ELSE '' end as distancialejos  , "
                    + "     case when  distanciapupilarcerca != '' then distanciapupilarcerca ELSE '' end as distaciacerca,  "
                    + "   COALESCE(observaciones,' ') as observaciones   "
                    + "    FROM    "
                    + "     agenda.receta_lente_opticos rlo    "
                    + "     inner join agenda.atencion_clinica_tecnologo aco on rlo.id_atencion = aco.id_atencion_tecnologo "
                    + "     join agenda.cita c on c.id_cita = aco.id_cita   "
                    + "      join agenda.paciente p on p.rut = c.rut_paciente   "
                    + "     join agenda.doctor d on aco.tecnolog = d.rut   "
                    + "     where rlo.id_atencion =  " + llego + "";
        } else {
            query = "SELECT   \n"
                    + "                      'RLO-'|| to_char( aco.fecharegistro, 'yyyymmdd')||'--OFT-'||  case when char_length(rlo.id_receta)>4 then cast(rlo.id_receta as text) else to_char(rlo.id_receta, '0000') end  as folio,  \n"
                    + "                     to_char(aco.fecharegistro,'dd-mm-yyyy HH24:MI:SS') as fecha , \n"
                    + "                       p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,   \n"
                    + "                        p.rut, to_char( p.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento,   \n"
                    + "                          to_char(age(p.fecha_nacimiento),'yy') as edad,  \n"
                    + "                          case when p.genero =1 then 'Femenino'else 'Masculino' end as sexo,    d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre,  d.rut as rutdoctor,  \n"
                    + "                      id_receta,  \n"
                    + "                       aco.idatencionoftalmologia,  \n"
                    + "                  \n"
                    + "                      case when  ojoderechoesfera != '' then ojoderechoesfera ||' ESF = ' ELSE '' end || \n"
                    + "                     case when  ojoderechocilindro != '' then  ojoderechocilindro || ' CIL ' ELSE '' end ||  \n"
                    + "                       case when  ojoderechoeje != '' then ' X '|| ojoderechoeje ||' ° ' ELSE '' end  as  ojoderecho, \n"
                    + "                         case when  ojoizquierdoesfera != '' then ojoizquierdoesfera ||' ESF = '  ELSE '' end ||  \n"
                    + "                         case when  ojoiaquierdacilindro != '' then  ojoiaquierdacilindro ||' CIL ' ELSE '' end ||   \n"
                    + "                       case when  ojoizquierdaeje != '' then ' X '|| ojoizquierdaeje ||' ° '   ELSE '' end as ojoizquierdo,  \n"
                    + "                         case when  aadd != '' then aadd ELSE '' end as losadd, \n"
                    + "                         case when  distanciapupilarlejos != '' then distanciapupilarlejos ELSE '' end as distancialejos  , \n"
                    + "                       case when  distanciapupilarcerca != '' then distanciapupilarcerca ELSE '' end as distaciacerca, \n"
                    + "                                           COALESCE(observaciones,' ') as observaciones   \n"
                    + "                       FROM    \n"
                    + "                        agenda.receta_lente_opticos rlo    \n"
                    + "                        inner join agenda.atencionoftalmologia aco on rlo.id_atencion = aco.idatencionoftalmologia\n"
                    + "                      join agenda.cita c on c.id_cita = aco.idcita   \n"
                    + "                          join agenda.paciente p on p.rut = c.rut_paciente   \n"
                    + "                        join agenda.doctor d on aco.usuario = d.rut   \n"
                    + "                        where rlo.id_atencion =  " + llego + "";
        }
        this.cnn.setSentenciaSQL(query);
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                a.setFolio(this.cnn.getRst().getString("folio"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setSexo(this.cnn.getRst().getString("sexo"));
                a.setDetallederivacion(this.cnn.getRst().getString("ojoderecho"));
                a.setDetallereceta(this.cnn.getRst().getString("ojoizquierdo"));
                a.setDomicilio(this.cnn.getRst().getString("losadd"));
                a.setCorreo_electronico(this.cnn.getRst().getString("distaciacerca"));
                a.setCasoges(this.cnn.getRst().getString("distancialejos"));
                a.setOtrasindicaciones(this.cnn.getRst().getString("observaciones"));
                a.setMedico_solicitante(this.cnn.getRst().getString("doctornombre"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return a;

    }

    /*IPD TECNOLOGO*/
    public atencion BuscarIPDTecnologo(int llego) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        atencion a = new atencion();
        this.cnn.setSentenciaSQL("SELECT 'IPD-'|| to_char( act.fecha_registro, 'yyyymmdd')||'--OFT-'|| case when char_length(act.id_atencion_tecnologo)>4 then cast(act.id_atencion_tecnologo as text) else to_char(act.id_atencion_tecnologo, '0000') end   as folio, "
                + "   to_char(act.fecha_registro,'dd-mm-yyyy HH24:MI:SS') as fecha , "
                + "   p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombre ,  "
                + "   p.rut, to_char( p.fecha_nacimiento, 'dd/mm/yyyy') as fechanacimiento, to_char(age(p.fecha_nacimiento),'yy') as edad,  "
                + "          case when p.genero =1 then 'Femenino'else 'Masculino' end as sexo,   "
                + "    case when  rp.confima = 1 then 'Si' else 'No' end as confirmavicio, "
                + "    case when rp.tratamientocerca = 1 then 'Tratamiento con Lentes Ópticos Cerca' else ' ' end  || "
                + "    case when rp.tratamientolejos = 1 then 'Tratamiento con Lentes Opticos Lejos ' else case when  "
                + "    rp.tratamientoeindicacion != '' then tratamientoeindicacion else '' end end  as   tratamiento,  "
                + "    case when rp.fundamentodeldiagnostico != '' then rp.fundamentodeldiagnostico else 'Examen Clínico' end as fundamentos,  "
                + "    d.nombre ||' '||  d.apellido_paterno ||' '|| d.apellido_materno as doctornombre,  "
                + "     d.rut as rutdoctor , d.profesion,  rp.diagnostico as diagnostico  "
                + "     FROM  "
                + "     agenda.atencion_clinica_tecnologo act  "
                + "     inner join agenda.cita c on c.id_cita = act.id_cita  "
                + "     join agenda.registropatalogia rp on rp.id_atencion = act.id_atencion_tecnologo  "
                + "      join agenda.paciente p on p.rut = c.rut_paciente  "
                + "      join agenda.doctor d on act.tecnolog = d.rut where act.id_atencion_tecnologo = " + llego + " ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                a.setFolio(this.cnn.getRst().getString("folio"));
                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre"));
                a.setRutpaciente(this.cnn.getRst().getString("rut"));
                a.setFechanacimiento(this.cnn.getRst().getString("fechanacimiento"));
                a.setEdadpaciente(this.cnn.getRst().getString("edad"));
                a.setSexo(this.cnn.getRst().getString("sexo"));
                a.setDetallederivacion(this.cnn.getRst().getString("confirmavicio"));
                a.setDetallereceta(this.cnn.getRst().getString("diagnostico"));
                a.setDomicilio(this.cnn.getRst().getString("tratamiento"));

                a.setMedico_solicitante(this.cnn.getRst().getString("doctornombre"));
                a.setRutdoctor(this.cnn.getRst().getString("rutdoctor"));
                a.setFundamentodeldiagnostico(this.cnn.getRst().getString("fundamentos"));
                a.setProfesion(this.cnn.getRst().getString("profesion"));
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return a;

    }

    /*todo para solicitud de examenes y procedimientos */
    public String InsertarSolicitudExamenes_Procedimiento(solicitudExamenes_Procedimientos sep) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.solicitudexamenes_procedimientooft\n"
                + "(\n"
                + "  idatencionoftalmologia,\n"
                + "  observacion ,\n"
                + "  estatus\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + sep.getIdatencionoftalmologia() + ",\n"
                + "  '" + sep.getObservacion() + "', 1 \n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    /*detalle*/
    public String InsertarDetalleSolicitudExamenes_Procedimiento(solicitudExamenes_Procedimientos sep) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.detallesolicitudexamen_procedimientooft\n"
                + "(\n"
                + "  idsolicitud,\n"
                + "  idsolicitado,\n"
                + "  tipo,\n"
                + "  estatus,idlateralidad, idtiempo\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + sep.getIdsolicitud() + ",\n"
                + "  " + sep.getLosolicitado() + ",\n"
                + "  " + sep.getTipo() + ",\n"
                + "  " + sep.getEstatus() + ","
                + "  " + sep.getIdlateralidad() + ","
                + "  " + sep.getIdtiempo() + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();

        return mensaje;
    }

    /*buscar solicitud de una atencion*/
    public int buscaridSolicitudExproporidAtencion(int idatencion) {

        int idsolicitud = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        atencion a = new atencion();
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idsolicitud\n"
                + "FROM \n"
                + "  agenda.solicitudexamenes_procedimientooft "
                + " where idatencionoftalmologia = " + idatencion + " ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                idsolicitud = this.cnn.getRst().getInt("idsolicitud");
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return idsolicitud;

    }

    /*Buscar solicitudes de examenes*/
    public solicitudExamenes_Procedimientos buscarexamenesProcedimientos(int atencion) {
        solicitudExamenes_Procedimientos s = new solicitudExamenes_Procedimientos();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        atencion a = new atencion();
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idsolicitud,\n"
                + "\n"
                + "  observacion,\n"
                + " to_char(ao.fecharegistro,'dd/mm/yyyy')as fecha,"
                + "  to_char(age(CURRENT_DATE, p.fecha_nacimiento),'yy')as edad,\n"
                + "  p.rut,\n"
                + "  p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombrep,\n"
                + "   doc.rut as rutdoc, \n"
                + "   doc.nombre||' '|| doc.apellido_paterno||' '|| doc.apellido_materno as doctor\n"
                + "FROM \n"
                + "  agenda.solicitudexamenes_procedimientooft sp inner join\n"
                + "  agenda.atencionoftalmologia ao on sp.idatencionoftalmologia = ao.idatencionoftalmologia\n"
                + "  join agenda.cita c on c.id_cita = ao.idcita\n"
                + "  join agenda.paciente p on p.rut = c.rut_paciente \n"
                + "  join agenda.doctor doc on doc.rut = ao.usuario where ao.idatencionoftalmologia=" + atencion + ";");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {

                s.setObservacion(this.cnn.getRst().getString("observacion"));
                s.setIdsolicitud(this.cnn.getRst().getInt("idsolicitud"));
                s.setFecha(this.cnn.getRst().getString("fecha"));
                s.setRutpaciente(this.cnn.getRst().getString("rut"));
                s.setNombrepaciente(this.cnn.getRst().getString("nombrep"));
                s.setRutprofesional(this.cnn.getRst().getString("rutdoc"));
                s.setNombreprofesional(this.cnn.getRst().getString("doctor"));
                s.setVariable1(this.cnn.getRst().getString("edad"));

            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return s;
    }

    public Vector<solicitudExamenes_Procedimientos> solicitudesdetalle(int solicitud, int tipo) {
        Vector<solicitudExamenes_Procedimientos> sep = new Vector<solicitudExamenes_Procedimientos>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        atencion a = new atencion();
        String query = "";
        if (tipo == 1) {
            query = "SELECT  idsolicitud,\n"
                    + "                      idsolicitado,\n"
                    + "                      tipo,\n"
                    + "                      case \n"
                    + "                      when dep.idlateralidad = 1 then 'ojo derecho'\n"
                    + "                      when dep.idlateralidad = 2 then 'ojo izquierdo'\n"
                    + "                       when dep.idlateralidad = 3 then 'ambos ojos '\n"
                    + "                       else ''\n"
                    + "                       end as lateralidad,\n"
                    + "                      COALESCE((select ', Realizar en' ||' '||descripcion from agenda.tiempo_solicitud where id_tiempo= dep.idtiempo  ),'') as tiempo,\n"
                    + "                     initcap(e.nombre_examenes) as nombre,  dep.idtiempo, dep.idlateralidad\n"
                    + "FROM \n"
                    + "  agenda.detallesolicitudexamen_procedimientooft dep\n"
                    + "  inner join agenda.examenes_especialidad ee  \n"
                    + "  on dep.idsolicitado = ee.idexamen_especialidad\n"
                    + "  join agenda.examenes e on e.id_examenes = ee.idexamenes\n"
                    + "\n"
                    + "  where tipo=1 and idsolicitud =" + solicitud + "";
        } else {
            query = "   SELECT  idsolicitud,\n"
                    + "                      idsolicitado,\n"
                    + "                      tipo,\n"
                    + "                      case \n"
                    + "                      when dep.idlateralidad = 1 then 'ojo derecho'\n"
                    + "                      when dep.idlateralidad = 2 then 'ojo izquierdo'\n"
                    + "                       when dep.idlateralidad = 3 then 'ambos ojos '\n"
                    + "                       else ''\n"
                    + "                       end as lateralidad,\n"
                    + "                      COALESCE((select ', Realizar en' ||' '|| descripcion from agenda.tiempo_solicitud where id_tiempo= dep.idtiempo  ),'') as tiempo,\n"
                    + "                     initcap(p.nombre_procedimiento) as nombre, dep.idtiempo, dep.idlateralidad\n"
                    + "   FROM \n"
                    + "  agenda.detallesolicitudexamen_procedimientooft dep\n"
                    + "  inner join agenda.procedimiento_especialidad pe\n"
                    + "  on  dep.idsolicitado = pe.idprocedimiento_especialidad\n"
                    + "  join agenda.procedimiento p on p.idprocedimiento = pe.idprocedimiento\n"
                    + "\n"
                    + "  where tipo=2 and idsolicitud =" + solicitud + " ";
        }

        this.cnn.setSentenciaSQL(query);

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                solicitudExamenes_Procedimientos s = new solicitudExamenes_Procedimientos();
                s.setVariable1(this.cnn.getRst().getString("nombre"));
                s.setVariable2(this.cnn.getRst().getString("lateralidad"));
                s.setVariable3(this.cnn.getRst().getString("tiempo"));
                s.setIdlateralidad(this.cnn.getRst().getInt("idlateralidad"));
                s.setIdtiempo(this.cnn.getRst().getInt("idtiempo"));

                s.setIdexamen(this.cnn.getRst().getInt("idsolicitado"));
                sep.add(s);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return sep;
    }

    /*Para seguimiento de las solicitudes de examenes */
    public Vector<solicitudExamenes_Procedimientos> buscarSolicitudesParaSeguimientos() {
        Vector<solicitudExamenes_Procedimientos> vs = new Vector<solicitudExamenes_Procedimientos>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  select \n"
                + "  sep.idatencionoftalmologia,\n"
                + "  doc.rut,\n"
                + "  doc.nombre ||' '|| doc.apellido_paterno as doctor,\n"
                + "  to_char(ao.fecharegistro, 'dd/mm/yyyy') as fecha,\n"
                + "  p.rut as rutpaciente,\n"
                + "  p.nombre ||' '|| p.apellido_paterno as pac ,  sep.idsolicitud\n"
                + "\n"
                + "   from agenda.solicitudexamenes_procedimientooft sep \n"
                + "  inner join agenda.atencionoftalmologia ao on sep.idatencionoftalmologia = ao.idatencionoftalmologia\n"
                + "  join agenda.doctor doc on ao.usuario = doc.rut\n"
                + "  join agenda.cita c on c.id_cita = ao.idcita\n"
                + "  join agenda.paciente p on c.rut_paciente = p.rut \n"
                + "  where sep.estatus = 1 order by ao.fecharegistro asc");

        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                solicitudExamenes_Procedimientos sep = new solicitudExamenes_Procedimientos();
                sep.setFecha(this.cnn.getRst().getString("fecha"));
                sep.setIdatencionoftalmologia(this.cnn.getRst().getInt("idatencionoftalmologia"));
                sep.setRutpaciente(this.cnn.getRst().getString("rutpaciente"));
                sep.setNombrepaciente(this.cnn.getRst().getString("pac"));
                sep.setRutprofesional(this.cnn.getRst().getString("rut"));
                sep.setNombreprofesional(this.cnn.getRst().getString("doctor"));
                sep.setIdsolicitud(this.cnn.getRst().getInt("idsolicitud"));
                vs.add(sep);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return vs;
    }

    /*para solicitud con tiempo*/
    public Vector<lugar> buscarTiempo() {
        Vector<lugar> tiempo = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_tiempo, descripcion\n"
                + "  FROM agenda.tiempo_solicitud;");

        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                lugar t = new lugar();
                t.setId_lugar(this.cnn.getRst().getInt("id_tiempo"));
                t.setNombre(this.cnn.getRst().getString("descripcion"));
                tiempo.add(t);
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return tiempo;
    }

    /*para modificacion de oftalmologia/
    
     */
 /*busco la atencioon*/
    public atencion buscaratencionporIdcita(int idcita) {
        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idatencionoftalmologia,\n"
                + "  idcita,\n"
                + "  fecharegistro,\n"
                + "  usuario,\n"
                + "  ipusuario,\n"
                + "  resumenatenicon,\n"
                + "  otrodiagnostico,\n"
                + "  otroexamenes,\n"
                + "  sic,\n"
                + "  iddestinopaciente,\n"
                + "  otrodestino,\n"
                + "  indicalente,\n"
                + "  cantidadlente,\n"
                + "  otrasindicaciones,\n"
                + "  solicitudpabellon,\n"
                + "  estatus,\n"
                + "  detallederivacion,\n"
                + "  diagnosticosic,\n"
                + "  otrodiagnosticosic,\n"
                + "  otroprocedimiento,\n"
                + "  pertinencia,\n"
                + "  tratamientocerca,\n"
                + "  tratamientolejos\n"
                + "FROM \n"
                + "  agenda.atencionoftalmologia where idcita = " + idcita + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {

                a.setIdatencion(this.cnn.getRst().getInt("idatencionoftalmologia"));
                a.setResumenatenicon(this.cnn.getRst().getString("resumenatenicon"));
                a.setOtrodiagnostico(this.cnn.getRst().getString("otrodiagnostico"));
                a.setOtroexamenes(this.cnn.getRst().getString("otroexamenes"));
                a.setSic(this.cnn.getRst().getInt("sic"));
                a.setIddestinopaciente(this.cnn.getRst().getInt("iddestinopaciente"));
                a.setOtrodestino(this.cnn.getRst().getString("otrodestino"));
                a.setIndicalente(this.cnn.getRst().getInt("indicalente"));
                a.setCantidadlente(this.cnn.getRst().getInt("cantidadlente"));
                a.setOtrasindicaciones(this.cnn.getRst().getString("otrasindicaciones"));
                a.setSolicitudpabellon(this.cnn.getRst().getBoolean("solicitudpabellon"));
                a.setDetallederivacion(this.cnn.getRst().getString("detallederivacion"));
                a.setDiagnosticosic(this.cnn.getRst().getInt("diagnosticosic"));
                a.setOtrodiagnosticosic(this.cnn.getRst().getString("otrodiagnosticosic"));
                a.setOtroprocedimiento(this.cnn.getRst().getString("otroprocedimiento"));
                a.setPertinencia(this.cnn.getRst().getInt("pertinencia"));
                a.setTratamientocerca(this.cnn.getRst().getInt("tratamientocerca"));
                a.setTratamientolejos(this.cnn.getRst().getInt("tratamientolejos"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }

    /*buscodiagnostiosatencionoftalmologia*/
    public Vector<atencion> buscarAtencionOftalmologia(int idatencion) {
        Vector<atencion> va = new Vector<atencion>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idatencionoftalmologia,\n"
                + "  iddiagnostico,\n"
                + "  idlateralidad,\n"
                + "  casoges,\n"
                + "  confirmadescarta,\n"
                + "  tratamiento,\n"
                + "  fundamento,\n"
                + "  confirmadescartadiagnostico\n"
                + "FROM \n"
                + "  agenda.diagnostoco_atencion_oftalmologia where idatencionoftalmologia = " + idatencion + " ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setDiagnostico(this.cnn.getRst().getInt("iddiagnostico"));
                a.setLateralidad(this.cnn.getRst().getInt("idlateralidad"));
                a.setGes(this.cnn.getRst().getInt("casoges"));
                a.setConfirmadescartadiagnostico(this.cnn.getRst().getInt("confirmadescartadiagnostico"));
                a.setConfirmadescartadocumentos(this.cnn.getRst().getInt("confirmadescarta"));
                a.setTratamientoeindicaciones(this.cnn.getRst().getString("tratamiento"));
                a.setFundamentodeldiagnostico(this.cnn.getRst().getString("fundamento"));

                va.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return va;
    }

    /*buscar procedimientos de oftalmologia */
    public Vector<atencion> buscarProcedimientosAtencionOftalmologia(int idatencion) {
        Vector<atencion> vp = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idhojadiaria,\n"
                + "  idprocedimiento_especialidad,\n"
                + "  tipoatencion\n"
                + "FROM \n"
                + "  agenda.procedimiento_hojadiaria where idhojadiaria = " + idatencion + " and tipoatencion = 2  ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setIddiagnostico(this.cnn.getRst().getInt("idprocedimiento_especialidad"));
                vp.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    /*buscar procedimientos de oftalmologia */
    public Vector<atencion> buscarExamenesAtencionOftalmologia(int idatencion) {
        Vector<atencion> vp = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idhojadiaria,\n"
                + "  idexamenes_especialidades,\n"
                + "  tipoatencion\n"
                + "FROM \n"
                + "  agenda.hojadiaria_examenes where tipoatencion=2 and idhojadiaria = " + idatencion + " ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setIddiagnostico(this.cnn.getRst().getInt("idexamenes_especialidades"));
                vp.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    public Vector<atencion> buscarindicacionesAtencionOftalmologia(int idatencion) {
        Vector<atencion> vp = new Vector<atencion>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idhojadiaria,\n"
                + "  idindicaciones_especialidad,\n"
                + "  cantidad,\n"
                + "  tipoatencion,\n"
                + "  descripcionindicacion\n"
                + "FROM \n"
                + "  agenda.idicaciones_hojadiaria where tipoatencion = 2 and  idhojadiaria =" + idatencion + ";");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setIddiagnostico(this.cnn.getRst().getInt("idindicaciones_especialidad"));
                a.setOtrodiagnostico(this.cnn.getRst().getString("descripcionindicacion"));
                vp.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    /*buscar las solicitudes de pabellon*/
    public Vector<lugar> buscarSolicitudes(int atencion) {
        Vector<lugar> vp = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idhojadiaria,\n"
                + "  iddiagnosticoespecialidad,\n"
                + "  intervencion,\n"
                + "  idanastesia,\n"
                + "  requerimiento,\n"
                + "  observacion,\n"
                + "  otrodiagnosticosolicitud,\n"
                + "  tipoatencion,\n"
                + "  lateralidad\n"
                + "FROM \n"
                + "  agenda.solicitudquirofanohojadiaria where tipoatencion = 2 and idhojadiaria=  " + atencion + "");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar a = new lugar();
                a.setId_diagnostico_urgencia(this.cnn.getRst().getInt("iddiagnosticoespecialidad"));
                a.setId_lugar(this.cnn.getRst().getInt("idanastesia"));
                a.setIntervencio(this.cnn.getRst().getString("intervencion"));
                a.setVariable1(this.cnn.getRst().getString("requerimiento"));
                a.setVariable2(this.cnn.getRst().getString("observacion"));
                a.setVariable3(this.cnn.getRst().getString("otrodiagnosticosolicitud"));
                a.setId_centro(this.cnn.getRst().getInt("lateralidad"));
                vp.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    public Vector<lugar> buscarNoindicaciones(int idatencion) {
        Vector<lugar> vp = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idatencionoftalmologica,\n"
                + "  iddiagnosticoespecialidad,\n"
                + "  lateralidad,\n"
                + "  otrodiagnostico,\n"
                + "  intervencion,\n"
                + "  razones,\n"
                + "  indicaciones\n"
                + "FROM \n"
                + "  agenda.noindicacionescirugia where idatencionoftalmologica=" + idatencion + ";");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar a = new lugar();
                a.setId_diagnostico_urgencia(this.cnn.getRst().getInt("iddiagnosticoespecialidad"));
                a.setVariable4(this.cnn.getRst().getString("razones"));
                a.setId_centro(this.cnn.getRst().getInt("lateralidad"));
                a.setIntervencio(this.cnn.getRst().getString("intervencion"));
                a.setVariable2(this.cnn.getRst().getString("indicaciones"));
                a.setVariable3(this.cnn.getRst().getString("otrodiagnostico"));

                vp.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    /*excepcion de garantia*/
    public Vector<excepcion_garantia> buscarexcepciondegarantia(int atencion) {
        Vector<excepcion_garantia> eg = new Vector<excepcion_garantia>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idexceptiondegarantia,\n"
                + "  id_atencion,\n"
                + "  rechazo_prestador_designado,\n"
                + "  rechazo_atencion_garantizado,\n"
                + "  prestacion_rechazada,\n"
                + "  otra_causa,\n"
                + "  observaciones,\n"
                + "  tipo,\n"
                + "  problemaauge\n"
                + "FROM \n"
                + "  agenda.exception_garantia where id_atencion = " + atencion + " and tipo =2 ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                excepcion_garantia e = new excepcion_garantia();
                e.setRechazo_atencion_garantizado(this.cnn.getRst().getInt("rechazo_atencion_garantizado"));
                e.setRechazo_prestador_designado(this.cnn.getRst().getInt("rechazo_prestador_designado"));
                e.setPrestacion_rechazada(this.cnn.getRst().getInt("prestacion_rechazada"));
                e.setOtra_causa(this.cnn.getRst().getInt("otra_causa"));
                e.setObservaciones(this.cnn.getRst().getString("observaciones"));
                e.setProblemaauge(this.cnn.getRst().getString("problemaauge"));

                eg.add(e);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return eg;
    }

    public lugar BuscarSolicituddeControl(int idcita) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idcontrol,\n"
                + "  idcita,\n"
                + "  fecharegistro,\n"
                + "  usuario_registra,\n"
                + "  to_char(fecha_control, 'dd/mm/yyyy') as fecha_control,\n"
                + "  motivo_control\n"
                + "FROM \n"
                + "  agenda.solicitud_control where idcita = " + idcita + ";");

        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                l.setFechanacimiento(this.cnn.getRst().getString("fecha_control"));
                l.setDescripcion(this.cnn.getRst().getString("motivo_control"));
            
                l.setEstatu(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return l;
    }

}
