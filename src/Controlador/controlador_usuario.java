/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.funcionario;
import Modelos.rol;
import Modelos.usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Informatica
 */
public class controlador_usuario extends General {

    /*ver si el funcionario ya se encuentra registrado*/
    public boolean buscarsiyaFuncionarioestaregistrado(String rut) {
        boolean versiexiste = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  *\n"
                + "FROM \n"
                + "  seguridad.registrousuario where rut = '" + rut + "' ;");
        this.cnn.conectar();
        usuario u = new usuario();
        try {
            if (this.cnn.getRst().next()) {
                versiexiste = true;
            }
        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return versiexiste;
    }

    //traer usuario con id
    public usuario buscarusuario(int id) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM seguridad.usuario where id_usuario=" + id + ";  ");
        this.cnn.conectar();
        usuario u = new usuario();
        try {
            if (this.cnn.getRst().next()) {
                u.setId_usuairo(this.cnn.getRst().getInt("id_usuario"));
                u.setNombre_usuario(this.cnn.getRst().getString("nombre_usuario"));
                u.setId_funcionario(this.cnn.getRst().getString("id_funcionario"));
                u.setEmail(this.cnn.getRst().getString("email"));

            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return u;
    }
    // actualizar usuario solo el email

    public String modificarUsuario(int id, String email) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL(" UPDATE seguridad.usuario  SET  email = '" + email + "' WHERE  id_usuario = " + id + ";");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Usuario Modificado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    //guardarfotodeusuario
    public String registrarfoto(int id, String nombrefoto, String dirArchivo)  {
         String actualizo = "";
          this.configurarConexion("");
        Connection cn = null;
        PreparedStatement pr = null;
        String sql = "UPDATE  seguridad.usuario set  nombreimagen = ? ,  tamannofoto = ?,  fotousuario = ? WHERE   id_usuario = ? ";
        try {
            Class.forName(this.cnn.getDriver());
            cn = DriverManager.getConnection(this.cnn.getNombreBaseDatos(), this.cnn.getUser(), this.cnn.getPassword());
            pr = cn.prepareStatement(sql);
            pr.setString(1, nombrefoto);

            //Parametros de la imagen
            File fichero = new File(dirArchivo);
            FileInputStream streamEntrada = new FileInputStream(fichero);
            int tamañoImagen = streamEntrada.available();
            //Establecer los parametros a la BD
            pr.setInt(2, tamañoImagen);
            pr.setBinaryStream(3, streamEntrada, (int) fichero.length());
            pr.setInt(4, id);
            if (pr.executeUpdate() == 1) {
                actualizo = "Usuario Actualizado Exitosamente!!";
            } else {
                actualizo = "Ocurrio un problema ! intente mas tarde";
            }
        } catch (Exception ex) {
            actualizo = ex.getMessage();
        } finally {
            try {
                pr.close();
                cn.close();
               
            } catch (Exception ex) {

            }
        }
        return actualizo;
    }

    // buscar usuario de funcionario o doctro por rut
    public usuario buscarPorRut(String rut) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM seguridad.usuario where upper(id_funcionario)=upper('" + rut + "');  ");
        this.cnn.conectar();
        usuario u = new usuario();
        try {
            if (this.cnn.getRst().next()) {
                u.setId_usuairo(this.cnn.getRst().getInt("id_usuario"));
                u.setNombre_usuario(this.cnn.getRst().getString("nombre_usuario"));
                u.setId_funcionario(this.cnn.getRst().getString("id_funcionario"));
                u.setEmail(this.cnn.getRst().getString("email"));

            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return u;
    }

    // eliminar roles de usuario
    public void eliminarRolesdeUsuario(int id) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL(" DELETE FROM seguridad.usuario_rol WHERE id_usuario = " + id + "");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

//validar si el usuario existe
    public boolean buscarusuario(String nombreusuario) {
        boolean siexiste = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM seguridad.usuario where nombre_usuario='" + nombreusuario + "';  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                siexiste = true;
            } else {
                siexiste = false;
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return siexiste;
    }

//eliminar 
    public String eliminarUsuario(int id) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE    seguridad.usuario  SET estatus = 0 WHERE id_usuario=" + id + " ; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Usuario Eliminado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

//ingresar al usuario
    public String ingresa_usuario(usuario usu) throws UnknownHostException {

        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";
        this.cnn.setSentenciaSQL("Insert into seguridad.usuario ( id_usuario,id_funcionario, nombre_usuario, contrasena,\n"
                + "  viejacontrasena, fecharegistro, email, estatus,\n"
                + "  foto, ipusuario, usuario)   "
                + "   VALUES ( "
                + "  " + usu.getId_usuairo() + ",'" + usu.getId_funcionario() + "','" + usu.getNombre_usuario() + "',"
                + " '" + usu.getContrasena() + "', '" + usu.getViejacontrasena() + "',  current_date ,"
                + "  '" + usu.getEmail() + "', " + usu.getEstatus() + ",0 ,'" + getIpUsuarioLoggin() + "', '" + getUsuarioLoggin() + "' ); ");

        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Usuario Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        //envio correo

        String cuerpo2 = "<html>\n"
                + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/banner.JPG\" >  \n"
                + "  <h1 style=\"color: #0059b3\"> Bienvenidos a los sistemas del CRS </h1>\n"
                + "  <p> Su Usuario es :" + usu.getNombre_usuario() + "</p>\n"
                + "  <p> Su contraseña es :" + usu.getContrasena() + "</p>\n"
                + "  <p>Favor ingresar al siguiente link:<a href=\"http://10.8.4.11:9090/Especialidades/\">Ingresar </a> </p>\n"
                + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/footer.JPG\" >  \n"
                + " </html>";
        String email = usu.getEmail();
        String asunto = "Creacion de Usuario en el sistema CRS";
        boolean valor = enviarEmailNotificacion(asunto, email, cuerpo2);
        if (valor == true) {
            System.out.println("se envio");
        } else {
            System.out.println("no se envio");
        }
        return mensaje;
    }

    //para buscar el ultimo usuairo
    public int buscarUsuario2() {
        int idusuario = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  max(id_usuario) as usuario FROM seguridad.usuario;  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                idusuario = this.cnn.getRst().getInt("usuario");
            } else {
                idusuario = 0;
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();

        return idusuario;
    }

    public String getCadenaAlfanumAleatoria(int longitud) {
        String cadenaAleatoria = "";
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while (i < longitud) {
            char c = (char) r.nextInt(255);
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
                cadenaAleatoria += c;
                i++;
            }
        }
        return cadenaAleatoria;
    }
    //registrar usuario_rol

    public void registrarUsuario_Rol(int idusuario, String[] roles) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        for (int i = 0; i < roles.length; ++i) {
            this.cnn.setSentenciaSQL("INSERT INTO seguridad.usuario_rol VALUES ("
                    + " " + idusuario + ", " + roles[i] + ");");
            this.cnn.conectar();
        }

        this.cnn.cerrarConexion();
    }

    // buscar todo los usuario
    public Vector<usuario> buscarUsuarios() {
        Vector<usuario> lista = new Vector();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT * FROM  seguridad.usuario u  inner join seguridad.usuario_rol ur\n"
                + " on u.id_usuario =  ur.id_usuario join seguridad.rol r on r.idrol = ur.id_rol \n"
                + " where u.estatus = 1 and r.id_sistema = 3 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                usuario u = new usuario();
                u.setId_usuairo(this.cnn.getRst().getInt("id_usuario"));
                u.setEmail(this.cnn.getRst().getString("email"));
                u.setNombre_usuario(this.cnn.getRst().getString("nombre_usuario"));
                u.setFecha_registro(this.cnn.getRst().getDate("fecharegistro"));
                lista.add(u);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

        return lista;

    }

    // buscar perfiles de usuario
    public ArrayList buscarListaRolsinUsuario(int id_usuario) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select * from seguridad.rol where idrol not in (SELECT id_rol FROM seguridad.usuario_rol where id_usuario=" + id_usuario + ") and estatus = 1 AND id_sistema = 3 ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                rol r = new rol();
                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                r.setNombreRol(this.cnn.getRst().getString("nombrerol"));

                lista.add(r);

            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return lista;

    }

    // buscar perfiles del usuario
    public ArrayList buscarListaRolconUsuario(int id_usuario) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT * FROM "
                + "seguridad.rol r join seguridad.usuario_rol ur on r.idrol = ur.id_rol "
                + "where   ur.id_usuario=" + id_usuario + " and r.estatus = 1 AND R.id_sistema = 3 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                rol r = new rol();
                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                r.setNombreRol(this.cnn.getRst().getString("nombrerol"));

                lista.add(r);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return lista;

    }

    //lista de roles
    public ArrayList buscarListaRol() {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from seguridad.rol where estatus = 1 and id_sistema = 3;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                rol r = new rol();
                r.setIdRol(this.cnn.getRst().getInt("idrol"));
                r.setNombreRol(this.cnn.getRst().getString("nombrerol"));

                lista.add(r);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return lista;

    }

    // obtener la foto del usuario
    public byte[] obtenImagenUsuario(String id_usuario) throws SQLException {

        byte[] buffer = null;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM seguridad.usuario where nombre_usuario='" + id_usuario + "';");
        
        try {
           this.cnn.conectar();
            while (this.cnn.getRst().next()) {

               
                InputStream inStream = cnn.getRst().getBinaryStream("fotousuario");
                int size = cnn.getRst().getInt("tamannofoto");
                buffer = new byte[size];
                int length = -1;
                int k = 0;
                try {
                    inStream.read(buffer, 0, size);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                //}
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_usuario.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return buffer;
    }

    /*area de funcionario*/
    public Vector<funcionario> BuscarFuncionaroconDoctorporEspecialidad(int es) {

        Vector<funcionario> f = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT d.rut, (apellido_paterno||' '|| apellido_materno||' '|| nombre) as nombre  \n"
                + "     FROM \n"
                + "    agenda.doctor d inner join agenda.doctor_especialidad de on \n"
                + "   d.rut = de.rut where id_especialidad = " + es + " and d.estatus = 1 order by nombre ; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                funcionario fun = new funcionario();
                fun.setRut(this.cnn.getRst().getString("rut"));
                fun.setNombre(this.cnn.getRst().getString("nombre"));

                f.add(fun);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return f;

    }

    /*buscar usuario para crear acceso al sistema*/
    public Vector<funcionario> BuscarUsuariossinAccesoparaCrearAcceso() {

        Vector<funcionario> f = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select rut as rut_funcionario ,\n"
                + "               (rut ||' / '  ||' ' ||nombre ||' '|| apellido_paterno ||' '||apellido_materno )\n"
                + "               as nombre from agenda.doctor where estatus = 1 and\n"
                + "               rut not in (select id_funcionario from seguridad.usuario u inner join seguridad.usuario_rol ur\n"
                + "               on u.id_usuario = ur.id_usuario \n"
                + "               join seguridad.rol r on r.idrol = ur.id_rol where r.id_sistema = 3) \n"
                + "               union \n"
                + "               select rut as rut_funcionario, \n"
                + "              (rut ||' / '  ||' ' ||nombre ||' '|| apellidop ||' '||apellidom  )  as nombre\n"
                + "               from seguridad.registrousuario where estatus = 1 and rut not in (select id_funcionario from seguridad.usuario u inner join seguridad.usuario_rol ur\n"
                + "               on u.id_usuario = ur.id_usuario \n"
                + "              join seguridad.rol r on r.idrol = ur.id_rol where r.id_sistema = 3) \n"
                + "              order by rut_funcionario , nombre  DESC ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                funcionario fun = new funcionario();
                fun.setRut(this.cnn.getRst().getString("rut_funcionario"));
                fun.setNombre(this.cnn.getRst().getString("nombre"));

                f.add(fun);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();
        return f;

    }

    public String registrarFuncionarioparacrearUsuario(funcionario f) {

        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        String mensaje = "";

        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  seguridad.registrousuario\n"
                + "(\n"
                + "  rut,\n"
                + "  nombre,\n"
                + "  apellidop,\n"
                + "  apellidom,\n"
                + "  estatus,\n"
                + "  telefono\n"
                + ") \n"
                + "VALUES (\n"
                + "  '" + f.getRut() + "',\n"
                + "  '" + f.getNombre() + "',\n"
                + "  '" + f.getApellidopaterno() + "',\n"
                + "  '" + f.getApellidomaterno() + "',\n"
                + "  1,\n"
                + " '" + f.getTelefono() + "'\n"
                + ");");

        this.cnn.conectar();

        if (this.cnn.getResultadoSQL()
                == 1) {
            mensaje = "Funcionario Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }

        this.cnn.cerrarConexion();
        return mensaje;
    }

}
