/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.opciones;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class controlador_menu extends General {

    //*menu como tipo 1 */
    public Vector<opciones> buscarmenu1(String us) {
        Vector<opciones> op = new Vector<opciones>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario= '" + us + "' and o.tipo=1 AND r.id_sistema = 3  order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                opciones o = new opciones();
                o.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                o.setNombre(this.cnn.getRst().getString("nombreopciones"));
                o.setUrl(this.cnn.getRst().getString("url"));

                op.add(o);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return op;
    }

    public boolean versiTieneMenu1(String us) {
        boolean tiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario= '" + us + "' and o.tipo=1 AND r.id_sistema = 3  order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                tiene = true;

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return tiene;
    }

    /* menu con tipo 2*/
    public Vector<opciones> buscarmenu2(String us) {
        Vector<opciones> op = new Vector<opciones>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario= '" + us + "' and o.tipo=2 AND r.id_sistema = 3  order by  o.nombreopciones;");
        this.cnn.conectar();
        try {

            while (this.cnn.getRst().next()) {
                opciones o = new opciones();
                o.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                o.setNombre(this.cnn.getRst().getString("nombreopciones"));
                o.setUrl(this.cnn.getRst().getString("url"));

                op.add(o);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return op;
    }

    public boolean versiTieneMenu2(String us) {
        boolean tiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario= '" + us + "' and o.tipo=2 AND r.id_sistema = 3  order by  o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                tiene = true;

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return tiene;

    }

    /*ver si tiene */
    public boolean versitieneMenu3(String us) {
        boolean tiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario= '" + us + "' and o.tipo=3 AND r.id_sistema = 3 AND r.id_sistema = 3  order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                tiene = true;

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return tiene;
    }

    /* menu  3*/
    public Vector<opciones> buscarmenu3(String us) {
        Vector<opciones> op = new Vector<opciones>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario='" + us + "' and o.tipo=3 AND r.id_sistema = 3   order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                opciones o = new opciones();
                o.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                o.setNombre(this.cnn.getRst().getString("nombreopciones"));
                o.setUrl(this.cnn.getRst().getString("url"));

                op.add(o);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return op;
    }

    /* menu  4*/
    public Vector<opciones> buscarmenu4(String us) {
        Vector<opciones> op = new Vector<opciones>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario= '" + us + "' and o.tipo=4 AND r.id_sistema = 3  order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                opciones o = new opciones();
                o.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                o.setNombre(this.cnn.getRst().getString("nombreopciones"));
                o.setUrl(this.cnn.getRst().getString("url"));

                op.add(o);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return op;
    }

    /*buscar si tiene menu 4*/
    public boolean buscarsitienemenu4(String us) {
        boolean tiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario='" + us + "' and o.tipo=4 AND r.id_sistema = 3  order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                tiene = true;

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return tiene;
    }

    /* menu  5*/
    public Vector<opciones> buscarmenu5(String us) {
        Vector<opciones> op = new Vector<opciones>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario='" + us + "' and o.tipo=5 AND r.id_sistema = 3 order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                opciones o = new opciones();
                o.setIdopcion(this.cnn.getRst().getInt("idopciones"));
                o.setNombre(this.cnn.getRst().getString("nombreopciones"));
                o.setUrl(this.cnn.getRst().getString("url"));

                op.add(o);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return op;
    }

    public boolean versiTieneOpciondeDental(String us) {
        boolean tiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select distinct (o.*) from "
                + "  seguridad.opciones o join seguridad.rol_opciones ro on o.idopciones= ro.idopciones "
                + " inner join seguridad.rol r on r.idrol = ro.idrol  "
                + " inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol "
                + " INNER join seguridad.usuario u on ur.id_usuario = u.id_usuario "
                + " where u.id_funcionario= '" + us + "' and o.tipo=5 AND r.id_sistema = 3  order by o.nombreopciones;");

        try {
            this.cnn.conectar();
            while (this.cnn.getRst().next()) {
                tiene = true;

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return tiene;
    }

    public int buscarperfil(String us) {
        int perfil = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT r.idrol\n"
                + "FROM \n"
                + "  seguridad.rol r inner join seguridad.usuario_rol ur on ur.id_rol = r.idrol\n"
                + "  join seguridad.usuario u on u.id_usuario = ur.id_usuario where\n"
                + "  u.id_funcionario='" + us + "' order by r.idrol asc;");

        try {
            this.cnn.conectar();
            if (this.cnn.getRst().next()) {
                perfil = this.cnn.getRst().getInt("idrol");
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return perfil;
    }

}
