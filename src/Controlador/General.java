/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.Conexion;
import Modelos.atencion;
import Modelos.atencion_clinica_tecnologo;
import Modelos.cita;
import Modelos.doctor;
import Modelos.lugar;
import Modelos.paciente;
import Modelos.planificar;
import Modelos.prevision;
import Modelos.protocolo;

import Modelos.rol;
import Modelos.usuario;
import bsh.ParseException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Informatica
 */
public class General {

    public Conexion cnn;

    public void configurarConexion(String tabla) {
        this.cnn = new Conexion();
        this.cnn.setDriver("org.postgresql.Driver");
        this.cnn.setNombreTabla(tabla);

         //this.cnn.setUser("especialidades");
         //this.cnn.setPassword("crsdb2008");
         //this.cnn.setNombreBaseDatos("jdbc:postgresql://10.8.4.11:5432/crsm");
         
        /*base de datos local*/
       this.cnn.setUser("postgres");
       this.cnn.setPassword("siigsa2019");
       this.cnn.setNombreBaseDatos("jdbc:postgresql://localhost:5432/crs");
    }

    public String getUsuarioLoggin() {
        String usu = "";

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        usu = auth.getName();
        return usu;

    }

    /*bUSCAR NOMBRE DE LA PERSONA QUE INICIA SESION */
    public Vector<String> buscarfichas() {
        Vector<String> vf = new Vector<String>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "\n"
                + "  fic_rut\n"
                + " \n"
                + "FROM \n"
                + "  schema_archivo.ficha where fic_usuario_creacion= '13.420.978-K'  ;");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                String nombre = this.cnn.getRst().getString("fic_rut");
                vf.add(nombre);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vf;
    }

    public String buscarombreDelUsuairioensession() {
        this.configurarConexion("");
        String nombre = "";
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  rut_funcionario,\n"
                + "  nombre\n"
                + "FROM \n"
                + "  agenda.vistafuncionario  WHERE  rut_funcionario= '" + getUsuarioLoggin() + "' ;");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                nombre = this.cnn.getRst().getString("nombre");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return nombre;
    }

    public String buscaremaildeAdministradores() {
        String email = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select email from seguridad.usuario u\n"
                + "inner join seguridad.usuario_rol ur \n"
                + "on u.id_usuario = ur.id_usuario\n"
                + "where ur.id_rol = 1 or ur.id_rol= 30 ");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                if (email.equals("")) {
                    email = this.cnn.getRst().getString("email");
                } else {
                    email = email + "," + this.cnn.getRst().getString("email");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return email;
    }

    public String buscaremaildeFichaExterna() {
        String email = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  email,\n"
                + "  estatus\n"
                + "   FROM \n"
                + "  schema_archivo.emailfichaexterna where estatus = 1 ; ");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                if (email.equals("")) {
                    email = this.cnn.getRst().getString("email");
                } else {
                    email = email + "," + this.cnn.getRst().getString("email");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return email;
    }

    public Vector<atencion_clinica_tecnologo> BuscarAtencionesdeunPaciente(String rut) {
        Vector<atencion_clinica_tecnologo> va = new Vector<atencion_clinica_tecnologo>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  paciente,\n"
                + "  fecha,\n"
                + "  especialidad,\n"
                + "  profesional,\n"
                + "  atencion,\n"
                + "  tipo, tipoatencion\n"
                + "FROM \n"
                + "  agenda.atenciones where upper(paciente) = upper('" + rut + "')\n"
                + "  order by  fecharegistro:: date  desc;");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                atencion_clinica_tecnologo act = new atencion_clinica_tecnologo();
                /*paciente*/
                act.setAuxiliaruno(this.cnn.getRst().getString("paciente"));
                act.setAuxiliarcinco(this.cnn.getRst().getString("fecha"));
                /*especialidad*/
                act.setAuxiliardos(this.cnn.getRst().getString("especialidad"));
                /*profesional*/
                act.setAuxiliartres(this.cnn.getRst().getString("profesional"));
                act.setId_atencion_tecnologo(this.cnn.getRst().getInt("atencion"));
                act.setAuxiliarcuatro(this.cnn.getRst().getInt("tipo"));
                act.setAuxiliarseis(this.cnn.getRst().getString("tipoatencion"));
                va.add(act);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;

    }

    public boolean buscarsipuedeCitar(String rut) {

        boolean puede = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from seguridad.opciones o inner join seguridad.rol_opciones ro\n"
                + " on o.idopciones = ro.idopciones \n"
                + " join seguridad.rol r on r.idrol = ro.idrol\n"
                + " join seguridad.usuario_rol ur on ur.id_rol = r.idrol\n"
                + " join seguridad.usuario u on u.id_usuario= ur.id_usuario\n"
                + " where r.id_sistema = 3 and  o.idopciones = 13 and upper(u.id_funcionario) = upper('" + rut + "');");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                puede = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return puede;

    }

    public String getIpUsuarioLoggin() throws UnknownHostException {
        String dire = "";
        String ip;
        dire = InetAddress.getLocalHost().getHostAddress();
        String nomb = "";
        nomb = InetAddress.getLocalHost().getHostName();
        ip = dire + "-" + nomb;
        return ip;
    }

    /*BUSCAR EL PERFIL Y EL NOMBRE DEL QUE ESTE LOGEADO*/
    public String[] buscarPerfilyNombredelloggeado() {
        String[] datos = new String[2];
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select v.nombre , r.nombrerol\n"
                + "from seguridad.usuario u\n"
                + " inner join agenda.vistafuncionario v   on u.id_funcionario = v.rut_funcionario\n"
                + " join seguridad.usuario_rol ur on ur.id_usuario = u.id_usuario \n"
                + " join seguridad.rol r on ur.id_rol= r.idrol where u.id_funcionario='" + getUsuarioLoggin() + "' ");
        this.cnn.conectar();

        try {

            while (this.cnn.getRst().next()) {
                datos[0] = this.cnn.getRst().getString("nombre");
                datos[1] = this.cnn.getRst().getString("nombrerol");
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return datos;
    }

    public String getLocal() {

         //String local = "http://10.8.4.11:9090/Especialidades/";
        String local = "http://localhost:8084/Especialidades/";

        return local;
    }

    //para guardar las fotors
    public String getLocallink() {
       //String local = "http://10.8.4.11:9090/Especialidades/vistas/";
        String local = "http://localhost:8084/Especialidades/vistas/";

        return local;
    }

    /*pasar fehca de string a date*/
    public Date DeStringADate(String fecha) throws java.text.ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String strFecha = fecha;
        Date fechaDate = null;
        fechaDate = formato.parse(strFecha);
        return fechaDate;
    }

    public lugar FichaPaciente(String rut) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + "  p.rut,"
                + "  p.nombre,"
                + "  p.apellido_paterno,"
                + "   p.apellido_moderno,"
                + "   p.fecha_nacimiento,"
                + "   p.direccion,"
                + "   p.email,"
                + "   p.contacto1,"
                + "   p.contacto2,"
                + "  case when p.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,"
                + "   p.fecha_registro,"
                + "   p.id_comuna,"
                + "   case when  p.provision= 1 then 'FONASA' ||' '||t.nombre when p.provision = 2 then 'PARTICULAR' else 'ISAPRE' end as prevension ,"
                + "   p.tramo,"
                + "   p.procedencia, t.nombre as tramo_nombre , "
                + "   c.nombre as comuna,"
                + "   pr.nombre_servicio_salud,"
                + "  to_char(age(CURRENT_DATE, p.fecha_nacimiento),'yy') ||' '|| 'años' as edad"
                + " FROM "
                + "  agenda.paciente p inner join agenda.tramo t on p.tramo= t.id "
                + "  inner join agenda.comuna c on p.id_comuna = c.id_comuna "
                + "  inner join agenda.procedencia pr on pr.id_servicio_salud = p.procedencia "
                + "  where rut = '" + rut + "'");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                l.setRut(this.cnn.getRst().getString("rut"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                l.setVariable1(this.cnn.getRst().getString("apellido_paterno"));
                l.setVariable2(this.cnn.getRst().getString("apellido_moderno"));
                l.setVariable3(this.cnn.getRst().getString("genero"));
                l.setFecha(this.cnn.getRst().getDate("fecha_nacimiento"));
                l.setDireccion(this.cnn.getRst().getString("direccion"));
                l.setEmail(this.cnn.getRst().getString("email"));
                l.setVariable4(this.cnn.getRst().getString("contacto1"));
                l.setComuna(this.cnn.getRst().getString("contacto2"));
                l.setPrescripcion(this.cnn.getRst().getString("prevension"));
                l.setTratamiento(this.cnn.getRst().getString("tramo_nombre"));
                l.setSederivapara(this.cnn.getRst().getString("nombre_servicio_salud"));
                l.setFecha2(this.cnn.getRst().getDate("fecha_registro"));
                l.setEdad(this.cnn.getRst().getString("edad"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return l;
    }

    //
    /* Metodo que permite enviar un correo electronico a cualquier destinatario */
    public boolean enviarEmailNotificacion(String asuntos, String correo, String mensajes) {
        try {

            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.redsalud.gov.cl");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props);
            String asunto = asuntos;
            String remitente = "sistemas.crsm@redsalud.gov.cl";
            String contrasena = "Minsal.2016";
            String destino = correo;
            String mensaje = mensajes;

            String destinos[] = destino.split(",");

            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(remitente));

            Address[] receptores = new Address[destinos.length];
            int j = 0;
            while (j < destinos.length) {
                receptores[j] = new InternetAddress(destinos[j]);
                j++;
            }

            BodyPart texto = new MimeBodyPart();
            texto.setText(mensaje);
            BodyPart adjunto = new MimeBodyPart();

            MimeMultipart multiParte = new MimeMultipart();

            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);

            message.addRecipients(Message.RecipientType.TO, receptores);
            message.setSubject(asunto);
            message.setContent(mensaje, "text/html");
            Transport t = session.getTransport("smtp");
            t.connect(remitente, contrasena);
            t.sendMessage(message,
                    message.getRecipients(Message.RecipientType.TO));

            t.close();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //olvido la contraseña
    public String recuperarContraseña(String us) {
        String contraseña = "";
        String mensaje = "";
        String email = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from seguridad.usuario where nombre_usuario = '" + us + "';  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                contraseña = this.cnn.getRst().getString("contrasena");
                email = this.cnn.getRst().getString("email");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        //envio correo con su nueva vieja contraseña
        String cuerpo2 = "<html>\n"
                + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/banner.JPG\" >  \n"
                + "  <h1 style=\"color: #0059b3\"> Recuperación de la contraseña </h1>\n"
                + "  <p> Su contraseña es :" + contraseña + "</p>\n"
                + "  <p>Favor ingresar al siguiente link:<a href=\"http://10.8.4.11:9090/Especialidades/\">Ingresar </a> </p>\n"
                + "  <img src=\"cid:http://10.8.4.11:9090/Especialidades/public/imagenes/footer.JPG\" >  \n"
                + "</html>";
        String asunto = "Mail enviado con clave de usuario";

        boolean valor = enviarEmailNotificacion(asunto, email, cuerpo2);
        if (valor == true) {
            mensaje = "Su clave fue enviada a la dirección de correo electrónico existente en el sistema (correo institucional)";
        } else {
            mensaje = "Ocurrio un error intente mas tarde";
        }
        return mensaje;
    }

    public String enviarEmailporCancelarCita(int idcita) {
        String men = "";

        String paciente_nombres = "";

        String fecha = "";
        String motivo_cancela = "";
        String hora = "";

        String doctor = "";
        String email = "";

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  to_char(p.fecha , 'dd-mm-yyyy') as fecha ,\n"
                + "  pc.nombre  ||' '|| pc.apellido_paterno ||' '|| pc.apellido_moderno as nombrepaciente, \n"
                + "  c.motivo_cancela,\n"
                + "  d.nombre  ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor,\n"
                + "  o.hora , pc.email \n"
                + "\n"
                + " FROM \n"
                + "  agenda.cita c inner join agenda.oferta o\n"
                + "  on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar p on\n"
                + "  p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.doctor d on d.rut = p.rut_doctor\n"
                + "  join agenda.paciente pc on pc.rut = c.rut_paciente\n"
                + "   where id_cita = " + idcita + " ;");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {

                paciente_nombres = this.cnn.getRst().getString("nombrepaciente");

                fecha = this.cnn.getRst().getString("fecha");
                hora = this.cnn.getRst().getString("hora");
                motivo_cancela = this.cnn.getRst().getString("motivo_cancela");
                doctor = this.cnn.getRst().getString("nombredoctor");
                email = this.cnn.getRst().getString("email");

            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        String cuerpo2 = "<html>\n"
                + "  <img src=\"cid:http://10.8.4.11:9090/Especialidades/public/imagenes/banner.JPG\" >  \n"
                + "  <h1 style=\"color: #0059b3\"> CANCELACIÓN DE CITA </h1>\n"
                + "  <p>     Estimad@ : " + paciente_nombres + " ;</p>\n"
                + "  <p> Su cita del dia : " + fecha + " a las " + hora + "  con el Profesional: " + doctor + " en el CRS-Maipu  </p>  \n"
                + "  <p> FUE CANCELADO por el siguiente motivo : " + motivo_cancela + "  </p>  \n"
                + "  <img src=\"cid:http://10.8.4.11:9090/Especialidades/public/imagenes/footer.JPG\" >  \n"
                + "</html>";
        String asunto = "Cancelación de Cita ";

        boolean valor = enviarEmailNotificacion(asunto, email, cuerpo2);
        if (valor == true) {
            men = "Su clave fue enviada a la dirección de correo electrónico existente en el sistema (correo institucional)";
        } else {
            men = "Ocurrio un error intente mas tarde";
        }
        return men;
    }

    public String enviarEmielconComprobanteCita(int idcita, String email) {
        String men = "";
        String rut = "";
        String paciente_nombres = "";

        Date fecha = new Date();
        String hora = "";
        String especialidad = "";
        String doctor = "";
        String tipoatencion = "";

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " p.rut, "
                + " (p.nombre || ' ' || p.apellido_paterno || ' ' || p.apellido_moderno) as paciente, "
                + " pl.fecha, "
                + " o.hora, "
                + " e.nombreespecialidad as especialidad, "
                + " (d.nombre ||' '||d.apellido_paterno ||' '|| d.apellido_materno) as doctor , "
                + " a.nombre as tipoatencion "
                + " FROM  "
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta "
                + "  join agenda.paciente p on c.rut_paciente = p.rut "
                + "   join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre "
                + "  join agenda.doctor d on d.rut = pl.rut_doctor "
                + "   join agenda.especialidades e on pl.id_especialidad = e.id_especialidad "
                + "  join agenda.atencion a on pl.id_atencion = a.id_atencion where c.id_cita= " + idcita + " ;  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                rut = this.cnn.getRst().getString("rut");
                paciente_nombres = this.cnn.getRst().getString("paciente");

                fecha = this.cnn.getRst().getDate("fecha");
                hora = this.cnn.getRst().getString("hora");
                especialidad = this.cnn.getRst().getString("especialidad");
                doctor = this.cnn.getRst().getString("doctor");
                tipoatencion = this.cnn.getRst().getString("tipoatencion");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        String cuerpo2 = "<html >\n"
                + "<head>\n"
                + "    <title>Comprobante de Cita</title>\n"
                + "\n"
                + "    <!-- This snippet is used in the Firefox extension (included from viewer.html) -->\n"
                + " \n"
                + "\n"
                + " </head>\n"
                + "\n"
                + "  <body >\n"
                + "   \n"
                + "\n"
                + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/banner.JPG\" >  \n"
                + "        <div >\n"
                + "          <div  style=\"font-size: 30px; font-family: sans-serif;\">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;COMPROBANTE DE CITA</div>\n"
                + "          <div style=\"left: 85px;  font-size: 30px; font-family: sans-serif; transform: scaleX(0.9998);\">__________________________________________________</div>\n"
                + "  <br>   <br> "
                + "          <div style=\"font-size: 30px; font-family: sans-serif;\">DATOS DEL PACIENTE</div>"
                + "  <br>   <br> "
                + "          <div style=\"font-size: 25px; font-family: sans-serif;\"> &nbsp;&nbsp; &nbsp;        RUT:&nbsp;" + rut + " </div>\n"
                + "          <div style=\"font-size: 25px; font-family: sans-serif;\">  &nbsp;&nbsp;  &nbsp;      NOMBRE: &nbsp; " + paciente_nombres + " </div>\n"
                + "  <br>   <br> "
                + "         <div style=\"font-size: 30px; font-family: sans-serif;\">DATOS DE LA CITA</div>\n"
                + "  <br>   <br> "
                + "          <div style=\"font-size: 25px; font-family: sans-serif;\"> &nbsp;&nbsp;&nbsp;         FECHA CITA:&nbsp; " + fecha + "</div>\n"
                + "          <div style=\"font-size: 25px; font-family: sans-serif;\">&nbsp;&nbsp;&nbsp;           HORA CITA:&nbsp;  " + hora + " </div>\n"
                + "          <div style=\"font-size: 25px; font-family: sans-serif;\">  &nbsp;&nbsp;&nbsp;        ESPECIALIDAD: &nbsp; Vicio Refracción </div>\n"
                + "          <div style=\"font-size: 25px; font-family: sans-serif;\">  &nbsp;&nbsp;&nbsp;      PROFESIONAL:&nbsp;  " + doctor + " </div>\n"
                + "          <div style=\"font-size: 25px; font-family: sans-serif;\">  &nbsp;&nbsp;&nbsp;         TIPO ATENCION:&nbsp; " + tipoatencion + " </div>"
                + "  <br>   <br> "
                + "          <div style=\"font-size: 30px; font-family: sans-serif;\">INDICACIONES</div>\n"
                + "  <br>   <br> "
                + "          <div style=\"left: 85px;  font-size: 13.3333px; font-family: sans-serif; transform: scaleX(1.02167);\"> &nbsp;&nbsp;                   * DEBE PRESENTARSE CON SU CEDULA DE IDENTIDAD Y DOCUMENTACION DE PREVISION AL DIA.</div>\n"
                + "          <div style=\"left: 85px;  font-size: 13.3333px; font-family: sans-serif; transform: scaleX(1.0135);\">   &nbsp;&nbsp;                 * DEBE LLEGAR 30 MINUTOS ANTES DE LA HORA DE ATENCIÓN.</div>\n"
                + "  <br>   "
                + "          <div style=\"left: 85px; font-size: 18.3333px; font-family: sans-serif; transform: scaleX(1.00183);\">&nbsp;&nbsp;LUGAR DE ATENCION: &nbsp;CENTRO DE REFERENCIA DE SALUD MAIPU (CRS Maipu)</div>\n"
                + "          <div style=\"left: 85px;  font-size: 18.3333px; font-family: sans-serif; transform: scaleX(0.999865);\">&nbsp;&nbsp;Direccion:&nbsp; Camino Rinconada # 1001 </div>\n"
                + "           <div style=\"left: 369.45px; top: 740px; font-size: 18.3333px; font-family: sans-serif; transform: scaleX(1.00546);\">&nbsp;&nbsp;Transantiago: &nbsp;108-109-I08-I09-I06-I03 </div>\n"
                + "          <div style=\"left: 653.883px; font-size: 18.3333px; font-family: sans-serif; transform: scaleX(1.02508);\">&nbsp;&nbsp;Telefono:&nbsp; 574 6450 </div>\n"
                + "          <div style=\"left: 85px;  font-size: 30px; font-family: sans-serif; transform: scaleX(0.9998);\">__________________________________________________</div>\n"
                + "          <div class=\"endOfContent\"></div>\n"
                + "          </div>\n"
                + "          </div>\n"
                + "          </div>\n"
                + "        </div>\n"
                + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/footer.JPG\" >  \n"
                + "        \n"
                + "      \n"
                + "\n"
                + "   \n"
                + "  \n"
                + "  \n"
                + "\n"
                + "\n"
                + "</body>\n"
                + "</html>";
        String asunto = "Comprobante de Cita CRS-Maipu";

        boolean valor = enviarEmailNotificacion(asunto, email, cuerpo2);
        if (valor == true) {
            men = "Correo electrónico enviado existente en el sistema (correo institucional)";
        } else {
            men = "Ocurrio un error intente mas tarde";
        }

        return men;
    }

    // busco ultima fecha de modificacion 
    public Date buscarultimafechademodificacion(String nombre_usuario) {
        Date debecambiar = null;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select fecharegistro from seguridad.usuario where nombre_usuario = '" + nombre_usuario + "'");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                debecambiar = this.cnn.getRst().getDate("fecharegistro");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return debecambiar;

    }

    // ahora veo cuantos dias lelva sin cambiar la fecha si son mas de 60 lo mando a cambiar fecha
    public boolean verificarsinecesitacambiarcontraseña(String nombreusuario, Date fecharegistro) {
        boolean debocambiar = false;
        int dias = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select extract(days from ( CURRENT_DATE - timestamp '" + fecharegistro + "'))");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {

                dias = this.cnn.getRst().getInt("days");
                if (dias >= 90) {
                    debocambiar = true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return debocambiar;
    }

    //decir que el usuario tiene foto
    public void ingresarfoto(String us) {
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("update seguridad.usuario set foto =1 where nombre_usuario='" + us + "'  ); ");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    // pasar datos de String a hora
    public Date pasarStringHora(String hora) throws java.text.ParseException {
        DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        Date date = sdf.parse(hora);
        return date;
    }

    public ArrayList generaAgenda(Date fechainicio, int ultimo) {
        ArrayList mes = new ArrayList();
        Calendar dia = Calendar.getInstance();

        int i = 1;
        mes.add(fechainicio);
        while (i < ultimo) {
            dia.setTime(fechainicio);
            dia.add(Calendar.DATE, 1);
            Date undiamas = dia.getTime();
            mes.add(undiamas);
            fechainicio = undiamas;
            ++i;
        }

        return mes;
    }

    public ArrayList generarAgendaporSemana(Date fechaInicio) {
        //array para la semana
        ArrayList semana = new ArrayList();

        //instancio el dia para restar y colocar la fecha al primer dia de la semana
        Calendar dia = Calendar.getInstance();
        dia.setTime(fechaInicio);

        Date diasemana = dia.getTime();
        semana.add(diasemana);
        //hago un while para el resto de los dias
        int i = 0;
        while (i < 6) {
            dia.setTime(diasemana);
            dia.add(Calendar.DATE, 1);
            Date undiamas = dia.getTime();
            semana.add(undiamas);
            diasemana = undiamas;
            ++i;

        }
        return semana;
    }

    public Vector<planificar> buscarsolocuposparaAgendaMensual(Date fecha, int especialidad, String doctor) {
        Vector<planificar> vp = new Vector<planificar>();
        String where = "";
        if (especialidad == -1) {
            where = " ";
        } else if (!doctor.equals("-1")) {
            where = "and p.id_especialidad= " + especialidad + " and p.rut_doctor = '" + doctor + "'";
        } else {
            where = "and p.id_especialidad= " + especialidad + " ";
        }
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL(" SELECT  a.abreviatura,(sum(p.cupos)+sum(s.cupos))- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados))as cupo, \n"
                + "                     (sum(p.cupos_disponibles)+sum(s.cupos_disponibles))  as disponible , \n"
                + "                       (((sum(p.cupos)+sum(s.cupos))- (sum(p.cupos_bloqueados)+sum(s.cupos_bloqueados))) - (sum(p.cupos_disponibles)+sum(s.cupos_disponibles))) as ocupados , p.id_atencion\n"
                + "                       FROM  agenda.planificar p inner join agenda.sobreplanificar s \n"
                + "                       on p.id_planificacion = s.id_planificar\n"
                + "                        join agenda.atencion a on a.id_atencion = p.id_atencion\n"
                + "               where  p.estatus= 1 and s.estatus=1 and p.fecha = '" + fecha + "' " + where + " \n"
                + "                 GROUP by p.id_atencion,s.id_atencion, abreviatura");

        this.cnn.conectar();
        try {

            while (this.cnn.getRst().next()) {
                planificar p = new planificar();
                p.setNumero_cupo(this.cnn.getRst().getInt("cupo"));
                p.setCupos_disponibles(this.cnn.getRst().getInt("ocupados"));
                p.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                p.setRut_doctor(this.cnn.getRst().getString("abreviatura"));

                vp.add(p);

            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vp;
    }

    public boolean buscarPlanificacion(Date fecha, int especialidad, String doctor) {
        boolean siexiste = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        if (especialidad == -1) {
            this.cnn.setSentenciaSQL("SELECT  * FROM agenda.planificar where fecha= '" + fecha + "' and estatus = 1; ");
        } else if (!doctor.equals("-1")) {
            this.cnn.setSentenciaSQL("SELECT  * FROM agenda.planificar where fecha=  '" + fecha + "' and id_especialidad=" + especialidad + " and rut_doctor ='" + doctor + "' and estatus = 1 ");

        } else {

            this.cnn.setSentenciaSQL("SELECT  * FROM agenda.planificar where fecha= '" + fecha + "' and id_especialidad=" + especialidad + "  and estatus = 1; ");
        }
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                siexiste = true;
            } else {
                siexiste = false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return siexiste;

    }

    public Vector<prevision> buscarPrevicion() {
        Vector<prevision> p = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_prevision,\n"
                + "  nombre\n"
                + "  FROM \n"
                + "  agenda.prevision where estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                prevision pre = new prevision();
                pre.setId_prevision(this.cnn.getRst().getInt("id_prevision"));
                pre.setNombre(this.cnn.getRst().getString("nombre"));
                p.add(pre);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*voy a buscar tramo de la misma manera*/
    public Vector<prevision> buscarTramo() {
        Vector<prevision> p = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id,nombre FROM agenda.tramo  where status = 1 order by nombre");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                prevision pre = new prevision();
                pre.setId_prevision(this.cnn.getRst().getInt("id"));
                pre.setNombre(this.cnn.getRst().getString("nombre"));
                p.add(pre);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*Buscar Region*/
    public Vector<lugar> buscarregion() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_region, nombre FROM agenda.region where estatus = 1 order by nombre;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_region"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    /*buscar procedencia */
    public Vector<lugar> buscarProcedencia() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_servicio_salud, nombre_servicio_salud FROM  agenda.procedencia where estado_servicio_salud = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_servicio_salud"));
                l.setNombre(this.cnn.getRst().getString("nombre_servicio_salud"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    /*buscar providencia*/
    public Vector<lugar> buscarProvidencia(int region) {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_provincia, nombre FROM agenda.provincia where estatus = 1 and  id_region= " + region + " order by nombre;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_provincia"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    public Vector<lugar> buscarComuna(int providencia) {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_comuna, nombre FROM agenda.comuna where estatus = 1 and id_provincia=" + providencia + " order by nombre;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_comuna"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    /*buscar todas las comunas*/
    public Vector<lugar> buscarTodasLasComuna() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT * FROM agenda.comuna where estatus = 1 order by nombre; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_comuna"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    /*buscar la atencion tomo a lugar para no crear mas modelos con solo dos atributos*/
    public Vector<lugar> buscarAtencion(int idatencion) {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select * from agenda.atencion where estatus = 1 and id_atencion = " + idatencion + "; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_atencion"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    /*buscar la atencion tomo a lugar para no crear mas modelos con solo dos atributos*/
    public Vector<lugar> buscarTipodeAtencion() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT  * FROM agenda.tipo_atencion where estatus = 1;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_tipoatencion"));
                l.setNombre(this.cnn.getRst().getString("descripcion"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    /*buscar la atencion tomo a lugar para no crear mas modelos con solo dos atributos*/
    public Vector<lugar> buscaropcionHito() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  id_opcion,descripcion FROM agenda.opcionhito where tipo = 1 and estatus = 1;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_opcion"));
                l.setNombre(this.cnn.getRst().getString("descripcion"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    public Vector<lugar> buscaropcionHito2() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  id_opcion,descripcion FROM agenda.opcionhito2 where tipo = 2 and estatus = 1;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_opcion"));
                l.setNombre(this.cnn.getRst().getString("descripcion"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    public Vector<lugar> buscaropcionHito3() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  id_opcion,descripcion FROM agenda.opcionhito3 where tipo = 3 and estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_opcion"));
                l.setNombre(this.cnn.getRst().getString("descripcion"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;
    }

    /*atencion para agendar cupos */
    public Vector<lugar> buscarAtencionparaAgendarCupos() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from agenda.atencion where estatus = 1  order by nombre;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_atencion"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;

    }

    /*atencion para agendar cupos */
    public Vector<lugar> buscarProgramaparaAgendarCupos() {
        Vector<lugar> lugar = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from agenda.programa where estatus =1 and idespecialidad = 1 order by nombre; ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setId_lugar(this.cnn.getRst().getInt("id_programa"));
                l.setNombre(this.cnn.getRst().getString("nombre"));
                lugar.add(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lugar;

    }

    public Vector<doctor> buscarAtencionporProfesional() {
        Vector<doctor> dres = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("Select count(*)  , d.rut as r ,\n"
                + "   (d.nombre || ' '  || d.apellido_paterno) as doctor \n"
                + "    from agenda.cita c inner join agenda.oferta o on \n"
                + "  c.id_oferta = o.id_oferta  \n"
                + "    join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "  join agenda.doctor d on d.rut = p.rut_doctor where\n"
                + "   EXTRACT(MONTH FROM age(timestamp 'now()',date(p.fecha) ) ) = 0 \n"
                + "    and EXTRACT(YEAR FROM age(timestamp 'now()',date(p.fecha) ) ) = 0 and  \n"
                + "   ( EXTRACT(day FROM age(timestamp 'now()',date(p.fecha) )) <=30 ) group by  doctor , r\n"
                + "   ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                doctor d = new doctor();
                d.setRut(this.cnn.getRst().getString("r"));
                d.setEstatus(this.cnn.getRst().getInt("count"));
                d.setNombre(this.cnn.getRst().getString("doctor"));
                dres.add(d);

            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return dres;

    }

    public Integer buscarcuantosAtendidosSatisfactoriamente(String rutdoctor) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select count(*) from  agenda.cita c inner join agenda.oferta o on \n"
                + "                     c.id_oferta = o.id_oferta  \n"
                + "                     join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                    join agenda.doctor d on d.rut = p.rut_doctor where\n"
                + "                     EXTRACT(MONTH FROM age(timestamp 'now()',date(p.fecha) ) ) = 0 \n"
                + "                    and EXTRACT(YEAR FROM age(timestamp 'now()',date(p.fecha) ) ) = 0 and  \n"
                + "                    ( EXTRACT(day FROM age(timestamp 'now()',date(p.fecha) )) <=30 ) and c.estatus = 4 and upper(p.rut_doctor) = upper('" + rutdoctor + "')");
        this.cnn.conectar();
        int cantidad = 0;
        try {
            if (this.cnn.getRst().next()) {
                cantidad = this.cnn.getRst().getInt("count");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return cantidad;

    }

    /*busqueda general*/
    public lugar buscaratencioCompleta(int idatencion) {
        lugar au = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select au.id_atencion, to_char(a.fecharegistro, 'dd/mm/yyyy') as fechallegada, \n"
                + "                  to_char(a.fecharegistro, 'HH24:MI:SS') as horallegada,   \n"
                + "                \n"
                + "                    a.motivo_consulta, \n"
                + "                      \n"
                + "                     du.descripcion || '-' || case when du.absceso_submucoso = true then ' Absceso Submucoso, ' else '' end \n"
                + "                    || case when du.gingivitis_ulcero = true then ' Gingivitis Ulcero necrotizante, ' else '' end  \n"
                + "                   || case when du.complicacion_post_exodoncia = true then ' Complicaciones post exodoncia, ' else '' end  \n"
                + "                    || case when du.traumatismo_dento_alveolares = true then ' Traumatismos dento alveolares, ' else '' end   \n"
                + "                    || case when du.pericoronaritis = true then ' Pericoronaritis, ' else '' end  \n"
                + "                    || case when du.pulpitis = true then ' Pulpitis. ' else '' end as diagnostico ,\n"
                + "                       \n"
                + "                   case when pug.urgencia = true then ' 2701012 Urgencia- ' else '' END  \n"
                + "                   || case when au.trepanacion = true then ' Trepanación- ' else '' end  \n"
                + "                   || case when au.pulpotamia = true then ' Pulpotamia- ' else '' end  \n"
                + "                    || case when au.recubrmiento = true then ' Recubrimiento- '  else '' end \n"
                + "                    || case when au.ferulizacion = true then ' Ferulización- ' else '' end \n"
                + "                    || case when au.terapia_farmacolofica = true then ' Terapia Farmacologica de Inf. Odontogenicas- ' else '' end \n"
                + "                   || case when au.cirugia_bucal = true then ' Cirugia Bucal- ' else '' end \n"
                + "                   || case when pug.exodoncia_permanente = true then ' 2701005 Exodoncia Permanente- ' || au.numero_exodonciap ||'- ' else '' end    \n"
                + "                    || case when pug.kit_aseo_bucal = true then ' 2701013 Examen Salud Oral- ' else '' end  \n"
                + "                    || case when pug.sutura_intraoral = true then ' 2701114 Sutura Intraoral- ' else '' end   \n"
                + "                    || case when pug.obturacion_composite = true then ' 2701010 Exodoncia Temporal- '|| au.numero_exodenciat ||'- ' else '' end   \n"
                + "                    || case when pug.radiografia_retroalvealar = true then ' 2701015 Radiografía Retroalveolar y Bite-Wing - ' || pug.numero_de_rx ||'- ' else '' end  \n"
                + "                   ||  case when pug.obturacion_vidrio_ionomero = true then ' 2701016 Obturación Vidrio Ionómero- ' else '' end ||  au.prescripcion as prestacion,  \n"
                + "                     case when au.ges = 1 then 'Si' else 'No' end as ges, \n"
                + "                   au.prescripcion as tratamiento,  au.intervencio as intervencion, \n"
                + "                    au.indicaciones as indicaciones, \n"
                + "                     d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor,  \n"
                + "                     to_char(au.fecharegistro,'HH24:MI:SS') as horaatencion, \n"
                + "                    d.rut as rutdoctor , au.ges as idges , au.interconsulta  from dental.atencion_urgencia au   \n"
                + "                    inner join dental.admision a on a.id_admision= au.idadmision  \n"
                + "                     join agenda.paciente pu  \n"
                + "                    on a.rut_paciente = pu.rut   \n"
                + "                    join dental.prestacion_urgenta pug on pug.id_atencion = au.id_atencion  \n"
                + "                   join agenda.comuna c on c.id_comuna = pu.id_comuna \n"
                + "                    join agenda.procedencia p on au.id_centro = p.id_servicio_salud   \n"
                + "                      join dental.diagnostico_urgencia du on au.id_atencion = du.id_atencion   \n"
                + "                   join agenda.doctor d on d.rut = au.rutdoctor where au.id_atencion =" + idatencion + "\n"
                + "    ; ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                au.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
                au.setVariable1(this.cnn.getRst().getString("fechallegada"));
                au.setVariable2(this.cnn.getRst().getString("horallegada"));
                au.setVariable3(this.cnn.getRst().getString("motivo_consulta"));
                au.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                au.setDescripcion(this.cnn.getRst().getString("ges"));
                au.setIntervencio(this.cnn.getRst().getString("intervencion"));
                au.setPrescripcion(this.cnn.getRst().getString("prestacion"));
                au.setIndicaciones(this.cnn.getRst().getString("indicaciones"));
                au.setTratamiento(this.cnn.getRst().getString("tratamiento"));
                au.setNombredoctor(this.cnn.getRst().getString("nombredoctor"));
                au.setVariable4(this.cnn.getRst().getString("horaatencion"));
                au.setGes(this.cnn.getRst().getInt("idges"));
                au.setInterconsulta(this.cnn.getRst().getBoolean("interconsulta"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return au;
    }

    /*cabecera*/
    public paciente buscardatosdecabeceraporAdmision(int a) {
        paciente p = new paciente();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  p.rut,\n"
                + "         p.nombre ||' '|| p.apellido_paterno ||' '|| p.apellido_moderno as nombrepaciente,\n"
                + "        to_char(age(p.fecha_nacimiento),'yy')|| '  Años' as edad,\n"
                + "         case when p.genero =1 then 'Femenino'else 'Masculino' end as genero,\n"
                + "        case when p.provision = 1 then 'Fonasa Tramo: ' || t.nombre else case when p.provision = 2 then 'Isapre' else  case when p.provision = 4 then 'Sin prevision' else 'Prais' end end end as prevencion  ,\n"
                + "        a.motivo_consulta\n"
                + "           FROM \n"
                + "           dental.admision a inner join agenda.paciente p on a.rut_paciente = p.rut \n"
                + "           join agenda.tramo t on t.id = p.tramo \n"
                + "         where a.id_admision= " + a + "");
        this.cnn.conectar();
        //utilizo el objeto de paciente para hacer la cosa mas facil pero en realidad son los datos de su historial
        try {
            while (this.cnn.getRst().next()) {

                p.setRut(this.cnn.getRst().getString("rut"));
                p.setNombre(this.cnn.getRst().getString("nombrepaciente"));
                p.setApellido_moderno(this.cnn.getRst().getString("motivo_consulta"));
                p.setApellido_paterno(this.cnn.getRst().getString("edad"));
                p.setContacto1(this.cnn.getRst().getString("genero"));
                p.setContacto2(this.cnn.getRst().getString("prevencion"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return p;
    }

    /*pdfnuevo*/
    public protocolo traerprotocolonuevo(int idprotocolo) {
        protocolo pro = new protocolo();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   pro.idprotocolo,  es.nombreespecialidad,  pro.fecha,  pc.rut, \n"
                + " pc.nombre || ' '|| pc.apellido_paterno ||' '|| pc.apellido_moderno as nombre, \n"
                + "  to_char(age(pc.fecha_nacimiento),'yy')|| '  Años' as edad,  case when pc.genero = 1 \n"
                + "  then 'Femenino' else 'Masculino' end as genero,  pre.nombre as prevision, \n"
                + "   sp.nombresalapabellon,  pro.horaingresopabellon,  pro.horaanestesia, \n"
                + "    pro.horainiciocirugia,  pro.horaterminocirugia,  pro.horasalidapabellon, \n"
                + "     pro.tiempoquirurjico,  mc.nombremotivo as diagnostico,  pro.preoperatorio,\n"
                + "       pro.postoperatorio,  doc.nombre ||' '|| doc.apellido_paterno || ' '|| doc.apellido_materno as cirujano, \n"
                + "        (select nombre ||' '|| apellido_paterno || ' '|| apellido_materno from agenda.doctor\n"
                + "         where rut= pro.rutdoctorayudante ) as ayudantecirujia,\n"
                + "           pa.nombreanastesista ||' '|| pa.apellidopanestesista || ' '|| pa.apellidomanestesista as anestisista,\n"
                + "             pab.nombrepabellonera ||' '|| pab.apellidoppabellonera ||' '|| pab.apellidompabellonera as pabellonera,  ars.nombrearsenalero ||' '|| ars.apellidoparsenalero ||' '|| ars.apellidomarsenalero as arsenalero, \n"
                + "              perf.nombreperfusionista ||' '|| perf.apellidoperfusionista ||' '|| perf.apellidomperfusionista as perfusionista,  \n"
                + "              ta.nombre as tipadeanestesia,  th.nombretipoherida as tipoherida,  pro.detalleoperatorio,  c.id_cita , \n"
                + "              (SELECT    p.codigo ||' '|| p.descripcion as nombreprestacion FROM    agenda.cita_prestacion cp   \n"
                + "               inner join caja.prestacion p on p.oid = cp.id_prestacion  where    cp.id_cita = c.id_cita and cp.tipo = 1 ) \n"
                + "               as prestacionplanificada \n"
                + "                  FROM    pabellon.protocolo pro inner join  \n"
                + "                    agenda.cita c on pro.idcita = c.id_cita  \n"
                + "                     join pabellon.\"salaPabellon\" sp on pro.idsalapabellon = sp.idsalapabellon  \n"
                + "                       join agenda.paciente pc    on c.rut_paciente = pc.rut  \n"
                + "                         join agenda.oferta o on o.id_oferta = c.id_oferta  \n"
                + "                           join agenda.planificar pla on pla.id_planificacion = o.id_plani_sobre \n"
                + "                              join agenda.especialidades es on es.id_especialidad = pla.id_especialidad    \n"
                + "                              join agenda.doctor doc on doc.rut = pro.rutmedicocirujano  \n"
                + "                              join agenda.atencion tp on pla.id_atencion = tp.id_atencion   \n"
                + "                               join agenda.motivoconsulta mc on mc.id_motivo = c.id_motivo \n"
                + "                                 join agenda.prevision pre on pre.id_prevision = pc.provision \n"
                + "                                  join pabellon.profesionalanestesista pa on pa.rutanastesista = pro.rutmedicoanestesista    \n"
                + "                                  join pabellon.pabellera pab on pab.rutpabellonera = pro.idpabellones \n"
                + "                                     join pabellon.arsenalero ars on ars.rutarsenalero = pro.rutarsenalero   \n"
                + "                                      join pabellon.perfusionista perf on perf.rutperfusionista = pro.idperfusionista\n"
                + "                                         join pabellon.tipoanestesia ta on ta.idtipoanestesia = pro.idtipoanestesia \n"
                + "  join pabellon.tipoherida th on th.idtipoherida = pro.idtipoherida    \n"
                + "   where pro.idprotocolo= " + idprotocolo + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                pro.setIdProtocolo(this.cnn.getRst().getInt("idprotocolo"));
                pro.setVariable1(this.cnn.getRst().getString("nombreespecialidad"));
                pro.setFechaRegistro(this.cnn.getRst().getDate("fecha"));
                pro.setUsuario(this.cnn.getRst().getString("rut"));
                pro.setVariable2(this.cnn.getRst().getString("nombre"));
                pro.setVariable3(this.cnn.getRst().getString("edad"));
                pro.setVariable4(this.cnn.getRst().getString("genero"));
                pro.setVariable5(this.cnn.getRst().getString("prevision"));
                pro.setVariable6(this.cnn.getRst().getString("nombresalapabellon"));
                pro.setHoraIngresoPabellon(this.cnn.getRst().getString("horaingresopabellon"));
                pro.setHoraAnestesia(this.cnn.getRst().getString("horaanestesia"));
                pro.setHoraInicioCirugia(this.cnn.getRst().getString("horainiciocirugia"));
                pro.setHoraTerminoCirugia(this.cnn.getRst().getString("horaterminocirugia"));
                pro.setHoraSalidaPabellon(this.cnn.getRst().getString("horasalidapabellon"));
                pro.setTiempoQuirurjico(this.cnn.getRst().getString("tiempoquirurjico"));
                pro.setVariable7(this.cnn.getRst().getString("diagnostico"));
                pro.setPostOperatorio(this.cnn.getRst().getString("postoperatorio"));
                pro.setPreOperatorio(this.cnn.getRst().getString("preoperatorio"));
                pro.setRutmedicocirujano(this.cnn.getRst().getString("cirujano"));
                pro.setRutProfesionalAyudante(this.cnn.getRst().getString("ayudantecirujia"));
                pro.setRutMedicoAnestesista(this.cnn.getRst().getString("anestisista"));
                pro.setRutPabellonera(this.cnn.getRst().getString("pabellonera"));
                pro.setRutArsenalero(this.cnn.getRst().getString("arsenalero"));
                pro.setRutPerfusionista(this.cnn.getRst().getString("perfusionista"));
                pro.setVariable8(this.cnn.getRst().getString("tipadeanestesia"));
                pro.setVariable9(this.cnn.getRst().getString("tipoherida"));
                pro.setDetalleOperatorio(this.cnn.getRst().getString("detalleoperatorio"));
                pro.setIdCita(this.cnn.getRst().getInt("id_cita"));
                pro.setVarialble10(this.cnn.getRst().getString("prestacionplanificada"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return pro;
    }

    public boolean verSipacienteTieneFichaenArchivo(String rutpaciente) {

        boolean tieneficha = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " *\n"
                + "FROM \n"
                + "  schema_archivo.ficha where upper(fic_rut )=upper('" + rutpaciente + "');");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                tieneficha = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return tieneficha;
    }

    public Vector<lugar> buscarPrestacionesRealizadas(int idcita) {
        Vector<lugar> va = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "\n"
                + "  p.codigo ||' '|| p.descripcion as nombrerealizadas\n"
                + "\n"
                + "FROM \n"
                + "  agenda.cita_prestacion c \n"
                + "  inner join caja.prestacion p on p.oid = c.id_prestacion  where \n"
                + "  id_cita = '" + idcita + "' and tipo = 2 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar ca = new lugar();
                ca.setDiagnostico(this.cnn.getRst().getString("nombrerealizadas"));
                va.add(ca);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    /*para validar rut*/
    public boolean validarRut(String rut) {

        boolean validacion = false;
        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    /*colocar formato a rut*/
    public String FormatearRUT(String rut) {

        int cont = 0;
        String format;
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        format = "-" + rut.substring(rut.length() - 1);
        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
        return format;
    }

    /*buscar los pabellones*/
    public Vector<lugar> salasactivas() {
        Vector<lugar> sv = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idsalapabellon,\n"
                + "  nombresalapabellon,\n"
                + "  estatus\n"
                + "FROM \n"
                + "  \"pabellon\".\"salaPabellon\" where estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar s = new lugar();
                s.setId_lugar(this.cnn.getRst().getInt("idsalapabellon"));
                s.setNombre(this.cnn.getRst().getString("nombresalapabellon"));
                sv.add(s);
            }

        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return sv;
    }

    /*buscar id de epicrisis por iddeprotocolo*/
    public int idepicrisis(int idprotocolo) {
        int id = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idepicrisis\n"
                + "   FROM \n"
                + "  pabellon.registroepicrisis where idprotocolo = " + idprotocolo + "  ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                id = this.cnn.getRst().getInt("idepicrisis");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return id;
    }

    public String fechadeldia() {
        String hoy = "";
        Locale currentLocale = new Locale("es", "CL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        Date dia = new Date();
        hoy = formateadorFecha.format(dia);
        return hoy;
    }

    /*control de horario para profesionaL*/
    public void InsertarsesiondeProfesional(String usuario) {
        this.configurarConexion("");
        int i = 0;
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO seguridad.control_horario(\n"
                + "            usuario, fecha_inicio)\n"
                + "    VALUES ('" + usuario + "',  CURRENT_TIMESTAMP)");
        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }

    /*verificar si el profesional ya ingreso en sesion*/
    public boolean Verrsiyaguardesesion(String usuario) {
        boolean tienesesion = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT usuario, fecha_inicio\n"
                + "  FROM seguridad.control_horario where usuario ='" + usuario + "' and fecha_inicio:: date = current_date;   ");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                tienesesion = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return tienesesion;
    }

    /*Buscar Profesionales que han iniciado sesion*/
    public Vector<doctor> BuscarProfesionalesqueinicaronsesion() {
        Vector<doctor> doctoresconiniciodesesion = new Vector<doctor>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT ch.usuario, d.nombre ||' '|| d.apellido_paterno||' '||  d.apellido_materno as doc, to_char(fecha_inicio , 'HH24:MI:SS') as inicio\n"
                + "  FROM seguridad.control_horario ch inner join agenda.doctor d\n"
                + "  on ch.usuario = d.rut where fecha_inicio :: date = current_date;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                doctor d = new doctor();
                d.setRut(this.cnn.getRst().getString("usuario"));
                d.setNombre(this.cnn.getRst().getString("doc"));
                d.setNombrefirmadoctor(this.cnn.getRst().getString("inicio"));
                doctoresconiniciodesesion.add(d);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return doctoresconiniciodesesion;

    }

    /*insertarcontrol*/
    public void InsertarsolicituddeControl(atencion aten) {
        this.configurarConexion("");
        int i = 0;
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO agenda.solicitud_control(\n"
                + "             idcita, fecharegistro, usuario_registra, fecha_control, \n"
                + "            motivo_control)\n"
                + "    VALUES ( " + aten.getIdcita() + ", CURRENT_TIMESTAMP, '" + aten.getUsuario()
                + "', '" + aten.getFecha_registro() + "', \n"
                + "            '" + aten.getDetallereceta() + "');");
        this.cnn.conectar();

        this.cnn.cerrarConexion();

    }


    /*Consultas para Informe contador Atenciones*/
    public Vector<doctor> buscarTodaslasatenciones() {

        Vector<doctor> doctoresconiniciodesesion = new Vector<doctor>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT \n"
                + "                       p.rut_doctor, d.nombre, d.apellido_paterno, es.nombreespecialidad,\n"
                + "                      es.id_especialidad\n"
                + "                     \n"
                + "                    FROM \n"
                + "                      agenda.cita C inner join agenda.oferta o \n"
                + "                      on c.id_oferta = o.id_oferta\n"
                + "                      join agenda.planificar p\n"
                + "                      on p.id_planificacion = o.id_plani_sobre\n"
                + "                       join agenda.doctor d on d.rut\n"
                + "                        = p.rut_doctor\n"
                + "                       join agenda.especialidades es on es.id_especialidad = p.id_especialidad \n"
                + "                      \n"
                + "                         where  p.id_atencion in (2,7,8,9,1) and es.id_especialidad IN(select e.id_especialidad\n"
                + "                          from agenda.especialidades e where e.estatus = 1) and date_part('year',p.fecha) = '2018'\n"
                + "                           group by\n"
                + "                         p.rut_doctor, \n"
                + "                         \n"
                + "                         es.id_especialidad, d.nombre ,\n"
                + "                     d.apellido_paterno ,es.nombreespecialidad ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                doctor d = new doctor();
                d.setRut(this.cnn.getRst().getString("rut_doctor"));
                d.setNombre(this.cnn.getRst().getString("nombre"));
                d.setApellido_paterno(this.cnn.getRst().getString("apellido_paterno"));
                d.setApellido_materno(this.cnn.getRst().getString("nombreespecialidad"));
                d.setEstatus(this.cnn.getRst().getInt("id_especialidad"));

                doctoresconiniciodesesion.add(d);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return doctoresconiniciodesesion;

    }

    /*Buscar Control*/
    public boolean buscarsiExistecontrol(int idcita) {
        boolean buscar = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT idcontrol,  fecha_control, \n"
                + "       motivo_control\n"
                + "  FROM agenda.solicitud_control where idcita= " + idcita + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                buscar = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return buscar;
    }

    /*modificar control*/
    public void modificarsitengoControl(atencion a) {
        this.configurarConexion("");
        int i = 0;
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE agenda.solicitud_control\n"
                + "   SET   fecha_control='" + a.getFecha_registro() + "', \n"
                + "       motivo_control='" + a.getDetallereceta() + "'\n"
                + " WHERE idcita=" + a.getIdcita() + " ");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    /*eliminar por si se arrepienten*/
    public void eliminarcontrol(int idcita) {
        this.configurarConexion("");
        int i = 0;
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM agenda.solicitud_control\n"
                + " WHERE idcita = " + idcita + " ; ");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
    }

    public Vector<atencion> BuscarSolicitudesDEQUIROFANOporRangodeFecha(Date fecha1, Date fecha2, String doctor, int especialidad) {

        String resto = "";
        if (especialidad != -1) {
            resto = resto + " and idespecialidad =  " + especialidad + "";
        }

        if (!doctor.equals("-1")) {
            resto = resto + "and rut_doctor='" + doctor + "'";
        }

        Vector<atencion> va = new Vector<atencion>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  rut_paciente,\n"
                + "  nombre_paciente,\n"
                + "  nombre_especialidad,\n"
                + "  diagnstico,\n"
                + "  complemento,\n"
                + "  intervencion,\n"
                + "  anestesia,\n"
                + "  idanestesia,\n"
                + "  requerimientos,\n"
                + "  observacion,\n"
                + "  fecha_registro,\n"
                + "  rut_doctor,\n"
                + "  nombre_doctor,\n"
                + "  idespecialidad, to_char(fecha_registro, 'dd/mm/yyyy') as fechastring \n"
                + "FROM \n"
                + "  agenda.solicitudesdepabellon where fecha_registro \n"
                + "  BETWEEN '" + fecha1 + "' and '" + fecha2 + "' " + resto + " order by  fecha_registro;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setEspecialidad(this.cnn.getRst().getString("nombre_especialidad"));
                a.setProfesion(this.cnn.getRst().getString("nombre_doctor"));
                a.setRutpaciente(this.cnn.getRst().getString("rut_paciente"));
                a.setNombrepaciente(this.cnn.getRst().getString("nombre_paciente"));
                a.setDetallereceta(this.cnn.getRst().getString("diagnstico"));
                a.setCasoges(this.cnn.getRst().getString("complemento"));
                a.setCorreo_electronico(this.cnn.getRst().getString("intervencion"));
                a.setDetallederivacion(this.cnn.getRst().getString("anestesia"));
                a.setDomicilio(this.cnn.getRst().getString("requerimientos"));
                a.setEdadpaciente(this.cnn.getRst().getString("observacion"));
                a.setFecha_registro(this.cnn.getRst().getDate("fecha_registro"));
                a.setFormatofecha(this.cnn.getRst().getString("fechastring"));
                va.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    public Vector<atencion> BuscarSolicitudesporRangodeFecha(Date fecha1, Date fecha2, String doctor, int especialidad) {

        String resto = "";
        if (especialidad != -1) {
            resto = resto + " and e.id_especialidad =  " + especialidad + "";
        }

        if (!doctor.equals("-1")) {
            resto = resto + "and d.rut='" + doctor + "'";
        }

        Vector<atencion> va = new Vector<atencion>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "e.nombreespecialidad,\n"
                + "d.rut ||' '|| d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as doctor,\n"
                + "to_char (sc.fecha_control, 'dd/mm/yyyy') as fecha,\n"
                + "sc.motivo_control,\n"
                + "pac.rut ||' '|| pac.nombre ||' '||pac.apellido_paterno ||' '|| pac.apellido_moderno as paciente\n"
                + "\n"
                + "FROM \n"
                + "  agenda.solicitud_control sc inner join \n"
                + "  agenda.cita c on c.id_cita = sc.idcita \n"
                + "  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.especialidades e on e.id_especialidad = p.id_especialidad\n"
                + "  join agenda.doctor d on d.rut = p.rut_doctor\n"
                + "  join agenda.paciente pac on pac.rut = c.rut_paciente where sc.fecharegistro ::date \n"
                + "  BETWEEN '" + fecha1 + "' and '" + fecha2 + "' " + resto + " ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                atencion a = new atencion();
                a.setEspecialidad(this.cnn.getRst().getString("nombreespecialidad"));
                a.setProfesion(this.cnn.getRst().getString("doctor"));

                a.setFormatofecha(this.cnn.getRst().getString("fecha"));
                a.setDetallereceta(this.cnn.getRst().getString("motivo_control"));
                a.setRutpaciente(this.cnn.getRst().getString("paciente"));
                va.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return va;
    }

    public atencion buscarControlporIdcita(int idcita) {

        atencion a = new atencion();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT idcontrol, to_char( fecha_control,'dd/mm/yyyy') as fecha_control, \n"
                + "       motivo_control\n"
                + "  FROM agenda.solicitud_control where idcita= " + idcita + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                a.setFormatofecha(this.cnn.getRst().getString("fecha_control"));
                a.setDetallereceta(this.cnn.getRst().getString("motivo_control"));
                a.setIdatencion(this.cnn.getRst().getInt("idcontrol"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return a;
    }


    /*contador de nuevos, controles, nsp controles, nsp nuevo*/
    public int BuscarContadores(int especialidad, String rutdoctor, int mes, String atencion, String estatuscita) {
        int cantidad = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "     count(c.id_cita) as cuantas\n"
                + "FROM \n"
                + "  agenda.cita C inner join agenda.oferta o \n"
                + "  on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar p \n"
                + "  on p.id_planificacion = o.id_plani_sobre\n"
                + "   join agenda.doctor d on d.rut\n"
                + "    = p.rut_doctor\n"
                + "   join agenda.especialidades es on es.id_especialidad = p.id_especialidad \n"
                + "    \n"
                + "     where  p.id_atencion in (" + atencion + ") and es.id_especialidad = " + especialidad + " and date_part('year',p.fecha) = '2018' \n"
                + "     and  p.rut_doctor='" + rutdoctor + "' and c.estatus in (" + estatuscita + ") and date_part('month'::text, p.fecha) = " + mes + "\n"
                + "     group by\n"
                + "     p.rut_doctor, p.id_atencion, date_part('month'::text, p.fecha) ,date_part('year',p.fecha), es.id_especialidad, d.nombre ,\n"
                + " d.apellido_paterno ,es.nombreespecialidad ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cantidad = this.cnn.getRst().getInt("cuantas");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return cantidad;
    }

    /*Consultas para Informe contador Atenciones Agenda antigua*/
    public Vector<doctor> buscarTodaslasatencionesAntigua() {

        Vector<doctor> doctoresconiniciodesesion = new Vector<doctor>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT e.nombre_especialidad_medica,\n"
                + "     f.nombre_doctor, f.apellido_doctor,\n"
                + "    C.id_doctor_cita_paciente, c.id_especialidad_cita_paciente\n"
                + "     from schema_agenda.cita_paciente C inner join \n"
                + "     schema_rrhh.especialidad_medica E on e.id_especialidad_medica =c.id_especialidad_cita_paciente\n"
                + "     join doctor_temp f on  f.id_doctor = c.id_doctor_cita_paciente\n"
                + "      WHERE \n"
                + "     C.tipo_cita_cita_paciente in (1,2) and   date_part('year', C.fecha_citacion_cita_paciente) ='2018' \n"
                + "     \n"
                + "      group by  c.id_doctor_cita_paciente, c.id_especialidad_cita_paciente,\n"
                + "      nombre_especialidad_medica, nombre_doctor, apellido_doctor");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                doctor d = new doctor();
                d.setRut(this.cnn.getRst().getString("id_doctor_cita_paciente"));
                d.setNombre(this.cnn.getRst().getString("nombre_doctor"));
                d.setApellido_paterno(this.cnn.getRst().getString("apellido_doctor"));
                d.setApellido_materno(this.cnn.getRst().getString("nombre_especialidad_medica"));
                d.setEstatus(this.cnn.getRst().getInt("id_especialidad_cita_paciente"));

                doctoresconiniciodesesion.add(d);
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return doctoresconiniciodesesion;

    }

    /*contador de nuevos, controles, nsp controles, nsp nuevo*/
    public int BuscarContadoresantigua(int especialidad, String rutdoctor, int mes, String atencion, String estatuscita) {
        int cantidad = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT  COUNT (C.id_cita_paciente) as cuantas\n"
                + "     from schema_agenda.cita_paciente C inner join \n"
                + "     schema_rrhh.especialidad_medica E on e.id_especialidad_medica =c.id_especialidad_cita_paciente\n"
                + "     join doctor_temp f on  f.id_doctor = c.id_doctor_cita_paciente\n"
                + "      WHERE \n"
                + "     C.tipo_cita_cita_paciente in (" + atencion + ") and   date_part('year', C.fecha_citacion_cita_paciente) ='2018' and\n"
                + "      c.estado_cita_paciente in (" + estatuscita + ") and f.id_doctor= '" + rutdoctor + "' \n"
                + "      and date_part('month'::text,  C.fecha_citacion_cita_paciente) = " + mes + " and\n"
                + "   c.id_especialidad_cita_paciente=" + especialidad + "\n"
                + "      group by  c.id_doctor_cita_paciente, c.id_especialidad_cita_paciente,date_part('month'::text,  C.fecha_citacion_cita_paciente), date_part('year', C.fecha_citacion_cita_paciente),\n"
                + "      nombre_especialidad_medica, nombre_doctor");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cantidad = this.cnn.getRst().getInt("cuantas");
            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return cantidad;
    }

    /*ingresar rut para estadistico*/
    public void ingresarRut(String rut) {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO rut(\n"
                + "            rut)\n"
                + "    VALUES ('"+rut+"');");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
      
    }

     /*ELIMINAR rut para estadistico*/
    public void ELIMINARRut() {
        this.configurarConexion("");

        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("DELETE FROM rut;");
        this.cnn.conectar();

        this.cnn.cerrarConexion();
      
    }
    
}
