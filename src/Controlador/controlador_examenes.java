/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.cita;
import Modelos.examenes_grupos;
import Modelos.lugar;
import Modelos.paciente;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;

/**
 *
 * @author a
 */
public class controlador_examenes extends General {

    public String BuscarExamenesdeunaAtencion(int idatencion) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " nombre_examenes\n"
                + "FROM \n"
                + "  agenda.atencion_examen_laboratorio ael inner join\n"
                + "  agenda.atencion_grupoexamen_laboratorio agl on\n"
                + "  ael.id_atencion = agl.id_atencion_laboratorio\n"
                + "  join agenda.grupo_examenes_laboratorio gel on\n"
                + "  gel.id_examenes_laboratorio = agl.id_grupo_examenes_laboratorio\n"
                + "  join agenda.examenes e on e.id_examenes = gel.idexamen \n"
                + "  where id_atencion = " + idatencion + ";");
        this.cnn.conectar();
        try {
            int x = 1;
            while (this.cnn.getRst().next()) {
                if (x == 1) {
                    mensaje = x + "-" + this.cnn.getRst().getString("nombre_examenes");
                } else {
                    mensaje = mensaje + " " + x + "-" + this.cnn.getRst().getString("nombre_examenes");
                }
                ++x;
            }
        } catch (SQLException ex) {

        }
        
        this.cnn.cerrarConexion();
        return mensaje;
        
    }

    public lugar AtencionExamenesdeLaboratorioporidAtencion(int idatencion) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion,\n"
                + "  id_cita,\n"
                + " to_char(fecha , 'HH24:MI:SS') as horaatencion , \n"
                + "  usuario,\n"
                + "  ipusuario,\n"
                + "  observacion\n"
                + "FROM \n"
                + "  agenda.atencion_examen_laboratorio where id_atencion = " + idatencion + " ;");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setHora(this.cnn.getRst().getString("horaatencion"));

                l.setObservaciones(this.cnn.getRst().getString("observacion"));

            }
        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

        return l;

    }

    /*Buscar los examenes de prioridad 1 para pintar la pantalla */
    public Vector<examenes_grupos> buscarExamesparaPantallas(int prioridad, int grupo) {
        Vector<examenes_grupos> eg = new Vector<examenes_grupos>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_examenes_laboratorio,\n"
                + "  \n"
                + "    ge.descripcion,\n"
                + "    e.nombre_examenes,\n"
                + "   ge.idgrupo,\n"
                + "  orden,\n"
                + "  prioridad,\n"
                + "  color\n"
                + "FROM \n"
                + "  agenda.grupo_examenes_laboratorio gel inner join\n"
                + "  agenda.grupo_examenes ge on ge.idgrupo = gel.id_grupoexamen\n"
                + "  join agenda.examenes e on e.id_examenes = gel.idexamen \n"
                + "  where prioridad = " + prioridad + " and gel.id_grupoexamen= " + grupo + " \n"
                + "  and gel.estatus = 1 order by id_grupoexamen , gel.orden asc;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                examenes_grupos e = new examenes_grupos();
                e.setId_examengrupo(this.cnn.getRst().getInt("id_examenes_laboratorio"));
                e.setDescripcionexamen(this.cnn.getRst().getString("nombre_examenes"));
                e.setDescripciongrupo(this.cnn.getRst().getString("descripcion"));
                e.setOrden(this.cnn.getRst().getInt("orden"));
                e.setPrioridad(this.cnn.getRst().getInt("prioridad"));
                e.setColortuboexamen(this.cnn.getRst().getString("color"));
                eg.add(e);
            }

        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return eg;

    }

    public int insertarElectrocardiograma(int idcita) throws UnknownHostException {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.electrocardiograma\n"
                + "(\n"
                + "  idcita,\n"
                + "  usuario,\n"
                + "  ipusuario,\n"
                + "  fecha_atencion\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + idcita + ",\n"
                + "  '" + getUsuarioLoggin() + "',\n"
                + "  '" + getIpUsuarioLoggin() + "',\n"
                + "  CURRENT_TIMESTAMP\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }

        this.cnn.cerrarConexion();
        return exito;
    }

    /*buscar los grupos para formar la pantalla*/
    public Vector<examenes_grupos> buscarGrupoPantallas(int prioridad) {
        Vector<examenes_grupos> eg = new Vector<examenes_grupos>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select ge.idgrupo , ge.descripcion from agenda.grupo_examenes ge inner join \n"
                + "  agenda.grupo_examenes_laboratorio gel on \n"
                + "  ge.idgrupo = gel.id_grupoexamen where \n"
                + "  gel.prioridad = " + prioridad + " and ge.estatus = 1 group by  "
                + "ge.idgrupo,ge.descripcion\n"
                + "  order by ge.idgrupo asc;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                examenes_grupos e = new examenes_grupos();
                e.setIdgrupo(this.cnn.getRst().getInt("idgrupo"));
                e.setDescripciongrupo(this.cnn.getRst().getString("descripcion"));
                eg.add(e);
            }

        } catch (SQLException ex) {

        }
        this.cnn.cerrarConexion();
        return eg;

    }

    /*insertar los registros*/
    public int guardarAtenciondeExamenes(examenes_grupos eg) throws UnknownHostException {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO "
                + "  agenda.atencion_examen_laboratorio\n"
                + "(\n"
                + "  id_cita,\n"
                + "  fecha,\n"
                + "  usuario,\n"
                + "  ipusuario,\n"
                + "  observacion\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + eg.getIdcita() + ",\n"
                + "  CURRENT_TIMESTAMP,\n"
                + "  '" + getUsuarioLoggin() + "',\n"
                + "  '" + getIpUsuarioLoggin() + "',\n"
                + "  '" + eg.getObservacion() + "'\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }

        this.cnn.cerrarConexion();
        return exito;
    }

    public int buscarIdAtencionLaboratorioporCita(int cita) {
        int idcita = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion\n"
                + "  FROM \n"
                + "  agenda.atencion_examen_laboratorio where id_cita = " + cita + ";");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                idcita = this.cnn.getRst().getInt("id_atencion");
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();
        return idcita;

    }

    /*insertar los examnes que se realizaron en una atencion*/
    public int ingresarExamenaAtencion(int idatencion, int idexamen) {
        int exito = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO \n"
                + "  agenda.atencion_grupoexamen_laboratorio\n"
                + "(\n"
                + "  id_atencion_laboratorio,\n"
                + "  id_grupo_examenes_laboratorio\n"
                + ") \n"
                + "VALUES (\n"
                + "  " + idatencion + ",\n"
                + "  " + idexamen + "\n"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            exito = 1;
        }

        this.cnn.cerrarConexion();
        return exito;
    }

    public Vector<cita> buscarPacientesElectrocardiogramas(Date fechainicio, Date fechafin) {

        Vector<cita> vc = new Vector<cita>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT \n"
                + "  to_char(pla.fecha, 'dd/mm/yyyy') as fecha,\n"
                + "  p.rut, p.nombre,p.apellido_paterno, p.apellido_moderno, \n"
                + "  case when p.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "  to_char(age(timenow(), p.fecha_nacimiento),'yy') as edad,\n"
                + "   vf.nombre as responsable,\n"
                + "   to_char(c.fecha_recepcion,'HH24:MI:SS' ) as horarecepcion,\n"
                + "   to_char(el.fecha_atencion,'HH24:MI:SS')as horaatencion\n"
                + "  FROM \n"
                + "  \n"
                + "  agenda.cita c \n"
                + "  inner join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar pla on pla.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.paciente p on p.rut = c.rut_paciente\n"
                + "  join agenda.electrocardiograma el on el.idcita = c.id_cita\n"
                + "  join agenda.vistafuncionario vf on vf.rut_funcionario = el.usuario\n"
                + "  where pla.id_especialidad = 76 and pla.fecha :: date  BETWEEN '" + fechainicio + "' and '" + fechafin + "'\n"
                + "  and c.estatus = 4;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("fecha"));
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales1(this.cnn.getRst().getString("nombre"));
                c.setTemporales2(this.cnn.getRst().getString("apellido_paterno"));
                c.setTemporales3(this.cnn.getRst().getString("apellido_moderno"));
                c.setTemporales4(this.cnn.getRst().getString("genero"));
                c.setTemporales5(this.cnn.getRst().getString("edad"));
                c.setTemporales6(this.cnn.getRst().getString("responsable"));
                c.setMotivo(this.cnn.getRst().getString("horarecepcion"));
                c.setMotivo_cancela(this.cnn.getRst().getString("horaatencion"));
                vc.add(c);
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();
        return vc;
    }

    public Vector<cita> buscarPacientesconExamenes(Date fechainicio, Date fechafin) {

        Vector<cita> vc = new Vector<cita>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT \n"
                + "  to_char(pla.fecha, 'dd/mm/yyyy') as fecha,\n"
                + "  p.rut, p.nombre,p.apellido_paterno, p.apellido_moderno, \n"
                + "  case when p.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "  to_char(age(timenow(), p.fecha_nacimiento),'yy') as edad,\n"
                + "   vf.nombre as responsable,\n"
                + "   to_char(c.fecha_recepcion,'HH24:MI:SS' ) as horarecepcion,\n"
                + "   to_char(el.fecha,'HH24:MI:SS')as horaatencion, el.id_atencion  , el.observacion, "
                + "case when  p.provision= 1 then 'FONASA' || ' '||t.nombre else case when p.provision = 2 then 'Isapre' else case when p.provision = 3 then 'Prais' else 'Sin Prevision' end  end end as prevension,\n"
                + " case when c.ges = 1 then 'Ges' else 'No Ges' end as ges\n"
                + "FROM \n"
                + "  \n"
                + "  agenda.cita c \n"
                + "  inner join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "  join agenda.planificar pla on pla.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.paciente p on p.rut = c.rut_paciente\n"
                + "  join agenda.atencion_examen_laboratorio el on el.id_cita = c.id_cita\n"
                + "  join agenda.vistafuncionario vf on vf.rut_funcionario = el.usuario"
                + "  join agenda.tramo t on t.id= p.tramo \n"
                + "  where pla.id_especialidad = 65 and pla.fecha :: date\n"
                + "    BETWEEN '" + fechainicio + "' and '" + fechafin + "'\n"
                + "  and c.estatus = 4;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("fecha"));
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales1(this.cnn.getRst().getString("nombre"));
                c.setTemporales2(this.cnn.getRst().getString("apellido_paterno"));
                c.setTemporales3(this.cnn.getRst().getString("apellido_moderno"));
                c.setTemporales4(this.cnn.getRst().getString("genero"));
                c.setTemporales5(this.cnn.getRst().getString("edad"));
                c.setTemporales6(this.cnn.getRst().getString("responsable"));
                c.setMotivo(this.cnn.getRst().getString("horarecepcion"));
                c.setMotivo_cancela(this.cnn.getRst().getString("horaatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("observacion"));
                c.setId_cita(this.cnn.getRst().getInt("id_atencion"));
                c.setTemporales7(this.cnn.getRst().getString("prevension"));
                c.setTemporales8(this.cnn.getRst().getString("ges"));
                vc.add(c);
            }
        } catch (SQLException ex) {

        }

        this.cnn.cerrarConexion();
        return vc;
    }

}
