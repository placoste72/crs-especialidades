/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.atencion;
import Modelos.cita;
import Modelos.lugar;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Informatica
 */
public class controlador_cita extends General {

    /*buscar una el estatus de una cita para hoy de un paciente ojo la fecha del cita no es necesariamente 
    la del registor de la cita es la fecha de la planificacion del doctor*/
    public Vector<lugar> buscarEstatusPaciente(String rut) {
        Vector<lugar> estatus = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT c.estatus FROM  agenda.cita c\n"
                + "inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + " join agenda.planificar p on p.id_planificacion = o.id_plani_sobre  and p.fecha = CURRENT_DATE\n"
                + "  where upper( c.rut_paciente)=upper('" + rut + "') ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar l = new lugar();
                l.setEstatu(this.cnn.getRst().getInt("estatus"));
                estatus.add(l);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return estatus;

    }

    public cita buscardatosdeSobrecupoParaCorreo(int id_cita) {
        cita c = new cita();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select \n"
                + " d.rut || '/' || d.nombre ||' '|| d.apellido_paterno as doctor,  \n"
                + "  to_char(pl.fecha, 'dd/mm/yyyy' ) as fechacita ,\n"
                + " p.rut || '/' || p.nombre ||' '|| p.apellido_paterno as paciente,\n"
                + " o.hora ,\n"
                + " c.usuario, (select re.nombre ||' '|| re.apellidop  from seguridad.registrousuario re where re.rut = c.usuario ) as nombreusuario,\n"
                + " to_char(c.fecha, 'dd/mm/yyyy' ) as fecha \n"
                + " from agenda.cita c inner join agenda.paciente p on p.rut = c.rut_paciente\n"
                + " join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + " join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + " join agenda.doctor d on d.rut = pl.rut_doctor\n"
                + "  where c.id_cita = " + id_cita + "");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setTemporales(this.cnn.getRst().getString("fechacita"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("hora"));
                c.setTemporales2(this.cnn.getRst().getString("usuario"));
                c.setTemporales3(this.cnn.getRst().getString("fecha"));
                c.setTemporales4(this.cnn.getRst().getString("nombreusuario"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return c;
    }

    /*buscar si el paciente tiene unas alertas proximas*/
    public Vector<cita> citasproximasdeunpaciente(String rut) {
        Vector<cita> vc = new Vector<cita>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select to_char(p.fecha, 'dd/mm/yyyy')as fecha, o.hora , e.nombreespecialidad,  a.nombre ,\n"
                + "d.nombre ||' '|| d.apellido_paterno as doctor\n"
                + " from agenda.cita c inner join \n"
                + "agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "join agenda.atencion a on p.id_atencion = a.id_atencion\n"
                + "join agenda.especialidades e on e.id_especialidad = p.id_especialidad\n"
                + "join agenda.doctor d on d.rut = p.rut_doctor\n"
                + " where c.rut_paciente ='" + rut + "'\n"
                + "and (c.estatus = 1 or c.estatus = 2) and p.fecha >= CURRENT_DATE order by p.fecha desc ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("fecha"));
                c.setTemporales1(this.cnn.getRst().getString("hora"));
                c.setTemporales2(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("nombre"));
                c.setTemporales4(this.cnn.getRst().getString("doctor"));
                vc.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return vc;

    }

    /*Ingresar cita con usuarios y ip de quien registra*/
    public boolean ingresarcita(cita c) throws UnknownHostException {
        boolean mensaje = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL(" INSERT INTO \n"
                + "  agenda.cita\n"
                + "( id_oferta, id_tipoatencion, ges,id_patologia, rut_paciente,estatus,confirmacion_cita, motivo, motivo_cancela, fecha ,ipusuario, usuario,iddiagnostico_especialidad, id_examenesespecialidad, id_procedimientoespecialidad) \n"
                + "VALUES (" + c.getId_oferta() + ", " + c.getId_tipoatencion() + ", " + c.getGes() + "," + c.getId_patologia() + ",'" + c.getRut_paciente() + "', " + c.getEstatus() + ", 0, '" + c.getMotivo() + "','motivo',  current_date, '" + getIpUsuarioLoggin() + "', '" + getUsuarioLoggin() + "' , " + c.getIddiagnosticoespecialdad() + " , " + c.getExamen() + "," + c.getProcedimiento() + ");");

        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = true;
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*esta mostrando informacion null*/
    public boolean enviarMensajeporSobrecupo(int id_cita) {
        cita c = buscardatosdeSobrecupoParaCorreo(id_cita);
        boolean mensaje = false;
        if (!c.getRut_doctor().equalsIgnoreCase(null)) {
            String destinos = buscaremaildeAdministradores();
            String cuerpo2 = "<html>\n"
                    + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/banner.JPG\" >  \n"
                    + "  <h1 style=\"color: #0059b3\">Sres Administradores. </h1>\n"
                    + "  <p> Se les informa que se ha Asignado un  SobreCupo </p>\n"
                    + "  <p> Del Profesional : " + c.getRut_doctor() + " a las " + c.getTemporales1() + " del dia " + c.getTemporales() + " <br></p>\n"
                    + "  <p> Para el Paciente : " + c.getRut_paciente() + ". <br> Este Sobrecupo lo creo el Usuario " + c.getTemporales2() + " - " + c.getTemporales4() + " del dia " + c.getTemporales3() + " </p>\n"
                    + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/footer.JPG\" >  \n"
                    + "</html>";
            String asuntos = "Asignacion de Sobrecupo a Paciente";
            mensaje = enviarEmailNotificacion(asuntos, destinos, cuerpo2);
        }
        return mensaje;

    }

    public Vector<cita> Ultimas5Citasdeunpaciente(String rutPaciente) {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "   c.id_cita,\n"
                + "    case when c.motivo= 'null' then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                   inner join   agenda.diagnostico_especialidad des\n"
                + "                   on di.iddiagnostico = des.iddiagnostico\n"
                + "                   where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else c.motivo\n"
                + "                   end as motivo,\n"
                + "  to_char(p.fecha,'DD/MM/YYYY') as fecha,\n"
                + "  o.hora,\n"
                + "  e.nombreespecialidad,\n"
                + "  (d.nombre  ||' ' || d.apellido_paterno  ||' ' || d.apellido_materno) as doctor,\n"
                + "  t.descripcion\n"
                + "\n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join  agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "  join agenda.especialidades e on p.id_especialidad = e.id_especialidad\n"
                + "  join agenda.doctor d on p.rut_doctor = d.rut \n"
                + "  join agenda.tipo_atencion t on c.id_tipoatencion = t.id_tipoatencion \n"
                + "  \n"
                + "  where  upper(c.rut_paciente) = upper('" + rutPaciente + "') order by p.fecha desc \n"
                + " limit 5  ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));

                c.setRut_doctor(this.cnn.getRst().getString("doctor"));

                c.setTemporales3(this.cnn.getRst().getString("fecha"));

                c.setTemporales1(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales2(this.cnn.getRst().getString("hora"));
                c.setMotivo(this.cnn.getRst().getString("motivo"));
                c.setTemporales(this.cnn.getRst().getString("descripcion"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*buscar la especialidad*/
    public int buscarIddeEspecialidaddeCita(int idCita) {
        int idespecialidad = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  e.id_especialidad\n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join  agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "  join agenda.especialidades e on p.id_especialidad = e.id_especialidad\n"
                + "  where  c.id_cita= " + idCita + " ;");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                idespecialidad = this.cnn.getRst().getInt("id_especialidad");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return idespecialidad;
    }

    /*Lista de citas de pacientes ya atendidos*/
    public Vector<cita> buscarcitasAtendidas() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_cita,\n"
                + "  pc.rut,\n"
                + " (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + " case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + " to_char(age(CURRENT_DATE, pc.fecha_nacimiento),'yy')as edad,\n"
                + "  e.nombreespecialidad,\n"
                + "  a.nombre as tipoatencion,\n"
                + " ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "   (p.fecha || ' ' || o.hora )as fechahora \n"
                + "\n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta  \n"
                + "  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "  join agenda.atencion a on p.id_atencion = a.id_atencion  where \n"
                + "   p.fecha = CURRENT_DATE and c.estatus = 5   and (e.id_especialidad=19) order by  p.fecha ,  o.hora  ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;
    }

    /*para oftalmologia*/
    public Vector<cita> buscarcitasConfirmadasparaHoy() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT \n"
                + "                  id_cita,\n"
                + "                  pc.rut,\n"
                + "                 (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "                 case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "                to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                  e.nombreespecialidad,\n"
                + "                 a.nombre as tipoatencion,\n"
                + "                 ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "                   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "                     case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo, c.estatus , to_char(c.fecha_recepcion , 'HH24:MI:SS') as fecharecepcion  \n"
                + "               FROM \n"
                + "                 agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "                 join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "                 join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                 join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "                  join agenda.atencion a on p.id_atencion = a.id_atencion\n"
                + "                    where \n"
                + "                   c.confirmacion_cita = 1 and (c.estatus = 2 or c.estatus = 7) and (e.id_especialidad=19) and p.fecha= CURRENT_DATE order by  p.fecha ,  o.hora  ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setTemporales5(this.cnn.getRst().getString("fecharecepcion"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*lista de pacientes para enfermeria anulados por enfermeria*/
 /*Lista de Pacientes para Enfermeria*/
    public Vector<cita> buscarcitasAnuladasparaHoyEnfermeriaTens() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   id_cita,\n"
                + "                  pc.rut,\n"
                + "                 (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "                 case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "                 to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                  e.nombreespecialidad, e.id_especialidad ,\n"
                + "                  a.nombre as tipoatencion,\n"
                + "                 ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "                   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "                   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                     inner join   agenda.diagnostico_especialidad des\n"
                + "                     on di.iddiagnostico = des.iddiagnostico\n"
                + "                     where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + "                 FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , c.estatus  , c.fecha_recepcion :: TIME as horarecepcion \n"
                + "               FROM \n"
                + "                agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "                  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "                 join agenda.atencion a on p.id_atencion = a.id_atencion  \n"
                + "                   where \n"
                + "                 ( c.estatus = 3 ) and  p.fecha= CURRENT_DATE and a.id_atencion <> 3 and  e.id_especialidad=46 order by  o.hora asc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));
                c.setTemporales5(this.cnn.getRst().getString("horarecepcion"));
                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*Lista de Pacientes para Enfermeria*/
    public Vector<cita> buscarcitasConfirmadasparaHoyEnfermeriaTens() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   id_cita,\n"
                + "                  pc.rut,\n"
                + "                 (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "                 case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "                 to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                  e.nombreespecialidad, e.id_especialidad ,\n"
                + "                  a.nombre as tipoatencion,\n"
                + "                 ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "                   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "                   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                     inner join   agenda.diagnostico_especialidad des\n"
                + "                     on di.iddiagnostico = des.iddiagnostico\n"
                + "                  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + "                 FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo, c.estatus  , c.fecha_recepcion :: TIME as horarecepcion \n"
                + "               FROM \n"
                + "                agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "                  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "                 join agenda.atencion a on p.id_atencion = a.id_atencion  \n"
                + "                 where \n"
                + "                 ( c.estatus = 2 ) and  p.fecha= CURRENT_DATE  and  e.id_especialidad=46 order by  o.hora asc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));
                c.setTemporales5(this.cnn.getRst().getString("horarecepcion"));
                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*para atencion de radiografia*/
    public Vector<cita> buscarcitasConfirmadasparaRadiologia() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   id_cita,\n"
                + "                  pc.rut,\n"
                + "                 (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "                 case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "                 to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                  e.nombreespecialidad, e.id_especialidad ,\n"
                + "                  a.nombre as tipoatencion,\n"
                + "                 ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "                   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "                   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                 inner join   agenda.diagnostico_especialidad des\n"
                + "                 on di.iddiagnostico = des.iddiagnostico\n"
                + "                  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + "             FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , c.estatus \n"
                + "               FROM \n"
                + "                agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "                  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "                 join agenda.atencion a on p.id_atencion = a.id_atencion  \n"
                + "                 where \n"
                + "                 ( c.estatus = 2 ) and  p.fecha= CURRENT_DATE and  e.id_especialidad = 40 and  p.rut_doctor ='" + getUsuarioLoggin() + "' order by  o.hora desc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*pacientes para oftalmologia*/
    public Vector<cita> buscarcitasConfirmadasparaOftalmologia() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   id_cita,\n"
                + "                  pc.rut,\n"
                + "                 (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "                 case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "                 to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                  e.nombreespecialidad, e.id_especialidad ,\n"
                + "                  a.nombre as tipoatencion,\n"
                + "                 ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "                   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "                   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                  inner join   agenda.diagnostico_especialidad des\n"
                + "                 on di.iddiagnostico = des.iddiagnostico\n"
                + "                 where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + "                 FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , c.estatus \n"
                + "               FROM \n"
                + "                agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "                  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "                 join agenda.atencion a on p.id_atencion = a.id_atencion  \n"
                + "                  where \n"
                + "                 ( c.estatus = 2 or c.estatus = 4 ) and   p.fecha= CURRENT_DATE and  e.id_especialidad in (1,14,24)  and  p.rut_doctor ='" + getUsuarioLoggin() + "' and a.id_atencion <> 3 order by  o.hora asc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*para todas las especialidades que no tienen especificaciones solo que el paciente este recepcionado*/
    public Vector<cita> buscarcitasConfirmadasparaHoyEspecialidades() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   id_cita,\n"
                + "                  pc.rut,\n"
                + "                 (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "                 case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "                 to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                  e.nombreespecialidad, e.id_especialidad ,\n"
                + "                  a.nombre as tipoatencion,\n"
                + "                 ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "                   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "                   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , c.estatus \n"
                + "               FROM \n"
                + "                agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "                  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "                 join agenda.atencion a on p.id_atencion = a.id_atencion  \n"
                + "                 where \n"
                + "                 ( c.estatus = 2 ) and  p.fecha= CURRENT_DATE and a.id_atencion <> 3 and e.id_especialidad <> 19 and  p.rut_doctor ='" + getUsuarioLoggin() + "' order by  o.hora desc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*buscar Pacientes confirmados para Examenes de Laboratorio y Electrocardiograma*/
    public Vector<cita> BuscarPacientesRecepcionadosparaunaEspecialidad(int especialidad) {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   id_cita,\n"
                + "     pc.rut,\n"
                + "    (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "     case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "    to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "   e.nombreespecialidad, e.id_especialidad ,\n"
                + "  a.nombre as tipoatencion,\n"
                + "   ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "  (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , c.estatus , c.fecha_recepcion :: TIME as horarecepcion\n"
                + "    FROM \n"
                + "    agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "   join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "    join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "   join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "   join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "   join agenda.atencion a on p.id_atencion = a.id_atencion  \n"
                + "    where \n"
                + "    (c.estatus = 2)and  p.fecha= CURRENT_DATE  and  p.id_especialidad= " + especialidad + "  order by  o.hora asc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));
                c.setTemporales5(this.cnn.getRst().getString("horarecepcion"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;
    }

    /**
     * Buscar atencion para entrevista preoperatoria
     */
    /*para todas las especialidades que no tienen especificaciones solo que el paciente este recepcionado*/
    public Vector<cita> buscarcitasVistasporelTensparahoy() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   id_cita,\n"
                + "                  pc.rut,\n"
                + "                 (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + "                 case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "                 to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                  e.nombreespecialidad, e.id_especialidad ,\n"
                + "                  a.nombre as tipoatencion,\n"
                + "                 ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "                   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "                   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + "                  inner join   agenda.diagnostico_especialidad des\n"
                + "                 on di.iddiagnostico = des.iddiagnostico\n"
                + "                  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + "                 FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , c.estatus , c.fecha_recepcion :: TIME as horarecepcion\n"
                + "               FROM \n"
                + "                agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta \n"
                + "                  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "                  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "                  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "                 join agenda.atencion a on p.id_atencion = a.id_atencion  \n"
                + "                   where \n"
                + "                 (c.estatus = 5 or c.estatus = 8)and  p.fecha= CURRENT_DATE  and  p.rut_doctor ='" + getUsuarioLoggin() + "' order by  o.hora asc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));
                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));
                c.setTemporales5(this.cnn.getRst().getString("horarecepcion"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }


    /*para oftalmologia*/
    public Vector<cita> buscarcitasConfirmadasparaHoyTecnologo() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_cita,\n"
                + "  pc.rut,\n"
                + " (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + " case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + " to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "  e.nombreespecialidad,\n"
                + "  a.nombre as tipoatencion,\n"
                + " ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "    case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo , c.estatus \n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta  \n"
                + "  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "  join agenda.atencion a on p.id_atencion = a.id_atencion  "
                + "   where \n"
                + "  ( c.estatus = 5 OR c.estatus = 8 ) and (e.id_especialidad=19) and  p.fecha= CURRENT_DATE and p.rut_doctor ='" + getUsuarioLoggin() + "' order by  p.fecha ,  o.hora  ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                c.setEstatus(this.cnn.getRst().getInt("estatus"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*Buscar citas canceladas temporalmente oftalmologia*/
    public Vector<cita> buscarcitasCanceladaspornoestarparaHoy() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_cita,\n"
                + "  pc.rut,\n"
                + " (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + " case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "  to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "  e.nombreespecialidad,\n"
                + "  a.nombre as tipoatencion,\n"
                + " ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo  \n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta  \n"
                + "  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "  join agenda.atencion a on p.id_atencion = a.id_atencion "
                + "  where \n"
                + "  ( c.estatus = 3 or c.estatus = 9) and (e.id_especialidad=19) and p.fecha= CURRENT_DATE order by  p.fecha ,  o.hora  ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    public Vector<cita> buscarInformaciondeCitasporDiaoEspecialidadoProfesional(String where) {
        Vector<cita> bc = new Vector<cita>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT         e.nombreespecialidad,\n"
                + "               o.hora,\n"
                + "                p.rut ,\n"
                + "                 p.nombre  || '  ' || p.apellido_paterno || '  ' || p.apellido_moderno as nombre  ,\n"
                + "                       \n"
                + "                to_char(age(p.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "                case when  p.provision= 1 then 'FONASA' ||' '|| t.nombre  else case when p.provision = 2 then 'Isapre' else case when p.provision = 3 then 'Prais' else 'Sin Prevision' end  end end as prevension, \n"
                + "                a.nombre as tipoatencion,\n"
                + "                pl.fecha,\n"
                + "              \n"
                + "                d.rut ||'/'|| d.nombre ||' '||d.apellido_paterno as profesional,\n"
                + "                case when (pl.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'"
                + "                else case when c.estatus = 1 then 'Cita Asignada' \n"
                + "                else case when c.estatus = 0 then 'Cita Cancelada' \n"
                + "                ELSE case when c.estatus = 2 then 'Cita Recepcionada'\n"
                + "                Else case when c.estatus = 3 then 'Cita Recepcionada y cancelada'\n"
                + "                else case when c.estatus = 4 then 'Cita Terminada Satisfactoriamente'\n"
                + "                else case when c.estatus = 5 then 'Cita en Proceso'\n"
                + "               else 'Cita cancelada por el Profesional' end end end end end end end as estatus ,"
                + "               case when c.fecha_recepcion is null then ' ' else to_char(c.fecha_recepcion, 'HH24:MI:SS') end  as horaderecepcion , c.estatus as idestatus ,  case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo ,\n"
                + "                 case when o.procede = 1 then 'Cupo' else 'Sobre-Cupo' end as quees ,\n" +
"                      (case when c.usuario_recepciona is not null then (select ru.nombre ||' '||ru.apellidop from seguridad.registrousuario ru where ru.rut=c.usuario_recepciona) else '' end)as rece\n" +
"             \n"
                + "                FROM \n"
                + "                 agenda.cita c inner join agenda.paciente p on\n"
                + "                 c.rut_paciente = p.rut\n"
                + "                  join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "                  join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "                 join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "                 join agenda.doctor d on pl.rut_doctor = d.rut\n"
                + "                 join agenda.especialidades e on e.id_especialidad = pl.id_especialidad\n"
                + "                 join agenda.tramo t on t.id = p.tramo \n"
                + "                "
                + "                 where c.estatus <> 0 and  " + where + " ORDER BY o.hora asc, e.nombreespecialidad");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales1(this.cnn.getRst().getString("hora"));
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales2(this.cnn.getRst().getString("nombre"));
                c.setTemporales3(this.cnn.getRst().getString("edad"));
                c.setTemporales4(this.cnn.getRst().getString("prevension"));
                c.setTemporales5(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("profesional"));
                c.setMotivo(this.cnn.getRst().getString("estatus"));
                c.setMotivo_cancela(this.cnn.getRst().getString("horaderecepcion"));
                c.setEstatus(this.cnn.getRst().getInt("idestatus"));
                c.setTemporales6(this.cnn.getRst().getString("nombremotivo"));
                c.setTemporales7(this.cnn.getRst().getString("quees"));
                c.setTemporales19(this.cnn.getRst().getString("rece"));

                bc.add(c);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return bc;
    }

    /*Buscar citas canceladas temporalmente oftalmologia*/
    public Vector<cita> buscarcitasCanceladaspornoestarparaHoyTecnologo() {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_cita,\n"
                + "  pc.rut,\n"
                + " (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + " case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + "  to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "  e.nombreespecialidad,\n"
                + "  a.nombre as tipoatencion,\n"
                + " ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "   (p.fecha || ' ' || o.hora )as fechahora ,\n"
                + "   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as nombremotivo \n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta  \n"
                + "  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "  join agenda.atencion a on p.id_atencion = a.id_atencion"
                + "   where \n"
                + "  ( c.estatus = 6 or c.estatus = 10)  and (e.id_especialidad=19) and p.fecha= CURRENT_DATE and p.rut_doctor ='" + getUsuarioLoggin() + "' order by  p.fecha ,  o.hora  ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setTemporales4(this.cnn.getRst().getString("nombremotivo"));
                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*Busco citas de la agenda vieja */
    public Vector<cita> BuscarCitasdeunPacienteAgendaAntigua(String rut) {
        Vector<cita> listaantigua = new Vector();
        listaantigua = buscarCitasdeunPaciente(rut);
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  rut,\n"
                + "  idcita,\n"
                + "  doctor,\n"
                + "  apellidodoctor,\n"
                + "  especialidad,\n"
                + "  fechacita,\n"
                + "  horacita,\n"
                + "  tipocita,\n"
                + "  estadocitint,\n"
                + "  programa,\n"
                + "  destino,\n"
                + "  estadocita\n"
                + "FROM \n"
                + "  schema_agenda.citasanteriores where upper(rut)= upper('" + rut + "') ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("especialidad"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor") + " " + this.cnn.getRst().getString("apellidodoctor"));
                c.setTemporales5(this.cnn.getRst().getString("fechacita"));
                c.setTemporales4(this.cnn.getRst().getString("horacita"));
                c.setTemporales1(this.cnn.getRst().getString("tipocita"));
                c.setRut_paciente(this.cnn.getRst().getString("tipocita"));
                c.setTemporales2(this.cnn.getRst().getString("estadocita"));
                c.setTemporales3(this.cnn.getRst().getString("programa"));
                c.setEstatus(1);
                listaantigua.add(c);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return listaantigua;
    }

    public Vector<cita> buscarCitasdeunPaciente(String rut) {
        Vector<cita> lista = new Vector();
        /*voy a agregar funcion para buscar citas viejas */

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "        e.nombreespecialidad,\n"
                + "        (d.nombre  ||' ' || d.apellido_paterno  ||' ' || d.apellido_materno) as doctor,\n"
                + "         p.fecha,"
                + "         to_char(p.fecha, 'dd/mm/yyyy') as fechastring ,  \n"
                + "          o.hora,\n"
                + "           a.nombre as atencion,\n"
                + "          case when (p.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP' else\n"
                + "                          case when c.estatus= 1 then 'Cita Asignada' \n"
                + "                          else case when c.estatus = 0 then 'Cita Cancelada' else \n"
                + "                          case when c.estatus = 2 then 'Cita Recepcionada' \n"
                + "                          else case when c.estatus = 3 then 'Cita Cancelada Tens' else\n"
                + "                           case when c.estatus = 4 then 'Cita Atendida Satisfractoriamente' else\n"
                + "                           case when c.estatus = 6 then 'Cita Cancelada por Profesional' else \n"
                + "                            'Cita en Proceso' end end end end end end end as estatus,\n"
                + "          case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as motivo, \n"
                + "           prog.nombre ,"
                + "       COALESCE( ( select  ru.nombre ||' '|| ru.apellidop from agenda.cita ci \n"
                + "			 join seguridad.registrousuario ru\n"
                + "			 on ci.usuario_cancela = ru.rut where ci.id_cita = c.id_cita), '') as usuario_cancela,\n"
                + "			  c.motivo_cancela, c.estatus as intestatus, c.motivocancelaatencion\n"
                + "               FROM \n"
                + "                 agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "                 join  agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "                 join agenda.especialidades e on p.id_especialidad = e.id_especialidad\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut \n"
                + "                  join agenda.tipo_atencion t on c.id_tipoatencion = t.id_tipoatencion \n"
                + "                  join agenda.atencion a on a.id_atencion = p.id_atencion\n"
                + "                  join agenda.programa prog on prog.id_programa = p.id_programa\n"
                + "               \n"
                + "                where  upper(c.rut_paciente) = upper('" + rut + "') order by p.fecha desc ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setFecha(this.cnn.getRst().getDate("fecha"));
                c.setTemporales4(this.cnn.getRst().getString("hora"));
                c.setTemporales1(this.cnn.getRst().getString("atencion"));
                c.setTemporales2(this.cnn.getRst().getString("estatus"));
                c.setRut_paciente(this.cnn.getRst().getString("motivo"));
                c.setTemporales3(this.cnn.getRst().getString("nombre"));
                c.setTemporales5(this.cnn.getRst().getString("fechastring"));
                c.setEstatus(this.cnn.getRst().getInt("intestatus"));
                c.setUsuario_cancela(this.cnn.getRst().getString("usuario_cancela"));
                c.setTemporales6(this.cnn.getRst().getString("motivo_cancela"));
                c.setTemporales7(this.cnn.getRst().getString("motivocancelaatencion"));

                lista.add(c);

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /* buscar las citas de hoy en adelante de un paciente para confirmar o cancelar*/
    public Vector<cita> buscarcitasdePacientedeHoyenAdelante(String rutPaciente) {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  c.* , e.nombreespecialidad as especialidad ,\n"
                + "(d.rut || ' / ' || d.nombre || '  ' || d.apellido_paterno) as doctor,\n"
                + "(c.rut_paciente || ' / ' || p.nombre || '  ' || p.apellido_paterno) as paciente,\n"
                + "a.nombre, pt.descripcion as patologia , o.hora , to_char(pl.fecha,'dd/mm/yyyy') as fechacita  \n"
                + "FROM agenda.cita c\n"
                + "inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "join agenda.especialidades e on pl.id_especialidad = e.id_especialidad\n"
                + "join agenda.doctor d on pl.rut_doctor = d.rut \n"
                + "join agenda.paciente p on c.rut_paciente = p.rut\n"
                + "join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "join agenda.patologias pt on c.id_patologia = pt.id\n"
                + "where upper(c.rut_paciente) = upper('" + rutPaciente + "') and "
                + "pl.fecha = current_date and c.estatus = 1 and c.confirmacion_cita = 0   ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));

                c.setRut_doctor(this.cnn.getRst().getString("doctor"));

                c.setId_tipoatencion(this.cnn.getRst().getInt("id_tipoatencion"));
                c.setGes(this.cnn.getRst().getInt("ges"));
                c.setId_patologia(this.cnn.getRst().getInt("id_patologia"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setConfirmacion_cita(this.cnn.getRst().getInt("confirmacion_cita"));
                //c.setFecha(this.cnn.getRst().getDate("fechacita"));
                c.setTemporales15(this.cnn.getRst().getString("fechacita"));
                c.setTemporales(this.cnn.getRst().getString("especialidad"));
                c.setTemporales1(this.cnn.getRst().getString("nombre"));
                c.setTemporales2(this.cnn.getRst().getString("patologia"));
                c.setTemporales4(this.cnn.getRst().getString("hora"));
                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /*para otro*/
    public Vector<cita> buscarListaPacienteporDiaEspecialidadyDoctro2(int idplanificacion) {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT o.hora , p.rut, to_char(age(p.fecha_nacimiento),'yy')  as edad , p.nombre || ' ' || p.apellido_paterno || ' ' || p.apellido_moderno as nombre , \n"
                + "case when p.tramo!= 0 then pre.nombre || ' ' || (select t.nombre from agenda.tramo t where t.id = p.tramo) else pre.nombre end as prev,\n"
                + "a.nombre as tipoatencion \n"
                + "  FROM \n"
                + "  agenda.cita c inner join  agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "   join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "   join agenda.paciente p on p.rut = c.rut_paciente\n"
                + " join agenda.prevision pre on pre.id_prevision = p.provision \n"
                + "  where   pl.id_planificacion = " + idplanificacion + " and c.estatus <> 0 order by o.hora asc");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("hora"));
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales1(this.cnn.getRst().getString("edad"));
                c.setTemporales2(this.cnn.getRst().getString("nombre"));
                c.setTemporales3(this.cnn.getRst().getString("prev"));
                c.setTemporales4(this.cnn.getRst().getString("tipoatencion"));

                lista.add(c);
            }

        } catch (SQLException e) {

        }
        this.cnn.cerrarConexion();

        return lista;

    }

    public Vector<cita> buscarListaPacienteporDiaEspecialidadyDoctro(Date fecha1, Date fecha2, int especialidad, String rutdoctor) {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT o.hora , p.rut, to_char(age(p.fecha_nacimiento),'yy')  as edad , p.nombre || ' ' || p.apellido_paterno || ' ' || p.apellido_moderno as nombre , \n"
                + "                case when p.tramo!= 0 then pre.nombre || ' ' || (select t.nombre from agenda.tramo t where t.id = p.tramo) else pre.nombre end as prev,\n"
                + "                 a.nombre as tipoatencion \n"
                + "                 FROM \n"
                + "                  agenda.cita c inner join  agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "                  join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre                   join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "                   join agenda.paciente p on p.rut = c.rut_paciente\n"
                + "                 join agenda.prevision pre on pre.id_prevision = p.provision \n"
                + "               where   pl.fecha BETWEEN '" + fecha1 + "' and '" + fecha2 + "'    and  \n"
                + "                          pl.id_especialidad =" + especialidad + " and pl.rut_doctor='" + rutdoctor + "' and pl.estatus <> 0 and c.estatus <> 0 order by o.hora asc\n"
                + "               ");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("hora"));
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales1(this.cnn.getRst().getString("edad"));
                c.setTemporales2(this.cnn.getRst().getString("nombre"));
                c.setTemporales3(this.cnn.getRst().getString("prev"));
                c.setTemporales4(this.cnn.getRst().getString("tipoatencion"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;

    }

    /* Se buscan las citas del usuario desde la fecha de hoy en adelante no se puede cancelar una cita de ayer */
    public Vector<cita> buscarcitasParaCancelardePacientedeHoyenAdelante(String rutPaciente) {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT  c.* , e.nombreespecialidad as especialidad ,\n"
                + " (d.rut || ' / ' || d.nombre || d.apellido_paterno) as doctor,\n"
                + " (c.rut_paciente || ' / ' || p.nombre || p.apellido_paterno) as paciente,\n"
                + " a.nombre, pt.descripcion as patologia , to_char(pl.fecha, 'dd/mm/yyyy') as fechacita  , o.hora,\n"
                + " case when o.procede =1 then 'Cupo' else 'Sobre-Cupo' end as cs\n"
                + " FROM agenda.cita c\n"
                + " inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + " join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + " join agenda.especialidades e on pl.id_especialidad = e.id_especialidad\n"
                + " join agenda.doctor d on pl.rut_doctor = d.rut \n"
                + " join agenda.paciente p on c.rut_paciente = p.rut\n"
                + " join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "  join agenda.patologias pt on c.id_patologia = pt.id\n"
                + " where upper(c.rut_paciente) = upper('" + rutPaciente + "') and pl.fecha >= current_date \n"
                + "  and (c.estatus =1 or c.estatus =2 )");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));

                c.setRut_doctor(this.cnn.getRst().getString("doctor"));

                c.setId_tipoatencion(this.cnn.getRst().getInt("id_tipoatencion"));
                c.setGes(this.cnn.getRst().getInt("ges"));
                c.setId_patologia(this.cnn.getRst().getInt("id_patologia"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setConfirmacion_cita(this.cnn.getRst().getInt("confirmacion_cita"));
                c.setFecha(this.cnn.getRst().getDate("fecha"));
                c.setTemporales(this.cnn.getRst().getString("especialidad"));
                c.setTemporales1(this.cnn.getRst().getString("nombre"));
                c.setTemporales2(this.cnn.getRst().getString("patologia"));
                c.setTemporales3(this.cnn.getRst().getString("fechacita"));
                c.setTemporales4(this.cnn.getRst().getString("hora"));
                c.setTemporales5(this.cnn.getRst().getString("cs"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return lista;
    }

    /* buscar todas las  citas de un paciente*/
    public Vector<cita> buscarcitasdePaciente(String rutPaciente) {
        Vector<cita> lista = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.cita where upper(rut_paciente) = upper('" + rutPaciente + "') and estatus = 1 ;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));

                c.setRut_doctor(this.cnn.getRst().getString("rut_doctor"));

                c.setId_tipoatencion(this.cnn.getRst().getInt("tid_tipoatencion"));
                c.setGes(this.cnn.getRst().getInt("ges"));
                c.setId_patologia(this.cnn.getRst().getInt("id_patologia"));
                c.setRut_paciente(this.cnn.getRst().getString("rut_paciente"));
                c.setConfirmacion_cita(this.cnn.getRst().getInt("confirmacion_cita"));

                lista.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lista;

    }

    public int buscaridcitaconOferta(int oferta) {
        int id = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_cita FROM  agenda.cita where id_oferta =" + oferta + " and estatus = 1;");
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                id = this.cnn.getRst().getInt("id_cita");
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return id;
    }

    /*cambiar estatus de la cita a vista por el tecnico estatus=5*/
    public String CambiarCitapasoporTecnico(int id_cita) {
        String men = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 5 \n"
                + "WHERE \n"
                + "  id_cita = " + id_cita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            men = "Cita Atendida Exitosamente.";
        } else {
            men = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return men;

    }

    /*pasar cita a cancelada por no llegar a tiempo estatus = 3*/
    public String cambiarCitaAcanceladapornoEstarPresente(int idcita, String motivo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 3 ,\n"
                + "  motivocancelaatencion = '" + motivo + "' \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita Atendida Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*pasar cita en proceso  a cancelada por no llegar a tiempo estatus = 3*/
    public String cambiarCitaenProcesoAcanceladapornoEstarPresente(int idcita, String motivo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 9 ,\n"
                + "  motivocancelaatencion = '" + motivo + "' \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita Atendida Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*pasar cita a cancelada por no llegar a tiempo estatus = 6*/
    public String cambiarCitaAcanceladapornoEstarPresentepROFESIONAL(int idcita, String motivo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 6 ,\n"
                + "  motivocancelaatencion = '" + motivo + "' \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita Atendida Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*paso a 10 un a cita que esta siendo atendida por el tecnologo pero se cancela porque llamo y no esta presente */
    public String cambiarCitaenProcesoAcanceladapornoEstarPresentepROFESIONAL(int idcita, String motivo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 10 ,\n"
                + "  motivocancelaatencion = '" + motivo + "' \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita Atendida Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }


    /*pasar la cita a estatus = 4 Es paciente atendido*/
    public String cambiarCitaAatendida(int idcita) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 4 "
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita Atendida Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*cambiar cita a devuelta al tecnico con una atencion en proceso*/
    public String citadevueltaaTensconAtencionenProceso(int idcita) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 7 \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Estatus modificado.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }

    public String cambiarcitaaAtencionGuardadaperonoFinalizada(int idcita) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 8 "
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Estatus modificado.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;
    }


    /*buscar cita pero con datos del paciente y de la oferta*/
    public cita buscarCitaporIdParaAtencion(int idcita) {

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_cita,\n"
                + "  pc.rut,\n"
                + " (pc.nombre || ' ' ||  pc.apellido_paterno || ' ' ||  pc.apellido_moderno) as paciente,\n"
                + " case when pc.genero = 1 THEN 'FEMENINO' ELSE 'MASCULINO' end as  genero,\n"
                + " to_char(age(pc.fecha_nacimiento),'yy') || ' Años'  as edad,\n"
                + "  e.nombreespecialidad,\n"
                + "  a.nombre as tipoatencion,\n"
                + " ( d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor,\n"
                + "   (p.fecha || ' ' || o.hora )as fechahora , to_char(p.fecha, 'dd/mm/yyyy')as fecha, \n"
                + "   case when  c.id_motivo is null then ( select di.nombre_diagnostico from  agenda.diagnosticos di\n"
                + " inner join   agenda.diagnostico_especialidad des\n"
                + " on di.iddiagnostico = des.iddiagnostico\n"
                + "  where des.iddiagnodtivo_especialidad = c.iddiagnostico_especialidad) else (select mc.nombremotivo\n"
                + " FROM  agenda.motivoconsulta mc   where mc.id_motivo = c.id_motivo ) end as motivo , p.id_especialidad , c.motivo as porquecito, a.id_atencion "
                + "\n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta  \n"
                + "  join agenda.planificar p on o.id_plani_sobre = p.id_planificacion \n"
                + "  join agenda.paciente pc on c.rut_paciente =  pc.rut\n"
                + "  join agenda.doctor d on p.rut_doctor = d.rut\n"
                + "  join agenda.especialidades e on p.id_especialidad = e.id_especialidad \n"
                + "  join agenda.atencion a on p.id_atencion = a.id_atencion "
                + "    where \n"
                + "   c.id_cita= '" + idcita + "' order by  p.fecha ,  o.hora  ; ");
        this.cnn.conectar();
        cita c = new cita();
        try {
            while (this.cnn.getRst().next()) {

                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setMotivo_cancela(this.cnn.getRst().getString("rut"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("genero"));
                c.setTemporales2(this.cnn.getRst().getString("edad"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales3(this.cnn.getRst().getString("tipoatencion"));
                c.setTemporales4(this.cnn.getRst().getString("motivo"));
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setMotivo(this.cnn.getRst().getString("fechahora"));
                c.setId_patologia(this.cnn.getRst().getInt("id_especialidad"));
                c.setTemporales5(this.cnn.getRst().getString("porquecito"));
                c.setId_tipoatencion(this.cnn.getRst().getInt("id_atencion"));
                c.setTemporales6(this.cnn.getRst().getString("fecha"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return c;

    }

    /*buscar cita por id*/
    public cita buscarCitaporId(int idcita) {

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  * FROM agenda.cita where id_cita = " + idcita + " ");
        this.cnn.conectar();
        cita c = new cita();
        try {
            while (this.cnn.getRst().next()) {

                c.setId_cita(this.cnn.getRst().getInt("id_cita"));

                c.setId_oferta(this.cnn.getRst().getInt("id_oferta"));

                c.setId_tipoatencion(this.cnn.getRst().getInt("id_tipoatencion"));
                c.setGes(this.cnn.getRst().getInt("ges"));
                c.setId_patologia(this.cnn.getRst().getInt("id_patologia"));
                c.setRut_paciente(this.cnn.getRst().getString("rut_paciente"));
                c.setConfirmacion_cita(this.cnn.getRst().getInt("confirmacion_cita"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return c;

    }

    /*cancelar cita*/
    public String cancelarCitaPaciente(int idcita, String motivo) {
        String mensaje = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  estatus = 0 ,"
                + " motivo_cancela = '" + motivo + "',"
                + " usuario_cancela = '" + getUsuarioLoggin() + "'\n"
                + " \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita cancelada Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }

    /*ver si un paciente ya tiene una cita asignada ese dia */
    public boolean buscarCitaRutFecha(String fecha, String rutPaciente, String hora) {
        boolean yatiene = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT * FROM \n"
                + "  agenda.cita c inner join  agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + " where  upper(c.rut_paciente) = upper('" + rutPaciente + "') "
                + "and pl.fecha = '" + fecha + "' and c.estatus = 1 and o.hora='" + hora + "' ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                yatiene = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return yatiene;
    }

    /*confirmar cita paciente*/
    public String confirmacionCita(int idcita) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  confirmacion_cita = 1,\n"
                + "  estatus = 2 , fecha_recepcion = CURRENT_TIMESTAMP ,"
                + " usuario_recepciona = '" + getUsuarioLoggin() + "' "
                + " \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Paciente recepcionado exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }

        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*metodo para reanudar la cita */
 /*confirmar cita paciente*/
    public String reanudarCita(int idcita, String motivo) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  confirmacion_cita = 1,\n"
                + "  estatus = 2 ,"
                + "  motivoreanudaratencion = '" + motivo + "'"
                + " \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita Recepcionada Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }

        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*reaunidar cita en tens con proceso */
    public String reanudarCitaenProceso(int idcita, String motivo) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  \n"
                + "  confirmacion_cita = 1,\n"
                + "  estatus = 7 ,"
                + "  motivoreanudaratencion = '" + motivo + "'"
                + " \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita Recepcionada Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }

        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*reaunidar cita de tecnologo*/
    public String reanudarCitaTecnologo(int idcita, String motivo) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  estatus = 5 ,"
                + "  motivoreanudaratencion = '" + motivo + "'"
                + " \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita modificada Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }

        this.cnn.cerrarConexion();
        return mensaje;
    }

    /*reaunidar cita de tecnologo que esta en proceso */
    public String reanudarCitaenProcesoTecnologo(int idcita, String motivo) {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE \n"
                + "  agenda.cita  \n"
                + "SET \n"
                + "  estatus = 8 ,"
                + "  motivoreanudaratencion = '" + motivo + "'"
                + " \n"
                + "WHERE \n"
                + "  id_cita = " + idcita + "; ");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Cita reaunidada Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }

        this.cnn.cerrarConexion();
        return mensaje;
    }

    public boolean buscarsicupotienecita(int idplanificacion) {
        boolean encontrocitas = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  id_cita\n"
                + "FROM agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "  where o.id_plani_sobre = " + idplanificacion + " and c.estatus = 1  ; ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                encontrocitas = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return encontrocitas;
    }

    public Vector<cita> buscarultimas2citasporPacienteapartirdeunafecha(String paciente, Date fecha) {
        Vector<cita> citas = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "                c.id_cita,\n"
                + "                  p.fecha,\n"
                + "                 o.hora,\n"
                + "                 e.nombreespecialidad,\n"
                + "                (d.nombre  ||' ' || d.apellido_paterno  ||' ' || d.apellido_materno) as doctor,\n"
                + "               case when (p.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'\n"
                + "                else case when  c.estatus = 1 then 'Cita Asignada' \n"
                + "                                else case when c.estatus = 0 then 'Cita Cancelada' \n"
                + "                                ELSE case when c.estatus = 2 then 'Cita Recepcionada'\n"
                + "                                else case when c.estatus = 3 then 'Cita Recepcionada y cancelada'\n"
                + "                                else case when c.estatus = 4 then 'Cita Terminada Satisfactoriamente'\n"
                + "                                else case when c.estatus = 5 then 'Cita en Proceso'\n"
                + "                                else 'Cita cancelada por el Profesional' end end end end end end end as estatus\n"
                + "              \n"
                + "               FROM \n"
                + "                 agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "                  join  agenda.planificar p on o.id_plani_sobre = p.id_planificacion\n"
                + "                  join agenda.especialidades e on p.id_especialidad = e.id_especialidad\n"
                + "                  join agenda.doctor d on p.rut_doctor = d.rut \n"
                + "                  join agenda.tipo_atencion t on c.id_tipoatencion = t.id_tipoatencion \n"
                + "                \n"
                + "                  where c.estatus <> 0 and  upper(c.rut_paciente) = upper('" + paciente + "') and p.fecha < '" + fecha + "' order by p.fecha desc\n"
                + "                limit 2");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setFecha(this.cnn.getRst().getDate("fecha"));
                c.setTemporales3(this.cnn.getRst().getString("hora"));
                c.setTemporales(this.cnn.getRst().getString("nombreespecialidad"));
                c.setTemporales2(this.cnn.getRst().getString("doctor"));
                c.setTemporales1(this.cnn.getRst().getString("estatus"));

                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }

    public int buscartotalesporatencionporEspecialidadyDoctor(Date fechainicio, Date fechafin, int especialidad, String doctor, int idatencion) {
        String resto = "";

        int totalatencion = 0;
        if (especialidad != -1) {
            resto = " and pl.id_especialidad =  " + especialidad + "";
        }

        if (!doctor.equals("-1")) {
            resto = resto + "and d.rut='" + doctor + "'";
        }
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   case when sum( pl.cupos ) is null then 0 else sum( pl.cupos ) end  as total \n"
                + "               \n"
                + "                  FROM \n"
                + "                 agenda.planificar pl \n"
                + "                      join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "                    join agenda.doctor d on d.rut = pl.rut_doctor\n"
                + "                    join agenda.especialidades e on e.id_especialidad = pl.id_especialidad\n"
                + "                where   pl.fecha BETWEEN '" + fechainicio + "' and '" + fechafin + "'\n"
                + "   " + resto + "  and pl.estatus <> 0 and a.id_atencion = " + idatencion + " \n"
                + "                   ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                totalatencion = this.cnn.getRst().getInt("total");
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return totalatencion;
    }

    public int totalpacientesparaunDoctorEspecialidadFecha(Date fechainicio, Date fechafin, int especialidad, String doctor) {
        int total = 0;
        String resto = "";
        if (especialidad != -1) {
            resto = " and pl.id_especialidad =  " + especialidad + "";
        }

        if (!doctor.equals("-1")) {
            resto = resto + "and d.rut='" + doctor + "'";
        }
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT sum (pl.cupos - pl.cupos_disponibles ) as totalpaciente  \n"
                + "  FROM \n"
                + " agenda.planificar pl \n"
                + "       join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "       join agenda.doctor d on d.rut = pl.rut_doctor\n"
                + "       join agenda.especialidades e on e.id_especialidad = pl.id_especialidad\n"
                + "  where   pl.fecha BETWEEN '" + fechainicio + "' and '" + fechafin + "'\n"
                + "   " + resto + " and pl.estatus <> 0");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                total = this.cnn.getRst().getInt("totalpaciente");

            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return total;
    }

    public Vector<cita> buscarCitasdeunaFechaoEspecialidadoDoctro(Date fechainicio, Date fechafin, int especialidad, String doctor) {
        Vector<cita> citas = new Vector();
        String resto = "";
        if (especialidad != -1) {
            resto = " and pl.id_especialidad =  " + especialidad + "";
        }

        if (!doctor.equals("-1")) {
            resto = resto + "and d.rut='" + doctor + "'";
        }
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT d.rut , d.nombre || ' '|| d.apellido_paterno ||' '|| d.apellido_materno as nombredoctor,\n"
                + "e.nombreespecialidad , e.id_especialidad, \n"
                + "  pl.id_planificacion ,a.nombre  \n"
                + "  FROM \n"
                + " agenda.planificar pl \n"
                + "       join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "       join agenda.doctor d on d.rut = pl.rut_doctor\n"
                + "       join agenda.especialidades e on e.id_especialidad = pl.id_especialidad\n"
                + "  where   pl.fecha BETWEEN '" + fechainicio + "' and '" + fechafin + "'\n"
                + "   " + resto + " and pl.estatus <> 0 order by pl.hora_inicio");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setRut_doctor(this.cnn.getRst().getString("rut"));
                c.setTemporales(this.cnn.getRst().getString("nombredoctor"));
                c.setTemporales1(this.cnn.getRst().getString("nombreespecialidad"));

                c.setId_motivo(this.cnn.getRst().getInt("id_especialidad"));

                c.setId_cita(this.cnn.getRst().getInt("id_planificacion"));
                c.setTemporales2(this.cnn.getRst().getString("nombre"));

                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }

    /*INFORMACION PARA ESTADISTICO DE ATENCIONES*/
    public Vector<cita> BuscarCitasAtendidasparaInformeEstadistico(Date fecha1, Date fecha2, String doctor, int especialidad) {
        Vector<cita> citas = new Vector();
        String resto = "p.fecha  BETWEEN  '" + fecha1 + "' AND '" + fecha2 + "' ";
        if (especialidad != -1) {
            resto = resto + " and e.id_especialidad =  " + especialidad + "";
        }

        if (!doctor.equals("-1")) {
            resto = resto + "and d.rut='" + doctor + "'";
        }
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  Select   pac.rut,\n"
                + "           pac.nombre,\n"
                + "           pac.apellido_paterno,\n"
                + "           pac.apellido_moderno,\n"
                + "           case when pac.genero = 1 then 'Femenino' else 'Masculino' end as genero,\n"
                + "           to_char(age(CURRENT_DATE, pac.fecha_nacimiento),'yy')as edad ,\n"
                + "           pac.contacto1,\n"
                + "           pac.contacto2,\n"
                + "            pac.direccion,\n"
                + "            to_char(p.fecha, 'dd/mm/yyyy') as fechacita,\n"
                + "            o.hora as horacita,\n"
                + "            e.nombreespecialidad,\n"
                + "             d.rut ||'-'||  d.nombre ||' '|| d.apellido_paterno ||' '|| d.apellido_materno as rutdoctor,\n"
                + "             a.nombre as tipocita,\n"
                + "             case when (p.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'\n"
                + "              else case when c.estatus = 1 then 'Cita Asignada'\n"
                + "                                else case when c.estatus = 0 then 'Cita Cancelada' \n"
                + "                                ELSE case when c.estatus = 2 then 'Cita Recepcionada'\n"
                + "                               Else case when c.estatus = 3 then 'Cita Recepcionada y cancelada'\n"
                + "                                else case when c.estatus = 4 then 'Cita Terminada Satisfactoriamente'\n"
                + "                               else case when c.estatus = 5 then 'Cita en Proceso'\n"
                + "                              else 'Cita cancelada por el Profesional' end end end end end end end as estatus ,\n"
                + "                              pres.codigo ||'/'|| pres.descripcion as prestacion,\n"
                + "                              case when c.ges =1 then 'GES' else 'NO GES' end as ges,  c.id_cita , e.id_especialidad, c.estatus as idestatus ,  case when  pac.provision= 1 then 'FONASA' ||' '|| t.nombre  else case when pac.provision = 2 then 'Isapre' else case when pac.provision = 3 then 'Prais' else 'Sin Prevision' end  end end as prevension, "
                + "                a.id_atencion, case when c.fecha_recepcion is not null then   to_char( c.fecha_recepcion, 'HH24:MI:SS') else '' end  as horarecepcion\n"
                + "                \n"
                + "                FROM \n"
                + "                 \n"
                + "                  agenda.cita c inner\n"
                + "                  join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "                  join agenda.planificar p on p.id_planificacion = o.id_plani_sobre\n"
                + "                  join agenda.especialidades e on e.id_especialidad= p.id_especialidad\n"
                + "                  join agenda.doctor d on d.rut = p.rut_doctor \n"
                + "                  join agenda.paciente pac on pac.rut = c.rut_paciente \n"
                + "                  join agenda.atencion a on p.id_atencion = a.id_atencion"
                + "                  join agenda.tramo t on t.id = pac.tramo  \n"
                + "            join caja.prestacion pres on pres.oid = p.id_prestacion where " + resto + " order by p.fecha \n"
                + "         ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales(this.cnn.getRst().getString("nombre"));
                c.setTemporales1(this.cnn.getRst().getString("apellido_paterno"));
                c.setTemporales2(this.cnn.getRst().getString("apellido_moderno"));
                c.setTemporales3(this.cnn.getRst().getString("genero"));
                c.setTemporales4(this.cnn.getRst().getString("edad"));
                c.setTemporales5(this.cnn.getRst().getString("contacto1"));
                c.setTemporales6(this.cnn.getRst().getString("contacto2"));
                c.setTemporales7(this.cnn.getRst().getString("direccion"));
                c.setTemporales8(this.cnn.getRst().getString("fechacita"));
                c.setTemporales9(this.cnn.getRst().getString("horacita"));
                c.setTemporales10(this.cnn.getRst().getString("nombreespecialidad"));
                c.setRut_doctor(this.cnn.getRst().getString("rutdoctor"));
                c.setTemporales11(this.cnn.getRst().getString("tipocita"));
                c.setTemporales12(this.cnn.getRst().getString("estatus"));
                c.setTemporales13(this.cnn.getRst().getString("prestacion"));
                c.setTemporales14(this.cnn.getRst().getString("ges"));
                c.setId_cita(this.cnn.getRst().getInt("id_cita"));
                c.setGes(this.cnn.getRst().getInt("id_especialidad"));
                c.setEstatus(this.cnn.getRst().getInt("idestatus"));
                c.setTemporales15(this.cnn.getRst().getString("prevension"));
                c.setId_tipoatencion(this.cnn.getRst().getInt("id_atencion"));
                c.setTemporales16(this.cnn.getRst().getString("horarecepcion"));

                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;

    }

    public lugar buscarATENCIONdeunaCita(int idcita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idatencion,\n"
                + "  idcita,\n"
                + "  horaatencion,\n"
                + "  diagnostico,\n"
                + "  tratamiento,\n"
                + "  examenes,\n"
                + "  procedimiento,\n"
                + "  observaciones, tipo\n"
                + "FROM \n"
                + "  agenda.atencionesporcitas where idcita = " + idcita + " ;");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setId_atencion(this.cnn.getRst().getInt("idatencion"));

                l.setHora(this.cnn.getRst().getString("horaatencion"));
                l.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                l.setTratamiento(this.cnn.getRst().getString("tratamiento"));
                l.setExamenes(this.cnn.getRst().getString("examenes"));
                l.setProcedimientos(this.cnn.getRst().getString("procedimiento"));
                l.setObservaciones(this.cnn.getRst().getString("observaciones"));
                l.setEstatu(this.cnn.getRst().getInt("tipo"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;
    }

    /*buscar comprobante de la cita*/
    public cita buscarComprobanteCita(String rut) {
        cita c = new cita();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " p.rut, "
                + " (p.nombre || ' ' || p.apellido_paterno || ' ' || p.apellido_moderno) as paciente, "
                + " pl.fecha, "
                + " o.hora, "
                + " e.nombreespecialidad as especialidad, "
                + " (d.nombre ||' '||d.apellido_paterno ||' '|| d.apellido_materno) as doctor , "
                + " a.nombre as tipoatencion "
                + " FROM  "
                + "  agenda.cita c inner join agenda.oferta o on c.id_oferta = o.id_oferta "
                + "  join agenda.paciente p on c.rut_paciente = p.rut "
                + "   join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre "
                + "  join agenda.doctor d on d.rut = pl.rut_doctor "
                + "   join agenda.especialidades e on pl.id_especialidad = e.id_especialidad "
                + "  join agenda.atencion a on pl.id_atencion = a.id_atencion where c.id_cita= " + rut + " ;  ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales(this.cnn.getRst().getString("paciente"));
                c.setFecha(this.cnn.getRst().getDate("fecha"));
                c.setTemporales2(this.cnn.getRst().getString("hora"));
                c.setTemporales3(this.cnn.getRst().getString("especialidad"));
                c.setTemporales4(this.cnn.getRst().getString("doctor"));
                c.setTemporales5(this.cnn.getRst().getString("tipoatencion"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return c;
    }

    /*buscar si es cirugia en los protocolos*/
    public lugar AtencionenPabellon(int idcita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" select pro.idprotocolo, pro.preoperatorio as diagnostico ,\n"
                + "pro.detalleoperatorio as observacion,\n"
                + " to_char(pro.fecha , 'HH24:MI:SS') as horaatencion\n"
                + "from pabellon.protocolo pro inner join         \n"
                + "agenda.cita c on pro.idcita = c.id_cita\n"
                + "where  idcita = " + idcita + "                 \n"
                + "union               \n"
                + "select po.id_protocolo_operatorio as idprotocolo,\n"
                + "CP.diag_preoperatorio as diagnostico,\n"
                + " cp.detalle_operatorio_complemento_protocolo as observacion,\n"
                + "to_char( po.hora_creacion_protocolo, 'HH24:MI:SS') as horaatencion\n"
                + " from schema_pabellon.protocolo_operatorio PO inner join \n"
                + " schema_pabellon.complemento_protocolo cp on PO.id_protocolo_operatorio = CP.id_protocolo_operatorio_complemento_protocolo\n"
                + "  where  PO.id_cita_protocolo_operatorio= " + idcita + "");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setId_atencion(this.cnn.getRst().getInt("idprotocolo"));
                l.setHora(this.cnn.getRst().getString("horaatencion"));
                l.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                l.setObservaciones(this.cnn.getRst().getString("observacion"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;
    }

    /*buscar si es vicio de refraccion*/
    public lugar AtencionenBoxViciodeRefraccion(int idcita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion_tecnologo,\n"
                + "  id_cita,\n"
                + "  to_char(fecha_registro , 'HH24:MI:SS') as horaatencion ,\n"
                + "  rp.diagnostico, rp.tratamientoeindicacion||\n"
                + "  case when rp.tratamientocerca = 1 then '- Lentes de Cerca' else '' end ||\n"
                + "  case when rp.tratamientolejos = 1 then ' - Lentes de Lejos' else '' end  as tratamiento,\n"
                + "  vf.indicaciones,\n"
                + "  vf.motivoconsulta\n"
                + "  \n"
                + "\n"
                + "FROM \n"
                + "  agenda.atencion_clinica_tecnologo act inner \n"
                + "  JOIN agenda.registropatalogia rp on  act.id_atencion_tecnologo = rp.id_atencion\n"
                + "  join agenda.viciorefraccion vf on vf.id_atencion = rp.id_atencion\n"
                + "  where act.id_cita = " + idcita + " ");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setId_atencion(this.cnn.getRst().getInt("id_atencion_tecnologo"));
                l.setHora(this.cnn.getRst().getString("horaatencion"));
                l.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                l.setTratamiento(this.cnn.getRst().getString("tratamiento"));
                l.setIndicaciones(this.cnn.getRst().getString("indicaciones"));
                l.setObservaciones(this.cnn.getRst().getString("motivoconsulta"));
                l.setExamenes("");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;

    }

    /* BUSCAR ENTREVISTA PREOPERATORIO PARA INFORME*/
    public lugar AtencionenBoxEntrevistaPreoperatoria(int idcita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  identrevista_preoperatoria,\n"
                + "  idcita,\n"
                + " to_char(fecha_registro , 'HH24:MI:SS') as horaatencion , \n"
                + "  diagnostico,\n"
                + "  cirugia_propuesta,\n"
                + "  tratamiento,\n"
                + "  observaciones\n"
                + "FROM \n"
                + "  agenda.entrevista_preoperatoria where idcita =" + idcita + " ;");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setHora(this.cnn.getRst().getString("horaatencion"));
                l.setDiagnostico(this.cnn.getRst().getString("diagnostico"));
                l.setTratamiento(this.cnn.getRst().getString("tratamiento"));
                l.setIndicaciones(this.cnn.getRst().getString("cirugia_propuesta"));
                l.setObservaciones(this.cnn.getRst().getString("observaciones"));
                l.setExamenes("");
                l.setId_atencion(this.cnn.getRst().getInt("identrevista_preoperatoria"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;

    }

    /*Buscar Examenes para atencion*/
    public lugar AtencionExamenesdeLaboratorioporCita(int idcita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_atencion,\n"
                + "  id_cita,\n"
                + " to_char(fecha , 'HH24:MI:SS') as horaatencion , \n"
                + "  usuario,\n"
                + "  ipusuario,\n"
                + "  observacion\n"
                + "FROM \n"
                + "  agenda.atencion_examen_laboratorio where id_cita = " + idcita + " ;");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setHora(this.cnn.getRst().getString("horaatencion"));
                l.setDiagnostico("");
                l.setTratamiento("");
                l.setIndicaciones("");
                l.setObservaciones(this.cnn.getRst().getString("observacion"));
                l.setExamenes("");
                l.setId_atencion(this.cnn.getRst().getInt("id_atencion"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;

    }

    /*Buscar atencion de electrocardiograma*/
    public lugar buscarAtencionElectrocardiograma(int idcita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idcita,\n"
                + "  idelectrocardiograma,\n"
                + "  usuario,\n"
                + "  ipusuario,\n"
                + "  to_char(fecha_atencion , 'HH24:MI:SS') as horaatencion \n"
                + "FROM \n"
                + "  agenda.electrocardiograma where idcita = " + idcita + " ;");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setHora(this.cnn.getRst().getString("horaatencion"));
                l.setDiagnostico("");
                l.setTratamiento("");
                l.setIndicaciones("");
                l.setObservaciones("");
                l.setExamenes("");
                l.setId_atencion(this.cnn.getRst().getInt("idelectrocardiograma"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;
    }

    /*Buscar las demas especialidades en hoja diaria*/
    public lugar AtencionHojaDiaria(int idcita) {
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idhojadiaria,\n"
                + "  idcita,\n"
                + "   to_char(fecharegistro , 'HH24:MI:SS') as horaatencion  ,\n"
                + " \n"
                + "  otrodiagnostico,\n"
                + "  otroprocedimiento,\n"
                + "  otroexamenes,\n"
                + "  otroindicaciones\n"
                + " \n"
                + "FROM \n"
                + "  agenda.hojadiaria where idcita = " + idcita + " ; ");
        this.cnn.conectar();
        lugar l = new lugar();
        try {
            if (this.cnn.getRst().next()) {
                l.setHora(this.cnn.getRst().getString("horaatencion"));
                l.setDiagnostico(this.cnn.getRst().getString("otrodiagnostico"));
                l.setTratamiento("");
                l.setIndicaciones(this.cnn.getRst().getString("otroindicaciones"));
                l.setObservaciones("");
                l.setExamenes(this.cnn.getRst().getString("otroexamenes"));
                l.setId_atencion(this.cnn.getRst().getInt("idhojadiaria"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;
    }

    /*lista para vicio*/
    public Vector<cita> buscarCitasdeVicioRefraccionParaHoy() {

        Vector<cita> citas = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "p.rut , p.nombre  || '  ' || p.apellido_paterno || '  ' || p.apellido_moderno as nombre  ,\n"
                + "a.nombre as tipoatencion,\n"
                + " pl.fecha,\n"
                + " o.hora,\n"
                + "\n"
                + "case when (pl.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'"
                + "else case when  c.estatus = 1 then 'Cita Asignada' \n"
                + "else case when c.estatus = 0 then 'Cita Cancelada' \n"
                + "ELSE case when c.estatus = 2 then 'Cita Recepcionada'\n"
                + "else case when c.estatus = 3 then 'Cita Recepcionada y cancelada'\n"
                + "else case when c.estatus = 4 then 'Cita Terminada Satisfactoriamente'\n"
                + "else case when c.estatus = 5 then 'Cita en Proceso'\n"
                + "else 'Cita cancelada por el Profesional' end end end end end end end as estatus\n"
                + "\n"
                + "  \n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.paciente p on\n"
                + "  c.rut_paciente = p.rut\n"
                + "  join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "  join agenda.doctor d on pl.rut_doctor = d.rut\n"
                + "  join agenda.especialidades e on e.id_especialidad = pl.id_especialidad "
                + "  where  (e.id_especialidad=19)  and pl.fecha = current_date ORDER BY o.hora ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales(this.cnn.getRst().getString("nombre"));
                c.setTemporales1(this.cnn.getRst().getString("tipoatencion"));
                c.setTemporales2(this.cnn.getRst().getString("fecha"));
                c.setTemporales3(this.cnn.getRst().getString("hora"));
                c.setTemporales4(this.cnn.getRst().getString("estatus"));
                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }

    /*lista de pacientes para un doctor*/
    public Vector<cita> buscarCitasdeDoctorParaHoy(String rutdoctor) {

        Vector<cita> citas = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "p.rut , p.nombre  || '  ' || p.apellido_paterno || '  ' || p.apellido_moderno as nombre  ,\n"
                + "a.nombre as tipoatencion,\n"
                + " pl.fecha, to_char(pl.fecha, 'dd/mm/yyyy') as fechacita, \n"
                + " o.hora,\n"
                + "\n"
                + "case when (pl.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'\n"
                + "else case when  c.estatus = 1 then 'Asignada' \n"
                + "else case when c.estatus = 0 then 'Cancelada' \n"
                + "ELSE case when c.estatus = 2 then 'Recepcionada'\n"
                + "else case when c.estatus = 3 then 'Cancelada Tens'\n"
                + "else case when c.estatus = 4 then 'Terminada '\n"
                + "else case when c.estatus = 6 then 'Cancelada por el Profesional'\n"
                + "else 'En Proceso' end end end end end end end as estatus,"
                + " case when c.fecha_recepcion is null then ' ' else to_char(c.fecha_recepcion, 'HH24:MI:SS') end  as horaderecepcion , c.estatus as idestatus \n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.paciente p on\n"
                + "  c.rut_paciente = p.rut\n"
                + "  join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "  join agenda.doctor d on pl.rut_doctor = d.rut\n"
                + "  where  upper(d.rut) = upper('" + rutdoctor + "') and pl.fecha = current_date and c.estatus<>0 ORDER BY o.hora ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales(this.cnn.getRst().getString("nombre"));
                c.setTemporales1(this.cnn.getRst().getString("tipoatencion"));
                c.setTemporales2(this.cnn.getRst().getString("fecha"));
                c.setTemporales3(this.cnn.getRst().getString("hora"));
                c.setTemporales4(this.cnn.getRst().getString("estatus"));
                c.setTemporales5(this.cnn.getRst().getString("horaderecepcion"));
                c.setEstatus(this.cnn.getRst().getInt("idestatus"));
                c.setTemporales21(this.cnn.getRst().getString("fechacita"));
                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }

    /*lista de  paciente citados para hoy*/
    public Vector<cita> buscarCitasParaHoyEntrevistaPreoperatoria() {

        Vector<cita> citas = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "p.rut , p.nombre  || '  ' || p.apellido_paterno || '  ' || p.apellido_moderno as nombre  ,\n"
                + "a.nombre as tipoatencion,\n"
                + " pl.fecha,\n"
                + " o.hora,\n"
                + "\n"
                + "case when (pl.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'"
                + "else case when  c.estatus = 1 then 'Cita Asignada' "
                + "\n"
                + "else case when c.estatus = 0 then 'Cita Cancelada' \n"
                + "ELSE case when c.estatus = 2 then 'Cita Recepcionada'\n"
                + "else case when c.estatus = 8 then 'Cita en Proceso'\n"
                + "else case when c.estatus = 4 then 'Cita Terminada Satisfactoriamente'\n"
                + "else case when c.estatus = 5 then 'Cita en Proceso'\n"
                + " else 'Cita cancelada por el Profesional' end  end end end end end end  as estatus\n"
                + "\n"
                + "  \n"
                + "FROM \n"
                + "  agenda.cita c inner join agenda.paciente p on\n"
                + "  c.rut_paciente = p.rut\n"
                + "  join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "  join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "  join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "  join agenda.doctor d on pl.rut_doctor = d.rut"
                + "  join agenda.especialidades e on e.id_especialidad = pl.id_especialidad\n"
                + "  where   pl.fecha = current_date and e.id_especialidad = 46  ORDER BY o.hora ");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales(this.cnn.getRst().getString("nombre"));
                c.setTemporales1(this.cnn.getRst().getString("tipoatencion"));
                c.setTemporales2(this.cnn.getRst().getString("fecha"));
                c.setTemporales3(this.cnn.getRst().getString("hora"));
                c.setTemporales4(this.cnn.getRst().getString("estatus"));
                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }


    /*lista de  paciente citados para hoy*/
    public Vector<cita> buscarCitasParaHoy() {

        Vector<cita> citas = new Vector();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("sELECT \n"
                + "               p.rut , p.nombre  || '  ' || p.apellido_paterno || '  ' || p.apellido_moderno as nombre  ,\n"
                + "               a.nombre as tipoatencion,\n"
                + "                pl.fecha,\n"
                + "                o.hora,\n"
                + "         case when (pl.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'            \n"
                + "              else case when  c.estatus = 1 then 'Cita Asignada' \n"
                + "               else case when c.estatus = 0 then 'Cita Cancelada' \n"
                + "              ELSE case when c.estatus = 2 then 'Cita Recepcionada'\n"
                + "                else case when c.estatus = 3 then 'Cita Recepcionada y cancelada'\n"
                + "               else case when c.estatus = 4 then 'Cita Terminada Satisfactoriamente'\n"
                + "                else case when c.estatus = 5 then 'Cita en Proceso'\n"
                + "                else 'Cita cancelada por el Profesional' end end end end end end end as estatus\n"
                + "                , es.nombreespecialidad , d.rut ||'/'|| d.nombre ||' '||d.apellido_paterno as docto,"
                + "              case when c.fecha_recepcion is null then ' ' else to_char(c.fecha_recepcion, 'HH24:MI:SS') end  as horaderecepcion,"
                + "             case when  c.usuario_recepciona is null then ' ' else  (select r.nombre ||' '|| r.apellidop from seguridad.registrousuario r where r.rut = c.usuario_recepciona )  end  as quienrecepciona ,"
                + "                c.estatus  as idestatus \n"
                + "                 \n"
                + "               \n"
                + "               FROM \n"
                + "                 agenda.cita c inner join agenda.paciente p on\n"
                + "                  c.rut_paciente = p.rut\n"
                + "                 join agenda.oferta o on c.id_oferta = o.id_oferta\n"
                + "                join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + "                 join agenda.atencion a on pl.id_atencion = a.id_atencion\n"
                + "                join agenda.doctor d on pl.rut_doctor = d.rut\n"
                + "                join agenda.especialidades es on es.id_especialidad = pl.id_especialidad\n"
                + "                where  c.estatus <> 0 and   pl.fecha = current_date ORDER BY o.hora asc");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setRut_paciente(this.cnn.getRst().getString("rut"));
                c.setTemporales(this.cnn.getRst().getString("nombre"));
                c.setTemporales1(this.cnn.getRst().getString("tipoatencion"));
                c.setTemporales2(this.cnn.getRst().getString("fecha"));
                c.setTemporales3(this.cnn.getRst().getString("hora"));
                c.setTemporales4(this.cnn.getRst().getString("estatus"));
                c.setRut_doctor(this.cnn.getRst().getString("docto"));
                c.setTemporales5(this.cnn.getRst().getString("nombreespecialidad"));
                c.setMotivo_cancela(this.cnn.getRst().getString("horaderecepcion"));
                c.setTemporales6(this.cnn.getRst().getString("quienrecepciona"));
                c.setEstatus(this.cnn.getRst().getInt("idestatus"));

                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }

    public Integer buscarestatusdecita(int idcita) {
        int estatus = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + " estatus\n"
                + "FROM \n"
                + "  agenda.cita where id_cita = '" + idcita + "' ; ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                estatus = this.cnn.getRst().getInt("estatus");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return estatus;
    }

    /*el aviso se genera si el paciente es menor de 60 años, si es fonasa c o d , si prestacion es de tipo 3
    si la cita es no ges y si es particulaor o sin prevision*/
    public lugar avisodepagoenAdmision(int idcita) {
        lugar l = new lugar();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select c.id_cita, pac.rut ||' '|| pac.nombre||' '|| pac.apellido_paterno as paciente,\n"
                + " age(CURRENT_DATE, pac.fecha_nacimiento ) as edad,\n"
                + "pre.nombre as prevision,\n"
                + "t.nombre as tramo , pt.codigo ||' '|| pt.descripcion as prestacion,"
                + " case when t.id = 3 then to_char(pt.preciofonasac,'LFM9,999,999')   else to_char(pt.preciofonasad,'LFM9,999,999') end  as precioapagar\n"
                + "                                  "
                + "from agenda.cita c inner join\n"
                + "agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + "join agenda.planificar p on p.id_planificacion = o.id_plani_sobre \n"
                + "join agenda.paciente pac on pac.rut = c.rut_paciente\n"
                + "join agenda.prevision pre on pre.id_prevision = pac.provision\n"
                + "join agenda.tramo t on t.id = pac.tramo\n"
                + "join caja.prestacion pt on pt.oid = p.id_prestacion\n"
                + "where age(CURRENT_DATE , pac.fecha_nacimiento ) < 60 \n"
                + "and c.ges = 2 and pre.id_prevision in (1) \n"
                + "and t.id in (3,4)  and pt.idtipo_prestacion = 1 and p.id_especialidad not in (65,69) \n"
                + "and c.id_cita = " + idcita + " ");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                l.setId_atencion(this.cnn.getRst().getInt("id_cita"));
                l.setRut(this.cnn.getRst().getString("paciente"));
                l.setFolio(this.cnn.getRst().getString("edad"));
                l.setDescripcion(this.cnn.getRst().getString("prevision"));
                l.setIndicaciones(this.cnn.getRst().getString("tramo"));
                l.setNombre(this.cnn.getRst().getString("prestacion"));
                l.setVariable1(this.cnn.getRst().getString("precioapagar").replace(',', '.'));
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return l;

    }

    /*buscar pertinencia de la atencion en vista*/
    public String BucarPertinenciadecita(int cita) {
        String pertinencia = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT pertinencia\n"
                + "  FROM agenda.pertinencia where id_cita = " + cita + ";");
        this.cnn.conectar();

        try {
            if (this.cnn.getRst().next()) {
                pertinencia = this.cnn.getRst().getString("pertinencia");
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return pertinencia;
    }

    /*vecotr de citas para reporte */
    public Vector<cita> buscarCitasNSP(String especialidad, String doctor, Date fecha1, Date fecha2) {

        Vector<cita> citas = new Vector();
        String query_consultas = "";
        if ((!especialidad.equalsIgnoreCase("-1")) && (doctor.equalsIgnoreCase("-1"))) {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.citansp where fechacita BETWEEN '" + fecha1 + "' and '" + fecha2 + "' and idespecialidad=" + especialidad + " ;";
        } else if (!doctor.equalsIgnoreCase("-1")) {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.citansp where fechacita BETWEEN '" + fecha1 + "' and '" + fecha2 + "' and idespecialidad=" + especialidad + " and rutdoctor= '" + doctor + "' ;";
        } else {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.citansp where fechacita BETWEEN '" + fecha1 + "' and '" + fecha2 + "' ;";

        }
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(query_consultas);
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setFecha(this.cnn.getRst().getDate("fechacita"));
                c.setTemporales(this.cnn.getRst().getString("horacita"));
                c.setOtrafecha(this.cnn.getRst().getDate("fechaasinada"));
                c.setTemporales1(this.cnn.getRst().getString("diferencia"));
                c.setTemporales2(this.cnn.getRst().getString("rutpaciente"));
                c.setTemporales3(this.cnn.getRst().getString("nombrepaciente"));
                c.setTemporales4(this.cnn.getRst().getString("genero"));
                c.setTemporales5(this.cnn.getRst().getString("edad"));
                c.setTemporales6(this.cnn.getRst().getString("contacto1"));
                c.setTemporales7(this.cnn.getRst().getString("direccion"));
                c.setGes(this.cnn.getRst().getInt("numerocitas"));
                c.setTemporales8(this.cnn.getRst().getString("tipocita"));
                c.setTemporales9(this.cnn.getRst().getString("estadocita"));

                c.setTemporales11(this.cnn.getRst().getString("tipoatencion"));
                c.setTemporales12(this.cnn.getRst().getString("destino"));
                c.setTemporales13(this.cnn.getRst().getString("profesional"));
                c.setTemporales14(this.cnn.getRst().getString("especialidad"));
                c.setTemporales15(this.cnn.getRst().getString("usuario"));
                c.setTemporales16(this.cnn.getRst().getString("programa"));
                c.setTemporales17(this.cnn.getRst().getString("ges"));
                c.setTemporales18(this.cnn.getRst().getString("patologia"));
                c.setTemporales19(this.cnn.getRst().getString("motivo"));
                c.setTemporales20(this.cnn.getRst().getString("servicio"));
                c.setId_patologia(this.cnn.getRst().getInt("idespecialidad"));

                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }


    /*vecotr de citas para reporte */
    public Vector<cita> buscarCitasParaReporte(String especialidad, String doctor, Date fecha1, Date fecha2) {

        Vector<cita> citas = new Vector();
        String query_consultas = "";
        if ((!especialidad.equalsIgnoreCase("-1")) && (doctor.equalsIgnoreCase("-1"))) {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.reportecita where fechacita BETWEEN '" + fecha1 + "' and '" + fecha2 + "' and idespecialidad=" + especialidad + " order by fechacita, horacita asc ;";
        } else if (!doctor.equalsIgnoreCase("-1")) {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.reportecita where fechacita BETWEEN '" + fecha1 + "' and '" + fecha2 + "' and idespecialidad=" + especialidad + " and rutdoctor= '" + doctor + "' order by fechacita, horacita asc ;";
        } else {
            query_consultas = "SELECT  * "
                    + " FROM  "
                    + " agenda.reportecita where fechacita BETWEEN '" + fecha1 + "' and '" + fecha2 + "' order by fechacita, horacita asc;";

        }

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(query_consultas);
        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setFecha(this.cnn.getRst().getDate("fechacita"));
                c.setTemporales(this.cnn.getRst().getString("horacita"));
                c.setOtrafecha(this.cnn.getRst().getDate("fechaasinada"));
                c.setTemporales1(this.cnn.getRst().getString("diferencia"));
                c.setTemporales2(this.cnn.getRst().getString("rutpaciente"));
                c.setTemporales3(this.cnn.getRst().getString("nombrepaciente"));
                c.setTemporales4(this.cnn.getRst().getString("genero"));
                c.setTemporales5(this.cnn.getRst().getString("edad"));
                c.setTemporales6(this.cnn.getRst().getString("contacto1"));
                c.setTemporales7(this.cnn.getRst().getString("direccion"));
                c.setGes(this.cnn.getRst().getInt("numerocitas"));
                c.setTemporales8(this.cnn.getRst().getString("tipocita"));
                c.setTemporales9(this.cnn.getRst().getString("estadocita"));
                c.setTemporales10(this.cnn.getRst().getString("motivocancela"));
                c.setTemporales11(this.cnn.getRst().getString("tipoatencion"));
                c.setTemporales12(this.cnn.getRst().getString("destino"));
                c.setTemporales13(this.cnn.getRst().getString("profesional"));
                c.setTemporales14(this.cnn.getRst().getString("especialidad"));
                c.setTemporales15(this.cnn.getRst().getString("usuario"));
                c.setTemporales16(this.cnn.getRst().getString("programa"));
                c.setTemporales17(this.cnn.getRst().getString("ges"));
                c.setTemporales18(this.cnn.getRst().getString("patologia"));
                c.setTemporales19(this.cnn.getRst().getString("motivo"));
                c.setTemporales20(this.cnn.getRst().getString("servicio"));
                c.setTemporales21(this.cnn.getRst().getString("prestacion"));
                c.setMotivo(this.cnn.getRst().getString("tipo"));
                c.setId_cita(this.cnn.getRst().getInt("idcita"));
                c.setId_tipoatencion(this.cnn.getRst().getInt("id_atencion"));
                c.setRut_paciente(this.cnn.getRst().getString("fecharecepcion"));
                c.setMotivo_cancela(this.cnn.getRst().getString("usuariorecepcion"));
                

                citas.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return citas;
    }
    
    

    /*para filtro a la hora de citar*/
    public Vector<lugar> buscardiagnosticosCita(int especialidad) {
        Vector<lugar> diagnosticos = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "de.iddiagnodtivo_especialidad,\n"
                + "  d.nombre_diagnostico\n"
                + "FROM \n"
                + "agenda.diagnosticoespecialidad_prestacion dep inner join\n"
                + " \n"
                + "  agenda.diagnostico_especialidad de on dep.iddiagnosticoespecialidad = de.iddiagnodtivo_especialidad\n"
                + "   join\n"
                + "  agenda.diagnosticos d on d.iddiagnostico = de.iddiagnostico\n"
                + "  \n"
                + "   where  de.idespecialidad  = " + especialidad + " and d.estatus = 1  group by  de.iddiagnodtivo_especialidad,d.nombre_diagnostico order by d.nombre_diagnostico;");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                lugar dig = new lugar();
                dig.setId_diagnostico_urgencia(this.cnn.getRst().getInt("iddiagnodtivo_especialidad"));
                dig.setNombre(this.cnn.getRst().getString("nombre_diagnostico"));
                diagnosticos.add(dig);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return diagnosticos;

    }

    public Vector<lugar> buscarPrestacionesparaCitas(int iddiagnosticoespecialidad) {
        Vector<lugar> prestaciones = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "p.oid , p.codigo ||' ' || p.descripcion as nombre\n"
                + "FROM \n"
                + "      agenda.diagnosticoespecialidad_prestacion dep \n"
                + " \n"
                + "   inner join caja.prestacion p \n"
                + "  on dep.idprestacion =  p.oid\n"
                + "  \n"
                + "   where  iddiagnosticoespecialidad  = " + iddiagnosticoespecialidad + " and p.estatus = 1   order by  p.codigo, p.descripcion;");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                lugar prest = new lugar();
                prest.setId_diagnostico_urgencia(this.cnn.getRst().getInt("oid"));
                prest.setNombre(this.cnn.getRst().getString("nombre"));
                prestaciones.add(prest);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return prestaciones;
    }

    /*buscar la lateralidad */
    public Vector<lugar> buscarLateralidad(int idespecialidad) {
        Vector<lugar> lateralidad = new Vector<lugar>();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT idlateralidad, nombre\n"
                + "  FROM agenda.lateralidad where idespecialidad = " + idespecialidad + " order by  nombre ;");
        this.cnn.conectar();

        try {
            while (this.cnn.getRst().next()) {
                lugar lt = new lugar();
                lt.setId_diagnostico_urgencia(this.cnn.getRst().getInt("idlateralidad"));
                lt.setNombre(this.cnn.getRst().getString("nombre"));
                lateralidad.add(lt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return lateralidad;
    }

    public boolean BuscarsiTieneFichaExterna(String rut) {
        boolean encontre = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  rut,\n"
                + "  estatus\n"
                + " FROM \n"
                + "  schema_archivo.pacientesbodegaexterna where  estatus = 1 "
                + " and  upper(rut)= upper('" + rut + "') ");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                encontre = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return encontre;
    }

    /*mensaje enviado si se cita a un paciente con ficha afuera*/
    public boolean enviarMensajeporFichaExterna(int id_cita) {

        boolean mensaje = false;
        cita c = buscarCitaporIdParaAtencion(id_cita);
        String destinos = buscaremaildeFichaExterna();
        String cuerpo2 = "<html>\n"
                + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/banner.JPG\" >  \n"
                + "  <h1 style=\"color: #0059b3\">Estimado. </h1>\n"
                + "  <p> Se ha creado una cita para un paciente que cuenta con Ficha en Bodega Externa; los datos de la cita son:</br>\n"
                + "Nombre del paciente: " + c.getRut_paciente() + " </br>\n"
                + "RUT: " + c.getMotivo_cancela() + " </br>\n "
                + "Fecha de citación: " + c.getTemporales6() + " </br>\n"
                + "Especialidad: " + c.getTemporales() + " </br>\n"
                + "Profesional: " + c.getRut_doctor() + " </br>\n"
                + "\n"
                + "\n"
                + "\n"
                + "Se solicita las gestiones correspondientes para que la ficha se encuentre antes de la atención  </p>\n"
                + "  <img src=\"http://10.8.4.11:9090/Especialidades/public/imagenes/footer.JPG\" >  \n"
                + "</html>";
        String asuntos = "Ficha en Bodega Externa";
        mensaje = enviarEmailNotificacion(asuntos, destinos, cuerpo2);

        return mensaje;

    }

    /*estadistico de especialidades Hitos */
    public Vector<lugar> buscarhitosparaestadistico(Date fechainicio, Date fechafin) {
        Vector<lugar> hitos = new Vector<lugar>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   "
                + " id_hito, "
                + " fecha, "
                + "  h.descripcion,"
                + "   rut_paciente,  "
                + " case when  id_llamada= 0 then '' else   oh.descripcion end as llamada , "
                + "  case when  id_carta= 0 then '' else  oh2.descripcion end as carta , "
                + "  case when  id_examen = 0 then '' else  oh3.descripcion end as examenes ,  "
                + "    h.ipusuario,  "
                + "    h.usuario,  "
                + "  (p.nombre ||' ' || p.apellido_paterno || ' ' || p.apellido_moderno)as nombrepaciente,  "
                + "   v.nombre as responsable  "
                + "   FROM   "
                + "   agenda.hito h inner join agenda.paciente p on h.rut_paciente = p.rut  "
                + "   join agenda.vistafuncionario v on v.rut_funcionario = h.usuario  "
                + "   join  agenda.opcionhito oh on oh.id_opcion = h.id_llamada "
                + "   join agenda.opcionhito2 oh2 on oh2.id_opcion = h.id_carta "
                + "   join agenda.opcionhito3 oh3 on oh3.id_opcion = h.id_examen "
                + "  where    fecha :: Date between '" + fechainicio + "' and '" + fechafin + "'");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                lugar h = new lugar();
                h.setId_lugar(this.cnn.getRst().getInt("id_hito"));
                h.setFecha(this.cnn.getRst().getDate("fecha"));
                h.setDescripcion(this.cnn.getRst().getString("descripcion"));
                h.setRut(this.cnn.getRst().getString("rut_paciente"));
                h.setNombre(this.cnn.getRst().getString("nombrepaciente"));
                h.setVariable1(this.cnn.getRst().getString("llamada"));
                h.setVariable2(this.cnn.getRst().getString("carta"));
                h.setVariable3(this.cnn.getRst().getString("examenes"));
                h.setVariable4(this.cnn.getRst().getString("responsable"));
                hitos.add(h);

            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return hitos;
    }

    /*estadias en cama */
    public Vector<cita> BuscarmediaEstadias(String rut) {
        Vector<cita> atenciones = new Vector<cita>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  idduo,\n"
                + "  fechaduo,\n"
                + "  horaingreso,\n"
                + "  cama,\n"
                + "  sala,\n"
                + "  estado,\n"
                + "  anamnesis,\n"
                + "  rutpaciente\n"
                + "FROM \n"
                + "  schema_uo.mediaestadia where upper(rutpaciente) = upper('" + rut + "')  ;");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita a = new cita();
                a.setId_cita(this.cnn.getRst().getInt("idduo"));
                a.setTemporales(this.cnn.getRst().getString("fechaduo"));
                a.setTemporales1(this.cnn.getRst().getString("horaingreso"));
                a.setTemporales2(this.cnn.getRst().getString("cama"));
                a.setTemporales3(this.cnn.getRst().getString("sala"));
                a.setTemporales4(this.cnn.getRst().getString("estado"));
                a.setTemporales5(this.cnn.getRst().getString("anamnesis"));
                atenciones.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }

        return atenciones;
    }

    
    /*PACIENTES CANCELADOS REPORTE DE NOMINA */
         
        /* @author Juan Fernández
         * @since 20-08-2019
         * @version 1.0.0
         * 
         * Se agrega el campo motivo a la consulta.
         */
    public Vector<cita> buscarnominasparareporte(String especialidad, String doctor, Date fecha1, Date fecha2) {
        Vector<cita> atenciones = new Vector<cita>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        String query_consultas = "";
        if ((!especialidad.equalsIgnoreCase("-1")) && (doctor.equalsIgnoreCase("-1"))) {
            query_consultas = "SELECT "
                    + "  id,idcita, idplanificacion, pc.rut, (pc.nombre || '-' || pc.apellido_paterno) as nombrepaciente, "
                    + " pc.contacto1, pc.contacto2,pc.email, pc.direccion, p.fecha, p.rut_doctor , "
                    + " (d.nombre ||'-'||d.apellido_paterno) as nombredoctor, a.nombre as tipocita,"
                    + "  p.fecha as fechacita, o.hora, b.motivo as motivo"
                    + "  FROM  "
                    + "  agenda.citascanceladasporbloqueo cc inner join agenda.cita c on cc.idcita = c.id_cita "
                    + "   join agenda.planificar p on p.id_planificacion = cc.idplanificacion  "
                    + "    join agenda.paciente pc on c.rut_paciente =  pc.rut "
                    + "    join agenda.doctor d on p.rut_doctor = d.rut  "
                    + "    join agenda.atencion a on p.id_atencion = a.id_atencion "
                    + "   join agenda.oferta o on c.id_oferta = o.id_oferta "
                    + "   join agenda.bloqueo b on p.id_planificacion = b.id_cupo"
                    + "  where (p.fecha between '" + fecha1 + "' and '" + fecha2 + "') and p.id_especialidad= " + especialidad + " ;";
        } else if (!doctor.equalsIgnoreCase("-1")) {
            query_consultas = "SELECT "
                    + "  id,idcita, idplanificacion, pc.rut, (pc.nombre || '-' || pc.apellido_paterno) as nombrepaciente, "
                    + " pc.contacto1, pc.contacto2,pc.email, pc.direccion, p.fecha, p.rut_doctor , "
                    + " (d.nombre ||'-'||d.apellido_paterno) as nombredoctor, a.nombre as tipocita,"
                    + "  p.fecha as fechacita , o.hora, b.motivo as motivo"
                    + "  FROM  "
                    + "  agenda.citascanceladasporbloqueo cc inner join agenda.cita c on cc.idcita = c.id_cita "
                    + "   join agenda.planificar p on p.id_planificacion = cc.idplanificacion  "
                    + "      join agenda.paciente pc on c.rut_paciente =  pc.rut "
                    + "    join agenda.doctor d on p.rut_doctor = d.rut  "
                    + "    join agenda.atencion a on p.id_atencion = a.id_atencion "
                    + "   join agenda.oferta o on c.id_oferta = o.id_oferta "
                    + "   join agenda.bloqueo b on p.id_planificacion = b.id_cupo"
                    + "  where (p.fecha between '" + fecha1 + "' and '" + fecha2 + "') and p.id_especialidad= " + especialidad + " and p.rut_doctor = '" + doctor + "' ;";
        } else {
            query_consultas = "SELECT "
                    + "  id,idcita, idplanificacion, pc.rut, (pc.nombre || '-' || pc.apellido_paterno) as nombrepaciente, "
                    + " pc.contacto1, pc.contacto2,pc.email, pc.direccion, p.fecha, p.rut_doctor , "
                    + " (d.nombre ||'-'||d.apellido_paterno) as nombredoctor, a.nombre as tipocita,"
                    + "  p.fecha as fechacita, o.hora, b.motivo as motivo"
                    + "  FROM  "
                    + "  agenda.citascanceladasporbloqueo cc inner join agenda.cita c on cc.idcita = c.id_cita "
                    + "   join agenda.planificar p on p.id_planificacion = cc.idplanificacion  "
                    + "   join agenda.paciente pc on c.rut_paciente =  pc.rut "
                    + "   join agenda.doctor d on p.rut_doctor = d.rut  "
                    + "   join agenda.atencion a on p.id_atencion = a.id_atencion "
                    + "   join agenda.oferta o on c.id_oferta = o.id_oferta "
                    + "   join agenda.bloqueo b on p.id_planificacion = b.id_cupo"
                    + "  where (p.fecha between '" + fecha1 + "' and '" + fecha2 + "')  ;";

        }
        this.cnn.setSentenciaSQL(query_consultas);

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita a = new cita();
                a.setRut_paciente(this.cnn.getRst().getString("rut"));
                a.setTemporales(this.cnn.getRst().getString("nombrepaciente"));
                a.setTemporales1(this.cnn.getRst().getString("contacto1"));
                a.setTemporales2(this.cnn.getRst().getString("contacto2"));
                a.setTemporales3(this.cnn.getRst().getString("email"));
                a.setTemporales4(this.cnn.getRst().getString("direccion"));
                a.setTemporales5(this.cnn.getRst().getString("tipocita"));
                a.setTemporales6(this.cnn.getRst().getString("nombredoctor"));
                a.setFecha(this.cnn.getRst().getDate("fechacita"));
                a.setTemporales7(this.cnn.getRst().getString("hora"));
                a.setTemporales8(this.cnn.getRst().getString("motivo"));
                atenciones.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return atenciones;
    }

    /*dealle de citas que me falta*/
    public Vector<cita> citasPosterioresaunaNsp(String rut, Date fecha, int especialidad) {
        Vector<cita> cts = new Vector<cita>();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT to_char(p.fecha, 'dd/mm/yyyy') as fecha, o.hora , case when (p.fecha< CURRENT_DATE and  c.estatus = 1) then 'NSP'            \n"
                + "                          else case when  c.estatus = 1 then 'Cita Asignada' \n"
                + "                          else case when c.estatus = 0 then 'Cita Cancelada' \n"
                + "                          else case when c.estatus = 2 then 'Cita Recepcionada'\n"
                + "                           else case when c.estatus = 3 then 'Cita Recepcionada y cancelada'\n"
                + "                           else case when c.estatus = 4 then 'Cita Terminada Satisfactoriamente'\n"
                + "                           else case when c.estatus = 5 then 'Cita en Proceso'\n"
                + "                            else 'Cita cancelada por el Profesional' end end end end end end end as estatus FROM agenda.cita c\n"
                + "   JOIN agenda.oferta o ON c.id_oferta = o.id_oferta\n"
                + "   JOIN agenda.planificar p ON o.id_plani_sobre = p.id_planificacion\n"
                + "   WHERE c.rut_paciente= '" + rut + "' and p.fecha>'" + fecha + "' and p.id_especialidad=" + especialidad + " order by p.fecha");

        this.cnn.conectar();
        try {
            while (this.cnn.getRst().next()) {
                cita c = new cita();
                c.setTemporales(this.cnn.getRst().getString("fecha"));
                c.setTemporales1(this.cnn.getRst().getString("hora"));
                c.setTemporales2(this.cnn.getRst().getString("estatus"));
                cts.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return cts;
    }

}
