/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Informe;

import Controlador.controlador_atencion;
import Modelos.atencion;
import Modelos.pdfconpieyfoto;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class InformeAtencionOftalmologia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
       
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        String idatencion = request.getParameter("idatencion");
        Locale currentLocale = new Locale("es", "CL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        PdfWriter writer = PdfWriter.getInstance(document, buffer);
        controlador_atencion ca = new controlador_atencion();
        atencion aten = ca.BuscarAtencionparaInformedePrestacionOftalmologia(Integer.parseInt(idatencion));
        int atencion = Integer.parseInt(idatencion);
        int SPACE_TITULO = 10;

        int SPACE_NORMAL2 = 17;
        int SPACE_ESPACIO = 4;

        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.UNDEFINED, new Color(0, 0, 0));
        Font TEXT_NORMAL3 = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));

        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));

        Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new Color(68, 114, 196));

        writer.setPageEvent(new pdfconpieyfoto());
        document.open();

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        String serviciot = "";
        String establecimientot = "";
        String especialidadt = "";
         String complemento = "INFORME DE ATENCIÓN";
        if (aten.getIdatencion()==4){
        complemento = "INFORME PROCEDIMIENTO";
        }
        

        serviciot = " Metropolitano Central(SSMC)";
        establecimientot = " Centro de Referencia de Salud de Maipú";
        especialidadt = " ";
       

        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        Paragraph rutCita = new Paragraph();
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, "Fecha :", TEXT_NORMAL));
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, formateadorFecha.format(aten.getFecha_registro()), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutCita);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);

        celda = new Cell(new Phrase(SPACE_TITULO, complemento, TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
         if(aten.getIdatencion()==4){
        Paragraph prestacion = new Paragraph();
        prestacion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        prestacion.add(new Phrase(SPACE_TITULO, aten.getPrestacion(), TEXT_NORMAL2));
        prestacion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        prestacion.add(new Phrase(SPACE_TITULO, "", TEXT_NORMAL3));
        celda = new Cell(prestacion);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);
        }

        /**/
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "    DATOS DEL PACIENTE", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "___________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa1 = new Paragraph();
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, aten.getRutpaciente() + "                 NOMBRE Y APELLIDOS:   " + aten.getNombrepaciente() + "         EDAD: " + aten.getEdadpaciente() + " Años", TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*Datos de la atencion*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*coloco la informacion de la prestacion*/
       

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        Paragraph hipo = new Paragraph();
        hipo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        hipo.add(new Phrase(SPACE_TITULO, "RESUMEN ATENCIÓN: ", TEXT_NORMAL2));
        hipo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        hipo.add(new Phrase(SPACE_TITULO, aten.getResumenatenicon(), TEXT_NORMAL3));
        celda = new Cell(hipo);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*BUSCO DIAGNOSTICO-INDICACIONES-PROCEDIMIENTO-EXAMENES*/
        Vector<atencion> diagnosticos = ca.BuscarDiagnosticosdeAtencionOftalmologica(atencion);
        String d = "";
        Vector<atencion> procedimiento = ca.BuscarProcedimientosdeAtencionOftalmologica(atencion);
        String p = "";
        Vector<atencion> examenes = ca.BuscarExamenesparaAtencionOftalmologica(atencion);
        String e = "";
        Vector<atencion> indicaciones = ca.BuscarIndicacionesparaAtencionenBox(atencion);
        String in = "";

        if (diagnosticos.size() > 0) {
            for (int i = 0; i < diagnosticos.size(); ++i) {
                d = d + diagnosticos.get(i).getConf()+"  "+ diagnosticos.get(i).getOtrodiagnostico() + " " + diagnosticos.get(i).getLateralidads() + " " + diagnosticos.get(i).getCasoges() + " ; ";
            }
            Paragraph diagnos = new Paragraph();
            diagnos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnos.add(new Phrase(SPACE_TITULO, "DIAGNOSTICO: ", TEXT_NORMAL2));
            diagnos.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnos.add(new Phrase(SPACE_TITULO, d, TEXT_NORMAL3));
            celda = new Cell(diagnos);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
        }
        if (procedimiento.size() > 0) {
            for (int j = 0; j < procedimiento.size(); ++j) {
                p = p + procedimiento.get(j).getOtroprocedimiento() +" ; ";
            }
            Paragraph proce = new Paragraph();
            proce.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            proce.add(new Phrase(SPACE_TITULO, "PROCEDIMIENTOS : ", TEXT_NORMAL2));
            proce.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            proce.add(new Phrase(SPACE_TITULO, p, TEXT_NORMAL3));
            celda = new Cell(proce);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
        }

        if (examenes.size() > 0) {
            for (int l = 0; l < examenes.size(); ++l) {
                e = e + examenes.get(l).getOtroexamenes()+" ; ";
            }

            Paragraph exa = new Paragraph();
            exa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            exa.add(new Phrase(SPACE_TITULO, "EXAMENES : ", TEXT_NORMAL2));
            exa.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            exa.add(new Phrase(SPACE_TITULO, e, TEXT_NORMAL3));
            celda = new Cell(exa);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
        }

        if (indicaciones.size() > 0) {
            for (int m = 0; m < indicaciones.size(); ++m) {
                in = in + indicaciones.get(m).getOtrasindicaciones() +" ; ";
            }
            Paragraph indi = new Paragraph();
            indi.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            indi.add(new Phrase(SPACE_TITULO, "INDICACIONES : ", TEXT_NORMAL2));
            indi.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            indi.add(new Phrase(SPACE_TITULO, in, TEXT_NORMAL3));
            celda = new Cell(indi);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
        }

        if (aten.getIndicalente() == 1) {
            Paragraph diagnostico = new Paragraph();
            diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnostico.add(new Phrase(SPACE_TITULO, "INDICA LENTE : ", TEXT_NORMAL2));
            diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
            diagnostico.add(new Phrase(SPACE_TITULO, aten.getIndicalenteS(), TEXT_NORMAL3));
            celda = new Cell(diagnostico);

            celda.setBorderWidth(0);
            celda.setColspan(3);
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabla_titulo.addCell(celda);
        }

        if(aten.getIddestinopaciente() != 0){
        
        Paragraph variable = new Paragraph();
        variable.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable.add(new Phrase(SPACE_TITULO, "DESTINO PACIENTE: ", TEXT_NORMAL2));
        variable.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable.add(new Phrase(SPACE_TITULO, aten.getDetallederivacion(), TEXT_NORMAL3));
        celda = new Cell(variable);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        }
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph profesional = new Paragraph();
        profesional.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesional.add(new Phrase(SPACE_TITULO, "    PROFESIONAL ", TEXT_SUPERTITULO));
        profesional.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        //profesional.add(new Phrase(SPACE_TITULO, vr.getMotivoconsulta(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(profesional);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "___________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph nombredoctor = new Paragraph();
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, "NOMBRE PROFESIONAL: ", TEXT_NORMAL));
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, aten.getRutdoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombredoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph profesion = new Paragraph();
        profesion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesion.add(new Phrase(SPACE_TITULO, "PROFESIÓN: ", TEXT_NORMAL));
        profesion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesion.add(new Phrase(SPACE_TITULO, aten.getProfesion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(profesion);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        document.add(tabla_titulo);

        document.close();
        try {

            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }

        } catch (Exception exstream) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(InformeAtencionOftalmologia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(InformeAtencionOftalmologia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
