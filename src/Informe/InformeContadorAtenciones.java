/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Informe;

import Controlador.General;
import Controlador.controlador_paciente;
import Modelos.cita;
import Modelos.doctor;
import Modelos.paciente;
import Modelos.protocolo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author a
 */
public class InformeContadorAtenciones extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, WriteException {
        Date fecha_del_dia = new Date();
        Locale currentLocale = new Locale("es", "CL");
        java.text.DateFormat formateadorFecha = java.text.DateFormat.getDateInstance(java.text.DateFormat.MEDIUM, currentLocale);

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=InformeSupremo" + formateadorFecha.format(fecha_del_dia) + ".xls");
        WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
        HSSFWorkbook wb = new HSSFWorkbook();
        int cont = 0;

        WritableSheet sheet = workbook.createSheet("nada", cont);
        WritableCellFormat formatoCodigobarra = new WritableCellFormat(NumberFormats.INTEGER);
        WritableFont tahoma16font = new WritableFont(WritableFont.createFont("Calibri"), 22, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.LIGHT_BLUE);
        WritableCellFormat formatoTitulos = new WritableCellFormat(tahoma16font);
        formatoTitulos.setAlignment(Alignment.CENTRE);
        formatoTitulos.setVerticalAlignment(jxl.write.VerticalAlignment.CENTRE);
        formatoTitulos.setBackground(Colour.WHITE);

//formatoTitulos.setShrinkToFit(true);
        WritableFont tahoma13font = new WritableFont(WritableFont.ARIAL, 13, WritableFont.BOLD, false);
        WritableCellFormat formatoCeldaTitulos = new WritableCellFormat(tahoma13font);

        WritableFont Item = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.WHITE);
        WritableCellFormat FormatoItem = new WritableCellFormat(Item);
        FormatoItem.setBorder(Border.ALL, BorderLineStyle.NONE);
        FormatoItem.setAlignment(Alignment.CENTRE);
        FormatoItem.setBackground(Colour.LIGHT_BLUE);
        FormatoItem.setWrap(true);

        WritableFont Datos = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatos = new WritableCellFormat(Datos);
        FormatoDatos.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatos.setAlignment(Alignment.LEFT);
        FormatoDatos.setBackground(Colour.WHITE);

        WritableFont Datoshitos = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatoshitos = new WritableCellFormat(Datoshitos);
        FormatoDatoshitos.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatoshitos.setAlignment(Alignment.LEFT);
        FormatoDatoshitos.setBackground(Colour.LIGHT_BLUE);

        WritableFont Datoscitas = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatoscitas = new WritableCellFormat(Datoscitas);
        FormatoDatoscitas.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatoscitas.setAlignment(Alignment.LEFT);
        FormatoDatoscitas.setBackground(Colour.LIGHT_GREEN);

        WritableFont Datosprotocolos = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatosprotocolos = new WritableCellFormat(Datosprotocolos);
        FormatoDatosprotocolos.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatosprotocolos.setAlignment(Alignment.LEFT);
        FormatoDatosprotocolos.setBackground(Colour.LIGHT_ORANGE);

        WritableFont Datosprestaciones = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatosprestaciones = new WritableCellFormat(Datosprestaciones);
        FormatoDatosprestaciones.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatosprestaciones.setAlignment(Alignment.LEFT);
        FormatoDatosprestaciones.setBackground(Colour.WHITE);
        FormatoDatosprestaciones.setWrap(true);

        WritableFont Datos_centrado = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.NO_BOLD, false);
        WritableCellFormat FormatoDatos_centrado = new WritableCellFormat(Datos_centrado);
        FormatoDatos_centrado.setBorder(Border.ALL, BorderLineStyle.THIN);
        FormatoDatos_centrado.setAlignment(Alignment.CENTRE);
        FormatoDatos_centrado.setBackground(Colour.WHITE);

        WritableFont DatosFecha = new WritableFont(WritableFont.TAHOMA, 8, WritableFont.NO_BOLD, false, UnderlineStyle.SINGLE, jxl.format.Colour.LIGHT_BLUE);
        WritableCellFormat FormatoDatosFechaGeneracion = new WritableCellFormat(DatosFecha);
        FormatoDatosFechaGeneracion.setAlignment(Alignment.LEFT);
        FormatoDatosFechaGeneracion.setBackground(Colour.WHITE);
        Label fechaGeneracion = new Label(0, 0, "Informe Generado el " + formateadorFecha.format(fecha_del_dia), FormatoDatosFechaGeneracion);
        sheet.addCell(fechaGeneracion);
        sheet.mergeCells(0, 0, 1, 2);

        try {
            General g = new General();
            int colum = 3;
            for (doctor doc : g.buscarTodaslasatencionesAntigua()) {

                Label etiqueta_num = new Label(0, colum, doc.getApellido_materno(), FormatoDatos);
                sheet.addCell(etiqueta_num);
                sheet.setColumnView(0, 20);
                Label rut_profesiona = new Label(1, colum, doc.getRut(), FormatoDatos);
                sheet.addCell(rut_profesiona);
                sheet.setColumnView(1, 20);

                Label nombre_profesional = new Label(2, colum, doc.getApellido_paterno(), FormatoDatos);
                sheet.addCell(nombre_profesional);
                sheet.setColumnView(2, 20);
                int cantidad = 0;

                int lugar = 3;
                for (int k = 1; k < 13; ++k) {
                    String atencion = "1";
                    String estatuscita = "1,6";
                    cantidad = g.BuscarContadoresantigua(doc.getEstatus(), doc.getRut(), k, atencion, estatuscita);
                    Label nuevos;
                    nuevos = new Label(lugar, colum, String.valueOf(cantidad) , FormatoDatoscitas);
                    sheet.addCell(nuevos);
                    sheet.setColumnView(3, 18);
                    lugar++;

                    
                    atencion = "2";
                   estatuscita = "1,6";
                    cantidad = g.BuscarContadoresantigua(doc.getEstatus(), doc.getRut(), k, atencion, estatuscita);
                    
                    Label num_pagare = new Label(lugar, colum, String.valueOf(cantidad), FormatoDatosprestaciones);
                    sheet.addCell(num_pagare);
                    sheet.setColumnView(4, 35);
                    lugar++;
                    
                     atencion = "1";
                     estatuscita = "0";
                    cantidad = g.BuscarContadoresantigua(doc.getEstatus(), doc.getRut(), k, atencion, estatuscita);
                    
                    Label fechahito = new Label(lugar, colum, String.valueOf(cantidad), FormatoDatoscitas);
                    sheet.addCell(fechahito);
                    sheet.setColumnView(5, 35);
                    lugar++;
                    
                    
                     atencion = "2";
                   estatuscita = "0";
                    cantidad = g.BuscarContadoresantigua(doc.getEstatus(), doc.getRut(), k, atencion, estatuscita);
                    
                    
                    Label nsp = new Label(lugar, colum, String.valueOf(cantidad), FormatoDatosprestaciones);
                    sheet.addCell(nsp);
                    sheet.setColumnView(6, 35);
                    lugar++;
                }

                colum++;

            }

        } catch (Exception ex) {
        }
        workbook.write();
        workbook.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (WriteException ex) {
            Logger.getLogger(InformeContadorAtenciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (WriteException ex) {
            Logger.getLogger(InformeContadorAtenciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
