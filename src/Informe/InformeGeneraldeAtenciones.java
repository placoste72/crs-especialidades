/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Informe;

import Controlador.controlador_cita;
import Controlador.controlador_especialidad;
import Controlador.controlador_examenes;
import Modelos.cita;
import Modelos.lugar;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

/**
 *
 * @author a
 */
public class InformeGeneraldeAtenciones extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        /*parametros de entrada*/
        String inicio = request.getParameter("inicio");
        String fin = request.getParameter("fin");

        Date fecha1 = new Date(Integer.parseInt(inicio.substring(6, 10)) - 1900, Integer.parseInt(inicio.substring(3, 5)) - 1, Integer.parseInt(inicio.substring(0, 2)), 0, 0, 0);
        Date fecha2 = new Date(Integer.parseInt(fin.substring(6, 10)) - 1900, Integer.parseInt(fin.substring(3, 5)) - 1, Integer.parseInt(fin.substring(0, 2)), 0, 0, 0);

        String espe = request.getParameter("especialidad");
        String doctor = request.getParameter("doctor");
        /*coloco el where */

 /**/
        Locale currentLocale = new Locale("es", "CL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        DateFormat formateadorFecha2 = DateFormat.getDateInstance(DateFormat.SHORT, currentLocale);
        Locale currentLocale2 = new Locale("es", "Ch");
        java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.DEFAULT, currentLocale2);
        Locale currentLocale3 = new Locale("en", "US");
        java.text.DateFormat formateadorFechaIngles = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT, currentLocale3);
        controlador_examenes ce = new controlador_examenes();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=citas_paciente.xls");

//Creamos el archivo de excell
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet1 = wb.createSheet("                                                                                                        NOMINA PACIENTE CON CITAS CANCELADAS POR BLOQUEO DE CUPO");

// ***************************************[CREO TOTDOS LOS FONT Y LOS STYLE QUE NECESITE]****************************************/
        HSSFFont fontDatos = wb.createFont();
        fontDatos.setFontHeightInPoints((short) 12);
        fontDatos.setFontName("Vereda");
        fontDatos.setColor((short) HSSFColor.LIGHT_BLUE.index);
        HSSFCellStyle style_datos = wb.createCellStyle();
        style_datos.setFont(fontDatos);
        style_datos.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        style_datos.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
        style_datos.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        style_datos.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_datos.setWrapText(true);

        /*otro*/
 /*para la linea*/
        HSSFFont fontDatos2 = wb.createFont();
        fontDatos2.setFontHeightInPoints((short) 12);
        fontDatos2.setFontName("Vereda");
        fontDatos2.setColor((short) HSSFColor.LIGHT_BLUE.index);
        HSSFCellStyle style_datos2 = wb.createCellStyle();
        style_datos2.setFont(fontDatos2);

        style_datos2.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
        style_datos2.setWrapText(true);
        /**/

        controlador_cita cc = new controlador_cita();
        HSSFFont fontTitulo = wb.createFont();
        fontTitulo.setFontHeightInPoints((short) 14);
        fontTitulo.setFontName("Calibri");
        fontTitulo.setColor((short) HSSFColor.INDIGO.index);

        fontTitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_Titulo = wb.createCellStyle();
        style_Titulo.setFont(fontTitulo);
        style_Titulo.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);

        /*ESTILO PARA LA FECHA*/
        HSSFFont fontfecha = wb.createFont();
        fontfecha.setFontHeightInPoints((short) 18);
        fontfecha.setFontName("Calibri");
        fontfecha.setColor((short) HSSFColor.INDIGO.index);

        fontfecha.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_fecha = wb.createCellStyle();
        style_fecha.setFont(fontfecha);
        style_fecha.setAlignment((short) HSSFCellStyle.ALIGN_RIGHT);;

        /**
         * **********************************************
         */
        HSSFFont fontSubtitulo = wb.createFont();
        HSSFCellStyle style_Subtitulo = wb.createCellStyle();

        fontSubtitulo.setFontHeightInPoints((short) 14);
        fontSubtitulo.setFontName("Calibri");
        fontSubtitulo.setColor((short) HSSFColor.WHITE.index);

        fontSubtitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style_Subtitulo.setFont(fontSubtitulo);
        style_Subtitulo.setWrapText(true);
        style_Subtitulo.setAlignment((short) HSSFCellStyle.ALIGN_CENTER);

        style_Subtitulo.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        style_Subtitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_Subtitulo.setFillBackgroundColor(new HSSFColor.AQUA().getIndex());
        style_Subtitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        /*estilo para la linea*/
        HSSFFont linea = wb.createFont();
        HSSFCellStyle stylelinea = wb.createCellStyle();
        linea.setFontHeightInPoints((short) 50);
        linea.setFontName("Calibri");
        linea.setColor((short) HSSFColor.LIGHT_BLUE.index);
        linea.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        stylelinea.setFont(linea);
        stylelinea.setWrapText(true);
        stylelinea.setAlignment((short) HSSFCellStyle.ALIGN_CENTER);
        stylelinea.setFillBackgroundColor(new HSSFColor.AQUA().getIndex());
        stylelinea.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        /**
         * **********************************************
         */
        HSSFFont fontSubtitulo2 = wb.createFont();
        HSSFCellStyle style_Subtitulo2 = wb.createCellStyle();

        fontSubtitulo2.setFontHeightInPoints((short) 14);
        fontSubtitulo2.setFontName("Calibri");
        fontSubtitulo2.setColor((short) HSSFColor.WHITE.index);
        //fontSubtitulo.setItalic(true);
        fontSubtitulo2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style_Subtitulo2.setFont(fontSubtitulo);
        style_Subtitulo2.setWrapText(true);
        style_Subtitulo2.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);
        style_Subtitulo2.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        style_Subtitulo2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_Subtitulo2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style_Subtitulo2.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo2.setBottomBorderColor(new HSSFColor.LIGHT_BLUE().getIndex());
        //style_Subtitulo2.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        //  style_Subtitulo2.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo2.setFont(fontSubtitulo2);
        //style_Subtitulo2.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);

        /*linea*/
        /**
         * **********************************************
         */
        HSSFFont fontSubtitulo3 = wb.createFont();
        HSSFCellStyle style_Subtitulo3 = wb.createCellStyle();

        fontSubtitulo3.setFontHeightInPoints((short) 14);
        fontSubtitulo3.setFontName("Calibri");
        fontSubtitulo3.setColor((short) HSSFColor.WHITE.index);
        //fontSubtitulo.setItalic(true);
        fontSubtitulo3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style_Subtitulo3.setFont(fontSubtitulo3);
        style_Subtitulo3.setWrapText(true);
        style_Subtitulo3.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);
        //style_Subtitulo3.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        //style_Subtitulo3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_Subtitulo3.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style_Subtitulo3.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo3.setTopBorderColor(new HSSFColor.LIGHT_BLUE().getIndex());
        //style_Subtitulo2.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        //  style_Subtitulo2.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo3.setFont(fontSubtitulo3);
        //
        controlador_especialidad ces = new controlador_especialidad();

//Esta es el Header y el Footer
        HSSFHeader cabecera = sheet1.getHeader();
        cabecera.setLeft("citasconatenciones");
        HSSFFooter pie = sheet1.getFooter();
        pie.setCenter("Pagina " + HSSFFooter.page() + " de " + HSSFFooter.numPages());//N° de Pagina
        /**/

        sheet1.createFreezePane(0, 7);

//*********************************************************************************************
// Sheet1.createFreezePane (3, 2, 3, 2);
        //  sheet1.createFreezePane(1, 5);
        /**
         * ***********************[ESTE ES TODO EL
         * ENCABEZADO]**************************************
         */
        HSSFRow rowTitulo = sheet1.createRow((short) 3); //Esta es la primera fila de la hoja
        HSSFRow rowHoy = sheet1.createRow((short) 1); //esta el la segunda
        HSSFRow rowlinea = sheet1.createRow((short) 4);
        HSSFRow rowlineafecha = sheet1.createRow((short) 2);
        HSSFCell cellTitulo = rowTitulo.createCell((short) 0); //TITULO
        cellTitulo.setCellValue("                        INFORME CITAS DE PACIENTE");
        cellTitulo.setCellStyle(style_Titulo);//le doy el estilo
        HSSFCell celllinea = rowlinea.createCell((short) 0);
        HSSFCell celllineafecha = rowlineafecha.createCell((short) 21);

        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 0); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 1); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 2); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 3); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 4); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 5); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 6); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 7); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 8); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 9); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 10); //crea la celda tipo
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 11);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 12);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 13);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 14);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 15);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 16);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 17);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 18);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 19);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 20);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 21);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 22);
        celllinea.setCellValue("");
        celllinea.setCellStyle(style_Subtitulo3);
        celllinea = rowlinea.createCell((short) 23);

        HSSFCell cellHoy = rowHoy.createCell((short) 22); //FECHA DE GEERACION DEL INFORME
        cellHoy.setCellValue("FECHA: " + formateadorFecha.format(new Date()));
        cellHoy.setCellStyle(style_fecha);//le doy el estilo  

        celllineafecha.setCellValue("");
        celllineafecha.setCellStyle(style_Subtitulo3);
        celllineafecha = rowlineafecha.createCell((short) 21); //crea la celda tipo
        celllineafecha.setCellValue("");
        celllineafecha.setCellStyle(style_Subtitulo3);
        celllineafecha = rowlineafecha.createCell((short) 22); //crea la celda tipo
        celllineafecha.setCellValue("");
        celllineafecha.setCellStyle(style_Subtitulo3);
        celllineafecha = rowlineafecha.createCell((short) 23);
        celllineafecha.setCellValue("");
        celllineafecha.setCellStyle(style_Subtitulo3);
        celllineafecha = rowlineafecha.createCell((short) 24);

        Region region = new Region(1, (short) 0, 1, (short) 7);
        sheet1.addMergedRegion(region); //es para combinar celdas

        sheet1.setDisplayGridlines(false);
        sheet1.createFreezePane(0, 7);

        short controlFila = 5;
        HSSFRow row1 = sheet1.createRow(controlFila); //crea el renglon o fila
        HSSFCell cell0 = row1.createCell((short) 0); //crea la celda tipo
        //voy a crear aqui el primer casilla

        cell0.setCellValue("DATOS DEL PACIENTE");
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 1); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 2); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 3); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 4); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 5); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 6); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 7); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 8); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 9); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 10); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);

        cell0.setCellValue("CITACIÓN");

        //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 11); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 12); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 13); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 14); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 15); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 16); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 17); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 18); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);

        cell0.setCellValue("ATENCION");
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 19); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 20); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 21); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 22); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 23); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);
        cell0 = row1.createCell((short) 24); //crea la celda tipo

        //segunda parte   
        region = new Region(controlFila, (short) 0, controlFila, (short) 9);
        sheet1.addMergedRegion(region); //es para combinar celdas
        region = new Region(controlFila, (short) 11, controlFila, (short) 18);
        sheet1.addMergedRegion(region); //es para combinar celdas
        region = new Region(controlFila, (short) 19, controlFila, (short) 24);
        sheet1.addMergedRegion(region); //es para combinar celdas

        controlFila++;

        row1 = sheet1.createRow(controlFila);
        cell0 = row1.createCell((short) 0); //crea la celda tipo
        cell0.setCellStyle(style_Subtitulo);

        cell0.setCellValue("RUT PACIENTE");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 1); //crea la celda tipo
        cell0.setCellValue("NOMBRE");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 2); //crea la celda tipo
        cell0.setCellValue("APELLIDO PATERNO");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 3); //crea la celda tipo
        cell0.setCellValue("APELLIDO MATERNO");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 4); //crea la celda tipo
        cell0.setCellValue("GENERO");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 5); //crea la celda tipo
        cell0.setCellValue("EDAD");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 6); //crea la celda tipo
        cell0.setCellValue("TELEFONO 1");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 7); //crea la celda tipo
        cell0.setCellValue("TELEFONO 2");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 8); //crea la celda tipo
        cell0.setCellValue("DIRECCION");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 9); //crea la celda tipo
        cell0.setCellValue("PREVISION");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 10); //crea la celda tipo
        /**/
        cell0.setCellValue("FECHA CITA");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 11); //crea la celda tipo
        cell0.setCellValue("HORA CITA");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 12); //crea la celda tipo
        cell0.setCellValue("ESPECIALIDAD");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 13); //crea la celda tipo
        cell0.setCellValue("PROFESIONAL");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 14); //crea la celda tipo
        cell0.setCellValue("TIPO CITA");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 15); //crea la celda tipo
        cell0.setCellValue("ESTATUS");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 16); //crea la celda tipo
        cell0.setCellValue("PRESTACION");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 17); //crea la celda tipo
        cell0.setCellValue("GES");
         cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 18); //crea la celda tipo
        cell0.setCellValue("HORA RECEPCION");

        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 19); //crea la celda tipo
        cell0.setCellValue("HORA ATENCION");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 20); //crea la celda tipo
        cell0.setCellValue("DIAGNOSTICO");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 21); //crea la celda tipo
        cell0.setCellValue("TRATAMIENTO O INDICACIONES");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 22); //crea la celda tipo
        cell0.setCellValue("EXAMENES");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 23); //crea la celda tipo
        cell0.setCellValue("PROCEDIMIENTO");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 24);
        cell0.setCellValue("OBSERVACION");
        cell0.setCellStyle(style_Subtitulo2);
        cell0 = row1.createCell((short) 25);
        //crea la celda tipo

        controlFila++;

//******************************************************  muestra pacientes  ******************************************
        String error = "";
        Vector<cita> temp = cc.BuscarCitasAtendidasparaInformeEstadistico(fecha1, fecha2, doctor, Integer.parseInt(espe));

        for (int k = 0; k < temp.size(); ++k) {

            try {
                row1 = sheet1.createRow(controlFila); //crea el renglon o fila
            } catch (Exception errorEx) {
                error = String.valueOf(errorEx);

            }

            cell0 = row1.createCell((short) 0); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getRut_paciente());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 1); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 2); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales1());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 3); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales2());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 4); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales3());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 5); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales4() + "  Años");
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 6); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales5());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 7); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales6());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 8); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales7());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 9); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales15());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }
            cell0 = row1.createCell((short) 10); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales8());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 11); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales9());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 12); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales10());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 13); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getRut_doctor());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 14); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales11());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 15); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales12());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 16); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales13());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 17); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales14());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }
            
             cell0 = row1.createCell((short) 18); //crea la celda tipo
            cell0.setCellValue(temp.get(k).getTemporales16());
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            String horaatencion = "";
            String diagnostico = "";
            String tratamiento = "";
            String examenes = "";
            String procedimiento = "";
            String observacion = "";
            String indicaciones = "";

            if (temp.get(k).getEstatus() == 4) {

                lugar vr = cc.buscarATENCIONdeunaCita(temp.get(k).getId_cita());
                horaatencion = vr.getHora();
                diagnostico = vr.getDiagnostico();
                tratamiento = vr.getTratamiento();
                examenes = vr.getExamenes();
                procedimiento = vr.getProcedimientos();
                observacion = vr.getObservaciones();

            }
            cell0 = row1.createCell((short) 19); //crea la celda tipo
            cell0.setCellValue(horaatencion);
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 20); //crea la celda tipo
            cell0.setCellValue(diagnostico);
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 21); //crea la celda tipo
            cell0.setCellValue(tratamiento );
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 22); //crea la celda tipo
            cell0.setCellValue(examenes);
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 23); //crea la celda tipo
            cell0.setCellValue(procedimiento);
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            cell0 = row1.createCell((short) 24); //crea la celda tipo
            cell0.setCellValue(observacion);
            if (k % 2 == 0) {
                cell0.setCellStyle(style_datos);
            } else {
                cell0.setCellStyle(style_datos2);
            }

            controlFila++;

        }

//******************************************************  muestra pacientes  ******************************************
        row1 = sheet1.createRow(controlFila);
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 0); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 1); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 2); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 3); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 4); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 5); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 6); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 7); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 8); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 9); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 10); //crea la celda tipo
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 11);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 12);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 13);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 14);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 15);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 16);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 17);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 18);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 19);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 20);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 21);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 22);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 23);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 24);
        cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 25);
         cell0.setCellValue("");
        cell0.setCellStyle(style_Subtitulo3);
        cell0 = row1.createCell((short) 26);

        sheet1.setColumnWidth((short) 0, (short) 10700);
        sheet1.setColumnWidth((short) 1, (short) 4700);
        sheet1.setColumnWidth((short) 2, (short) 7500);
        sheet1.setColumnWidth((short) 3, (short) 9500);
        sheet1.setColumnWidth((short) 4, (short) 7500);
        sheet1.setColumnWidth((short) 5, (short) 10500);
        sheet1.setColumnWidth((short) 6, (short) 5500);
        sheet1.setColumnWidth((short) 7, (short) 6500);
        sheet1.setColumnWidth((short) 8, (short) 6500);
        sheet1.setColumnWidth((short) 9, (short) 6500);
        sheet1.setColumnWidth((short) 10, (short) 6500);
        sheet1.setColumnWidth((short) 11, (short) 6500);
        sheet1.setColumnWidth((short) 12, (short) 6500);
        sheet1.setColumnWidth((short) 13, (short) 6500);
        sheet1.setColumnWidth((short) 14, (short) 7500);
        sheet1.setColumnWidth((short) 15, (short) 7500);
        sheet1.setColumnWidth((short) 16, (short) 7500);
        sheet1.setColumnWidth((short) 17, (short) 7500);
        sheet1.setColumnWidth((short) 18, (short) 7500);
        sheet1.setColumnWidth((short) 19, (short) 8500);
        sheet1.setColumnWidth((short) 20, (short) 7500);
        sheet1.setColumnWidth((short) 21, (short) 7500);
        sheet1.setColumnWidth((short) 22, (short) 7500);
        sheet1.setColumnWidth((short) 23, (short) 7500);
        sheet1.setColumnWidth((short) 24, (short) 7500);
        sheet1.setColumnWidth((short) 25, (short) 7500);
         sheet1.setColumnWidth((short) 26, (short) 7500);

        //crea la celda tipo
        HSSFPrintSetup printSetup = sheet1.getPrintSetup();
        printSetup.setLandscape(true);
        printSetup.setPaperSize(printSetup.LETTER_PAPERSIZE);
        printSetup.setScale((short) 60);
        /**
         * *******************************************************************************************************************
         */
        wb.write(response.getOutputStream());

        response.flushBuffer();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
