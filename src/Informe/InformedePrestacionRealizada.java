/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Informe;

import Controlador.controlador_atencion;
import Controlador.controlador_doctor;
import Modelos.pdfpagina;
import Modelos.atencion;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.*;
import java.awt.Color;
import java.util.Vector;
import java.util.Date;
import java.text.DateFormat;
import java.util.Locale;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfEncryptor;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Header;
import com.lowagie.text.pdf.PdfWriter;
import java.sql.*;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author a
 */
public class InformedePrestacionRealizada extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException {
        response.setContentType("application/pdf");
          Locale currentLocale = new Locale("es", "CL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
        
        
        Document document = new Document(PageSize.LETTER, 50, 50, 50, 100);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        String idatencion= request.getParameter("idatencion");

        PdfWriter writer = PdfWriter.getInstance(document, buffer);
        controlador_atencion ca = new controlador_atencion();
        atencion aten = ca.BuscarAtencionparaInformedePrestacion(Integer.parseInt(idatencion));

        int SPACE_TITULO = 10;
        int SPACE_NORMAL = 12;
        int SPACE_NORMAL2 = 17;
        int SPACE_ESPACIO = 4;
        Font TEXT = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new Color(255, 255, 255));
        Font TEXT_TITULO = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(68, 117, 196));
        Font TEXT_NORMAL = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_NORMAL2 = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.UNDEFINED, new Color(0, 0, 0));
        Font TEXT_NORMAL3 = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_TITULOSUB = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, new Color(0, 0, 3));
        Font TEXT_SUPERTITULO = FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD, new Color(0, 0, 0));
        Font TEXT_SUPERTITULONORMAL = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new Color(0, 0, 0));
        Font TEXT_CURSI = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new Color(68, 117, 196));
        Font Linea = FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, new Color(68, 114, 196));
        Font fecha1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, new Color(32, 55, 100));

        document.open();

        Table tabla_titulo;
        Cell celda;

        tabla_titulo = new Table(3);
        tabla_titulo.setBorderWidth(0);
        tabla_titulo.setPadding(1);
        tabla_titulo.setSpacing(0);
        tabla_titulo.setWidth(100);

        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        String serviciot = "";
        String establecimientot = "";
        String especialidadt = "";
        String unidadt = "";

        serviciot = " Metropolitano Central(SSMC)";
        establecimientot = " Centro de Referencia de Salud de Maipú";
        especialidadt = " ";
        unidadt = " ";

       

        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        Paragraph rutCita = new Paragraph();
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, "Fecha :", TEXT_NORMAL));
        rutCita.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        rutCita.add(new Phrase(SPACE_TITULO, formateadorFecha.format(aten.getFecha_registro()), TEXT_SUPERTITULONORMAL));
        celda = new Cell(rutCita);
        celda.setBorderWidth(0);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
   
        
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
         celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);

        celda = new Cell(new Phrase(SPACE_TITULO, "INFORME DE ECOGRAFÍA ABDOMINAL", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla_titulo.addCell(celda);

        /**/
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);


        celda = new Cell(new Phrase(SPACE_TITULO, "    DATOS DEL PACIENTE", TEXT_SUPERTITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "___________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph AtencionGlosa1 = new Paragraph();
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "RUT: ", TEXT_NORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, "", TEXT_SUPERTITULONORMAL));
        AtencionGlosa1.add(new Phrase(SPACE_TITULO, aten.getRutpaciente() + "                 NOMBRE Y APELLIDOS:   " + aten.getNombrepaciente()+ "         EDAD: "+aten.getEdadpaciente()+" Años", TEXT_SUPERTITULONORMAL));
        celda = new Cell(AtencionGlosa1);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        /*Datos de la atencion*/
        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

    

        /*coloco la informacion de vicio de refraccion*/
     
        
        Paragraph prestacion = new Paragraph();
        prestacion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        prestacion.add(new Phrase(SPACE_TITULO, aten.getPrestacion(), TEXT_NORMAL2));
        prestacion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        prestacion.add(new Phrase(SPACE_TITULO, "", TEXT_NORMAL3));
        celda = new Cell(prestacion);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        
        celda = new Cell(new Phrase(SPACE_ESPACIO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tabla_titulo.addCell(celda);
        
        Paragraph hipo = new Paragraph();
        hipo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        hipo.add(new Phrase(SPACE_TITULO, "HIPOTESIS DIAGNOSTICA: ", TEXT_NORMAL2));
        hipo.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        hipo.add(new Phrase(SPACE_TITULO, aten.getHipotesis_diagnostica()+ "                      PROFESIONAL SOLICITANTE:  " + aten.getMedico_solicitante(), TEXT_NORMAL3));
        celda = new Cell(hipo);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph diagnostico = new Paragraph();
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, "HIGADO: ", TEXT_NORMAL2));
        diagnostico.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        diagnostico.add(new Phrase(SPACE_TITULO, aten.getHigado(), TEXT_NORMAL3));
        celda = new Cell(diagnostico);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph variable = new Paragraph();
        variable.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable.add(new Phrase(SPACE_TITULO, "VÍA BILIAR: ", TEXT_NORMAL2));
        variable.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable.add(new Phrase(SPACE_TITULO, aten.getVia_biliar(), TEXT_NORMAL3));
        celda = new Cell(variable);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph variable1 = new Paragraph();
        variable1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable1.add(new Phrase(SPACE_TITULO, "VESICULA: ", TEXT_NORMAL2));
        variable1.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable1.add(new Phrase(SPACE_TITULO, aten.getVesicula(), TEXT_NORMAL3));
        celda = new Cell(variable1);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph variable3 = new Paragraph();
        variable3.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable3.add(new Phrase(SPACE_TITULO, "PANCREAS: ", TEXT_NORMAL2));
        variable3.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable3.add(new Phrase(SPACE_TITULO, aten.getPancreas(), TEXT_NORMAL3));
        celda = new Cell(variable3);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph variable4 = new Paragraph();
        variable4.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable4.add(new Phrase(SPACE_TITULO, "BAZO: ", TEXT_NORMAL2));
        variable4.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable4.add(new Phrase(SPACE_TITULO, aten.getOtro_bazo(), TEXT_NORMAL3));
        celda = new Cell(variable4);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph variable5 = new Paragraph();
        variable5.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable5.add(new Phrase(SPACE_TITULO, "AORTA: ", TEXT_NORMAL2));
        variable5.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable5.add(new Phrase(SPACE_TITULO, aten.getOtro_aorta(), TEXT_NORMAL3));
        celda = new Cell(variable5);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        
         Paragraph variable7 = new Paragraph();
        variable7.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable7.add(new Phrase(SPACE_TITULO, "RIÑONES: ", TEXT_NORMAL2));
        variable7.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable7.add(new Phrase(SPACE_TITULO, aten.getRinones(), TEXT_NORMAL3));
        celda = new Cell(variable7);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph variable6 = new Paragraph();
        variable6.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable6.add(new Phrase(SPACE_TITULO, "CONCLUSION: ", TEXT_NORMAL2));
        variable6.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        variable6.add(new Phrase(SPACE_TITULO, aten.getConclusion(), TEXT_NORMAL3));
        celda = new Cell(variable6);

        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        celda = new Cell(new Phrase(SPACE_TITULO, "\n", TEXT_TITULO));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

  

        Paragraph profesional = new Paragraph();
        profesional.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesional.add(new Phrase(SPACE_TITULO, "    PROFESIONAL: ", TEXT_SUPERTITULO));
        profesional.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        //profesional.add(new Phrase(SPACE_TITULO, vr.getMotivoconsulta(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(profesional);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        celda = new Cell(new Phrase(SPACE_TITULO, "___________________________________________", Linea));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        Paragraph nombredoctor = new Paragraph();
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, "NOMBRE PROFESIONAL: ", TEXT_NORMAL));
        nombredoctor.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        nombredoctor.add(new Phrase(SPACE_TITULO, aten.getRutdoctor(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(nombredoctor);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);
        
            Paragraph profesion = new Paragraph();
        profesion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesion.add(new Phrase(SPACE_TITULO, "PROFESIÓN: ", TEXT_NORMAL));
        profesion.add(new Phrase(SPACE_NORMAL2, "", TEXT_SUPERTITULONORMAL));
        profesion.add(new Phrase(SPACE_TITULO, aten.getProfesion(), TEXT_SUPERTITULONORMAL));
        celda = new Cell(profesion);
        // celda.setBackgroundColor(new Color(217, 225, 242));
        celda.setBorderWidth(0);
        celda.setColspan(3);
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        tabla_titulo.addCell(celda);

        document.add(tabla_titulo);

        document.close();
        try {

            DataOutput output = new DataOutputStream(response.getOutputStream());
            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                output.writeByte(bytes[i]);
            }

        } catch (Exception exstream) {
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(InformedePrestacionRealizada.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(InformedePrestacionRealizada.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
