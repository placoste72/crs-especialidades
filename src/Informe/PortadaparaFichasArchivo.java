/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Informe;

import Controlador.controlador_cita;
import Modelos.cita;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

/**
 *
 * @author a
 */
public class PortadaparaFichasArchivo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Locale currentLocale = new Locale("es", "CL");
        Locale currentLocaleHora = new Locale("es", "CHL");
        DateFormat formateadorFecha = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);

        DateFormat formateaHora = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocaleHora);

        String fechainicio = request.getParameter("inicio");
        String fechafin = request.getParameter("fin");
        Date fecha1 = new Date(Integer.parseInt(fechainicio.substring(6, 10)) - 1900, Integer.parseInt(fechainicio.substring(3, 5)) - 1, Integer.parseInt(fechainicio.substring(0, 2)), 0, 0, 0);
        Date fecha2 = new Date(Integer.parseInt(fechafin.substring(6, 10)) - 1900, Integer.parseInt(fechafin.substring(3, 5)) - 1, Integer.parseInt(fechafin.substring(0, 2)), 0, 0, 0);

        String doctor = request.getParameter("doctor");
        String especialidad = request.getParameter("especialidad");

        controlador_cita cc = new controlador_cita();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=ListaCitasporProfesional.xls");
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet1 = wb.createSheet("                                                                                                        NOMINA PACIENTE CON CITAS CANCELADAS POR BLOQUEO DE CUPO");

// ***************************************[CREO TOTDOS LOS FONT Y LOS STYLE QUE NECESITE]****************************************/
        HSSFFont fontDatos = wb.createFont();
        fontDatos.setFontHeightInPoints((short) 8);
        fontDatos.setFontName("Vereda");
        fontDatos.setColor((short) HSSFColor.LIGHT_BLUE.index);
        HSSFCellStyle style_datos = wb.createCellStyle();
        style_datos.setFont(fontDatos);
        style_datos.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        style_datos.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
        style_datos.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        style_datos.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        /*otro*/
 /*para la linea*/
        HSSFFont fontDatos2 = wb.createFont();
        fontDatos2.setFontHeightInPoints((short) 8);
        fontDatos2.setFontName("Vereda");
        fontDatos2.setColor((short) HSSFColor.LIGHT_BLUE.index);
        HSSFCellStyle style_datos2 = wb.createCellStyle();
        style_datos2.setFont(fontDatos2);

        style_datos2.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
        /**/

        HSSFFont fontTitulo = wb.createFont();
        fontTitulo.setFontHeightInPoints((short) 22);
        fontTitulo.setFontName("Calibri");
        fontTitulo.setColor((short) HSSFColor.INDIGO.index);

        fontTitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_Titulo = wb.createCellStyle();
        style_Titulo.setFont(fontTitulo);
        style_Titulo.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);

        /*ESTILO PARA LA FECHA*/
        HSSFFont fontfecha = wb.createFont();
        fontfecha.setFontHeightInPoints((short) 18);
        fontfecha.setFontName("Calibri");
        fontfecha.setColor((short) HSSFColor.INDIGO.index);

        fontfecha.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle style_fecha = wb.createCellStyle();
        style_fecha.setFont(fontfecha);
        style_fecha.setAlignment((short) HSSFCellStyle.ALIGN_RIGHT);;

        /**
         * **********************************************
         */
        HSSFFont fontSubtitulo = wb.createFont();
        HSSFCellStyle style_Subtitulo = wb.createCellStyle();

        fontSubtitulo.setFontHeightInPoints((short) 11);
        fontSubtitulo.setFontName("Calibri");
        fontSubtitulo.setColor((short) HSSFColor.WHITE.index);

        fontSubtitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style_Subtitulo.setFont(fontSubtitulo);
        style_Subtitulo.setWrapText(true);
        style_Subtitulo.setAlignment((short) HSSFCellStyle.ALIGN_CENTER);

        style_Subtitulo.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        style_Subtitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_Subtitulo.setFillBackgroundColor(new HSSFColor.AQUA().getIndex());
        style_Subtitulo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        /*estilo para la linea*/
        HSSFFont linea = wb.createFont();
        HSSFCellStyle stylelinea = wb.createCellStyle();
        linea.setFontHeightInPoints((short) 50);
        linea.setFontName("Calibri");
        linea.setColor((short) HSSFColor.LIGHT_BLUE.index);
        linea.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        stylelinea.setFont(linea);
        stylelinea.setWrapText(true);
        stylelinea.setAlignment((short) HSSFCellStyle.ALIGN_CENTER);
        stylelinea.setFillBackgroundColor(new HSSFColor.AQUA().getIndex());
        stylelinea.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        /**
         * **********************************************
         */
        HSSFFont fontSubtitulo2 = wb.createFont();
        HSSFCellStyle style_Subtitulo2 = wb.createCellStyle();

        fontSubtitulo2.setFontHeightInPoints((short) 11);
        fontSubtitulo2.setFontName("Calibri");
        fontSubtitulo2.setColor((short) HSSFColor.WHITE.index);
        //fontSubtitulo.setItalic(true);
        fontSubtitulo2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style_Subtitulo2.setFont(fontSubtitulo);
        style_Subtitulo2.setWrapText(true);
        style_Subtitulo2.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);
        style_Subtitulo2.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        style_Subtitulo2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_Subtitulo2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style_Subtitulo2.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo2.setBottomBorderColor(new HSSFColor.LIGHT_BLUE().getIndex());
        //style_Subtitulo2.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        //  style_Subtitulo2.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo2.setFont(fontSubtitulo2);
        //style_Subtitulo2.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);

        
        /*ESTILO DE LETRAS PARA TOTALES*/
        
        HSSFFont fontSubtitulo4 = wb.createFont();
        HSSFCellStyle style_Subtitulo4 = wb.createCellStyle();

        fontSubtitulo4.setFontHeightInPoints((short) 9);
        fontSubtitulo4.setFontName("Calibri");
        fontSubtitulo4.setColor(HSSFColor.INDIGO.index);
        //fontSubtitulo.setItalic(true);
       // fontSubtitulo4.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style_Subtitulo4.setFont(fontSubtitulo);
        style_Subtitulo4.setWrapText(true);
        style_Subtitulo4.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);
        style_Subtitulo4.setFillForegroundColor(HSSFColor.WHITE.index);
        style_Subtitulo4.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_Subtitulo4.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style_Subtitulo4.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo4.setBottomBorderColor(new HSSFColor.LIGHT_BLUE().getIndex());
        //style_Subtitulo2.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        //  style_Subtitulo2.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo4.setFont(fontSubtitulo4);
        //style_Subtitulo2.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        
        
        /*linea*/
        /**
         * **********************************************
         */
        HSSFFont fontSubtitulo3 = wb.createFont();
        HSSFCellStyle style_Subtitulo3 = wb.createCellStyle();

        fontSubtitulo3.setFontHeightInPoints((short) 1);
        fontSubtitulo3.setFontName("Calibri");
        fontSubtitulo3.setColor((short) HSSFColor.WHITE.index);
        //fontSubtitulo.setItalic(true);
        fontSubtitulo3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style_Subtitulo3.setFont(fontSubtitulo3);
        style_Subtitulo3.setWrapText(true);
        style_Subtitulo3.setAlignment((short) HSSFCellStyle.ALIGN_LEFT);
        //style_Subtitulo3.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        //style_Subtitulo3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style_Subtitulo3.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style_Subtitulo3.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo3.setTopBorderColor(new HSSFColor.LIGHT_BLUE().getIndex());
        //style_Subtitulo2.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
        //  style_Subtitulo2.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        style_Subtitulo3.setFont(fontSubtitulo3);
        //

//Esta es el Header y el Footer
        HSSFHeader cabecera = sheet1.getHeader();
        cabecera.setLeft("Citas por Profesional");
        HSSFFooter pie = sheet1.getFooter();
        pie.setCenter("Pagina " + HSSFFooter.page() + " de " + HSSFFooter.numPages());//N° de Pagina
        /**/

        sheet1.createFreezePane(0, 7);

//*********************************************************************************************
// Sheet1.createFreezePane (3, 2, 3, 2);
        //  sheet1.createFreezePane(1, 5);
        /**
         * ***********************[ESTE ES TODO EL
         * ENCABEZADO]**************************************
         */
        HSSFRow rowTitulo = sheet1.createRow((short) 3); //Esta es la primera fila de la hoja
        HSSFRow rowHoy = sheet1.createRow((short) 1); //esta el la segunda
        HSSFRow rowlinea = sheet1.createRow((short) 4);
        HSSFRow rowlineafecha = sheet1.createRow((short) 2);
        HSSFCell cellTitulo = rowTitulo.createCell((short) 0); //TITULO
        cellTitulo.setCellValue("LISTADO DE PACIENTES DEL" + fechainicio + " AL " + fechafin + "");
        cellTitulo.setCellStyle(style_Titulo);//le doy el estilo
        HSSFCell celllinea = rowlinea.createCell((short) 0);
        HSSFCell celllineafecha = rowlineafecha.createCell((short) 15);
        int totalNuevos = 0;
        totalNuevos = cc.buscartotalesporatencionporEspecialidadyDoctor(fecha1, fecha2, Integer.parseInt(especialidad), doctor, 1);
        int totalControl = 0;

        totalControl = cc.buscartotalesporatencionporEspecialidadyDoctor(fecha1, fecha2, Integer.parseInt(especialidad), doctor, 2);
        int totalExamenes = 0;
        totalExamenes = cc.buscartotalesporatencionporEspecialidadyDoctor(fecha1, fecha2, Integer.parseInt(especialidad), doctor, 5);
        int totalProcedimientos = 0;
        String error = "";
        totalProcedimientos = cc.buscartotalesporatencionporEspecialidadyDoctor(fecha1, fecha2, Integer.parseInt(especialidad), doctor, 4);
        int totalCI = 0;

        totalCI = cc.buscartotalesporatencionporEspecialidadyDoctor(fecha1, fecha2, Integer.parseInt(especialidad), doctor, 3);
        int tp = cc.totalpacientesparaunDoctorEspecialidadFecha(fecha1, fecha2, Integer.parseInt(especialidad), doctor);

        Vector<cita> dp = cc.buscarCitasdeunaFechaoEspecialidadoDoctro(fecha1, fecha2, Integer.parseInt(especialidad), doctor);
        if (dp.size() > 0) {
            int m = 0;
            short controlFila = 5;
            HSSFRow row1 = sheet1.createRow(controlFila); //crea el renglon o fila
            HSSFCell cell0 = row1.createCell((short) 0);

            celllinea.setCellStyle(style_Subtitulo3);
            celllinea = rowlinea.createCell((short) 0); //crea la celda tipo
            celllinea.setCellValue("TOTAL NUEVOS:" + totalNuevos);
            celllinea.setCellStyle(style_Subtitulo4);
            celllinea = rowlinea.createCell((short) 1); //crea la celda tipo
            celllinea.setCellValue("TOTAL CONTROL:" + totalControl);
            celllinea.setCellStyle(style_Subtitulo4);
            celllinea = rowlinea.createCell((short) 2); //crea la celda tipo
            celllinea.setCellValue("TOTAL EXAMENES: " + totalExamenes);
            celllinea.setCellStyle(style_Subtitulo4);
            celllinea = rowlinea.createCell((short) 3); //crea la celda tipo
            celllinea.setCellValue("TOTAL PROCEDIMIENTOS:" + totalProcedimientos);
            celllinea.setCellStyle(style_Subtitulo4);
            celllinea = rowlinea.createCell((short) 4); //crea la celda tipo
            celllinea.setCellValue("TOTAL CIRUGIAS:"+totalCI);
            celllinea.setCellStyle(style_Subtitulo4);
            celllinea = rowlinea.createCell((short) 5); //crea la celda tipo
            celllinea.setCellValue("TOTAL PACIENTES:"+tp);
            celllinea.setCellStyle(style_Subtitulo4);
            celllinea = rowlinea.createCell((short) 6); //crea la celda tipo
            celllinea.setCellValue("");
            celllinea.setCellStyle(style_Subtitulo4);
            celllinea = rowlinea.createCell((short) 7); //crea la celda tipo

            //crea la celda tipo
            //voy a crear aqui el primer casilla
            controlFila = 6;

            /*informacion*/
            rowlinea = sheet1.createRow(controlFila);
            cell0 = row1.createCell((short) 0);
            celllinea.setCellStyle(style_Subtitulo3);
            celllinea = rowlinea.createCell((short) 0); //crea la celda tipo
            celllinea.setCellValue("");
            celllinea.setCellStyle(style_Subtitulo2);
            celllinea = rowlinea.createCell((short) 1); //crea la celda tipo
            celllinea.setCellValue("Especialidad:" + dp.get(m).getTemporales1());
            celllinea.setCellStyle(style_Subtitulo2);
            celllinea = rowlinea.createCell((short) 2); //crea la celda tipo
            celllinea.setCellValue("");
            celllinea.setCellStyle(style_Subtitulo2);
            celllinea = rowlinea.createCell((short) 3); //crea la celda tipo
            celllinea.setCellValue("");
            celllinea.setCellStyle(style_Subtitulo2);
            celllinea = rowlinea.createCell((short) 4); //crea la celda tipo
            celllinea.setCellValue("PROFESIONAL:" + dp.get(m).getTemporales());
            celllinea.setCellStyle(style_Subtitulo2);
            celllinea = rowlinea.createCell((short) 5); //crea la celda tipo
            celllinea.setCellValue("");
            celllinea.setCellStyle(style_Subtitulo2);
            celllinea = rowlinea.createCell((short) 6); //crea la celda tipo
            celllinea.setCellValue("FECHA:" + formateadorFecha.format(new Date()));
            celllinea.setCellStyle(style_Subtitulo2);
            celllinea = rowlinea.createCell((short) 7); //crea la celda tipo

            //crea la celda tipo
            sheet1.setDisplayGridlines(false);
            sheet1.createFreezePane(0, 8);

            //voy a crear aqui el primer casilla
            controlFila++;

            row1 = sheet1.createRow(controlFila);
            cell0 = row1.createCell((short) 0); //crea la celda tipo
            cell0.setCellStyle(style_Subtitulo);

            cell0.setCellValue("N°");
            cell0.setCellStyle(style_Subtitulo2);
            cell0 = row1.createCell((short) 1); //crea la celda tipo
            cell0.setCellValue("HORA");
            cell0.setCellStyle(style_Subtitulo2);
            cell0 = row1.createCell((short) 2); //crea la celda tipo
            cell0.setCellValue("RUT");
            cell0.setCellStyle(style_Subtitulo2);
            cell0 = row1.createCell((short) 3); //crea la celda tipo
            cell0.setCellValue("EDAD");
            cell0.setCellStyle(style_Subtitulo2);
            cell0 = row1.createCell((short) 4); //crea la celda tipo
            cell0.setCellValue("PACIENTE");
            cell0.setCellStyle(style_Subtitulo2);
            cell0 = row1.createCell((short) 5); //crea la celda tipo
            cell0.setCellValue("TIPO DE ATENCIÓN");
            cell0.setCellStyle(style_Subtitulo2);
            cell0 = row1.createCell((short) 6); //crea la celda tipo
            cell0.setCellValue("INFORMACION RELEVANTE");
            cell0.setCellStyle(style_Subtitulo2);
            cell0 = row1.createCell((short) 7); //crea la celda tipo

            controlFila++;
            int controlLinea = 2;
//******************************************************  muestra pacientes  ******************************************

            int x = 0;
            for (cita doc : cc.buscarCitasdeunaFechaoEspecialidadoDoctro(fecha1, fecha2, Integer.parseInt(especialidad), doctor)) {

                for (cita c : cc.buscarListaPacienteporDiaEspecialidadyDoctro(fecha1, fecha2, Integer.parseInt(especialidad), doctor)) {

                    try {
                        row1 = sheet1.createRow(controlFila); //crea el renglon o fila
                    } catch (Exception errorEx) {
                        error = String.valueOf(errorEx);
                    }
                    cell0 = row1.createCell((short) 0); //crea la celda tipo
                    cell0.setCellValue(String.valueOf(x));
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }
                    cell0 = row1.createCell((short) 1); //crea la celda tipo
                    cell0.setCellValue(c.getTemporales());
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }
                    cell0 = row1.createCell((short) 2); //crea la celda tipo
                    cell0.setCellValue(c.getRut_paciente());
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }

                    cell0 = row1.createCell((short) 3); //crea la celda tipo
                    cell0.setCellValue(c.getTemporales1() + "  Años");
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }

                    cell0 = row1.createCell((short) 4); //crea la celda tipo
                    cell0.setCellValue(c.getTemporales2());
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }

                    cell0 = row1.createCell((short) 5); //crea la celda tipo
                    cell0.setCellValue(doc.getTemporales2());
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }

                    cell0 = row1.createCell((short) 6); //crea la celda tipo
                    cell0.setCellValue("______________________________________");
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }

                    cell0 = row1.createCell((short) 15); //crea la celda tipo
                    cell0.setCellValue("");
                    if (x % 2 == 0) {
                        cell0.setCellStyle(style_datos);
                    } else {
                        cell0.setCellStyle(style_datos2);
                    }

                    controlFila++;
                    controlLinea++;
                    x++;
                }
            }

            row1 = sheet1.createRow(controlFila);
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 0); //crea la celda tipo
            cell0.setCellValue("");
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 1); //crea la celda tipo
            cell0.setCellValue("");
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 2); //crea la celda tipo
            cell0.setCellValue("");
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 3); //crea la celda tipo
            cell0.setCellValue("");
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 4); //crea la celda tipo
            cell0.setCellValue("");
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 5); //crea la celda tipo
            cell0.setCellValue("");
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 6); //crea la celda tipo
            cell0.setCellValue("");
            cell0.setCellStyle(style_Subtitulo3);
            cell0 = row1.createCell((short) 7); //crea la celda tipo
          

            sheet1.setColumnWidth((short) 0, (short) 7500);
            sheet1.setColumnWidth((short) 1, (short) 7500);
            sheet1.setColumnWidth((short) 2, (short) 7500);
            sheet1.setColumnWidth((short) 3, (short) 7500);
            sheet1.setColumnWidth((short) 4, (short) 12500);
            sheet1.setColumnWidth((short) 5, (short) 6500);
            sheet1.setColumnWidth((short) 6, (short) 8000);
            sheet1.setColumnWidth((short) 7, (short) 17500);
            

            //crea la celda tipo
        }
        HSSFPrintSetup printSetup = sheet1.getPrintSetup();
        printSetup.setLandscape(true);
        printSetup.setPaperSize(printSetup.LETTER_PAPERSIZE);
        printSetup.setScale((short) 60);

        wb.write(response.getOutputStream());

        response.flushBuffer();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
