/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cargamasiva;
import Controlador.*;
import Modelos.cita;
import Modelos.doctor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Placoste
 */
public class paciente extends General {
    public cita leer(String id_cita) {
        cita c = new cita();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        
        this.cnn.setSentenciaSQL("select \n"
                + " d.rut || '/' || d.nombre ||' '|| d.apellido_paterno as doctor,  \n"
                + "  to_char(pl.fecha, 'dd/mm/yyyy' ) as fechacita ,\n"
                + " p.rut || '/' || p.nombre ||' '|| p.apellido_paterno as paciente,\n"
                + " o.hora ,\n"
                + " c.usuario, (select re.nombre ||' '|| re.apellidop  from seguridad.registrousuario re where re.rut = c.usuario ) as nombreusuario,\n"
                + " to_char(c.fecha, 'dd/mm/yyyy' ) as fecha \n"
                + " from agenda.cita c inner join agenda.paciente p on p.rut = c.rut_paciente\n"
                + " join agenda.oferta o on o.id_oferta = c.id_oferta\n"
                + " join agenda.planificar pl on pl.id_planificacion = o.id_plani_sobre\n"
                + " join agenda.doctor d on d.rut = pl.rut_doctor\n"
                + "  where c.id_cita = " + id_cita + "");
        this.cnn.conectar();
        try {
            if (this.cnn.getRst().next()) {
                c.setRut_doctor(this.cnn.getRst().getString("doctor"));
                c.setTemporales(this.cnn.getRst().getString("fechacita"));
                c.setRut_paciente(this.cnn.getRst().getString("paciente"));
                c.setTemporales1(this.cnn.getRst().getString("hora"));
                c.setTemporales2(this.cnn.getRst().getString("usuario"));
                c.setTemporales3(this.cnn.getRst().getString("fecha"));
                c.setTemporales4(this.cnn.getRst().getString("nombreusuario"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(controlador_cita.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.cnn.cerrarConexion();
        }
        return c;
    }
    public String buscar() {
        return null;
    }
    public String buscarFonasa() {
        return null;
    }
    public String Crear (doctor d) throws UnknownHostException, FileNotFoundException, IOException {
        this.configurarConexion("");
        String mensaje = "";
        this.cnn.setEsSelect(false);

        this.cnn.setSentenciaSQL("INSERT INTO "
                + "  agenda.doctor "
                + "   (  rut,   "
                + "    nombre,  "
                + "    apellido_paterno,  "
                + "     apellido_materno,  "
                + "    telefono, "
                + "    email, "
                + "     estatus, "
                + "      ipusuario, "
                + "      usuario, "
                + "     profesion ) "
                + "VALUES ("
                + "  '" + d.getRut() + "',"
                + "  '" + d.getNombre() + "',"
                + "  '" + d.getApellido_paterno() + "',"
                + "  '" + d.getApellido_materno() + "',"
                + "  '" + d.getTelefono() + "',"
                + "  '" + d.getEmail() + "',"
                + "  " + d.getEstatus() + ","
                + "  '" + getIpUsuarioLoggin() + "' ,"
                + "  '" + getUsuarioLoggin() + "', "
                + "  '" + d.getProfesion() + "'"
                + ");");
        this.cnn.conectar();
        if (this.cnn.getResultadoSQL() == 1) {
            mensaje = "Profesional Registrado Exitosamente.";
        } else {
            mensaje = "Ocurrio un problema!! Intente mas tarde";
        }
        this.cnn.cerrarConexion();
        return mensaje;

    }
}
