/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_atencion_clinica_oftalmologia;
import Modelos.motivo_consulta;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class registrarMotivoConsulta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
   
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        String motivo = request.getParameter("nombremotivo");
        String atencion = request.getParameter("atencion");
         String[] perfiles  = request.getParameterValues("roles");
         int id_motivo = caco.buscarUltimoidMotivoConsulta()+1;
         motivo_consulta mc = new motivo_consulta();
         mc.setId_motivo(id_motivo);
         mc.setNombremotivo(motivo);
         mc.setAtencion_cita(Integer.parseInt(atencion));
         String men = caco.InsertarMotivodeConsulta(mc);
         for(int i = 0 ; i<perfiles.length ; ++i){
             caco.motivoPerfiles(id_motivo, Integer.parseInt(perfiles[i]));
         }
        response.sendRedirect(caco.getLocallink() + "Registros/MotivoConsulta.jsp?men=" + men) ;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
