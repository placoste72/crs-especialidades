/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_atencion_clinica_oftalmologia;
import Controlador.controlador_cita;
import Modelos.atencion_clinica_tecnologo;
import Modelos.excepcion_garantia;
import Modelos.receta_lente_opticos;
import Modelos.registrodepatologia;
import Modelos.vicio_refraccion;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class volveraTens extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        /*para volver del TENS debo guardar y cambiar de estatus*/

        response.setContentType("text/html;charset=UTF-8");
        atencion_clinica_tecnologo act = new atencion_clinica_tecnologo();
       String men = "";
     
        String cita = request.getParameter("cita");
        act.setId_cita(Integer.parseInt(cita));


        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        int idatencio = 0;
        idatencio = caco.buscaridtencionTecnologoconIdCita(Integer.parseInt(cita));

        if (idatencio != 0) {
            act.setId_atencion_tecnologo(idatencio);

        } else {
            men = caco.IngresarAtenciontecnologo(act);
            idatencio = caco.buscaridtencionTecnologoconIdCita(Integer.parseInt(cita));
        }
        /*cambio el estatus de la cita*/
        controlador_cita cc = new controlador_cita();

        /*vicio refraccion*/
        String motivodelaconsulta = "";
        if (!request.getParameter("motivodelaconsulta").equals("")) {
            motivodelaconsulta = request.getParameter("motivodelaconsulta");
        }
        String amhta = "0";
        if (!request.getParameter("amhta").equals("")) {
            amhta = request.getParameter("amhta");
        }

        String amdm = "0";
        if (!request.getParameter("amdm").equals("")) {
            amdm = request.getParameter("amdm");
        }

        String amano = "";
        if (!request.getParameter("amano").equals("")) {
            amano = request.getParameter("amano");
        }

        String amglaucoma = "";
        if (!request.getParameter("amglaucoma").equals("")) {
            amglaucoma = request.getParameter("amglaucoma");
        }

        String aamotro = "";
        if (!request.getParameter("amotro").equals("")) {
            aamotro = request.getParameter("amotro");
        }

        String piod = "";
        if (!request.getParameter("piod").equals("")) {
            piod = request.getParameter("piod");
        }

        String pioi = "";
        if (!request.getParameter("pioi").equals("")) {
            pioi = request.getParameter("pioi");
        }

        String plscod = "";
        if (!request.getParameter("plscod").equals("")) {
            plscod = request.getParameter("plscod");
        }

        String plccod = "";
        if (!request.getParameter("plccod").equals("")) {
            plccod = request.getParameter("plccod");
        }

        String plscoi = "";
        if (!request.getParameter("plscoi").equals("")) {
            plscoi = request.getParameter("plscoi");
        }

        String plccoi = "";
        if (!request.getParameter("plccoi").equals("")) {
            plccoi = request.getParameter("plccoi");
        }

        String plccadi = "";
        if (!request.getParameter("plccadi").equals("")) {
            plccadi = request.getParameter("plccadi");
        }

        String reflejofotomotor = "";
        if (!request.getParameter("reflejofotomotor").equals("")) {
            reflejofotomotor = request.getParameter("reflejofotomotor");
        }

        String rojopupilar = "";
        if (!request.getParameter("rojopupilar").equals("")) {
            rojopupilar = request.getParameter("rojopupilar");
        }

        String bmcpoloposterior = "";
        if (!request.getParameter("bmcpoloposterior").equals("")) {
            bmcpoloposterior = request.getParameter("bmcpoloposterior");
        }

        String indicacionesvr = "";
        if (!request.getParameter("indicacionesvr").equals("")) {
            indicacionesvr = request.getParameter("indicacionesvr");
        }

        vicio_refraccion vc = new vicio_refraccion();

        /*asigno y registro*/
        vc.setId_atencion(idatencio);
        vc.setMotivoconsulta(motivodelaconsulta);
        vc.setAmdm(Integer.parseInt(amdm));
        vc.setAmglaucoma(Integer.parseInt(amglaucoma));
        vc.setAmhta(Integer.parseInt(amhta));
        vc.setAmotro(aamotro);
        vc.setAnosam(amano);
        vc.setPiod(piod);
        vc.setPioi(pioi);

        vc.setPlccod(plccod);

        vc.setPlccoi(plccoi);
        vc.setPlscoi(plscoi);

        vc.setPlscod(plscod);
        vc.setPlccadi(plccadi);

        vc.setBmcpoloposterior(bmcpoloposterior);
        vc.setIndicaciones(indicacionesvr);

        vc.setReflejofotomotor(reflejofotomotor);
        vc.setRojopopular(rojopupilar);

        /*ingreso la patalogia*/
        String ges = "-1";
        registrodepatologia rp = new registrodepatologia();
        receta_lente_opticos rlo = new receta_lente_opticos();
        if (!request.getParameter("ges").equals("")) {
            ges = request.getParameter("ges");

        }
        vc.setGes(Integer.parseInt(ges));
        /*Debo buscar si existe si no modifico */
        int idviciorefraccion = 0;
        idviciorefraccion = caco.buscarsiVicioRefraccion(idatencio);
        if (idviciorefraccion == 0) {
            caco.InsertarvicioRefraccion(vc);
        } else {
            vc.setId_viciorefraccion(idviciorefraccion);
            caco.ModificarViciodeRefraccion(vc);
        }

        /*variable para registro de patologia*/
        String vicio = "";
        String confirma = "";
        String diagnosticoconges = "";
        String tratamientolentesCerca = "";
        String tratamientolentesLejos = "";
        String diagnosticosiges = "";
        String fundamentodediagnostico = "";
        String tratamientoeindicacion = "";

        String vicionoges = "0";
        String diagnosticonoges = "";
        String fundamentodediagnosticonoges = "";
        String tratamientoeindicacionnoges = "";

        /*registro receta de  lente */
        String odf = "";
        String odc = "";
        String ode = "";
        String oie = "";
        String oic = "";
        String oiej = "";
        String add = "";
        String dpl = "";
        String dpc = "";
        boolean tengoreceta = false;
        String odenoges = "";
        String odcnoges = "";
        String odejnoges = "";
        String oienoges = "";
        String oicnoges = "";
        String oiejnoges = "";
        String addnoges = "";
        String dplnoges = "";
        String dpcnoges = "";
        String observacionesreceta = "";

        String observacionesrecetanoges = "";

        /*variables para excepcion de garantia*/
        int rpd = 0;
        int rapg = 0;
        int pr = 0;
        int ocdp = 0;
        String observaciongarantia = "";
        int idgarantia = 0;

        /*pregunto si tengo excepcion de no se asi sigo con el ges o no */
        excepcion_garantia excepciondegarantia = new excepcion_garantia();
        if (request.getParameter("excepciondegarantia") != null) {
            if (request.getParameter("excepciondegarantia").equals("checkbox")) {

                /*elimino el registro de patologia y la receta y registro o modifico la excepciondegarantia*/
                if (request.getParameter("rpd") != null) {
                       if (request.getParameter("rpd").equals("checkbox")) {
                         rpd = 1;  
                       }
                   }
                
                if (request.getParameter("rapg") != null) {
                       if (request.getParameter("rapg").equals("checkbox")) {
                         rapg = 1;  
                       }
                   }
                
                if (request.getParameter("pr") != null) {
                       if (request.getParameter("pr").equals("checkbox")) {
                         pr = 1;  
                       }
                   }
                
                if (request.getParameter("ocdp") != null) {
                       if (request.getParameter("ocdp").equals("checkbox")) {
                         ocdp = 1;  
                       }
                   }
                
                if (!request.getParameter("observaciongarantia").equals("")) {
                        observaciongarantia = request.getParameter("observaciongarantia");
                    }
                
                excepciondegarantia.setRechazo_atencion_garantizado( rapg);
                excepciondegarantia.setRechazo_prestador_designado(rpd);
                excepciondegarantia.setPrestacion_rechazada(pr);
                excepciondegarantia.setOtra_causa(ocdp);
                excepciondegarantia.setObservaciones(observaciongarantia);
                excepciondegarantia.setId_atencion(idatencio);
                 excepcion_garantia gx = caco.buscarGarantiaporIddeAtencion(idatencio,1);
                 if(gx.getIdexcepcion_garantia() != 0){
                    excepciondegarantia.setIdexcepcion_garantia(gx.getIdexcepcion_garantia());
                    caco.ModificarGarantia(excepciondegarantia);
                 }else{
                    caco.insertarGarantia(excepciondegarantia);  
                 }
               
            
            }
        }


        /*ges es si*/
        if (ges.equalsIgnoreCase("1")) {

            /*pregunto por datos */
            if (request.getParameter("vicio") != null) {
                if (request.getParameter("vicio").equals("checkbox")) {
                    rp.setVicio_refraccion(1);
                }

            }

            if (request.getParameter("confirma") != null) {

                if (request.getParameter("confirma").equals("si")) {

                    rp.setConfima(1);
                    rp.setFundamentodeldiagnostico("");
                    rp.setTratamientoeindicacion("");
                    /*coloco tratamiento y eligo tratamiento lejos y cerca y  coloco receta */

                    if (!request.getParameter("diagnosticoconges").equals("")) {
                        diagnosticoconges = request.getParameter("diagnosticoconges");
                    }

                    rp.setDiagnostico(diagnosticoconges);
                    if (request.getParameter("tratamientolentesCerca") != null) {
                        if (request.getParameter("tratamientolentesCerca").equals("checkbox")) {

                            rp.setTratamientocerca(1);
                            tengoreceta = true;
                        }

                    }
                    if (request.getParameter("tratamientolentesLejos") != null) {
                        if (request.getParameter("tratamientolentesLejos").equals("checkbox")) {
                            rp.setTratamientolejos(1);
                            tengoreceta = true;
                        }

                    }

                    rp.setId_atencion(idatencio);
                    int buscarregistropatologia = caco.buscarregistroPatologia(idatencio);
                    if (buscarregistropatologia == 0) {
                        caco.ingresarRegistrodePatologia(rp);
                    } else {
                        rp.setId_registropatologia(buscarregistropatologia);
                        caco.ModificarregistroPatologia(rp);
                    }

                    if (tengoreceta == true) {

                        if (!request.getParameter("odf").equals("")) {
                            odf = request.getParameter("odf");
                        }
                        rlo.setOjoderechoesfera(odf);

                        if (!request.getParameter("odc").equals("")) {
                            odc = request.getParameter("odc");
                        }
                        rlo.setOjoderechocilindro(odc);

                        if (!request.getParameter("ode").equals("")) {
                            ode = request.getParameter("ode");
                        }
                        rlo.setOjoderechoeje(ode);

                        if (!request.getParameter("oie").equals("")) {
                            oie = request.getParameter("oie");
                        }
                        rlo.setOjoizquierdoesfera(oie);

                        if (!request.getParameter("oic").equals("")) {
                            oic = request.getParameter("oic");
                        }
                        rlo.setOjoiaquierdacilindro(oic);
                        oiej = "";
                        if (!request.getParameter("oiej").equals("")) {
                            oiej = request.getParameter("oiej");
                        }
                        rlo.setOjoizquierdaeje(oiej);

                        if (!request.getParameter("add").equals("")) {
                            add = request.getParameter("add");
                        }
                        rlo.setAadd(add);

                        if (!request.getParameter("dpl").equals("")) {
                            dpl = request.getParameter("dpl");
                        }
                        rlo.setDistanciapupilarlejos(dpl);

                        if (!request.getParameter("dpc").equals("")) {
                            dpc = request.getParameter("dpc");
                        }
                        rlo.setDistanciapupilarcerca(dpc);
                        if (!request.getParameter("observacionesreceta").equals("")) {
                            observacionesreceta = request.getParameter("observacionesreceta");
                        }

                        rlo.setObservaciones(observacionesreceta);

                        rlo.setId_atencion(idatencio);
                        int idreceta = 0;
                        idreceta = caco.buscarRecetadeLentes(idatencio,0);
                        if (idreceta == 0) {
                            caco.InsertarRecetaLenteOpticos(rlo);
                        } else {
                            rlo.setId_receta(idreceta);
                            caco.ModificarRecetadeLentes(rlo);
                        }

                    } else {
                        /*escribo tratamiento fundamento diagnostico sin receta*/
                        int idreceta = caco.buscarRecetadeLentes(idatencio,0);
                        if (idreceta != 0) {
                            caco.eliminarReceta(idreceta);
                        }
                    }

                } else if (request.getParameter("confirma").equals("no")) {
                    rp.setConfima(0);
                    if (request.getParameter("vicionoges") != null) {
                        if (request.getParameter("vicionoges").equals("checkbox")) {
                            rp.setVicio_refraccion(1);
                        }

                    }

                    if (!request.getParameter("diagnosticosiges").equals("")) {
                        diagnosticonoges = request.getParameter("diagnosticosiges");
                    }
                    rp.setDiagnostico(diagnosticonoges);
                    if (!request.getParameter("fundamentodediagnostico").equals("")) {
                        fundamentodediagnosticonoges = request.getParameter("fundamentodediagnostico");
                    }
                    rp.setFundamentodeldiagnostico(fundamentodediagnosticonoges);

                    if (!request.getParameter("tratamientoeindicacion").equals("")) {
                        tratamientoeindicacionnoges = request.getParameter("tratamientoeindicacion");
                    }

                    rp.setTratamientoeindicacion(tratamientoeindicacionnoges);
                    rp.setId_atencion(idatencio);
                    rp.setTratamientocerca(0);
                    rp.setTratamientolejos(0);

                    /*buscar si existe si no modifico*/
                    int buscarregistropatologia = caco.buscarregistroPatologia(idatencio);
                    if (buscarregistropatologia == 0) {
                        caco.ingresarRegistrodePatologia(rp);
                    } else {
                        rp.setId_registropatologia(buscarregistropatologia);
                        caco.ModificarregistroPatologia(rp);
                    }
                    /*escribo tratamiento fundamento diagnostico sin receta*/
                    int idreceta = caco.buscarRecetadeLentes(idatencio,0);
                    if (idreceta != 0) {
                        caco.eliminarReceta(idreceta);
                    }

                }
            }

        } else if (ges.equalsIgnoreCase("0")) {

            if (request.getParameter("vicionoges") != null) {
                if (request.getParameter("vicionoges").equals("checkbox")) {
                    rp.setVicio_refraccion(1);
                }

            }

            if (!request.getParameter("diagnosticonoges").equals("")) {
                diagnosticonoges = request.getParameter("diagnosticonoges");
            }
            rp.setDiagnostico(diagnosticonoges);

            if (!request.getParameter("fundamentodediagnosticonoges").equals("")) {
                fundamentodediagnosticonoges = request.getParameter("fundamentodediagnosticonoges");
            }
            rp.setFundamentodeldiagnostico(fundamentodediagnosticonoges);

            if (!request.getParameter("tratamientoeindicacionnoges").equals("")) {
                tratamientoeindicacionnoges = request.getParameter("tratamientoeindicacionnoges");
            }
            rp.setTratamientoeindicacion(tratamientoeindicacionnoges);

            rp.setId_atencion(idatencio);
            rp.setTratamientocerca(0);
            rp.setTratamientolejos(0);
            /*receta */
            int buscarregistropatologia = caco.buscarregistroPatologia(idatencio);
            if (buscarregistropatologia == 0) {
                caco.ingresarRegistrodePatologia(rp);
            } else {
                rp.setId_registropatologia(buscarregistropatologia);
                caco.ModificarregistroPatologia(rp);
            }
            if (!request.getParameter("odenoges").equals("") || !request.getParameter("odcnoges").equals("") || !request.getParameter("odejnoges").equals("") || !request.getParameter("oienoges").equals("") || !request.getParameter("oicnoges").equals("") || !request.getParameter("oiejnoges").equals("") || !request.getParameter("addnoges").equals("") || !request.getParameter("dplnoges").equals("") || !request.getParameter("dpcnoges").equals("")) {
                if (!request.getParameter("odenoges").equals("")) {
                    odenoges = request.getParameter("odenoges");
                }
                rlo.setOjoderechoesfera(odenoges);

                if (!request.getParameter("odcnoges").equals("")) {
                    odcnoges = request.getParameter("odcnoges");
                }

                rlo.setOjoderechocilindro(odcnoges);

                if (!request.getParameter("odejnoges").equals("")) {
                    odejnoges = request.getParameter("odejnoges");
                }

                rlo.setOjoderechoeje(odejnoges);

                if (!request.getParameter("oienoges").equals("")) {
                    oienoges = request.getParameter("oienoges");
                }
                rlo.setOjoizquierdoesfera(oienoges);

                if (!request.getParameter("oicnoges").equals("")) {
                    oicnoges = request.getParameter("oicnoges");
                }
                rlo.setOjoiaquierdacilindro(oicnoges);

                if (!request.getParameter("oiejnoges").equals("")) {
                    oiejnoges = request.getParameter("oiejnoges");
                }
                rlo.setOjoizquierdaeje(oiejnoges);

                if (!request.getParameter("addnoges").equals("")) {
                    addnoges = request.getParameter("addnoges");
                }
                rlo.setAadd(addnoges);

                if (!request.getParameter("dplnoges").equals("")) {
                    dplnoges = request.getParameter("dplnoges");
                }
                rlo.setDistanciapupilarlejos(dplnoges);
                if (!request.getParameter("dpcnoges").equals("")) {
                    dpcnoges = request.getParameter("dpcnoges");
                }
                rlo.setDistanciapupilarcerca(dpcnoges);

                if (!request.getParameter("observacionesrecetanoges").equals("")) {
                    observacionesrecetanoges = request.getParameter("observacionesrecetanoges");
                }
                rlo.setObservaciones(observacionesrecetanoges);

                rlo.setId_atencion(idatencio);
                int idreceta = 0;
                idreceta = caco.buscarRecetadeLentes(idatencio,0);
                if (idreceta == 0) {
                    caco.InsertarRecetaLenteOpticos(rlo);
                } else {
                    rlo.setId_receta(idreceta);
                    caco.ModificarRecetadeLentes(rlo);
                }

            } else {
                int idreceta = 0;
                idreceta = caco.buscarRecetadeLentes(idatencio,0);
                if (idreceta != 0) {

                    caco.eliminarReceta(idreceta);
                }

            }
        }

        cc.citadevueltaaTensconAtencionenProceso(Integer.parseInt(cita));
        response.sendRedirect(caco.getLocallink() + "Oftalmologia/AtencionClinicaTecnologo2.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
