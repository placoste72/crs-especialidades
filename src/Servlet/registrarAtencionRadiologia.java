/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_atencion;
import Controlador.controlador_cita;
import Modelos.atencion;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class registrarAtencionRadiologia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*inserto la atencion*/

        String higado = request.getParameter("higado").replaceAll("'", "''").toUpperCase();
        String viabiliar = request.getParameter("viabiliar").replaceAll("'", "''").toUpperCase();
        String vesicula = request.getParameter("vesicula").replaceAll("'", "''").toUpperCase();
        String pancreas = request.getParameter("pancreas").replaceAll("'", "''").toUpperCase();
        String bazo = request.getParameter("bazo");
        String otrobrazo1 = request.getParameter("otrobrazo1");
        String aorta = request.getParameter("aorta");
        String otroaorta2 = request.getParameter("otroaorta2");
        String conclusion = request.getParameter("conclusion").replaceAll("'", "''").toUpperCase();
        String rinones = request.getParameter("rinones").replaceAll("'", "''").toUpperCase();
        String hipotesis_diagnostica= request.getParameter("hipotesis_diagnostica").replaceAll("'", "''").toUpperCase();
        String profesional_solicitante = request.getParameter("profesional_solicitante");
        String cita = request.getParameter("cita");
        controlador_atencion ca = new controlador_atencion();
        atencion a = new atencion();
        a.setHigado(higado);
        a.setIdaorta(Integer.parseInt(aorta));
        a.setIdcita(Integer.parseInt(cita));
        a.setVesicula(vesicula);
        a.setVia_biliar(viabiliar);
        a.setPancreas(pancreas);
        a.setIdbazo(Integer.parseInt(bazo));
        a.setOtro_bazo(otrobrazo1);
        a.setOtro_aorta(otroaorta2);
        a.setConclusion(conclusion);
        a.setRinones(rinones);
        a.setHipotesis_diagnostica(hipotesis_diagnostica);
        a.setMedico_solicitante(profesional_solicitante);
        String mensaje = ca.ingresarAtencionRadiologia(a);
        int id = ca.idatencionporCita(Integer.parseInt(cita));
        controlador_cita cc = new controlador_cita();
        if(id !=0){
            cc.cambiarCitaAatendida(Integer.parseInt(cita));
        }
         response.sendRedirect(ca.getLocal() + "vistas/Radiologia/ListaPacientes.jsp?men=" + mensaje+"&idatencion="+id);
         

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
