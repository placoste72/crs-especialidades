/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_cita;
import Controlador.controlador_cupos;
import Controlador.controlador_paciente;
import Modelos.cita;
import Modelos.lugar;
import Modelos.oferta;
import Modelos.paciente;
import Modelos.planificar;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Informatica
 */
public class AsignarCita extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ParseException {

        controlador_cita cc = new controlador_cita();
        controlador_cupos cu = new controlador_cupos();
        controlador_paciente cp = new controlador_paciente();
        String URL = "";
        String paciente = request.getParameter("rutpaciente");
        String especialidad = request.getParameter("especialdiad");
        String doctor = request.getParameter("funcionario");
        String fecha = request.getParameter("txt_inicial");
        String emaildoctor = "";
        boolean escorrecto = cu.validarRut(paciente);
        String rutconformato = "";
        if (escorrecto == true) {
            rutconformato = cu.FormatearRUT(paciente);

            boolean existe = cp.buscarpaciente(rutconformato);
            /*registro o modificacion  del paciente */
            if (existe == false) {

                String nombre = request.getParameter("nombrepaciente");
                String apellido1 = request.getParameter("apellidoppaciente").replace("'", "");
                String apellido2 = request.getParameter("apellidompaciente").replace("'", "");
                String genero = request.getParameter("genero");
                String fechanacimiento = request.getParameter("txt");
                String telefonoPaciente = request.getParameter("telefonoPaciente");
                String telefonoPaciente1 = request.getParameter("telefonoPaciente1");
                emaildoctor = request.getParameter("emaildoctor");
                String direccion = request.getParameter("direccion").replace("'", "");
                String comuna = request.getParameter("comuna");
                String tramo = request.getParameter("tramo");
                String provision = request.getParameter("provision");
                String procedencia = request.getParameter("procedencia");
                String nacionalidad = request.getParameter("nacionalidad");
                String nombresocial = request.getParameter("nombresocial");
                paciente p = new paciente();
                String mes = "";
                String region = request.getParameter("region");
                String provincia = request.getParameter("provincia");
                Date fecha1 = new Date(Integer.parseInt(fechanacimiento.substring(6, 10)) - 1900, Integer.parseInt(fechanacimiento.substring(3, 5)) - 1, Integer.parseInt(fechanacimiento.substring(0, 2)), 0, 0, 0);
                p.setRut(rutconformato);
                /*if apellido2 = null entonces guardar un -*/
                if (!apellido2.equals("")) {
                    p.setApellido_moderno(apellido2);
                } else {
                    p.setApellido_moderno("-");
                }

                p.setApellido_paterno(apellido1);
                p.setGenero(Integer.parseInt(genero));
                p.setContacto1(telefonoPaciente);
                p.setContacto2(telefonoPaciente1);
                p.setDireccion(direccion);
                p.setNombre(nombre);
                p.setFecha_nacimiento(fecha1);
                p.setEmail(emaildoctor);
                p.setEstatus(1);
                p.setId_comuna(Integer.parseInt(comuna));
                p.setProvision(Integer.parseInt(provision));
                p.setTramo(Integer.parseInt(tramo));
                p.setProcedencia(Integer.parseInt(procedencia));
                p.setId_region(Integer.parseInt(region));
                p.setTramo(Integer.parseInt(tramo));
                p.setId_provincia(Integer.parseInt(provincia));
                p.setProcedencia(Integer.parseInt(procedencia));
                p.setIdnacionalidad(Integer.parseInt(nacionalidad));
                p.setNombresocial(nombresocial);

                cp.guardarPaciente(p);
            } else if (request.getParameter("direccion") != null) {
                String nombre = request.getParameter("nombrepaciente");
                String apellido1 = request.getParameter("apellidoppaciente").replace("'", "");
                String apellido2 = request.getParameter("apellidompaciente").replace("'", "");
                String genero = request.getParameter("genero");
                String fechanacimiento = request.getParameter("txt");
                String telefonoPaciente = request.getParameter("telefonoPaciente");
                String telefonoPaciente1 = request.getParameter("telefonoPaciente1");
                emaildoctor = request.getParameter("emaildoctor");
                String direccion = request.getParameter("direccion").replace("'", "");
                String comuna = request.getParameter("comuna");
                String tramo = request.getParameter("tramo");
                String provision = request.getParameter("provision");
                String procedencia = request.getParameter("procedencia");
                String region = request.getParameter("region");
                String provincia = request.getParameter("provincia");
                String nacionalidad = request.getParameter("nacionalidad");
                String nombresocial = request.getParameter("nombresocial");
                paciente p = new paciente();
                String mes = "";

                Date fecha1 = new Date(Integer.parseInt(fechanacimiento.substring(6, 10)) - 1900, Integer.parseInt(fechanacimiento.substring(3, 5)) - 1, Integer.parseInt(fechanacimiento.substring(0, 2)), 0, 0, 0);
                p.setRut(rutconformato);
                p.setApellido_moderno(apellido2);
                p.setApellido_paterno(apellido1);
                p.setGenero(Integer.parseInt(genero));
                p.setContacto1(telefonoPaciente);
                p.setContacto2(telefonoPaciente1);
                p.setDireccion(direccion);
                p.setIdnacionalidad(Integer.parseInt(nacionalidad));
                p.setNombresocial(nombresocial);
                p.setNombre(nombre);
                p.setFecha_nacimiento(fecha1);
                p.setEmail(emaildoctor);
                p.setEstatus(1);
                if (region == null) {
                    p.setId_region(-1);
                } else {
                    p.setId_region(Integer.parseInt(region));
                }

                if (provincia == null) {
                    p.setId_provincia(-1);
                } else {
                    p.setId_provincia(Integer.parseInt(provincia));
                }

                if (comuna == null) {
                    p.setId_comuna(-1);
                } else {
                    p.setId_comuna(Integer.parseInt(comuna));
                }

                p.setProvision(Integer.parseInt(provision));
                if (tramo == null) {
                    p.setTramo(0);
                } else {
                    p.setTramo(Integer.parseInt(tramo));
                }

                if (procedencia == null) {
                    p.setProcedencia(0);
                } else {
                    p.setProcedencia(Integer.parseInt(procedencia));
                }

                cp.modificartodoelPaciente(p);
            } else {
                String email = request.getParameter("emaildoctor");
                String telefono = request.getParameter("telefonoPaciente");
                cp.ModificarEmailyContactoPaciente(rutconformato, email, telefono);
            }
            existe = cp.buscarpaciente(rutconformato);
            /*si registro con exito*/
            if (existe == true) {

                String id_oferta = request.getParameter("oferta");
                /*busco el tipo de atencion si es 6 debo poner la cita en estatus = 2*/
                oferta of = cu.buscarOfertaporIdparaconsulta(Integer.parseInt(id_oferta));
                //String motivo = request.getParameter("porquesecita");
                String ges = request.getParameter("patologiages");
                String atencion = request.getParameter("atencion");
                String patologia = request.getParameter("patologia");
                String diagnosticocita = request.getParameter("diagnosticocita");

                cita c = new cita();

                c.setId_oferta(Integer.parseInt(id_oferta));
                c.setId_tipoatencion(1);
                c.setGes(Integer.parseInt(ges));
                c.setId_patologia(Integer.parseInt(patologia));
                paciente rutoriginal = cp.buscarpacienteporrut(rutconformato);
                c.setRut_paciente(rutoriginal.getRut());
                c.setIddiagnosticoespecialdad(Integer.parseInt(diagnosticocita));
                if (Integer.parseInt(atencion) == 4) {
                       c.setProcedimiento(Integer.parseInt(request.getParameter("procedimientos")));
                    }
                if (Integer.parseInt(atencion) == 5) {
                       c.setExamen(Integer.parseInt(request.getParameter("examen")));
                    }
                //  c.setMotivo(motivo);

                /*saber si es cupo o sobrecupo*/
                planificar pla = cu.buscarplanificacionytipo(Integer.parseInt(id_oferta));
                if (of.getProcede() == 6) {
                    c.setEstatus(2);
                } else {
                    c.setEstatus(1);
                }

                String men = "";
                boolean ingreso = false;
                boolean mandarcorreo = false;
                boolean traeprestacion = false;
                /*preguntar por el tipo de atencion*/

                // String id_motivo = request.getParameter("id_motivo");
                // c.setId_motivo(Integer.parseInt(id_motivo));

                /*buscar si la oferta ya tiene cita si si no guardar
                 */
                int tengocita = cc.buscaridcitaconOferta(Integer.parseInt(id_oferta));
                if (tengocita == 0) {
                    ingreso = cc.ingresarcita(c);
                }

                int idcita = 0;
                if (ingreso == true) {

                    if (pla.getEstatus() == 1) {
                        cu.disminuirCuposDoctor(pla.getId_planificacion());
                        cu.cambiarestatusdeoferta(Integer.parseInt(id_oferta));
                    } else {
                        cu.disminuirSobreCuposDoctor(pla.getId_planificacion());
                        cu.cambiarestatusdeoferta(Integer.parseInt(id_oferta));
                        mandarcorreo = true;

                    }

                    idcita = cc.buscaridcitaconOferta(c.getId_oferta());
                    /*mandar correo si la ficha se encuentra en bodega externa para no duplicado de ficha*/
                    if (Integer.parseInt(atencion) == 3) {
                        traeprestacion = true;
                    }


                    /*validar si el tipo de atencion es = 3 debo guardar prestacion de la pantalla */
                    if (traeprestacion == true) {
                        String prestacion = request.getParameter("prestacion");
                        if (request.getParameter("prestacion") != null) {
                            cu.insertarCitaPrestacion(idcita, Integer.parseInt(prestacion), 1);
                        } else {
                            cu.insertarCitaPrestacion(idcita, pla.getId_prestacion(), 1);
                        }
                    } else {

                        cu.insertarCitaPrestacion(idcita, pla.getId_prestacion(), 1);
                    }
                    /*Colocar aqui lo de los email*/
                    if (mandarcorreo == true) {
                        cc.enviarMensajeporSobrecupo(idcita);
                    }

                    if (!emaildoctor.equals("")) {
                        cc.enviarEmielconComprobanteCita(idcita, emaildoctor);
                    }
                    // cc.enviarMensajeporFichaExterna(idcita);

                    boolean tienefichaafuera = cc.BuscarsiTieneFichaExterna(rutoriginal.getRut());
                    if (tienefichaafuera == true) {
                        cc.enviarMensajeporFichaExterna(idcita);
                    }
                    lugar l = new lugar();
                    try {
                        l = cc.avisodepagoenAdmision(idcita);
                    } catch (Exception e) {
                    }
                    String avisopago = "";
                    if (l.getRut() != null) {
                        avisopago = " ********Aviso de PAGO********* Total a Pagar por la Cita=" + l.getVariable1();
                    }

                    URL = cc.getLocallink() + "Cita/AsignarCita.jsp?error=0&men=Cita registrada exitosamente fecha:" + fecha + " hora: " + of.getHora() + " " + avisopago + "&atencion=" + pla.getId_atencion() + "&especialidad=" + especialidad + "&funcionario=" + doctor + "&txt_inicial=" + fecha + "&idcita=" + idcita + "&paciente=" + rutoriginal.getRut();

                } else {
                    URL = cc.getLocallink() + "Cita/AsignarCita.jsp?error=1&men=HORA YA TOMADA , NO SE PUDO REGISTRAR LA CITA!!CAMBIE DE HORA Y INTENTE DE NUEVO&atencion=" + pla.getId_atencion() + "&especialidad=" + especialidad + "&funcionario=" + doctor + "&txt_inicial=" + fecha;

                }
            } else {
                URL = cc.getLocallink() + "Cita/AsignarCita.jsp?error=1&men=NO SE PUDO REGISTRAR EL PACIENTE!! INTENTE DE NUEVO&especialidad=" + especialidad + "&funcionario=" + doctor + "&txt_inicial=" + fecha;

            }
        } else {
            URL = cc.getLocallink() + "Cita/AsignarCita.jsp?error=1&men=NO SE PUDO REGISTRAR LA CITA!!RUT INVALIDA!! INTENTE DE NUEVO&especialidad=" + especialidad + "&funcionario=" + doctor + "&txt_inicial=" + fecha;

        }

        response.sendRedirect(URL);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(AsignarCita.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AsignarCita.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(AsignarCita.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AsignarCita.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
