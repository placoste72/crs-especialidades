/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.General;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 *
 * @author a
 */
public class Ingresarrutreporte extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileUploadException, Exception {

        /*me llega fecha1, fecha 2*/
 /*formdata*/
        ServletRequestContext src = new ServletRequestContext(request);
        List lista2 = null;
         String inicio = "";
            String fin = "";
        General g = new General();
        //Si el formulario es enviado con Multipart
        if (ServletFileUpload.isMultipartContent(src)) {
           
            

            String path2 = "C:\\carpeta";

            File destino = new File(path2);
            DiskFileItemFactory factory = new DiskFileItemFactory((1024 * 1024), destino);

            ServletFileUpload upload = new ServletFileUpload(factory);

            List lista = upload.parseRequest(src);
            lista2 = lista;
            File file = null;

            Iterator it = lista.iterator();

            boolean sw_observacion = false;
            boolean sw_observacion_revision = false;
            while (it.hasNext()) {

                FileItem item = (FileItem) it.next();

                if (item.isFormField()) {
                    
                    if(item.getFieldName().equals("fecha_inicio"))
                        {   
                       String fieldName = item.getFieldName();
                        inicio = item.getString();
                        }
                    else if(item.getFieldName().equals("fecha_fin")){
                         String fieldName = item.getFieldName();
                        fin = item.getString();
                    }
                } else if (item.getName().length() > 0) {

                    file = new File(item.getName());

                    item.write(new File(destino, "archivo.xls"));
                    out.println("Fichero subido");
                }
            }

        }
        /*elimino todas las rut existente*/
        g.ELIMINARRut();

        /*hago el insert*/
        String documentoDir = "C:\\carpeta\\archivo.xls";
        try {
            //Se abre el fichero Excel   
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(documentoDir));

            //Se obtiene el libro Excel   
            HSSFWorkbook wb = new HSSFWorkbook(fs);

            //Se obtiene la primera hoja   
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFCell cell;
            for (int k = 1; k <= sheet.getLastRowNum(); k++) {
                HSSFRow row = sheet.getRow(k);

                cell = row.getCell((short) 0);
                // out.write("" + cell.getStringCellValue());
                String rut = "";
                try {

                    rut = "" + cell.getStringCellValue().trim();

                } catch (Exception ex) {

                }

                g.ingresarRut(rut);

                // inserto las rut
            }

        } catch (IOException ex) {

            // System.out.println();
        }

        response.sendRedirect("informe?fecha1=" + inicio + "&fecha2=" + fin);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Ingresarrutreporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Ingresarrutreporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
