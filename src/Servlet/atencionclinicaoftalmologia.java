/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_atencion_clinica_oftalmologia;
import Controlador.controlador_cita;
import Modelos.atencion_clinica_oftalmologia;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class atencionclinicaoftalmologia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String idcita = request.getParameter("cita");

        String autorefraccion = "0";
        if (!request.getParameter("autorefraccion").equals("")) {
            autorefraccion = request.getParameter("autorefraccion");
        }
        String observacion = "";
        if (!request.getParameter("observacion").equals("") ) {
            observacion = request.getParameter("observacion");
        }
        String oi = "0";
        if (!request.getParameter("oi").equals("")) {
            oi = request.getParameter("oi");
        }
        String od = "0";

        if (!request.getParameter("od").equals("")) {
            od = request.getParameter("od");
        }
        String numerooi = "0";
        if (!request.getParameter("numerooi").equals("")) {
            numerooi = request.getParameter("numerooi");
        }
        String numerood = "0";

        if (!request.getParameter("numerood").equals("")) {
            numerood = request.getParameter("numerood");
        }

        String observacionpresion = "";
        if (!request.getParameter("observacionpresion").equals("")) {
            observacionpresion = request.getParameter("observacionpresion");
        }
        String dilatacionoi = "0";
        if (!request.getParameter("dilatacionoi").equals("")) {

            dilatacionoi = request.getParameter("dilatacionoi");
        }
        String dilatacionod = "0";

        if (!request.getParameter("dilatacionod").equals("")) {
            dilatacionod = request.getParameter("dilatacionod");
        }
        String colirio = "";
        if (!request.getParameter("colirio").equals("")) {
            colirio = request.getParameter("colirio");
        }
        String observaciondilatacion = "";
        if (!request.getParameter("observaciondilatacion").equals("")) {
            observaciondilatacion = request.getParameter("observaciondilatacion");
        }
        String observacionr = "";
        if (!request.getParameter("observacionr").equals("")) {
            observacionr = request.getParameter("observacionr");
        }
        String men = "";

        atencion_clinica_oftalmologia ac = new atencion_clinica_oftalmologia();
        ac.setId_cita(Integer.parseInt(idcita));
        ac.setAutorefraccion(Integer.parseInt(autorefraccion));
        ac.setAutorefaccionobservaciones(observacion);
        ac.setPresionio(Integer.parseInt(oi));
        ac.setPresionod(Integer.parseInt(od));
        ac.setNumerooi(Double.parseDouble(numerooi));
        ac.setNumerood(Double.parseDouble(numerood));
        ac.setPresionobservaciones(observacionpresion);
        ac.setDilatacionod(Integer.parseInt(dilatacionod));
        ac.setDilatacionoi(Integer.parseInt(dilatacionoi));
        ac.setDilatacion_colirio(colirio);
        ac.setDilatacion_observaciones(observaciondilatacion);
        ac.setRegistro_de_observaciones(observacionr);
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        controlador_cita cc = new controlador_cita();
       /*debo preguntar si ya tengo un atencio clinica si es 7 ya guarde debo convertir revisar a ver si convierto en 8
        o en 5*/
        int estatus = cc.buscarestatusdecita(Integer.parseInt(idcita));
                
               if (estatus == 7) {
                 
                 int idatenciontens = caco.buscarIdatencionTensporidCita(Integer.parseInt(idcita));
                 ac.setId_atencion_coftalmologia(idatenciontens);
                   men =caco.ModificarRegistrodelTens(ac);
                /*debo ver si tengo id_atencion entonces coloco estatus a 8 si no a 5*/
                 int idatencion = caco.buscaridtencionTecnologoconIdCita(Integer.parseInt(idcita));
                  if(idatencion ==0){
                      cc.CambiarCitapasoporTecnico(Integer.parseInt(idcita));
                  }else{
                      cc.cambiarcitaaAtencionGuardadaperonoFinalizada(Integer.parseInt(idcita));  
                  }
                  ac.setId_atencion_coftalmologia(idatencion);
                  
                
            }else{
                  men = caco.insertarAtencionClinica(ac); 
                   cc.CambiarCitapasoporTecnico(Integer.parseInt(idcita));
               }
        
       
        response.sendRedirect("vistas/Cita/AtencionClinicaOftalmologia2.jsp?men=" + men);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
