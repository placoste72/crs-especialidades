/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_cita;
import Controlador.controlador_examenes;
import Modelos.examenes_grupos;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class atenderExamendeLaboratorio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String cita = request.getParameter("cita");
        String observacion = request.getParameter("observaciones");
        controlador_examenes ce = new controlador_examenes();
        controlador_cita cc= new controlador_cita();
        examenes_grupos atencion = new examenes_grupos();
        examenes_grupos ExamenesdeAtencion = new examenes_grupos();
        atencion.setIdcita(Integer.parseInt(cita));
        atencion.setObservacion(observacion);
        String men = "";
        int exito = ce.guardarAtenciondeExamenes(atencion);
        if (exito == 1) {
            int idatencion = ce.buscarIdAtencionLaboratorioporCita(Integer.parseInt(cita));
            for (examenes_grupos grupo : ce.buscarGrupoPantallas(1)) {
                for (examenes_grupos eg : ce.buscarExamesparaPantallas(1, grupo.getIdgrupo())) {
                    if (request.getParameter("examen"+eg.getId_examengrupo()) != null) {
                        if (request.getParameter("examen"+eg.getId_examengrupo()).equals("checkbox")) {
                            ce.ingresarExamenaAtencion(idatencion, eg.getId_examengrupo());
                        }
                    }
                }

            }

            for (examenes_grupos grupo : ce.buscarGrupoPantallas(2)) {
                for (examenes_grupos eg : ce.buscarExamesparaPantallas(2, grupo.getIdgrupo())) {
                    if (request.getParameter("examen" + eg.getId_examengrupo()) != null) {
                        if (request.getParameter("examen" + eg.getId_examengrupo()).equals("checkbox")) {
                            ce.ingresarExamenaAtencion(idatencion, eg.getId_examengrupo());
                        }
                    }
                }

            }
            men = "Atencion de Examenes de Laboratorio, guardada con exito";
            /*cambiar el estatus */
            cc.cambiarCitaAatendida(Integer.parseInt(cita));
            

        } else {
            men = "Ocurrio un Erro!! Intente mas tarde";
        }
         response.sendRedirect(ce.getLocallink() + "Examenes/PacientesparaExamen.jsp?men="+men);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
