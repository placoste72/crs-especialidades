/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Controlador.controlador_usuario;
import Modelos.usuario;
import java.io.File;
import java.io.FileInputStream;
import java.util.Calendar;

import java.util.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Informatica
 */
public class ingresar_usuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession respuesta = request.getSession(true);

            //creamos objeto del contrador
            String funcionario = request.getParameter("funcionario");

            String email = request.getParameter("email");
            String contrasenavieja = "";
            //Calendar hoy = Calendar.getInstance();

            int estatus = 1;
            controlador_usuario cu = new controlador_usuario();
            usuario u = new usuario();
            int idusuario = cu.buscarUsuario2();
            idusuario = idusuario + 1;
            String contrasena = cu.getCadenaAlfanumAleatoria(8);

            //Establecer los parametros a la BD
            u.setId_usuairo(idusuario);
            u.setId_funcionario(funcionario);
            u.setContrasena(contrasena);
            //  u.setFecha_registro((java.sql.Date) hoy.getTime());

            u.setNombre_usuario(funcionario);
            u.setEstatus(estatus);
            u.setViejacontrasena(contrasenavieja);
            u.setEmail(email);
            String men = "";
            boolean verifica = false;
            verifica = cu.buscarusuario(funcionario);
            if (verifica == true) {
                men = "Usuario ya existe";
                response.sendRedirect(cu.getLocallink() + "Planificar/RegistrarUsuario.jsp?men=" + men);
            } else {
                men = cu.ingresa_usuario(u);
                //para cuando sean multiples
                String[] roles = request.getParameterValues("roles");
                cu.registrarUsuario_Rol(idusuario, roles);
                ServletContext context = this.getServletConfig().getServletContext();
                String path2 = context.getRealPath("");
                cu.registrarfoto(u.getId_usuairo(), u.getId_funcionario() + ".jpg", path2 + "\\public\\fotos\\usuario.png");
                respuesta.setAttribute("ok", "Usuario Registrado");
                response.sendRedirect(cu.getLocallink() + "Planificar/RegistrarUsuario.jsp?men=" + men);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
