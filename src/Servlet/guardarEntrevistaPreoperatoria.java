/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_especialidad;
import Modelos.entrevista_preoperatoria;
import Modelos.lugar;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class guardarEntrevistaPreoperatoria extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     //   response.setContentType("text/html;charset=UTF-8");
        entrevista_preoperatoria ep = new entrevista_preoperatoria();
        String identrevista = request.getParameter("identrevista");
        String diagnosticoenfermera = request.getParameter("diagnosticoenfermera").replace("'", "");
        String cirugiapropuesta = request.getParameter("cirugiapropuesta").replace("'", "");
        String peso = request.getParameter("peso");
        String talla = request.getParameter("talla");
        String imc = request.getParameter("imc");
        String pa = request.getParameter("pa");
        String cardiaca = request.getParameter("cardiaca");
        String sat = request.getParameter("sat");
        String asma = request.getParameter("asma");
        
        String tuberculosis = request.getParameter("tuberculosis");
        
        ep.setTuberculosis(Integer.parseInt(tuberculosis));
        
        String ira = request.getParameter("ira");
        ep.setIra(Integer.parseInt(ira));
        
        String renitis = request.getParameter("renitis");
        ep.setRinitis(Integer.parseInt(renitis));
        
        String tabaco = request.getParameter("tabaco");
        if (tabaco != null) {
            ep.setTabaco(Integer.parseInt(tabaco));
        }
        String desde = request.getParameter("desde");
        String annos = request.getParameter("annos");
        String diabetes = request.getParameter("diabetes");
        
        String obesidad = request.getParameter("obesidad");
        ep.setObesidad(Integer.parseInt(obesidad));
        
        String rge = request.getParameter("rge");
        ep.setRge(Integer.parseInt(rge));
        
        String hipo = request.getParameter("hipo");
        ep.setHipo(Integer.parseInt(hipo));
        String ulcera = request.getParameter("ulcera");
        ep.setUlcera(Integer.parseInt(ulcera));
        String alergias = request.getParameter("alergias");
        String cualesalergias = request.getParameter("cualesalergias");
        String coronario = request.getParameter("coronario");
        String arritmias = request.getParameter("arritmias");
        String coagulapatia = request.getParameter("coagulapatia");
        String valvular = request.getParameter("valvular");
        ep.setValvular(Integer.parseInt(valvular));
        String marcapasos = request.getParameter("marcapasos");
        ep.setMarcapasos(Integer.parseInt(marcapasos));
        String dislipidemia = request.getParameter("dislipidemia");
        String ecv = request.getParameter("ecv");
        String depresion = request.getParameter("depresion");
        String alcohol = request.getParameter("alcohol");
        String esquizofrenia = request.getParameter("esquizofrenia");
        ep.setEsquizofrenia(Integer.parseInt(esquizofrenia));
        String epilepsia = request.getParameter("epilepsia");
        String otraenfermedad = request.getParameter("otraenfermedad");
        String intervencion = request.getParameter("intervencion");
        ep.setIntervencion(Integer.parseInt(intervencion));
        String anastesico = request.getParameter("anastesico");
        String otraanastesico = request.getParameter("otraanastesico");
        String tratamiento = request.getParameter("tratamiento");
        String observaciones = request.getParameter("observaciones").replace("'", "");
        String cita = request.getParameter("cita");
        String reddeapoyo = request.getParameter("reddeapoyo").replace("'", "");
        ep.setReddeapoyo(reddeapoyo);
        String especificacionalcohol = request.getParameter("especificaciondealcohol");
        ep.setEspecificacionalcohol(especificacionalcohol);
        
        String nefropatia = request.getParameter("nefropatia");
        String infeccionurinaria = request.getParameter("infeccionurinaria");
        String uropatia = request.getParameter("uropatia");
        String hta = request.getParameter("hta");
        
        String descripcion_ventilatorios = request.getParameter("descripcion_ventilatorios").replace("'", "");
        String descripcion_metabolicos = request.getParameter("descripcion_metabolicos").replace("'", "");
        String descripcion_cardiovasculares = request.getParameter("descripcion_cardiovasculares").replace("'", "");
        String descripcionneuro = request.getParameter("descripcionneuro").replace("'", "");
        String descripcion_urinarios = request.getParameter("descripcion_urinarios").replace("'", "");
        String otrosantecedentesmorbidos = request.getParameter("otrosantecedentesmorbidos").replace("'", "");
        String descripcion_antecedentes = request.getParameter("descripcion_antecedentes").replace("'", "");
        String descripcion_examenes = request.getParameter("descripcion_examenes").replace("'", "");
        String ipa = request.getParameter("ipa");
        String pase = request.getParameter("pase");
        
        ep.setDescripcion_ventilatorios(descripcion_ventilatorios);
        ep.setDescripcion_metabolicos(descripcion_metabolicos);
        ep.setDescripcion_cardiovasculares(descripcion_cardiovasculares);
        ep.setDescripcionneuro(descripcionneuro);
        ep.setDescripcion_urinarios(descripcion_urinarios);
        ep.setOtrosantecedentesmorbidos(otrosantecedentesmorbidos);
        ep.setDescripcion_antecedentes(descripcion_antecedentes);
        ep.setDescripcion_examenes(descripcion_examenes);
        ep.setNefropatia(Integer.parseInt(nefropatia));
        ep.setUropatiaobstructiva(Integer.parseInt(infeccionurinaria));
        ep.setInfeccionurinaria(Integer.parseInt(uropatia));
        ep.setPase(Integer.parseInt(pase));
        ep.setIpa(Double.parseDouble(ipa));
        ep.setHta(Integer.parseInt(hta));
        String paretro = "";
        String cardiacaretro = "";
        String satretro = "";
        int pertinencia = -1;
        int atencion = Integer.parseInt(request.getParameter("idatencioncita"));
        if (atencion == 1) {
            pertinencia = Integer.parseInt(request.getParameter("pertinencia"));
            ep.setPertinencia(pertinencia);
        } else {
            ep.setPertinencia(pertinencia);
        }

        /*retro*/
        if (request.getParameter("retrocontrol") != null) {
            if (request.getParameter("retrocontrol").equals("checkbox")) {
                ep.setRetrocontrol(1);
                paretro = request.getParameter("paretro");
                cardiacaretro = request.getParameter("cardiacaretro");
                satretro = request.getParameter("satretro");
            }
        } else {
            ep.setRetrocontrol(0);
        }
        
        ep.setParetro(paretro);
        ep.setCardiacaretro(cardiacaretro);
        ep.setSatretro(satretro);
        controlador_especialidad ce = new controlador_especialidad();
        ep.setIdentrevista_preoperatoria(Integer.parseInt(identrevista));
        ep.setIdCita(Integer.parseInt(cita));
        ep.setAlcohol(Integer.parseInt(alcohol));
        ep.setAlergias(Integer.parseInt(alergias));
        ep.setAnos(annos);
        ep.setAntecendetes_anestesicos(Integer.parseInt(anastesico));
        ep.setArritmias(Integer.parseInt(arritmias));
        ep.setAsma(Integer.parseInt(asma));
        ep.setCardiaca(cardiaca);
        ep.setCirugia_propuesta(cirugiapropuesta);
        ep.setCoagulopatia(Integer.parseInt(coagulapatia));
        ep.setCoronario(Integer.parseInt(coronario));
        ep.setCualesalergias(cualesalergias);
        ep.setDepresion(Integer.parseInt(depresion));
        ep.setDesde(desde);
        ep.setDiabetes(Integer.parseInt(diabetes));
        ep.setDiagnostico(diagnosticoenfermera);
        ep.setDislipidemia(Integer.parseInt(dislipidemia));
        ep.setEcv(Integer.parseInt(ecv));
        ep.setEpilepsia(Integer.parseInt(epilepsia));
        ep.setEspecificar(otraanastesico);
        ep.setPeso(peso);
        ep.setTalla(talla);
        ep.setImc(imc);
        ep.setPa(pa);
        ep.setSat(sat);
        ep.setOtrasenfpsiq(otraenfermedad);
        ep.setTratamieto(tratamiento);
        ep.setObservaciones(observaciones);
        int modifique = ce.modificarEntrevistaPreoperatoria(ep);
        Controlador.controlador_cita cc = new Controlador.controlador_cita();
        
        String mensaje = "";
        String pasar = "";
        /*busco el id de la entrevistay registro*/
        if (modifique == 1) {
            /*registro examenes de la entrevista*/
            boolean tengo = ce.buscarsitengoexamenesdeEntrevista(Integer.parseInt(identrevista));
            
            String hemotocrito = request.getParameter("hemotocrito");
            String glicemia = request.getParameter("glicemia");
            String protobina = request.getParameter("protobina");
            String rectoblancos = request.getParameter("rectoblancos");
            String bun = request.getParameter("bun");
            String ttpk = request.getParameter("ttpk");
            String plaquetas = request.getParameter("plaquetas");
            String nak = request.getParameter("nak");
            String ecg = request.getParameter("ecg");
            String gruporh = request.getParameter("gruporh");
            String orinacompleta = request.getParameter("orinacompleta");
            String otrosexamenes = request.getParameter("otrosexamenes");
            lugar l = new lugar();
            /*tomo los String de las clases para no crear modelos*/
            l.setId_atencion(Integer.parseInt(identrevista));
            l.setComuna(hemotocrito);
            l.setDescripcion(glicemia);
            l.setDetalle_de_la_derivacion(protobina);
            l.setDiagnostico(rectoblancos);
            l.setDireccion(bun);
            l.setEdad(ttpk);
            l.setEmail(plaquetas);
            l.setExamenesrealizados(nak);
            l.setFechanacimiento(ecg);
            l.setFolio(gruporh);
            l.setIndicaciones(orinacompleta);
            l.setIntervencio(otrosexamenes);
            
            if (tengo == true) {
                ce.modificarexamenesEntrevista(l);
            } else {
                ce.insertarexamenesEntrevistaPreoperatorio(l);
            }
            
             if (request.getParameter("fechacontrol") != null && !request.getParameter("fechacontrol").equals("")) {

                Modelos.atencion a = new Modelos.atencion();
                String fechacontrol = request.getParameter("fechacontrol");
                Date fechac;
                Date fecha1 = new Date(Integer.parseInt(fechacontrol.substring(6, 10)) - 1900, Integer.parseInt(fechacontrol.substring(3, 5)) - 1, Integer.parseInt(fechacontrol.substring(0, 2)), 0, 0, 0);
                a.setFecha_registro(fecha1);
                a.setDetallereceta(request.getParameter("motivocontrol"));
                a.setUsuario(ce.getUsuarioLoggin());
                a.setIdcita(Integer.parseInt(cita));
                boolean tengor = ce.buscarsiExistecontrol(Integer.parseInt(cita));
                if (tengor == false) {
                    ce.InsertarsolicituddeControl(a);
                } else {
                    ce.modificarsitengoControl(a);
                }
            }else{
                boolean tengor = ce.buscarsiExistecontrol(Integer.parseInt(cita));
                if (tengor == true) {
                    ce.eliminarcontrol(Integer.parseInt(cita));
                } 
             }
            cc.cambiarcitaaAtencionGuardadaperonoFinalizada(Integer.parseInt(cita));
            /*cambio estado de la cita*/
            mensaje = "Entrevista Guardada exitosamente";
            pasar = "2";
            
        } else {
            mensaje = "Ocurrio un ERROR!! ";
            pasar = "0";
        }
        
        response.sendRedirect("vistas/Especialidades/ListaPacientesEntrevista.jsp?pasar=" + pasar + "&identrevista=" + identrevista + "&mensaje=" + mensaje);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
