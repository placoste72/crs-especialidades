/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.General;
import Modelos.solicitudExamenes_Procedimientos;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author a
 */
public class carrito_solicitud extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        General g = new General();
        HttpSession session1 = request.getSession(); // cambiado fuera del try el 24 de enero
        try {

            ArrayList carrito_solicitud;
            ArrayList<solicitudExamenes_Procedimientos> carrito_adicional;
            /*parametros recibidos*/
            int idexamenes = Integer.parseInt(request.getParameter("examen"));
            int idlaateralidad = Integer.parseInt(request.getParameter("lateralidad"));
            int idtiempo = Integer.parseInt(request.getParameter("tiempo"));

            //Si no existe la sesion creamos al carrito de compras
            if (session1.getAttribute("carrito_ficha") == null) {
                carrito_solicitud = new ArrayList();
                carrito_adicional = new ArrayList<solicitudExamenes_Procedimientos>();
            } else {
                carrito_solicitud = (ArrayList) session1.getAttribute("carrito_ficha");
                carrito_adicional = (ArrayList<solicitudExamenes_Procedimientos>) session1.getAttribute("carrito_adicional");
            }

            /*
            modx==1  ingresa
            modx==2  elimina
            
             */
            solicitudExamenes_Procedimientos axu = new solicitudExamenes_Procedimientos();
            axu.setIdexamen(idexamenes);
            axu.setIdlateralidad(idlaateralidad);
            axu.setIdtiempo(idtiempo);
            carrito_adicional.add(axu);
            //"" + axu.getPer_nombre()

            response.sendRedirect(g.getLocallink()+"AtencionOftalmologia/AtencionOftalmologia.jsp");

        } catch (ArrayIndexOutOfBoundsException ex) {
            out.write("<br><b>Ocurrio un error, intente nuevamente<b><br><br>");
            out.write("<a href='inicio.jsp'>Ir al inicio...</a><br><br>" + ex);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
