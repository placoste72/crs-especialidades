/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_especialidad;
import Controlador.controlador_cita;
import Modelos.cita;
import Modelos.lugar;
import Modelos.hojadiaria;
import Modelos.atencion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class registrarhojadiaria extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String cita = request.getParameter("cita");
         controlador_cita cc = new controlador_cita();
         controlador_especialidad ce = new controlador_especialidad();
        
        
        String destinopaciente = request.getParameter("destinopaciente");
        String otrodestino = request.getParameter("otrodestino");
        String sic = request.getParameter("sic");
        String ges = request.getParameter("ges");
        String detalledederivacion = request.getParameter("detalledederivacion");
        String constanciages= request.getParameter("constanciages");
        String diagnosticosic = request.getParameter("diagnosticosic");
        String otrodiagnosticosic = request.getParameter("otrodiagnosticosic");
        
        
        
        /*colocar especialidad y atencion*/
        int especialidad = Integer.parseInt(request.getParameter("especialidad"));
        int  idatencioncita = Integer.parseInt(request.getParameter("idatencioncita"));
        int pertinencia = -1;
         if (especialidad != 69){
             if(idatencioncita == 1){
                 pertinencia = Integer.parseInt(request.getParameter("pertinencia"));
             }
         }
        
        int idhoja= ce.buscariddeHojadiariaporCita(Integer.parseInt(cita));
        cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
       
        int idespecialidad = 0;
        idespecialidad = c.getId_patologia();
        Vector<lugar> vl = ce.buscarDiagnosticosporEspecialidad(idespecialidad);
        int diagnostico[] = new int[vl.size()];
        boolean tengodiagnostico = false;
        boolean tengocataratas = false;
        String otroindicaciones = "";

        for (int i = 0; i < vl.size(); i++) {
            if (request.getParameter("diagnostico" + vl.get(i).getId_atencion()) != null) {
                if (request.getParameter("diagnostico" + vl.get(i).getId_atencion()).equals("checkbox")) {
                    diagnostico[i] = vl.get(i).getId_atencion();
                    tengodiagnostico = true;
                   if(diagnostico[i] == 2 || diagnostico[i] == 31 ){
                       tengocataratas = true;
                   } 
                    
                }

            }
        }

        String otrodiagnostico = request.getParameter("otrodiagnostico");
        Vector<lugar> vlp = ce.buscarlosProcedimientosporEspecialidad(idespecialidad);
        boolean tengoprocedimientos = false;
        int procedimientos[] = new int[vlp.size()];
        for (int i = 0; i < vlp.size(); i++) {
            if (request.getParameter("procedimiento" + vlp.get(i).getId_atencion()) != null) {
                if (request.getParameter("procedimiento" + vlp.get(i).getId_atencion()).equals("checkbox")) {
                    procedimientos[i] = vlp.get(i).getId_atencion();
                    tengoprocedimientos = true;
                }

            }
        }

        String otroprocedimiento = request.getParameter("otroprocedimiento");
        Vector<lugar> vle = ce.buscarlosExamenesporEspecialidad(idespecialidad);
        boolean tengoexamenes = false;
        int examenes[] = new int[vle.size()];
        for (int i = 0; i < vle.size(); i++) {
            if (request.getParameter("examenes" + vle.get(i).getId_atencion()) != null) {
                if (request.getParameter("examenes" + vle.get(i).getId_atencion()).equals("checkbox")) {
                    examenes[i] = vle.get(i).getId_atencion();
                    tengoexamenes = true;
                }

            }
        }

        String otroexamene = request.getParameter("otroexamene");

        /*indicaciones*/
        Vector<lugar> vli = ce.buscarlasindicacionesporEspecialidad(idespecialidad);
        boolean tengoindicacion = ce.buscarsitengoindicaciones(idespecialidad);
        boolean tengoindicaciones = false;
        int indicaciones[] = new int[vli.size()];
        int cantidades[] = new int[vli.size()];
         String detallereceta[]= new String[vli.size()];
        if (tengoindicacion == true) {
            /*indicaciones*/

            for (int i = 0; i < vli.size(); i++) {
                if (request.getParameter("indicaciones" + vli.get(i).getId_atencion()) != null) {
                    if (request.getParameter("indicaciones" + vli.get(i).getId_atencion()).equals("checkbox")) {
                        indicaciones[i] = vli.get(i).getId_atencion();
                        if (request.getParameter("cantidad" + vli.get(i).getId_atencion()) != null){
                        cantidades[i] = Integer.parseInt(request.getParameter("cantidad" + vli.get(i).getId_atencion()));
                        }else{
                           cantidades[i] = 1; 
                        }
                        tengoindicaciones = true;
                    }

                }
            }

            otroindicaciones = request.getParameter("otroindicaciones");

        }

        hojadiaria hd = new hojadiaria();
        hd.setConstancia(Integer.parseInt(constanciages));
        hd.setDiagnosticosic(Integer.parseInt(diagnosticosic));
        hd.setDiagnosticootrosic(otrodiagnosticosic);
        hd.setGes(Integer.parseInt(ges));
        hd.setIdcita(Integer.parseInt(cita));
        hd.setIddestinopaciente(Integer.parseInt(destinopaciente));
        hd.setOtras_indicaciones(otroindicaciones);
        hd.setOtro_diagnostico(otrodiagnostico);
        hd.setOtro_examen(otroexamene);
        hd.setOtro_procedimiento(otroprocedimiento);
        hd.setOtrodestino_paciente(otrodestino);
        hd.setSic(Integer.parseInt(sic));
       hd.setPertinencia(pertinencia);
        if(Integer.parseInt(ges)==1 && Integer.parseInt(constanciages) ==1 && tengocataratas == true ){
            hd.setFundamentocataratas(request.getParameter("fundamentodeldiagnostico"));
            hd.setTratamientocataratas(request.getParameter("tratamientoeindicaciones"));
        }
        
        /*solicitudes de pabellon*/
        if (request.getParameter("solicituddepabellon") != null) {
            if (request.getParameter("solicituddepabellon").equals("checkbox")) {
                hd.setSolicitudpabellon(true);
            }
        } else {
            hd.setSolicitudpabellon(false);
        }
        hd.setParaquesederive(detalledederivacion);
        if (idhoja != 0){
          hd.setIdhojadiaria(idhoja);
          ce.modificarhojadiaria(hd);
          ce.eliminarDiagnosticohojadiaria(idhoja);
          ce.eliminarExameneshojadiaria(idhoja);
          ce.eliminarIndicacioneshojadiaria(idhoja);
          ce.eliminarprocedimientoshojadiaria(idhoja);
        }else{
        ce.insertarHojadiaria(hd);
        }
        /*busco el id de la hojadiaria por cita*/
        int idhojadiaria = ce.buscariddeHojadiariaporCita(Integer.parseInt(cita));
        /*agregar solicitudes*/
        if (request.getParameter("numerodesolicitudes") != "") {
            int cantidad = Integer.parseInt(request.getParameter("numerodesolicitudes"));
            String diagnosticosolicitud = "";
            String intervencionsolicitud = "";
            String anestesiasolicitud = "";
            String requerimientossolicitud = "";
            String observacionsolicitud = "";

            for (int i = 0; i < cantidad; i++) {
                diagnosticosolicitud = request.getParameter("diagnosticos"+i);
                intervencionsolicitud = request.getParameter("intervencion"+i);
                anestesiasolicitud = request.getParameter("anestesia"+i);
                requerimientossolicitud = request.getParameter("requerimientos"+i);
                observacionsolicitud = request.getParameter("observacion"+i);
                otrodiagnostico = request.getParameter("otrodiagnostico"+i);
                if( (!diagnosticosolicitud.equalsIgnoreCase("")) & (idhojadiaria != 0)){
                   lugar solicituddocumento = new lugar();
                   solicituddocumento.setId_atencion(idhojadiaria);
                   solicituddocumento.setId_diagnostico_urgencia(Integer.parseInt(diagnosticosolicitud));
                   solicituddocumento.setDescripcion(intervencionsolicitud);
                   solicituddocumento.setId_lugar(Integer.parseInt(anestesiasolicitud));
                   solicituddocumento.setDiagnostico(requerimientossolicitud);
                   solicituddocumento.setIndicaciones(observacionsolicitud);
                   solicituddocumento.setIntervencio(otrodiagnostico);
                   solicituddocumento.setEstatu(1);
                   ce.insertarSolicitudQuirofanoHojaDiaria(solicituddocumento);
                   
                }
            }

        }

        String mensaje = "";
        if (idhojadiaria != 0) {
            if (tengodiagnostico == true) {
                ce.insertardiagnosticoshojadiaria(diagnostico, idhojadiaria);

            }
            if (tengoprocedimientos == true) {
                ce.insertarprocedimientoshojadiaria(procedimientos, idhojadiaria, 1);
            }
            if (tengoexamenes == true) {
                ce.insertarexameneshojadiaria(examenes, idhojadiaria,1);
            }

            if (tengoindicaciones == true) {
                ce.insertarindicacioneshojadiaria(indicaciones, idhojadiaria, cantidades,detallereceta,1);
            }
            
               if (request.getParameter("fechacontrol") != null && !request.getParameter("fechacontrol").equals("")) {
                    atencion aten = new atencion();
                    String fechacontrol = request.getParameter("fechacontrol");
                    Date fecha1 = new Date(Integer.parseInt(fechacontrol.substring(6, 10)) - 1900, Integer.parseInt(fechacontrol.substring(3, 5)) - 1, Integer.parseInt(fechacontrol.substring(0, 2)), 0, 0, 0);
                    
                    aten.setFecha_registro(fecha1);
                    aten.setDetallereceta(request.getParameter("motivocontrol"));
                    aten.setUsuario(ce.getUsuarioLoggin());
                    aten.setIdcita(Integer.parseInt(cita));
                    ce.InsertarsolicituddeControl(aten);
                }
            mensaje = "Registro Exitoso!!";
            cc.cambiarCitaAatendida(Integer.parseInt(cita));
            
            
            
        } else {
            mensaje = "Ocurrio un error";

        }
        response.sendRedirect(ce.getLocallink() + "Documentos/documentosEspecialidades.jsp?men=" + mensaje + "&hojadiario=" + idhojadiaria);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
