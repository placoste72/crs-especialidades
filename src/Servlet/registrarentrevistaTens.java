/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_especialidad;
import Controlador.controlador_cita;
import Modelos.entrevista_preoperatoria;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class registrarentrevistaTens extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        controlador_especialidad ce = new controlador_especialidad();
        String cita = request.getParameter("cita");
        
        String peso = request.getParameter("peso");
        String talla = request.getParameter("talla");
        String imc = request.getParameter("imc");
        String pa = request.getParameter("pa");
        String cardiaca = request.getParameter("cardiaca");
        String sat = request.getParameter("sat");
        /*registro el tens*/
        entrevista_preoperatoria ep= new entrevista_preoperatoria();
        controlador_cita cc= new controlador_cita();
        ep.setIdCita(Integer.parseInt(cita));
        ep.setEstatus(1);
       
        ep.setPeso(peso);
        ep.setTalla(talla);
        ep.setImc(imc);
        ep.setPa(pa);
        ep.setCardiaca(cardiaca);
        ep.setSat(sat);
        /*colocar todos los combos como -1*/
        ep.setAlergias(-1);
        ep.setCoagulopatia(-1);
        ep.setCoronario(-1);
        ep.setDepresion(-1);
        ep.setDiabetes(-1);
        ep.setEcv(-1);
        ep.setValvular(-1);
        ep.setUlcera(-1);
        ep.setTuberculosis(-1);
        ep.setUropatiaobstructiva(-1);
        ep.setAlcohol(-1);
        ep.setArritmias(-1);
        ep.setAsma(-1);
        ep.setDislipidemia(-1);
        ep.setEpilepsia(-1);
        ep.setHipo(-1);
        ep.setHta(-1);
        ep.setMarcapasos(-1);
        ep.setIra(-1);
        ep.setInfeccionurinaria(-1);
        ep.setTuberculosis(-1);
       
        ep.setRge(-1);
        ep.setRinitis(-1);
        ep.setObesidad(-1);
        ep.setAntecendetes_anestesicos(-1);
        ep.setIntervencion(-1);
        ep.setNefropatia(-1);
        ep.setEsquizofrenia(-1);
        ep.setPase(-1);
        /*registro para tens y cambio estatus de la cita a 5 vista por el tens*/
        
        ce.insertarEntrevistaPreoperatoria(ep);
        int identrevista = ce.idEntrevistaPreoperatoriaporIdCita(Integer.parseInt(cita));
        String mensaje = "";
        if (identrevista != 0) {
            mensaje = "Paciente Atendido con Exito!!";
             cc.CambiarCitapasoporTecnico(Integer.parseInt(cita));
        }else{
            mensaje = "Ocurrio un Error!! Intente de Nuevo";
        }
          response.sendRedirect("vistas/Especialidades/AtencionClinicaEnfermeria.jsp?men=" + mensaje);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
