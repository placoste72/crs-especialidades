/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_atencion;
import Controlador.controlador_atencion_clinica_oftalmologia;
import Controlador.controlador_cita;
import Controlador.controlador_especialidad;
import Modelos.cita;
import Modelos.excepcion_garantia;
import Modelos.lugar;
import Modelos.receta_lente_opticos;
import Modelos.solicitudExamenes_Procedimientos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class modificacionAtencionOftalmologia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String cita = request.getParameter("cita");
        controlador_cita cc = new controlador_cita();
        controlador_especialidad ce = new controlador_especialidad();
        controlador_atencion ca = new controlador_atencion();
        controlador_atencion_clinica_oftalmologia caco = new controlador_atencion_clinica_oftalmologia();
        String mensaje = "";
        receta_lente_opticos rlo = new receta_lente_opticos();

        /*datos de atencion*/
        String resumenatencion = request.getParameter("resumenatencion").replaceAll("'", "''");
        String otrodiagnostico = request.getParameter("otrodiagnostico");
        String otroprocedimiento = request.getParameter("otroprocedimiento");
        String otroexamene = request.getParameter("otroexamene");
        String sic = request.getParameter("sic");
        String diagnosticosic = request.getParameter("diagnosticosic");
        String otrodiagnosticosic = request.getParameter("otrodiagnosticosic");
        String destinopaciente = request.getParameter("destinopaciente");
        String otrodestino = request.getParameter("otrodestino");
        String detalledederivacion = request.getParameter("detalledederivacion");
        String indialt = request.getParameter("indialt");


        /*pertinencia*/
        int idatencioncita = Integer.parseInt(request.getParameter("idatencioncita"));
        int pertinencia = -1;
        if (idatencioncita == 1) {

            pertinencia = Integer.parseInt(request.getParameter("pertinencia"));
        }

        String otroindicaciones = request.getParameter("otroindicaciones");

        /*atencion */
        Modelos.atencion ao = new Modelos.atencion();
        ao.setIdcita(Integer.parseInt(cita));
        ao.setResumenatenicon(resumenatencion);
        ao.setOtrodiagnostico(otrodiagnostico);
        ao.setOtroexamenes(otroexamene);
        ao.setSic(Integer.parseInt(sic));
        ao.setIddestinopaciente(Integer.parseInt(destinopaciente));
        ao.setOtrodestino(otrodestino);

        ao.setOtrasindicaciones(otroindicaciones);
        boolean solicitud = false;
        ao.setSolicitudpabellon(solicitud);
        ao.setDetallederivacion(detalledederivacion);
        ao.setDiagnosticosic(Integer.parseInt(diagnosticosic));
        ao.setOtrodiagnosticosic(otrodiagnosticosic);
        ao.setOtroprocedimiento(otroprocedimiento);
        ao.setPertinencia(pertinencia);
        ao.setIndialt(indialt);

        /*inserto la atencion*/
        int idatencion = ca.idatencionporCitaoftalmologia(Integer.parseInt(cita));
        ao.setIdatencion(idatencion);
        ca.modificaratencionoftalmologia(ao);

        cita c = cc.buscarCitaporIdParaAtencion(Integer.parseInt(cita));
        /*elimino cualquier diagnostico, procedimientos o examenes*/
        ca.eliminardiagnosicosoftalmologia(ao);
        ca.eliminarlasprocedimientosOftalmologia(ao);
        ca.eliminarlasExamenesOftalmologia(ao);

        /*diagnostico-atencion*/
        int idespecialidad = 0;
        idespecialidad = c.getId_patologia();
        Vector<lugar> vl = ce.buscarDiagnosticosporEspecialidad(idespecialidad);

        for (int i = 0; i < vl.size(); i++) {
            if (request.getParameter("diagnostico" + vl.get(i).getId_atencion()) != null) {
                if (request.getParameter("diagnostico" + vl.get(i).getId_atencion()).equals("checkbox")) {
                    Modelos.atencion di = new Modelos.atencion();

                    Modelos.atencion a = new Modelos.atencion();
                    a.setIdatencion(idatencion);
                    a.setIddiagnostico(vl.get(i).getId_atencion());
                    a.setLateralidad(Integer.parseInt(request.getParameter("lateralidad" + vl.get(i).getId_atencion())));
                    a.setGes(Integer.parseInt(request.getParameter("ges" + vl.get(i).getId_atencion())));
                    a.setConfirmadescartadocumentos(Integer.parseInt(request.getParameter("confirmadescarta" + vl.get(i).getId_atencion())));
                    a.setFundamentodeldiagnostico(request.getParameter("fundamentodeldiagnostico" + vl.get(i).getId_atencion()));
                    a.setTratamientoeindicaciones(request.getParameter("tratamientoeindicaciones" + vl.get(i).getId_atencion()));
                    a.setConfirmadescartadiagnostico(Integer.parseInt(request.getParameter("confirmodecartod" + vl.get(i).getId_atencion())));
                    ca.diagnosticooftalmologia(a);
                }

            }
        }

        Vector<lugar> vlp = ce.buscarlosProcedimientosporEspecialidad(idespecialidad);
        boolean tengoprocedimientos = false;
        int procedimientos[] = new int[vlp.size()];
        for (int i = 0; i < vlp.size(); i++) {
            if (request.getParameter("procedimiento" + vlp.get(i).getId_atencion()) != null) {
                if (request.getParameter("procedimiento" + vlp.get(i).getId_atencion()).equals("checkbox")) {
                    procedimientos[i] = vlp.get(i).getId_atencion();
                    tengoprocedimientos = true;
                }

            }
        }

        Vector<lugar> vle = ce.buscarlosExamenesporEspecialidad(idespecialidad);
        boolean tengoexamenes = false;
        int examenes[] = new int[vle.size()];
        for (int i = 0; i < vle.size(); i++) {
            if (request.getParameter("examenes" + vle.get(i).getId_atencion()) != null) {
                if (request.getParameter("examenes" + vle.get(i).getId_atencion()).equals("checkbox")) {
                    examenes[i] = vle.get(i).getId_atencion();
                    tengoexamenes = true;
                }

            }
        }

        /*elmino las indicacions*/
        ca.eliminarlasIndicacionesOftalmologia(ao);
        /*indicaciones*/
        Vector<lugar> vli = ce.buscarlasindicacionesporEspecialidad(idespecialidad);
        boolean tengoindicacion = ce.buscarsitengoindicaciones(idespecialidad);
        boolean tengoindicaciones = false;
        int indicaciones[] = new int[vli.size()];
        int cantidades[] = new int[vli.size()];
        String detallereceta[] = new String[vli.size()];
        if (tengoindicacion == true) {
            /*indicaciones*/

            for (int i = 0; i < vli.size(); i++) {
                if (request.getParameter("indicaciones" + vli.get(i).getId_atencion()) != null) {
                    if (request.getParameter("indicaciones" + vli.get(i).getId_atencion()).equals("checkbox")) {
                        indicaciones[i] = vli.get(i).getId_atencion();

                        cantidades[i] = 1;

                        tengoindicaciones = true;

                        /*registrar receta*/
                        detallereceta[i] = request.getParameter("descripcionreceta" + vli.get(i).getId_atencion()).replaceAll("'", "''").toUpperCase();

                    }

                }
            }

        }

        /*solicitud es de manera diferente */
        ca.eliminardsolicitudquirofanooftalmologia(ao);
        /*agregar solicitudes*/
        if (request.getParameter("numerodesolicitudes") != "") {
            int cantidad = Integer.parseInt(request.getParameter("numerodesolicitudes"));
            String diagnosticosolicitud = "";
            String intervencionsolicitud = "";
            String anestesiasolicitud = "";
            String requerimientossolicitud = "";
            String observacionsolicitud = "";
            String lateralidad = "";

            for (int i = 1; i <= cantidad; i++) {
                diagnosticosolicitud = request.getParameter("diagnosticos" + i);
                intervencionsolicitud = request.getParameter("intervencion" + i);
                anestesiasolicitud = request.getParameter("anestesia" + i);
                requerimientossolicitud = request.getParameter("requerimientos" + i);
                observacionsolicitud = request.getParameter("observacion" + i);
                otrodiagnostico = request.getParameter("otrodiagnostico" + i);
                lateralidad = request.getParameter("lateralidades" + i);

                lugar solicituddocumento = new lugar();
                solicituddocumento.setId_atencion(idatencion);
                solicituddocumento.setId_diagnostico_urgencia(Integer.parseInt(diagnosticosolicitud));
                solicituddocumento.setDescripcion(intervencionsolicitud);
                solicituddocumento.setId_lugar(Integer.parseInt(anestesiasolicitud));
                solicituddocumento.setDiagnostico(requerimientossolicitud);
                solicituddocumento.setIndicaciones(observacionsolicitud);
                solicituddocumento.setIntervencio(otrodiagnostico);
                solicituddocumento.setEstatu(2);
                solicituddocumento.setGes(Integer.parseInt(lateralidad));
                ce.insertarSolicitudQuirofanoHojaDiaria(solicituddocumento);
                solicitud = true;

            }
            ca.modificarquetengosolicituddeQuirofano(idatencion, solicitud);

        }

        ca.eliminarlasNOsolicitudquirofanooftalmologia(ao);
        /*NO INDICACIONES DE CIRUGIA*/
        if (request.getParameter("numerodesolicitudesnrpt") != "") {
            int cantidad = Integer.parseInt(request.getParameter("numerodesolicitudesnrpt"));
            String diagnosticonrpt = "";
            String otrodiagnosticonrpt = "";

            String intervencionnrpt = "";
            String razonesnrpt = "";
            String indicacionesnrpt = "";
            String lateralidadesnrpt = "";

            for (int i = 1; i <= cantidad; i++) {
                diagnosticonrpt = request.getParameter("diagnosticonrpt" + i);
                intervencionnrpt = request.getParameter("intervencionnrpt" + i);

                indicacionesnrpt = request.getParameter("indicacionesnrpt" + i);
                razonesnrpt = request.getParameter("razonesnrpt" + i);
                otrodiagnosticonrpt = request.getParameter("otrodiagnosticonrpt" + i);
                lateralidadesnrpt = request.getParameter("lateralidadesnrpt" + i);

                lugar solicituddocumento = new lugar();
                solicituddocumento.setId_atencion(idatencion);
                solicituddocumento.setId_diagnostico_urgencia(Integer.parseInt(diagnosticonrpt));
                solicituddocumento.setDescripcion(intervencionnrpt);

                solicituddocumento.setDiagnostico(razonesnrpt);
                solicituddocumento.setIndicaciones(indicacionesnrpt);
                solicituddocumento.setIntervencio(otrodiagnosticonrpt);

                solicituddocumento.setGes(Integer.parseInt(lateralidadesnrpt));
                /*REGISTRO*/
                ca.noIndicaciondeCirugia(solicituddocumento);

            }

        }

        /*PROCEDIMIENTOS*/
        if (tengoprocedimientos == true) {
            ce.insertarprocedimientoshojadiaria(procedimientos, idatencion, 2);
        }
        if (tengoexamenes == true) {
            ce.insertarexameneshojadiaria(examenes, idatencion, 2);
        }

        if (tengoindicaciones == true) {
            ce.insertarindicacioneshojadiaria(indicaciones, idatencion, cantidades, detallereceta, 2);
            ca.IncrementarIdReceta(idatencion);

        }
        /*excepcion de garantia */

        String problemaauge = "";
        int rpd = 0;
        int rapg = 0;
        int pr = 0;
        int ocdp = 0;
        String observaciongarantia = "";

        boolean tengogarantia = false;
        if (request.getParameter("problemaauge") != "") {
            tengogarantia = true;
            problemaauge = request.getParameter("problemaauge");

            if (request.getParameter("rpd") != null) {
                if (request.getParameter("rpd").equals("checkbox")) {
                    rpd = 1;
                }
            }

            if (request.getParameter("rapg") != null) {
                if (request.getParameter("rapg").equals("checkbox")) {
                    rapg = 1;
                }
            }

            if (request.getParameter("pr") != null) {
                if (request.getParameter("pr").equals("checkbox")) {
                    pr = 1;
                }
            }

            if (request.getParameter("ocdp") != null) {
                if (request.getParameter("ocdp").equals("checkbox")) {
                    ocdp = 1;
                }
            }

            if (!request.getParameter("observaciongarantia").equals("")) {
                observaciongarantia = request.getParameter("observaciongarantia");
            }
        }

        if (tengogarantia == true) {
            excepcion_garantia eg = new excepcion_garantia();
            eg.setProblemaauge(problemaauge);
            eg.setId_atencion(idatencion);
            eg.setTipo(2);
            eg.setObservaciones(observaciongarantia);
            eg.setOtra_causa(ocdp);
            eg.setPrestacion_rechazada(pr);
            eg.setRechazo_atencion_garantizado(rapg);
            eg.setRechazo_prestador_designado(rpd);
            /*buscar excepcion de garantia para ver si modifico*/
            excepcion_garantia gx = caco.buscarGarantiaporIddeAtencion(idatencion, 2);
            if (gx.getIdexcepcion_garantia() != 0) {
                eg.setIdexcepcion_garantia(gx.getIdexcepcion_garantia());
                caco.ModificarGarantia(eg);
            } else {
                caco.insertarGarantia(eg);
            }
        } else {
            ca.eliminarlasExcepciondegaratia(ao);
        }

        String[] examenessolicitados = null;
        String[] procedimientossolicitados = null;
        boolean tengoalgo = false;
        /*Solicitudes de examen oftalmologico*/

        String observacionsolicitud = request.getParameter("observacionsolicitud");

        solicitudExamenes_Procedimientos sep = new solicitudExamenes_Procedimientos();
        sep.setEstatus(1);
        sep.setObservacion(observacionsolicitud);
        sep.setIdatencionoftalmologia(idatencion);

        int buscarsolicituddeatencion = 0;
        /*elimino todas las atenciones*/
        buscarsolicituddeatencion = caco.buscaridSolicitudExproporidAtencion(idatencion);
        ca.eliminardetalleExamanenes_procedimiento(buscarsolicituddeatencion);

        boolean tsep = false;

        /*cambios para incluir lateralidad, tiempo*/
        for (lugar e : ce.buscarlosExamenesparaOftalmologia()) {

            if (request.getParameter("solicitudesexamenes" + e.getId_atencion()) != null) {
                tsep = true;
                int late = Integer.parseInt(request.getParameter("lateralidadexamenes" + e.getId_atencion()));
                int tiempo = Integer.parseInt(request.getParameter("tiempoexamenes" + e.getId_atencion()));

                solicitudExamenes_Procedimientos sepex = new solicitudExamenes_Procedimientos();
                buscarsolicituddeatencion = caco.buscaridSolicitudExproporidAtencion(idatencion);
                if (buscarsolicituddeatencion == 0) {
                    caco.InsertarSolicitudExamenes_Procedimiento(sep);
                    buscarsolicituddeatencion = caco.buscaridSolicitudExproporidAtencion(idatencion);
                } else {
                    sep.setIdsolicitud(buscarsolicituddeatencion);
                    ca.modificarDetalleExamane(sep);
                }

                sepex.setTipo(1);
                sepex.setEstatus(1);
                sepex.setLosolicitado(e.getId_atencion());
                sepex.setIdsolicitud(buscarsolicituddeatencion);
                sepex.setIdlateralidad(late);
                sepex.setIdtiempo(tiempo);
                caco.InsertarDetalleSolicitudExamenes_Procedimiento(sepex);

            }
        }

        /*Cambios para procedimientos */
        for (lugar e : ce.buscarlosProcedimientos()) {

            if (request.getParameter("solicitudesprocedimiento" + e.getId_atencion()) != null) {
                tsep = true;
                int late = Integer.parseInt(request.getParameter("lateralidadprocedimiento" + e.getId_atencion()));
                int tiempo = Integer.parseInt(request.getParameter("tiempoprocedimiento" + e.getId_atencion()));

                solicitudExamenes_Procedimientos sepex = new solicitudExamenes_Procedimientos();
                buscarsolicituddeatencion = caco.buscaridSolicitudExproporidAtencion(idatencion);
                if (buscarsolicituddeatencion == 0) {
                    caco.InsertarSolicitudExamenes_Procedimiento(sep);
                    buscarsolicituddeatencion = caco.buscaridSolicitudExproporidAtencion(idatencion);
                } else {
                    sep.setIdsolicitud(buscarsolicituddeatencion);
                    ca.modificarDetalleExamane(sep);
                }

                sepex.setTipo(2);
                sepex.setEstatus(1);
                sepex.setLosolicitado(e.getId_atencion());
                sepex.setIdsolicitud(buscarsolicituddeatencion);
                sepex.setIdlateralidad(late);
                sepex.setIdtiempo(tiempo);
                caco.InsertarDetalleSolicitudExamenes_Procedimiento(sepex);

            }
        }
        /*validar por si no tengo ni procedimiento ni examenes eliminar el examen procedimeitno*/

        if (tsep == false) {
            ca.eliminarSoliitudExamanenes_procedimiento(idatencion);
        }

        if (Integer.parseInt(request.getParameter("controlsolicito")) == 1) {
            Modelos.atencion a = new Modelos.atencion();
            String fechacontrol = request.getParameter("fechacontrol");
            Date fecha1 = new Date(Integer.parseInt(fechacontrol.substring(6, 10)) - 1900, Integer.parseInt(fechacontrol.substring(3, 5)) - 1, Integer.parseInt(fechacontrol.substring(0, 2)), 0, 0, 0);

            a.setFecha_registro(fecha1);
            a.setDetallereceta(request.getParameter("motivocontrol"));
            a.setUsuario(caco.getUsuarioLoggin());
            a.setIdcita(Integer.parseInt(cita));
            /*buco si tiene control y lo modifico o inserto*/
            Modelos.atencion atenc = caco.buscarControlporIdcita(Integer.parseInt(cita));
            if (atenc.getIdatencion() == 0) {

                caco.InsertarsolicituddeControl(a);
            } else {
                ca.modificarSolicituddeControl(a);
            }
        } else {
            /*eliminar control*/
            ca.eliminarSolicitudControlOftalmologia(Integer.parseInt(cita));
        }
        if (Integer.parseInt(request.getParameter("lentesolicito")) == 1) {
            /*lente*/

            if (request.getParameter("tratamientolentesCerca") != null) {
                if (request.getParameter("tratamientolentesCerca").equals("checkbox")) {

                    ao.setTratamientocerca(1);

                }

            }
            if (request.getParameter("tratamientolentesLejos") != null) {
                if (request.getParameter("tratamientolentesLejos").equals("checkbox")) {
                    ao.setTratamientolejos(1);

                }

            }
            ao.setIdatencion(idatencion);
            ca.modificarTratamientodeLejosyCerca(ao);

            if (!request.getParameter("odf").equals("")) {
                rlo.setOjoderechoesfera(request.getParameter("odf"));
            }

            if (!request.getParameter("odc").equals("")) {
                rlo.setOjoderechocilindro(request.getParameter("odc"));
            }

            if (!request.getParameter("ode").equals("")) {
                rlo.setOjoderechoeje(request.getParameter("ode"));
            }

            if (!request.getParameter("oie").equals("")) {
                rlo.setOjoizquierdoesfera(request.getParameter("oie"));
            }

            if (!request.getParameter("oic").equals("")) {
                rlo.setOjoiaquierdacilindro(request.getParameter("oic"));
            }

            if (!request.getParameter("oiej").equals("")) {
                rlo.setOjoizquierdaeje(request.getParameter("oiej"));
            }

            if (!request.getParameter("add").equals("")) {
                rlo.setAadd(request.getParameter("add"));
            }

            if (!request.getParameter("dpl").equals("")) {
                rlo.setDistanciapupilarlejos(request.getParameter("dpl"));
            }

            if (!request.getParameter("dpc").equals("")) {
                rlo.setDistanciapupilarcerca(request.getParameter("dpc"));
            }

            if (!request.getParameter("observacionesreceta").equals("")) {
                rlo.setObservaciones(request.getParameter("observacionesreceta"));
            }

            rlo.setTipo(1);
            rlo.setId_atencion(idatencion);
            int idreceta = 0;
            idreceta = caco.buscarRecetadeLentes(idatencion, 1);
            if (idreceta == 0) {
                caco.InsertarRecetaLenteOpticos(rlo);
            } else {
                rlo.setId_receta(idreceta);
                caco.ModificarRecetadeLentes(rlo);
            }

        } else {
            /*elimino posible receta de lente*/
            ca.eliminarlasrecetadeLENTEoftalmologia(ao);
        }

        mensaje = "Registro Exitoso!!";
        cc.cambiarCitaAatendida(Integer.parseInt(cita));

        response.sendRedirect(ce.getLocallink() + "AtencionOftalmologia/DocumentosOftalmologia.jsp?men=" + mensaje + "&idatencion=" + idatencion);

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
