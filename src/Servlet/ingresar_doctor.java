/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Modelos.doctor;
import Controlador.controlador_doctor;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;

/**
 *
 * @author Informatica
 */
public class ingresar_doctor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileUploadException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession respuesta = request.getSession(true);
            ServletRequestContext src = new ServletRequestContext(request);
            List lista2 = null;
            String rut = "";
            String nombre = "";
            String apellidop = "";
            String apellidom = "";
            String telefono = "";
            String email = "";
            int estatus = 1;
            String especialidades = "";
            int especialidad[] = new int[100];
            String profesion = "";

            boolean tienefirma = false;

            String et = "";
            if (ServletFileUpload.isMultipartContent(src)) {
                ServletContext context = this.getServletConfig().getServletContext();
                String path2 = context.getRealPath("").replace("nbbuild\\web/", "WebContent\\fotos");
                File destino = new File(path2);
                DiskFileItemFactory factory = new DiskFileItemFactory((1024 * 1024), destino);
                ServletFileUpload upload = new ServletFileUpload(factory);
                List lista = upload.parseRequest(src);

                lista2 = lista;
                File file = null;
                Iterator it = lista.iterator();

                boolean sw_observacion = false;
                boolean sw_observacion_revision = false;
                int i = 0;
                while (it.hasNext()) {
                    //Rescatamos el fileItem
                    FileItem item = (FileItem) it.next();

                    //Comprobamos si es un campo de formulario
                    if (item.isFormField()) //Hacemos lo que queramos con el.
                    {

                        if (item.getFieldName().equals("rutdoctor")) {
                            String fieldName = item.getFieldName();
                            rut = item.getString();
                        }

                        if (item.getFieldName().equals("nombredoctor")) {
                            String fieldName = item.getFieldName();
                            nombre = item.getString();
                        }

                        if (item.getFieldName().equals("apellidopdoctor")) {
                            String fieldName = item.getFieldName();
                            apellidop = item.getString();
                        }

                        if (item.getFieldName().equals("apellidomdoctor")) {
                            String fieldName = item.getFieldName();
                            apellidom = item.getString();
                        }

                        if (item.getFieldName().equals("telefonodoctor")) {
                            String fieldName = item.getFieldName();
                            telefono = item.getString();
                        }
                        if (item.getFieldName().equals("emaildoctor")) {
                            String fieldName = item.getFieldName();
                            email = item.getString();
                        }
                        if (item.getFieldName().equals("especialidades")) {
                            String fieldName = item.getFieldName();
                            especialidades = item.getString();
                            especialidad[i] = Integer.parseInt(especialidades);
                            i++;
                        }

                        if (item.getFieldName().equals("profesion")) {
                            profesion = item.getString();
                        }

                    } else if (item.getName().length() > 0) {

                        //Si no, es un fichero y lo subimos al servidor.
                        //Primero creamos un objeto file a partir del nombre del fichero.
                        file = new File(item.getName());

                        item.write(new File(destino, item.getName()));
                        String simbolo = " ";
                        simbolo = "\\";
                        et = destino + "\\" + item.getName();
                        tienefirma = true;
                    }
                }
            }
            doctor d = new doctor();
            d.setRut(rut);
            d.setNombre(nombre);
            d.setApellido_paterno(apellidop);
            d.setApellido_materno(apellidom);
            d.setTelefono(telefono);
            d.setEmail(email);
            d.setEstatus(estatus);
            d.setProfesion(profesion);
            controlador_doctor cd = new controlador_doctor();
            boolean existe = cd.buscardoctor(rut);
            String men = "";
            if (existe == true) {
                men = "Ya existe un doctor con esa Rut";
                response.sendRedirect(cd.getLocallink() + "Planificar/RegistrarDoctor.jsp?men=" + men);
            } else {
                if (tienefirma == true) {
                    men = cd.ingresarDoctor(d, et);
                } else {
                    men = cd.ingresarDoctorSinFoto(d);

                }
                cd.doctor_especialidad(rut, especialidad);

                response.sendRedirect(cd.getLocallink() + "Planificar/RegistrarDoctor.jsp?men=" + men);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ingresar_doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ingresar_doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
