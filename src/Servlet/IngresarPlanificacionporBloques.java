/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_cupos;
import Modelos.planificar;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class IngresarPlanificacionporBloques extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String rut = request.getParameter("funcionario");

        String horainicio = request.getParameter("horainicio");
        String horafin = request.getParameter("horafin");
        int atencion = parseInt(request.getParameter("atencion"));
        int programa = parseInt(request.getParameter("programa"));
        String duracion = request.getParameter("duracion");
        String numerocupo = request.getParameter("numerocupo1");
        String id_especialidad = request.getParameter("id_especialidad");
        String id_prestacion = request.getParameter("prestacion");
        String fecha = request.getParameter("fecha_inicio");
        String a = fecha.substring(6);
        String d = fecha.substring(0, 2);
        String m = fecha.substring(3, 5);
        /*problemas con idioma*/
        String fecha2 = m + "/" + d + "/" + a;
        //  String fecha2 = d + "/" + m + "/" + a;
        Date fecha1 = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);
        controlador_cupos cc = new controlador_cupos();
        int id = 1;
        id = id + cc.buscarultimaplanificacion();
        int idpabellon = 0;
        String fechainciopabellon = "00:00:00";
        String fechafinpabellon = "00:00:00";

        planificar p = new planificar();

        p.setPabellon(idpabellon);
        p.setHorainiciopabellon(fechainciopabellon);
        p.setHorafinpabellon(fechafinpabellon);
        p.setId_planificacion(id);
        p.setDuracion(Integer.parseInt(duracion));
        p.setEstatus(1);
        p.setFecha(fecha1);
        p.setHora_inicio(horainicio);
        p.setHora_fin(horafin);
        p.setId_programa(programa);
        p.setId_atencion(atencion);
        p.setNumero_cupo(Integer.parseInt(numerocupo));
        p.setCupos_disponibles(Integer.parseInt(numerocupo));
        p.setRut_doctor(rut);
        p.setId_planificacion(id);
        p.setId_prestacion(Integer.parseInt(id_prestacion));
        p.setId_especialidad(Integer.parseInt(id_especialidad));

        /*ojo modificar porque bloqueo por bloques*/
        boolean buscarplanificacion = cc.buscarsielprofecionaltienehoraasignada(rut, fecha1, horainicio, horafin);
        boolean buscarofertas = cc.buscarsitieneOfertasBloqueadasparaquepermitaPlanificar(rut, fecha1, horainicio, horafin);
        String men = "";
        if ((buscarofertas == false) && (buscarplanificacion == true)) {

            men = "El doctor posee planificacion a esas horas";

        } else {

            men = cc.insertarcupos(p);
            cc.guardarofertasparaBloquesdePlanificacion(p, 1, 1);
            cc.insertarsobreplanificacion(p);
            cc.guardarofertasparaBloquesdePlanificacion(p, 2, 2);
            int cuposmaximo = cc.duracionparaAgendamientoporBloque(p);
            int cuposreales = Math.round(cuposmaximo * p.getNumero_cupo());

            p.setNumero_cupo(cuposreales);
            p.setCupos_disponibles(cuposreales);
            cc.updatePlanificacionCuposydisponible(p);
            cc.updateSobreplanificacionCuposydisponible(p);

        }

        response.sendRedirect(cc.getLocallink() + "Planificar/AgendarPlanificacionporBloques.jsp?especialidad=" + id_especialidad + "&men=" + men + "&fecha_inicio=" + fecha + "&funcionario=" + rut);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
