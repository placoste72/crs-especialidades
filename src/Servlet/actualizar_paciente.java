/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_paciente;
import Modelos.paciente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Informatica
 */
public class actualizar_paciente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         String rut = request.getParameter("rutpaciento");
        String nombre = request.getParameter("nombrepaciente");
        String apellido1 = request.getParameter("apellidoppaciente");
        String apellido2 = request.getParameter("apellidompaciente");
        String genero = request.getParameter("genero");
        String fecha = request.getParameter("txt_inicial");
        String telefonoPaciente = request.getParameter("telefonoPaciente");
        String telefonoPaciente1 = request.getParameter("telefonoPaciente1");
        String emaildoctor = request.getParameter("emaildoctor");
        String direccion = request.getParameter("direccion"); 
        String comuna = request.getParameter("comuna");
        String procedencia = request.getParameter("procedencia");
      String tramo = request.getParameter("tramo");
        String provision = request.getParameter("provision");
         paciente p = new paciente();
            Date fecha1 = new Date(Integer.parseInt(fecha.substring(6,10))-1900,Integer.parseInt(fecha.substring(3,5))-1,Integer.parseInt(fecha.substring(0,2)),0,0,0);
        p.setRut(rut);
        p.setApellido_moderno(apellido2);
        p.setApellido_paterno(apellido1);
        p.setGenero(Integer.parseInt(genero));
        p.setContacto1(telefonoPaciente);
        p.setContacto2(telefonoPaciente1);
        p.setDireccion(direccion);
        p.setNombre(nombre);
        p.setFecha_nacimiento(fecha1);
        p.setEmail(emaildoctor);
        p.setEstatus(1);
        p.setId_comuna(Integer.parseInt(comuna));
        p.setProvision(Integer.parseInt(provision));
        p.setTramo(Integer.parseInt(tramo));
        p.setProcedencia(Integer.parseInt(procedencia));
        controlador_paciente cp = new controlador_paciente();
        String mes = cp.modificarPaciente(p);
        
         response.sendRedirect("vistas/Cita/ActualizarPaciente.jsp?cod="+p.getRut()+"&men="+mes);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
