/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Controlador.controlador_cupos;
import java.util.Date;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Informatica
 */
public class bloquear_planificacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession respuesta = request.getSession(true);
            ArrayList lista = new ArrayList();
            controlador_cupos cc = new controlador_cupos();
            String doctor = request.getParameter("doctor");
            String fecha = request.getParameter("doctor1");
             Date fecha1 = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);
    
            String motivo = request.getParameter("motivo");
            String trajo = request.getParameter("chk1");
            String trajo2 = request.getParameter("chk3");
            lista = cc.buscarPlanificacionPorFechayDoctor(fecha1, doctor);
            for (int i = 0; i < lista.size(); i++) {
                String str;

                str = "chk" + lista.get(i);
                if (request.getParameter(str) != null) {
                    if (request.getParameter(str).equals("checkbox")) {
                        cc.bloquearcupo(Integer.parseInt(lista.get(i).toString()), motivo);
                    }
                }
            }
            String men = "";
            response.sendRedirect(cc.getLocallink() + "Planificar/BloquearCupo.jsp?men=Cupos Bloqueados Exitosamente");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
