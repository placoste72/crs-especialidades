/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_cita;
import Controlador.controlador_paciente;
import Modelos.cita;
import Modelos.paciente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Informatica
 */
public class Confirmar_Cita extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String codigo = request.getParameter("cita");
        HttpSession respuesta = request.getSession(true);
        controlador_cita cc = new controlador_cita();
        String men = cc.confirmacionCita(Integer.parseInt(codigo));
        controlador_paciente cp = new controlador_paciente();
        cita p = cc.buscarCitaporId(Integer.parseInt(codigo));
        String telefonoPaciente = request.getParameter("telefono");
        String telefonoPaciente1 = request.getParameter("telefono1");
        String emaildoctor = request.getParameter("email");
        String direccion = request.getParameter("direccion");
        String region = request.getParameter("region");
        String provincia = request.getParameter("provincia");
        String comuna = request.getParameter("comuna");
        paciente pac = new paciente();
        pac.setRut(p.getRut_paciente());
        pac.setDireccion(direccion);
        pac.setContacto1(telefonoPaciente);
        pac.setContacto2(telefonoPaciente1);
        pac.setEmail(emaildoctor);
        pac.setId_region(Integer.parseInt(region));
        pac.setId_provincia(Integer.parseInt(provincia));
        pac.setId_comuna(Integer.parseInt(comuna));
        cp.modificardatosdepacienteporqueseingresohito(pac);
        response.sendRedirect(cc.getLocallink() + "Cita/ConfirmarCita.jsp?men=" + men + "&paciente=" + p.getRut_paciente());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
