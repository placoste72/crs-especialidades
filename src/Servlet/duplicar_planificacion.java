/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.General;
import Controlador.controlador_cupos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.planificar;
import java.util.Calendar;
import java.util.Iterator;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Informatica
 */
public class duplicar_planificacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            General g = new General();
            HttpSession respuesta = request.getSession(true);
            ArrayList lista = new ArrayList();
            ArrayList fechas = new ArrayList();
            controlador_cupos cc = new controlador_cupos();
            String doctor = request.getParameter("doctor");
            String fll = request.getParameter("fecha_inicio2");
            String mes = request.getParameter("mes");
            String anno = request.getParameter("anno");
            Calendar calIncio = Calendar.getInstance();
            calIncio.set(Integer.parseInt(anno), Integer.parseInt(mes), 1);
            int ultimo = calIncio.getActualMaximum(Calendar.DAY_OF_MONTH);
            String men = "";
            Date DD = calIncio.getTime();

            fechas = g.generaAgenda(DD, ultimo);
            int a1 = Integer.parseInt(fll.substring(6));
            int d1 = Integer.parseInt(fll.substring(0, 2));
            int m1 = Integer.parseInt(fll.substring(3, 5));
            String fb = (m1 + "/" + d1 + "/" + a1);
              Date fecha1 = new Date(Integer.parseInt(fll.substring(6, 10)) - 1900, Integer.parseInt(fll.substring(3, 5)) - 1, Integer.parseInt(fll.substring(0, 2)), 0, 0, 0);
    
                  Iterator it = fechas.iterator();
          while(it.hasNext()) {
                String alt;
                 Date d = (Date) it.next();
                alt = "chk"+d.getDate();
                 Date fr = new Date(Integer.toString(d.getMonth() + 1) + "/" + Integer.toString(d.getDate())+ "/" + Integer.toString(d.getYear() + 1900));
                if (request.getParameter(alt) != null) {
                    if (request.getParameter(alt).equals("checkbox")) {
                        String fb1 = "";
                      
                        
                        fb1 =   Integer.toString(d.getDate()) +"/"+Integer.toString(d.getMonth() + 1)+"/" + Integer.toString(d.getYear() + 1900);
                        
                          
                             lista = cc.buscarPlanificacionPorFechayDoctor(fecha1, doctor);

                        for (int i = 0; i < lista.size(); i++) {
                            String str;

                            str = "chk" + lista.get(i);
                            if (request.getParameter(str) != null) {
                                if (request.getParameter(str).equals("checkbox")) {
                                    planificar pn = new planificar();
                                    pn = cc.buscarplanificacionyarmar(Integer.parseInt(lista.get(i).toString()), fr);

                                    boolean buscarsisepuede = cc.buscarsielprofecionaltienehoraasignada(pn.getRut_doctor(), fr, pn.getHora_inicio(), pn.getHora_fin());
                                    if (buscarsisepuede == false) {
                                        cc.insertarcupos(pn);
                                        cc.guardarofertasporplanificacion(pn, 1,1);
                                        cc.insertarsobreplanificacion(pn);
                                        cc.guardarofertasporplanificacion(pn, 2,2);
                                     men="Cupos Duplicados Exitosamente fecha :"+fb1+ " hora :"+pn.getHora_inicio();
                                    }else{
                                    men="Profesional con Planificacion a esa  fecha:"+fb1+ " hora :"+pn.getHora_inicio();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            response.sendRedirect(cc.getLocallink() + "Planificar/DuplicarCupo.jsp?men="+men);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
