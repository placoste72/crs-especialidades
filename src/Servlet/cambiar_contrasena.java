/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Controlador.contralador_contrasena;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Informatica
 */
public class cambiar_contrasena extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            String primeraclave = request.getParameter("password1");
            String segundaclave = request.getParameter("password2");
            if (primeraclave.equals(segundaclave)) {
                contralador_contrasena cc = new contralador_contrasena();
                HttpSession respuesta = request.getSession(true);
                String nombreusuario = "";
                Authentication auth = SecurityContextHolder.getContext()
                        .getAuthentication();
                nombreusuario = auth.getName();
                boolean eraigual = false;
                eraigual = cc.verificarcontrasena(primeraclave, nombreusuario);
                String mensaje = "";
                if (eraigual == true) {

                    mensaje = "No puede usar la misma contraseña anterior ";
                    response.sendRedirect(cc.getLocal() + "Planificar/CambiarContrasena.jsp");
                } else {
                    cc.registrarviejacontrasena(nombreusuario);
                    cc.registrarcontrasena(primeraclave, nombreusuario);

                    mensaje = "Contraseña cambiada exitosamente";
                    response.sendRedirect(cc.getLocallink() + "Planificar/CambiarContrasena.jsp?men=" + mensaje);
                }

            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
