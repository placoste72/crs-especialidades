
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_paciente;
import Modelos.paciente;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Informatica
 */
public class ingresar_paciente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession respuesta = request.getSession(true);
        controlador_paciente cp = new controlador_paciente();
        String rut = request.getParameter("rutpaciento");
        boolean correcto = cp.validarRut(rut);
        String mes = "";
        if (correcto == true) {
            String rutcodificado = cp.FormatearRUT(rut);
            String nombre = request.getParameter("nombrepaciente");
            String apellido1 = request.getParameter("apellidoppaciente").replace("'", "");
            String apellido2 = request.getParameter("apellidompaciente").replace("'", "");
            String genero = request.getParameter("genero");
            String fecha = request.getParameter("txt_inicial");
            String telefonoPaciente = request.getParameter("telefonoPaciente");
            String telefonoPaciente1 = request.getParameter("telefonoPaciente1");
            String emaildoctor = request.getParameter("email");
            String direccion = request.getParameter("direccion").replace("'", "");
            String comuna = request.getParameter("comuna");
            String tramo = request.getParameter("tramo");
            String provision = request.getParameter("provision");
            String procedencia = request.getParameter("procedencia");
            String region = request.getParameter("region");
            String provincia = request.getParameter("provincia");
            String nombresocial= request.getParameter("nombresocial");
            String nacionalidad = request.getParameter("nacionalidad");
            paciente p = new paciente();

            Date fecha1 = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);
            p.setRut(rutcodificado);
            if (!apellido2.equals("")) {
                p.setApellido_moderno(apellido2);
            } else {
                p.setApellido_moderno("-");
            }

            p.setApellido_paterno(apellido1);
            p.setGenero(Integer.parseInt(genero));
            p.setContacto1(telefonoPaciente);
            p.setContacto2(telefonoPaciente1);
            p.setDireccion(direccion);
            p.setNombre(nombre);
            p.setFecha_nacimiento(fecha1);
            p.setEmail(emaildoctor);
            p.setEstatus(1);
            p.setId_comuna(Integer.parseInt(comuna));
            p.setProvision(Integer.parseInt(provision));
            p.setTramo(Integer.parseInt(tramo));
            p.setProcedencia(Integer.parseInt(procedencia));
            p.setId_region(Integer.parseInt(region));
            p.setId_provincia(Integer.parseInt(provincia));
            p.setNombresocial(nombresocial);
            p.setIdnacionalidad(Integer.parseInt(nacionalidad));

            boolean existe = cp.buscarpaciente(rutcodificado);
            if (existe == true) {
                try {
                    paciente tengo = cp.buscarpacienteporrut(rutcodificado);
                    p.setRut(tengo.getRut());
                } catch (ParseException ex) {
                    Logger.getLogger(ingresar_paciente.class.getName()).log(Level.SEVERE, null, ex);
                }

                mes = cp.modificartodoelPaciente(p);
            } else {
                mes = cp.guardarPaciente(p);

            }
        } else {
            mes = "Rut incorrecto!!Verifique y vuelva a intentar";
        }
        response.sendRedirect(cp.getLocallink() + "Cita/RegistrarPaciente.jsp?men=" + mes);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
