/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_rol;
import Modelos.opciones;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class ingresar_diagnostico extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int tipo = Integer.parseInt(request.getParameter("tipo"));

        controlador_rol cr = new controlador_rol();
        opciones o = new opciones();

        switch (tipo) {
            case 1:
                /*ingreso*/
                String nombre = request.getParameter("nombre");
                int ges = Integer.parseInt(request.getParameter("ges"));
                String[] opciones = request.getParameterValues("opciones2");
                o.setNombre(nombre);
                o.setStatus(ges);

                cr.Ingresar_diagnostico(o);
                /*busco el id del diagnostico*/

                int iddiagnostico = cr.buscarIdDiagnosticoporNombreYcaso(o.getNombre(), o.getStatus());
                /*ingreso los diagnostico-especiaaldiad*/
                for (int i = 0; i < opciones.length; ++i) {
                    cr.ingresarDiagnosticoEspecialdiad(Integer.parseInt(opciones[i]), iddiagnostico);
                }

                /*devolver a registro diagnostico*/
                response.sendRedirect(cr.getLocallink() + "Registros/RegistrarDiagnostico.jsp?men=Registro Exitoso!!");

                break;
            case 2:
                /*modificar*/
                int id = Integer.parseInt(request.getParameter("cod"));
                nombre = request.getParameter("nombre");
                ges = Integer.parseInt(request.getParameter("ges"));
                opciones = request.getParameterValues("opciones2");
                o.setNombre(nombre);
                o.setStatus(ges);

                /*modifico todo lo demas menos el id y el estatus*/
                o.setIdopcion(id);
                cr.ModificarDiagnostico(o);

                /*elimino todas las especialidades de ese diagnostico paara registrarlas nuevamente*/
                cr.EliminarDiagnosticoEspecialdiad(id);
                for (int i = 0; i < opciones.length; ++i) {
                    cr.ingresarDiagnosticoEspecialdiad(Integer.parseInt(opciones[i]), id);
                }
                response.sendRedirect(cr.getLocallink() + "Registros/ActualizarDiagnostico.jsp?men=Registro Exitoso!!&cod=" + id);

                break;
            case 3:
                /*eliminar */
                int idelimina = Integer.parseInt(request.getParameter("cod"));
                cr.EliminarDiagnostico(idelimina);
                /*devolver a registro diagnostico*/
                response.sendRedirect(cr.getLocallink() + "Registros/RegistrarDiagnostico.jsp?men=Eliminacion Exitoso!!");

                break;

        }

        /*regreso a la pagina */
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
