/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_cupos;
import Modelos.oferta;
import Modelos.planificar;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author a
 */
public class bloquear_oferta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        ArrayList lista = new ArrayList();
        controlador_cupos cc = new controlador_cupos();
        String doctor = request.getParameter("doctor");
        String fecha = request.getParameter("doctor1");
          Date fecha1 = new Date(Integer.parseInt(fecha.substring(6, 10)) - 1900, Integer.parseInt(fecha.substring(3, 5)) - 1, Integer.parseInt(fecha.substring(0, 2)), 0, 0, 0);
      
        String motivo = request.getParameter("motivo");
        int p = 0;
        int sobrecupo = 0;
        for (oferta o : cc.buscarlasOfertasdeUnDoctor(doctor, fecha1)) {
            String str;

            str = "chk" + o.getId_oferta();
            if (request.getParameter(str) != null) {
                if (request.getParameter(str).equals("checkbox")) {

                    cc.cambiarestatusdeofertaporBloqueo(o.getId_oferta(),motivo);
                    cc.buscarCitayCancelarporBloqueodeOferta(o.getId_oferta());
                    /*buscar sobrecupo de ese cupo y bloquear */
                    p = o.getId_plani_sobre();

                    /*busco el sobrecupo*/
                    sobrecupo = cc.SobrecupodeunaOferta(o.getId_plani_sobre(), o.getId_oferta());
                    cc.cambiarestatusdeofertaporBloqueo(sobrecupo,motivo);
                    cc.buscarCitayCancelarporBloqueodeOferta(sobrecupo);
                    cc.AumentarlosCuposBloqueados(o.getId_plani_sobre());
                    /* busco a ver si es necesario bloquear mas */
                    planificar puedo = cc.buscarSobreCuposporIdparaconsulta(sobrecupo);
                    if (puedo.getCupos_bloqueados() < puedo.getNumero_cupo()) {
                        cc.AumentarlosSobreCuposBloqueados(o.getId_plani_sobre());
                    }
                    /**/
                    cc.disminuirCuposDoctor(p);
                    if (puedo.getCupos_disponibles() > 0) {
                        cc.disminuirSobreCuposDoctor(p);
                    }
                }
            }

        }

   

        String men = "";
        response.sendRedirect(cc.getLocallink() + "Planificar/BloqueodeOferta.jsp?men=Cupos Bloqueados Exitosamente");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
