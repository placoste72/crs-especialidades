/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Controlador.controlador_cita;
import Controlador.controlador_cupos;
import Modelos.cita;
import Modelos.planificar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Informatica
 */
public class Cancelar_Cita extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
        String codigo = request.getParameter("cod");
        String motivo = request.getParameter("motivo");
        HttpSession respuesta = request.getSession(true);
        controlador_cita cc = new controlador_cita();
        controlador_cupos cp = new controlador_cupos();
        String men = cc.cancelarCitaPaciente(Integer.parseInt(codigo), motivo);
        cita c = cc.buscarCitaporId(Integer.parseInt(codigo));
        planificar p = cp.buscarplanificacionytipo(c.getId_oferta());

        int tipo = p.getEstatus();
        if (tipo == 1) {
            cp.AumentarCuposDoctor(p.getId_planificacion());
            cp.cancelaroncambiarestatusdeoferta(c.getId_oferta());
        } else {
            cp.AumentarSobreCuposDoctor(p.getId_planificacion());
            cp.cancelaroncambiarestatusdeoferta(c.getId_oferta());
        }
          cp.enviarEmailporCancelarCita(Integer.parseInt(codigo));
        response.sendRedirect(cc.getLocallink() + "Cita/CancelarCitaComentario.jsp?men=" + men);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
