/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructura;


import static com.lowagie.text.pdf.PdfObject.STRING;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.util.Collections.*;
import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import static java.util.Collections.list;
import java.util.Iterator;
import static org.apache.poi.hssf.usermodel.HeaderFooter.date;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Placoste
 */
public class Proceso extends Conexion {

    public String[][] lineas = new String[1000][100];
    public int cantidadRegistros = 0;
    
    public void leerExcel(String archivo) throws FileNotFoundException, IOException {
        
        int valorcampo = 0;
        
        File myFile = new File(archivo);
        DecimalFormat df = new DecimalFormat("##########");
        FileInputStream fis = new FileInputStream(myFile);
        XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        Iterator<Row> rowIterator = mySheet.iterator();
        while (rowIterator.hasNext()) {
            this.cantidadRegistros++;
            valorcampo=0;
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                valorcampo++;
                String valorcelda = null;
                int valores = 0;
                Cell cell = cellIterator.next();
                
                    switch (cell.getCellType()) {
                        case  Cell.CELL_TYPE_NUMERIC:
                            if(DateUtil.isCellDateFormatted(cell)) {
                                DateFormat fecha = new SimpleDateFormat("dd/MM/YYYY");
                                valorcelda = fecha.format(cell.getDateCellValue());
                            } else {
                                valorcelda = df.format(cell.getNumericCellValue()); 
                            }    
                            break;
                        case Cell.CELL_TYPE_STRING:
                            valorcelda = cell.getStringCellValue().trim();
                            break;        
                    }
                this.lineas[this.cantidadRegistros][cell.getColumnIndex()+1] = valorcelda; 
                valorcelda="";
            }
        }
        fis.close();
    }
    
     
   

   
    public String lecturaCuposDisponibles(int atencion, int especialidad) throws SQLException, Exception {
        
        String SQL ="select o.id_oferta , to_char(o.hora,'HH24:MI:SS')  ||'-'  as hora, o.procede, (d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor, p.fecha as totalFecha, p.id_prestacion "
                    + "from agenda.planificar p inner join agenda.oferta o on p.id_planificacion = o.id_plani_sobre "
                    + "inner join agenda.doctor d on d.rut = p.rut_doctor " 
                    + "where p.fecha > CURRENT_TIMESTAMP and p.id_atencion = "+ atencion +" and p.id_especialidad = " + especialidad
                    + " and p.estatus = 1 and p.cupos_disponibles > 0 and o.estatus = 1 and o.procede = 1 " 
                    + "UNION "
                    + "select o.id_oferta , to_char(o.hora,'HH24:MI:SS') ||'-'|| '(sobrecupo)' as hora,o.procede, (d.nombre || ' ' || d.apellido_paterno || ' ' || d.apellido_materno) as doctor, s.fecha as totalFecha, s.id_prestacion "
                    + "from agenda.sobreplanificar s inner join agenda.oferta o "
                    + "on s.id_planificar = o.id_plani_sobre "
                    + "inner join agenda.doctor d on d.rut = s.rut_doctor " 
                    + "where s.fecha > CURRENT_TIMESTAMP and s.id_atencion = "+ atencion +" and s.id_especialidad = " + especialidad
                    + " and s.estatus = 1 and s.cupos_disponibles > 0 and o.estatus = 1 and o.procede = 2  order by procede,totalFecha, hora;";
        return SQL;
    }
    public String IngresotablaBitacoraMasivo(String usuarioSession, String archivoNombre) throws ClassNotFoundException, SQLException, UnknownHostException {
        
        String SQL ="insert into agenda.bitacora_masivo values (default,default,default,default,"
                    + "'"+usuarioSession+"'"
                    + ",'"+Proceso.localIP()+"'"
                    + ",'"+archivoNombre+"',0,0,0,'');";             
        return SQL;
        
    }
    public String validaRegistrosDetalleBitacora(int valorcodigo) throws ClassNotFoundException, SQLException {
        int x;
        int y;
        int codigodiagnostico = 0;
        String SQL=null;
        Conexion conexion = new Conexion();
        conexion.connect();
        for (x=1;x<=this.cantidadRegistros;x++) {
            String observaciones="";
            
            SQL = "insert into agenda.bitacora_detalle ("
                + "id_detalle,id_bitacora,rut,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,"
                + "direccion,email,telefono_1,telefono_2,genero,id_provincia,id_comuna,id_region,id_nacionalidad, "
                + "prevision,tramo,procedencia,id_atencion,atencion_nombre,id_especialidad,especialidad_descripcion,"
                + "id_diagnostico,diagnostico_nombre,id_patologia,patologia_nombre,"
                + "fecha,hora,detalle_estado,observaciones)"
                + " values (default,"+valorcodigo;
            if (!validarRut(this.lineas[x][1])) {
                observaciones = "EL RUN es Invalido; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][1]+"'";
            }
            if ((this.lineas[x][2] == null)) {
                observaciones = observaciones + "No viene el Nombre; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][2]+"'";
            }
            if ((this.lineas[x][3] == null)) {
                observaciones = observaciones + "No viene el Apellido Paterno; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][3]+"'";
            }
            if ((this.lineas[x][4] == null)) {
                observaciones = observaciones + "No viene el Apellido Materno; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][4]+"'";
            }
            if ((this.lineas[x][5] == null)) {
                observaciones = observaciones + "No viene la Fecha de nacimiento; ";
                SQL = SQL + ",'01/01/2000'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][5]+"'";
            }
            if ((this.lineas[x][6] == null)) {
                observaciones = observaciones + "No viene la Dirección; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][6]+"'";
            }
            if ((this.lineas[x][7] == null) || (this.lineas[x][7].equals(""))) {
                observaciones = observaciones + "No viene el EMail; ";
                SQL = SQL + ",'No Tiene Email'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][7]+"'";
            }
            if ((this.lineas[x][8] == null)) {
                observaciones = observaciones + "No viene el Número 1; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                if (this.lineas[x][8].length()>8) {
                    SQL = SQL + ",'"+this.lineas[x][8]+"'";
                } else {
                    SQL = SQL + ",'Falta Información'";
                    observaciones = observaciones + "No Tiene un Formato Correcto el Telefono 1; ";
                }
            }
            if ((this.lineas[x][9] == null)) {
                observaciones = observaciones + "No viene el Número 2; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                if (this.lineas[x][9].length()>8) {
                    SQL = SQL + ",'"+this.lineas[x][9]+"'";
                } else {
                    SQL = SQL + ",'Falta Información'";
                    observaciones = observaciones + "No Tiene un Formato Correcto el Telefono 2; ";
                }
                
            }
            if ((this.lineas[x][10] == null)) {
                observaciones = observaciones + "No viene Género; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][10];
            }
            if ((this.lineas[x][11] == null)) {
                observaciones = observaciones + "No viene la Provincia; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][11];
            }
            if ((this.lineas[x][12] == null)) {
                observaciones = observaciones + "No viene la Comuna; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][12];
            }
            if ((this.lineas[x][13] == null)) {
                observaciones = observaciones + "No viene la Región; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][13];
            }
            if ((this.lineas[x][14] == null)) {
                observaciones = observaciones + "No viene la Nacionalidad; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][14];
            }
            if ((this.lineas[x][15] == null)) {
                observaciones = observaciones + "No viene la Previsión; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][15];
            }
            if ((this.lineas[x][16] == null)) {
                observaciones = observaciones + "No viene el Tramo; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][16];
            }
            if ((this.lineas[x][17] == null)) {
                observaciones = observaciones + "No viene el Procedencia; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][17];
            }
            if ((this.lineas[x][18] == null)) {
                observaciones = observaciones + "No viene el Código Atención; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][18];
            }
            if ((this.lineas[x][19] == null)) {
                observaciones = observaciones + "No viene la Descripción Atención; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][19]+"'";
            }
            if ((this.lineas[x][20] == null)) {
                observaciones = observaciones + "No viene la Código de la Especialidad; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][20];
            }
            if ((this.lineas[x][21] == null)) {
                observaciones = observaciones + "No viene la descripción de la Especialidad; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ",'"+this.lineas[x][21]+"'";
            }
            
            if ((this.lineas[x][22] == null)) {
                observaciones = observaciones + "No viene el Código del Diagnóstico; ";
                SQL = SQL + ",0";
            } else {
                SQL = SQL + ","+this.lineas[x][22];
                codigodiagnostico = Integer.parseInt(this.lineas[x][22]);
            }
            if ((this.lineas[x][23] == null)) {
                observaciones = observaciones + "No viene la Descripción Diagnóstico; ";
                SQL = SQL + ",'No Tiene'";
            } else {
                SQL = SQL + ",'"+this.lineas[x][23]+"'";
            }
            
            if (!esGES(codigodiagnostico)) {
                if ((this.lineas[x][24] == null)) {
                    SQL = SQL + ",0";
                } else {
                    SQL = SQL + ","+this.lineas[x][24];
                }
                if ((this.lineas[x][25] == null)) {
                    SQL = SQL + ",''";
                } else {
                    SQL = SQL + ",'"+this.lineas[x][25]+"'";
                }
            } else {
                SQL = SQL + ",0,''";
            }
            SQL = SQL + ",default,default,1,'"+observaciones+"'); ";
            int valor = conexion.updateSQL(SQL);
        }
        return SQL;
    }
     
    public static int Buscardiagnosticoespecialidad(int codigodiagnostico,int codigoespecialidad) throws ClassNotFoundException, SQLException {
        int salida = 0;
        Conexion conexion = new Conexion();
        conexion.connect();
        String SQL = "select iddiagnodtivo_especialidad from agenda.diagnostico_especialidad "
                + "where iddiagnostico="+codigodiagnostico+" and idespecialidad="+codigodiagnostico;
        ResultSet resultado = conexion.execSQL(SQL);
        if (resultado.next()) {
            salida = resultado.getInt("iddiagnodtivo_especialidad");
        }
        return salida;
    }
    
    
    public static boolean esGES(int codigodiagnostico) throws ClassNotFoundException, SQLException {
        int salida = 0;
        Conexion conexion = new Conexion();
        conexion.connect();
        String SQL = "select caso from agenda.diagnosticos "
                + "where iddiagnostico="+codigodiagnostico;
        ResultSet resultado = conexion.execSQL(SQL);
        if (resultado.next()) {
            salida = resultado.getInt("caso");
        }
        if (salida==2) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean buscarPaciente (String rut) throws ClassNotFoundException, SQLException {
        boolean salida = false;
        Conexion conexion = new Conexion();
        conexion.connect();
        String SQL = "select 1 from agenda.paciente "
                + "where rut='"+rut+"'";
        ResultSet resultado = conexion.execSQL(SQL);
        if (resultado.next()) {
            salida = true;
        }
        return salida;
    }
    
    public static void CrearPaciente(int codigo) throws ClassNotFoundException, SQLException, UnknownHostException {
        String SQL=null;
        Conexion conexion = new Conexion();
        conexion.connect();
        SQL = "select * from agenda.bitacora_detalle where id_detalle="+codigo;
        ResultSet resultado = conexion.execSQL(SQL);
        if (resultado.next()) {
            SQL = "Insert into agenda.paciente values ("
                + "'"+resultado.getString("rut")+"',"
                + "'"+resultado.getString("nombre")+"',"
                + "'"+resultado.getString("apellido_paterno")+"',"
                + "'"+resultado.getString("apellido_materno")+"',"
                + "'"+resultado.getDate("fecha_nacimiento")+"',"
                + "'"+resultado.getString("Direccion")+"',"
                + "'"+resultado.getString("email")+"',"
                + "'"+resultado.getString("telefono_1")+"',"        
                + "'"+resultado.getString("telefono_2")+"',"
                + "1,'"+resultado.getString("genero")+"',"
                + "CURRENT_DATE,"
                + resultado.getInt("id_comuna")+","
                + resultado.getInt("prevision")+","    
                + resultado.getInt("tramo")+","
                + resultado.getInt("procedencia")+","
                + "'"+Proceso.localIP()+"',"
                + "'JFernandez'"    
                + resultado.getInt("id_region")+","
                + resultado.getInt("id_provincia")+","
                + "1,''";      
            int valor = conexion.updateSQL(SQL);
        }
        
    }
    
    public static String localIP() throws UnknownHostException {
        String dire = "";
        String ip;
        dire = InetAddress.getLocalHost().getHostAddress();
        String nomb = "";
        nomb = InetAddress.getLocalHost().getHostName();
        ip = dire + "-" + nomb;
        return ip;
    }
    
    public void CrearCita(int oferta, int codigo) throws ClassNotFoundException, SQLException, UnknownHostException {
        
        String SQL=null;
        Conexion conexion = new Conexion();
        conexion.connect();
        SQL = "select * from agenda.bitacora_detalle where id_detalle="+codigo;
        ResultSet resultado = conexion.execSQL(SQL);
        if (resultado.next()) {
            String valorRut = resultado.getString("rut");
            int valorDiagnostico = resultado.getInt("id_diagnostico");
            int valorEspecialidad = resultado.getInt("id_especialidad");
            boolean valorsalida = Proceso.esGES(valorDiagnostico);
            int valorPatologia = 0;
            int valorGES;
            if (valorsalida) {
                valorGES=2;
                valorPatologia = resultado.getInt("id_patologia");
            } else { 
                valorGES=1;
                valorPatologia = 0;
            }    
            int valorEstatus = 1;
            String valorMotivo="NULL";
            String valorMotivo_cancelada="Motivo";
            String valorUsuario_IP = Proceso.localIP();
            String valorusuario = "jFernandez";
            int valorDiagnosticoEspecialidad= Proceso.Buscardiagnosticoespecialidad(valorDiagnostico,valorEspecialidad);
            int valorExamenes = 0;
            int valorProcedimiento = 0;
            SQL = "Insert into agenda.cita "
            + "(id_oferta, id_tipoatencion, ges,id_patologia, rut_paciente,estatus,confirmacion_cita, motivo, motivo_cancela, fecha ,ipusuario, usuario,iddiagnostico_especialidad, id_examenesespecialidad, id_procedimientoespecialidad) "
            + "VALUES (" + oferta + ", 1, " + valorGES + "," + valorPatologia + ",'" + valorRut + "', " + valorEstatus + ", 0, '" + valorMotivo + "','motivo',  current_date, '" + valorUsuario_IP + "', '" + valorusuario + "' , " + valorDiagnosticoEspecialidad + " , " + valorExamenes + "," + valorProcedimiento + ")";     
            int valor = conexion.updateSQL(SQL);
            //SQL = (" INSERT INTO \n");
            //    + "  agenda.cita\n"
            //    + "( id_oferta, id_tipoatencion, ges,id_patologia, rut_paciente,estatus,confirmacion_cita, motivo, motivo_cancela, fecha ,ipusuario, usuario,iddiagnostico_especialidad, id_examenesespecialidad, id_procedimientoespecialidad) \n"
            //    + "VALUES (" + oferta + ", 1, " + c.getGes() + "," + c.getId_patologia() + ",'" + c.getRut_paciente() + "', " + c.getEstatus() + ", 0, '" + c.getMotivo() + "','motivo',  current_date, '" + getIpUsuarioLoggin() + "', '" + getUsuarioLoggin() + "' , " + c.getIddiagnosticoespecialdad() + " , " + c.getExamen() + "," + c.getProcedimiento() + ");");             
        }
    
    }
    
    public static boolean validarRut(String rut) {
        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
            } catch (Exception e) {
        }
        return validacion;
    }
    

}    
    
    
    