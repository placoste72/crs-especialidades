/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructura;

import java.sql.*;  
import java.io.*;  

public class Conexion {

  String dbURL = "jdbc:postgresql://localhost:5432/crs";
  String dbDriver = "org.postgresql.Driver"; 
  private Connection dbCon;

  public Conexion(){  
   super();    
   }

  public boolean connect() throws ClassNotFoundException,SQLException{ 
  Class.forName(dbDriver); 
  dbCon = DriverManager.getConnection(dbURL,"postgres","siigsa2019"); 
    return true; 
  }

 

  public void close() throws SQLException{ 
  dbCon.close(); 
   }

  public ResultSet execSQL(String sql) throws SQLException{

  Statement s = dbCon.createStatement(); 
  ResultSet r = s.executeQuery(sql); 
  return (r == null) ? null : r; 
  }

  public int updateSQL(String sql) throws SQLException{   
     Statement s = dbCon.createStatement();
   int r = s.executeUpdate(sql);
   return (r == 0) ? 0 : r; 
  }

} 