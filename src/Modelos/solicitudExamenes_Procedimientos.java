/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class solicitudExamenes_Procedimientos {
    
    private int idatencionoftalmologia;
    private int idsolicitud;
    private int losolicitado;
    private String observacion;
    private int tipo;
    private int estatus;
    private String rutpaciente;
    private String nombrepaciente;
    private String rutprofesional;
    private String nombreprofesional;
    private String fecha;
    private String variable1;
    private int idexamen;
    private int idlateralidad;
    private int idtiempo;
    private String variable2;
    private String variable3;

    public int getLosolicitado() {
        return losolicitado;
    }

    public void setLosolicitado(int losolicitado) {
        this.losolicitado = losolicitado;
    }

    public int getIdexamen() {
        return idexamen;
    }

    public void setIdexamen(int idexamen) {
        this.idexamen = idexamen;
    }

    public int getIdlateralidad() {
        return idlateralidad;
    }

    public void setIdlateralidad(int idlateralidad) {
        this.idlateralidad = idlateralidad;
    }

    public int getIdtiempo() {
        return idtiempo;
    }

    public void setIdtiempo(int idtiempo) {
        this.idtiempo = idtiempo;
    }

    public String getVariable2() {
        return variable2;
    }

    public void setVariable2(String variable2) {
        this.variable2 = variable2;
    }

    public String getVariable3() {
        return variable3;
    }

    public void setVariable3(String variable3) {
        this.variable3 = variable3;
    }
    
    
    

    public String getVariable1() {
        return variable1;
    }

    public void setVariable1(String variable1) {
        this.variable1 = variable1;
    }

    
    
    
    
    public int getIdatencionoftalmologia() {
        return idatencionoftalmologia;
    }

    public void setIdatencionoftalmologia(int idatencionoftalmologia) {
        this.idatencionoftalmologia = idatencionoftalmologia;
    }

    public int getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(int idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getRutpaciente() {
        return rutpaciente;
    }

    public void setRutpaciente(String rutpaciente) {
        this.rutpaciente = rutpaciente;
    }

    public String getNombrepaciente() {
        return nombrepaciente;
    }

    public void setNombrepaciente(String nombrepaciente) {
        this.nombrepaciente = nombrepaciente;
    }

    public String getRutprofesional() {
        return rutprofesional;
    }

    public void setRutprofesional(String rutprofesional) {
        this.rutprofesional = rutprofesional;
    }

    public String getNombreprofesional() {
        return nombreprofesional;
    }

    public void setNombreprofesional(String nombreprofesional) {
        this.nombreprofesional = nombreprofesional;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
    
    
    
}
