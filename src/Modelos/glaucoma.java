/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class glaucoma {

    private Integer id_glaucoma;
    private Integer id_atencionoftalmologo;
    private Integer latanoprost;
    private Integer latanoprost_6meses;
    private Integer latonoprost_3meses;
    private Integer latanoprost_1mes;
    private Integer latanoprost_124;
    private Integer latanoprost_112;
    private Integer latanoprost_18;
    private Integer betaxolol;
    private Integer betaxolol_6meses;
    private Integer betaxolol_3meses;
    private Integer betaxolol_1mes;
    private Integer betaxolol_124;
    private Integer betaxolol_112;
    private Integer betaxolol_18;
    private Integer timolol;
    private Integer timolol_6meses;
    private Integer timolol_3meses;
    private Integer timolol_1mes;
    private Integer timolol_124;
    private Integer timolol_112;
    private Integer timolol_18;
    private Integer dorzolamida;
    private Integer dorzolamida_6meses;
    private Integer dorzolamida_3meses;
    private Integer dorzolamida_1mes;
    private Integer dorzolamida_124;
    private Integer dorzolamida_112;
    private Integer dorzolamida_18;
    private String observaciones;

    public Integer getId_glaucoma() {
        return id_glaucoma;
    }

    public void setId_glaucoma(Integer id_glaucoma) {
        this.id_glaucoma = id_glaucoma;
    }

    public Integer getId_atencionoftalmologo() {
        return id_atencionoftalmologo;
    }

    public void setId_atencionoftalmologo(Integer id_atencionoftalmologo) {
        this.id_atencionoftalmologo = id_atencionoftalmologo;
    }

    public Integer getLatanoprost() {
        return latanoprost;
    }

    public void setLatanoprost(Integer latanoprost) {
        this.latanoprost = latanoprost;
    }

    public Integer getLatanoprost_6meses() {
        return latanoprost_6meses;
    }

    public void setLatanoprost_6meses(Integer latanoprost_6meses) {
        this.latanoprost_6meses = latanoprost_6meses;
    }

    public Integer getLatonoprost_3meses() {
        return latonoprost_3meses;
    }

    public void setLatonoprost_3meses(Integer latonoprost_3meses) {
        this.latonoprost_3meses = latonoprost_3meses;
    }

    public Integer getLatanoprost_1mes() {
        return latanoprost_1mes;
    }

    public void setLatanoprost_1mes(Integer latanoprost_1mes) {
        this.latanoprost_1mes = latanoprost_1mes;
    }

    public Integer getLatanoprost_124() {
        return latanoprost_124;
    }

    public void setLatanoprost_124(Integer latanoprost_124) {
        this.latanoprost_124 = latanoprost_124;
    }

    public Integer getLatanoprost_112() {
        return latanoprost_112;
    }

    public void setLatanoprost_112(Integer latanoprost_112) {
        this.latanoprost_112 = latanoprost_112;
    }

    public Integer getLatanoprost_18() {
        return latanoprost_18;
    }

    public void setLatanoprost_18(Integer latanoprost_18) {
        this.latanoprost_18 = latanoprost_18;
    }

    public Integer getBetaxolol() {
        return betaxolol;
    }

    public void setBetaxolol(Integer betaxolol) {
        this.betaxolol = betaxolol;
    }

    public Integer getBetaxolol_6meses() {
        return betaxolol_6meses;
    }

    public void setBetaxolol_6meses(Integer betaxolol_6meses) {
        this.betaxolol_6meses = betaxolol_6meses;
    }

    public Integer getBetaxolol_3meses() {
        return betaxolol_3meses;
    }

    public void setBetaxolol_3meses(Integer betaxolol_3meses) {
        this.betaxolol_3meses = betaxolol_3meses;
    }

    public Integer getBetaxolol_1mes() {
        return betaxolol_1mes;
    }

    public void setBetaxolol_1mes(Integer betaxolol_1mes) {
        this.betaxolol_1mes = betaxolol_1mes;
    }

    public Integer getBetaxolol_124() {
        return betaxolol_124;
    }

    public void setBetaxolol_124(Integer betaxolol_124) {
        this.betaxolol_124 = betaxolol_124;
    }

    public Integer getBetaxolol_112() {
        return betaxolol_112;
    }

    public void setBetaxolol_112(Integer betaxolol_112) {
        this.betaxolol_112 = betaxolol_112;
    }

    public Integer getBetaxolol_18() {
        return betaxolol_18;
    }

    public void setBetaxolol_18(Integer betaxolol_18) {
        this.betaxolol_18 = betaxolol_18;
    }

    public Integer getTimolol() {
        return timolol;
    }

    public void setTimolol(Integer timolol) {
        this.timolol = timolol;
    }

    public Integer getTimolol_6meses() {
        return timolol_6meses;
    }

    public void setTimolol_6meses(Integer timolol_6meses) {
        this.timolol_6meses = timolol_6meses;
    }

    public Integer getTimolol_3meses() {
        return timolol_3meses;
    }

    public void setTimolol_3meses(Integer timolol_3meses) {
        this.timolol_3meses = timolol_3meses;
    }

    public Integer getTimolol_1mes() {
        return timolol_1mes;
    }

    public void setTimolol_1mes(Integer timolol_1mes) {
        this.timolol_1mes = timolol_1mes;
    }

    public Integer getTimolol_124() {
        return timolol_124;
    }

    public void setTimolol_124(Integer timolol_124) {
        this.timolol_124 = timolol_124;
    }

    public Integer getTimolol_112() {
        return timolol_112;
    }

    public void setTimolol_112(Integer timolol_112) {
        this.timolol_112 = timolol_112;
    }

    public Integer getTimolol_18() {
        return timolol_18;
    }

    public void setTimolol_18(Integer timolol_18) {
        this.timolol_18 = timolol_18;
    }

    public Integer getDorzolamida() {
        return dorzolamida;
    }

    public void setDorzolamida(Integer dorzolamida) {
        this.dorzolamida = dorzolamida;
    }

    public Integer getDorzolamida_6meses() {
        return dorzolamida_6meses;
    }

    public void setDorzolamida_6meses(Integer dorzolamida_6meses) {
        this.dorzolamida_6meses = dorzolamida_6meses;
    }

    public Integer getDorzolamida_3meses() {
        return dorzolamida_3meses;
    }

    public void setDorzolamida_3meses(Integer dorzolamida_3meses) {
        this.dorzolamida_3meses = dorzolamida_3meses;
    }

    public Integer getDorzolamida_1mes() {
        return dorzolamida_1mes;
    }

    public void setDorzolamida_1mes(Integer dorzolamida_1mes) {
        this.dorzolamida_1mes = dorzolamida_1mes;
    }

    public Integer getDorzolamida_124() {
        return dorzolamida_124;
    }

    public void setDorzolamida_124(Integer dorzolamida_124) {
        this.dorzolamida_124 = dorzolamida_124;
    }

    public Integer getDorzolamida_112() {
        return dorzolamida_112;
    }

    public void setDorzolamida_112(Integer dorzolamida_112) {
        this.dorzolamida_112 = dorzolamida_112;
    }

    public Integer getDorzolamida_18() {
        return dorzolamida_18;
    }

    public void setDorzolamida_18(Integer dorzolamida_18) {
        this.dorzolamida_18 = dorzolamida_18;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    

}
