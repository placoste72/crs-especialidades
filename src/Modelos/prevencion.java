/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class prevencion {
    
 private int  id_sesion_prevencion;
private int id_prevencion;
 private boolean  tecnica_usoseda;
 private boolean destartraje_pulidocorona;
 private boolean fluoracion;
 private boolean aplicacion_sellante;
 private boolean usosustancia_reveladora;
 private boolean aplicacion_topicofluoruro;
 private String tu;
 private String dpc;
 private String f;
 private String as;
 private String usr;
 private String atf;
 private String doctor;
 private Date fecha;
 
 
 

    public prevencion() {
    }

    public String getTu() {
        return tu;
    }

    public void setTu(String tu) {
        this.tu = tu;
    }

    public String getDpc() {
        return dpc;
    }

    public void setDpc(String dpc) {
        this.dpc = dpc;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getAs() {
        return as;
    }

    public void setAs(String as) {
        this.as = as;
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getAtf() {
        return atf;
    }

    public void setAtf(String atf) {
        this.atf = atf;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
   
     
    public int getId_prevencion() {
        return id_prevencion;
    }

    public void setId_prevencion(int id_prevencion) {
        this.id_prevencion = id_prevencion;
    }
 
 
    public int getId_sesion_prevencion() {
        return id_sesion_prevencion;
    }

    public void setId_sesion_prevencion(int id_sesion_prevencion) {
        this.id_sesion_prevencion = id_sesion_prevencion;
    }

   
    public boolean isTecnica_usoseda() {
        return tecnica_usoseda;
    }

    public void setTecnica_usoseda(boolean tecnica_usoseda) {
        this.tecnica_usoseda = tecnica_usoseda;
    }

    public boolean isDestartraje_pulidocorona() {
        return destartraje_pulidocorona;
    }

    public void setDestartraje_pulidocorona(boolean destartraje_pulidocorona) {
        this.destartraje_pulidocorona = destartraje_pulidocorona;
    }

    public boolean isFluoracion() {
        return fluoracion;
    }

    public void setFluoracion(boolean fluoracion) {
        this.fluoracion = fluoracion;
    }

    public boolean isAplicacion_sellante() {
        return aplicacion_sellante;
    }

    public void setAplicacion_sellante(boolean aplicacion_sellante) {
        this.aplicacion_sellante = aplicacion_sellante;
    }

    public boolean isUsosustancia_reveladora() {
        return usosustancia_reveladora;
    }

    public void setUsosustancia_reveladora(boolean usosustancia_reveladora) {
        this.usosustancia_reveladora = usosustancia_reveladora;
    }

    public boolean isAplicacion_topicofluoruro() {
        return aplicacion_topicofluoruro;
    }

    public void setAplicacion_topicofluoruro(boolean aplicacion_topicofluoruro) {
        this.aplicacion_topicofluoruro = aplicacion_topicofluoruro;
    }
 
 
    
}
