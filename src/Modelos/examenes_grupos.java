/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class examenes_grupos {
    
    private int id_examengrupo;
    private String descripciongrupo;
    private String descripcionexamen;
    private String colortuboexamen;
    private int prioridad;
    private int orden;
    private int idgrupo;
    private int idcita;
    private String observacion;

    public int getId_examengrupo() {
        return id_examengrupo;
    }

    public int getIdcita() {
        return idcita;
    }

    public void setIdcita(int idcita) {
        this.idcita = idcita;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    

    public void setId_examengrupo(int id_examengrupo) {
        this.id_examengrupo = id_examengrupo;
    }

    public String getDescripciongrupo() {
        return descripciongrupo;
    }

    public void setDescripciongrupo(String descripciongrupo) {
        this.descripciongrupo = descripciongrupo;
    }

    public String getDescripcionexamen() {
        return descripcionexamen;
    }

    public void setDescripcionexamen(String descripcionexamen) {
        this.descripcionexamen = descripcionexamen;
    }

    public String getColortuboexamen() {
        return colortuboexamen;
    }

    public void setColortuboexamen(String colortuboexamen) {
        this.colortuboexamen = colortuboexamen;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public int getIdgrupo() {
        return idgrupo;
    }

    public void setIdgrupo(int idgrupo) {
        this.idgrupo = idgrupo;
    }
    
    
    
}
