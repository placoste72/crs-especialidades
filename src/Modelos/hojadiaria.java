/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class hojadiaria {

    private int idhojadiaria;
    private int iddestinopaciente;
    private String otrodestino_paciente;
    private int sic;
    private int ges;
    private String otro_diagnostico;
    private String otro_procedimiento;
    private String otro_examen;
    private String otras_indicaciones;
    private Date fecha_registro;
    private int idcita;
    private String auxiliar1;
    private String auxiliar2;
    private String auxiliar3;
    private String auxiliar4;
    private String auxiliar5;
    private String auxiliar6;
    private String auxiliar7;
    private boolean solicitudpabellon;
    private String paraquesederive;
    private int diagnosticosic;
    private String diagnosticootrosic;
    private int constancia;
    private String otrodiagnosticosolicitud;
    private String fundamentocataratas;
    private String tratamientocataratas;
    private int pertinencia;

    public String getAuxiliar7() {
        return auxiliar7;
    }

    public String getFundamentocataratas() {
        return fundamentocataratas;
    }

    public int getPertinencia() {
        return pertinencia;
    }

    public void setPertinencia(int pertinencia) {
        this.pertinencia = pertinencia;
    }
    
    

    public void setFundamentocataratas(String fundamentocataratas) {
        this.fundamentocataratas = fundamentocataratas;
    }

    public String getTratamientocataratas() {
        return tratamientocataratas;
    }

    public void setTratamientocataratas(String tratamientocataratas) {
        this.tratamientocataratas = tratamientocataratas;
    }
    
    

    public void setAuxiliar7(String auxiliar7) {
        this.auxiliar7 = auxiliar7;
    }

    
    
    public int getIdcita() {
        return idcita;
    }

    public String getAuxiliar5() {
        return auxiliar5;
    }

    public void setAuxiliar5(String auxiliar5) {
        this.auxiliar5 = auxiliar5;
    }

    public String getAuxiliar6() {
        return auxiliar6;
    }

    public void setAuxiliar6(String auxiliar6) {
        this.auxiliar6 = auxiliar6;
    }
    
    

    public int getDiagnosticosic() {
        return diagnosticosic;
    }

    public void setDiagnosticosic(int diagnosticosic) {
        this.diagnosticosic = diagnosticosic;
    }

    public String getDiagnosticootrosic() {
        return diagnosticootrosic;
    }

    public void setDiagnosticootrosic(String diagnosticootrosic) {
        this.diagnosticootrosic = diagnosticootrosic;
    }

    public int getConstancia() {
        return constancia;
    }

    public void setConstancia(int constancia) {
        this.constancia = constancia;
    }

    public String getOtrodiagnosticosolicitud() {
        return otrodiagnosticosolicitud;
    }

    public void setOtrodiagnosticosolicitud(String otrodiagnosticosolicitud) {
        this.otrodiagnosticosolicitud = otrodiagnosticosolicitud;
    }
    
    

    public boolean isSolicitudpabellon() {
        return solicitudpabellon;
    }

    public void setSolicitudpabellon(boolean solicitudpabellon) {
        this.solicitudpabellon = solicitudpabellon;
    }

    public String getParaquesederive() {
        return paraquesederive;
    }

    public void setParaquesederive(String paraquesederive) {
        this.paraquesederive = paraquesederive;
    }

    
    
    public String getAuxiliar1() {
        return auxiliar1;
    }

    public void setAuxiliar1(String auxiliar1) {
        this.auxiliar1 = auxiliar1;
    }

    public String getAuxiliar2() {
        return auxiliar2;
    }

    public void setAuxiliar2(String auxiliar2) {
        this.auxiliar2 = auxiliar2;
    }

    public String getAuxiliar3() {
        return auxiliar3;
    }

    public void setAuxiliar3(String auxiliar3) {
        this.auxiliar3 = auxiliar3;
    }

    public String getAuxiliar4() {
        return auxiliar4;
    }

    public void setAuxiliar4(String auxiliar4) {
        this.auxiliar4 = auxiliar4;
    }

    public void setIdcita(int idcita) {
        this.idcita = idcita;
    }

    public int getIdhojadiaria() {
        return idhojadiaria;
    }

    public void setIdhojadiaria(int idhojadiaria) {
        this.idhojadiaria = idhojadiaria;
    }

    public int getIddestinopaciente() {
        return iddestinopaciente;
    }

    public void setIddestinopaciente(int iddestinopaciente) {
        this.iddestinopaciente = iddestinopaciente;
    }

    public String getOtrodestino_paciente() {
        return otrodestino_paciente;
    }

    public void setOtrodestino_paciente(String otrodestino_paciente) {
        this.otrodestino_paciente = otrodestino_paciente;
    }

    public int getSic() {
        return sic;
    }

    public void setSic(int sic) {
        this.sic = sic;
    }

    public int getGes() {
        return ges;
    }

    public void setGes(int ges) {
        this.ges = ges;
    }

    public String getOtro_diagnostico() {
        return otro_diagnostico;
    }

    public void setOtro_diagnostico(String otro_diagnostico) {
        this.otro_diagnostico = otro_diagnostico;
    }

    public String getOtro_procedimiento() {
        return otro_procedimiento;
    }

    public void setOtro_procedimiento(String otro_procedimiento) {
        this.otro_procedimiento = otro_procedimiento;
    }

    public String getOtro_examen() {
        return otro_examen;
    }

    public void setOtro_examen(String otro_examen) {
        this.otro_examen = otro_examen;
    }

    public String getOtras_indicaciones() {
        return otras_indicaciones;
    }

    public void setOtras_indicaciones(String otras_indicaciones) {
        this.otras_indicaciones = otras_indicaciones;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

}
