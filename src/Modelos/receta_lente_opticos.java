/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class receta_lente_opticos {

    private Integer id_receta;
    private Integer id_atencion;
  
   private String ojoderechoesfera;
   private String ojoderechocilindro;
   private String ojoderechoeje;
   private String ojoizquierdoesfera;
   private String ojoiaquierdacilindro;
   private String ojoizquierdaeje;
   private String aadd;
   private String distanciapupilarlejos;
   private String distanciapupilarcerca;
 
    private String observaciones;
    private int tipo;

    public Integer getId_receta() {
        return id_receta;
    }

    public void setId_receta(Integer id_receta) {
        this.id_receta = id_receta;
    }

    public Integer getId_atencion() {
        return id_atencion;
    }

    public void setId_atencion(Integer id_atencion) {
        this.id_atencion = id_atencion;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getOjoderechoesfera() {
        return ojoderechoesfera;
    }

    public void setOjoderechoesfera(String ojoderechoesfera) {
        this.ojoderechoesfera = ojoderechoesfera;
    }

    public String getOjoderechocilindro() {
        return ojoderechocilindro;
    }

    public void setOjoderechocilindro(String ojoderechocilindro) {
        this.ojoderechocilindro = ojoderechocilindro;
    }

    public String getOjoderechoeje() {
        return ojoderechoeje;
    }

    public void setOjoderechoeje(String ojoderechoeje) {
        this.ojoderechoeje = ojoderechoeje;
    }

    public String getOjoizquierdoesfera() {
        return ojoizquierdoesfera;
    }

    public void setOjoizquierdoesfera(String ojoizquierdoesfera) {
        this.ojoizquierdoesfera = ojoizquierdoesfera;
    }

    public String getOjoiaquierdacilindro() {
        return ojoiaquierdacilindro;
    }

    public void setOjoiaquierdacilindro(String ojoiaquierdacilindro) {
        this.ojoiaquierdacilindro = ojoiaquierdacilindro;
    }

    public String getOjoizquierdaeje() {
        return ojoizquierdaeje;
    }

    public void setOjoizquierdaeje(String ojoizquierdaeje) {
        this.ojoizquierdaeje = ojoizquierdaeje;
    }

    public String getAadd() {
        return aadd;
    }

    public void setAadd(String aadd) {
        this.aadd = aadd;
    }

    public String getDistanciapupilarlejos() {
        return distanciapupilarlejos;
    }

    public void setDistanciapupilarlejos(String distanciapupilarlejos) {
        this.distanciapupilarlejos = distanciapupilarlejos;
    }

    public String getDistanciapupilarcerca() {
        return distanciapupilarcerca;
    }

    public void setDistanciapupilarcerca(String distanciapupilarcerca) {
        this.distanciapupilarcerca = distanciapupilarcerca;
    }

 

   

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    
    
}
