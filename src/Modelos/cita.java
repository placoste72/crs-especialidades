/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author Informatica
 */
public class cita {

    private int id_cita;

    private String rut_doctor;
    private Date otrafecha;

    private int id_tipoatencion;
    private int ges;
    private int id_patologia;
    private int id_oferta;
    private String rut_paciente;
    private int estatus;
    private int confirmacion_cita;
    private String motivo;
    private String motivo_cancela;
    private Date fecha;
    private int iddiagnosticoespecialdad;
    private int examen;
    private int procedimiento;
  
    private String temporales;
    private String temporales1;
    private String temporales2;
    private String temporales3;
    private String temporales4;
    private int id_motivo;
    private String temporales5;
    private String temporales6;
    private String temporales7;
    private String temporales8;
    private String temporales9;
    private String temporales10;
    private String temporales11;
    private String temporales12;
    private String temporales13;
    private String temporales14;
    private String temporales15;
    private String temporales16;
    private String temporales17;
    private String temporales18;
    private String temporales19;
    private String temporales20;
    private String temporales21;
    private String usuario_cancela;

    public cita() {
    }

    public int getExamen() {
        return examen;
    }

    public void setExamen(int examen) {
        this.examen = examen;
    }

    public int getProcedimiento() {
        return procedimiento;
    }

    public void setProcedimiento(int procedimiento) {
        this.procedimiento = procedimiento;
    }
    
    

    public Date getOtrafecha() {
        return otrafecha;
    }

    public String getTemporales17() {
        return temporales17;
    }

    public String getUsuario_cancela() {
        return usuario_cancela;
    }

    public void setUsuario_cancela(String usuario_cancela) {
        this.usuario_cancela = usuario_cancela;
    }

    public int getIddiagnosticoespecialdad() {
        return iddiagnosticoespecialdad;
    }

    public void setIddiagnosticoespecialdad(int iddiagnosticoespecialdad) {
        this.iddiagnosticoespecialdad = iddiagnosticoespecialdad;
    }

    
   
    
    

    public void setTemporales17(String temporales17) {
        this.temporales17 = temporales17;
    }

    public String getTemporales18() {
        return temporales18;
    }

    public void setTemporales18(String temporales18) {
        this.temporales18 = temporales18;
    }

    public String getTemporales19() {
        return temporales19;
    }

    public void setTemporales19(String temporales19) {
        this.temporales19 = temporales19;
    }

    public String getTemporales20() {
        return temporales20;
    }

    public void setTemporales20(String temporales20) {
        this.temporales20 = temporales20;
    }

    public String getTemporales21() {
        return temporales21;
    }

    public void setTemporales21(String temporales21) {
        this.temporales21 = temporales21;
    }
    
    

    public void setOtrafecha(Date otrafecha) {
        this.otrafecha = otrafecha;
    }

    public String getTemporales7() {
        return temporales7;
    }

    public void setTemporales7(String temporales7) {
        this.temporales7 = temporales7;
    }

    public String getTemporales8() {
        return temporales8;
    }

    public void setTemporales8(String temporales8) {
        this.temporales8 = temporales8;
    }

    public String getTemporales9() {
        return temporales9;
    }

    public void setTemporales9(String temporales9) {
        this.temporales9 = temporales9;
    }

    public String getTemporales10() {
        return temporales10;
    }

    public void setTemporales10(String temporales10) {
        this.temporales10 = temporales10;
    }

    public String getTemporales11() {
        return temporales11;
    }

    public void setTemporales11(String temporales11) {
        this.temporales11 = temporales11;
    }

    public String getTemporales12() {
        return temporales12;
    }

    public void setTemporales12(String temporales12) {
        this.temporales12 = temporales12;
    }

    public String getTemporales13() {
        return temporales13;
    }

    public void setTemporales13(String temporales13) {
        this.temporales13 = temporales13;
    }

    public String getTemporales14() {
        return temporales14;
    }

    public void setTemporales14(String temporales14) {
        this.temporales14 = temporales14;
    }

    public String getTemporales15() {
        return temporales15;
    }

    public void setTemporales15(String temporales15) {
        this.temporales15 = temporales15;
    }

    public String getTemporales16() {
        return temporales16;
    }

    public void setTemporales16(String temporales16) {
        this.temporales16 = temporales16;
    }

    public String getTemporales6() {
        return temporales6;
    }

    public void setTemporales6(String temporales6) {
        this.temporales6 = temporales6;
    }

    public String getTemporales5() {
        return temporales5;
    }

    public void setTemporales5(String temporales5) {
        this.temporales5 = temporales5;
    }

    public int getId_motivo() {
        return id_motivo;
    }

    public void setId_motivo(int id_motivo) {
        this.id_motivo = id_motivo;
    }

    public String getMotivo_cancela() {
        return motivo_cancela;
    }

    public String getTemporales4() {
        return temporales4;
    }

    public void setTemporales4(String temporales4) {
        this.temporales4 = temporales4;
    }

    public void setMotivo_cancela(String motivo_cancela) {
        this.motivo_cancela = motivo_cancela;
    }

    public int getId_oferta() {
        return id_oferta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setId_oferta(int id_oferta) {
        this.id_oferta = id_oferta;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getTemporales() {
        return temporales;
    }

    public void setTemporales(String temporales) {
        this.temporales = temporales;
    }

    public String getTemporales1() {
        return temporales1;
    }

    public void setTemporales1(String temporales1) {
        this.temporales1 = temporales1;
    }

    public String getTemporales2() {
        return temporales2;
    }

    public void setTemporales2(String temporales2) {
        this.temporales2 = temporales2;
    }

    public String getTemporales3() {
        return temporales3;
    }

    public void setTemporales3(String temporales3) {
        this.temporales3 = temporales3;
    }

    public int getConfirmacion_cita() {
        return confirmacion_cita;
    }

    public void setConfirmacion_cita(int confirmacion_cita) {
        this.confirmacion_cita = confirmacion_cita;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public int getId_cita() {
        return id_cita;
    }

    public void setId_cita(int id_cita) {
        this.id_cita = id_cita;
    }

    public String getRut_doctor() {
        return rut_doctor;
    }

    public void setRut_doctor(String rut_doctor) {
        this.rut_doctor = rut_doctor;
    }

    public int getId_tipoatencion() {
        return id_tipoatencion;
    }

    public void setId_tipoatencion(int id_tipoatencion) {
        this.id_tipoatencion = id_tipoatencion;
    }

    public int getGes() {
        return ges;
    }

    public void setGes(int ges) {
        this.ges = ges;
    }

    public int getId_patologia() {
        return id_patologia;
    }

    public void setId_patologia(int id_patologia) {
        this.id_patologia = id_patologia;
    }

    public String getRut_paciente() {
        return rut_paciente;
    }

    public void setRut_paciente(String rut_paciente) {
        this.rut_paciente = rut_paciente;
    }

}
