/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Informatica
 */
public class opciones {
    
    private int idopcion ;
    private String nombre;
    private String url;
    private int status ;

    public opciones() {
    }
    
    
    

    public opciones(int idopcion, String nombre, String url, int status) {
        this.idopcion = idopcion;
        this.nombre = nombre;
        this.url = url;
        this.status = status;
    }

    public int getIdopcion() {
        return idopcion;
    }

    public void setIdopcion(int idopcion) {
        this.idopcion = idopcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
    
    
    
}
