/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.InputStream;

/**
 *
 * @author Informatica
 */
public class especialidades {
  private int id_especialidad;
    private String nombre;
    private String nombreimagen;
    private int tamanoimagen;
    private InputStream foto;
    private int estatus;

    public especialidades(int id_especialidad, String nombre, String nombreimagen, int tamanoimagen, InputStream foto, int estatus) {
        this.id_especialidad = id_especialidad;
        this.nombre = nombre;
        this.nombreimagen = nombreimagen;
        this.tamanoimagen = tamanoimagen;
        this.foto = foto;
        this.estatus = estatus;
    }
   

   
    public especialidades() {
       
    }

    public String getNombreimagen() {
        return nombreimagen;
    }

    public void setNombreimagen(String nombreimagen) {
        this.nombreimagen = nombreimagen;
    }

    public int getTamanoimagen() {
        return tamanoimagen;
    }

    public void setTamanoimagen(int tamanoimagen) {
        this.tamanoimagen = tamanoimagen;
    }

    public InputStream getFoto() {
        return foto;
    }

    public void setFoto(InputStream foto) {
        this.foto = foto;
    }

    
    public int getId_especialidad() {
        return id_especialidad;
    }

    public void setId_especialidad(int id_especialidad) {
        this.id_especialidad = id_especialidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

   
}
