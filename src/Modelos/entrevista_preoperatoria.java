/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class entrevista_preoperatoria {

    private String diagnostico;
    private int idCita;
    private int identrevista_preoperatoria;
    private String cirugia_propuesta;
    private String pa;
    private String peso;
    private String talla;
    private String imc;
    private String cardiaca;
    private String sat;
    private int asma;
    private int ira;
    private int tuberculosis;
    private int rinitis;
    private int tabaco;
    private String desde;
    private String anos;
    private int diabetes;
    private int hipo;
    private int alergias;
    private int obesidad;
    private int ulcera;
    private int rge;
    private String cualesalergias;
    private int coronario;
    private int valvular;
    private int arritmias;
    private int marcapasos;
    private int coagulopatia;
    private int dislipidemia;
    private int ecv;
    private int esquizofrenia;
    private int depresion;
    private int epilepsia;
    private int alcohol;
    private String especificacionalcohol;
    private String otrasenfpsiq;
    private int nefropatia;
    private int infeccionurinaria;
    private int uropatiaobstructiva;
    private int intervencion;
    private int antecendetes_anestesicos;
    private String especificar;
    private String tratamieto;
    private String observaciones;
    private Date fecharegistro;
    private int estatus;
    private String otrosexamenes;
    private String auxiliar1;
    private String auxiliar2;
    private String auxiliar3;
    private String auxiliar4;
    private String auxiliar5;
    private String auxiliar6;
    private int hta;
    private String paretro;
    private String cardiacaretro;
    private String satretro;
    private int retrocontrol;
    private String reddeapoyo;
    private String descripcion_ventilatorios;
    private String descripcion_metabolicos;
    private String descripcion_cardiovasculares;
    private String descripcionneuro;
    private String descripcion_urinarios;
    private String otrosantecedentesmorbidos;
    private String descripcion_antecedentes;
    private String descripcion_examenes;
    private double ipa;
    private int pase;
    private int pertinencia;

    public int getPase() {
        return pase;
    }

    public void setPase(int pase) {
        this.pase = pase;
    }
    
    

    public String getDescripcion_ventilatorios() {
        return descripcion_ventilatorios;
    }

    public void setDescripcion_ventilatorios(String descripcion_ventilatorios) {
        this.descripcion_ventilatorios = descripcion_ventilatorios;
    }

    public String getDescripcion_metabolicos() {
        return descripcion_metabolicos;
    }

    public void setDescripcion_metabolicos(String descripcion_metabolicos) {
        this.descripcion_metabolicos = descripcion_metabolicos;
    }

    public String getDescripcion_cardiovasculares() {
        return descripcion_cardiovasculares;
    }

    public void setDescripcion_cardiovasculares(String descripcion_cardiovasculares) {
        this.descripcion_cardiovasculares = descripcion_cardiovasculares;
    }

    public String getDescripcionneuro() {
        return descripcionneuro;
    }

    public void setDescripcionneuro(String descripcionneuro) {
        this.descripcionneuro = descripcionneuro;
    }

    public String getDescripcion_urinarios() {
        return descripcion_urinarios;
    }

    public void setDescripcion_urinarios(String descripcion_urinarios) {
        this.descripcion_urinarios = descripcion_urinarios;
    }

    public String getOtrosantecedentesmorbidos() {
        return otrosantecedentesmorbidos;
    }

    public void setOtrosantecedentesmorbidos(String otrosantecedentesmorbidos) {
        this.otrosantecedentesmorbidos = otrosantecedentesmorbidos;
    }

    public String getDescripcion_antecedentes() {
        return descripcion_antecedentes;
    }

    public void setDescripcion_antecedentes(String descripcion_antecedentes) {
        this.descripcion_antecedentes = descripcion_antecedentes;
    }

    public String getDescripcion_examenes() {
        return descripcion_examenes;
    }

    public void setDescripcion_examenes(String descripcion_examenes) {
        this.descripcion_examenes = descripcion_examenes;
    }

    public double getIpa() {
        return ipa;
    }

    public void setIpa(double ipa) {
        this.ipa = ipa;
    }



    
    
    public int getPertinencia() {
        return pertinencia;
    }

    public void setPertinencia(int pertinencia) {
        this.pertinencia = pertinencia;
    }

    public String getReddeapoyo() {
        return reddeapoyo;
    }

    public void setReddeapoyo(String reddeapoyo) {
        this.reddeapoyo = reddeapoyo;
    }

    public String getParetro() {
        return paretro;
    }

    public int getRetrocontrol() {
        return retrocontrol;
    }

    public void setRetrocontrol(int retrocontrol) {
        this.retrocontrol = retrocontrol;
    }

    public String getEspecificacionalcohol() {
        return especificacionalcohol;
    }

    public void setEspecificacionalcohol(String especificacionalcohol) {
        this.especificacionalcohol = especificacionalcohol;
    }

    public void setParetro(String paretro) {
        this.paretro = paretro;
    }

    public String getCardiacaretro() {
        return cardiacaretro;
    }

    public void setCardiacaretro(String cardiacaretro) {
        this.cardiacaretro = cardiacaretro;
    }

    public String getSatretro() {
        return satretro;
    }

    public void setSatretro(String satretro) {
        this.satretro = satretro;
    }

    public int getHta() {
        return hta;
    }

    public void setHta(int hta) {
        this.hta = hta;
    }

    public String getAuxiliar1() {
        return auxiliar1;
    }

    public void setAuxiliar1(String auxiliar1) {
        this.auxiliar1 = auxiliar1;
    }

    public String getAuxiliar2() {
        return auxiliar2;
    }

    public void setAuxiliar2(String auxiliar2) {
        this.auxiliar2 = auxiliar2;
    }

    public String getAuxiliar3() {
        return auxiliar3;
    }

    public void setAuxiliar3(String auxiliar3) {
        this.auxiliar3 = auxiliar3;
    }

    public String getAuxiliar4() {
        return auxiliar4;
    }

    public void setAuxiliar4(String auxiliar4) {
        this.auxiliar4 = auxiliar4;
    }

    public String getAuxiliar5() {
        return auxiliar5;
    }

    public void setAuxiliar5(String auxiliar5) {
        this.auxiliar5 = auxiliar5;
    }

    public String getAuxiliar6() {
        return auxiliar6;
    }

    public void setAuxiliar6(String auxiliar6) {
        this.auxiliar6 = auxiliar6;
    }

    public String getOtrosexamenes() {
        return otrosexamenes;
    }

    public void setOtrosexamenes(String otrosexamenes) {
        this.otrosexamenes = otrosexamenes;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public int getIdentrevista_preoperatoria() {
        return identrevista_preoperatoria;
    }

    public void setIdentrevista_preoperatoria(int identrevista_preoperatoria) {
        this.identrevista_preoperatoria = identrevista_preoperatoria;
    }

    public String getCirugia_propuesta() {
        return cirugia_propuesta;
    }

    public void setCirugia_propuesta(String cirugia_propuesta) {
        this.cirugia_propuesta = cirugia_propuesta;
    }

    public String getPa() {
        return pa;
    }

    public void setPa(String pa) {
        this.pa = pa;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getImc() {
        return imc;
    }

    public void setImc(String imc) {
        this.imc = imc;
    }

    public String getCardiaca() {
        return cardiaca;
    }

    public void setCardiaca(String cardiaca) {
        this.cardiaca = cardiaca;
    }

    public String getSat() {
        return sat;
    }

    public void setSat(String sat) {
        this.sat = sat;
    }

    public int getAsma() {
        return asma;
    }

    public void setAsma(int asma) {
        this.asma = asma;
    }

    public int getIra() {
        return ira;
    }

    public void setIra(int ira) {
        this.ira = ira;
    }

    public int getTuberculosis() {
        return tuberculosis;
    }

    public void setTuberculosis(int tuberculosis) {
        this.tuberculosis = tuberculosis;
    }

    public int getRinitis() {
        return rinitis;
    }

    public void setRinitis(int rinitis) {
        this.rinitis = rinitis;
    }

    public int getTabaco() {
        return tabaco;
    }

    public void setTabaco(int tabaco) {
        this.tabaco = tabaco;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getAnos() {
        return anos;
    }

    public void setAnos(String anos) {
        this.anos = anos;
    }

    public int getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(int diabetes) {
        this.diabetes = diabetes;
    }

    public int getHipo() {
        return hipo;
    }

    public void setHipo(int hipo) {
        this.hipo = hipo;
    }

    public int getAlergias() {
        return alergias;
    }

    public void setAlergias(int alergias) {
        this.alergias = alergias;
    }

    public int getObesidad() {
        return obesidad;
    }

    public void setObesidad(int obesidad) {
        this.obesidad = obesidad;
    }

    public int getUlcera() {
        return ulcera;
    }

    public void setUlcera(int ulcera) {
        this.ulcera = ulcera;
    }

    public int getRge() {
        return rge;
    }

    public void setRge(int rge) {
        this.rge = rge;
    }

    public String getCualesalergias() {
        return cualesalergias;
    }

    public void setCualesalergias(String cualesalergias) {
        this.cualesalergias = cualesalergias;
    }

    public int getCoronario() {
        return coronario;
    }

    public void setCoronario(int coronario) {
        this.coronario = coronario;
    }

    public int getValvular() {
        return valvular;
    }

    public void setValvular(int valvular) {
        this.valvular = valvular;
    }

    public int getArritmias() {
        return arritmias;
    }

    public void setArritmias(int arritmias) {
        this.arritmias = arritmias;
    }

    public int getMarcapasos() {
        return marcapasos;
    }

    public void setMarcapasos(int marcapasos) {
        this.marcapasos = marcapasos;
    }

    public int getCoagulopatia() {
        return coagulopatia;
    }

    public void setCoagulopatia(int coagulopatia) {
        this.coagulopatia = coagulopatia;
    }

    public int getDislipidemia() {
        return dislipidemia;
    }

    public void setDislipidemia(int dislipidemia) {
        this.dislipidemia = dislipidemia;
    }

    public int getEcv() {
        return ecv;
    }

    public void setEcv(int ecv) {
        this.ecv = ecv;
    }

    public int getEsquizofrenia() {
        return esquizofrenia;
    }

    public void setEsquizofrenia(int esquizofrenia) {
        this.esquizofrenia = esquizofrenia;
    }

    public int getDepresion() {
        return depresion;
    }

    public void setDepresion(int depresion) {
        this.depresion = depresion;
    }

    public int getEpilepsia() {
        return epilepsia;
    }

    public void setEpilepsia(int epilepsia) {
        this.epilepsia = epilepsia;
    }

    public int getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(int alcohol) {
        this.alcohol = alcohol;
    }

    public String getOtrasenfpsiq() {
        return otrasenfpsiq;
    }

    public void setOtrasenfpsiq(String otrasenfpsiq) {
        this.otrasenfpsiq = otrasenfpsiq;
    }

    public int getNefropatia() {
        return nefropatia;
    }

    public void setNefropatia(int nefropatia) {
        this.nefropatia = nefropatia;
    }

    public int getInfeccionurinaria() {
        return infeccionurinaria;
    }

    public void setInfeccionurinaria(int infeccionurinaria) {
        this.infeccionurinaria = infeccionurinaria;
    }

    public int getUropatiaobstructiva() {
        return uropatiaobstructiva;
    }

    public void setUropatiaobstructiva(int uropatiaobstructiva) {
        this.uropatiaobstructiva = uropatiaobstructiva;
    }

    public int getIntervencion() {
        return intervencion;
    }

    public void setIntervencion(int intervencion) {
        this.intervencion = intervencion;
    }

    public int getAntecendetes_anestesicos() {
        return antecendetes_anestesicos;
    }

    public void setAntecendetes_anestesicos(int antecendetes_anestesicos) {
        this.antecendetes_anestesicos = antecendetes_anestesicos;
    }

    public String getEspecificar() {
        return especificar;
    }

    public void setEspecificar(String especificar) {
        this.especificar = especificar;
    }

    public String getTratamieto() {
        return tratamieto;
    }

    public void setTratamieto(String tratamieto) {
        this.tratamieto = tratamieto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

}
