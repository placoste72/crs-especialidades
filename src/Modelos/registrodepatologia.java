/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class registrodepatologia {

    int id_atencion;
    int id_registropatologia;
   
    int vicio_refraccion;
    String diagnostico;
    String fundamentodeldiagnostico;
    String tratamientoeindicacion;
    int confima;
    int tratamientocerca;
    int tratamientolejos;

    public int getId_atencion() {
        return id_atencion;
    }

    public void setId_atencion(int id_atencion) {
        this.id_atencion = id_atencion;
    }

    public int getId_registropatologia() {
        return id_registropatologia;
    }

    public void setId_registropatologia(int id_registropatologia) {
        this.id_registropatologia = id_registropatologia;
    }



    public int getVicio_refraccion() {
        return vicio_refraccion;
    }

    public void setVicio_refraccion(int vicio_refraccion) {
        this.vicio_refraccion = vicio_refraccion;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getFundamentodeldiagnostico() {
        return fundamentodeldiagnostico;
    }

    public void setFundamentodeldiagnostico(String fundamentodeldiagnostico) {
        this.fundamentodeldiagnostico = fundamentodeldiagnostico;
    }

    public String getTratamientoeindicacion() {
        return tratamientoeindicacion;
    }

    public void setTratamientoeindicacion(String tratamientoeindicacion) {
        this.tratamientoeindicacion = tratamientoeindicacion;
    }

    public int getConfima() {
        return confima;
    }

    public void setConfima(int confima) {
        this.confima = confima;
    }

    public int getTratamientocerca() {
        return tratamientocerca;
    }

    public void setTratamientocerca(int tratamientocerca) {
        this.tratamientocerca = tratamientocerca;
    }

    public int getTratamientolejos() {
        return tratamientolejos;
    }

    public void setTratamientolejos(int tratamientolejos) {
        this.tratamientolejos = tratamientolejos;
    }

}
