/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author Informatica
 */
public class paciente {

    private String rut;
    private String nombre;
    private String apellido_paterno;
    private String apellido_moderno;
    private Date fecha_nacimiento;
    private String direccion;
    private String email;
    private String contacto1;
    private String contacto2;
    private int estatus;
    private int id_comuna;
    private int id_region;
    private int id_provincia;
    private Date fecharegistro;
    private int genero;
    private int provision;
    private int tramo;
    private int procedencia;
    private String temporal1;
    private String temporal2;
    private String temporal3;
    private String temporal4;
    private String temporal5;
    private String temporal6;
    private String temporal7;
    private int idnacionalidad;
    private String nombresocial;

    public paciente() {
    }

    public paciente(String rut, String nombre, String apellido_paterno, String apellido_moderno, Date fecha_nacimiento, String direccion, String email, String contacto1, String contacto2, int estatus, int id_comuna, Date fecharegistro, int genero, int provision, int tramo) {
        this.rut = rut;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_moderno = apellido_moderno;
        this.fecha_nacimiento = fecha_nacimiento;
        this.direccion = direccion;
        this.email = email;
        this.contacto1 = contacto1;
        this.contacto2 = contacto2;
        this.estatus = estatus;
        this.id_comuna = id_comuna;
        this.fecharegistro = fecharegistro;
        this.genero = genero;
        this.provision = provision;
        this.tramo = tramo;
    }

    public int getIdnacionalidad() {
        return idnacionalidad;
    }

    public void setIdnacionalidad(int idnacionalidad) {
        this.idnacionalidad = idnacionalidad;
    }

    public String getNombresocial() {
        return nombresocial;
    }

    public void setNombresocial(String nombresocial) {
        this.nombresocial = nombresocial;
    }

    
    
    public String getTemporal7() {
        return temporal7;
    }

    public void setTemporal7(String temporal7) {
        this.temporal7 = temporal7;
    }

    public String getTemporal3() {
        return temporal3;
    }

    public void setTemporal3(String temporal3) {
        this.temporal3 = temporal3;
    }

    public String getTemporal4() {
        return temporal4;
    }

    public void setTemporal4(String temporal4) {
        this.temporal4 = temporal4;
    }

    public String getTemporal5() {
        return temporal5;
    }

    public void setTemporal5(String temporal5) {
        this.temporal5 = temporal5;
    }

    public String getTemporal6() {
        return temporal6;
    }

    public void setTemporal6(String temporal6) {
        this.temporal6 = temporal6;
    }

    public String getTemporal1() {
        return temporal1;
    }

    public void setTemporal1(String temporal1) {
        this.temporal1 = temporal1;
    }

    public String getTemporal2() {
        return temporal2;
    }

    public void setTemporal2(String temporal2) {
        this.temporal2 = temporal2;
    }

    public int getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(int procedencia) {
        this.procedencia = procedencia;
    }

    public int getProvision() {
        return provision;
    }

    public void setProvision(int provision) {
        this.provision = provision;
    }

    public int getTramo() {
        return tramo;
    }

    public void setTramo(int tramo) {
        this.tramo = tramo;
    }

    public int getGenero() {
        return genero;
    }

    public void setGenero(int genero) {
        this.genero = genero;
    }

    public int getId_region() {
        return id_region;
    }

    public void setId_region(int id_region) {
        this.id_region = id_region;
    }

    public int getId_provincia() {
        return id_provincia;
    }

    public void setId_provincia(int id_provincia) {
        this.id_provincia = id_provincia;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_moderno() {
        return apellido_moderno;
    }

    public void setApellido_moderno(String apellido_moderno) {
        this.apellido_moderno = apellido_moderno;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContacto1() {
        return contacto1;
    }

    public void setContacto1(String contacto1) {
        this.contacto1 = contacto1;
    }

    public String getContacto2() {
        return contacto2;
    }

    public void setContacto2(String contacto2) {
        this.contacto2 = contacto2;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public int getId_comuna() {
        return id_comuna;
    }

    public void setId_comuna(int id_comuna) {
        this.id_comuna = id_comuna;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

}
