/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;


import java.io.InputStream;
import java.sql.Date;

/**
 *
 * @author Informatica
 */
public class usuario {
    
    private int id_usuairo;
    private String id_funcionario;
    private String nombre_usuario;
    private String contrasena;
    private String viejacontrasena;
    private Date fecha_registro;
    private int estatus;
    private String email;
     private String nombreimagen;
    private int tamanoimagen;
    private InputStream foto;

    public usuario(int id_usuairo, String id_funcionario, String nombre_usuario, String contrasena, String viejacontrasena, Date fecha_registro, int estatus, String email) {
        this.id_usuairo = id_usuairo;
        this.id_funcionario = id_funcionario;
        this.nombre_usuario = nombre_usuario;
        this.contrasena = contrasena;
        this.viejacontrasena = viejacontrasena;
        this.fecha_registro = fecha_registro;
        this.estatus = estatus;
        this.email = email;
    }

   


    public usuario() {
        //To change body of generated methods, choose Tools | Templates.
    }
    
    

    public String getNombreimagen() {
        return nombreimagen;
    }

    public void setNombreimagen(String nombreimagen) {
        this.nombreimagen = nombreimagen;
    }

    public int getTamanoimagen() {
        return tamanoimagen;
    }

    public void setTamanoimagen(int tamanoimagen) {
        this.tamanoimagen = tamanoimagen;
    }

    public InputStream getFoto() {
        return foto;
    }

    public void setFoto(InputStream foto) {
        this.foto = foto;
    }
    
    

    public int getId_usuairo() {
        return id_usuairo;
    }

    public void setId_usuairo(int id_usuairo) {
        this.id_usuairo = id_usuairo;
    }

    public String getId_funcionario() {
        return id_funcionario;
    }

    public void setId_funcionario(String id_funcionario) {
        this.id_funcionario = id_funcionario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getViejacontrasena() {
        return viejacontrasena;
    }

    public void setViejacontrasena(String viejacontrasena) {
        this.viejacontrasena = viejacontrasena;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
