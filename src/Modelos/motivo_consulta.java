/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class motivo_consulta {
    
   private Integer id_motivo;
   private String nombremotivo;
   private Integer atencion_cita;
   private Integer  estatus;
   private String nombreatencion;

    public Integer getId_motivo() {
        return id_motivo;
    }

    public String getNombreatencion() {
        return nombreatencion;
    }

    public void setNombreatencion(String nombreatencion) {
        this.nombreatencion = nombreatencion;
    }
 
    
     
    public void setId_motivo(Integer id_motivo) {
        this.id_motivo = id_motivo;
    }

    public String getNombremotivo() {
        return nombremotivo;
    }

    public void setNombremotivo(String nombremotivo) {
        this.nombremotivo = nombremotivo;
    }

    public Integer getAtencion_cita() {
        return atencion_cita;
    }

    public void setAtencion_cita(Integer atencion_cita) {
        this.atencion_cita = atencion_cita;
    }

 
    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }
   
   
    
}
