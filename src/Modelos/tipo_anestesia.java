/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class tipo_anestesia {
    
    private int idtipo_atencion;
    private String nombretipo;
    private int estatus;

    public int getIdtipo_atencion() {
        return idtipo_atencion;
    }

    public void setIdtipo_atencion(int idtipo_atencion) {
        this.idtipo_atencion = idtipo_atencion;
    }

    public String getNombretipo() {
        return nombretipo;
    }

    public void setNombretipo(String nombretipo) {
        this.nombretipo = nombretipo;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
    
    
    
}
