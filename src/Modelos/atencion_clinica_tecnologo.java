/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class atencion_clinica_tecnologo {

    private int id_atencion_tecnologo;
    private int id_cita;
    private Date fecha_registro;
    private String tecnologo;
    private String auxiliaruno;
    private String auxiliardos;
    private String auxiliartres;
    private int auxiliarcuatro;
    private String auxiliarcinco;
    private String auxiliarseis;
    private String auxiliarsiete;
    private String auxiliarocho;
    private String auxiliarnueve;
    private String auxiliardiez;
    private String auxiliaronce;
    private String auxiliardoce;
    private String auxiliartrece;
    private String auxiliarcatorce;
    private String auxiliarquince;
    private int pertinencia;
    

    public String getTecnologo() {
        return tecnologo;
    }

    public String getAuxiliartrece() {
        return auxiliartrece;
    }

    public int getPertinencia() {
        return pertinencia;
    }

    public void setPertinencia(int pertinencia) {
        this.pertinencia = pertinencia;
    }
    
    

    public void setAuxiliartrece(String auxiliartrece) {
        this.auxiliartrece = auxiliartrece;
    }

    public String getAuxiliarcatorce() {
        return auxiliarcatorce;
    }

    public void setAuxiliarcatorce(String auxiliarcatorce) {
        this.auxiliarcatorce = auxiliarcatorce;
    }

    public String getAuxiliarquince() {
        return auxiliarquince;
    }

    public void setAuxiliarquince(String auxiliarquince) {
        this.auxiliarquince = auxiliarquince;
    }
    
     
     
    public String getAuxiliarcinco() {
        return auxiliarcinco;
    }

    public void setAuxiliarcinco(String auxiliarcinco) {
        this.auxiliarcinco = auxiliarcinco;
    }

    public String getAuxiliarseis() {
        return auxiliarseis;
    }

    public void setAuxiliarseis(String auxiliarseis) {
        this.auxiliarseis = auxiliarseis;
    }

    public String getAuxiliarsiete() {
        return auxiliarsiete;
    }

    public void setAuxiliarsiete(String auxiliarsiete) {
        this.auxiliarsiete = auxiliarsiete;
    }

    public String getAuxiliarocho() {
        return auxiliarocho;
    }

    public void setAuxiliarocho(String auxiliarocho) {
        this.auxiliarocho = auxiliarocho;
    }

    public String getAuxiliarnueve() {
        return auxiliarnueve;
    }

    public void setAuxiliarnueve(String auxiliarnueve) {
        this.auxiliarnueve = auxiliarnueve;
    }

    public String getAuxiliardiez() {
        return auxiliardiez;
    }

    public void setAuxiliardiez(String auxiliardiez) {
        this.auxiliardiez = auxiliardiez;
    }

    public String getAuxiliaronce() {
        return auxiliaronce;
    }

    public void setAuxiliaronce(String auxiliaronce) {
        this.auxiliaronce = auxiliaronce;
    }

    public String getAuxiliardoce() {
        return auxiliardoce;
    }

    public void setAuxiliardoce(String auxiliardoce) {
        this.auxiliardoce = auxiliardoce;
    }
     

    public String getAuxiliaruno() {
        return auxiliaruno;
    }

    public void setAuxiliaruno(String auxiliaruno) {
        this.auxiliaruno = auxiliaruno;
    }

    public String getAuxiliardos() {
        return auxiliardos;
    }

    public void setAuxiliardos(String auxiliardos) {
        this.auxiliardos = auxiliardos;
    }

    public String getAuxiliartres() {
        return auxiliartres;
    }

    public void setAuxiliartres(String auxiliartres) {
        this.auxiliartres = auxiliartres;
    }

    public int getAuxiliarcuatro() {
        return auxiliarcuatro;
    }

    public void setAuxiliarcuatro(int auxiliarcuatro) {
        this.auxiliarcuatro = auxiliarcuatro;
    }

    public void setTecnologo(String tecnologo) {
        this.tecnologo = tecnologo;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public int getId_atencion_tecnologo() {
        return id_atencion_tecnologo;
    }

    public void setId_atencion_tecnologo(int id_atencion_tecnologo) {
        this.id_atencion_tecnologo = id_atencion_tecnologo;
    }

    public int getId_cita() {
        return id_cita;
    }

    public void setId_cita(int id_cita) {
        this.id_cita = id_cita;
    }

}
