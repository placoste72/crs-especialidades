/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Informatica
 */


public class rol {
    private  int idRol;
    private String nombreRol;
    private int estatus;

    public rol() {
    }
 
    
    
    
    public rol(int idRol, String nombreRol, int estatus) {
        this.idRol = idRol;
        this.nombreRol = nombreRol;
        this.estatus = estatus;
       
    }

    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

  
    
    
    
}
