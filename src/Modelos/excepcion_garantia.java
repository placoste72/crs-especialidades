/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class excepcion_garantia {
     
    private int id_atencion;
    private int idexcepcion_garantia;
    private int  rechazo_prestador_designado;
    private int rechazo_atencion_garantizado;
    private int prestacion_rechazada;
    private int  otra_causa;
    private String  observaciones ;
    private String auxiliar1;
    private String auxiliar2;
    private String auxiliar3;
    private String auxiliar4;
    private String auxiliar5;
    private String auxiliar6;
    private String auxiliar7;
    private String auxiliar8;
    private String auxiliar9;
    private String auxiliar10;
    private String problemaauge;
    private int tipo;
    

    public int getIdexcepcion_garantia() {
        return idexcepcion_garantia;
    }

    public String getAuxiliar1() {
        return auxiliar1;
    }

    public String getProblemaauge() {
        return problemaauge;
    }

    public void setProblemaauge(String problemaauge) {
        this.problemaauge = problemaauge;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    

    public void setAuxiliar1(String auxiliar1) {
        this.auxiliar1 = auxiliar1;
    }

    public String getAuxiliar2() {
        return auxiliar2;
    }

    public void setAuxiliar2(String auxiliar2) {
        this.auxiliar2 = auxiliar2;
    }

    public String getAuxiliar3() {
        return auxiliar3;
    }

    public void setAuxiliar3(String auxiliar3) {
        this.auxiliar3 = auxiliar3;
    }

    public String getAuxiliar4() {
        return auxiliar4;
    }

    public void setAuxiliar4(String auxiliar4) {
        this.auxiliar4 = auxiliar4;
    }

    public String getAuxiliar5() {
        return auxiliar5;
    }

    public void setAuxiliar5(String auxiliar5) {
        this.auxiliar5 = auxiliar5;
    }

    public String getAuxiliar6() {
        return auxiliar6;
    }

    public void setAuxiliar6(String auxiliar6) {
        this.auxiliar6 = auxiliar6;
    }

    public String getAuxiliar7() {
        return auxiliar7;
    }

    public void setAuxiliar7(String auxiliar7) {
        this.auxiliar7 = auxiliar7;
    }

    public String getAuxiliar8() {
        return auxiliar8;
    }

    public void setAuxiliar8(String auxiliar8) {
        this.auxiliar8 = auxiliar8;
    }

    public String getAuxiliar9() {
        return auxiliar9;
    }

    public void setAuxiliar9(String auxiliar9) {
        this.auxiliar9 = auxiliar9;
    }

    public String getAuxiliar10() {
        return auxiliar10;
    }

    public void setAuxiliar10(String auxiliar10) {
        this.auxiliar10 = auxiliar10;
    }
    
    

    public void setIdexcepcion_garantia(int idexcepcion_garantia) {
        this.idexcepcion_garantia = idexcepcion_garantia;
    }

    
    
    public int getId_atencion() {
        return id_atencion;
    }

    public void setId_atencion(int id_atencion) {
        this.id_atencion = id_atencion;
    }

    public int getRechazo_prestador_designado() {
        return rechazo_prestador_designado;
    }

    public void setRechazo_prestador_designado(int rechazo_prestador_designado) {
        this.rechazo_prestador_designado = rechazo_prestador_designado;
    }

    public int getRechazo_atencion_garantizado() {
        return rechazo_atencion_garantizado;
    }

    public void setRechazo_atencion_garantizado(int rechazo_atencion_garantizado) {
        this.rechazo_atencion_garantizado = rechazo_atencion_garantizado;
    }

    public int getPrestacion_rechazada() {
        return prestacion_rechazada;
    }

    public void setPrestacion_rechazada(int prestacion_rechazada) {
        this.prestacion_rechazada = prestacion_rechazada;
    }

    public int getOtra_causa() {
        return otra_causa;
    }

    public void setOtra_causa(int otra_causa) {
        this.otra_causa = otra_causa;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }


}
