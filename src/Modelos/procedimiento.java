/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Informatica
 */
public class procedimiento {
    
    private int id;
    private String grupofonasa;
    private String subgrupofonasa;
    private String prestacionesfonasa;
    private String descripcion;
    private int estatus;
    private int id_especialidad;

    public procedimiento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGrupofonasa() {
        return grupofonasa;
    }

    public void setGrupofonasa(String grupofonasa) {
        this.grupofonasa = grupofonasa;
    }

    public String getSubgrupofonasa() {
        return subgrupofonasa;
    }

    public void setSubgrupofonasa(String subgrupofonasa) {
        this.subgrupofonasa = subgrupofonasa;
    }

    public String getPrestacionesfonasa() {
        return prestacionesfonasa;
    }

    public void setPrestacionesfonasa(String prestacionesfonasa) {
        this.prestacionesfonasa = prestacionesfonasa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public int getId_especialidad() {
        return id_especialidad;
    }

    public void setId_especialidad(int id_especialidad) {
        this.id_especialidad = id_especialidad;
    }
    
    
    
}
