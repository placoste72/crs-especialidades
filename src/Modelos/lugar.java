/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class lugar {

    private int id_lugar;
    private String nombre;
    private int estatu;
    private boolean temporal;
    private boolean temporal1;
    private boolean temporal2;
    private Date fecha;
    private Date fecha2;

    /*para informe general*/
    private int id_atencion;
    private int id_diagnostico_urgencia;
    private int idadmision;
    private int ges;
    private String intervencio;
    private int id_prestacion_urgencia;
    private String indicaciones;
    private boolean interconsulta;
    private String detalle_de_la_derivacion;
    private int id_centro;
    private int urgencia;
    private String prescripcion;
    private String descripcion;

    private String folio;

    private String rut;
    private String fechanacimiento;
    private String edad;
    private String sexo;
    private String direccion;
    private String comuna;
    private String telefono1;
    private String email;
    private String sederivapara;
    private String seeviapara;
    private String diagnostico;
    private String examenesrealizados;
    private String nombredoctor;
    private String rutdoctor;

    private boolean absceso_submucoso;
    private boolean gingivitis_ulcero;
    private boolean complicacion_post_exodoncia;
    private boolean traumatismo_dento_alveolares;
    private boolean pericoronaritis;
    private boolean pulpitis;

    private boolean siurgencia;
    private boolean exodoncia_permanente;
    private boolean kit_aseo_bucal;
    private boolean sutura_intraoral;
    private boolean obturacion_composite;
    private boolean radiografia_retroalvealar;
    private boolean obturacion_vidrio_ionomero;

    private int numero_de_rx;

    private boolean trepanacion;
    private boolean pulpotamia;
    private boolean recubrmiento;
    private boolean ferulizacion;
    private boolean terapia_farmacolofica;
    private boolean cirugia_bucal;

    private int numero_exodenciat;
    private int numero_exodonciap;
    private String hora;

    private String tratamiento;
    private String examenes;
    private String procedimientos;

    private String observaciones;
    private String variable1;
    private String variable2;
    private String variable3;
    private String variable4;
    private double precioapagar;

    public int getId_atencion() {
        return id_atencion;
    }

    public double getPrecioapagar() {
        return precioapagar;
    }

    public void setPrecioapagar(double precioapagar) {
        this.precioapagar = precioapagar;
    }

    public Date getFecha2() {
        return fecha2;
    }

    public void setFecha2(Date fecha2) {
        this.fecha2 = fecha2;
    }
    
    

    public String getHora() {
        return hora;
    }

    public String getVariable1() {
        return variable1;
    }

    public void setVariable1(String variable1) {
        this.variable1 = variable1;
    }

    public String getVariable2() {
        return variable2;
    }

    public void setVariable2(String variable2) {
        this.variable2 = variable2;
    }

    public String getVariable3() {
        return variable3;
    }

    public void setVariable3(String variable3) {
        this.variable3 = variable3;
    }

    public String getVariable4() {
        return variable4;
    }

    public void setVariable4(String variable4) {
        this.variable4 = variable4;
    }
    
    

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getExamenes() {
        return examenes;
    }

    public void setExamenes(String examenes) {
        this.examenes = examenes;
    }

    public String getProcedimientos() {
        return procedimientos;
    }

    public void setProcedimientos(String procedimientos) {
        this.procedimientos = procedimientos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public void setId_atencion(int id_atencion) {
        this.id_atencion = id_atencion;
    }

    public int getId_diagnostico_urgencia() {
        return id_diagnostico_urgencia;
    }

    public void setId_diagnostico_urgencia(int id_diagnostico_urgencia) {
        this.id_diagnostico_urgencia = id_diagnostico_urgencia;
    }

    public int getIdadmision() {
        return idadmision;
    }

    public void setIdadmision(int idadmision) {
        this.idadmision = idadmision;
    }

    public int getGes() {
        return ges;
    }

    public void setGes(int ges) {
        this.ges = ges;
    }

    public String getIntervencio() {
        return intervencio;
    }

    public void setIntervencio(String intervencio) {
        this.intervencio = intervencio;
    }

    public int getId_prestacion_urgencia() {
        return id_prestacion_urgencia;
    }

    public void setId_prestacion_urgencia(int id_prestacion_urgencia) {
        this.id_prestacion_urgencia = id_prestacion_urgencia;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public boolean isInterconsulta() {
        return interconsulta;
    }

    public void setInterconsulta(boolean interconsulta) {
        this.interconsulta = interconsulta;
    }

    public String getDetalle_de_la_derivacion() {
        return detalle_de_la_derivacion;
    }

    public void setDetalle_de_la_derivacion(String detalle_de_la_derivacion) {
        this.detalle_de_la_derivacion = detalle_de_la_derivacion;
    }

    public int getId_centro() {
        return id_centro;
    }

    public void setId_centro(int id_centro) {
        this.id_centro = id_centro;
    }

    public int getUrgencia() {
        return urgencia;
    }

    public void setUrgencia(int urgencia) {
        this.urgencia = urgencia;
    }

    public String getPrescripcion() {
        return prescripcion;
    }

    public void setPrescripcion(String prescripcion) {
        this.prescripcion = prescripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(String fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSederivapara() {
        return sederivapara;
    }

    public void setSederivapara(String sederivapara) {
        this.sederivapara = sederivapara;
    }

    public String getSeeviapara() {
        return seeviapara;
    }

    public void setSeeviapara(String seeviapara) {
        this.seeviapara = seeviapara;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getExamenesrealizados() {
        return examenesrealizados;
    }

    public void setExamenesrealizados(String examenesrealizados) {
        this.examenesrealizados = examenesrealizados;
    }

    public String getNombredoctor() {
        return nombredoctor;
    }

    public void setNombredoctor(String nombredoctor) {
        this.nombredoctor = nombredoctor;
    }

    public String getRutdoctor() {
        return rutdoctor;
    }

    public void setRutdoctor(String rutdoctor) {
        this.rutdoctor = rutdoctor;
    }

    public boolean isAbsceso_submucoso() {
        return absceso_submucoso;
    }

    public void setAbsceso_submucoso(boolean absceso_submucoso) {
        this.absceso_submucoso = absceso_submucoso;
    }

    public boolean isGingivitis_ulcero() {
        return gingivitis_ulcero;
    }

    public void setGingivitis_ulcero(boolean gingivitis_ulcero) {
        this.gingivitis_ulcero = gingivitis_ulcero;
    }

    public boolean isComplicacion_post_exodoncia() {
        return complicacion_post_exodoncia;
    }

    public void setComplicacion_post_exodoncia(boolean complicacion_post_exodoncia) {
        this.complicacion_post_exodoncia = complicacion_post_exodoncia;
    }

    public boolean isTraumatismo_dento_alveolares() {
        return traumatismo_dento_alveolares;
    }

    public void setTraumatismo_dento_alveolares(boolean traumatismo_dento_alveolares) {
        this.traumatismo_dento_alveolares = traumatismo_dento_alveolares;
    }

    public boolean isPericoronaritis() {
        return pericoronaritis;
    }

    public void setPericoronaritis(boolean pericoronaritis) {
        this.pericoronaritis = pericoronaritis;
    }

    public boolean isPulpitis() {
        return pulpitis;
    }

    public void setPulpitis(boolean pulpitis) {
        this.pulpitis = pulpitis;
    }

    public boolean isSiurgencia() {
        return siurgencia;
    }

    public void setSiurgencia(boolean siurgencia) {
        this.siurgencia = siurgencia;
    }

    public boolean isExodoncia_permanente() {
        return exodoncia_permanente;
    }

    public void setExodoncia_permanente(boolean exodoncia_permanente) {
        this.exodoncia_permanente = exodoncia_permanente;
    }

    public boolean isKit_aseo_bucal() {
        return kit_aseo_bucal;
    }

    public void setKit_aseo_bucal(boolean kit_aseo_bucal) {
        this.kit_aseo_bucal = kit_aseo_bucal;
    }

    public boolean isSutura_intraoral() {
        return sutura_intraoral;
    }

    public void setSutura_intraoral(boolean sutura_intraoral) {
        this.sutura_intraoral = sutura_intraoral;
    }

    public boolean isObturacion_composite() {
        return obturacion_composite;
    }

    public void setObturacion_composite(boolean obturacion_composite) {
        this.obturacion_composite = obturacion_composite;
    }

    public boolean isRadiografia_retroalvealar() {
        return radiografia_retroalvealar;
    }

    public void setRadiografia_retroalvealar(boolean radiografia_retroalvealar) {
        this.radiografia_retroalvealar = radiografia_retroalvealar;
    }

    public boolean isObturacion_vidrio_ionomero() {
        return obturacion_vidrio_ionomero;
    }

    public void setObturacion_vidrio_ionomero(boolean obturacion_vidrio_ionomero) {
        this.obturacion_vidrio_ionomero = obturacion_vidrio_ionomero;
    }

    public int getNumero_de_rx() {
        return numero_de_rx;
    }

    public void setNumero_de_rx(int numero_de_rx) {
        this.numero_de_rx = numero_de_rx;
    }

    public boolean isTrepanacion() {
        return trepanacion;
    }

    public void setTrepanacion(boolean trepanacion) {
        this.trepanacion = trepanacion;
    }

    public boolean isPulpotamia() {
        return pulpotamia;
    }

    public void setPulpotamia(boolean pulpotamia) {
        this.pulpotamia = pulpotamia;
    }

    public boolean isRecubrmiento() {
        return recubrmiento;
    }

    public void setRecubrmiento(boolean recubrmiento) {
        this.recubrmiento = recubrmiento;
    }

    public boolean isFerulizacion() {
        return ferulizacion;
    }

    public void setFerulizacion(boolean ferulizacion) {
        this.ferulizacion = ferulizacion;
    }

    public boolean isTerapia_farmacolofica() {
        return terapia_farmacolofica;
    }

    public void setTerapia_farmacolofica(boolean terapia_farmacolofica) {
        this.terapia_farmacolofica = terapia_farmacolofica;
    }

    public boolean isCirugia_bucal() {
        return cirugia_bucal;
    }

    public void setCirugia_bucal(boolean cirugia_bucal) {
        this.cirugia_bucal = cirugia_bucal;
    }

    public int getNumero_exodenciat() {
        return numero_exodenciat;
    }

    public void setNumero_exodenciat(int numero_exodenciat) {
        this.numero_exodenciat = numero_exodenciat;
    }

    public int getNumero_exodonciap() {
        return numero_exodonciap;
    }

    public void setNumero_exodonciap(int numero_exodonciap) {
        this.numero_exodonciap = numero_exodonciap;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getId_lugar() {
        return id_lugar;
    }

    public boolean isTemporal() {
        return temporal;
    }

    public void setTemporal(boolean temporal) {
        this.temporal = temporal;
    }

    public boolean isTemporal1() {
        return temporal1;
    }

    public void setTemporal1(boolean temporal1) {
        this.temporal1 = temporal1;
    }

    public boolean isTemporal2() {
        return temporal2;
    }

    public void setTemporal2(boolean temporal2) {
        this.temporal2 = temporal2;
    }

    public void setId_lugar(int id_lugar) {
        this.id_lugar = id_lugar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstatu() {
        return estatu;
    }

    public void setEstatu(int estatu) {
        this.estatu = estatu;
    }

}
