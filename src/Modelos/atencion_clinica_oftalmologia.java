/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class atencion_clinica_oftalmologia {

    private int id_cita;
   private int id_atencion_coftalmologia;
  
  private int autorefraccion;
  private String autorefaccionobservaciones;
  private int  presionio;
  private int  presionod;
  private double numerooi;
  private double numerood;
  private String presionobservaciones;
  private int dilatacionoi;
  private int dilatacionod;
  private String dilatacion_colirio;
  private String dilatacion_observaciones;
  private String registro_de_observaciones;
  private String tecnico;
  private Date fecha_registro;
  private String usuario;
  private String idusuario;
 

    public int getId_cita() {
        return id_cita;
    }

    public String getUsuario() {
        return usuario;
    }

    

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }
    
    

    public void setId_cita(int id_cita) {
        this.id_cita = id_cita;
    }

    public int getDilatacionoi() {
        return dilatacionoi;
    }

    public void setDilatacionoi(int dilatacionoi) {
        this.dilatacionoi = dilatacionoi;
    }

    public int getDilatacionod() {
        return dilatacionod;
    }

    public void setDilatacionod(int dilatacionod) {
        this.dilatacionod = dilatacionod;
    }
    
    

    public String getTecnico() {
        return tecnico;
    }

    public void setTecnico(String tecnico) {
        this.tecnico = tecnico;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }
    
    

    public int getId_atencion_coftalmologia() {
        return id_atencion_coftalmologia;
    }

    public void setId_atencion_coftalmologia(int id_atencion_coftalmologia) {
        this.id_atencion_coftalmologia = id_atencion_coftalmologia;
    }

    public int getAutorefraccion() {
        return autorefraccion;
    }

    public void setAutorefraccion(int autorefraccion) {
        this.autorefraccion = autorefraccion;
    }

    public String getAutorefaccionobservaciones() {
        return autorefaccionobservaciones;
    }

    public void setAutorefaccionobservaciones(String autorefaccionobservaciones) {
        this.autorefaccionobservaciones = autorefaccionobservaciones;
    }

    public int getPresionio() {
        return presionio;
    }

    public void setPresionio(int presionio) {
        this.presionio = presionio;
    }

    public int getPresionod() {
        return presionod;
    }

    public void setPresionod(int presionod) {
        this.presionod = presionod;
    }

    public double getNumerooi() {
        return numerooi;
    }

    public void setNumerooi(double numerooi) {
        this.numerooi = numerooi;
    }

    public double getNumerood() {
        return numerood;
    }

    public void setNumerood(double numerood) {
        this.numerood = numerood;
    }

    public String getPresionobservaciones() {
        return presionobservaciones;
    }

    public void setPresionobservaciones(String presionobservaciones) {
        this.presionobservaciones = presionobservaciones;
    }

  

    public String getDilatacion_colirio() {
        return dilatacion_colirio;
    }

    public void setDilatacion_colirio(String dilatacion_colirio) {
        this.dilatacion_colirio = dilatacion_colirio;
    }

    public String getDilatacion_observaciones() {
        return dilatacion_observaciones;
    }

    public void setDilatacion_observaciones(String dilatacion_observaciones) {
        this.dilatacion_observaciones = dilatacion_observaciones;
    }

    public String getRegistro_de_observaciones() {
        return registro_de_observaciones;
    }

    public void setRegistro_de_observaciones(String registro_de_observaciones) {
        this.registro_de_observaciones = registro_de_observaciones;
    }

   

}
