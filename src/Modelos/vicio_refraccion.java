/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class vicio_refraccion {

    private int id_atencion;
    private int id_viciorefraccion;
    private int amhta;
    private int amdm;
    private String anosam;
    private int amglaucoma;
    private String amotro;
    private String piod;
    private String pioi;
    private String plscod;
private String plccod;
private String plscoi;
private String plccoi;
private String plccadi;
    private String reflejofotomotor;
    private String rojopopular;
    private String bmcpoloposterior;
    private String indicaciones;
    private String motivoconsulta;
    private int ges;

    public int getId_atencion() {
        return id_atencion;
    }

    public int getGes() {
        return ges;
    }

    public void setGes(int ges) {
        this.ges = ges;
    }

    
    
    public String getMotivoconsulta() {
        return motivoconsulta;
    }

    public void setMotivoconsulta(String motivoconsulta) {
        this.motivoconsulta = motivoconsulta;
    }
    
    

    public void setId_atencion(int id_atencion) {
        this.id_atencion = id_atencion;
    }

    public int getId_viciorefraccion() {
        return id_viciorefraccion;
    }

    public void setId_viciorefraccion(int id_viciorefraccion) {
        this.id_viciorefraccion = id_viciorefraccion;
    }

    public int getAmhta() {
        return amhta;
    }

    public void setAmhta(int amhta) {
        this.amhta = amhta;
    }

    public int getAmdm() {
        return amdm;
    }

    public void setAmdm(int amdm) {
        this.amdm = amdm;
    }

    public String getAnosam() {
        return anosam;
    }

    public void setAnosam(String anosam) {
        this.anosam = anosam;
    }

    public int getAmglaucoma() {
        return amglaucoma;
    }

    public void setAmglaucoma(int amglaucoma) {
        this.amglaucoma = amglaucoma;
    }

    public String getAmotro() {
        return amotro;
    }

    public void setAmotro(String amotro) {
        this.amotro = amotro;
    }

    public String getPiod() {
        return piod;
    }

    public void setPiod(String piod) {
        this.piod = piod;
    }

    public String getPioi() {
        return pioi;
    }

    public void setPioi(String pioi) {
        this.pioi = pioi;
    }

    public String getPlscod() {
        return plscod;
    }

    public void setPlscod(String plscod) {
        this.plscod = plscod;
    }

    public String getPlccadi() {
        return plccadi;
    }

    public void setPlccadi(String plccadi) {
        this.plccadi = plccadi;
    }

    
    public String getPlscoi() {
        return plscoi;
    }

    public void setPlscoi(String plscoi) {
        this.plscoi = plscoi;
    }

    public String getPlccod() {
        return plccod;
    }

    public void setPlccod(String plccod) {
        this.plccod = plccod;
    }

    public String getPlccoi() {
        return plccoi;
    }

    public void setPlccoi(String plccoi) {
        this.plccoi = plccoi;
    }


    public String getReflejofotomotor() {
        return reflejofotomotor;
    }

    public void setReflejofotomotor(String reflejofotomotor) {
        this.reflejofotomotor = reflejofotomotor;
    }

    public String getRojopopular() {
        return rojopopular;
    }

    public void setRojopopular(String rojopopular) {
        this.rojopopular = rojopopular;
    }

    public String getBmcpoloposterior() {
        return bmcpoloposterior;
    }

    public void setBmcpoloposterior(String bmcpoloposterior) {
        this.bmcpoloposterior = bmcpoloposterior;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }
    
    
    

}
