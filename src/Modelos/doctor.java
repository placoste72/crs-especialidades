/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.InputStream;

/**
 *
 * @author Informatica
 */
public class doctor {
   private  String rut;
   private String nombre;
   private String apellido_paterno;
   private String apellido_materno;
   private String telefono;
   private String email;
   private int estatus;
    private String nombrefirmadoctor;
    private int tamanofirmadoctor;
    private InputStream firmadoctor;
    private String profesion;
    private int variable;

    public doctor(String rut, String nombre, String apellido_paterno, String apellido_materno, String telefono, String email, int estatus) {
        this.rut = rut;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.telefono = telefono;
        this.email = email;
        this.estatus = estatus;
    }

    public int getVariable() {
        return variable;
    }

    public void setVariable(int variable) {
        this.variable = variable;
    }

    
    
    public doctor() {
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    
    
    public String getNombrefirmadoctor() {
        return nombrefirmadoctor;
    }

    public void setNombrefirmadoctor(String nombrefirmadoctor) {
        this.nombrefirmadoctor = nombrefirmadoctor;
    }

    public int getTamanofirmadoctor() {
        return tamanofirmadoctor;
    }

    public void setTamanofirmadoctor(int tamanofirmadoctor) {
        this.tamanofirmadoctor = tamanofirmadoctor;
    }

    public InputStream getFirmadoctor() {
        return firmadoctor;
    }

    public void setFirmadoctor(InputStream firmadoctor) {
        this.firmadoctor = firmadoctor;
    }

    
    
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
   
   
   
 
    
}
