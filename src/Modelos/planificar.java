/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Informatica
 */
public class planificar {

    private int id_planificacion;
    private String rut_doctor;
    private String hora_inicio;
    private String hora_fin;

    private Date fecha;

    private int id_atencion;
    private int id_programa;
    private int numero_cupo;
    private int duracion;
    private int estatus;
    private int id_especialidad;
    private int cupos_disponibles;
    private int id_prestacion;
    private int cupos_bloqueados;
    private String especialidad;
    private String atencion;
    private String programa;
    private String prestacion;
    private String fechaconformato;
    
    private int pabellon;
    private String horainiciopabellon;
    private String horafinpabellon;

    public planificar() {
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public int getPabellon() {
        return pabellon;
    }

    public void setPabellon(int pabellon) {
        this.pabellon = pabellon;
    }

    public String getHorainiciopabellon() {
        return horainiciopabellon;
    }

    public void setHorainiciopabellon(String horainiciopabellon) {
        this.horainiciopabellon = horainiciopabellon;
    }

    public String getHorafinpabellon() {
        return horafinpabellon;
    }

    public void setHorafinpabellon(String horafinpabellon) {
        this.horafinpabellon = horafinpabellon;
    }
    
    

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public int getCupos_bloqueados() {
        return cupos_bloqueados;
    }

    public void setCupos_bloqueados(int cupos_bloqueados) {
        this.cupos_bloqueados = cupos_bloqueados;
    }
    
    

    public String getAtencion() {
        return atencion;
    }

    public void setAtencion(String atencion) {
        this.atencion = atencion;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getPrestacion() {
        return prestacion;
    }

    public void setPrestacion(String prestacion) {
        this.prestacion = prestacion;
    }

    public String getFechaconformato() {
        return fechaconformato;
    }

    public void setFechaconformato(String fechaconformato) {
        this.fechaconformato = fechaconformato;
    }
    
    

    public int getId_prestacion() {
        return id_prestacion;
    }

    public void setId_prestacion(int id_prestacion) {
        this.id_prestacion = id_prestacion;
    }
    
    

    public int getCupos_disponibles() {
        return cupos_disponibles;
    }

    public void setCupos_disponibles(int cupos_disponibles) {
        this.cupos_disponibles = cupos_disponibles;
    }

   

    public int getId_especialidad() {
        return id_especialidad;
    }

    public void setId_especialidad(int id_especialidad) {
        this.id_especialidad = id_especialidad;
    }

    public int getId_planificacion() {
        return id_planificacion;
    }

    public void setId_planificacion(int id_planificacion) {
        this.id_planificacion = id_planificacion;
    }

    public String getRut_doctor() {
        return rut_doctor;
    }

    public void setRut_doctor(String rut_doctor) {
        this.rut_doctor = rut_doctor;
    }

    public String getHora_inicio() {
        return hora_inicio;
    }

    public void setHora_inicio(String hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public String getHora_fin() {
        return hora_fin;
    }

    public void setHora_fin(String hora_fin) {
        this.hora_fin = hora_fin;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getId_atencion() {
        return id_atencion;
    }

    public void setId_atencion(int id_atencion) {
        this.id_atencion = id_atencion;
    }

    public int getId_programa() {
        return id_programa;
    }

    public void setId_programa(int id_programa) {
        this.id_programa = id_programa;
    }

    public int getNumero_cupo() {
        return numero_cupo;
    }

    public void setNumero_cupo(int numero_cupo) {
        this.numero_cupo = numero_cupo;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

}
