/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author a
 */
public class oferta {

    private Integer id_oferta;
    private Integer id_plani_sobre;
    private String hora;
    private Integer estatus;
    private Integer procede;
    private String horapabellon;
    private String temporal;
    private String temporal1;
    private String temporal2;
    private String temporal3;
    private String temporal4;
    private String temporal5;
    private String temporal6;
    private String temporal7;
    private String temporal8;
    private int cupos;
    private int duracion;
    private int sala;
    private int temporal9;

    public String getTemporal4() {
        return temporal4;
    }

    public void setTemporal4(String temporal4) {
        this.temporal4 = temporal4;
    }

    public String getTemporal5() {
        return temporal5;
    }

    public void setTemporal5(String temporal5) {
        this.temporal5 = temporal5;
    }

    public String getTemporal6() {
        return temporal6;
    }

    public void setTemporal6(String temporal6) {
        this.temporal6 = temporal6;
    }

    public String getTemporal7() {
        return temporal7;
    }

    public void setTemporal7(String temporal7) {
        this.temporal7 = temporal7;
    }

    public String getTemporal8() {
        return temporal8;
    }

    public void setTemporal8(String temporal8) {
        this.temporal8 = temporal8;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getSala() {
        return sala;
    }

    public void setSala(int sala) {
        this.sala = sala;
    }
    
    

    public Integer getId_oferta() {
        return id_oferta;
    }

    public int getCupos() {
        return cupos;
    }

    public void setCupos(int cupos) {
        this.cupos = cupos;
    }

    public void setId_oferta(Integer id_oferta) {
        this.id_oferta = id_oferta;
    }

    public String getTemporal() {
        return temporal;
    }

    public void setTemporal(String temporal) {
        this.temporal = temporal;
    }

    public String getTemporal1() {
        return temporal1;
    }

    public void setTemporal1(String temporal1) {
        this.temporal1 = temporal1;
    }

    public String getTemporal2() {
        return temporal2;
    }

    public void setTemporal2(String temporal2) {
        this.temporal2 = temporal2;
    }

    public String getTemporal3() {
        return temporal3;
    }

    public void setTemporal3(String temporal3) {
        this.temporal3 = temporal3;
    }

    public String getHorapabellon() {
        return horapabellon;
    }

    public void setHorapabellon(String horapabellon) {
        this.horapabellon = horapabellon;
    }

    public Integer getId_plani_sobre() {
        return id_plani_sobre;
    }

    public void setId_plani_sobre(Integer id_plani_sobre) {
        this.id_plani_sobre = id_plani_sobre;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Integer getProcede() {
        return procede;
    }

    public void setProcede(Integer procede) {
        this.procede = procede;
    }

    public int getTemporal9() {
        return temporal9;
    }

    public void setTemporal9(int temporal9) {
        this.temporal9 = temporal9;
    }
    

}
