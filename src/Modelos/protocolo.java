/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class protocolo {
    private int idProtocolo;
    private int idCita;
    private int idSalaPabellon;
    private String horaIngresoPabellon;
    private String horaInicioCirugia;
    private String horaSalidaPabellon;
    private String horaTerminoCirugia;
    private String tiempoQuirurjico;
    private int idTipoAnestesia;
    private String horaAnestesia;
    private int idTipoHerida;
    private int electrobisturi;
    private String preOperatorio;
    private String postOperatorio;
    
    private String rutArsenalero;
    private String rutProfesionalAyudante;
    private String rutMedicoAnestesista;
    private String rutPabellonera;
    private String rutPerfusionista;
    private String detalleOperatorio;
    private String recomendaciones;
    private Date fechaRegistro;
    private String usuario;
    private String rutmedicocirujano;
    private String variable1;
    private String variable2;
      private String variable3;
    private String variable4;
      private String variable5;
    private String variable6;
     private String variable7;
     private String variable8;
     private String variable9;
     private String varialble10;

    public String getVarialble10() {
        return varialble10;
    }

    public void setVarialble10(String varialble10) {
        this.varialble10 = varialble10;
    }
     
     

    public String getVariable7() {
        return variable7;
    }

    public void setVariable7(String variable7) {
        this.variable7 = variable7;
    }

    public String getVariable8() {
        return variable8;
    }

    public void setVariable8(String variable8) {
        this.variable8 = variable8;
    }

    public String getVariable9() {
        return variable9;
    }

    public void setVariable9(String variable9) {
        this.variable9 = variable9;
    }


    
     

    public int getIdProtocolo() {
        return idProtocolo;
    }

    public String getVariable1() {
        return variable1;
    }

    public String getVariable3() {
        return variable3;
    }

    public void setVariable3(String variable3) {
        this.variable3 = variable3;
    }

    public String getVariable4() {
        return variable4;
    }

    public void setVariable4(String variable4) {
        this.variable4 = variable4;
    }

    public String getVariable5() {
        return variable5;
    }

    public void setVariable5(String variable5) {
        this.variable5 = variable5;
    }

    public String getVariable6() {
        return variable6;
    }

    public void setVariable6(String variable6) {
        this.variable6 = variable6;
    }
    
    

    public void setVariable1(String variable1) {
        this.variable1 = variable1;
    }

    public String getVariable2() {
        return variable2;
    }

    public void setVariable2(String variable2) {
        this.variable2 = variable2;
    }
    
    

    public String getRutmedicocirujano() {
        return rutmedicocirujano;
    }

    public void setRutmedicocirujano(String rutmedicocirujano) {
        this.rutmedicocirujano = rutmedicocirujano;
    }
 
    
    
    public void setIdProtocolo(int idProtocolo) {
        this.idProtocolo = idProtocolo;
    }

    public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }

    public int getIdSalaPabellon() {
        return idSalaPabellon;
    }

    public void setIdSalaPabellon(int idSalaPabellon) {
        this.idSalaPabellon = idSalaPabellon;
    }

    public String getHoraIngresoPabellon() {
        return horaIngresoPabellon;
    }

    public void setHoraIngresoPabellon(String horaIngresoPabellon) {
        this.horaIngresoPabellon = horaIngresoPabellon;
    }

    public String getHoraInicioCirugia() {
        return horaInicioCirugia;
    }

    public void setHoraInicioCirugia(String horaInicioCirugia) {
        this.horaInicioCirugia = horaInicioCirugia;
    }

    public String getHoraSalidaPabellon() {
        return horaSalidaPabellon;
    }

    public void setHoraSalidaPabellon(String horaSalidaPabellon) {
        this.horaSalidaPabellon = horaSalidaPabellon;
    }

    public String getHoraTerminoCirugia() {
        return horaTerminoCirugia;
    }

    public void setHoraTerminoCirugia(String horaTerminoCirugia) {
        this.horaTerminoCirugia = horaTerminoCirugia;
    }

    public String getTiempoQuirurjico() {
        return tiempoQuirurjico;
    }

    public void setTiempoQuirurjico(String tiempoQuirurjico) {
        this.tiempoQuirurjico = tiempoQuirurjico;
    }

    public int getIdTipoAnestesia() {
        return idTipoAnestesia;
    }

    public void setIdTipoAnestesia(int idTipoAnestesia) {
        this.idTipoAnestesia = idTipoAnestesia;
    }

    public String getHoraAnestesia() {
        return horaAnestesia;
    }

    public void setHoraAnestesia(String horaAnestesia) {
        this.horaAnestesia = horaAnestesia;
    }

    public int getIdTipoHerida() {
        return idTipoHerida;
    }

    public void setIdTipoHerida(int idTipoHerida) {
        this.idTipoHerida = idTipoHerida;
    }

    public int getElectrobisturi() {
        return electrobisturi;
    }

    public void setElectrobisturi(int electrobisturi) {
        this.electrobisturi = electrobisturi;
    }

    public String getPreOperatorio() {
        return preOperatorio;
    }

    public void setPreOperatorio(String preOperatorio) {
        this.preOperatorio = preOperatorio;
    }

    public String getPostOperatorio() {
        return postOperatorio;
    }

    public void setPostOperatorio(String postOperatorio) {
        this.postOperatorio = postOperatorio;
    }

    public String getRutArsenalero() {
        return rutArsenalero;
    }

    public void setRutArsenalero(String rutArsenalero) {
        this.rutArsenalero = rutArsenalero;
    }

    public String getRutProfesionalAyudante() {
        return rutProfesionalAyudante;
    }

    public void setRutProfesionalAyudante(String rutProfesionalAyudante) {
        this.rutProfesionalAyudante = rutProfesionalAyudante;
    }

    public String getRutMedicoAnestesista() {
        return rutMedicoAnestesista;
    }

    public void setRutMedicoAnestesista(String rutMedicoAnestesista) {
        this.rutMedicoAnestesista = rutMedicoAnestesista;
    }

    public String getRutPabellonera() {
        return rutPabellonera;
    }

    public void setRutPabellonera(String rutPabellonera) {
        this.rutPabellonera = rutPabellonera;
    }

    public String getRutPerfusionista() {
        return rutPerfusionista;
    }

    public void setRutPerfusionista(String rutPerfusionista) {
        this.rutPerfusionista = rutPerfusionista;
    }

    public String getDetalleOperatorio() {
        return detalleOperatorio;
    }

    public void setDetalleOperatorio(String detalleOperatorio) {
        this.detalleOperatorio = detalleOperatorio;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
         
    
}
