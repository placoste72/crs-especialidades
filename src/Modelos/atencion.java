/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author a
 */
public class atencion {

    private int idatencion;
    private int idcita;
    private String higado;
    private String via_biliar;
    private String vesicula;
    private String pancreas;
    private int idbazo;
    private int idaorta;
    private String otro_bazo;
    private String otro_aorta;
    private String conclusion;
    private Date fecha_registro;
    private String usuario;
    private String ipusuario;
    private String rutpaciente;
    private String nombrepaciente;
    private String rutdoctor;
    private String profesion;
    private String prestacion;
    private String edadpaciente;
    private String fechanacimiento;
    private String sexo;
    private String domicilio;
    private String comuna;
    private String telefono;
    private String correo_electronico;
    private boolean tengoexcepcion;

    private String resumenatenicon;
    private String otrodiagnostico;
    private String otroexamenes;
    private int sic;
    private int iddestinopaciente;
    private String otrodestino;
    private int indicalente;
    private int cantidadlente;
    private String otrasindicaciones;
    private boolean solicitudpabellon;
    private int estatus;
    private String detallederivacion;

    private int diagnosticosic;
    private String otrodiagnosticosic;
    private String otroprocedimiento;
    private boolean contgipd;

    /**/
    private String rinones;
    private String hipotesis_diagnostica;
    private String medico_solicitante;

    /*para relacion de oftalmologia*/
    private int diagnostico;
    private int lateralidad;
    private int ges;
    private int confirmadescartadocumentos;
    private int confirmadescartadiagnostico;
    private String fundamentodeldiagnostico;
    private String tratamientoeindicaciones;
    private String indicalenteS;
    private int iddiagnostico;
    private String folio;
    private String formatofecha;
    private String lateralidads;
    private String casoges;
    private String especialidad;
    private String conf;
    private boolean tengoipd;
    private String indialt;

    /*para receta*/
    private int numeroreceta;
    private String detallereceta;
    private boolean tengoreceta;
    private boolean tengoindicacionnoquirurjica;
    private int pertinencia;
    private String hora;
    private boolean tengosolicitu;
    private int tratamientolejos;
    private int tratamientocerca;
  
    public boolean isTengoexcepcion() {
        return tengoexcepcion;
    }

    public void setTengoexcepcion(boolean tengoexcepcion) {
        this.tengoexcepcion = tengoexcepcion;
    }

    public int getTratamientolejos() {
        return tratamientolejos;
    }

    public String getIndialt() {
        return indialt;
    }

    public void setIndialt(String indialt) {
        this.indialt = indialt;
    }

    public void setTratamientolejos(int tratamientolejos) {
        this.tratamientolejos = tratamientolejos;
    }

    public int getTratamientocerca() {
        return tratamientocerca;
    }

    public void setTratamientocerca(int tratamientocerca) {
        this.tratamientocerca = tratamientocerca;
    }
    
    

    public boolean isTengosolicitu() {
        return tengosolicitu;
    }

    public void setTengosolicitu(boolean tengosolicitu) {
        this.tengosolicitu = tengosolicitu;
    }

    
    
    public String getHora() {
        return hora;
    }

    /**/
    public void setHora(String hora) {
        this.hora = hora;
    }

    public boolean isTengoindicacionnoquirurjica() {
        return tengoindicacionnoquirurjica;
    }

    public void setTengoindicacionnoquirurjica(boolean tengoindicacionnoquirurjica) {
        this.tengoindicacionnoquirurjica = tengoindicacionnoquirurjica;
    }

    public int getPertinencia() {
        return pertinencia;
    }

    public void setPertinencia(int pertinencia) {
        this.pertinencia = pertinencia;
    }

    public int getNumeroreceta() {
        return numeroreceta;
    }

    public boolean isTengoreceta() {
        return tengoreceta;
    }

    public void setTengoreceta(boolean tengoreceta) {
        this.tengoreceta = tengoreceta;
    }

    public void setNumeroreceta(int numeroreceta) {
        this.numeroreceta = numeroreceta;
    }

    public String getDetallereceta() {
        return detallereceta;
    }

    public void setDetallereceta(String detallereceta) {
        this.detallereceta = detallereceta;
    }

    public boolean isTengoipd() {
        return tengoipd;
    }

    public void setTengoipd(boolean tengoipd) {
        this.tengoipd = tengoipd;
    }

    public int getConfirmadescartadocumentos() {
        return confirmadescartadocumentos;
    }

    public void setConfirmadescartadocumentos(int confirmadescartadocumentos) {
        this.confirmadescartadocumentos = confirmadescartadocumentos;
    }

    public int getConfirmadescartadiagnostico() {
        return confirmadescartadiagnostico;
    }

    public void setConfirmadescartadiagnostico(int confirmadescartadiagnostico) {
        this.confirmadescartadiagnostico = confirmadescartadiagnostico;
    }

    public String getCasoges() {
        return casoges;
    }

    public boolean isContgipd() {
        return contgipd;
    }

    public void setContgipd(boolean contgipd) {
        this.contgipd = contgipd;
    }

    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public void setCasoges(String casoges) {
        this.casoges = casoges;
    }

    public int getDiagnosticosic() {
        return diagnosticosic;
    }

    public String getLateralidads() {
        return lateralidads;
    }

    public void setLateralidads(String lateralidads) {
        this.lateralidads = lateralidads;
    }

    public String getFolio() {
        return folio;
    }

    public String getFormatofecha() {
        return formatofecha;
    }

    public void setFormatofecha(String formatofecha) {
        this.formatofecha = formatofecha;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(String fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public void setDiagnosticosic(int diagnosticosic) {
        this.diagnosticosic = diagnosticosic;
    }

    public String getOtrodiagnosticosic() {
        return otrodiagnosticosic;
    }

    public void setOtrodiagnosticosic(String otrodiagnosticosic) {
        this.otrodiagnosticosic = otrodiagnosticosic;
    }

    public String getOtroprocedimiento() {
        return otroprocedimiento;
    }

    public void setOtroprocedimiento(String otroprocedimiento) {
        this.otroprocedimiento = otroprocedimiento;
    }

    public int getIdatencion() {
        return idatencion;
    }

    public int getIddiagnostico() {
        return iddiagnostico;
    }

    public void setIddiagnostico(int iddiagnostico) {
        this.iddiagnostico = iddiagnostico;
    }

    public String getIndicalenteS() {
        return indicalenteS;
    }

    public void setIndicalenteS(String indicalenteS) {
        this.indicalenteS = indicalenteS;
    }

    public int getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(int diagnostico) {
        this.diagnostico = diagnostico;
    }

    public int getLateralidad() {
        return lateralidad;
    }

    public void setLateralidad(int lateralidad) {
        this.lateralidad = lateralidad;
    }

    public int getGes() {
        return ges;
    }

    public void setGes(int ges) {
        this.ges = ges;
    }

    public String getFundamentodeldiagnostico() {
        return fundamentodeldiagnostico;
    }

    public void setFundamentodeldiagnostico(String fundamentodeldiagnostico) {
        this.fundamentodeldiagnostico = fundamentodeldiagnostico;
    }

    public String getTratamientoeindicaciones() {
        return tratamientoeindicaciones;
    }

    public void setTratamientoeindicaciones(String tratamientoeindicaciones) {
        this.tratamientoeindicaciones = tratamientoeindicaciones;
    }

    public String getResumenatenicon() {
        return resumenatenicon;
    }

    public String getRinones() {
        return rinones;
    }

    public void setRinones(String rinones) {
        this.rinones = rinones;
    }

    public String getHipotesis_diagnostica() {
        return hipotesis_diagnostica;
    }

    public void setHipotesis_diagnostica(String hipotesis_diagnostica) {
        this.hipotesis_diagnostica = hipotesis_diagnostica;
    }

    public String getMedico_solicitante() {
        return medico_solicitante;
    }

    public void setMedico_solicitante(String medico_solicitante) {
        this.medico_solicitante = medico_solicitante;
    }

    public void setResumenatenicon(String resumenatenicon) {
        this.resumenatenicon = resumenatenicon;
    }

    public String getOtrodiagnostico() {
        return otrodiagnostico;
    }

    public void setOtrodiagnostico(String otrodiagnostico) {
        this.otrodiagnostico = otrodiagnostico;
    }

    public String getOtroexamenes() {
        return otroexamenes;
    }

    public void setOtroexamenes(String otroexamenes) {
        this.otroexamenes = otroexamenes;
    }

    public int getSic() {
        return sic;
    }

    public void setSic(int sic) {
        this.sic = sic;
    }

    public int getIddestinopaciente() {
        return iddestinopaciente;
    }

    public void setIddestinopaciente(int iddestinopaciente) {
        this.iddestinopaciente = iddestinopaciente;
    }

    public String getOtrodestino() {
        return otrodestino;
    }

    public void setOtrodestino(String otrodestino) {
        this.otrodestino = otrodestino;
    }

    public int getIndicalente() {
        return indicalente;
    }

    public void setIndicalente(int indicalente) {
        this.indicalente = indicalente;
    }

    public int getCantidadlente() {
        return cantidadlente;
    }

    public void setCantidadlente(int cantidadlente) {
        this.cantidadlente = cantidadlente;
    }

    public String getOtrasindicaciones() {
        return otrasindicaciones;
    }

    public void setOtrasindicaciones(String otrasindicaciones) {
        this.otrasindicaciones = otrasindicaciones;
    }

    public boolean isSolicitudpabellon() {
        return solicitudpabellon;
    }

    public void setSolicitudpabellon(boolean solicitudpabellon) {
        this.solicitudpabellon = solicitudpabellon;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getDetallederivacion() {
        return detallederivacion;
    }

    public void setDetallederivacion(String detallederivacion) {
        this.detallederivacion = detallederivacion;
    }

    public void setIdatencion(int idatencion) {
        this.idatencion = idatencion;
    }

    public String getEdadpaciente() {
        return edadpaciente;
    }

    public void setEdadpaciente(String edadpaciente) {
        this.edadpaciente = edadpaciente;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getPrestacion() {
        return prestacion;
    }

    public void setPrestacion(String prestacion) {
        this.prestacion = prestacion;
    }

    public int getIdcita() {
        return idcita;
    }

    public void setIdcita(int idcita) {
        this.idcita = idcita;
    }

    public String getRutpaciente() {
        return rutpaciente;
    }

    public void setRutpaciente(String rutpaciente) {
        this.rutpaciente = rutpaciente;
    }

    public String getNombrepaciente() {
        return nombrepaciente;
    }

    public void setNombrepaciente(String nombrepaciente) {
        this.nombrepaciente = nombrepaciente;
    }

    public String getRutdoctor() {
        return rutdoctor;
    }

    public void setRutdoctor(String rutdoctor) {
        this.rutdoctor = rutdoctor;
    }

    public String getHigado() {
        return higado;
    }

    public void setHigado(String higado) {
        this.higado = higado;
    }

    public String getVia_biliar() {
        return via_biliar;
    }

    public void setVia_biliar(String via_biliar) {
        this.via_biliar = via_biliar;
    }

    public String getVesicula() {
        return vesicula;
    }

    public void setVesicula(String vesicula) {
        this.vesicula = vesicula;
    }

    public String getPancreas() {
        return pancreas;
    }

    public void setPancreas(String pancreas) {
        this.pancreas = pancreas;
    }

    public int getIdbazo() {
        return idbazo;
    }

    public void setIdbazo(int idbazo) {
        this.idbazo = idbazo;
    }

    public int getIdaorta() {
        return idaorta;
    }

    public void setIdaorta(int idaorta) {
        this.idaorta = idaorta;
    }

    public String getOtro_bazo() {
        return otro_bazo;
    }

    public void setOtro_bazo(String otro_bazo) {
        this.otro_bazo = otro_bazo;
    }

    public String getOtro_aorta() {
        return otro_aorta;
    }

    public void setOtro_aorta(String otro_aorta) {
        this.otro_aorta = otro_aorta;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getIpusuario() {
        return ipusuario;
    }

    public void setIpusuario(String ipusuario) {
        this.ipusuario = ipusuario;
    }

}
