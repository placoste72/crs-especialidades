/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;
import java.io.InputStream;

/**
 *
 * @author Placoste
 */
public class cargar_masiva_citas {
    private  String rut;
    private String nombre;
    private String apellidopaterno;
    private String apellidomaterno;
    private String telefono;
    private String email;
    private int idmotivo;
    private String Descripcion_motivo;
    
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getApellidoPaterno() {
        return apellidopaterno;
    }

    public void setApellidoPaterno(String apellidopaterno) {
        this.apellidopaterno = apellidopaterno;
    }
    
    public String getApellidoMaterno() {
        return apellidomaterno;
    }

    public void setApellidoMaterno(String apellidomaterno) {
        this.apellidomaterno = apellidomaterno;
    }
}
